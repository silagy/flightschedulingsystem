﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Endilo.ExportToExcel
{
    public class ExportToExcelAttribute : Attribute
    {
        public Type ResourceType { get; set; }
        public string HeaderName { get; set; }
    }

    public static class Helpers
    {
        public static string GetEnumDescription<TEnum>(this TEnum value)
        {
            FieldInfo fi = value.GetType().GetField(value.ToString());

            if (fi != null)
            {
                var attributes = (DescriptionAttribute[])fi.GetCustomAttributes(typeof(DescriptionAttribute), false);

                if (attributes.Length > 0)
                    return attributes[0].Description;
            }

            return value.ToString();
        }
    }
}
