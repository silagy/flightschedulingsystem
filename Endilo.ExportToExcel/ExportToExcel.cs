﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Resources;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Drawing;
using Endilo.ExportToExcel;
using OfficeOpenXml;
using OfficeOpenXml.Style;


namespace Endilo.ExportToExcel
{
    public class ExportToExcel<T>
    {
        // Setting the data holder
        private List<T> data;
        private ExcelPackage wp;
        private ExcelWorksheet ws;
        private FileInfo file;
        private string SheetName;


        //Getting the data when creating new instance of this class
        public ExportToExcel(List<T> i_obj, string i_SheetName)
        {
            data = i_obj;
            SheetName = i_SheetName;
            InitiatingExcelWOrkPackage();
            CreatingHeaders();
            CreatingValues();
            CustomizeWorksheet();
        }

        public MemoryStream GetFile()
        {

            var stream = new MemoryStream();
            wp.SaveAs(stream);

            return stream;
        }

        private void InitiatingExcelWOrkPackage()
        {
            ExcelPackage.LicenseContext = OfficeOpenXml.LicenseContext.NonCommercial;

            file = new FileInfo(SheetName);
            wp = new ExcelPackage(file);

            wp.Workbook.Worksheets.Add(SheetName);

            ws = wp.Workbook.Worksheets[0];

            ws.View.RightToLeft = true;
            ws.Name = SheetName;
            ws.Cells.Style.Font.Size = 11;
            ws.Cells.Style.Font.Name = "arial";

            

            ws.Cells[1, 1, 300, 300].Style.ReadingOrder = ExcelReadingOrder.RightToLeft;

            ws.Cells[1, 1].Style.Font.Size = 16;
            ws.Cells[1, 1].Style.Font.Bold = true;
            ws.Cells[1, 1].Value = SheetName;
            ws.Cells[2, 1].Value = "מכיל מידע מוגן לפי חוק הגנת הפרטיות - המוסר שלא כדין עובר עבירה";

            ws.Cells[2, 1, 2, 8].Merge = true;
            ws.Cells[1, 1, 1, 8].Merge = true;

        }

        private void CustomizeWorksheet()
        {
            ws.Cells["A1:K20"].AutoFitColumns();
        }

        private void CreatingHeaders()
        {
            int row = 5;
            int column = 1;
            foreach (var prop in typeof(T).GetProperties().Where(prop => Attribute.IsDefined(prop,typeof(ExportToExcelAttribute))))
            {
                //var n = prop.GetCustomAttributes(true).OfType<T>().Cast<T>();
                ws.Cells[row, column].Value = GetDisplayName(prop);
                ws.Cells[row, column].Style.Fill.PatternType = ExcelFillStyle.Solid;
                ws.Cells[row,column].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(229,229,229));
                ws.Cells[row, column].Style.Font.Bold = true;
                column++;
            }
        }

        private void CreatingValues()
        {
            // Check if the query has values
            if (data.Count > 0)
            {

                //ws.Cells["A6"].LoadFromCollection(data);
                ws.Cells["A6"].LoadFromDataTable(ToDataTable(data), false);

                foreach (var cell in ws.Cells)
                {
                    if (cell.Value != null && cell != null)
                    {
                        DateTime datetimeObject;
                        if (DateTime.TryParse(cell.Value.ToString(), out datetimeObject))
                        {
                            cell.Style.Numberformat.Format = "dd/MM/yyyy";
                        }

                    }


                }
            }
        }

        private string GetDisplayName(PropertyInfo property)
        {


            var attrName = GetAttributeDisplayName(property);
            if (!string.IsNullOrEmpty(attrName))
                return attrName;

            var metaName = GetMetaDisplayName(property);
            if (!string.IsNullOrEmpty(metaName))
                return metaName;

            return property.Name.ToString();
        }

        private string GetAttributeDisplayName(PropertyInfo property)
        {
            var at = property.GetCustomAttributes<ExportToExcelAttribute>(true).SingleOrDefault();
            if (at == null)
                return null;
            ResourceManager resourceManager = new ResourceManager(at.ResourceType.Namespace + "." + at.ResourceType.Name, at.ResourceType.Assembly);
            return resourceManager.GetString(at.HeaderName);

        }

        private string GetMetaDisplayName(PropertyInfo property)
        {
            var atts = property.DeclaringType.GetCustomAttributes(true);
            if (atts.Length == 0)
                return null;

            var metaAttr = atts[0] as MetadataTypeAttribute;
            var metaProperty =
                metaAttr.MetadataClassType.GetProperty(property.Name);
            if (metaProperty == null)
                return null;
            return GetAttributeDisplayName(metaProperty);
        }

        public DataTable ToDataTable<T>(List<T> data)
        {
            PropertyDescriptorCollection props =
                TypeDescriptor.GetProperties(typeof(T));

            List<PropertyDescriptor> reportProps = new List<PropertyDescriptor>();

            DataTable table = new DataTable();
            for (int i = 0; i < props.Count; i++)
            {
                PropertyDescriptor prop = props[i];
                bool hasDataMember = prop.Attributes.OfType<ExportToExcelAttribute>().Any();
                if (hasDataMember)
                {
                    if (prop.PropertyType.IsEnum)
                    {
                        table.Columns.Add(prop.Name, typeof(string));
                    }
                    else
                    {
                        table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
                    }
                    
                    reportProps.Add(prop);
                }
                
                
            }
            PropertyDescriptorCollection rProps = new PropertyDescriptorCollection(reportProps.ToArray());

            object[] values = new object[rProps.Count];
            foreach (T item in data)
            {
                for (int i = 0; i < values.Length; i++)
                {
                    if (rProps[i].PropertyType.IsEnum)
                    {
                        values[i] = Enum.ToObject(rProps[i].PropertyType, rProps[i].GetValue(item))
                            .GetEnumDescription();
                    }
                    else
                    {
                        values[i] = rProps[i].GetValue(item);
                    }
                }
                table.Rows.Add(values);
            }
            return table;
        }

        

    }
}
