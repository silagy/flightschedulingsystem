﻿CREATE TABLE [dbo].[TripsFlightAssociations] (
    [TripID]   INT NOT NULL,
    [FlightID] INT NOT NULL,
    PRIMARY KEY CLUSTERED ([TripID] ASC, [FlightID] ASC),
    CONSTRAINT [FlightFKF2] FOREIGN KEY ([FlightID]) REFERENCES [dbo].[Flights] ([ID]),
    CONSTRAINT [TripsFKF1] FOREIGN KEY ([TripID]) REFERENCES [dbo].[Trips] ([ID])
);

