﻿CREATE TABLE [dbo].[Trips] (
    [ID]            INT            IDENTITY (1, 1) NOT NULL,
    [DepartureDate] DATETIME       NOT NULL,
    [DestinationID] INT            NOT NULL,
    [ReturningDate] DATETIME       NOT NULL,
    [ReturingField] INT            NOT NULL,
    [Comments]      NVARCHAR (MAX) NULL,
    [IsActive]      BIT            DEFAULT ((1)) NOT NULL,
    [CreationDate]  DATETIME       DEFAULT (getdate()) NOT NULL,
    [UpdateDate]    DATETIME       NULL,
    PRIMARY KEY CLUSTERED ([ID] ASC)
);

