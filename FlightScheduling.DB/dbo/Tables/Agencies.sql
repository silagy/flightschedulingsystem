﻿CREATE TABLE [dbo].[Agencies] (
    [ID]           INT             IDENTITY (1, 1) NOT NULL,
    [Name]         NVARCHAR (2000) NOT NULL,
    [Phone]        NVARCHAR (2000) NULL,
    [Fax]          NVARCHAR (2000) NULL,
    [Email]        NVARCHAR (2000) NULL,
    [Manager]      NVARCHAR (2000) NULL,
    [ManagerPhone] NVARCHAR (2000) NULL,
    [IsActive]     BIT             DEFAULT ((1)) NOT NULL,
    [CreationDate] DATETIME        DEFAULT (getdate()) NOT NULL,
    [UpdateDate]   DATETIME        NULL,
    PRIMARY KEY CLUSTERED ([ID] ASC)
);

