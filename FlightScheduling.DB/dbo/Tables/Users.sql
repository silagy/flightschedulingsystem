﻿CREATE TABLE [dbo].[Users] (
    [ID]           INT             IDENTITY (1, 1) NOT NULL,
    [Firstname]    NVARCHAR (2000) NOT NULL,
    [Lastname]     NVARCHAR (2000) NOT NULL,
    [Email]        NVARCHAR (2000) NOT NULL,
    [Password]     NVARCHAR (2000) NOT NULL,
    [Phone]        NVARCHAR (2000) NULL,
    [UserType]     INT             NOT NULL,
    [IsActive]     BIT             DEFAULT ((1)) NOT NULL,
    [IsActivate]   BIT             DEFAULT ((1)) NOT NULL,
    [AgencyID]     INT             NULL,
    [SchoolID]     INT             NULL,
    [CreationDate] DATETIME        NOT NULL,
    PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [SchoolIDFKF1] FOREIGN KEY ([SchoolID]) REFERENCES [dbo].[Schools] ([ID]),
    CONSTRAINT [UsersFKF1] FOREIGN KEY ([AgencyID]) REFERENCES [dbo].[Agencies] ([ID])
);

