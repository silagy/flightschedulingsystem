﻿CREATE TABLE [dbo].[Schools] (
    [ID]           INT             IDENTITY (1, 1) NOT NULL,
    [InstituteID]  INT             NOT NULL,
    [Name]         NVARCHAR (2000) NOT NULL,
    [Email]        NVARCHAR (2000) NULL,
    [Fax]          NVARCHAR (2000) NULL,
    [Phone]        NVARCHAR (2000) NULL,
    [Manager]      NVARCHAR (2000) NULL,
    [ManagerPhone] NVARCHAR (2000) NULL,
    [ManagerEmail] NVARCHAR (2000) NULL,
    [IsActive]     BIT             DEFAULT ((1)) NOT NULL,
    [CreationDate] DATETIME        DEFAULT (getdate()) NOT NULL,
    [UpdateDate]   DATETIME        NULL,
    PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [SchoolsInstituteIDUQFKF1] UNIQUE NONCLUSTERED ([InstituteID] ASC)
);

