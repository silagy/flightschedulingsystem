﻿CREATE TABLE [dbo].[Flights] (
    [ID]                 INT             IDENTITY (1, 1) NOT NULL,
    [StartDate]          DATETIME        NOT NULL,
    [DepartureFieldID]   INT             NOT NULL,
    [DestinationID]      INT             NOT NULL,
    [NumberOfPassengers] INT             NOT NULL,
    [AirlineName]        NVARCHAR (2000) NULL,
    [FlightNumber]       NVARCHAR (2000) NULL,
    [TakeOffFromIsrael]  BIT             NULL,
    [Comments]           NVARCHAR (MAX)  NULL,
    [IsActive]           BIT             DEFAULT ((1)) NOT NULL,
    [CreationDate]       DATETIME        DEFAULT (getdate()) NOT NULL,
    [UpdateDate]         DATETIME        NULL,
    PRIMARY KEY CLUSTERED ([ID] ASC)
);

