﻿CREATE TABLE [dbo].[ExpeditionsFlights] (
    [ID]                 INT IDENTITY (1, 1) NOT NULL,
    [ExpeditionID]       INT NOT NULL,
    [FlightID]           INT NOT NULL,
    [NumberOfPassengers] INT NOT NULL,
    PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [ExpeditionIDFKF1] FOREIGN KEY ([ExpeditionID]) REFERENCES [dbo].[Expeditions] ([ID]),
    CONSTRAINT [FlightsFKF2] FOREIGN KEY ([FlightID]) REFERENCES [dbo].[Flights] ([ID])
);

