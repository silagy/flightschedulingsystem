﻿CREATE Procedure [dbo].[FlightCountPerTrip]
AS
BEGIN
SET NOCOUNT ON

;WITH FlightCount AS (
SELECT 
	FL.ID,
	FL.NumberOfPassengers,
	SUM(EF.NumberOfPassengers) AS AssignedPassengers
	FROM Flights as FL
	LEFT JOIN ExpeditionsFlights AS EF ON FL.ID = EF.FlightID
	GROUP BY
		FL.ID,
		FL.NumberOfPassengers)

SELECT 
	TFA.TripID,
	TR.DepartureDate,
	TR.DestinationID AS TripDestination,
	TFA.FlightID, 
	FL.StartDate,
	FL.DestinationID AS FlightDestination,
	FL.AirlineName,
	FL.FlightNumber,
	FC.NumberOfPassengers,
	CASE 
	WHEN FC.AssignedPassengers IS NULL THEN 0
	ELSE FC.AssignedPassengers
	END as 'AssignedPassengers',
	FC.NumberOfPassengers - 
	CASE 
	WHEN FC.AssignedPassengers IS NULL THEN 0
	ELSE FC.AssignedPassengers
	END as 'AvailableSeats'
FROM TripsFlightAssociations AS TFA
INNER JOIN Trips AS TR ON TFA.TripID = TR.ID
INNER JOIN Flights AS FL ON TFA.FlightID = FL.ID
INNER JOIN FlightCount AS FC ON FL.ID = FC.ID
WHERE FL.TakeOffFromIsrael = 1
ORDER BY TR.DepartureDate ASC
END