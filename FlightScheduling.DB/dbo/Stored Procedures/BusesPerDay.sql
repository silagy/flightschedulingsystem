﻿CREATE Procedure [dbo].[BusesPerDay]
(
	@StartDate DATETIME,
	@EndDate DATETIME
)
AS
BEGIN
SET NOCOUNT ON


--SET @StartDate = '2016-09-01'
--SET @EndDate = '2016-09-30';

;WITH 
 N0 AS (SELECT 1 AS n UNION ALL SELECT 1)
,N1 AS (SELECT 1 AS n FROM N0 t1, N0 t2)
,N2 AS (SELECT 1 AS n FROM N1 t1, N1 t2)
,N3 AS (SELECT 1 AS n FROM N2 t1, N2 t2)
,N4 AS (SELECT 1 AS n FROM N3 t1, N3 t2)
,N5 AS (SELECT 1 AS n FROM N4 t1, N4 t2)
,N6 AS (SELECT 1 AS n FROM N5 t1, N5 t2)
,nums AS (SELECT ROW_NUMBER() OVER (ORDER BY (SELECT 1)) AS num FROM N6),
DaysInRange as (select
            DATEADD(day, num-1, @StartDate) [Date],
            datepart(dw,DATEADD(day, num-1, @StartDate)) AS NumOfDayInWeek,
            datename(dw,DATEADD(day, num-1, @StartDate)) AS NameOfDayInWeek
            FROM nums
            WHERE num <= DATEDIFF(day, @StartDate, @EndDate) + 1),
ExpeditionStatus AS (
	SELECT 
		TP.ID as TripID,
		TP.DepartureDate,
		TP.ReturningDate,
		EX.ID as ExpeditionID,
		EX.BasesPerDay
	FROM Trips as TP
	INNER JOIN Expeditions as EX ON TP.ID = EX.TripID
	WHERE 
		TP.DepartureDate >= @StartDate 
		AND  TP.DepartureDate <= @EndDate 
		AND EX.Status = 2
)

SELECT 
	DIR.Date,
	CASE 
	WHEN SUM(ES.BasesPerDay) IS NULL THEN 0
	ELSE SUM(ES.BasesPerDay)
	END as BusesPerDay,
	COUNT(ES.ExpeditionID) as ExpeditionsPerDay
FROM DaysInRange as DIR
LEFT JOIN ExpeditionStatus AS ES ON Date BETWEEN ES.DepartureDate AND ES.ReturningDate
GROUP BY
	DIR.Date
ORDER BY DIR.Date
END


