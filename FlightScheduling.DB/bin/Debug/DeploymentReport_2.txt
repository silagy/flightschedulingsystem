﻿** Highlights
     Tables that will be rebuilt
       [dbo].[Expeditions]
     Clustered indexes that will be dropped
       None
     Clustered indexes that will be created
       None
     Possible data issues
       None

** User actions
     Drop
       unnamed constraint on [dbo].[Expeditions] (Default Constraint)
       unnamed constraint on [dbo].[Expeditions] (Default Constraint)
       unnamed constraint on [dbo].[Expeditions] (Default Constraint)
       unnamed constraint on [dbo].[Expeditions] (Default Constraint)
     Table rebuild
       [dbo].[Expeditions] (Table)

** Supporting actions
     Drop
       [dbo].[SchoolFKF1] (Foreign Key)
       [dbo].[TripExpFKF1] (Foreign Key)
       [dbo].[UserIDExpFKF1] (Foreign Key)
       [dbo].[AgencyIDExpFKF1] (Foreign Key)
       [dbo].[ExpeditionIDFKF1] (Foreign Key)
     Create
       [dbo].[SchoolFKF1] (Foreign Key)
       [dbo].[TripExpFKF1] (Foreign Key)
       [dbo].[UserIDExpFKF1] (Foreign Key)
       [dbo].[AgencyIDExpFKF1] (Foreign Key)
       [dbo].[ExpeditionIDFKF1] (Foreign Key)
     Refresh
       [dbo].[BusesPerDay] (Procedure)
