﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FlightScheduling.DAL;
using FlightScheduling.DAL.CustomEntities.Notifications;
using Endilo.Emails;
using Flight.DAL.FlightDB.Methods;
using Flight.Membership;
using FlightScheduling.DAL.Abstractions;
using FlightScheduling.DAL.CustomEntities.Outbox;
using FlightScheduling.DAL.DomainEvents;
using FlightScheduling.DAL.NotificationsRepo;
using Newtonsoft.Json;

namespace NotificationCenterJob
{
    class Program
    {
        private static DBRepository _dbDapper = new DBRepository();
        private static List<Notifications> _notifications = new List<Notifications>();

        static void Main(string[] args)
        {
            
            HandleOutBoxMessages();

            //Get Notifications to be Sent
            _notifications = _dbDapper.GetRecords<Notifications>(QueriesRepository.GetNotificationsToBeSnetByStatus, new { Status = (int)eNotificationStatus.Pendding }).ToList();

            foreach (var _notification in _notifications)
            {
                bool results = SendMail.SendSingleMail(_notification.EmailAddress, _notification.MessageSubject, _notification.MessageBody);
                if (results)
                {
                    string message = "Email has been sent successfully";
                    _dbDapper.ExecuteCommand(QueriesRepository.UpdateNotificationToBeSnet, new { NotificationStatus = (int)eNotificationStatus.Sent, SendDate = DateTime.Now, Remark = message, _notification.ID });
                }
                else
                {
                    string message = "There was an error, therefore email hasn't been sent";
                    _dbDapper.ExecuteCommand(QueriesRepository.UpdateNotificationToBeSnet, new { NotificationStatus = (int)eNotificationStatus.Failed, SendDate = DateTime.Now, Remark = message, _notification.ID });
                }
            }

            // Delete Old Notifications
            bool deleteNotifications = _dbDapper.UpdateOperation(QueriesRepository.DeleteOldNotifications, new { Date = DateTime.Now.AddMonths(-3) });
        }

        static void HandleOutBoxMessages()
        {
            var outboxMessages =
                _dbDapper.GetRecords<Outbox>(QueriesRepository.Outbox.GetTopOutboxMessages, new {});

            foreach (var outboxMessage in outboxMessages)
            {
                IDomainEvent domainEvent;

                try
                {
                    domainEvent = JsonConvert.DeserializeObject<IDomainEvent>(outboxMessage.Content,
                        new JsonSerializerSettings()
                        {
                            TypeNameHandling = TypeNameHandling.All
                        });

                    if (domainEvent is null)
                    {
                        continue;
                    }

                    switch (domainEvent.GetType().Name)
                    {
                        case nameof(InstructorCreatedDomainEvent):
                            var result = HandelInstructorCreatedDomainEvent(domainEvent as InstructorCreatedDomainEvent);
                            if (!result.IsSuccuss)
                            {
                                outboxMessage.Error = result.Error;
                            }
                            
                            // Create User created domain event
                            var userCreatedDomainEvent = new UserCreatedDomainEvent(result.Value.ID);
                            var outbox = DomainEventExtensions.GetOutboxFromDomainEvent(userCreatedDomainEvent);
                            bool outboxResults = _dbDapper.ExecuteCommand(QueriesRepository.Outbox.InsertOutbox, outbox);
                            
                            break;
                        case nameof(UserCreatedDomainEvent):

                            Result<Users> userCreatedResult = HandelUserCreatedDomainEvent(domainEvent as UserCreatedDomainEvent);
                            break;
                    }
                    
                    outboxMessage.ProcessOnUtc = DateTime.Now;
                }
                catch (Exception ex)
                {
                    outboxMessage.ProcessOnUtc = DateTime.Now;
                    outboxMessage.Error = ex.Message;
                }

                bool results = _dbDapper.UpdateOperation(QueriesRepository.Outbox.UpdateOutboxMessage, new
                {
                    outboxMessage.ProcessOnUtc,
                    outboxMessage.Error,
                    outboxMessage.Id
                });
            }
        }

        private static Result<Users> HandelUserCreatedDomainEvent(UserCreatedDomainEvent domainEvent)
        {
            var result = new Result<Users>();
            // Get the user
            var user = _dbDapper.GetUserByID(domainEvent.Id);
            if (NotificationRepository.IsValidEmail(user.Email))
            {

                Notifications notificationToSend = NotificationRepository.CreateWelcomeMessage(user);
                
                bool results = SendMail.SendSingleMail(notificationToSend.EmailAddress, notificationToSend.MessageSubject, notificationToSend.MessageBody);

                result.IsSuccuss = results;
                result.Value = user;
            }

            result.IsSuccuss = false;
            return result;
        }

        static Result<Users> HandelInstructorCreatedDomainEvent(InstructorCreatedDomainEvent domainEvent)
        {
            var result = new Result<Users>();
            // Get Instructor
            var instructor = _dbDapper.GetInstructor(domainEvent.InstructorId);
            
            if(instructor == null)
            {
                result.IsSuccuss = false;
                result.Error = "Instructor not found";
                return result;
            }
            
            // Get Instructor Role
            var instructorRole = _dbDapper.GetRecords<InstructorRoles>(
                QueriesRepository.GetInstructorRole,
                new { ID = instructor.RoleID })
                .FirstOrDefault();

            if (instructorRole == null)
            {
                result.IsSuccuss = false;
                result.Error = "Cannot find instructor role";
                return result;
            }
            
            // Create user for instructor only if in the 'Allow Login' is enabled
            if (instructorRole.AllowLogin != null && (bool)instructorRole.AllowLogin)
            {
                return CreateUserForInstructor(instructor, result);
            }
            
            return result;
        }

        private static Result<Users> CreateUserForInstructor(Instructors instructor, Result<Users> result)
        {
            Users user = new Users();
            // Check if user with the same email already exists
            user = _dbDapper.GetRecords<Users>(QueriesRepository.GetUserByEmail, new
            {
                Email = instructor.Email
            }).FirstOrDefault();

            if (!(user != null && user.ID > 0))
            {
                //User doesn't exits
                user = new Users();
                user.Firstname = instructor.FirstNameHE;
                user.Lastname = instructor.LastNameHE;
                user.Email = instructor.Email;
                user.IsActive = true;
                user.IsActivate = true;
                user.CreationDate = DateTime.Now;
                user.UserType = (int)eRole.Instructor;
                //Update user password
                user.Password = instructor.InstructorID;
                var _newPassword = CryptoHelpers.EncryptPassword(user.Password, CryptoHelpers.k_SHA256CryptoAlgorithm);
                user.Password = _newPassword;

                // Set permission Role ID
                user.PermissionRoleID = Int32.Parse(ConfigurationManager.AppSettings["PermissionRoleId"]);

                // Set the instructor internal ID as the instructorID in the users table
                user.InstructorID = instructor.ID;

                //Add user
                int results = _dbDapper.GetRecords<int>(QueriesRepository.AddUserFromInstructorFolder, new
                {
                    user.Firstname,
                    user.Lastname,
                    user.Email,
                    user.Password,
                    user.Phone,
                    user.UserType,
                    user.IsActive,
                    user.IsActivate,
                    user.SchoolID,
                    user.CreationDate,
                    user.PermissionRoleID,
                    user.InstructorID
                }).FirstOrDefault();

                result.IsSuccuss = results > 0;
                user.ID = results;
                result.Value = user;
                return result;
            }
            else
            {
                bool resutls = _dbDapper.UpdateOperation(QueriesRepository.UpdateInstructorIDInUsers,
                    new { InstructorID = instructor.ID, user.ID });
                result.IsSuccuss = resutls;
                result.Value = user;
                return result;
            }
        }
    }
}
