﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FlightScheduling.Web.Models.Expeditions
{
    public class FullCalendar
    {
        public List<FullCalendarEvent> events { get; set; }

        public FullCalendar()
        {
            events = new List<FullCalendarEvent>();
        }
    }

    public class FullCalendarEvent
    {
        public string id { get; set; }
        public string title { get; set; }
        public bool allDay { get; set; }
        public string start { get; set; }
        public string end { get; set; }
        public string rendering { get; set; }
        public string backgroundColor { get; set; }
        public string eventBackgroundColor { get; set; }
        public string color { get; set; }
        public string overlap { get; set; }
        public string url { get; set; }

    }

    public enum EventRendering
    {
        background,
        inversebackground
    }


}