﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Endilo.ExportToExcel;
using Flight.DAL.FlightDB.Methods;
using FlightScheduling.DAL;
using FlightScheduling.Language;
using FlightScheduling.Web.Areas.Administration.Models.Flights;
using FlightScheduling.Web.Areas.Administration.Models.Schools;
using FlightScheduling.Web.Areas.Administration.Models.Trips;
using LinqKit;

namespace FlightScheduling.Web.Models.Expeditions
{
    public class ExpeditionStatusViewModel
    {
        //Properties
        public ExpeditionFilters filters { get; set; }

        public List<ExpeditionStatus> Expeditions { get; set; }

        public List<DAL.Expeditions> _expeditions { get; set; }

        private FlightDB _db;

        public ExpeditionStatusViewModel()
        {
            filters = new ExpeditionFilters();
            Expeditions = new List<ExpeditionStatus>();
            _expeditions = new List<DAL.Expeditions>();
            _db = new FlightDB();
        }

        public void FilterExpeditions(Users _CurrentUser)
        {
            // Enabled Expedition Status Filter
            bool _EnabledExpeditionStatus = true;

            //Constract predicate filters
            var predicate = PredicateBuilder.True<DAL.Expeditions>();

            //Remove deleted records
            predicate = predicate.And(e => e.IsActive == true);

            //Apply Logic for filtering the results based on the user role
            switch (_CurrentUser.RuleEnum)
            {
                case eRole.Agent:
                    {
                        predicate = predicate.And(e => e.AgencyID == _CurrentUser.AgencyID);
                        break;
                    }
                case eRole.Viewer:
                    {
                        predicate = predicate.And(e => e.Status != (int)eExpeditionStatus.Rejected);
                        _EnabledExpeditionStatus = false;
                        break;
                    }
                case eRole.SchoolManager:
                    {
                        predicate = predicate.And(e => e.SchoolID == _CurrentUser.SchoolID);
                        break;
                    }
            }

            if (this.filters.StartTime != null)
            {
                predicate = predicate.And(e => e.Trips.DepartureDate >= this.filters.StartTime);
            }

            if (this.filters.EndTime != null)
            {
                predicate = predicate.And(e => e.Trips.ReturningDate <= this.filters.EndTime);
            }

            if (this.filters.ExpeditionID != null)
            {
                predicate = predicate.And(e => e.ID == this.filters.ExpeditionID);
            }

            if (this.filters.ExpeditionsStatues != null && this.filters.ExpeditionsStatues.Any() && _EnabledExpeditionStatus)
            {
                predicate = predicate.And(e => this.filters.ExpeditionsStatues.Any(s => s == e.Status));
            }

            if (this.filters.InstituteID != null)
            {
                predicate = predicate.And(e => e.Schools.InstituteID == this.filters.InstituteID);
            }

            if (this.filters.SchoolID != null)
            {
                predicate = predicate.And(e => e.SchoolID == this.filters.SchoolID);
            }
            var expeditions = _db.Expeditions.Include("ExpeditionsFlights").AsExpandable().Where(predicate);
            _expeditions = expeditions.ToList();
        }

        public void ConverExpeditionToViewModel()
        {
            Expeditions = _expeditions.Select(e => new ExpeditionStatus()
            {
                ID = e.ID,
                BusesRequiredPerDay = e.BasesPerDay,
                Comments = e.Comments,
                ExpeditionManager = e.Manager,
                ExpeditionManagerCell = e.ManagerCell,
                ExpeditionManagerEmail = e.ManagerEmail,
                ExpeditionManagerPhone = e.ManagerPhone,
                NumOfPassangersRequired = e.NumberOFpassengers,
                AssignedPassangers = e.ExpeditionsFlights.Where(f => f.Flights.TakeOffFromIsrael == true).Sum(f => f.NumberOfPassengers),
                CreationDate = e.CreationDate,
                School = e.Schools,
                Status = (eExpeditionStatus)e.Status,
                AgentStatus = e.AgencyID == 6 ? eAgentExpeditionStatus.NotAssigned : e.AgentApproval == true ? eAgentExpeditionStatus.Approved : eAgentExpeditionStatus.Assigned,
                TripDetails = new TripViewModelBasic()
                {
                    ID = e.Trips.ID,
                    CreationDate = e.Trips.CreationDate,
                    DepartureDate = e.Trips.DepartureDate,
                    Destenation = (FlightViewModel.FlightsFields)e.Trips.DestinationID,
                    ReturnDate = e.Trips.ReturningDate,
                    ReturnedFrom = (FlightViewModel.FlightsFields)e.Trips.ReturingField,
                    Comments = e.Comments
                },
                PotentialPassengers = e.NumberOFpassengers,
                LinkedFlights = e.ExpeditionsFlights.Select(f => new LinkedFlights()
                {
                    ID = f.FlightID,
                    AssignPassengers = f.NumberOfPassengers,
                    AirlineName = f.Flights.AirlineName,
                    FlightNumber = f.Flights.FlightNumber,
                    DepartureField = (FlightViewModel.FlightsFields)f.Flights.DepartureFieldID,
                    DepartureDate = f.Flights.StartDate,
                    DestenationField = (FlightViewModel.FlightsFields)f.Flights.DestinationID,
                    Comments = f.Flights.Comments
                }).ToList()
            }).OrderByDescending(o => o.TripDetails.DepartureDate).ToList();
        }
    }

    [Serializable]
    public class ExpeditionFilters
    {
        //Properties
        [Display(ResourceType = typeof(ExpeditionStrings), Name = "ID")]
        public int? ExpeditionID { get; set; }

        [Display(ResourceType = typeof(SchoolsStrings), Name = "InstituteID")]
        public int? InstituteID { get; set; }

        [Display(ResourceType = typeof(ExpeditionStrings), Name = "Status")]
        public eExpeditionStatus EExpeditionStatus { get; set; }

        public IEnumerable<int> ExpeditionsStatues { get; set; }

        [Display(ResourceType = typeof(SchoolsStrings), Name = "Name")]
        public List<EntitySelectList> Schools { get; set; }

        public int? SchoolID { get; set; }

        [Display(ResourceType = typeof(TripsStrings), Name = "DepartureDate")]
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }

        public ExpeditionFilters()
        {
            Schools = new List<EntitySelectList>();
            StartTime = new DateTime(DateTime.Now.Year, 01, 01);
            EndTime = new DateTime(DateTime.Now.Year + 1, 12, 31);
        }

        public void LoadSchools()
        {
            using (var _db = new FlightDB())
            {
                Schools = _db.Schools.Where(s => s.IsActive == true).Select(s => new EntitySelectList()
                {
                    ID = s.ID,
                    Name = s.Name
                }).ToList();
            }
        }


    }
    public class ExpeditionStatus
    {
        //Properties
        [Display(ResourceType = typeof(ExpeditionStrings), Name = "ID")]
        [ExportToExcel(ResourceType = typeof(ExpeditionStrings), HeaderName = "ID")]

        public int ID { get; set; }

        public TripViewModelBasic TripDetails { get; set; }

        [ExportToExcel(ResourceType = typeof(TripsStrings), HeaderName = "ID")]
        public int TripID => this.TripDetails.ID;

        [ExportToExcel(ResourceType = typeof(TripsStrings), HeaderName = "DepartureDate")]
        public DateTime DepartureDate => this.TripDetails.DepartureDate;

        [Display(ResourceType = typeof(ExpeditionStrings), Name = "PotentialPassengers")]
        [ExportToExcel(ResourceType = typeof(ExpeditionStrings), HeaderName = "PotentialPassengers")]
        public int PotentialPassengers { get; set; }

        [Display(ResourceType = typeof(ExpeditionStrings), Name = "TotalPassengers")]
        [ExportToExcel(ResourceType = typeof(ExpeditionStrings), HeaderName = "TotalPassengers")]
        public int TotalPassengers { get; set; }

        [Display(ResourceType = typeof(ExpeditionStrings), Name = "NumOfPassangersRequired")]
        [ExportToExcel(ResourceType = typeof(ExpeditionStrings), HeaderName = "NumOfPassangersRequired")]
        public int NumOfPassangersRequired { get; set; }

        [Display(ResourceType = typeof(ExpeditionStrings), Name = "BusesRequiredPerDay")]
        [ExportToExcel(ResourceType = typeof(ExpeditionStrings), HeaderName = "BusesRequiredPerDay")]
        public int BusesRequiredPerDay { get; set; }

        [ExportToExcel(ResourceType = typeof(ExpeditionStrings), HeaderName = "ExpeditionManager")]
        [Display(ResourceType = typeof(ExpeditionStrings), Name = "ExpeditionManager")]
        public string ExpeditionManager { get; set; }

        [ExportToExcel(ResourceType = typeof(ExpeditionStrings), HeaderName = "ExpeditionManagerPhone")]
        [Display(ResourceType = typeof(ExpeditionStrings), Name = "ExpeditionManagerPhone")]
        public string ExpeditionManagerPhone { get; set; }

        [ExportToExcel(ResourceType = typeof(ExpeditionStrings), HeaderName = "ExpeditionManagerCell")]
        [Display(ResourceType = typeof(ExpeditionStrings), Name = "ExpeditionManagerCell")]
        public string ExpeditionManagerCell { get; set; }

        [ExportToExcel(ResourceType = typeof(ExpeditionStrings), HeaderName = "ExpeditionManagerEmail")]
        [Display(ResourceType = typeof(ExpeditionStrings), Name = "ExpeditionManagerEmail")]
        public string ExpeditionManagerEmail { get; set; }

        [ExportToExcel(ResourceType = typeof(ExpeditionStrings), HeaderName = "Status")]
        [Display(ResourceType = typeof(ExpeditionStrings), Name = "Status")]
        public eExpeditionStatus Status { get; set; }

        [ExportToExcel(ResourceType = typeof(ExpeditionStrings), HeaderName = "AgentStatus")]
        [Display(ResourceType = typeof(ExpeditionStrings), Name = "AgentStatus")]
        public eAgentExpeditionStatus AgentStatus { get; set; }

        [ExportToExcel(ResourceType = typeof(CommonStrings), HeaderName = "CreationDate")]
        [Display(ResourceType = typeof(CommonStrings), Name = "CreationDate")]
        public DateTime CreationDate { get; set; }

        [ExportToExcel(ResourceType = typeof(ExpeditionStrings), HeaderName = "RegisteredPassengers")]
        [Display(ResourceType = typeof(ExpeditionStrings), Name = "RegisteredPassengers")]
        public int AssignedPassangers { get; set; }


        public Schools School { get; set; }
        
        [ExportToExcel(ResourceType = typeof(SchoolsStrings), HeaderName = "Name")]
        public string SchoolName => this.School.Name;

        [ExportToExcel(ResourceType = typeof(SchoolsStrings), HeaderName = "InstituteID")]
        public int SchoolInstitute => this.School.InstituteID;

        [Display(ResourceType = typeof(ExpeditionStrings), Name = "Comments")]
        public string Comments { get; set; }

        public List<LinkedFlights> LinkedFlights { get; set; }


        public ExpeditionStatus()
        {

            TripDetails = new TripViewModelBasic();
            LinkedFlights = new List<LinkedFlights>();
            School = new Schools();
        }
    }

    public class LinkedFlights
    {
        //Properties

        [Display(ResourceType = typeof(FlightsStrings), Name = "ID")]
        public int ID { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [Display(ResourceType = typeof(FlightsStrings), Name = "DepartureDate")]
        public DateTime? DepartureDate { get; set; }

        [Display(ResourceType = typeof(FlightsStrings), Name = "DepartureField")]
        public FlightViewModel.FlightsFields DepartureField { get; set; }

        public bool TakeOffFromIsrael { get; set; }

        [Display(ResourceType = typeof(FlightsStrings), Name = "DestenationField")]
        public FlightViewModel.FlightsFields DestenationField { get; set; }

        [Display(ResourceType = typeof(FlightsStrings), Name = "AssignPassengers")]
        public int AssignPassengers { get; set; }

        [Display(ResourceType = typeof(FlightsStrings), Name = "AirlineName")]
        public string AirlineName { get; set; }

        [Display(ResourceType = typeof(FlightsStrings), Name = "FlightNumber")]
        public string FlightNumber { get; set; }

        [Display(ResourceType = typeof(FlightsStrings), Name = "Comments")]
        public string Comments { get; set; }
    }

    public class UpdateAgentStatusViewModel
    {
        public int ExpeditionID { get; set; }

        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(ExpeditionStrings), Name = "AgentStatus")]
        public eAgentExpeditionStatus AgentStatusEnum { get; set; }

        public DAL.Expeditions Expedition { get; set; }

    }
}