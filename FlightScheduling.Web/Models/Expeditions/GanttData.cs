﻿using System;
using System.Collections.Generic;

namespace FlightScheduling.Web.Models.Expeditions
{
    public class GanttData
    {
        public int id { get; set; }
        public string title { get; set; }
        public string tooltip { get; set; } = string.Empty;
        public string start { get; set; }
        public string end { get; set; }
        public bool editable { get; set; }
        public int numberOfBuses { get; set; }

        public string color { get; set; } = "#E3178E";

        public IEnumerable<int> resource { get; set; }
        public GanttData()
        {
            
        }

        public GanttData(
            int id, 
            string title, 
            string tooltip, 
            DateTime start, 
            DateTime end,
            List<int> resource,
            int numberOfBuses,
            string color = "#E3178E")
        {
            this.id = id;
            this.title = title;
            this.tooltip = tooltip;
            this.start = start.ToString("yyyy-MM-ddTHH:mm:ss");
            this.end = end.ToString("yyyy-MM-ddTHH:mm:ss");
            this.editable = false;
            this.resource = resource;
            this.numberOfBuses = numberOfBuses;
            this.color = color;
        }
    }
}