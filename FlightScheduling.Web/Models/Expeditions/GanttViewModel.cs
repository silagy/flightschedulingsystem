﻿using System.Collections.Generic;

namespace FlightScheduling.Web.Models.Expeditions
{
    public class GanttViewModel
    {
        public IEnumerable<GanttData> Date { get; set; }
    }
}