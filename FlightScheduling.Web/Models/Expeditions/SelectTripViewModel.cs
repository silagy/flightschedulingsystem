﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FlightScheduling.DAL;
using FlightScheduling.Language;
using FlightScheduling.Web.Areas.Administration.Models.Flights;
using FlightScheduling.Web.Areas.Administration.Models.Trips;
using FlightScheduling.Web.Infrastructure;
using Endilo.Helpers;
using FlightScheduling.Website.Infrastructure;
using System.Globalization;
using FlightScheduling.DAL.CustomEntities;

namespace FlightScheduling.Web.Models.Expeditions
{

    public class TripAvilabilityViewModel
    {
        public List<TripsAvilibility> TripsAvilability { get; set; }
        public List<DateTime> Months { get; set; }

        public TripAvilabilityViewModel()
        {
            TripsAvilability = new List<TripsAvilibility>();
        }
    }

    public class TotalPassengersPerTrip
    {
        public int ID { get; set; }
        public int TotalPassensgers { get; set; }
    }

    public class TripBusesPerDay
    {
        public int TripID { get; set; }
        public DateTime DepartureDate { get; set; }
        public DateTime ReturningDate { get; set; }
        public int BasesPerDay { get; set; }
    }

    public class TripBookedPassengers
    {
        public int TripID { get; set; }
        public int BookedPassengers { get; set; }
    }

    public class TripsAvilibility
    {
        public int TripID { get; set; }
        public DateTime DepartureDate { get; set; }
        public DateTime CreationDate { get; set; }
        public int DestinationID { get; set; }
        public eFlightsFields DestinationEnum => (eFlightsFields)DestinationID;
        public DateTime ReturningDate { get; set; }
        public int ReturingField { get; set; }
        public eFlightsFields ReturingFieldEnum => ((eFlightsFields)ReturingField);
        public int TripDuration => Convert.ToInt32((ReturningDate - DepartureDate).TotalDays + 1);
        public int TotalPassengers { get; set; }
        public int BookedPassengers { get; set; }
        public int AvilablePassengers => (TotalPassengers - BookedPassengers) < 0 ? 0 : (TotalPassengers - BookedPassengers);
        public int NumberOfBuses { get; set; }
        public int BasesPerDay { get; set; }
        public int AvilableBuses => (AppConfig.MaxBusesPerDay - NumberOfBuses) < 0 ? 0 : (AppConfig.MaxBusesPerDay - NumberOfBuses);
        public DateTime? ArrivalTime { get; set; }

        public bool LateTrip
        {
            get
            {
                // I revoked the late flight due to request from the customer
                // TODO
                return false;
                if (ArrivalTime == null) return false;

                TimeSpan start = new TimeSpan(01, 0, 0); //10 o'clock
                TimeSpan end = new TimeSpan(17, 0, 0); //12 o'clock
                TimeSpan now = ((DateTime)ArrivalTime).TimeOfDay;

                // Early flight
                if ((now >= start) && (now < end))
                {
                    return false;
                }

                return true;
            }
        }

        public int TripAvilability
        {
            get
            {
                if (AvilableBuses == 0 || AvilablePassengers <= 0)
                {
                    return 0;

                }
                else if (AvilableBuses < SystemConfigurationRepository.TripAvilabilityBusesTreshold || AvilablePassengers < SystemConfigurationRepository.TripAvilabilityPassengerTreshold)
                {
                    return 1;
                }

                return 2;
            }
        }
    }

    public class SelectTripsViewModel
    {
        //Properties
        public List<SelectTripsViewModel> SelectTripsViewModels { get; set; }
        public FullCalendar FullCalendar { get; set; }
        public string eventsJSON { get; set; }

        public SelectTripsViewModel()
        {
            SelectTripsViewModels = new List<SelectTripsViewModel>();
            FullCalendar = new FullCalendar();
        }
    }

    public class SelectTripViewModel
    {
        //Properties

        [Display(ResourceType = typeof(TripsStrings), Name = "ID")]
        public int TripID { get; set; }


        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(TripsStrings), Name = "DepartureDate")]
        public DateTime DepartureDate { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(TripsStrings), Name = "ReturnDate")]
        public DateTime ReturnDate { get; set; }

        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(TripsStrings), Name = "Destenation")]
        public FlightViewModel.FlightsFields Destenation { get; set; }

        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(TripsStrings), Name = "TakeOffField")]
        public FlightViewModel.FlightsFields ReturnedFrom { get; set; }

        [Display(ResourceType = typeof(ExpeditionStrings), Name = "PotentialSeats")]
        public int PotentialSeats { get; set; }

        public List<BusPerDay> listOfBusesPerDay { get; set; }

        public SelectTripViewModel()
        {
            listOfBusesPerDay = new List<BusPerDay>();
        }
    }

    public class BusPerDay
    {
        //Properties
        public DateTime Day { get; set; }
        public int NumOdBuses { get; set; }
        public int AvailableBuses { get; set; }
    }

    public class ExpeditionViewModel
    {
        //Properties
        [Display(ResourceType = typeof(ExpeditionStrings), Name = "ID")]
        public int ID { get; set; }

        public TripViewModelBasic TripDetails { get; set; }

        [Display(ResourceType = typeof(ExpeditionStrings), Name = "PotentialPassengers")]
        public int PotentialPassengers { get; set; }

        [Display(ResourceType = typeof(ExpeditionStrings), Name = "TotalPassengers")]
        public int TotalPassengers { get; set; }

        [Display(ResourceType = typeof(ExpeditionStrings), Name = "NumOfPassangersRequired")]
        [ExpeditionPassangersValidation("BusesRequiredPerDay", ErrorMessageResourceType = typeof(ExpeditionStrings), ErrorMessageResourceName = "NumberOfBasesValidationError")]
        public int NumOfPassangersRequired { get; set; }

        public int SchoolID { get; set; }

        [Display(ResourceType = typeof(ExpeditionStrings), Name = "School")]
        //public List<Schools> Schools { get; set; }
        public List<SchoolSelectList> Schools { get; set; }


        [Display(ResourceType = typeof(ExpeditionStrings), Name = "BusPerDays")]
        public List<BusPerDay> BusPerDays { get; set; }

        public int MinimumBusesForTrip { get; set; }

        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(ExpeditionStrings), Name = "BusesRequiredPerDay")]
        public int BusesRequiredPerDay { get; set; }

        [Display(ResourceType = typeof(ExpeditionStrings), Name = "ExpeditionManager")]
        public string ExpeditionManager { get; set; }

        [Display(ResourceType = typeof(ExpeditionStrings), Name = "ExpeditionManagerPhone")]
        public string ExpeditionManagerPhone { get; set; }

        [Display(ResourceType = typeof(ExpeditionStrings), Name = "ExpeditionManagerCell")]
        public string ExpeditionManagerCell { get; set; }

        [Display(ResourceType = typeof(ExpeditionStrings), Name = "ExpeditionManagerEmail")]
        public string ExpeditionManagerEmail { get; set; }

        [Display(ResourceType = typeof(ExpeditionStrings), Name = "Comments")]
        public string Comments { get; set; }

        public int AgencyID { get; set; }

        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(ExpeditionStrings), Name = "Agency")]
        public List<EntitySelectList> AgenciesList { get; set; }


        public bool DisableSchoolSelection { get; set; }
        public bool DisableOrder { get; set; }


        public ExpeditionViewModel()
        {

            TripDetails = new TripViewModelBasic();
            Schools = new List<SchoolSelectList>();
            BusPerDays = new List<BusPerDay>();
            AgenciesList = new List<EntitySelectList>();
            DisableSchoolSelection = false;
            DisableOrder = false;
        }
    }

    public class SchoolSelectList
    {
        public int ID { get; set; }
        public string Name { get; set; }
    }

    public class EntitySelectList
    {
        public int ID { get; set; }
        public string Name { get; set; }
    }

    public class EntitySelectGroupList : EntitySelectList
    {
        public string Group { get; set; }
    }

    public class TripWeekSelectList
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Group { get; set; }
    }


    public class SchoolExpeditionsViewModel
    {
        public int OrderID { get; set; }
        public int TripID { get; set; }
        public int SchoolID { get; set; }
        public DateTime DepartureDate { get; set; }
        public DateTime MinDateLimited => DepartureDate.AddDays(-4);
        public DateTime MaxDateLimited => DepartureDate.AddDays(4);


    }

    public class QuickExpeditionViewModel
    {
        //Properties
        [Display(ResourceType = typeof(ExpeditionStrings), Name = "ID")]
        public int ID { get; set; }

        public int SchoolID { get; set; }

        public int? GroupID { get; set; }

        [Display(ResourceType = typeof(ExpeditionStrings), Name = "School")]
        public List<SchoolSelectList> Schools { get; set; }

        public int TripId { get; set; }

        [Display(ResourceType = typeof(TripsStrings), Name = "SelectTrip")]
        public List<EntitySelectList> Trips { get; set; }

        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(ExpeditionStrings), Name = "BusesRequiredPerDay")]
        public int BusesRequiredPerDay { get; set; }

        [Display(ResourceType = typeof(ExpeditionStrings), Name = "NumOfPassangersRequired")]
        public int NumOfPassangersRequired { get; set; }

        [Display(ResourceType = typeof(ExpeditionStrings), Name = "NumOfPassangersRequired")]
        public List<TripWeekSelectList> SelectedWeek { get; set; }

        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        public int AgencyID { get; set; }

        [Display(ResourceType = typeof(ExpeditionStrings), Name = "Agency")]
        public List<EntitySelectList> AgenciesList { get; set; }

        public TransactionResult TransactionResult { get; set; }

        public QuickExpeditionViewModel()
        {
            
            Trips = new List<EntitySelectList>();
            BuildWeeks();
            
        }

        public void BuildWeeks()
        {
            DateTime startDate = DateTime.Now;
            DateTime endDate = DateTime.Now.AddYears(2);
            SelectedWeek = new List<TripWeekSelectList>();
            while (startDate <= endDate)
            {
                DateTime firstDayOfWeek = startDate.StartOfWeek(DayOfWeek.Sunday);
                this.SelectedWeek.Add(new TripWeekSelectList()
                {
                    Id = firstDayOfWeek.ToString(CultureInfo.CurrentCulture.DateTimeFormat),
                    Name = $"{CommonStrings.Week} {Endilo.Helpers.Helpers.GetWeekNumberOfMonth(firstDayOfWeek)} | {firstDayOfWeek.ToString("dd")} - {firstDayOfWeek.AddDays(7).ToString("dd")}",
                    Group = $"{CommonStrings.Month} {firstDayOfWeek.ToString("MMMM")} - {firstDayOfWeek.Year}"
                });
                startDate = firstDayOfWeek.AddDays(7);
            }
        }

    }

}