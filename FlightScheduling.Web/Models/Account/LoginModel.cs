﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using FlightScheduling.Language;
using FlightScheduling.Web.Areas.Administration.Models.Messages;
using Otakim.Language;

namespace Otakim.Website.Models
{
	public class LoginModel
	{
		[Required(ErrorMessageResourceType = typeof(AccountStrings), ErrorMessageResourceName = "EmailAddress")]
		[Display(ResourceType = typeof(AccountStrings), Name = "EmailAddress")]
		public string UserName { get; set; }

		[Required(ErrorMessageResourceType = typeof(AccountStrings), ErrorMessageResourceName = "PasswordRequiredValidation")]
		[DataType(DataType.Password)]
		[Display(ResourceType = typeof(AccountStrings), Name = "Password")]
		public string Password { get; set; }

		[Display(ResourceType = typeof(AccountStrings), Name = "RememberMe")]
		public bool RememberMe { get; set; }

		public string ReturnURL { get; set; }

        //Messages
	    public IEnumerable<MessagesDetailsViewModel> Messages { get; set; }
	}
}
