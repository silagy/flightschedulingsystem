﻿using FlightScheduling.Language;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FlightScheduling.Web.Models.Account
{
    public class ResetPasswordViewModel
    {
        [Required(ErrorMessageResourceType = typeof(AccountStrings), ErrorMessageResourceName = "PasswordRequiredValidation")]
        [DataType(DataType.Password)]
        [Display(ResourceType = typeof(AccountStrings), Name = "NewPassword")]
        public string Password { get; set; }

        [Required(ErrorMessageResourceType = typeof(AccountStrings), ErrorMessageResourceName = "PasswordRequiredValidation")]
        [DataType(DataType.Password)]
        [Compare("Password", ErrorMessageResourceType = typeof(AccountStrings), ErrorMessageResourceName = "ConfirmPasswordValidation")]
        [Display(ResourceType = typeof(AccountStrings), Name = "ConfirmPassword")]
        public string ConfirmPassword { get; set; }

        public int UserID { get; set; }

        // Internal properties
        public bool TokenExpired { get; set; }

        public ResetPasswordViewModel()
        {
            TokenExpired = false;
        }
    }
}