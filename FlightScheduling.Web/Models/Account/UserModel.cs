﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Security.Principal;
using System.Web;
using Flight.DAL.FlightDB.Methods;
using FlightScheduling.DAL;
using FlightScheduling.DAL.CustomEntities.Permissions;
using FlightScheduling.Language;
using FlightScheduling.Web.Areas.Administration.Models;
using FlightScheduling.Web.Areas.Administration.Models.Common;
using FlightScheduling.Web.Areas.Administration.Models.Schools;
using Otakim.Language;

namespace FlightScheduling.Website.Models.Account
{
    public class UserModel
    {
        [Display(ResourceType = typeof(AccountStrings), Name = "UserID")]
        public int UserID { get; set; }

        [DataType(DataType.Password)]
        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(AccountStrings), Name = "Password")]
        public string Password { get; set; }

        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(AccountStrings), Name = "FirstName")]
        public string FirstName { get; set; }

        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(AccountStrings), Name = "LastName")]
        public string LastName { get; set; }

        [DataType(DataType.EmailAddress, ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Email")]
        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(AccountStrings), Name = "EmailAddress")]
        public string Email { get; set; }

        [DataType(DataType.PhoneNumber)]
        [Display(ResourceType = typeof(AccountStrings), Name = "Phone")]
        public string Phone { get; set; }

        public int? UserRole { get; set; }

        [Display(ResourceType = typeof(SchoolsStrings), Name = "IsActive")]
        public bool IsActivate { get; set; }

        public bool Selected { get; set; }

        [Display(ResourceType = typeof(AccountStrings), Name = "Role")]
        public eRole? Role
        {
            get
            {
                if (UserRole != null)
                {
                    return (eRole)UserRole;
                }

                return null;
            }
            set
            {
                UserRole = (int)value;
            }
        }

        [Display(ResourceType = typeof(AccountStrings), Name = "Active")]
        public Nullable<bool> Active { get; set; }



        [Display(ResourceType = typeof(AccountStrings), Name = "CreationDate")]
        public System.DateTime CreationDate { get; set; }


        public int? PermissionRoleID { get; set; }
        public string PermissionRoleName { get; set; }

        [Display(ResourceType = typeof(PermissionStrings), Name = "RoleName")]
        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        public CustomSelectList PermissionRoles { get; set; }


        public UserModel()
        {
            PermissionRoles = new CustomSelectList();
        }

        public void LoadPermissionRoles()
        {
            using (DBRepository _dbRepo = new DBRepository())
            {
                PermissionRoles.Items = _dbRepo.GetRecords<CustomSelectListItem>(QueriesRepository.GetPermissionRoles, null).ToList();
            }
        }


        public override string ToString()
        {
            return String.Format("{0} {1}", this.FirstName, this.LastName);
        }
    }



    [Serializable]
    public class UserSessionModel
    {
        [Display(ResourceType = typeof(AccountStrings), Name = "UserID")]
        public int UserID { get; set; }

        [Display(ResourceType = typeof(AccountStrings), Name = "FirstName")]
        public string FirstName { get; set; }

        [Display(ResourceType = typeof(AccountStrings), Name = "LastName")]
        public string LastName { get; set; }

        [Display(ResourceType = typeof(AccountStrings), Name = "FullName")]
        public string FullName { get; set; }

        [Display(ResourceType = typeof(AccountStrings), Name = "EmailAddress")]
        public string Email { get; set; }

        //Profile Picture
        public string ProfilePicture { get; set; }

        public string RoleName { get; set; }

        public int RoleID { get; set; }

        public int? RegionID { get; set; }

        public int? RoleType { get; set; }

        public eRoleType? RoleTypeEnum
        {
            get
            {
                if (RoleType != null)
                {
                    return (eRoleType)RoleType;
                }

                return null;
            }
        }

        public int? PermissionRoleID { get; set; }

        public int? InstructorID { get; set; }

        public bool IsGroupManager => this.InstructorID != null;

        public int? AgencyID { get; set; }
    }

    [Serializable]
    public class UserMixPanelModel
    {
        [Display(ResourceType = typeof(AccountStrings), Name = "UserID")]
        public int UserID { get; set; }

        [Display(ResourceType = typeof(AccountStrings), Name = "FirstName")]
        public string FirstName { get; set; }

        [Display(ResourceType = typeof(AccountStrings), Name = "LastName")]
        public string LastName { get; set; }

        [Display(ResourceType = typeof(AccountStrings), Name = "FullName")]
        public string FullName => $"{FirstName} {LastName}";


        public string RoleName { get; set; }

        public int RoleID { get; set; }

        public int? RegionID { get; set; }

        public int? RoleType { get; set; }

        public eRoleType? RoleTypeEnum
        {
            get
            {
                if (RoleType != null)
                {
                    return (eRoleType)RoleType;
                }

                return null;
            }
        }

        public int? PermissionRoleID { get; set; }

        public int? InstructorID { get; set; }

        public bool IsGroupManager => this.InstructorID != null;
    }

    public class SchoolUserViewModel
    {
        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(AccountStrings), Name = "InstituteID")]
        public int InstituteID { get; set; }

        public SchoolViewModel School { get; set; }


        public SchoolUserViewModel()
        {
            School = new SchoolViewModel();
        }
    }

    public class MyPrincipal : IPrincipal
    {
        public MyPrincipal(IIdentity identity)
        {
            Identity = identity;
        }

        public IIdentity Identity
        {
            get;
            private set;
        }

        public UserSessionModel User { get; set; }

        public bool IsInRole(string role)
        {
            return true;
        }
    }
}