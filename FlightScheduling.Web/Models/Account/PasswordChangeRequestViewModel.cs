﻿using FlightScheduling.Language;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FlightScheduling.Web.Models.Account
{
    public class PasswordChangeRequestViewModel
    {
        public string ID { get; set; }
        public string HashedID { get; set; }
        public DateTime RequestDate { get; set; }
        public int UserID { get; set; }

        // UI Properties
        [Required(ErrorMessageResourceType = typeof(AccountStrings), ErrorMessageResourceName = "EmailAddress")]
        [Display(ResourceType = typeof(AccountStrings), Name = "EmailAddress")]
        public string UserName { get; set; }



    }
}