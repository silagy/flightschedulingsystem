﻿using FlightScheduling.Web.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FlightScheduling.Web.Models
{
    public class ReportViewerViewModel
    {
        // Properties
        public string ReportUrl { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }

        public Dictionary<string,string> ReportParameters { get; set; }

        public ReportViewerViewModel()
        {
            Username = AppConfig.ReportViewerUsername;
            Password = AppConfig.ReportViewerPassword;
            ReportParameters = new Dictionary<string, string>();
        }

    }
}