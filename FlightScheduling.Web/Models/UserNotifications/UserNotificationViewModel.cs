﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Endilo.Helpers;
using FlightScheduling.DAL.CustomEntities.Notifications;

namespace FlightScheduling.Web.Models.UserNotifications
{
    public class UserNotificationViewModel
    {
        //Properties

        public int ID { get; set; }

        public int NotificationTempateID { get; set; }
        public string MessageSubject { get; set; }
        public string MessageBody { get; set; }
        public string MessageBodyNotHtml => Endilo.Helpers.Helpers.RemoveUnwantedTags(MessageBody);
        public DateTime SendDate { get; set; }
        public int NotificationStatus { get; set; }
        public eNotificationStatus NotificationStatusEnum => (eNotificationStatus)NotificationStatus;
        public int TriggerType { get; set; }
        public eNotificationTriggerType TriggerTypeEnum => (eNotificationTriggerType)TriggerType;
    }
}