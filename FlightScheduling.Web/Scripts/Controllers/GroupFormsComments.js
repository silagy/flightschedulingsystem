﻿$(function () {

    $("#send-comment").click(function (e) {

        var comment = $("#form-comment");
        var FormID = comment.data("formid");
        var FormChapterType = comment.data("formchaptertype");
        var CommentText = comment.val();


        $.ajax({
            url: "/Administration/GroupForms/AddCommentToForm",
            type: "POST",
            data: "FormID=" + FormID + "&FormChapterType=" + FormChapterType + "&Comment=" + CommentText,
            success: function (result) {
                $("#form-comments").replaceWith(result);
                comment.val("");
            }
        });


    });

    $("#form-comment").on("keyup", function () {

        if ($("#form-comment").val().length < 1) {
            $("#send-comment").attr("disabled", "disabled");
        } else {
            $("#send-comment").removeAttr("disabled");
        }
    });

});