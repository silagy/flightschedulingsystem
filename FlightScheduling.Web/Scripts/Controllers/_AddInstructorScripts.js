﻿$(function () {

    $("#expedition-instructor").autocomplete({
        source: function (request, response) {

            $.ajax({
                url: "/GroupForms/AddInstructor",
                type: "GET",
                data: "SearchTerm=" + $("#expedition-instructor").val(),
                success: function (result) {
                    var json = jQuery.parseJSON(result);
                    response(json);
                }
            });


        },
        minLength: 3,
        open: function () { },
        close: function () { },
        focus: function (event, ui) { },
        select: function (event, ui) {
            $("#expedition-manager").val(ui.item.ID);
            $("#polinWebsite").addClass("hidden");
            $.ajax({
                url: "GroupForms/CheckInstructorRegistration",
                type: "GET",
                data: "InstructorID=" + ui.item.ID + "&GroupID=" + $('#GroupID').val() + "&InternalID=" + ui.item.InternalID,
                success: function (result) {
                    var json = jQuery.parseJSON(result);
                    console.log(json);
                    if (json.Success == false && json.AllowOverRide == false) {
                        $("#expedition-manager-expired-validation").removeClass("hidden");
                        $("#expedition-manager-expired-validation-text").removeClass("hidden");
                        $("#expedition-manager-expired-validation-text").text(json.ErrorReason);
                    } else if (json.Success == false || json.AllowOverRide == true) {

                        $("#expedition-manager-expired-validation").removeClass("hidden");
                        $("#expedition-manager-expired-validation-text").removeClass("hidden");
                        $("#polinWebsite").removeClass("hidden");
                        $("#expedition-manager-expired-validation-text").text(json.ErrorReason);

                        //Add the manager to the DB
                        AddManagertoDB(json);
                    } else {
                        $("#expedition-manager-expired-validation").addClass("hidden");
                        $("#expedition-manager-expired-validation-text").addClass("hidden");
                        $("#polinWebsite").addClass("hidden");

                        //Add the manager to the DB
                        AddManagertoDB(json);
                    }
                }
            });



            return false;
        }
    }).autocomplete("instance")._renderItem = function (ul, item) {
        return $("<li>")
            .append("<div><div class='ac_instructor_picture'><img src='@Url.Content("~/content/")" + item.InstructorPictureUrl + "' alt='InstructorProfile' /></div><div class='ac_instructor_row'><div class='ac_instrctor_name'>" + item.FirstName + ", " + item.LastName + "</div><div class='ac_instructorID'>תז: " + item.ID + "</div></div></div>")
            .appendTo(ul);
    };
});