﻿
$(function () {



    $('button[mp-event]').on('click', function () {
        // Getting the elemnet
        var elm = $(this);
        this.setEvent(elm);
        
    });

    $('a[mp-event]').on('click', function () {
        // Getting the elemnet
        var elm = $(this);
        setEvent(elm);

    });

    $('a[mp-navigation-event]').on('click', function () {
        // Getting the elemnet
        var elm = $(this);
        setNavigationEvent(elm);

    });

    $('div[mp-event]').on('click', function () {
        // Getting the elemnet
        var elm = $(this);
        setEvent(elm);

    });

    function setEvent(elm) {
        // Getting the mixpanel event name
        var mixpanelEvent = elm.attr('mp-event');
        mixpanel.track(mixpanelEvent);
    }

    function setNavigationEvent(elm) {
        var mixpanelEvent = elm.attr('mp-navigation-event');
        mixpanel.track(
            "Navigation",
            { "target": mixpanelEvent }
        );
    }

    $(document).on('ready', function () {
        var elm = $('div[mp-init]');
        if (elm != null) {
            var user = elm.attr('mp-init');
            if (user != null) {
                // Set mixpanel identiry
                var userObject = jQuery.parseJSON(user);
                mixpanel.identify(userObject.UserID);
                mixpanel.register(userObject);
            }
        }
    });
});