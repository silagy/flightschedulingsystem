﻿$(function () {

    var options = {
        size: 'default'
        , checked: undefined
        , onText: 'Y'
        , offText: 'N'
        , onSwitchColor: '#64BD63'
        , offSwitchColor: '#fff'
        , onJackColor: '#fff'
        , offJackColor: '#fff'
        , showText: false
        , disabled: false
        , onInit: function () { }
        , beforeChange: function () { }
        , onChange: function () { }
        , beforeRemove: function () { }
        , onRemove: function () { }
        , beforeDestroy: function () { }
        , onDestroy: function () { }
    };

    var el = $('.switch-checkbox');
    var myswitch = new Switch(el, options);
});