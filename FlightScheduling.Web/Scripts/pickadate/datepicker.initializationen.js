﻿$(function() {
    $('.datepicker').pickadate({
        selectMonths: true,
        selectYears: 7,
        today: 'Today',
        clear: 'Clear',
        close: 'Close',
        monthsFull: ['ינואר', 'פברואר', 'מרץ', 'אפריל', 'מאי', 'יוני', 'יולי', 'אוגוסט', 'ספטמבר', 'אוקטובר', 'נובמבר', 'דצמבר'],
        monthsShort: ['ינואר', 'פברואר', 'מרץ', 'אפריל', 'מאי', 'יוני', 'יולי', 'אוגוסט', 'ספטמבר', 'אוקטובר', 'נובמבר', 'דצמבר'],
        weekdaysFull: ['ראשון', 'שני', 'שלישי', 'רביעי', 'חמישי', 'שישי', 'שבת'],
        weekdaysShort: ['א', 'ב', 'ג', 'ד', 'ה', 'ו', 'ש'],
        weekdaysLetter: ['א', 'ב', 'ג', 'ד', 'ה', 'ו', 'ש'],
        labelMonthNext: 'לחודש הבא',
        labelMonthPrev: 'לחודש הקודם',
        labelMonthSelect: 'בחירת חודש',
        labelYearSelect: 'בחירת שנה',
        format: "dd/mm/yyyy",
        klass: { footer: "picker__footer hidden" },
    });
});