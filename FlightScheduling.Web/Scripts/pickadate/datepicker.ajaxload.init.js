﻿$(function () {
    $('.datepicker').pickadate({
        selectMonths: true,
        selectYears: 7,
        today: 'היום',
        clear: 'איפוס',
        close: 'סגירה',
        monthsFull: ['ינואר', 'פברואר', 'מרץ', 'אפריל', 'מאי', 'יוני', 'יולי', 'אוגוסט', 'ספטמבר', 'אוקטובר', 'נובמבר', 'דצמבר'],
        monthsShort: ['ינואר', 'פברואר', 'מרץ', 'אפריל', 'מאי', 'יוני', 'יולי', 'אוגוסט', 'ספטמבר', 'אוקטובר', 'נובמבר', 'דצמבר'],
        weekdaysFull: ['ראשון', 'שני', 'שלישי', 'רביעי', 'חמישי', 'שישי', 'שבת'],
        weekdaysShort: ['א', 'ב', 'ג', 'ד', 'ה', 'ו', 'ש'],
        weekdaysLetter: ['א', 'ב', 'ג', 'ד', 'ה', 'ו', 'ש'],
        labelMonthNext: 'לחודש הבא',
        labelMonthPrev: 'לחודש הקודם',
        labelMonthSelect: 'בחירת חודש',
        labelYearSelect: 'בחירת שנה',
        format: "dd/mm/yyyy",
        klass: { footer: "picker__footer hidden" },
        selectYears: 150,
        min: new Date(1940, 1, 1),
        max: new Date(2060, 1, 1),
        onRender: function () {
            var picker = this;
            console.log(picker);
            var pickerBox = picker.$holder.find('.picker__box');
            var html = "<div class='picker_day_container'><div class='picker_full_day'>" + picker.get('select', 'dddd') + "</div><div class='picker_month_display'>" + picker.get('select', 'mmmm') + "</div><div class='picker_day_display'>" + picker.get('select', 'd') + "</div><div class='picker_year_display'>" + picker.get('select', 'yyyy') + "</div></div>";
            pickerBox.prepend(html);
        }
    });

    $('.timepicker').pickatime({
        formatSubmit: 'HH:i',
        format: 'HH:i',
        formatLabel: '<b>HH</b>:i',
        hiddenName: true,
        interval: 5
    });
});