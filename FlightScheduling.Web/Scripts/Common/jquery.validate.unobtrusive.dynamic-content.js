﻿/// <reference path="jquery-1.4.4.js" />
/// <reference path="jquery.validate.js" />
/// <reference path="jquery.validate.unobtrusive.js" />

(function ($)
{
	$.validator.unobtrusive.parseDynamicContent = function (selector)
	{
		//get the relevant form
		var $forms = $(selector).find('form');
		$forms.each(function (index, obj)
		{
			var $form = $(obj);
			$form.removeData("validator") /* added by the raw jquery.validate plugin */
				.removeData("unobtrusiveValidation");  /* added by the jquery unobtrusive plugin */

			$.validator.unobtrusive.parse($form);

			$form.on("submit", function (event)
			{
				var $formToSubmit = $(this);
				if (!$formToSubmit.valid())
				{
					event.preventDefault();
					return false;
				}

			});
		});
		

	}
})($);