﻿$(function () {

    jQuery.validator.unobtrusive.adapters.add('israeliid', function (options) {
        options.rules['israeliid'] = {};
        options.messages['israeliid'] = options.message;
    });


    jQuery.validator.addMethod('israeliid', function (value, element, params) {
        if (value !== null) {
            // Just in case -> convert to string
            var IDnum = String(value);

            // Validate correct input
            if ((IDnum.length > 9) || (IDnum.length < 5))
                return false;
            if (isNaN(IDnum))
                return false;

            // The number is too short - add leading 0000
            if (IDnum.length < 9) {
                while (IDnum.length < 9) {
                    IDnum = '0' + IDnum;
                }
            }

            // CHECK THE ID NUMBER
            var mone = 0, incNum;
            for (var i = 0; i < 9; i++) {
                incNum = Number(IDnum.charAt(i));
                incNum *= (i % 2) + 1;
                if (incNum > 9)
                    incNum -= 9;
                mone += incNum;
            }


            if (mone % 10 === 0) {
                element.value = IDnum;
                return true;
            }
            else {
                return false;
            }
        }

        return false;
    }, '');
});