﻿$(function () {


    $('.validate-form').on("click", function (event) {
        
        var $form = $("#" + $(this).closest("form").attr('id'));
        $.validator.unobtrusive.parse($form);

        event.preventDefault();
        if ($form.valid()) {
            $form.submit();
        }
    });
});