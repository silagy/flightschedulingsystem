﻿function OpenDialogForNultipleActions(DialogContainerID) {
    var $DialogContainer = $('#' + DialogContainerID);
    var $jQval = $.validator; //This is the validator
    $jQval.unobtrusive.parse($DialogContainer); // and here is where you set it up.
    $DialogContainer.modal();

    var $form = $DialogContainer.find("form");
    $.validator.unobtrusive.parse($form);

    //$form.on("submit", function (event) {
    //    debugger;
    //    var $form = $(this);

    //    //Function is defined later...
    //    $DialogContainer.modal("hide");
    //    $form.submit();
    //    //submitAsyncForm($form,
    //    //    function (data) {
    //    //        $DialogContainer.modal("hide");
    //    //        window.location.href = window.location.href;

    //    //    },
    //    //    function (xhr, ajaxOptions, thrownError) {
    //    //        console.log(xhr.responseText);
    //    //        $("body").html(xhr.responseText);
    //    //    });
    //    event.preventDefault();
    //});
}


function OpenDialog(DialogContainerID) {
    var $DialogContainer = $('#' + DialogContainerID);
    var $jQval = $.validator; //This is the validator
    $jQval.unobtrusive.parse($DialogContainer); // and here is where you set it up.
    $DialogContainer.modal();

    var $form = $DialogContainer.find("form");
    $.validator.unobtrusive.parse($form);

    $form.on("submit", function (event) {
        var $form = $(this);

        //Function is defined later...
        submitAsyncForm($form,
            function (data) {
                $DialogContainer.modal("hide");
                window.location.href = window.location.href;

            },
            function (xhr, ajaxOptions, thrownError) {
                console.log(xhr.responseText);
                $("body").html(xhr.responseText);
            });
        event.preventDefault();
    });
}

function submit(DialogContainerID) {
    var $DialogContainer = $('#' + DialogContainerID);
    var $jQval = $.validator; //This is the validator
    $jQval.unobtrusive.parse($DialogContainer); // and here is where you set it up.
    $DialogContainer.modal();

    var $form = $DialogContainer.find("form");
    $.validator.unobtrusive.parse($form);

    $form.on("submit", function (event) {
        var $form = $(this);

        //Function is defined later...
        submitAsyncForm($form,
            function (data) {
                $DialogContainer.modal("hide");
                window.location.href = window.location.href;

            },
            function (xhr, ajaxOptions, thrownError) {
                console.log(xhr.responseText);
                $("body").html(xhr.responseText);
            });
        event.preventDefault();
    });
}

/*
String.Format function to be used like:
String.format('{0} is dead, but {1} is alive! {0} {2}', 'ASP', 'ASP.NET');
*/
if (!String.format) {
    String.format = function (format) {
        var args = Array.prototype.slice.call(arguments, 1);
        return format.replace(/{(\d+)}/g, function (match, number) {
            return typeof args[number] != 'undefined'
                ? args[number]
                : match
                ;
        });
    };
}

function popupwindow(url, title, w, h) {
    var left = (screen.width / 2) - (w / 2);
    var top = (screen.height / 2) - (h / 2);
    return window.open(url, title, 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);
}

function handleError(ajaxContext) {
    debugger;
    var response = ajaxContext.responseText;
    var statusText = response.statusText;
    var win = popupwindow("", "Server Error", screen.width * 0.8, screen.height * 0.7);
    win.document.body.innerHTML = response;
}

function GetUserNotifications() {

    $.ajax({
        url: "/Account/GetUserNotificationCount",
        type: "GET",
        success: function (result) {
            var json = jQuery.parseJSON(result);
            if (json.NotificationCount > 0) {
                $("#notification-number").text(json.NotificationCount);
                $("#notification-number").show();
            } else {
                $("#notification-number").text(0);
                $("#notification-number").hide();
            }
        }
    });
}





$(function () {

    GetUserNotifications();
    var interval = 1000 * 60 * 5; // where X is your every X minutes
    setInterval(GetUserNotifications, interval);

    //Get User Notifications
    $("#notification-bell").click(function () {
        $(".notification-popup").show();
        event.stopPropagation();

        // Load notifications

        var includeReadNotifications = false;

        $.ajax({
            url: "/Account/GetUserNotification",
            type: "GET",
            data: "includeReadNotifications=" + includeReadNotifications,
            success: function (result) {
                $("#user-notifications").replaceWith(result);
                $("#user-notifications").show();
            },
            beforeSend: function () {
                $("#notification-loading").show();
            },
            complete: function () {
                $("#notification-loading").hide();
            }
        });
    });


   



    $(window).click(function () {
        //Hide the menus if visible
        if ($(".notification-popup").is(":visible")) {
            $(".notification-popup").hide();
        }
    });

    $(".notification-popup").click(function (event) {
        event.stopPropagation();
    });

    $(function () {
        $('[data-toggle="tooltip"]').tooltip();
    });


    $('.form-validation').on("submit", function (event) {
        var $form = $(this);
        $.validator.unobtrusive.parse($form);

        ////Function is defined later...
        //submitAsyncForm($form,
        //function (data) {
        //    $DialogContainer.modal("hide");
        //    window.location.href = window.location.href;

        //},
        //function (xhr, ajaxOptions, thrownError) {
        //    console.log(xhr.responseText);
        //    $("body").html(xhr.responseText);
        //});

        event.preventDefault();
        console.log("In");
    });

    $('.form-validation').on("load", function (event) {
        console.log("In");
    });


    $('div[data-toggle="collapse-target"]').click(function () {

        var targetElement = $('' + $(this).attr('data-target') + '');

        targetElement.slideToggle();


    });

    setValidators();


    // MULTI ACTIONS SCRIPTS
    $(".multi-action").click(function (e) {
        e.preventDefault();
        if ($('.tcheck').is(':checked')) {
            var selectedUsers = $('.tcheck:checkbox:checked').map(function () {
                return this.id;
            }).toArray();
            debugger;
            var aLink = $(this).children('a');
            var action = aLink.attr('data-action');
            var returnID = aLink.attr('data-return-param');
            var container = aLink.attr('data-container');
            var data;
            // Create the data property
            if (typeof returnID !== 'undefined') {
                data = {
                    ReturnID: returnID,
                    Items: selectedUsers
                };
            } else {
                data = {
                    Items: selectedUsers
                };
            }
            data.Items = selectedUsers;

            $.ajax({
                url: action,
                type: "POST",
                data: data,
                success: function (result) {
                    $("#" + container).empty();
                    $("#" + container).append(result);
                    OpenDialogForNultipleActions(container);
                }
            });
        }
    });


    $('#chkHeader').change(function () {
        if ($(this).is(':checked')) {
            disableMultiActions(false);
            $('.tcheck').prop('checked', true);
        } else {
            disableMultiActions(true);
            $('.tcheck').prop('checked', false);
        }
    });

    $('.tcheck').change(function () {
        if ($(this).is(':checked')) {
            disableMultiActions(false);
        } else {
            disableMultiActions(true);
        }

    });

    function disableMultiActions(disabled) {
        if (disabled) {
            $('.multi-action').addClass('disabled');
        } else {
            $('.multi-action').removeClass('disabled');
        }
    }
    //END MULTI ACTIONS SCRIPTS
});

function setValidators() {
    Globalize.culture('he-IL');
    $.validator.methods.date = function (value, element) {
        if (Globalize.parseDate(value)) {
            return true;
        }
        return false;
    }

    //Fix the range to use globalized methods
    jQuery.extend(jQuery.validator.methods, {
        range: function (value, element, param) {
            //Use the Globalization plugin to parse the value
            var val = Globalize.parseFloat(value);
            return this.optional(element) || (val >= param[0] && val <= param[1]);
        }
    });

}
