﻿; (function ($) {
    $('.chosen-select').chosen({
        width: "100%",
        no_results_text: "לא נמצאו תוצאות",
        placeholder_text_multiple: "בחר אופציה"
    });
})(jQuery);