﻿; (function ($) {
    $('.chosen-select').chosen({
        width: "100%",
        no_results_text: "לא נמצאו תוצאות",
        //disable_search_threshold: 10,
        placeholder_text_multiple: "בחר אופציה"
    });


})(jQuery);