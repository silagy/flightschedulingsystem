﻿$(function () {
    flatpickr.localize(flatpickr.l10ns.he);
    $('.datepicker').flatpickr({
        dateFormat: 'd/m/Y',
        allowInput: false,
        altInput: true,
        altFormat: "d/m/Y",
        local: 'he',
        onReady: function (selectedDate, dateStr, instance) {
            console.log(instance);
            console.log(selectedDate);
            
            if ($('.datepicker').val().length > 0) {
                self.setDate('defatDate', new Date($('.datepicker').val()));
                //instance.formatDate(new Date($('.datepicker').val()),'d/m/Y');
            } else {
                self.setDate('defatDate', new Date());

                //instance.formatDate(new Date(), 'd/m/Y');
            }
        }
    });

    $('.timepicker').flatpickr({
        allowInput: false,
        enableTime: true,
        noCalendar: true,
        time_24hr: true
    });
});