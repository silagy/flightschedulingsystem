﻿// Initialize your tinyMCE Editor with your preferred options
tinyMCE.init({
    // General options
    selector: "textarea.tinymce",
    theme: "silver",
    height: 300,
    menubar: 'edit insert view format table tools',
    plugins: 'autolink directionality visualchars fullscreen link codesample table anchor advlist lists code template',
    toolbar1: 'formatselect | bold italic strikethrough forecolor backcolor | link | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | removeformat | template code',
    image_advtab: false,
    templates: [
        //{ title: 'Test template 1', content: 'Test 1' },
        //{ title: 'Test template 2', content: 'Test 2' }
    ],
    content_css: [
        '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
        '//www.tinymce.com/css/codepen.min.css'
    ],
    // Example content CSS (should be your site CSS)
    content_css: "../../Content/css/home.css",
});