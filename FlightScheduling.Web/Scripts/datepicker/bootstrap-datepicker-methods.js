﻿$(function() {
    $('.datepicker').datepicker({
        format: "dd/mm/yyyy",
        todayBtn: "linked",
        language: "he",
        orientation: "top left",
        autoclose: true,
        todayHighlight: true,
        clearBtn: true,
        startdate: '01/01/2000',
        enddate: new Date().setYear(new Date().getYear() + 6)
    });
});
