﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Principal;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Script.Serialization;
using System.Web.Security;
using Flight.Membership;
using FlightScheduling.DAL;
using FlightScheduling.Web.App_Start;
using FlightScheduling.Web.Infrastructure;
using FlightScheduling.Website.Infrastructure;
using FlightScheduling.Website.Models.Account;
using Logzio.DotNet.NLog;
using NLog;
using NLog.Config;
using StackExchange.Profiling;

namespace FlightScheduling.Web
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            ModelBinders.Binders.Add(typeof(DateTime), new DateBinder());
            AutoMapperConfig.RegisterMappings();
            SystemConfigurationConfig.SetSystemConfigurationSettings();
            PermissionLoader.LoadRolesAndPermissions();
            BundleTable.EnableOptimizations = false;

            ClientDataTypeModelValidatorProvider.ResourceClassKey = "ValidationStrings";
            DefaultModelBinder.ResourceClassKey = "ValidationStrings";
        }

        protected void Application_PreRequestHandlerExecute()
        {
            //It's important to check whether session object is ready
            if (HttpContext.Current.Session != null)
            {
                CultureInfo ci = (CultureInfo)this.Session["Culture"];
                //Checking first if there is no value in session 
                //and set default language 
                //this can happen for first user's request
                if (ci == null)
                {
                    //Sets default culture to english invariant
                    string langName = "he-IL";

                    //Try to get values from Accept lang HTTP header
                    //if (HttpContext.Current.Request.UserLanguages != null && HttpContext.Current.Request.UserLanguages.Length != 0)
                    //{
                    //Gets accepted list 
                    //	langName = HttpContext.Current.Request.UserLanguages[0].Substring(0, 2);
                    //}
                    ci = new CultureInfo(langName);
                    ci.DateTimeFormat.ShortDatePattern = "dd/MM/yyyy";
                    ci.DateTimeFormat.LongTimePattern = "dd/MM/yyyy hh:mm";
                    this.Session["Culture"] = ci;
                }
                //Finally setting culture for each request
                Thread.CurrentThread.CurrentUICulture = ci;
                Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(ci.Name);
            }

            if (User.Identity.IsAuthenticated && HttpContext.Current.Session != null && HttpContext.Current.Session["UserProfile"] == null)
            {
                //Set User Sesision
                FlightMembershipProvider provider = new FlightMembershipProvider();
                Users authentictedUser = provider.GetUserByEmailOrUserName(User.Identity.Name);
                if (authentictedUser != null)
                {
                    UserSessionModel user = FunctionHelpers.GetUserSessionModel(authentictedUser);
                    HttpContext.Current.Session.Add("UserProfile", user);
                }
                else
                {
                    FormsAuthentication.SignOut();
                    Session.Abandon();
                }

            }
        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {
            HttpCookie authCookie = Request.Cookies[FormsAuthentication.FormsCookieName];
            if (authCookie != null)
            {
                // Get the forms authentication ticket.
                FormsAuthenticationTicket authTicket = FormsAuthentication.Decrypt(authCookie.Value);
                var identity = new GenericIdentity(authTicket.Name, "Forms");
                var principal = new MyPrincipal(identity);

                // Get the custom user data encrypted in the ticket.
                string userData = ((FormsIdentity)(Context.User.Identity)).Ticket.UserData;

                // Deserialize the json data and set it on the custom principal.
                var serializer = new JavaScriptSerializer();
                principal.User = (UserSessionModel)serializer.Deserialize(userData, typeof(UserSessionModel));



                // Set the context user.
                Context.User = principal;
            }
        }


    }
}
