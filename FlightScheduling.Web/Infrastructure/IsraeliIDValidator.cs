﻿using FlightScheduling.Language;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FlightScheduling.Web.Infrastructure
{
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false, Inherited = true)]
    public class IsraeliIDValidator : ValidationAttribute, IClientValidatable
    {
        private string _DefaultErrorMessage = ValidationStrings.IsraeliIDValidation;

        public IsraeliIDValidator()
        {
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if (value != null)
            {
                string IDNum = (string)value;

                if (!System.Text.RegularExpressions.Regex.IsMatch(IDNum, @"^\d{5,9}$"))
                    return new ValidationResult(_DefaultErrorMessage);

                // The number is too short - add leading 0000
                if (IDNum.Length < 9)
                {
                    while (IDNum.Length < 9)
                    {
                        IDNum = '0' + IDNum;
                    }
                }

                // CHECK THE ID NUMBER
                int mone = 0;
                int incNum;
                for (int i = 0; i < 9; i++)
                {
                    incNum = Convert.ToInt32(IDNum[i].ToString());
                    incNum *= (i % 2) + 1;
                    if (incNum > 9)
                        incNum -= 9;
                    mone += incNum;
                }
                if (mone % 10 == 0)
                    return ValidationResult.Success;
                else
                    return new ValidationResult(_DefaultErrorMessage);
            }
            // Validate correct input
            return new ValidationResult(_DefaultErrorMessage);
        }

        public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
        {
            var rule = new ModelClientValidationRule
            {
                ValidationType = "israeliid",
                ErrorMessage = _DefaultErrorMessage
            };

            yield return rule;
        }

    }
}