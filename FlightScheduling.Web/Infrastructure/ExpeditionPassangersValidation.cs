﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;
using System.Resources;
using System.Web;
using FlightScheduling.Language;

namespace FlightScheduling.Web.Infrastructure
{
    public class ExpeditionPassangersValidation : ValidationAttribute
    {
        //Properties
        public string PropertyName { get; private set; }


        public ExpeditionPassangersValidation(String propertyName)
        {
            this.PropertyName = propertyName;
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var property = validationContext.ObjectType.GetProperty(PropertyName);

            if (value != null)
            {
                int NumberOfPassengers = (int) value;

                int NumberOfbasses = (int) property.GetValue(validationContext.ObjectInstance, null);

                if (NumberOfbasses < 2)
                {
                    return new ValidationResult(ValidationStrings.NumberOfBuses);
                }

                int valueToCheck = NumberOfPassengers/NumberOfbasses;

                if (valueToCheck < 40 || valueToCheck > 50)
                {
                    ResourceManager rm = new ResourceManager(ErrorMessageResourceType.FullName, ErrorMessageResourceType.Assembly);

                    return new ValidationResult(rm.GetString(ErrorMessageResourceName));
                }

            }

            return ValidationResult.Success;
        }
    }
}