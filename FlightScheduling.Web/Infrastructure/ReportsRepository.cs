﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace FlightScheduling.Web.Infrastructure
{
    public static class ReportsRepository
    {

        public static string ReportServerURL => ConfigurationManager.AppSettings["ReportServerUrl"];
        public static string Form60 => "Form60";

        public static string GetReportFullPath(string reportUrl)
        {
            return $"/Reports/{reportUrl}";
        }
    }
}