﻿using FlightScheduling.Language;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Linq.Expressions;
using System.Diagnostics;
using System.Resources;
using Flight.DAL.FlightDB.Methods;
using FlightScheduling.Web.Areas.Administration.Models.GroupForms;
using FlightScheduling.DAL;

namespace FlightScheduling.Web.Infrastructure.Notifications
{

    public interface ConvertProperties
    {
        string ConvertProperties(string _stringToConvert);
    }

    public class NotificationTemplateCustomDetails
    {
        //Properties
        [Display(ResourceType = typeof(AccountStrings), Name = "UserID")]
        public int UserID { get; set; }
        [Display(ResourceType = typeof(AccountStrings), Name = "FirstName")]
        public string UserFirstName { get; set; }
        [Display(ResourceType = typeof(AccountStrings), Name = "LastName")]
        public string UserLastName { get; set; }
    }

    public class NotificationFile180CustomDetails : NotificationTemplateCustomDetails, ConvertProperties
    {
        //Properties
        [Display(ResourceType = typeof(GroupsStrings), Name = "FileID")]
        public int FileID { get; set; }
        [Display(ResourceType = typeof(GroupsStrings), Name = "FileName")]
        public string FileName { get; set; }

        [Display(ResourceType = typeof(GroupsStrings), Name = "GroupInternalID")]
        public int GroupID { get; set; }

        [Display(ResourceType = typeof(GroupsStrings), Name = "ExpeditionID")]
        public string GroupExternalID { get; set; }

        public string GroupUrl { get; set; }

        public string FileUrl { get; set; }

        public string ConvertProperties(string _stringToConvert)
        {
            Dictionary<string, string> PropertiesToConvert = new Dictionary<string, string>();
            PropertiesToConvert = NotificationHelper.GetPropertiesNamesAndValues<NotificationFile180CustomDetails>(this);

            foreach (var _property in PropertiesToConvert)
            {
                _stringToConvert = _stringToConvert.Replace(_property.Key, _property.Value);
            }

            return _stringToConvert;
        }
    }

    public class NotificationGroupFileCreation : NotificationTemplateCustomDetails, ConvertProperties
    {
        // Properties
        [Display(ResourceType = typeof(GroupsStrings), Name = "FileName")]
        public string Name { get; set; }

        [Display(ResourceType = typeof(GroupsStrings), Name = "FileType")]
        public eGroupFileType FileType { get; set; }

        [Display(ResourceType = typeof(GroupsStrings), Name = "FileStatus")]
        public eGroupFileStatus FileStatus { get; set; }

        [Display(ResourceType = typeof(GroupsStrings), Name = "FileComments")]
        public string Comments { get; set; }

        public string Url { get; set; }

        [Display(ResourceType = typeof(GroupsStrings), Name = "GroupInternalID")]
        public int GroupID { get; set; }

        [Display(ResourceType = typeof(GroupsStrings), Name = "ExpeditionID")]
        public string GroupExternalID { get; set; }

        public string FormUrl { get; set; }

        public string ConvertProperties(string _stringToConvert)
        {
            Dictionary<string, string> PropertiesToConvert = new Dictionary<string, string>();
            PropertiesToConvert = NotificationHelper.GetPropertiesNamesAndValues<NotificationGroupFileCreation>(this);

            foreach (var _property in PropertiesToConvert)
            {
                _stringToConvert = _stringToConvert.Replace(_property.Key, _property.Value);
            }

            return _stringToConvert;
        }
    }

    public class NotificationForm180Creation : NotificationTemplateCustomDetails, ConvertProperties
    {
        //Properties
        [Display(ResourceType = typeof(SchoolsStrings), Name = "ID")]
        public int SchoolID { get; set; }

        [Display(ResourceType = typeof(SchoolsStrings), Name = "Name")]
        public string SchoolName { get; set; }

        [Display(ResourceType = typeof(GroupsStrings), Name = "GroupInternalID")]
        public int GroupID { get; set; }

        [Display(ResourceType = typeof(GroupsStrings), Name = "ExpeditionID")]
        public string GroupExternalID { get; set; }

        public string FormUrl { get; set; }

        public string ConvertProperties(string _stringToConvert)
        {
            Dictionary<string, string> PropertiesToConvert = new Dictionary<string, string>();
            PropertiesToConvert = NotificationHelper.GetPropertiesNamesAndValues<NotificationForm180Creation>(this);

            foreach (var _property in PropertiesToConvert)
            {
                _stringToConvert = _stringToConvert.Replace(_property.Key, _property.Value);
            }

            return _stringToConvert;
        }
    }

    public class NotificationForm180StatusUpdate : NotificationTemplateCustomDetails, ConvertProperties
    {
        //Properties
        [Display(ResourceType = typeof(SchoolsStrings), Name = "ID")]
        public int SchoolID { get; set; }

        [Display(ResourceType = typeof(SchoolsStrings), Name = "Name")]
        public string SchoolName { get; set; }

        [Display(ResourceType = typeof(GroupsStrings), Name = "GroupInternalID")]
        public int GroupID { get; set; }

        [Display(ResourceType = typeof(GroupsStrings), Name = "ExpeditionID")]
        public string GroupExternalID { get; set; }

        [Display(ResourceType = typeof(CommonStrings), Name = "UpdateDate")]
        public DateTime UpdateDate { get; set; }

        [Display(ResourceType = typeof(GroupFormsStrings), Name = "FormStatus")]
        public string FormStatus { get; set; }

        public string FormUrl { get; set; }

        public string ConvertProperties(string _stringToConvert)
        {
            Dictionary<string, string> PropertiesToConvert = new Dictionary<string, string>();
            PropertiesToConvert = NotificationHelper.GetPropertiesNamesAndValues<NotificationForm180StatusUpdate>(this);

            foreach (var _property in PropertiesToConvert)
            {
                _stringToConvert = _stringToConvert.Replace(_property.Key, _property.Value);
            }

            return _stringToConvert;
        }
    }

    public class NotificationForm60StatusUpdate : NotificationTemplateCustomDetails, ConvertProperties
    {
        //Properties

        [Display(ResourceType = typeof(GroupsStrings), Name = "GroupInternalID")]
        public int GroupID { get; set; }

        [Display(ResourceType = typeof(GroupsStrings), Name = "ExpeditionID")]
        public string GroupExternalID { get; set; }

        [Display(ResourceType = typeof(CommonStrings), Name = "UpdateDate")]
        public DateTime UpdateDate { get; set; }

        [Display(ResourceType = typeof(GroupFormsStrings), Name = "FormStatus")]
        public string FormStatus { get; set; }

        public string FormUrl { get; set; }

        //public GroupManagerData ManagerData { get; set; }

        public string ConvertProperties(string _stringToConvert)
        {
            Dictionary<string, string> PropertiesToConvert = new Dictionary<string, string>();
            PropertiesToConvert = NotificationHelper.GetPropertiesNamesAndValues<NotificationForm60StatusUpdate>(this);

            foreach (var _property in PropertiesToConvert)
            {
                _stringToConvert = _stringToConvert.Replace(_property.Key, _property.Value);
            }

            return _stringToConvert;
        }
    }

    public static class NotificationHelper
    {
        public static Dictionary<string, string> GetPropertiesNames<T>()
        {
            Dictionary<string, string> Properties = new Dictionary<string, string>();

            foreach (var property in typeof(T).GetProperties())
            {
                Properties.Add(property.Name, GetPropertyDisplayName<T>(property));
            }

            return Properties;
        }

        public static Dictionary<string, string> GetPropertiesNamesAndValues<T>(T obj)
        {
            Dictionary<string, string> Properties = new Dictionary<string, string>();

            foreach (var property in typeof(T).GetProperties())
            {
                Properties.Add(String.Format("{{{0}}}", property.Name), obj.GetType().GetProperty(property.Name).GetValue(obj, null).ToString());
            }

            return Properties;
        }

        public static bool IsValidEmail(string email)
        {
            try
            {
                var addr = new System.Net.Mail.MailAddress(email);
                return addr.Address == email;
            }
            catch
            {
                return false;
            }
        }

        public static string GetPropertyDisplayName<T>(PropertyInfo property)
        {
            var at = property.GetCustomAttributes<DisplayAttribute>(true).SingleOrDefault();
            if (at == null)
                return null;
            ResourceManager resourceManager = new ResourceManager(at.ResourceType.Namespace + "." + at.ResourceType.Name, at.ResourceType.Assembly);
            return resourceManager.GetString(at.Name);
        }
    }


}