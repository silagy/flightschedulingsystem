﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FlightScheduling.DAL;

namespace FlightScheduling.Web.Infrastructure
{
    public static class SystemConfigurationConfig
    {

        public static void SetSystemConfigurationSettings(bool update = false)
        {
            using (var _db = new DBRepository())
            {
                var data = _db.GetSystemConfiguraion().ToList();

                foreach (var sysConfigProperty in data)
                {
                    if (update)
                    {
                        HttpContext.Current.Application[sysConfigProperty.PropertyName] = sysConfigProperty.PropertyValue;
                    }
                    else
                    {
                        HttpContext.Current.Application.Add(sysConfigProperty.PropertyName, sysConfigProperty.PropertyValue);
                    }

                }
            }
        }
    }
}