﻿using FlightScheduling.DAL.CustomEntities.Permissions;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;

namespace FlightScheduling.Web.Infrastructure
{
    public static class AppConfig
    {
        public static string OrdersStorageRoot
        {
            get { return HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["OrdersStorage"]); }
        }


        public static string[] BundleDatePickerCss
        {
            get
            {
                List<string> files = new List<string>();
                files.Add("~/Scripts/pickadate/themes/default.css");
                files.Add("~/Scripts/pickadate/themes/default.date.css");
                files.Add("~/Scripts/pickadate/themes/default.time.css");
                files.Add("~/Scripts/pickadate/themes/pickadate.custom.css");
                files.Add("~/Scripts/pickadate/themes/rtl.css");

                // New datepicker Flatpickr
                //files.Add("~/Scripts/Flatpicker/flatpickr.css");
                //files.Add("~/Scripts/Flatpicker/ie.css");

                //if (FlightScheduling.Web.Infrastructure.AppConfig.CurrentCulture ==
                //    FlightScheduling.Web.Infrastructure.AppConfig.eLanguage.Hebrew)
                //{
                //    files.Add("~/Scripts/pickadate/themes/rtl.css");
                //}

                return files.ToArray();
            }
        }

        public static string[] BundleDatePickerScripts
        {
            get
            {
                List<string> files = new List<string>();
                //Old date picker
                files.Add("~/Scripts/pickadate/picker.js");
                files.Add("~/Scripts/pickadate/picker.date.js");
                files.Add("~/Scripts/pickadate/picker.time.js");
                files.Add("~/Scripts/pickadate/datepicker.initialization.js");

                //New date picker Flatpickr
                //files.Add("~/Scripts/Flatpicker/flatpickr.js");
                //files.Add("~/Scripts/Flatpicker/l10n/he.js");
                //files.Add("~/Scripts/Flatpicker/flatpickr.initilization.js");

                //if (FlightScheduling.Web.Infrastructure.AppConfig.CurrentCulture ==
                //    FlightScheduling.Web.Infrastructure.AppConfig.eLanguage.Hebrew)
                //{
                //    files.Add("~/Scripts/pickadate/datepicker.initialization.js");
                //}
                //else
                //{
                //    files.Add("~/Scripts/pickadate/datepicker.initialization_en.js");
                //}

                return files.ToArray();
            }
        }

        public static int MaxBusesPerDay
        {
            get { return Convert.ToInt16(ConfigurationManager.AppSettings["MaxBusesPerDay"]); }
        }

        public static string GetSiteRootUrl => HttpContext.Current.Request.Url.Scheme + "://" + HttpContext.Current.Request.Url.Authority +
    HttpContext.Current.Request.ApplicationPath.TrimEnd('/') + "/";

        public static string InstructorFilesRelativePath
            => ConfigurationManager.AppSettings["InstructorFilesRelativePath"];

        public static int PassYearToCheck => Convert.ToInt32(ConfigurationManager.AppSettings["PassYearToCheck"]);

        public static string ReportViewerUsername => ConfigurationManager.AppSettings["ReportServerUsername"];
        public static string ReportViewerPassword => ConfigurationManager.AppSettings["ReportServerPassword"];

        public static string MixPanelProjectId => ConfigurationManager.AppSettings["MixPanelProjectId"];


        public static string InstructorProfilePictureRelativePath
            => ConfigurationManager.AppSettings["InstructorProfilePictureRelativePath"];

        public static string FormatTime => "HH:mm";
        public static string FormatDate => "dd/MM/yyyy";
        public static string FormatDateMonthLong => "dd MMM, yyyy";
        public static string FormatDateAndTime => "dd/MM/yyyy HH:mm";
        public static string FormatDateGrid => "{0:dd/MM/yyyy}";
        public static string FormatDateTimeGrid => "{0:dd/MM/yyyy HH:mm}";
        public static string[] DateTimeFormatParsing = { "dd/MM/yyyy", "dd/M/yyyy", "d/M/yyyy", "d/MM/yyyy", "dd/MM/yy", "dd/M/yy", "d/M/yy", "d/MM/yy", "d/MM/yyyy", "d.M.yyyy", @"dd\M\yyyy", "d.M.yy" };
        public static string[] DateTimeWithTimeFormatParsing = { "dd/MM/yyyy hh:mm", "dd/M/yyyy hh:mm", "d/M/yyyy hh:mm", "d/MM/yyyy hh:mm", "dd/MM/yy hh:mm", "dd/M/yy hh:mm", "d/M/yy hh:mm", "d/MM/yy hh:mm", "d/MM/yyyy hh:mm", "d.M.yyyy hh:mm", @"dd\M\yyyy hh:mm", "d.M.yy hh:mm", "dd/MM/yyyy HH:mm" };
        public static string[] DateTimeWithTime24FormatParsing = { "dd/MM/yyyy HH:mm" };

        public static PermissionBitEnum[] AdministrationPermission = {
            PermissionBitEnum.Administration_AgencyViewing,
            PermissionBitEnum.Administration_Sites_View
        };

        public static string GroupsFilesStorageRoot
        {
            get { return HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["GroupFilesStorageRoot"]); }
        }

        public static string GroupFilesPathRootRelative => ConfigurationManager.AppSettings["GroupFilesStorageRootRelative"];

        public static string Groups180FormsFileStorage
        {
            get { return HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["Group180FormFiles"]); }
        }

        public static string Groups180FormsFileStorageRelative
            => ConfigurationManager.AppSettings["Group180FormFilesRelative"];

        public static IEnumerable<int> Group180SearchForMentors = ConfigurationManager.AppSettings["Group180SearchForMentors"].Split(',').Select(x => (int)Convert.ToInt16(x)).AsEnumerable();
        public static IEnumerable<int> SearchForAllStaf = ConfigurationManager.AppSettings["SearchForAllStaf"].Split(',').Select(x => (int)Convert.ToInt16(x)).AsEnumerable();
        public static int SearchForInstructors = Convert.ToInt16(ConfigurationManager.AppSettings["SearchForInstructors"]);

        public static string GroupsImportTemplateRoot
        {
            get { return HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["GroupImportsTemplatesRoot"]); }
        }


        public static string InstructorsFilesStorageRoot
        {
            get { return HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["InstructorFilesStorageRoot"]); }
        }

        public static string InstructorsProfilePictureStorageRoot
        {
            get { return HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["InstructorProfilePictureStorageRoot"]); }
        }

        public static string CurrentLangruageTwoLetters
        {
            get { return Thread.CurrentThread.CurrentCulture.TwoLetterISOLanguageName; }
        }


        public static string TempImportRoot
        {
            get { return HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["TempImportFilesStorageRoot"]); }
        }

        public static string TempImportRelativePath
            => ConfigurationManager.AppSettings["TempImportFilesRelativePath"];

        public static eLanguage CurrentCulture
        {
            get
            {
                if (Thread.CurrentThread.CurrentCulture.Equals(CultureInfo.CreateSpecificCulture("he-IL")))
                {
                    return eLanguage.Hebrew;
                }
                else
                {
                    return eLanguage.English;
                }
            }
        }

        public enum eLanguage
        {
            Hebrew,
            English,
        }
    }
}