﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Flight.Membership;
using FlightScheduling.DAL;
using FlightScheduling.Website.Infrastructure;


namespace Otakim.Website.Infrastructure
{
    [AttributeUsage(AttributeTargets.Method | AttributeTargets.Class, Inherited = true, AllowMultiple = true)]
    public class EndiloAuthorization : AuthorizeAttribute
    {
        public EndiloAuthorization(params object[] roles)
        {
            if (roles.Any(r => r.GetType().BaseType != typeof(Enum)))
                throw new ArgumentException("roles");

            this.Roles = string.Join(",", roles.Select(r => Enum.GetName(r.GetType(), r)));
        }

        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            if (filterContext == null) { throw new ArgumentNullException("filterContext"); }

            // check if user has access and return HttpUnauthorizedResult when user is not allowed

            if (filterContext.HttpContext.User.Identity.IsAuthenticated)
            {


                foreach (var role in Roles.Split(','))
                {
                    if (filterContext.HttpContext.User.IsInRole(role.ToString()))
                    {
                        

                        if (filterContext.HttpContext.Session["UserProfile"] == null)
                        {
                            var membership = new FlightMembershipProvider();
                            Users _user = membership.GetUserByEmailOrUserName(filterContext.HttpContext.User.Identity.Name);
                            filterContext.HttpContext.Session.Add("UserProfile", FunctionHelpers.GetUserSessionModel(_user));
                        }
                        SetCachePolicy(filterContext);
                        return;
                    }

                }
                
            }
            
            filterContext.Result = new HttpUnauthorizedResult();
            SetCachePolicy(filterContext);
            

        }

        public void CacheValidationHandler(HttpContext context, object data, ref HttpValidationStatus validationStatus)
        {
            validationStatus = OnCacheAuthorization(new HttpContextWrapper(context));
        }

        protected void SetCachePolicy(AuthorizationContext filterContext)
        {
            // disable caching, so that a user that is not logged in cannot accidentally get a cached version of a secure page
            HttpCachePolicyBase cachePolicy = filterContext.HttpContext.Response.Cache;
            cachePolicy.SetProxyMaxAge(new TimeSpan(0));
            cachePolicy.AddValidationCallback(CacheValidationHandler, null /* data */);
        }
    }
}