﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FlightScheduling.Web.Infrastructure
{
    public static class SystemConfigurationRepository
    {
        public static DateTime ExpeditionPublishStartDate => Convert.ToDateTime(HttpContext.Current.Application["ExpeditionPublishStartDate"]);
        public static DateTime ExpeditionPublishEndDate => Convert.ToDateTime(HttpContext.Current.Application["ExpeditionPublishEndDate"]);
        public static DateTime TripsFilterStartDate => Convert.ToDateTime(HttpContext.Current.Application["TripsFilterStartDate"]);
        public static DateTime TripsFilterEndDate => Convert.ToDateTime(HttpContext.Current.Application["TripsFilterEndDate"]);
        public static DateTime FlightsFilterStartDate => Convert.ToDateTime(HttpContext.Current.Application["FlightsFilterStartDate"]);
        public static DateTime FlightsFilterEndDate => Convert.ToDateTime(HttpContext.Current.Application["FlightsFilterEndDate"]);
        public static DateTime GroupsFilterStartDate => Convert.ToDateTime(HttpContext.Current.Application["GroupsFilterStartDate"]);
        public static DateTime GroupsFilterEndDate => Convert.ToDateTime(HttpContext.Current.Application["GroupsFilterEndDate"]);
        public static int TripAvilabilityBusesTreshold => Convert.ToInt32(HttpContext.Current.Application["TripAvilabilityBusesTreshold"]);
        public static int TripAvilabilityPassengerTreshold => Convert.ToInt32(HttpContext.Current.Application["TripAvilabilityPassengerTreshold"]);
        public static DateTime TrainingsFilterStartDate => Convert.ToDateTime(HttpContext.Current.Application["TrainingsFilterStartDate"]);
        public static DateTime TrainingsFilterEndDate => Convert.ToDateTime(HttpContext.Current.Application["TrainingsFilterEndDate"]);
        public static IEnumerable<int> RolesToIncludeSpecialValidationRule => HttpContext.Current.Application["RolesToIncludeSpecialValidationRule"].ToString().Split(',').Select(x => Convert.ToInt32(x)).AsEnumerable();
        public static int NumberOfRequiredHoursForSpecialTraining => Convert.ToInt32(HttpContext.Current.Application["NumberOfRequiredHoursForSpecialTraining"].ToString());
        public static int GroupManagerRoleId => Convert.ToInt32(HttpContext.Current.Application["GroupManagerRoleID"].ToString());
        public static int InstructorRoleId => Convert.ToInt32(HttpContext.Current.Application["InstructorPermissionRole"].ToString());
        public static string OrderTripInstructionsLink => HttpContext.Current.Application["OrderTripInstructionsLink"].ToString();
        public static DateTime DisableExpeditions => Convert.ToDateTime(HttpContext.Current.Application["DisableExpeditions"]);

    }
}
