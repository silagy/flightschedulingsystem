﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using CsvHelper;
using CsvHelper.TypeConversion;
using FlightScheduling.Web.Areas.Administration.Models.Flights;
using FlightScheduling.Website.Infrastructure;
using FlightScheduling.Language;
using CsvHelper.Configuration;

namespace FlightScheduling.Web.Infrastructure
{
    public static class CsvConverterHelper
    {
        public static IEnumerable<T> ConvertFromCsv<T>(CsvReader csv, Type mapperType, bool hasHeaderRecord = false)
        {
            var dataToReturn = new List<T>();
            csv.Configuration.HasHeaderRecord = hasHeaderRecord;
            csv.Configuration.RegisterClassMap(mapperType);
            csv.Configuration.TrimOptions = TrimOptions.Trim;
            //csv.Configuration. = true;
            csv.Configuration.ReadingExceptionOccurred = exception =>
            {
                throw new CsvHelperException(exception.ReadingContext, string.Format("could not parse row:{0} at row#:{1}", string.Join(",", exception.ReadingContext.CurrentIndex), exception.ReadingContext.Row));
            };
            try
            {
                dataToReturn = csv.GetRecords<T>().ToList();
            }

            catch (TypeConverterException typeExc)
            {
                Console.WriteLine("Error: " + typeExc.Data["CsvHelper"]);
                Console.WriteLine(typeExc);
                throw typeExc;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw e;
            }

            return dataToReturn;
        }

        public class CsvDateConverter : DefaultTypeConverter
        {
            public bool CanConvertFrom(Type type)
            {
                return type == typeof(string);
            }

            public bool CanConvertTo(Type type)
            {
                return type == typeof(string);
            }

            public object ConvertFromString(TypeConverterOptions options, string text)
            {
                return DateTime.ParseExact(text, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            }

            public string ConvertToString(TypeConverterOptions options, object value)
            {
                var theDate = (DateTime)value;
                return theDate.ToString("dd/MM/yyyy");
            }
        }

        public class CsvFlightsFieldsConverter : DefaultTypeConverter
        {
            public bool CanConvertFrom(Type type)
            {
                return type == typeof(string);
            }

            public bool CanConvertTo(Type type)
            {
                return type == typeof(string);
            }

            public object ConvertFromString(TypeConverterOptions options, string text)
            {
                throw new NotImplementedException();
            }

            public string ConvertToString(TypeConverterOptions options, object value)
            {
                FlightViewModel.FlightsFields enumValue = (FlightViewModel.FlightsFields)value;
                return FunctionHelpers.GetResourceString<FlightsStrings>(enumValue.ToString());
            }
        }
    }
}