﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Resources;
using System.Security.Principal;
using System.Text;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Script.Serialization;
using System.Web.Security;
using Endilo.Helpers;
using FlightScheduling.DAL;
using FlightScheduling.DAL.CustomEntities.Groups;
using FlightScheduling.Language;
using FlightScheduling.Web.Areas.Administration.Models.Common;
using FlightScheduling.Web.Areas.Administration.Models.Flights;
using FlightScheduling.Web.Infrastructure;
using FlightScheduling.Website.Models.Account;

namespace FlightScheduling.Website.Infrastructure
{
    public static class FunctionHelpers
    {
        public static bool IsInAnyRole(this IPrincipal user, List<string> roles)
        {
            var userRoles = Roles.GetRolesForUser(user.Identity.Name);

            return userRoles.Any(u => roles.Contains(u));
        }

        public static GroupColor GetRandomColorID(List<GroupColor> groupsColors)
        {
            var rnd = new Random(DateTime.Now.Millisecond);
            return groupsColors[rnd.Next(1, groupsColors.Count) - 1];
        }

        public static MvcHtmlString GetNegativeNumber(int number)
        {
            string str = String.Empty;

            if (number < 0)
            {
                str = String.Format("{0}{1}", Math.Abs(number), "-");
            }
            else
            {
                str = String.Format("{0}", number);
            }

            return MvcHtmlString.Create(str);
        }

        public static bool HasFile(this HttpPostedFileBase file)
        {
            return file != null && file.ContentLength > 0;
        }

        public static UserSessionModel GetUserSessionModel(Users authentictedUser)
        {
            UserSessionModel _userSession = new UserSessionModel();
            _userSession.UserID = authentictedUser.ID;
            _userSession.FirstName = authentictedUser.Firstname;
            _userSession.LastName = authentictedUser.Lastname;
            _userSession.FullName = authentictedUser.FullName;
            _userSession.Email = authentictedUser.Email;
            _userSession.RoleID = authentictedUser.UserType;
            _userSession.RegionID = authentictedUser.Region;
            _userSession.RoleType = authentictedUser.UserRole;
            _userSession.PermissionRoleID = authentictedUser.PermissionRoleID;
            _userSession.InstructorID = authentictedUser.InstructorID;
            _userSession.AgencyID = authentictedUser.AgencyID;


            return _userSession;

        }

        public static UserSessionModel User
        {
            get
            {
                if (HttpContext.Current.User.Identity.IsAuthenticated &&
                    HttpContext.Current.Session["UserProfile"] != null)
                {
                    // The user is authenticated. Return the user from the forms auth ticket.
                    return (UserSessionModel)HttpContext.Current.Session["UserProfile"];
                }
                else
                {
                    return null;
                }

            }
        }

        public static string GetResourceString<T>(string key)
        {
            ResourceManager rm = new ResourceManager(typeof(T));
            string someString = rm.GetString(key);
            return someString;
        }

        public static string GetUserJson
        {
            get
            {
                if (HttpContext.Current.User.Identity.IsAuthenticated &&
                    HttpContext.Current.Session["UserProfile"] != null)
                {
                    UserSessionModel user = (UserSessionModel)HttpContext.Current.Session["UserProfile"];
                    UserMixPanelModel mixPanelUser = new UserMixPanelModel()
                    {
                        FirstName = user.FirstName,
                        LastName = user.LastName,
                        InstructorID = user.InstructorID,
                        PermissionRoleID = user.PermissionRoleID,
                        RegionID = user.RegionID,
                        RoleID = user.RoleID,
                        RoleName = user.RoleName,
                        RoleType = user.RoleType,
                        UserID = user.UserID
                    };

                    return new JavaScriptSerializer().Serialize(mixPanelUser);
                }

                return null;
            }
        }

        public static string GetFlightFieldString(string field)
        {
            if (field != null)
            {
                return ((FlightViewModel.FlightsFields)Convert.ToInt32(field)).GetDescription();
            }

            return "";
        }

        public static MvcHtmlString Nl2Br(this HtmlHelper htmlHelper, string text)
        {
            if (string.IsNullOrEmpty(text))
                return MvcHtmlString.Create(text);
            else
            {
                StringBuilder builder = new StringBuilder();
                string[] lines = text.Split('\n');
                for (int i = 0; i < lines.Length; i++)
                {
                    if (i > 0)
                        builder.Append("<br/>\n");
                    builder.Append(HttpUtility.HtmlEncode(lines[i]));
                }
                return MvcHtmlString.Create(builder.ToString());
            }
        }

        public static Bundle NonOrdering(this Bundle bundle)
        {
            bundle.Orderer = new NonOrderingBundleOrderer();
            return bundle;
        }

        public static IEnumerable<DateTime> MonthsBetween(
            DateTime startDate,
            DateTime endDate)
        {
            DateTime iterator;
            DateTime limit;

            if (endDate > startDate)
            {
                iterator = new DateTime(startDate.Year, startDate.Month, 1);
                limit = endDate;
            }
            else
            {
                iterator = new DateTime(endDate.Year, endDate.Month, 1);
                limit = startDate;
            }

            var dateTimeFormat = CultureInfo.CurrentCulture.DateTimeFormat;
            while (iterator <= limit)
            {
                yield return new DateTime(iterator.Year, iterator.Month, 1);
                iterator = iterator.AddMonths(1);
            }
        }

        public static string ReturnTripIndicationClass(int tripIndication)
        {
            string tripIndicationClass = "";
            switch (tripIndication)
            {
                case 0:
                    {
                        tripIndicationClass = "trip-red";
                        break;
                    }
                case 1:
                    {
                        tripIndicationClass = "trip-orange";
                        break;
                    }
                case 2:
                    {
                        tripIndicationClass = "trip-green";
                        break;
                    }
                default:
                    break;
            }

            return tripIndicationClass;
        }

        public static IEnumerable<TimeSelectListItem> GetTimes(int minuteInterval = 5, double StartHour = 0, double EndHour = 0, bool specialFormat = false)
        {
            DateTime _date = new DateTime(1990, 01, 01, 00, 00, 00);
            DateTime _targetDate = _date.AddDays(1);
            _date = _date.AddHours(StartHour);
            _targetDate = _targetDate.AddHours(-EndHour);

            List<TimeSelectListItem> _items = new List<TimeSelectListItem>();

            while (_targetDate.CompareTo(_date) > 0)
            {
                string time = "";

                if (specialFormat)
                {
                    time = String.Format("{0} {1}", _date.TimeOfDay.TotalHours.ToString(), _date.TimeOfDay.TotalHours < 1.5 ? CommonStrings.Hour : CommonStrings.Hours);
                }


                _items.Add(new TimeSelectListItem()
                {
                    ID = _date.ToString("HH:mm"),
                    Name = specialFormat ? time : _date.ToString("HH:mm")
                });

                _date = _date.AddMinutes(minuteInterval);
            }

            return _items.AsEnumerable();
        }

        public static IEnumerable<TimeSelectListItem> GetTimes(int startHour, int StartMinutes, int EndHours, int EndMinutes, int MinutesInterval = 30)
        {
            DateTime _date = new DateTime(1990, 01, 01, startHour, StartMinutes, 0);
            DateTime _targetDate = new DateTime(_date.Year, _date.Month, _date.Day, EndHours, EndMinutes, 0);

            List<TimeSelectListItem> _items = new List<TimeSelectListItem>();

            while (_targetDate.CompareTo(_date) >= 0)
            {
                _items.Add(new TimeSelectListItem()
                {
                    ID = _date.ToString("HH:mm"),
                    Name = _date.ToString("HH:mm")
                });

                _date = _date.AddMinutes(MinutesInterval);
            }

            return _items.AsEnumerable();
        }

    }
}