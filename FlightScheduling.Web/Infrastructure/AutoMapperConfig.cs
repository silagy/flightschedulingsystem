﻿using System;
using System.Runtime.InteropServices;
using AutoMapper;
using FlightScheduling.DAL.CustomEntities;
using FlightScheduling.DAL.CustomEntities.Groups;
using FlightScheduling.Web.Areas.Administration.Models.Reports;
using FlightScheduling.Web.Areas.Administration.Models.Administration;
using FlightScheduling.Web.Areas.Administration.Models.Groups;
using FlightScheduling.Web.Areas.Administration.Models.Messages;
using FlightScheduling.DAL;
using FlightScheduling.Web.Areas.Administration.Models.Instructors;
using FlightScheduling.Web.Areas.Administration.Models.Schools;
using FlightScheduling.DAL.CustomEntities.Notifications;
using FlightScheduling.Web.Areas.Administration.Models.Flights;
using FlightScheduling.Web.Areas.Administration.Models.Trips;

namespace FlightScheduling.Web.Infrastructure
{
    public static class AutoMapperConfig
    {

        public static void RegisterMappings()
        {

            AutoMapper.Mapper.Initialize(cfg =>
            {
                cfg.CreateMap<ExpeditionToCSVIntegration, ExpeditionToCsvIntegrationViewModel>()
                .BeforeMap((src, desc) => desc.DestinationID = (Areas.Administration.Models.Flights.FlightViewModel.FlightsFields)src.DestinationID)
                .BeforeMap((src, desc) => desc.ReturingField = (Areas.Administration.Models.Flights.FlightViewModel.FlightsFields)src.ReturingField);
                cfg.CreateMap<SystemConfiguration, SystemConfigurationItemViewModel>();
                cfg.CreateMap<Messages, MessageViewModel>();
                cfg.CreateMap<Schools, SchoolViewModel>()
                .BeforeMap((src,desc) => desc.RegionEnum = src.RegionEnum);
                cfg.CreateMap<MessageViewModel, Messages>();
                cfg.CreateMap<MessagesDetails, MessagesDetailsViewModel>();
                cfg.CreateMap<GroupViewModel, Group>()
                    .BeforeMap((src, desc) => desc.ColorID = src.Color.ID)
                    .BeforeMap((src, desc) => desc.Status = (int)src.GroupStatus)
                    .BeforeMap((src, desc) => desc.AgencyID = src.AgencyID);
                cfg.CreateMap<Group, GroupViewModel>()
                    .BeforeMap((src, desc) => desc.Color.ID = src.ColorID)
                    .BeforeMap((src, desc) => desc.GroupStatus = src.GroupStatus)
                    .BeforeMap((src, desc) => desc.AgencyID = src.AgencyID);
                cfg.CreateMap<GroupFiles, GroupFileViewModel>()
                .BeforeMap((src, desc) => desc.FileType = src.GroupFileTypeEnum)
                .BeforeMap((src, desc) => desc.FileStatus = src.GroupFileStatusEnum).ReverseMap();
                cfg.CreateMap<GroupRemarks, GroupNoteViewModel>()
                    .BeforeMap((src, desc) => desc.UserFirstName = src.Users.Firstname)
                    .BeforeMap((src, desc) => desc.SchoolName = src.Schools.Name)
                    .ForMember(x => x.Schools, opt => opt.Ignore())
                    .BeforeMap((src, desc) => desc.UserLastName = src.Users.Lastname);
                cfg.CreateMap<GroupNoteViewModel, GroupRemarks>()
                    .BeforeMap((src, desc) => desc.ID = src.ID)
                    .BeforeMap((src, desc) => desc.Notes = src.Notes)
                    .BeforeMap((src, desc) => desc.SchoolID = src.SchoolID)
                    .BeforeMap((src, desc) => desc.UserID = src.UserID)
                    .ForMember(x => x.Schools, opt => opt.Ignore())
                    .ForMember(x => x.Groups, opt => opt.Ignore());
                cfg.CreateMap<GroupParticipants, GroupParticipantViewModel>()
                    .BeforeMap((src, desc) => desc.ParticipantStatusEnum = src.ParticipantStatusEnum)
                    .BeforeMap((src, desc) => desc.ParticipantTypeEnum = src.PatricipantTypeEnum)
                    .BeforeMap((src, desc) => desc.SEX = src.ParticipantsSexEnum);
                cfg.CreateMap<GroupParticipantViewModel, GroupParticipants>()
                    .BeforeMap((src, desc) => desc.ParticipantType = (int)src.ParticipantTypeEnum)
                    .BeforeMap((src, desc) => desc.Status = (int)src.ParticipantStatusEnum)
                    .BeforeMap((src, desc) => desc.SEX = (int)src.SEX)
                    .BeforeMap((src, desc) => desc.ClassType = (int)src.ClassTypeEnum);
                cfg.CreateMap<InstructorViewModel, Instructors>()
                .BeforeMap((src, desc) => desc.SEX = (int)src.SEX)
                .BeforeMap((src, desc) => desc.OrganizationalStatus = (int)src.OrganizationalStatus);
                cfg.CreateMap<Instructors, InstructorViewModel>()
                .BeforeMap((src, desc) => desc.OrganizationalStatus = src.OrganizationalStatusEnum)
                    .ForMember(m => m.InstructorRoleModel,
                        opt => opt.MapFrom(o => Mapper.Map<InstructorRoles, InstructorRoleViewModel>(o.InstructorRoles)))
                        .BeforeMap((src, desc) => desc.InstructorValidity = src.InstructorValidity);
                cfg.CreateMap<InstructorRoles, InstructorRoleViewModel>()
                    .BeforeMap((src, desc) => desc.StartingValidity = (eStartingValidity)src.StartingValidity);
                cfg.CreateMap<InstructorRoleEntity, InstructorRoleViewModel>()
                    .BeforeMap((src, desc) => desc.StartingValidity = (eStartingValidity)src.StartingValidity);
                cfg.CreateMap<InstructorRoleViewModel, InstructorRoles>()
                    .BeforeMap((src, desc) => desc.StartingValidity = (int)src.StartingValidity);
                cfg.CreateMap<TrainingViewModel, Trainings>()
                    .BeforeMap((src, desc) => desc.TrainingLocation = (int)src.TrainingLocation)
                    .BeforeMap((src, desc) => desc.RegionID = (int)src.TrainingRegion)
                    .BeforeMap((src, desc) => desc.TrainingType = (int)src.TrainingType);
                cfg.CreateMap<Trainings, TrainingViewModel>()
                    .BeforeMap((src, desc) => desc.TrainingType = src.TrainingTypeEnum)
                    .BeforeMap((src, desc) => desc.TrainingRegion = src.TrainingRegion)
                    .BeforeMap((src, desc) => desc.TrainingLocation = src.TrainingLocationEnum)
                    .ForMember(t => t.InstructorRole,
                        opt => opt.MapFrom(m => Mapper.Map<InstructorRoles, InstructorRoleViewModel>(m.InstructorRoles)));
                cfg.CreateMap<TrainingViewModel, Training>()
                    .BeforeMap((src, desc) => desc.TrainingLocationID = (int)src.TrainingLocation)
                    .BeforeMap((src, desc) => desc.RegionID = (int)src.TrainingRegion)
                    .BeforeMap((src, desc) => desc.TrainingValidationID = (int)src.TrainingType);
                cfg.CreateMap<Training, TrainingViewModel>()
                    .BeforeMap((src, desc) => desc.TrainingType = src.TrainingTypeEnum)
                    .BeforeMap((src, desc) => desc.TrainingRegion = src.TrainingRegion)
                    .BeforeMap((src, desc) => desc.TrainingLocation = src.TrainingLocationEnum)
                    .ForMember(t => t.InstructorRole,
                        opt => opt.MapFrom(m => Mapper.Map<InstructorRoleEntity, InstructorRoleViewModel>(m.Role)));
                cfg.CreateMap<InstructorTrainings, InstructorTrainingViewModel>()
                    .ForMember(m => m.TrainingDetails,
                        opt => opt.MapFrom(o => Mapper.Map<Trainings, TrainingViewModel>(o.Trainings)))
                    .ForMember(m => m.InstructorDetails,
                        opt => opt.MapFrom(o => Mapper.Map<Instructors, InstructorViewModel>(o.Instructors)));
                cfg.CreateMap<InstructorRemarks, InstructorNoteViewModel>()
                    .BeforeMap((src, desc) => desc.UserFirstName = src.Users.Firstname)
                    .BeforeMap((src, desc) => desc.UserLastName = src.Users.Lastname).ReverseMap();
                cfg.CreateMap<InstructorFiles, InstructorFileViewModel>().ReverseMap();
                cfg.CreateMap<InstructorGroups, InstructorGroupViewModel>()
                    //.ForMember(m => m.Group, opt => opt.MapFrom(o => Mapper.Map<Groups, GroupViewModel>(o.Groups)))
                    .ForMember(m => m.Instructor, opt => opt.MapFrom(o => Mapper.Map<Instructors, InstructorViewModel>(o.Instructors)))
                    .ForMember(m => m.InstructorRole, opt => opt.MapFrom(o => Mapper.Map<InstructorRoles, InstructorRoleViewModel>(o.InstructorRoles)));
                cfg.CreateMap<NotificationTemplate, NotificationTemplateViewModel>();
                cfg.CreateMap<NotificationTemplateViewModel, NotificationTemplate>()
                .BeforeMap((src, desc) => desc.SchedulingType = (int)src.NotificationSchedulingType)
                .BeforeMap((src, desc) => desc.TriggerType = (int)src.NotificationTriggerType)
                .BeforeMap((src, desc) => desc.UserType = (int)src.UserTypeEnum);
                cfg.CreateMap<FlightViewModel, Flights>()
                .BeforeMap((src, desc) => desc.DepartureFieldID = (int)src.DepartureField)
                .BeforeMap((src, desc) => desc.DestinationID = (int)src.DestenationField)
                .BeforeMap((src, desc) => desc.StartDate = (DateTime)src.DepartureDate)
                .BeforeMap((src, desc) => desc.ArrivalTime = (DateTime)src.ArrivalDate).ReverseMap();
                cfg.CreateMap<TripViewModelBasic, Trips>()
                .BeforeMap((src, desc) => desc.DestinationID = (int)src.Destenation)
                .BeforeMap((src, desc) => desc.ReturningDate = src.ReturnDate)
                .BeforeMap((src, desc) => desc.ReturingField = (int)src.ReturnedFrom).ReverseMap();
            });

        }
    }
}