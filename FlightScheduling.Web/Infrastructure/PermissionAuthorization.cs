﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Authentication;
using System.Web;
using System.Web.Mvc;
using Flight.Membership;
using FlightScheduling.DAL;
using FlightScheduling.DAL.CustomEntities.Permissions;
using FlightScheduling.Web.App_Start;
using FlightScheduling.Website.Infrastructure;


namespace Otakim.Website.Infrastructure
{
    [AttributeUsage(AttributeTargets.Method | AttributeTargets.Class, Inherited = true, AllowMultiple = true)]
    public class PermissionAuthorization : AuthorizeAttribute
    {
        private IEnumerable<PermissionBitEnum> _requiredPermission;
        public PermissionAuthorization(params PermissionBitEnum[] _permissions)
        {
            if (_permissions.Any(r => r.GetType().BaseType != typeof(Enum)))
                throw new ArgumentException("Permissions");

            this._requiredPermission = _permissions.AsEnumerable();
        }

        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            if (filterContext == null) { throw new ArgumentNullException("filterContext"); }

            // check if user has access and return HttpUnauthorizedResult when user is not allowed

            bool allowProceed = false;

            if (filterContext.HttpContext.User.Identity.IsAuthenticated && FunctionHelpers.User != null && FunctionHelpers.User.PermissionRoleID != null)
            {
                IEnumerable<PermissionBits> _UserPermissions = PermissionLoader.GetUserPermission((int)FunctionHelpers.User.PermissionRoleID);
                //Check for permission
                allowProceed = !_requiredPermission.Except(_UserPermissions.Select(p => p.PermissionBitEnum)).Any();
            }

            if (!allowProceed)
            {
                //send them off to the login page
                var url = new UrlHelper(filterContext.RequestContext);
                var loginUrl = url.Content("~/Account/Login");
                filterContext.HttpContext.Response.Redirect(loginUrl, true);
            }


            SetCachePolicy(filterContext);


        }

        public void CacheValidationHandler(HttpContext context, object data, ref HttpValidationStatus validationStatus)
        {
            validationStatus = OnCacheAuthorization(new HttpContextWrapper(context));
        }

        protected void SetCachePolicy(AuthorizationContext filterContext)
        {
            // disable caching, so that a user that is not logged in cannot accidentally get a cached version of a secure page
            HttpCachePolicyBase cachePolicy = filterContext.HttpContext.Response.Cache;
            cachePolicy.SetProxyMaxAge(new TimeSpan(0));
            cachePolicy.AddValidationCallback(CacheValidationHandler, null /* data */);
        }
    }
}