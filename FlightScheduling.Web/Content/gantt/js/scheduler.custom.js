﻿mobiscroll.setOptions({
    theme: 'material',
    themeVariant: 'light',
    locale: mobiscroll.locale['he']
});

$(function (){
    myResources = [{
        id: 'contractors',
        name: 'לו"ז',
        collapsed: false,
        children: [{
            id: 1,
            name: 'תחנת רכבת',
        }, 
            {
            id: 2,
            name: 'בי"ק לודז',
        }]
    }, {
        id: 'staff',
        name: 'אנשי צוות',
        collapsed: false,
        children: [{
            id: 1,
            name: 'פאבל'
        },{
            id: 2,
            name: 'מריוש'
        }]
    }];
    
    
    
    var inst = $('#scheduler').mobiscroll().eventcalendar({
        rtl: true,
        view: {
          timeline: {
              type: 'day',
              startDay: 0,
              endDay: 5,
              startTime: '06:00',
              endTime: '24:00',
              timeCellStep: 30,
              timeLabelStep: 30,
              weekNumbers: false
          }  
        },
        renderScheduleEvent: function (data) {
            var ev = data.original;
            var color = data.color;

            return '<div class="md-timeline-template-event" style="border-color:' + color + ';background:' + color + '">' +
                '<div class="md-timeline-template-event-cont">' +
                '<span class="md-timeline-template-title">' + ev.title + '</span>' +
                '<span class="md-timeline-template-time" style="color:' + color + ';">' + data.start + ' - ' + data.end + '</span>' +
                '<span class="md-timeline-template-title"><span class="md-timeline-template-body">מספר אוטו: ' + ev.numberOfBuses + '<span>' +
                '<span class="md-timeline-template-body"> | אנשי צוות: פאבל</span></span>' +
                '</div></div>';
        },
            onEventHoverIn: function (args, inst) {
            var event = args.event;
            //var resource = doctors.find(function (dr) { return dr.id === event.resource });
            var time = formatDate('hh:mm A', new Date(event.start)) + ' - ' + formatDate('hh:mm A', new Date(event.end));
            var button = {};

            currentEvent = event;

            $header.css('background-color', resource.color);
            $data.text(event.title + ', Age: ' + event.age);
            $time.text(time);

            $status.text(event.confirmed ? 'Confirmed' : 'Canceled');

            $statusButton.text(button.text);
            $statusButton.mobiscroll('setOptions', { color: button.type });

            clearTimeout(timer);
            timer = null;

            tooltip.setOptions({ anchor: args.domEvent.target });
            tooltip.open();
        },
        onEventHoverOut: function (args) {
            if (!timer) {
                timer = setTimeout(function () {
                    tooltip.close();
                }, 200);
            }
        },
        onEventClick: function (event, inst) {
            tooltip.open();
        }
    }).mobiscroll('getInst');;

    $.ajax({
        url: 'GetGanttData',
        type: 'GET',
        success: function (result){
            
            console.log(result);
            console.log(inst);
            inst.setEvents(result);
            inst.setOptions({
                resources: myResources
            });

            mobiscroll.toast({
                message: 'New events loaded'
            });
        }
    });
});