﻿using System.Web;
using System.Web.Optimization;
using FlightScheduling.Web.Infrastructure;
using FlightScheduling.Website.Infrastructure;

namespace FlightScheduling.Web
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
               "~/Scripts/Common/jquery-2.1.0.js",
               "~/Scripts/Common/Common.js"));


            bundles.Add(new ScriptBundle("~/bundles/jquery/validate").Include(
                "~/Scripts/Globalize/globalize.js",
                "~/Scripts/Globalize/globalize.culture.en-US.js",
                "~/Scripts/Globalize/globalize.culture.he-IL.js",
                "~/Scripts/Globalize/globalize.culture.he.js",
                "~/Scripts/Common/jquery.validate.js",
                "~/Scripts/Common/jquery.unobtrusive-ajax.js",
                "~/Scripts/Common/jquery.validate.unobtrusive.js",
                "~/Scripts/Common/jquery.validate.unobtrusive.bootstrap.js",
                "~/Scripts/Common/jquery.validate.filetypevalidation.js",
                "~/Scripts/Common/jquery.validate.israeliid.js"));


            bundles.Add(new ScriptBundle("~/bundles/colorPicker").Include(
                "~/Scripts/colorPicker/colpick.js"));

            bundles.Add(new ScriptBundle("~/bundles/noty").IncludeDirectory("~/Scripts/noty/", "*.js", true).Include(
                "~/Scripts/noty/jquery.noty.js"));


            bundles.Add(new ScriptBundle("~/bundles/chosen").Include(
                "~/Scripts/chosen/chosen.jquery.min.js",
                "~/Scripts/chosen/chosen.proto.min.js",
                "~/Scripts/chosen/chosen-methods.js"));


            //bundles.Add(new ScriptBundle("~/bundles/datepicker").Include(
            //    "~/Scripts/datepicker/bootstrap-datepicker.js",
            //    "~/Scripts/datepicker/bootstrap-datepicker.he.js",
            //    "~/Scripts/datepicker/bootstrap-datepicker-methods.js"));

            bundles.Add(new ScriptBundle("~/bundles/datepicker").NonOrdering().Include(AppConfig.BundleDatePickerScripts));

            //bundles.Add(new ScriptBundle("~/bundles/datepicker_he_IL").Include(
            //    "~/Scripts/pickadate/translations/he_IL.js"));

            //"~/Scripts/pickadate/translations/he_IL.js",
            //bundles.Add(new ScriptBundle("~/bundles/datepicker").Include(
            //    "~/Scripts/bootstrap-datepicker.js",
            //    "~/Scripts/locals/bootstrap-datepicker.he.min.js",
            //    "~/Scripts/datepicker/bootstrap-datepicker-methods.js"));


            bundles.Add(new Bundle("~/bundles/tinymce").Include(
                "~/Scripts/tinymce/tinymce.min.js",
                "~/Scripts/tinymce/plugins/autolink/plugin.min.js",
                "~/Scripts/tinymce/TinymceInit.js"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrapRTL").Include(
                "~/Scripts/Bootstrap/bootstrap-rtl.js"));

            bundles.Add(new StyleBundle("~/Content/css/bootstrapRTL").Include(
                "~/Content/css/bootstrap-rtl.css"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrapLTR").Include(
               "~/Scripts/Bootstrap/bootstrap.js"));

            bundles.Add(new StyleBundle("~/Content/css/bootstrapLTR").Include(
                "~/Content/css/bootstrap.css"));

            bundles.Add(new StyleBundle("~/Content/css/Icons").Include(
                "~/Content/css/fontawesome-free-5.12.0-web/css/all.min.css"));


            bundles.Add(new StyleBundle("~/Content/css/colorPicker").Include(
                "~/Content/css/colorPicker/colpick.css"));

            bundles.Add(new StyleBundle("~/bundles/css/chosen").Include(
                "~/Content/css/chosen/chosen.min.css",
                "~/Content/css/chosen/customized-chosen.css"));

            bundles.Add(new StyleBundle("~/Content/css/tinymce").Include(
               "~/Content/css/bootstrap-rtl.css"));

            bundles.Add(new StyleBundle("~/Content/css/checkbox-switch").Include(
               "~/Scripts/BootstrapToggle/switch.css"));

            bundles.Add(new StyleBundle("~/Content/css/datepicker").NonOrdering().Include(AppConfig.BundleDatePickerCss));

            //bundles.Add(new StyleBundle("~/Content/css/datepicker_he_IL").Include(
            //  "~/Scripts/pickadate/themes/rtl.css"));

            //bundles.Add(new StyleBundle("~/Content/css/datepicker").Include(
            //   "~/Content/css/datepicker/datepicker.css",
            //   "~/Content/css/datepicker/datepicker-he.css"));

            //bundles.Add(new StyleBundle("~/Content/css/datepicker").Include(
            //   "~/Content/css/bootstrap-datepicker3.css",
            //   "~/Content/css/datepicker/datepicker-he.css"));

            //Full calendar 
            bundles.Add(new StyleBundle("~/Bundles/Content/css/fullcalendar").Include(
               "~/Content/css/fullcalendar/fullcalendar.min.css",
               "~/Content/css/fullcalendar/fullcalendar.override.css"));

            bundles.Add(new ScriptBundle("~/bundles/Scripts/FullCalendar").Include(
                "~/Scripts/fullcalendar/lib/moment.min.js",
                "~/Scripts/fullcalendar/fullcalendar.min.js",
                "~/Scripts/fullcalendar/gcal.js",
                "~/Scripts/fullcalendar/lang-all.js"));


            //Jquery UI
            bundles.Add(new StyleBundle("~/Content/css/jQueryUI").Include(
                "~/Content/css/jQueryUI/jquery-ui.min.css",
                "~/Content/css/jQueryUI/custom-css.css"));

            bundles.Add(new ScriptBundle("~/bundles/jQueryUI").Include(
                "~/Scripts/jquery-ui-1.12.1.min.js"));


            //Switch checkbox
            bundles.Add(new ScriptBundle("~/bundles/checkbox-switch").Include(
                "~/Scripts/BootstrapToggle/switch.js",
                "~/Scripts/BootstrapToggle/switch-init.js"));

            // Increase Font Size
            bundles.Add(new StyleBundle("~/Content/css/IncreaseFontSize").Include(
                "~/Content/css/Increase-font-size.css"));
        }
    }
}
