﻿using FlightScheduling.DAL;
using FlightScheduling.DAL.CustomEntities.Permissions;
using FlightScheduling.Website.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FlightScheduling.Web.App_Start
{
    public static class PermissionLoader
    {
        public static void LoadRolesAndPermissions()
        {
            DBRepository _dbDapper = new DBRepository();
            IEnumerable<PermissionRole> Roles = _dbDapper.GetPermissionsRoles();
            HttpContext.Current.Application["Roles"] = Roles;
        }

        public static IEnumerable<PermissionBits> GetUserPermission(int RoleID)
        {
            if (HttpContext.Current.Application["Roles"] !=null)
            {
                return ((IEnumerable<PermissionRole>)HttpContext.Current.Application["Roles"]).Where(r => r.ID == RoleID).FirstOrDefault().Permissions;
            }

            return null;
        }

        public static bool CheckUserPermissions(params PermissionBitEnum[] _permissions)
        {
            bool allowProceed = false;

            if (HttpContext.Current.User.Identity.IsAuthenticated && FunctionHelpers.User != null && FunctionHelpers.User.PermissionRoleID != null)
            {
                IEnumerable<PermissionBits> _UserPermissions = PermissionLoader.GetUserPermission((int)FunctionHelpers.User.PermissionRoleID);
                //Check for permission
                bool hasPermission = !_permissions.Except(_UserPermissions.Select(p => p.PermissionBitEnum)).Any();

                allowProceed = hasPermission;

            }

            return allowProceed;
        }
    }
}