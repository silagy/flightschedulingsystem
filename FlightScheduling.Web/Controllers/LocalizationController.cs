﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FlightScheduling.Web.Controllers
{
    public class LocalizationController : Controller
    {
        //
        // GET: /Localization/
        public ActionResult ChangeCulture(string lang, string returnUrl)
        {
            var ci = new CultureInfo(lang);
            ci.DateTimeFormat.ShortDatePattern = "dd/MM/yyyy";
            ci.DateTimeFormat.LongTimePattern = "dd/MM/yyyy hh:mm";
            Session["Culture"] = ci;
            return Redirect(returnUrl);
        }
	}
}