﻿using System.Collections.ObjectModel;
using System.Web.Script.Serialization;
using Flight.Membership;
using FlightScheduling.DAL;
using FlightScheduling.Website.Infrastructure;
using FlightScheduling.Website.Models.Account;
using MvcContrib;
using Newtonsoft.Json;
using Otakim.Website.Infrastructure;
using Otakim.Website.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using System.Data;
using System.Data.Entity.Migrations;
using System.Text.RegularExpressions;
using AutoMapper;
using Flight.DAL.FlightDB.Methods;
using FlightScheduling.DAL.CustomEntities;
using FlightScheduling.Language;
using FlightScheduling.Web.Areas.Administration.Models.Messages;
using FlightScheduling.Web.Areas.Administration.Models.Schools;
using FlightScheduling.DAL.CustomEntities.Notifications;
using System.Dynamic;
using NLog;
using NLog.Config;
using Logzio.DotNet.NLog;
using FlightScheduling.Web.Models.Account;
using FlightScheduling.Web.Infrastructure;
using Endilo.Emails;
using StackExchange.Profiling.Helpers.Dapper;

namespace FlightScheduling.Website.Controllers
{
    public class AccountController : Controller
    {

        private FlightDB _db = new FlightDB();
        private DBRepository _dbDapper = new DBRepository();
        private static Logger logger = LogManager.GetCurrentClassLogger();


        protected override void Dispose(bool disposing)
        {
            _db.Dispose();
            _dbDapper.Dispose();
            base.Dispose(disposing);
        }

        [HttpGet]
        public ActionResult Login(string ReturnUrl)
        {
            LoginModel model = new LoginModel();
            model.ReturnURL = ReturnUrl;

            model.Messages = Mapper.Map<List<MessagesDetails>, List<MessagesDetailsViewModel>>(_dbDapper.GetMassagesByDates(DateTime.Today, DateTime.Today).ToList()).OrderByDescending(m => m.ActiveStartDate).ThenByDescending(m => m.UpdateDate);

            return View(model);
        }

        [HttpPost]
        public ActionResult Login(LoginModel i_LoginData)
        {
            if (ModelState.IsValid)
            {
                if (Membership.ValidateUser(i_LoginData.UserName, i_LoginData.Password))
                {
                    FormsAuthentication.SetAuthCookie(i_LoginData.UserName, i_LoginData.RememberMe);

                    //Set User Sesision
                    FlightMembershipProvider provider = new FlightMembershipProvider();
                    Users authentictedUser = provider.GetUserByEmailOrUserName(i_LoginData.UserName);

                    // Need to check if the school manager is disabled if so to redirect him to login page with error
                    if (authentictedUser.IsActivate == false || authentictedUser.IsActive == false)
                    {
                        i_LoginData.Messages = Mapper.Map<List<MessagesDetails>, List<MessagesDetailsViewModel>>(_dbDapper.GetMassagesByDates(DateTime.Today, DateTime.Today).ToList()).OrderBy(m => m.ActiveStartDate);

                        ModelState.AddModelError("SchoolManagerDisabled", AccountStrings.SchoolManagerDisabled);
                        return View(i_LoginData);
                    }

                    UserSessionModel user = FunctionHelpers.GetUserSessionModel(authentictedUser);

                    Session.Add("UserProfile", user);

                    // Create the authentication ticket with custom user data.
                    var serializer = new JavaScriptSerializer();
                    string userData = serializer.Serialize(user);

                    HttpContext.Items.Add("User", userData);

                    FormsAuthenticationTicket ticket = new FormsAuthenticationTicket(1,
                            i_LoginData.UserName,
                            DateTime.Now,
                            DateTime.Now.AddDays(30),
                            true,
                            userData,
                            FormsAuthentication.FormsCookiePath);

                    // Encrypt the ticket.
                    string encTicket = FormsAuthentication.Encrypt(ticket);

                    // Create the cookie.
                    Response.Cookies.Add(new HttpCookie(FormsAuthentication.FormsCookieName, encTicket));

                    if (Url.IsLocalUrl(i_LoginData.ReturnURL) && i_LoginData.ReturnURL.Length > 1 && i_LoginData.ReturnURL.StartsWith("/")
                        && !i_LoginData.ReturnURL.StartsWith("//") && !i_LoginData.ReturnURL.StartsWith("/\\"))
                    {
                        return Redirect(i_LoginData.ReturnURL);
                    }
                    else
                    {
                        //logger.Info($"{user.FullName} has logged in successfuly");
                        logger.Error($"{user.FullName} has logged in successfuly");
                        if (User.IsInRole(eRole.Instructor.ToString()))
                        {
                            return RedirectToAction("InstructorFolder", "Instructors", new {ID = user.InstructorID, myFile = true });
                        }
                        return RedirectToAction("GetTripsListView", "Expeditions");
                        //return RedirectToAction("GetTrips", "Expeditions");
                    }
                }
                else
                {
                    ModelState.AddModelError("", "");
                }
            }
            return View();
        }



        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            Session.Abandon();

            return RedirectToAction("GetTripsListView", "Expeditions");
        }


        public ActionResult GetSchoolBasedOnInstituteID()
        {
            SchoolUserViewModel model = new SchoolUserViewModel();

            return View(model);
        }

        [HttpPost]
        public ActionResult GetSchoolBasedOnInstituteID(SchoolUserViewModel model)
        {
            if (ModelState.IsValid)
            {

                model.School = _db.Schools.Where(s => s.InstituteID.Equals(model.InstituteID)).Select(s => new SchoolViewModel()
                {
                    ID = s.ID,
                    Email = s.Email,
                    Fax = s.Fax,
                    InstituteID = s.InstituteID,
                    IsActive = s.IsActive,
                    Manager = s.Manager,
                    ManagerEmail = s.ManagerEmail,
                    ManagerPhone = s.ManagerPhone,
                    Name = s.Name,
                    Phone = s.Phone
                }).FirstOrDefault();

                if (model.School == null)
                {
                    ModelState.AddModelError("InstituteIDNotFound", AccountStrings.InstituteIDNotFound);
                    return View("GetSchoolBasedOnInstituteID", model);
                }

                FlightMembershipProvider provider = new FlightMembershipProvider();
                Users user = provider.GetUserByEmailOrUserName(model.InstituteID.ToString());

                return View("UpdateSchoolDetails", model);
            }

            return View(model);
        }

        [HttpPost]
        public ActionResult UpdateSchoolDetails(SchoolUserViewModel model)
        {
            if (ModelState.IsValid)
            {
                //Load user data
                Users _user = _db.GetUserByLoginName(model.InstituteID.ToString());

                //Update user password
                //_user.Password = Membership.GeneratePassword(6, 1);
                //_user.Password = Regex.Replace(_user.Password, @"[^a-zA-Z0-9]", m => "9");
                _user.IsActivate = false;
                FlightMembershipProvider provider = new FlightMembershipProvider();

                _user = provider.UpdateUser(_user);

                //Update School details
                Schools _school = _db.Schools.FirstOrDefault(s => s.InstituteID == model.InstituteID);

                _school.Email = model.School.Email;
                _school.Fax = model.School.Fax;
                _school.Manager = model.School.Manager;
                _school.ManagerEmail = model.School.ManagerEmail;
                _school.ManagerPhone = model.School.ManagerPhone;
                _school.Phone = model.School.Phone;
                _school.Name = model.School.Name;
                _school.UpdateDate = DateTime.Now;

                _db.Schools.AddOrUpdate(_school);

                int results = _db.SaveChanges();

                return RedirectToAction("GetTripsListView", "Expeditions");

            }

            return View(model);
        }

        [HttpGet]
        public PartialViewResult GetUserNotification(bool includeReadNotifications = false)
        {
            DateTime Date = DateTime.Now.AddMonths(-1);

            IEnumerable<Notifications> Notifications = _dbDapper.GetRecords<Notifications>(QueriesRepository.GetUserNotificationsByDate, new
            {
                Date,
                FunctionHelpers.User.UserID,
                IsRead = includeReadNotifications
            });

            return PartialView("_UserNotifications", Notifications);
        }

        [HttpGet]
        public JsonResult GetUserNotificationCount()
        {
            if (FunctionHelpers.User != null)
            {
                DateTime Date = DateTime.Now.AddMonths(-1);

                dynamic json = new ExpandoObject();
                json.NotificationCount = _dbDapper.GetRecords<int>(QueriesRepository.GetUserNotificationCountbyDate, new
                {
                    Date,
                    FunctionHelpers.User.UserID,
                });

                return Json(JsonConvert.SerializeObject(json), JsonRequestBehavior.AllowGet);
            }

            return null;

        }

        [HttpGet]
        public JsonResult MarkNotificationAsRead(int ID)
        {
            bool results = _dbDapper.UpdateOperation(QueriesRepository.MarkNotificationAsRead, new { ID });

            dynamic json = new ExpandoObject();
            json.success = results;

            return Json(JsonConvert.SerializeObject(json), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult PasswordChangeRequest()
        {
            PasswordChangeRequestViewModel model = new PasswordChangeRequestViewModel();

            return View("PasswordChangeRequest", model);
        }

        [HttpPost]
        public ActionResult PasswordChangeRequest(PasswordChangeRequestViewModel model)
        {
            if (ModelState.IsValid)
            {
                FlightMembershipProvider provider = new FlightMembershipProvider();

                Users authentictedUser = provider.GetUserByEmailOrUserName(model.UserName);

                if (authentictedUser != null && authentictedUser.ID > -1)
                {
                    model.ID = Guid.NewGuid().ToString();
                    model.HashedID = CryptoHelpers.EncryptPassword(model.ID.ToString(), CryptoHelpers.k_SHA256CryptoAlgorithm);
                    model.RequestDate = DateTime.Now;
                    model.UserID = authentictedUser.ID;

                    bool results = _dbDapper.UpdateOperation(QueriesRepository.InsertNewPasswordChangeRequest, new { ID = model.HashedID, model.RequestDate, model.UserID });
                    if (authentictedUser.SchoolID != null)
                    {
                        SchoolViewModel _school = _dbDapper.GetRecords<SchoolViewModel>(QueriesRepository.GetSchoolManagerDetailsBySchoolID, new { SchoolID = authentictedUser.SchoolID }).FirstOrDefault();
                        authentictedUser.Email = _school.ManagerEmail;
                        authentictedUser.Firstname = _school.Manager;
                        authentictedUser.Lastname = "";
                    }

                    if (results)
                    {
                        string ForgetPasswordURL = String.Format("{0}{1}", AppConfig.GetSiteRootUrl, Url.Action("ResetPassword", "Account", new { UID = model.ID }));
                        //Need to send email to the user
                        string str = String.Format(@"<p style='direction: rtl;'>שלום רב {0},</p>
                                                        <p style='direction: rtl;'>ביקשת לשנות את הסיסמה באתר הומלנד.</p>
                                                        <p style='direction: rtl;'>יש ללחוץ על הקישור המצורף כדי לשנות את הסיסמה.</p>
                                                        <p style='direction: rtl;'>{1}</p>
                                                        <p style='direction: rtl;'>כאן בשבילך,</p>
                                                        <p style='direction: rtl;'>מערכת הומלנד</p>", String.Format("{0} {1}", authentictedUser.Firstname, authentictedUser.Lastname), ForgetPasswordURL);

                        try
                        {
                            SendMail.SendSimpleEmail(authentictedUser.Email, "שינוי סיסמה", str);
                            return View("PasswordChangeRequestSent", authentictedUser);
                        }
                        catch (Exception ex)
                        {

                        }
                    }
                }
            }
            return View("PasswordChangeRequest", model);
        }

        [HttpGet]
        public ActionResult ResetPassword(string UID)
        {
            // Recieve the record of the user
            string HashedID = CryptoHelpers.EncryptPassword(UID.ToString(), CryptoHelpers.k_SHA256CryptoAlgorithm);
            PasswordChangeRequestViewModel _passChangeRequest = _dbDapper.GetRecords<PasswordChangeRequestViewModel>(QueriesRepository.GetPasswordChangeRequest, new { UserID = HashedID }).FirstOrDefault();

            ResetPasswordViewModel model = new ResetPasswordViewModel();
            model.UserID = _passChangeRequest.UserID;
            // Check if the request date is not valid, meaning it is older than 24 hours
            if (_passChangeRequest.RequestDate < DateTime.Now.AddDays(-1))
            {
                model.TokenExpired = true;
            }

            model.UserID = _passChangeRequest.UserID;
            return View("ResetPassword", model);
        }

        [HttpPost]
        public ActionResult ResetPassword(ResetPasswordViewModel model)
        {
            ModelState.Remove("UserID");
            if (ModelState.IsValid)
            {
                string newPassword = CryptoHelpers.EncryptPassword(model.Password, CryptoHelpers.k_SHA256CryptoAlgorithm);

                bool results = _dbDapper.UpdateOperation(QueriesRepository.UpdateUserPassword, new { Password = newPassword, model.UserID });

               if (results)
                {
                    return View("ResetPasswordSuccessful");
                }

            }
            return View("ResetPasswordFailed");
        }
    }
}