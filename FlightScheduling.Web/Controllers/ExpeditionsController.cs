﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Endilo.Helpers;
using Flight.DAL.FlightDB.Methods;
using FlightScheduling.DAL;
using FlightScheduling.Web.Areas.Administration.Models.Common;
using FlightScheduling.Web.Models.Expeditions;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Security.Cryptography.X509Certificates;
using FlightScheduling.Language;
using FlightScheduling.Web.Areas.Administration.Models.Flights;
using FlightScheduling.Web.Areas.Administration.Models.Trips;
using FlightScheduling.Web.Infrastructure;
using FlightScheduling.Website.Models.Account;
using Otakim.Website.Infrastructure;
using FlightScheduling.Website.Infrastructure;
using FlightScheduling.DAL.CustomEntities.Permissions;
using Endilo.ExportToExcel;
using Newtonsoft.Json;
using FlightScheduling.Web.Areas.Administration.Models.Schools;
using FlightScheduling.DAL.CustomEntities.Trip;

namespace FlightScheduling.Web.Controllers
{
    [Authorize]
    public class ExpeditionsController : Controller
    {

        private FlightDB _db = new FlightDB();
        DBRepository _dbDapper = new DBRepository();

        protected override void Dispose(bool disposing)
        {
            _db.Dispose();
            base.Dispose(disposing);
        }
        


        public ActionResult GetTripsListView(DateTime? StartDate, DateTime? EndDate)
        {
            if (StartDate == null)
            {
                StartDate = SystemConfigurationRepository.ExpeditionPublishStartDate < DateTime.Now ? DateTime.Now : SystemConfigurationRepository.ExpeditionPublishStartDate;
            }

            if (EndDate == null)
            {
                EndDate = SystemConfigurationRepository.ExpeditionPublishEndDate;
            }

            // Check if the user read the instructions
            if (Session["ReadOrderTripInstructions"] == null)
            {
                Session.Add("ReadOrderTripInstructions", false);
            }

            TripAvilabilityViewModel model = new TripAvilabilityViewModel();

            //Get all trips in the date range
            model.TripsAvilability = _dbDapper.GetRecords<TripsAvilibility>(QueriesRepository.GetTripsInDateRange, new { StartDate, EndDate }).OrderBy(t => t.DepartureDate).ToList();

            //Get Trips Buses Per Day
            List<TripBusesPerDay> _tripBusesPerDay = _dbDapper.GetRecords<TripBusesPerDay>(QueriesRepository.GetTripBusesPerDay, new { StartDate, EndDate }).ToList();

            // The first departure date
            DateTime _date = DateTime.Now;
            if (_tripBusesPerDay != null && _tripBusesPerDay.Count() > 0)
            {
                _date = _tripBusesPerDay.Min(t => t.DepartureDate);
            }

            //The last returning date
            DateTime _LastTripDate = DateTime.Now;
            if (_tripBusesPerDay != null && _tripBusesPerDay.Count() > 0)
            {
                _LastTripDate = _tripBusesPerDay.Max(t => t.ReturningDate);
            }

            //This dictionary is used to calculate the number of buses per day
            Dictionary<DateTime, int> _busesPerDay = new Dictionary<DateTime, int>();
            while (_date <= _LastTripDate)
            {
                var tryd = _tripBusesPerDay.Where(t => t.DepartureDate <= _date && t.ReturningDate >= _date);
                int TotalBuses = _tripBusesPerDay.Where(t => t.DepartureDate <= _date && t.ReturningDate >= _date).Sum(t => t.BasesPerDay);
                _busesPerDay.Add(_date, TotalBuses);
                _date = _date.AddDays(1);
            }

            //Total Paggengers
            List<TotalPassengersPerTrip> TotalPassengersPerTrip = _dbDapper.GetRecords<TotalPassengersPerTrip>(QueriesRepository.GetTotalPaggengersForTrips, new { StartDate, EndDate }).ToList();

            //Get Booked Passengers per trip
            List<TripBookedPassengers> _bookedPassengers = _dbDapper.GetRecords<TripBookedPassengers>(QueriesRepository.GetTripBookedPassengers, new { StartDate, EndDate }).ToList();

            foreach (var trip in model.TripsAvilability)
            {
                // Enter the maximum number of buses in the trip date range
                if (_busesPerDay.Any(b => b.Key >= trip.DepartureDate && b.Key < trip.ReturningDate))
                {
                    trip.NumberOfBuses = _busesPerDay.Where(b => b.Key >= trip.DepartureDate && b.Key <= trip.ReturningDate).Max(t => t.Value);
                }

                // Enter the booked passengers for the trip
                if (_bookedPassengers.Any(t => t.TripID == trip.TripID))
                {
                    trip.BookedPassengers = _bookedPassengers.FirstOrDefault(t => t.TripID == trip.TripID).BookedPassengers;
                }

                // Enter the total passengers
                if (TotalPassengersPerTrip.Any(t => t.ID == trip.TripID))
                {
                    trip.TotalPassengers = TotalPassengersPerTrip.Where(t => t.ID == trip.TripID).FirstOrDefault().TotalPassensgers;
                }
            }
            model.TripsAvilability = model.TripsAvilability.OrderBy(t => t.DepartureDate).ToList();
            //model.TripsAvilability = _dbDapper.GetRecords<TripsAvilibility>(QueriesRepository.GetTripsWithAvailablilityInDateRange, new { StartDate, EndDate }).OrderBy(t => t.DepartureDate).ToList();
            if (model.TripsAvilability.Count() > 0)
            {
                model.Months = FunctionHelpers.MonthsBetween(model.TripsAvilability.Min(t => t.DepartureDate), model.TripsAvilability.Max(t => t.DepartureDate)).ToList();
            }

            return View("TripsOrder", model);
        }

        public ActionResult GetTrips(int? Months = 2)
        {
            SelectTripsViewModel model = new SelectTripsViewModel();

            // Getting the system configuration settings
            DateTime StartDate = SystemConfigurationRepository.ExpeditionPublishStartDate;
            DateTime EndDate = SystemConfigurationRepository.ExpeditionPublishEndDate;

            //Returninig the user to the last visited month
            //if (Session["LastVisitedMonth"] != null)
            //{

            //    if ((DateTime)Session["LastVisitedMonth"] > StartDate)
            //    {
            //        StartDate = (DateTime)Session["LastVisitedMonth"];
            //    }
            //}

            //Get 
            List<Trips> trips =
                _db.Trips.Include(t => t.Expeditions).Where(
                       t =>
                       DbFunctions.TruncateTime(t.DepartureDate) >= StartDate &&
                       DbFunctions.TruncateTime(t.DepartureDate) <= EndDate).ToList();

            //List<Trips> trips =
            //   _db.Trips.Include(t => t.Expeditions).ToList();

            model.FullCalendar.events = trips.Select(t => new FullCalendarEvent()
            {
                id = t.ID.ToString(),
                //backgroundColor = "#E5E5E5",
                rendering = EventRendering.background.ToString(),
                start = t.DepartureDate.ToString("s"),
                end = t.DepartureDate.ToString("s"),
                //color = "#ff9f89",
            }).ToList();


            var jsonSerialiser = new JavaScriptSerializer();
            model.eventsJSON = jsonSerialiser.Serialize(model.FullCalendar.events);

            return View(model);
        }

        public JsonResult GetTripsAsEvents(string Month, string Year)
        {
            SelectTripsViewModel model = new SelectTripsViewModel();
            DateTime date = DateTime.Parse(Month + "/" + Year);

            // Getting the system configuration settings
            DateTime StartDate = SystemConfigurationRepository.ExpeditionPublishStartDate;
            DateTime EndDate = SystemConfigurationRepository.ExpeditionPublishEndDate;


            if (date < StartDate)
            {
                date = StartDate;
            }

            //Store last visited month
            Session.Add("LastVisitedMonth", date);

            List<Trips> trips =
               _db.Trips.Include(t => t.Expeditions).Where(t => DbFunctions.TruncateTime(t.DepartureDate) >= date && DbFunctions.TruncateTime(t.DepartureDate) <= EndDate && t.IsActive).ToList();

            var events = trips.Select(t => new
            {
                id = t.ID.ToString(),
                rendering = "background",
                start = t.DepartureDate.ToString("yyyy-MM-dd"),
                end = t.DepartureDate.ToString("yyyy-MM-dd"),
                color = "#006AB3",
            }).ToList();


            return Json(events.ToArray(), JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetTripsForDate(DateTime date)
        {
            TripsViewModel model = new TripsViewModel();

            // Getting the system configuration settings
            DateTime StartDate = SystemConfigurationRepository.ExpeditionPublishStartDate;
            DateTime EndDate = SystemConfigurationRepository.ExpeditionPublishEndDate;

            model.TripViewModelBasics =
                _db.Trips.RemoveInActive().Where(t => (t.DepartureDate >= date && t.DepartureDate <= EndDate) && t.DepartureDate == date).Select(t => new TripViewModelBasic()
                {
                    ID = t.ID,
                    Comments = t.Comments,
                    CreationDate = t.CreationDate,
                    DepartureDate = t.DepartureDate,
                    Destenation = (FlightViewModel.FlightsFields)t.DestinationID,
                    ReturnDate = t.ReturningDate,
                    ReturnedFrom = (FlightViewModel.FlightsFields)t.ReturingField
                }).ToList();

            return View(model);
        }

        [EndiloAuthorization(eRole.Administrator, eRole.Staf, eRole.Agent, eRole.SchoolManager)]
        public ActionResult AddExpedition(int ID)
        {
            ExpeditionViewModel model = new ExpeditionViewModel();

            model.TripDetails = _db.Trips.Where(t => t.ID.Equals(ID)).Select(t => new TripViewModelBasic()
            {
                ID = t.ID,
                Comments = t.Comments,
                CreationDate = t.CreationDate,
                DepartureDate = t.DepartureDate,
                Destenation = (FlightViewModel.FlightsFields)t.DestinationID,
                ReturnDate = t.ReturningDate,
                ReturnedFrom = (FlightViewModel.FlightsFields)t.ReturingField,
                UpdatenDate = t.UpdateDate
            }).FirstOrDefault();

            Trips trip =
                _db.Trips.Include(t => t.Flights).Include(t => t.Expeditions).Include("Expeditions.ExpeditionsFlights").FirstOrDefault(t => t.ID.Equals(ID));

            model.TotalPassengers =
                trip.Flights.Where(f => f.DepartureFieldID.Equals((int)FlightViewModel.FlightsFields.Israel))
                    .Sum(f => f.NumberOfPassengers);


            var expeditions = trip.Expeditions.Where(
                e =>
                (e.Status.Equals((int)eExpeditionStatus.Approved) ||
                 e.Status.Equals((int)eExpeditionStatus.InProcess)) &&
                e.ExpeditionsFlights.Any(
                    f => f.Flights.DepartureFieldID.Equals((int)(FlightViewModel.FlightsFields.Israel)))).Select(e => new
                    {
                        Booked = e.ExpeditionsFlights.Count() != 0 ? e.ExpeditionsFlights.Where(f => f.Flights.DepartureFieldID.Equals((int)(FlightViewModel.FlightsFields.Israel))).Sum(f => f.NumberOfPassengers) : 0
                    });

            model.PotentialPassengers = expeditions.Sum(e => e.Booked);


            int Numberofdays = Convert.ToInt16((trip.ReturningDate - trip.DepartureDate).TotalDays);

            #region Calculate Number Of buses Per Day
            // I have changed this line to calculate the number of buses per day from the store procedure instead on what i worte in the code
            model.BusPerDays = _db.BusesPerDay(trip.DepartureDate, trip.ReturningDate).Select(b => new BusPerDay()
            {
                Day = (DateTime)b.Date,
                NumOdBuses = (int)b.BusesPerDay,
                AvailableBuses = AppConfig.MaxBusesPerDay - (int)b.BusesPerDay
            }).ToList();

            //for (int i = 0; i < Numberofdays; i++)
            //{
            //    BusPerDay busPerDay = new BusPerDay();
            //    busPerDay.Day = trip.DepartureDate.AddDays(i);

            //    busPerDay.NumOdBuses =
            //        _db.Trips.Include(t => t.Expeditions)
            //            .Where(t => busPerDay.Day >= t.DepartureDate && busPerDay.Day < t.ReturningDate)
            //            .Sum(
            //                t =>
            //                    (int?)t.Expeditions.Where(e => e.Status == (int)eExpeditionStatus.Approved || e.Status == (int)eExpeditionStatus.InProcess)
            //                        .Sum(e => (int?)e.BasesPerDay) ?? 0);

            //    busPerDay.AvailableBuses = 56 - busPerDay.NumOdBuses;

            //    model.BusPerDays.Add(busPerDay);
            //}

            model.MinimumBusesForTrip = model.BusPerDays.Min(b => b.AvailableBuses);
            #endregion

            model.Schools = _db.Schools.Select(s => new SchoolSelectList()
            {
                ID = s.ID,
                Name = s.InstituteID + " | " + s.Name
            }).ToList();

            if (Session["UserProfile"] != null)
            {
                UserSessionModel _user = (UserSessionModel)Session["UserProfile"];
                if (_user.RoleID == (int)eRole.SchoolManager)
                {
                    int instituteID = Convert.ToInt32(_user.Email);
                    model.SchoolID = _db.Schools.FirstOrDefault(s => s.InstituteID.Equals(instituteID)).ID;
                    model.DisableSchoolSelection = true;

                    //TODO: To check if the school can go on that expedition
                    List<SchoolExpeditionsViewModel> _previousExpeditions =
                        _dbDapper.GetRecords<SchoolExpeditionsViewModel>(
                            QueriesRepository.GetSchoolPreviousExpeditions,
                            new { model.SchoolID, TripID = model.ID, MinDate = DateTime.Now.AddYears(-AppConfig.PassYearToCheck) }).ToList();
                    for (int year = 1; year < AppConfig.PassYearToCheck + 1; year++)
                    {


                        if (_previousExpeditions.Any(o => o.MinDateLimited <= model.TripDetails.DepartureDate.AddYears(-year) && o.MaxDateLimited >= model.TripDetails.DepartureDate.AddYears(-year)) && !model.DisableOrder)
                        {
                            model.DisableOrder = true;
                            //TODO: TO add the details of the expedition due to the school can't order this trip.
                            var results =
                            _previousExpeditions.Where(
                                o =>
                                    o.MinDateLimited <= model.TripDetails.DepartureDate.AddYears(-year) &&
                                    o.MaxDateLimited >= model.TripDetails.DepartureDate.AddYears(-year));
                        }
                    }

                }
            }

            model.AgenciesList = _db.Agencies.RemoveInActive().Select(a => new EntitySelectList()
            {
                ID = a.ID,
                Name = a.Name
            }).OrderBy(a => a.Name).ToList();

            return View(model);
            //f.ExpeditionsFlights.Count() != 0 ? f.ExpeditionsFlights.Sum(e => e.NumberOfPassengers) : f.NumberOfPassengers
        }

        [PermissionAuthorization(PermissionBitEnum.Form180_MoveForm)]
        public ActionResult AddQuickExpedition(int? GroupID)
        {
            QuickExpeditionViewModel model = new QuickExpeditionViewModel();

            if (GroupID != null)
            {
                model = _dbDapper.GetRecords<QuickExpeditionViewModel>(QueriesRepository.GetOrderForClonning, new { GroupID }).FirstOrDefault();
                model.GroupID = GroupID;
            }

            model.Schools = _db.Schools.Select(s => new SchoolSelectList()
            {
                ID = s.ID,
                Name = s.InstituteID + " | " + s.Name
            }).ToList();

            model.AgenciesList = _db.Agencies.RemoveInActive().Select(a => new EntitySelectList()
            {
                ID = a.ID,
                Name = a.Name
            }).OrderBy(a => a.Name).ToList();

            return View("AddQuickExpedition", model);
        }

        [PermissionAuthorization(PermissionBitEnum.Form180_MoveForm)]
        [HttpPost]
        public ActionResult AddQuickExpedition(QuickExpeditionViewModel model)
        {
            bool redirectToMoveOrder = model.GroupID != null;
            ModelState.Remove("SelectedWeek");
            if (ModelState.IsValid)
            {
                #region Adding Expedition
                Expeditions _expeditionToAdd = new Expeditions();

                //Getting School Details
                SchoolViewModel school = _dbDapper.GetRecords<SchoolViewModel>(QueriesRepository.GetSchoolByID, new { ID = model.SchoolID }).FirstOrDefault();

                _expeditionToAdd.ExpeditionStatusEnum = eExpeditionStatus.Approved;
                _expeditionToAdd.IsActive = true;
                _expeditionToAdd.Comments = String.Empty;
                _expeditionToAdd.CreationDate = DateTime.Now;
                _expeditionToAdd.Manager = school.Manager;
                _expeditionToAdd.ManagerCell = school.ManagerPhone;
                _expeditionToAdd.ManagerEmail = school.ManagerEmail;
                _expeditionToAdd.ManagerPhone = school.ManagerPhone;
                _expeditionToAdd.NumberOFpassengers = model.NumOfPassangersRequired;
                _expeditionToAdd.UserID = FunctionHelpers.User.UserID;

                _expeditionToAdd.SchoolID = model.SchoolID;
                _expeditionToAdd.TripID = model.TripId;

                _expeditionToAdd.BasesPerDay = model.BusesRequiredPerDay;

                _expeditionToAdd.AgencyID = model.AgencyID;
                #endregion

                #region Add Passengers to Trip 
                Trips trip = _dbDapper.GetRecords<Trips>(QueriesRepository.GetTripById, new { ID = model.TripId }).FirstOrDefault();
                //Get Trip Flights
                trip.Flights = new List<Flights>();
                trip.Flights = _dbDapper.GetRecords<Flights>(QueriesRepository.GetTripFlightsByTripID, new { TripID = model.TripId });

                TripFlightAvilability tripFlightAvailablility = _dbDapper.GetRecords<TripFlightAvilability>(QueriesRepository.GetTripFlightsAvailability, new { TripID = trip.ID }).FirstOrDefault();

                #endregion

                model.TransactionResult = _dbDapper.AddQuickExpedition(_expeditionToAdd, trip, tripFlightAvailablility);

                if (model.TransactionResult.Result == false)
                {
                    model.Schools = _db.Schools.Select(s => new SchoolSelectList()
                    {
                        ID = s.ID,
                        Name = s.InstituteID + " | " + s.Name
                    }).ToList();

                    model.AgenciesList = _db.Agencies.RemoveInActive().Select(a => new EntitySelectList()
                    {
                        ID = a.ID,
                        Name = a.Name
                    }).OrderBy(a => a.Name).ToList();

                    model.BuildWeeks();

                    return View("AddQuickExpedition", model);

                }

                if (redirectToMoveOrder)
                {
                    return RedirectToAction("MoveOrderSelectTripStep", "Groups", new { GroupID = model.GroupID, area = "Administration" });
                }
                else
                {
                    return RedirectToAction("AddQuickExpedition");
                }

            }

            return RedirectToAction("AddQuickExpedition");
        }

        public JsonResult GetTripsInWeek(DateTime startDate)
        {
            DateTime endDate = startDate.AddDays(7);
            List<EntitySelectList> json = _dbDapper.GetRecords<TripsAvilibility>(QueriesRepository.GetTripsInDateRange, new { StartDate = startDate, EndDate = endDate })
                .Select(v => new EntitySelectList()
                {
                    ID = v.TripID,
                    Name = $"{v.TripID} | {v.DestinationEnum.GetDescription()} - {v.ReturingFieldEnum.GetDescription()} | {v.DepartureDate.ToString("ddd")}"
                }).ToList();




            return Json(JsonConvert.SerializeObject(json), JsonRequestBehavior.AllowGet);
        }

        [EndiloAuthorization(eRole.Administrator, eRole.Staf, eRole.Agent, eRole.SchoolManager)]
        [HttpPost]
        public ActionResult AddExpedition(ExpeditionViewModel model)
        {

            if (ModelState.IsValid && model.BusesRequiredPerDay <= model.MinimumBusesForTrip)
            {
                Users _user = _db.GetUserByLoginName(User.Identity.Name);

                Expeditions expedition = new Expeditions();

                expedition.ExpeditionStatusEnum = eExpeditionStatus.PendingApproval;
                expedition.IsActive = true;
                expedition.Comments = model.Comments;
                expedition.CreationDate = DateTime.Now;
                expedition.Manager = model.ExpeditionManager;
                expedition.ManagerCell = model.ExpeditionManagerCell;
                expedition.ManagerEmail = model.ExpeditionManagerEmail;
                expedition.ManagerPhone = model.ExpeditionManagerPhone;
                expedition.NumberOFpassengers = model.NumOfPassangersRequired;
                expedition.UserID = _user.ID;

                expedition.SchoolID = model.SchoolID;
                expedition.TripID = model.TripDetails.ID;

                expedition.BasesPerDay = model.BusesRequiredPerDay;

                expedition.AgencyID = model.AgencyID;

                _db.Expeditions.Add(expedition);

                int results = _db.SaveChanges();

                return View("ExpeditionConfirmation", expedition);
            }

            if (model.BusesRequiredPerDay > model.MinimumBusesForTrip)
            {
                ModelState.AddModelError("MinimumBusesPerDay", ExpeditionStrings.MinimumBusesPerDay);
            }

            model.TripDetails = _db.Trips.Where(t => t.ID.Equals(model.TripDetails.ID)).Select(t => new TripViewModelBasic()
            {
                ID = t.ID,
                Comments = t.Comments,
                CreationDate = t.CreationDate,
                DepartureDate = t.DepartureDate,
                Destenation = (FlightViewModel.FlightsFields)t.DestinationID,
                ReturnDate = t.ReturningDate,
                ReturnedFrom = (FlightViewModel.FlightsFields)t.ReturingField,
                UpdatenDate = t.UpdateDate
            }).FirstOrDefault();

            Trips trip =
                _db.Trips.Include(t => t.Flights).Include(t => t.Expeditions).Include("Expeditions.ExpeditionsFlights").FirstOrDefault(t => t.ID.Equals(model.TripDetails.ID));

            model.TotalPassengers =
                trip.Flights.Where(f => f.DepartureFieldID.Equals((int)FlightViewModel.FlightsFields.Israel))
                    .Sum(f => f.NumberOfPassengers);


            var expeditions = trip.Expeditions.Where(
                e =>
                (e.Status.Equals((int)eExpeditionStatus.Approved) ||
                 e.Status.Equals((int)eExpeditionStatus.InProcess)) &&
                e.ExpeditionsFlights.Any(
                    f => f.Flights.DepartureFieldID.Equals((int)(FlightViewModel.FlightsFields.Israel)))).Select(e => new
                    {
                        Booked = e.ExpeditionsFlights.Count() != 0 ? e.ExpeditionsFlights.Where(f => f.Flights.DepartureFieldID.Equals((int)(FlightViewModel.FlightsFields.Israel))).Sum(f => f.NumberOfPassengers) : 0
                    });

            model.PotentialPassengers = expeditions.Sum(e => e.Booked);


            int Numberofdays = Convert.ToInt16((trip.ReturningDate - trip.DepartureDate).TotalDays);

            for (int i = 0; i < Numberofdays; i++)
            {
                BusPerDay busPerDay = new BusPerDay();
                busPerDay.Day = trip.DepartureDate.AddDays(i);

                busPerDay.NumOdBuses =
                    _db.Trips.Include(t => t.Expeditions)
                        .Where(t => busPerDay.Day >= t.DepartureDate && busPerDay.Day < t.ReturningDate)
                        .Sum(
                            t =>
                                (int?)t.Expeditions.Where(e => e.Status == (int)eExpeditionStatus.Approved || e.Status == (int)eExpeditionStatus.InProcess)
                                    .Sum(e => (int?)e.BasesPerDay) ?? 0);

                busPerDay.AvailableBuses = 56 - busPerDay.NumOdBuses;

                model.BusPerDays.Add(busPerDay);
            }

            model.Schools = _db.Schools.Select(s => new SchoolSelectList()
            {
                ID = s.ID,
                Name = s.InstituteID + " | " + s.Name
            }).ToList();

            if (Session["UserProfile"] != null)
            {
                UserSessionModel _user = (UserSessionModel)Session["UserProfile"];
                if (_user.RoleID == (int)eRole.SchoolManager)
                {
                    int instituteID = Convert.ToInt32(_user.Email);
                    model.SchoolID = _db.Schools.FirstOrDefault(s => s.InstituteID.Equals(instituteID)).ID;
                    model.DisableSchoolSelection = true;
                }
            }

            model.AgenciesList = _db.Agencies.RemoveInActive().Select(a => new EntitySelectList()
            {
                ID = a.ID,
                Name = a.Name
            }).OrderBy(a => a.Name).ToList();

            return View(model);


        }

        [EndiloAuthorization(eRole.Administrator, eRole.Staf, eRole.Agent, eRole.SchoolManager)]
        public ActionResult EditExpedition(int ID)
        {
            ExpeditionViewModel model = new ExpeditionViewModel();

            var _expedition = _db.Expeditions.FirstOrDefault(e => e.ID == ID);

            model.ID = _expedition.ID;
            model.AgencyID = _expedition.AgencyID;
            model.BusesRequiredPerDay = (int)_expedition.BasesPerDay;
            model.NumOfPassangersRequired = _expedition.NumberOFpassengers;
            model.Comments = _expedition.Comments;
            model.ExpeditionManager = _expedition.Manager;
            model.ExpeditionManagerCell = _expedition.ManagerCell;
            model.ExpeditionManagerEmail = _expedition.ManagerEmail;
            model.ExpeditionManagerPhone = _expedition.ManagerPhone;
            model.SchoolID = (int)_expedition.SchoolID;

            int TripID = _expedition.TripID;

            model.TripDetails = _db.Trips.Where(t => t.ID.Equals(TripID)).Select(t => new TripViewModelBasic()
            {
                ID = t.ID,
                Comments = t.Comments,
                CreationDate = t.CreationDate,
                DepartureDate = t.DepartureDate,
                Destenation = (FlightViewModel.FlightsFields)t.DestinationID,
                ReturnDate = t.ReturningDate,
                ReturnedFrom = (FlightViewModel.FlightsFields)t.ReturingField,
                UpdatenDate = t.UpdateDate
            }).FirstOrDefault();

            Trips trip =
               _db.Trips.Include(t => t.Flights).Include(t => t.Expeditions).Include("Expeditions.ExpeditionsFlights").FirstOrDefault(t => t.ID.Equals(TripID));

            model.TotalPassengers =
                trip.Flights.Where(f => f.DepartureFieldID.Equals((int)FlightViewModel.FlightsFields.Israel))
                    .Sum(f => f.NumberOfPassengers);


            var expeditions = trip.Expeditions.Where(
                e =>
                (e.Status.Equals((int)eExpeditionStatus.Approved) ||
                 e.Status.Equals((int)eExpeditionStatus.InProcess)) &&
                e.ExpeditionsFlights.Any(
                    f => f.Flights.DepartureFieldID.Equals((int)(FlightViewModel.FlightsFields.Israel)))).Select(e => new
                    {
                        Booked = e.ExpeditionsFlights.Count() != 0 ? e.ExpeditionsFlights.Where(f => f.Flights.DepartureFieldID.Equals((int)(FlightViewModel.FlightsFields.Israel))).Sum(f => f.NumberOfPassengers) : 0
                    });

            model.PotentialPassengers = expeditions.Sum(e => e.Booked);


            int Numberofdays = Convert.ToInt16((trip.ReturningDate - trip.DepartureDate).TotalDays);

            #region Calculate Number Of buses Per Day
            // I have changed this line to calculate the number of buses per day from the store procedure instead on what i worte in the code
            model.BusPerDays = _db.BusesPerDay(trip.DepartureDate, trip.ReturningDate).Select(b => new BusPerDay()
            {
                Day = (DateTime)b.Date,
                NumOdBuses = (int)b.BusesPerDay,
                AvailableBuses = AppConfig.MaxBusesPerDay - (int)b.BusesPerDay
            }).ToList();

            //for (int i = 0; i < Numberofdays; i++)
            //{
            //    BusPerDay busPerDay = new BusPerDay();
            //    busPerDay.Day = trip.DepartureDate.AddDays(i);

            //    busPerDay.NumOdBuses =
            //        _db.Trips.Include(t => t.Expeditions)
            //            .Where(t => busPerDay.Day >= t.DepartureDate && busPerDay.Day < t.ReturningDate)
            //            .Sum(
            //                t =>
            //                    (int?)t.Expeditions.Where(e => e.Status == (int)eExpeditionStatus.Approved || e.Status == (int)eExpeditionStatus.InProcess)
            //                        .Sum(e => (int?)e.BasesPerDay) ?? 0);

            //    busPerDay.AvailableBuses = 56 - busPerDay.NumOdBuses;

            //    model.BusPerDays.Add(busPerDay);
            //}

            model.MinimumBusesForTrip = model.BusPerDays.Min(b => b.AvailableBuses);
            #endregion

            model.Schools = _db.Schools.Select(s => new SchoolSelectList()
            {
                ID = s.ID,
                Name = s.InstituteID + " | " + s.Name
            }).ToList();

            if (Session["UserProfile"] != null)
            {
                UserSessionModel _user = (UserSessionModel)Session["UserProfile"];
                if (_user.RoleID == (int)eRole.SchoolManager)
                {
                    int instituteID = Convert.ToInt32(_user.Email);
                    model.SchoolID = _db.Schools.FirstOrDefault(s => s.InstituteID.Equals(instituteID)).ID;
                    model.DisableSchoolSelection = true;
                }
            }

            model.AgenciesList = _db.Agencies.RemoveInActive().Select(a => new EntitySelectList()
            {
                ID = a.ID,
                Name = a.Name
            }).OrderBy(a => a.Name).ToList();

            ModelState.Remove("NumOfPassangersRequired");
            ModelState.Remove("BusesRequiredPerDay");

            return View(model);
        }

        [EndiloAuthorization(eRole.Administrator, eRole.Staf, eRole.Agent, eRole.SchoolManager)]
        [HttpPost]
        public ActionResult EditExpedition(ExpeditionViewModel model)
        {
            ModelState.Remove("NumOfPassangersRequired");
            ModelState.Remove("BusesRequiredPerDay");

            if (ModelState.IsValid)
            {

                Users _user = _db.GetUserByLoginName(User.Identity.Name);

                Expeditions expedition = _db.Expeditions.FirstOrDefault(e => e.ID == model.ID);

                //expedition.ExpeditionStatusEnum = eExpeditionStatus.PendingApproval; // Revoked 
                expedition.IsActive = true;
                expedition.Comments = model.Comments;
                expedition.CreationDate = DateTime.Now;
                expedition.Manager = model.ExpeditionManager;
                expedition.ManagerCell = model.ExpeditionManagerCell;
                expedition.ManagerEmail = model.ExpeditionManagerEmail;
                expedition.ManagerPhone = model.ExpeditionManagerPhone;
                //expedition.NumberOFpassengers = model.NumOfPassangersRequired;
                expedition.UserID = _user.ID;

                expedition.SchoolID = model.SchoolID;
                expedition.TripID = model.TripDetails.ID;

                //expedition.BasesPerDay = model.BusesRequiredPerDay;

                expedition.AgencyID = model.AgencyID;

                _db.Expeditions.AddOrUpdate(expedition);

                int results = _db.SaveChanges();

                return RedirectToAction("EditExpedition", new { ID = model.ID });
            }


            model.TripDetails = _db.Trips.Where(t => t.ID.Equals(model.TripDetails.ID)).Select(t => new TripViewModelBasic()
            {
                ID = t.ID,
                Comments = t.Comments,
                CreationDate = t.CreationDate,
                DepartureDate = t.DepartureDate,
                Destenation = (FlightViewModel.FlightsFields)t.DestinationID,
                ReturnDate = t.ReturningDate,
                ReturnedFrom = (FlightViewModel.FlightsFields)t.ReturingField,
                UpdatenDate = t.UpdateDate
            }).FirstOrDefault();

            Trips trip =
                _db.Trips.Include(t => t.Flights).Include(t => t.Expeditions).Include("Expeditions.ExpeditionsFlights").FirstOrDefault(t => t.ID.Equals(model.TripDetails.ID));

            model.TotalPassengers =
                trip.Flights.Where(f => f.DepartureFieldID.Equals((int)FlightViewModel.FlightsFields.Israel))
                    .Sum(f => f.NumberOfPassengers);


            var expeditions = trip.Expeditions.Where(
                e =>
                (e.Status.Equals((int)eExpeditionStatus.Approved) ||
                 e.Status.Equals((int)eExpeditionStatus.InProcess)) &&
                e.ExpeditionsFlights.Any(
                    f => f.Flights.DepartureFieldID.Equals((int)(FlightViewModel.FlightsFields.Israel)))).Select(e => new
                    {
                        Booked = e.ExpeditionsFlights.Count() != 0 ? e.ExpeditionsFlights.Where(f => f.Flights.DepartureFieldID.Equals((int)(FlightViewModel.FlightsFields.Israel))).Sum(f => f.NumberOfPassengers) : 0
                    });

            model.PotentialPassengers = expeditions.Sum(e => e.Booked);


            int Numberofdays = Convert.ToInt16((trip.ReturningDate - trip.DepartureDate).TotalDays);

            for (int i = 0; i < Numberofdays; i++)
            {
                BusPerDay busPerDay = new BusPerDay();
                busPerDay.Day = trip.DepartureDate.AddDays(i);

                busPerDay.NumOdBuses =
                    _db.Trips.Include(t => t.Expeditions)
                        .Where(t => busPerDay.Day >= t.DepartureDate && busPerDay.Day < t.ReturningDate)
                        .Sum(
                            t =>
                                (int?)t.Expeditions.Where(e => e.Status == (int)eExpeditionStatus.Approved || e.Status == (int)eExpeditionStatus.InProcess)
                                    .Sum(e => (int?)e.BasesPerDay) ?? 0);

                busPerDay.AvailableBuses = 56 - busPerDay.NumOdBuses;

                model.BusPerDays.Add(busPerDay);
            }

            model.Schools = _db.Schools.Select(s => new SchoolSelectList()
            {
                ID = s.ID,
                Name = s.InstituteID + " | " + s.Name
            }).ToList();

            if (Session["UserProfile"] != null)
            {
                UserSessionModel _user = (UserSessionModel)Session["UserProfile"];
                if (_user.RoleID == (int)eRole.SchoolManager)
                {
                    int instituteID = Convert.ToInt32(_user.Email);
                    model.SchoolID = _db.Schools.FirstOrDefault(s => s.InstituteID.Equals(instituteID)).ID;
                    model.DisableSchoolSelection = true;
                }
            }

            model.AgenciesList = _db.Agencies.RemoveInActive().Select(a => new EntitySelectList()
            {
                ID = a.ID,
                Name = a.Name
            }).OrderBy(a => a.Name).ToList();

            return View(model);
        }

        /*[EndiloAuthorization(eRole.Administrator, eRole.Staf, eRole.Agent, eRole.SchoolManager)]
        public PartialViewResult ManageExpedition(int ExpeditionID)
        {
            return PartialView("_ManageExpedition");
        }*/

        [EndiloAuthorization(eRole.Administrator, eRole.Staf, eRole.Agent, eRole.SchoolManager, eRole.Viewer)]
        public ActionResult ExpeditionStatus()
        {
            //TODO: Load all expeditions for admin and Staf
            Users _user = _db.GetUserByLoginName(User.Identity.Name);

            ExpeditionStatusViewModel model = new ExpeditionStatusViewModel();
            model.FilterExpeditions(_user);
            model.filters.LoadSchools();

            model.ConverExpeditionToViewModel();

            Session.Add("ExpeditionStatusExcelReport", model.Expeditions.ToList());

            return View(model);
        }

        [HttpPost]
        [EndiloAuthorization(eRole.Administrator, eRole.Staf, eRole.Agent, eRole.SchoolManager, eRole.Viewer)]
        public ActionResult FilterExpeditions(ExpeditionFilters filters)
        {
            Users _user = _db.GetUserByLoginName(User.Identity.Name);
            ExpeditionStatusViewModel model = new ExpeditionStatusViewModel();
            model.filters = filters;
            model.FilterExpeditions(_user);
            model.filters.LoadSchools();
            model.ConverExpeditionToViewModel();

            Session.Add("ExpeditionStatusExcelReport", model.Expeditions.ToList());
            return View("ExpeditionStatus", model);
        }

        [EndiloAuthorization(eRole.Administrator, eRole.Staf, eRole.Agent, eRole.SchoolManager, eRole.Viewer)]
        public ActionResult ExportExpeditionStatusReport()
        {
            if (Session["ExpeditionStatusExcelReport"] != null)
            {
                List<ExpeditionStatus> reportdata = (List<ExpeditionStatus>)Session["ExpeditionStatusExcelReport"];
                ExportToExcel<ExpeditionStatus> exc = new ExportToExcel<ExpeditionStatus>(reportdata.ToList(), ExpeditionStrings.ExpeditionStatus);
                var stream = exc.GetFile();
                string fileName = String.Format("{0} {1}.xlsx", ExpeditionStrings.ExpeditionStatus, DateTime.Now.ToString("dd-MM-yyyy hh:ss"));
                string contentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";

                stream.Position = 0;
                return File(stream, contentType, fileName);
            }

            return null;
        }

        [EndiloAuthorization(eRole.Administrator, eRole.Staf, eRole.Agent, eRole.SchoolManager)]
        [HttpGet]
        public ActionResult DeleteExpedition(int ID)
        {
            Expeditions _expedition = _db.Expeditions.Include(e => e.Trips).FirstOrDefault(e => e.ID.Equals(ID));

            DeleteViewModel model = new DeleteViewModel();
            model.IsDeletable = false;
            model.ID = ID;

            if (_expedition.Trips.DepartureDate.AddMonths(-6) > DateTime.Now)
            {
                model.IsDeletable = true;
            }

            return PartialView("_DeleteExpedition", model);
        }


        [EndiloAuthorization(eRole.Administrator, eRole.Agent)]
        [HttpGet]
        public ActionResult EditAgentStatus(int ID)
        {
            Expeditions _expedition = _db.Expeditions.Include(e => e.Trips).FirstOrDefault(e => e.ID.Equals(ID));

            UpdateAgentStatusViewModel model = new UpdateAgentStatusViewModel();

            model.ExpeditionID = _expedition.ID;
            model.Expedition = _expedition;

            return PartialView("_UpdateAgentStatus", model);
        }

        [EndiloAuthorization(eRole.Administrator, eRole.Agent)]
        [HttpPost]
        public ActionResult EditAgentStatus(UpdateAgentStatusViewModel model)
        {
            if (ModelState.IsValid)
            {
                Expeditions _expedition = _db.Expeditions.Include(e => e.Trips).FirstOrDefault(e => e.ID.Equals(model.ExpeditionID));

                _expedition.AgentStatus = (int)model.AgentStatusEnum;
                _expedition.AgentApproval = true;

                _db.Expeditions.AddOrUpdate(_expedition);

                int results = _db.SaveChanges();

                return RedirectToAction("ExpeditionStatus");
            }

            return PartialView("_UpdateAgentStatus", model);

        }


        [EndiloAuthorization(eRole.Administrator, eRole.Staf, eRole.Agent, eRole.SchoolManager)]
        [HttpPost]
        public ActionResult DeleteExpedition(DeleteViewModel model)
        {
            Expeditions _expedition = _db.Expeditions.Include(e => e.ExpeditionsFlights).Include(e => e.Trips).FirstOrDefault(e => e.ID.Equals(model.ID));

            foreach (var item in _expedition.ExpeditionsFlights)
            {
                _db.ExpeditionsFlights.Remove(item);
            }
            _expedition.Status = (int)eExpeditionStatus.Rejected;
            _expedition.IsActive = false;

            _db.Expeditions.AddOrUpdate(_expedition);

            int results = _db.SaveChanges();

            return RedirectToAction("ExpeditionStatus");
        }

        [HttpPost]
        public JsonResult ReadTheInstructions()
        {
            Session.Add("ReadOrderTripInstructions", true);
            return Json("true", JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult ShowGantt()
        {
            return View("Gantt");
        }

        [HttpGet]
        public JsonResult GetGanttData()
        {
            List<GanttData> data = new List<GanttData>()
            {
                new GanttData(1, $"משלחת 305  |  2 אוטובוסים", "My tooltip", new DateTime(2023, 01, 29, 08, 00, 00), new DateTime(2023, 01, 29, 11, 00, 00), new List<int>(){2}, 2, color: "#00AD82"),
                //new GanttData(1, $"4 אוטובוסים", "My tooltip", new DateTime(2023, 01, 29, 13, 00, 00), new DateTime(2023, 01, 29, 16, 00, 00), new List<int>(){2}, 2),
                new GanttData(2, "משלחת  205 |  2 אוטובוסים", "My tooltip", new DateTime(2023, 01, 29, 09, 00, 00), new DateTime(2023, 01, 29, 10, 30, 00), new List<int>(){2}, 2, color: "#00AD82"),
                new GanttData(2, "משלחת  203 |  2 אוטובוסים", "My tooltip", new DateTime(2023, 01, 29, 08, 00, 00), new DateTime(2023, 01, 29, 10, 30, 00), new List<int>(){1,1}, 2),
            };

            return Json(data, JsonRequestBehavior.AllowGet);

        }
    }
}