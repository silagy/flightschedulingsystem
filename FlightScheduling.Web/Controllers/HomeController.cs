﻿using FlightScheduling.Web.Infrastructure;
using Microsoft.Reporting.WebForms;
using System;
using System.Web.Mvc;
using System.Web.UI.WebControls;

namespace FlightScheduling.Web.Controllers
{
    public class HomeController : Controller
    {
        //
        // GET: /Home/
        public ActionResult Index()
        {

            var reportViewer = new ReportViewer()
            {
                ProcessingMode = ProcessingMode.Remote,
                SizeToReportContent = true,
                Width = Unit.Percentage(100),
                Height = Unit.Percentage(100),
            };


            IReportServerCredentials irsc = new CustomReportCredentials("administrator", "$up#014", "");
            reportViewer.ServerReport.ReportServerCredentials = irsc;
            reportViewer.ServerReport.ReportPath = "/Reports/Form60";
            reportViewer.ServerReport.ReportServerUrl = new Uri("http://192.168.32.3/Reportserver/");

            ViewBag.ReportViewer = reportViewer;

            return View();
        }
	}
}