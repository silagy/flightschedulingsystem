﻿using FlightScheduling.Web.Models;
using System;
using System.Web.Mvc;
using FlightScheduling.Web.Infrastructure;
using Microsoft.Reporting.WebForms;
using System.Web.UI.WebControls;
using System.Collections.Generic;

namespace FlightScheduling.Web.Controllers
{
    public class ReportViewerReportsController : Controller
    {

        public ActionResult ExecuteReport(ReportViewerViewModel reportModel)
        {
            var reportViewer = new ReportViewer()
            {
                ProcessingMode = ProcessingMode.Remote,
                SizeToReportContent = true,
                Width = Unit.Pixel(400),
                Height = Unit.Pixel(600),
            };

            List<ReportParameter> _parameters = new List<ReportParameter>();
            foreach (var rParam in reportModel.ReportParameters)
            {
                ReportParameter _param = new ReportParameter(rParam.Key, rParam.Value);
                _parameters.Add(_param);
            }

            reportViewer.ServerReport.ReportPath = reportModel.ReportUrl;
            reportViewer.ServerReport.ReportServerUrl = new Uri(ReportsRepository.ReportServerURL);
            IReportServerCredentials irsc = new CustomReportCredentials(reportModel.Username, reportModel.Password, "");
            reportViewer.ServerReport.ReportServerCredentials = irsc;
            if (_parameters.Count > 0)
            {
                reportViewer.ServerReport.SetParameters(_parameters);
            }
            

            ViewBag.ReportViewer = reportViewer;

            return View("ViewReport");
        }
    }
}