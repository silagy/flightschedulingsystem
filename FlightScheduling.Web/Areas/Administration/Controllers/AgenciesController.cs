﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Flight.DAL.FlightDB.Methods;
using FlightScheduling.DAL;
using System.Data.Entity;
using FlightScheduling.Web.Areas.Administration.Models;
using FlightScheduling.Web.Areas.Administration.Models.Agencies;
using Otakim.Website.Infrastructure;

namespace FlightScheduling.Web.Areas.Administration.Controllers
{
    [EndiloAuthorization(eRole.Administrator, eRole.Staf)]
    public class AgenciesController : Controller
    {

        private FlightDB _db = new FlightDB();

        protected override void Dispose(bool disposing)
        {
            _db.Dispose();
            base.Dispose(disposing);
        }
        

        [HttpGet]
        public ActionResult AddAgency()
        {
            AgencyViewModel model = new AgencyViewModel();
            return View(model);
        }

        [HttpPost]
        public ActionResult AddAgency(AgencyViewModel model)
        {
            if (ModelState.IsValid)
            {
                Agencies _agencyToAdd = new Agencies();
                _agencyToAdd.IsActive = true;
                _agencyToAdd.Name = model.Name;
                _agencyToAdd.Email = model.Email;
                _agencyToAdd.Phone = model.Phone;
                _agencyToAdd.Fax = model.Fax;
                _agencyToAdd.Manager = model.Manager;
                _agencyToAdd.ManagerPhone = model.ManagerPhone;
                _agencyToAdd.CreationDate = DateTime.Now;
                _agencyToAdd.UpdateDate = DateTime.Now;

                _db.Agencies.Add(_agencyToAdd);

                int results = _db.SaveChanges();

                return RedirectToAction("GetAgencies");
            }

            return View(model);
        }

        public ActionResult GetAgencies()
        {
            AgenciesViewModel model = new AgenciesViewModel();
            model.AgencyViewModels = _db.Agencies.RemoveInActiveAgencies().Select(a => new AgencyViewModel()
            {
                ID = a.ID,
                Email = a.Email,
                Fax = a.Fax,
                Manager = a.Manager,
                ManagerPhone = a.ManagerPhone,
                Name = a.Name,
                Phone = a.Phone
            }).ToList();

            return View(model);
        }

        [HttpGet]
        public ActionResult EditAgency(int ID)
        {
            if (ID != null)
            {
                Agencies _agency = _db.Agencies.FirstOrDefault(a => a.ID.Equals(ID));

                AgencyViewModel model = new AgencyViewModel();

                model.IsActive = _agency.IsActive;
                model.Name = _agency.Name;
                model.Email = _agency.Email;
                model.Phone = _agency.Phone;
                model.Fax = _agency.Fax;
                model.Manager = _agency.Manager;
                model.ManagerPhone = _agency.ManagerPhone;

                return View(model);
            }

            return RedirectToAction("GetAgencies");
        }

        [HttpPost]
        public ActionResult EditAgency(AgencyViewModel model)
        {
            if (ModelState.IsValid)
            {
                Agencies _agency = _db.Agencies.FirstOrDefault(a => a.ID.Equals(model.ID));

                _agency.IsActive = model.IsActive;
                _agency.Name = model.Name;
                _agency.Email = model.Email;
                _agency.Phone = model.Phone;
                _agency.Fax = model.Fax;
                _agency.Manager = model.Manager;
                _agency.ManagerPhone = model.ManagerPhone;
                _agency.UpdateDate = DateTime.Now;

                _db.Agencies.AddOrUpdate(_agency);

                int results = _db.SaveChanges();

                return RedirectToAction("GetAgencies");
            }

            return View(model);
        }

        [HttpGet]
        public ActionResult DeleteAgency(int ID)
        {
            var _agency = _db.Agencies.Include(a => a.Users).FirstOrDefault(a => a.ID.Equals(ID));

            DeleteAgencyViewModel model = new DeleteAgencyViewModel();
            model.ID = _agency.ID;
            model.Users = _agency.Users.ToList();

            return PartialView("_AgencyDeleteConfirm",model);
        }

        [HttpPost]
        public ActionResult DeleteAgency(DeleteAgencyViewModel model)
        {
            var _agency = _db.Agencies.FirstOrDefault(a => a.ID.Equals(model.ID));
            _agency.IsActive = false;

            _db.Agencies.AddOrUpdate(_agency);

            int results = _db.SaveChanges();

            return RedirectToAction("GetAgencies");
        }
	}
}