﻿using Endilo.Helpers;
using Flight.DAL.FlightDB.Methods;
using FlightScheduling.DAL;
using FlightScheduling.DAL.CustomEntities.GroupForms;
using FlightScheduling.DAL.CustomEntities.Notifications;
using FlightScheduling.DAL.CustomEntities.Permissions;
using FlightScheduling.Helpers;
using FlightScheduling.Language;
using FlightScheduling.Web.App_Start;
using FlightScheduling.Web.Areas.Administration.Models.Administration;
using FlightScheduling.Web.Areas.Administration.Models.Common;
using FlightScheduling.Web.Areas.Administration.Models.GroupForms;
using FlightScheduling.Web.Areas.Administration.Models.Users;
using FlightScheduling.Web.Controllers;
using FlightScheduling.Web.Infrastructure;
using FlightScheduling.Web.Infrastructure.Notifications;
using FlightScheduling.Web.Models;
using FlightScheduling.Website.Infrastructure;
using Newtonsoft.Json;
using Otakim.Website.Infrastructure;
using Rotativa.MVC;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Web.Mvc;

namespace FlightScheduling.Web.Areas.Administration.Controllers
{
    [Authorize]
    public class GroupFormsController : Controller
    {
        //Properties
        private DBRepository _dbDapper = new DBRepository();

        public PartialViewResult GetGroup180FormNavigation(int SchoolID, int GroupID, eFormChapterType CurrentFormStep)
        {
            GroupFormFullStatus model = _dbDapper
                .GetRecords<GroupFormFullStatus>(QueriesRepository.GetGroup180FormFullStatus, new { GroupID, SchoolID })
                .FirstOrDefault();
            model.CurrentGroupFormStep = CurrentFormStep;

            return PartialView("_Group180FormNavigation", model);
        }

        public ActionResult GetGroup180Forms(int GroupID)
        {
            Group180FormsViewModel model = new Group180FormsViewModel();
            int? SchoolID = null;
            int? Region = null;

            if (User.IsInRole(eRole.CountyManager.ToString()))
            {
                Region = FunctionHelpers.User.RegionID;
            }
            else if (User.IsInRole(eRole.SchoolManager.ToString()))
            {
                SchoolID = _dbDapper.GetSchoolIDByUserID(FunctionHelpers.User.UserID);
            }

            model.Forms = _dbDapper.GetRecords<GroupFormFullStatus>(QueriesRepository.GetGroup180Forms,
                new { GroupID, SchoolID, Region });
            model.GroupFolderData.GroupFolderData = _dbDapper.GetGroupFolderDataByID(GroupID);

            return View("Group180Forms", model);
        }

        [HttpGet]
        [PermissionAuthorization(PermissionBitEnum.Form180_MoveForm)]
        public PartialViewResult MoveForm180(int GroupID, int SchoolID)
        {
            Move180ViewModel model = new Move180ViewModel();
            model = _dbDapper
                .GetRecords<Move180ViewModel>(QueriesRepository.GetSchoolGroupForMove180, new { GroupID, SchoolID })
                .FirstOrDefault();
            model.Groups =
                _dbDapper.GetRecords<SchoolGroupsViewModel>(QueriesRepository.GetSchoolsGroupsBySchoolandGroup,
                    new { GroupID, SchoolID });

            return PartialView("_MoveForm180", model);
        }

        [HttpGet]
        [PermissionAuthorization(PermissionBitEnum.Form180_MoveForm)]
        public JsonResult CheckIfTargetGroupHas180Form(int GroupID, int SchoolID)
        {
            var _results = _dbDapper
                .GetRecords<dynamic>(QueriesRepository.CheckIfTargetGroupHas180Form, new { GroupID, SchoolID })
                .FirstOrDefault();

            return Json(JsonConvert.SerializeObject(_results), JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        [PermissionAuthorization(PermissionBitEnum.Form180_MoveForm)]
        public ActionResult MoveForm180(Move180ViewModel model)
        {
            if (model.TargetGroupSchoolID > 0)
            {
                // TODO: DELETE ALL FORMS BEFORE MOVING OTHER FORMS
                //bool results = _dbDapper.UpdateOperation(QueriesRepository.Move180ReplaceForm, new { model.GroupFormID, model.TargetGroupSchoolID });
            }
            else
            {
                // Get the target Group School ID connection in the SubSchoolGroups Table
                int GroupSchoolID = _dbDapper.GetRecords<int>(QueriesRepository.GetSchoolGroupIDByGroupIDAndSchoolID,
                    new { GroupID = model.TargetGroupID, model.SchoolID }).FirstOrDefault();

                // Get the internal ID of the Group Form. It will be used to move it to the new Group School ID
                int GroupFormInternalID = _dbDapper
                    .GetRecords<int>(QueriesRepository.GetGroupFormIntenalIDByGroupFormID, new { model.GroupFormID })
                    .FirstOrDefault();

                // Replace the forms
                bool results = _dbDapper.UpdateOperation(QueriesRepository.Move180FromByReplacingGroupFormID,
                    new { GroupSchoolID, ID = GroupFormInternalID });
            }

            return RedirectToAction("GetGroup180Forms", new { GroupID = model.TargetGroupID });
        }

        public void CreatingGroupFormStatusUpdateNotification(int FormID, eGroupFormStatus FormStatus,
            eFormChapterType FormType)
        {
            IEnumerable<NotificationForm180StatusUpdate>
                notificationsData = new List<NotificationForm180StatusUpdate>();
            List<NotificationTemplate> _notificationTemplates = new List<NotificationTemplate>();
            switch (FormType)
            {
                case eFormChapterType.Administration:
                {
                    notificationsData = _dbDapper.GetRecords<NotificationForm180StatusUpdate>(
                        QueriesRepository.GetGroup180AdministrativeFormNotificationStatusUpdate, new { FormID });
                    _notificationTemplates = _dbDapper.GetRecords<NotificationTemplate>(
                        QueriesRepository.GetNotificationTemplatesByTrrigerType,
                        new { TriggerType = (int)eNotificationTriggerType.GroupFormAdministration180StatusChange });
                    break;
                }
                case eFormChapterType.Instructors:
                {
                    notificationsData = _dbDapper.GetRecords<NotificationForm180StatusUpdate>(
                        QueriesRepository.GetGroup180InstructorFormNotificationStatusUpdate, new { FormID });
                    _notificationTemplates = _dbDapper.GetRecords<NotificationTemplate>(
                        QueriesRepository.GetNotificationTemplatesByTrrigerType,
                        new { TriggerType = (int)eNotificationTriggerType.GroupFormInstructors180StatusChange });
                    break;
                }
                case eFormChapterType.Plan:
                {
                    notificationsData =
                        _dbDapper.GetRecords<NotificationForm180StatusUpdate>(
                            QueriesRepository.GetGroup180PlanFormNotificationStatusUpdate, new { FormID });
                    _notificationTemplates = _dbDapper.GetRecords<NotificationTemplate>(
                        QueriesRepository.GetNotificationTemplatesByTrrigerType,
                        new { TriggerType = (int)eNotificationTriggerType.GroupFormPlan180StatusChange });
                    break;
                }
            }


            foreach (var _notificationData in notificationsData)
            {
                _notificationData.FormUrl = String.Format("{0}{1}", AppConfig.GetSiteRootUrl,
                    Url.Action("GetGroup180Forms", "GroupForms", new { _notificationData.GroupID }));

                _notificationData.FormStatus = FormStatus.GetDescription();

                foreach (var _notification in _notificationTemplates)
                {
                    List<UserViewModel> _UsersToSend = new List<UserViewModel>();
                    if (_notification.UserTypeEnum == eRole.SchoolManager)
                    {
                        _UsersToSend = _dbDapper.GetRecords<UserViewModel>(QueriesRepository.GetUsersBySchoolID,
                            new { _notificationData.SchoolID });
                    }
                    else if (_notification.UserTypeEnum == eRole.Staf && FormType == eFormChapterType.Administration)
                    {
                        _UsersToSend = _dbDapper.GetRecords<UserViewModel>(QueriesRepository.GetUsersByTypeAndRole,
                            new { _notification.UserType, UserRole = (int)eRoleType.Administration });
                    }
                    else if (_notification.UserTypeEnum == eRole.CountyManager &&
                             FormType == eFormChapterType.Instructors)
                    {
                        _UsersToSend = _dbDapper.GetRecords<UserViewModel>(
                            QueriesRepository.GetCountyManagersBySchoolID,
                            new { _notificationData.SchoolID, _notification.UserType });
                    }
                    else if (_notification.UserTypeEnum == eRole.Staf && FormType == eFormChapterType.Plan)
                    {
                        _UsersToSend = _dbDapper.GetRecords<UserViewModel>(QueriesRepository.GetUsersByTypeAndRole,
                            new { _notification.UserType, UserRole = (int)eRoleType.PreparationPlan });
                    }


                    foreach (var _user in _UsersToSend)
                    {
                        if (NotificationHelper.IsValidEmail(_user.Email))
                        {
                            Notifications _notificationToSend = new Notifications();
                            _notificationToSend.NotificationTempateID = _notification.ID;
                            _notificationToSend.MessageSubject = _notification.Subject;
                            _notificationToSend.UserID = _user.ID;
                            _notificationToSend.EmailAddress = _user.Email;
                            _notificationToSend.CallToAction = Url.Action("GetGroup180Forms", "GroupForms",
                                new { GroupID = _notificationData.GroupID });
                            _notificationToSend.CreationDate = DateTime.Now;
                            _notificationToSend.IsRead = false;

                            //Complete the Custom Notification Details
                            _notificationData.UserID = _user.ID;
                            _notificationData.UserFirstName = _user.FirstName;
                            _notificationData.UserLastName = _user.LastName;

                            _notificationToSend.NotificationStatus = eNotificationStatus.Pendding;
                            _notificationToSend.MessageBody =
                                _notificationData.ConvertProperties(_notification.Message);

                            // TODO: To insert all of the notifications at once. 
                            bool _result = _dbDapper.ExecuteCommand(QueriesRepository.InsertNotification,
                                new
                                {
                                    _notificationToSend.NotificationTempateID, _notificationToSend.UserID,
                                    _notificationToSend.EmailAddress, _notificationToSend.NotificationStatus,
                                    _notificationToSend.MessageSubject, _notificationToSend.MessageBody,
                                    _notificationToSend.CallToAction, _notificationToSend.CreationDate,
                                    _notificationToSend.IsRead
                                });
                        }
                    }
                }
            }
        }


        #region Administration Section

        [HttpGet]
        [PermissionAuthorization(PermissionBitEnum.Form180_Adimistrative_View)]
        public ActionResult Get180AdministrativeForm(int SchoolID, int GroupID)
        {
            //TODO: Check that the logged on user is a school manager and have access to the page.
            //if (User.IsInRole(eRole.SchoolManager.ToString()) && SchoolID != _dbDapper.GetSchoolIDByUserID(FunctionHelpers.User.UserID))
            //{
            //    // The user is not allowed to edit the form, therfore he is being directed to the login
            //    return Redirect("~/");
            //}

            AdministrationForm180ViewModel model = new AdministrationForm180ViewModel();
            model.GroupDetails.GroupID = GroupID;

            //Load the generic details
            model.SchoolData = _dbDapper
                .GetRecords<GroupFormSchoolData>(QueriesRepository.GetAdministrativeFormSchoolDetails,
                    new { SchoolID, GroupID }).FirstOrDefault();

            // Check if a form has been created, if not it will be created
            model.GeneralFormData = _dbDapper.GetRecords<GroupFormViewModel>(QueriesRepository.GetGroup180FormDetails,
                new { model.GroupDetails.GroupID, model.SchoolData.SchoolID }).FirstOrDefault();
            if (model.GeneralFormData.ID == null)
            {
                model.GeneralFormData = new GroupFormViewModel();
                model.GeneralFormData.FormType = (int)eGroupFormType.Form180;
                model.GeneralFormData.CreationDate = DateTime.Now;
                model.GeneralFormData.UpdateDate = DateTime.Now;
                model.GeneralFormData.ID = _dbDapper.GetRecords<int>(QueriesRepository.Create180GeneralForm, new
                {
                    model.SchoolData.SchoolGroupAssosicationID,
                    model.GeneralFormData.FormType,
                    model.GeneralFormData.CreationDate,
                    model.GeneralFormData.UpdateDate,
                }).FirstOrDefault();

                model.AdministrativeFormData.FormStatus = (int)eGroupFormStatus.New;
                model.AdministrativeFormData.GroupFormID = (int)model.GeneralFormData.ID;
                model.AdministrativeFormData.CreationDate = DateTime.Now;
                model.AdministrativeFormData.UpdateDate = DateTime.Now;
                model.AdministrativeFormData.UserID = FunctionHelpers.User.UserID;

                model.AdministrativeFormData.ID = _dbDapper.GetRecords<int>(
                    QueriesRepository.Create180AdministrativeFormBasicData, new
                    {
                        model.AdministrativeFormData.GroupFormID,
                        model.AdministrativeFormData.FormStatus,
                        model.AdministrativeFormData.CreationDate,
                        model.AdministrativeFormData.UpdateDate,
                        model.AdministrativeFormData.UserID
                    }).FirstOrDefault();
            }
            else
            {
                model.AdministrativeFormData = _dbDapper
                    .GetRecords<AdministrativeFormViewModel>(QueriesRepository.GetAdiministrativeFormDetails,
                        new { GroupFormID = model.GeneralFormData.ID }).FirstOrDefault();

                //Check if the form data exists, if not create new form
                if (model.AdministrativeFormData == null)
                {
                    model.AdministrativeFormData = new AdministrativeFormViewModel();
                    model.AdministrativeFormData.FormStatus = (int)eGroupFormStatus.New;
                    model.AdministrativeFormData.GroupFormID = (int)model.GeneralFormData.ID;
                    model.AdministrativeFormData.CreationDate = DateTime.Now;
                    model.AdministrativeFormData.UpdateDate = DateTime.Now;
                    model.AdministrativeFormData.UserID = FunctionHelpers.User.UserID;

                    model.AdministrativeFormData.ID = _dbDapper.GetRecords<int>(
                        QueriesRepository.Create180AdministrativeFormBasicData, new
                        {
                            model.AdministrativeFormData.GroupFormID,
                            model.AdministrativeFormData.FormStatus,
                            model.AdministrativeFormData.CreationDate,
                            model.AdministrativeFormData.UpdateDate,
                            model.AdministrativeFormData.UserID
                        }).FirstOrDefault();
                }

                if (model.AdministrativeFormData.GroupFormStatusEnum == eGroupFormStatus.WaitingApproval ||
                    model.AdministrativeFormData.GroupFormStatusEnum == eGroupFormStatus.Approved)
                {
                    model.IsDisabled = true;
                }

                if (FunctionHelpers.User.RoleID == (int)eRole.SchoolManager)
                {
                    model.BusesEditingDisabled = true;
                }
            }


            // Remove the school infromation from the form when the Form is new - to force the school manager to type in the details.
            if (model.AdministrativeFormData.GroupFormStatusEnum == eGroupFormStatus.New)
            {
                model.SchoolData.Manager = null;
                model.SchoolData.ManagerCell = null;
                model.SchoolData.ManagerEmail = null;
                model.SchoolData.ManagerPhone = null;
                model.SchoolData.SchoolEmail = null;
                model.SchoolData.SchoolFax = null;
                model.SchoolData.SchoolPhone = null;
            }

            //Load the trip Details
            model.TripDetails = _dbDapper
                .GetRecords<TripDetailsViewModel>(QueriesRepository.GetAdministrative180SchoolDetails,
                    new { ID = GroupID }).FirstOrDefault();

            model.GroupDetails = _dbDapper
                .GetRecords<GroupDetailsViewModel>(QueriesRepository.GetAdministrativeFormGroupDetails, new { GroupID })
                .FirstOrDefault();

            model.GroupDetails.Agencies.Items = _dbDapper
                .GetRecords<CustomSelectListItem>(QueriesRepository.GetAllAgencies, null).Select(a =>
                    new CustomSelectListItem()
                    {
                        ID = a.ID,
                        Name = a.Name
                    }).ToList();

            //Load manager Data,
            model.ManagerData = _dbDapper.GetRecords<GroupManagerData>(QueriesRepository.GetGroupManagerDataByGroupID,
                    new { model.GroupDetails.GroupID, RoleID = (int)eInstructorRoleType.ExpeditionManager })
                .FirstOrDefault();

            //Check if after returning from the DB, the manager data is null
            if (model.ManagerData == null)
            {
                model.ManagerData = new GroupManagerData();
            }

            //Load form messages
            model.FormComments.Comments = _dbDapper.GetRecords<FormCommentViewModel>(QueriesRepository.GetFormComments,
                new { FormID = model.GeneralFormData.ID, FormChapterType = (int)eFormChapterType.Administration });
            model.FormComments.FormID = (int)model.GeneralFormData.ID;
            model.FormComments.FormChapterType = eFormChapterType.Administration;

            //Check approval mode - this enabled the option to approve the form
            if ((model.AdministrativeFormData.GroupFormStatusEnum != eGroupFormStatus.Approved ||
                 model.AdministrativeFormData.GroupFormStatusEnum != eGroupFormStatus.New) &&
                PermissionLoader.CheckUserPermissions(PermissionBitEnum.Form180_Adimistrative_Approval))
            {
                model.ApprovalMode = true;
            }

            return View("Get180AdministrativeForm", model);
        }

        [HttpPost]
        [PermissionAuthorization(PermissionBitEnum.Form180_Adimistrative_Editing)]
        public ActionResult Save180AdministrativeForm(AdministrationForm180ViewModel model)
        {
            if (ModelState.IsValid)
            {
                var validationSucess = true;
                // Updating General Information
                GroupFormViewModel _formDetails = _dbDapper
                    .GetRecords<GroupFormViewModel>(QueriesRepository.GetGroup180FormDetails,
                        new { model.GroupDetails.GroupID, model.SchoolData.SchoolID }).FirstOrDefault();

                //Update the administrative 180 from data
                model.AdministrativeFormData.UpdateDate = DateTime.Now;

                // Update the form to Inprogres
                if (model.AdministrativeFormData.FormStatus == (int)eGroupFormStatus.New)
                {
                    model.AdministrativeFormData.FormStatus = (int)eGroupFormStatus.InProgress;
                    var resultAdministrativeFromData = _dbDapper.UpdateAdministrative180FormUpdate(
                        (int)model.AdministrativeFormData.ID, (int)model.AdministrativeFormData.FormStatus,
                        model.AdministrativeFormData.UpdateDate);
                }


                // Update Form general detials like class type and cost per student
                bool updateForm180AdministrativeResults = _dbDapper.UpdateOperation(
                    QueriesRepository.UpdateAdministrative180FormGenericData,
                    new
                    {
                        model.AdministrativeFormData.ClassType, model.AdministrativeFormData.CostPerStudent,
                        model.AdministrativeFormData.ID
                    });


                // DEALING WITH SAVING FILES WITHOUR SUBMITTING THE FORM
                if (model.AdministrativeFormData.SignaturePage.HasFile())
                {
                    if (!Directory.Exists(Path.Combine(AppConfig.Groups180FormsFileStorage,
                            model.GroupDetails.GroupID.ToString())))
                    {
                        Directory.CreateDirectory(Path.Combine(AppConfig.Groups180FormsFileStorage,
                            model.GroupDetails.GroupID.ToString()));
                    }

                    string filename = String.Format("{0}{1}{2}{3}", model.GroupDetails.GroupID,
                        model.SchoolData.SchoolID, DateTime.Now.ToString("ddMMyyyyhhmmssffffff"),
                        Path.GetExtension(model.AdministrativeFormData.SignaturePage.FileName));
                    model.AdministrativeFormData.SignaturePage.SaveAs(Server.MapPath(String.Format("{0}/{1}",
                        AppConfig.Groups180FormsFileStorageRelative, filename)));
                    model.AdministrativeFormData.SignatureUrl = filename;

                    //Update database
                    var sighnatureUpdateresults = _dbDapper.UpdateOperation(
                        QueriesRepository.UpdateAdministrativeForm180SignatureUrl,
                        new { model.AdministrativeFormData.SignatureUrl, model.AdministrativeFormData.ID });
                }

                if (model.AdministrativeFormData.Contract.HasFile())
                {
                    if (!Directory.Exists(Path.Combine(AppConfig.Groups180FormsFileStorage,
                            model.GroupDetails.GroupID.ToString())))
                    {
                        Directory.CreateDirectory(Path.Combine(AppConfig.Groups180FormsFileStorage,
                            model.GroupDetails.GroupID.ToString()));
                    }

                    string filename = String.Format("{0}{1}{2}{3}", model.GroupDetails.GroupID,
                        model.SchoolData.SchoolID, DateTime.Now.ToString("ddMMyyyyhhmmssffffff"),
                        Path.GetExtension(model.AdministrativeFormData.Contract.FileName));
                    model.AdministrativeFormData.Contract.SaveAs(Server.MapPath(String.Format("{0}/{1}",
                        AppConfig.Groups180FormsFileStorageRelative, filename)));
                    model.AdministrativeFormData.ContractUrl = filename;

                    //Update database
                    var sighnatureUpdateresults = _dbDapper.UpdateOperation(
                        QueriesRepository.UpdateAdministrativeForm180ContactUrl,
                        new { model.AdministrativeFormData.ContractUrl, model.AdministrativeFormData.ID });
                }

                // END DEALING WITH SAVING FILES WITHOUT SUBMITTING THE FORM

                if (model.SaveAndSubmit)
                {
                    //Check validity of the form

                    //Check if form has files
                    AdministrativeFormFiles _files = _dbDapper
                        .GetRecords<AdministrativeFormFiles>(QueriesRepository.AdministrativeForm180_GetFiles,
                            new { model.AdministrativeFormData.ID }).FirstOrDefault();

                    if (!model.AdministrativeFormData.SignaturePage.HasFile() &&
                        String.IsNullOrEmpty(_files.SignatureUrl))
                    {
                        ModelState.AddModelError("AdministrativeFormData.SignaturePage",
                            "עליך לעלות דף חתימות חתום בפורמט PDF");
                        validationSucess = false;
                    }
                    else if (model.AdministrativeFormData.SignaturePage.HasFile() &&
                             Path.GetExtension(model.AdministrativeFormData.SignaturePage.FileName) != ".pdf")
                    {
                        ModelState.AddModelError("AdministrativeFormData.SignaturePage",
                            "עליך לעלות דף חתימות חתום בפורמט PDF");
                        validationSucess = false;
                    }
                    else if (String.IsNullOrEmpty(_files.SignatureUrl) ||
                             model.AdministrativeFormData.SignaturePage.HasFile())
                    {
                        if (!Directory.Exists(Path.Combine(AppConfig.Groups180FormsFileStorage,
                                model.GroupDetails.GroupID.ToString())))
                        {
                            Directory.CreateDirectory(Path.Combine(AppConfig.Groups180FormsFileStorage,
                                model.GroupDetails.GroupID.ToString()));
                        }

                        string filename = String.Format("{0}{1}{2}{3}", model.GroupDetails.GroupID,
                            model.SchoolData.SchoolID, DateTime.Now.ToString("ddMMyyyyhhmmssffffff"),
                            Path.GetExtension(model.AdministrativeFormData.SignaturePage.FileName));
                        model.AdministrativeFormData.SignaturePage.SaveAs(Server.MapPath(String.Format("{0}/{1}",
                            AppConfig.Groups180FormsFileStorageRelative, filename)));
                        model.AdministrativeFormData.SignatureUrl = filename;

                        //Update database
                        var sighnatureUpdateresults = _dbDapper.UpdateOperation(
                            QueriesRepository.UpdateAdministrativeForm180SignatureUrl,
                            new { model.AdministrativeFormData.SignatureUrl, model.AdministrativeFormData.ID });
                    }

                    if (!model.AdministrativeFormData.Contract.HasFile() && String.IsNullOrEmpty(_files.ContractUrl))
                    {
                        ModelState.AddModelError("AdministrativeFormData.Contract", "עליך לעלות חוזה חתום בפורמט PDF");
                        validationSucess = false;
                    }
                    else if (model.AdministrativeFormData.Contract.HasFile() &&
                             Path.GetExtension(model.AdministrativeFormData.Contract.FileName) != ".pdf")
                    {
                        ModelState.AddModelError("AdministrativeFormData.Contract", "עליך לעלות חוזה חתום בפורמט PDF");
                        validationSucess = false;
                    }
                    else if (String.IsNullOrEmpty(_files.ContractUrl) ||
                             model.AdministrativeFormData.Contract.HasFile())
                    {
                        if (!Directory.Exists(Path.Combine(AppConfig.Groups180FormsFileStorage,
                                model.GroupDetails.GroupID.ToString())))
                        {
                            Directory.CreateDirectory(Path.Combine(AppConfig.Groups180FormsFileStorage,
                                model.GroupDetails.GroupID.ToString()));
                        }

                        string filename = String.Format("{0}{1}{2}{3}", model.GroupDetails.GroupID,
                            model.SchoolData.SchoolID, DateTime.Now.ToString("ddMMyyyyhhmmssffffff"),
                            Path.GetExtension(model.AdministrativeFormData.Contract.FileName));
                        model.AdministrativeFormData.Contract.SaveAs(Server.MapPath(String.Format("{0}/{1}",
                            AppConfig.Groups180FormsFileStorageRelative, filename)));
                        model.AdministrativeFormData.ContractUrl = filename;

                        //Update database
                        var sighnatureUpdateresults = _dbDapper.UpdateOperation(
                            QueriesRepository.UpdateAdministrativeForm180ContactUrl,
                            new { model.AdministrativeFormData.ContractUrl, model.AdministrativeFormData.ID });
                    }

                    //Load manager Data,
                    model.ManagerData = _dbDapper
                        .GetRecords<GroupManagerData>(QueriesRepository.GetGroupManagerDataByGroupID,
                            new { model.GroupDetails.GroupID, RoleID = (int)eInstructorRoleType.ExpeditionManager })
                        .FirstOrDefault();

                    if (model.ManagerData == null)
                    {
                        model.ManagerData = new GroupManagerData();
                        ModelState.AddModelError("ManagerData.InstructorID", "עליך לציין מי הוא מנהל המשלחת");
                        validationSucess = false;
                    }


                    if (!validationSucess)
                    {
                        //Load Agencies
                        model.GroupDetails.Agencies.Items = _dbDapper
                            .GetRecords<CustomSelectListItem>(QueriesRepository.GetAllAgencies, null).Select(a =>
                                new CustomSelectListItem()
                                {
                                    ID = a.ID,
                                    Name = a.Name
                                }).ToList();

                        //Load form messages
                        model.FormComments.Comments = _dbDapper.GetRecords<FormCommentViewModel>(
                            QueriesRepository.GetFormComments,
                            new
                            {
                                FormID = model.GeneralFormData.ID,
                                FormChapterType = (int)eFormChapterType.Administration
                            });
                        model.FormComments.FormID = (int)model.GeneralFormData.ID;
                        model.FormComments.FormChapterType = eFormChapterType.Administration;

                        return View("Get180AdministrativeForm", model);
                    }

                    model.AdministrativeFormData.FormStatus = (int)eGroupFormStatus.WaitingApproval;
                    var resultAdministrativeFromData = _dbDapper.UpdateAdministrative180FormUpdate(
                        (int)model.AdministrativeFormData.ID, (int)model.AdministrativeFormData.FormStatus,
                        model.AdministrativeFormData.UpdateDate);

                    //Send Notifications
                    if (resultAdministrativeFromData)
                    {
                        CreatingGroupFormStatusUpdateNotification((int)model.AdministrativeFormData.ID,
                            eGroupFormStatus.WaitingApproval, eFormChapterType.Administration);
                    }
                }

                //Updating Form School Data
                var resultsSchoolData = _dbDapper.UpdateAdministrative180FormSchoolData(
                    model.SchoolData.SchoolID,
                    model.SchoolData.SchoolEmail,
                    model.SchoolData.SchoolFax,
                    model.SchoolData.SchoolPhone,
                    model.SchoolData.SchoolStream,
                    model.SchoolData.IsSpecialEducation,
                    model.SchoolData.Manager,
                    model.SchoolData.ManagerPhone,
                    model.SchoolData.SchoolEmail);

                // Updating Group Data
                var updateGroupDataResults = _dbDapper.UpdateOperation(
                    QueriesRepository.UpdateAdministrative180FormGroupData, new
                    {
                        model.GroupDetails.NumberofBuses,
                        model.GroupDetails.AgencyID,
                        model.GroupDetails.GroupID
                    });


                if (model.SaveAndSubmit && validationSucess)
                {
                    return RedirectToAction("Get180InstructorsForm",
                        new { SchoolID = model.SchoolData.SchoolID, GroupID = model.GroupDetails.GroupID });
                }
            }


            return RedirectToAction("Get180AdministrativeForm",
                new { SchoolID = model.SchoolData.SchoolID, GroupID = model.GroupDetails.GroupID });
        }

        [HttpGet]
        public JsonResult SearchForGroupManager(string SearchTerm)
        {
            List<int> _menagers = new List<int>();
            _menagers.Add(2);
            List<AutoCompleteItem> items =
                _dbDapper.GetRecords<AutoCompleteItem>(QueriesRepository.SearchForManager,
                    new { SearchTerm, RoleID = _menagers.AsEnumerable() });

            return Json(JsonConvert.SerializeObject(items), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult SearchForInstructor(string SearchTerm)
        {
            IEnumerable<int> _instructors = AppConfig.Group180SearchForMentors;
            List<AutoCompleteItem> items =
                _dbDapper.GetRecords<AutoCompleteItem>(QueriesRepository.SearchForManager,
                    new { SearchTerm, RoleID = _instructors });

            return Json(JsonConvert.SerializeObject(items), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult CheckInstructorRegistration(string InstructorID, int GroupID, int InternalID)
        {
            dynamic json = new ExpandoObject();
            json.InstructorID = InstructorID;

            if (!_dbDapper.CheckInstructorExistByInstructorID(InstructorID))
            {
                json.Success = false;
                json.ErrorReason = InstructorsStrings.InstructorDoesntExist;
                json.AllowOverRide = false;
                return Json(JsonConvert.SerializeObject(json), JsonRequestBehavior.AllowGet);
            }

            if (_dbDapper.CheckInstructorRegisteredToGroup(InstructorID, GroupID))
            {
                json.Success = false;
                json.ErrorReason = InstructorsStrings.InstructorAlreadyAssigned;
                json.AllowOverRide = false;
                return Json(JsonConvert.SerializeObject(json), JsonRequestBehavior.AllowGet);
            }

            Trips trip = _dbDapper.GetTripByGroupID(GroupID);

            int? returnGroupID =
                _dbDapper.CheckIfInstructorIsInTrainingInTimeFrame(InternalID, GroupID, trip.DepartureDate,
                    trip.ReturningDate);

            if (returnGroupID != null)
            {
                json.Success = false;
                json.ErrorReason = InstructorsStrings.InstrucorNotAvailable;
                json.AllowOverRide = false;
                return Json(JsonConvert.SerializeObject(json), JsonRequestBehavior.AllowGet);
            }

            if (!_dbDapper.CheckInstructorValidity(InstructorID))
            {
                json.Success = false;
                json.ErrorReason = GroupFormsStrings.ManagerIsNotValid;
                json.AllowOverRide = true;
                return Json(JsonConvert.SerializeObject(json), JsonRequestBehavior.AllowGet);
            }

            json.success = true;

            return Json(JsonConvert.SerializeObject(json), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult AddManagerToGroup(string InstructorID, int GroupID)
        {
            dynamic json = new ExpandoObject();
            json.Success = true;

            bool deleteResults = _dbDapper.DeleteManagerFromGroupByGroupID(GroupID);
            Instructors _instructor = _dbDapper.GetInstructorByInstructorID(InstructorID);
            //bool results = _dbDapper.RegisterInstructorToGroup(GroupID, _instructor.ID, (int)eInstructorRoleType.ExpeditionManager);

            int RowID = _dbDapper.GetRecords<int>(QueriesRepository.AddInstructorToGroupWithoutSchoolID, new
            {
                GroupID,
                InstructorID = _instructor.ID,
                RoleID = eInstructorRoleType.ExpeditionManager,
                Comments = "",
                IsGroupManager = true,
                IsDeputyDirector = false
            }).FirstOrDefault();
            json.Instructor = _instructor;


            return Json(JsonConvert.SerializeObject(json), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [PermissionAuthorization(PermissionBitEnum.Form180_Adimistrative_Approval)]
        public JsonResult ChangeAdministrationFormStatus(int GroupFormID, bool FormStatus)
        {
            dynamic json = new ExpandoObject();
            json.Success = true;
            if (!((FunctionHelpers.User.RoleID == (int)eRole.Staf &&
                   FunctionHelpers.User.RoleTypeEnum == eRoleType.Administration) ||
                  (FunctionHelpers.User.RoleID == (int)eRole.Administrator)))
            {
                json.Sucess = false;
                json.ErrorText = "The user does not have permission to change the form status";

                return json;
            }

            var results = _dbDapper.UpdateOperation(QueriesRepository.UpdateAdministrative180FormStatus,
                new
                {
                    ID = GroupFormID, UpdateDate = DateTime.Now,
                    FormStatus = FormStatus ? (int)eGroupFormStatus.Approved : (int)eGroupFormStatus.Rejected
                });

            if (results)
            {
                json.FormStatus = true;
                CreatingGroupFormStatusUpdateNotification(GroupFormID,
                    FormStatus ? eGroupFormStatus.Approved : eGroupFormStatus.Rejected,
                    eFormChapterType.Administration);
            }
            else
            {
                json.FormStatus = false;
            }

            return Json(JsonConvert.SerializeObject(json), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public PartialViewResult AddCommentToForm(int FormID, string FormChapterType, string Comment)
        {
            dynamic json = new ExpandoObject();
            json.Success = true;

            FormCommentViewModel model = new FormCommentViewModel();
            model.Comment = Comment;
            model.FormID = FormID;
            model.FormChapterTypeEnum = FormChapterType.ToEnum<eFormChapterType>();
            model.FormChapterType = (int)model.FormChapterTypeEnum;
            model.UserID = FunctionHelpers.User.UserID;
            model.CreationDate = DateTime.Now;
            model.UpdateDate = model.CreationDate;

            int MessageID = _dbDapper.InsertOperation(QueriesRepository.InsertFormComment, new
            {
                model.FormChapterType,
                model.FormID,
                model.Comment,
                model.UserID,
                model.CreationDate,
                model.UpdateDate
            });

            if (MessageID == 0)
            {
                json.Success = false;
            }
            else
            {
                model.ID = MessageID;
                json.message = model;
            }

            IEnumerable<FormCommentViewModel> _comments =
                _dbDapper.GetRecords<FormCommentViewModel>(QueriesRepository.GetFormComments,
                    new { FormID, model.FormChapterType });

            return PartialView("_FormListOfComments", _comments);
        }

        #endregion

        #region Instructor Section

        [HttpGet]
        [PermissionAuthorization(PermissionBitEnum.Form180_Instructors_View)]
        public ActionResult Get180InstructorsForm(int SchoolID, int GroupID)
        {
            bool permissionAllowed = false;

            //Initiate new instance of instructor step
            GroupInstructorsForm180ViewModel model = new GroupInstructorsForm180ViewModel();

            model.GroupID = GroupID;
            model.GroupDetails.GroupID = GroupID;
            //Load the generic details
            model.SchoolData = _dbDapper
                .GetRecords<GroupFormSchoolData>(QueriesRepository.GetAdministrativeFormSchoolDetails,
                    new { SchoolID, GroupID }).FirstOrDefault();

            ////TODO: Check that the logged on user is a school manager and have access to the page.
            //if (User.IsInRole(eRole.SchoolManager.ToString()) && SchoolID == _dbDapper.GetSchoolIDByUserID(FunctionHelpers.User.UserID))
            //{
            //    permissionAllowed = true;
            //    // The user is not allowed to edit the form, therfore he is being directed to the login
            //}
            //else if ((User.IsInRole(eRole.CountyManager.ToString()) && FunctionHelpers.User.RegionID == model.SchoolData.RegionID))
            //{
            //    //Check if the user is county manager with the right region
            //    permissionAllowed = true;

            //}
            //else if (User.IsInRole(eRole.Administrator.ToString()))
            //{
            //    permissionAllowed = true;

            //}

            //if (!permissionAllowed)
            //{
            //    return Redirect("~/");
            //}


            // Check if a form has been created, if not it will be created
            model.GeneralFormData = _dbDapper.GetRecords<GroupFormViewModel>(QueriesRepository.GetGroup180FormDetails,
                new { model.GroupDetails.GroupID, model.SchoolData.SchoolID }).FirstOrDefault();

            //Get Instructor Form Details
            model.InstructorFormDetails = _dbDapper
                .GetRecords<InstructorFormViewModel>(QueriesRepository.GetGroupInstructorFormDetails,
                    new { FormID = model.GeneralFormData.ID }).FirstOrDefault();

            //Create the form if not exists
            if (model.InstructorFormDetails == null)
            {
                model.InstructorFormDetails = new InstructorFormViewModel();
                model.InstructorFormDetails.GroupFormID = model.GeneralFormData.ID;
                model.InstructorFormDetails.FormStatus = (int)eGroupFormStatus.New;
                model.InstructorFormDetails.CreationDate = DateTime.Now;
                model.InstructorFormDetails.UpdateDate = model.InstructorFormDetails.CreationDate;

                model.InstructorFormDetails.ID = _dbDapper.GetRecords<int>(
                    QueriesRepository.InsertGroupInstructorFormDetails, new
                    {
                        FormID = model.InstructorFormDetails.GroupFormID,
                        model.InstructorFormDetails.FormStatus,
                        model.InstructorFormDetails.CreationDate,
                        model.InstructorFormDetails.UpdateDate
                    }).FirstOrDefault();
            }

            model.GroupDetails = _dbDapper
                .GetRecords<GroupDetailsViewModel>(QueriesRepository.GetAdministrativeFormGroupDetails, new { GroupID })
                .FirstOrDefault();

            model.GeneralDetails.NumberOfBuses = model.GroupDetails.NumberofBuses;

            // Get List of Instructors
            model.Instructors = _dbDapper.GetRecords<GroupInstructorViewModel>(
                QueriesRepository.GetListofInstructorsInGroupsBySchoolID, new
                {
                    model.GroupID,
                    model.SchoolData.SchoolID
                });
            //Check how many instructors are associated with the school
            model.GeneralDetails.ActualNumberOfInstructors = model.Instructors.Count();
            model.IsValid = model.GeneralDetails.ActualNumberOfInstructors >=
                            model.GeneralDetails.MinimumNumberOfInstructors;

            //Set the minimum number of instructors - Allow submiting the form 
            if (model.SchoolData.NumberOfInstructors != null)
            {
                model.GeneralDetails.SchoolNumberOfInstructors = model.SchoolData.NumberOfInstructors;
                model.IsValid = model.GeneralDetails.ActualNumberOfInstructors >=
                                model.GeneralDetails.SchoolNumberOfInstructors;
            }

            //Load form messages
            model.FormComments.Comments = _dbDapper.GetRecords<FormCommentViewModel>(QueriesRepository.GetFormComments,
                new { FormID = model.GeneralFormData.ID, FormChapterType = (int)eFormChapterType.Instructors });
            model.FormComments.FormID = (int)model.GeneralFormData.ID;
            model.FormComments.FormChapterType = eFormChapterType.Instructors;

            //Check approval mode - this enabled the option to approve the form
            if ((model.InstructorFormDetails.GroupFormStatusEnum != eGroupFormStatus.Approved ||
                 model.InstructorFormDetails.GroupFormStatusEnum != eGroupFormStatus.New) &&
                PermissionLoader.CheckUserPermissions(PermissionBitEnum.Form180_Instructors_Approval))
            {
                model.ApprovalMode = true;
            }

            //Set form disable
            if (model.InstructorFormDetails.GroupFormStatusEnum == eGroupFormStatus.WaitingApproval ||
                model.InstructorFormDetails.GroupFormStatusEnum == eGroupFormStatus.Approved)
            {
                model.IsDisabled = true;
            }

            return View("Get180InstructorStepForm", model);
        }

        [HttpPost]
        [PermissionAuthorization(PermissionBitEnum.Form180_Instructors_Editing)]
        public ActionResult Save180InstructorsForm(GroupInstructorsForm180ViewModel model)
        {
            if (!(User.IsInRole(eRole.CountyManager.ToString()) || User.IsInRole(eRole.Administrator.ToString()) ||
                  User.IsInRole(eRole.SchoolManager.ToString())))
            {
                // The user is not allowed to edit the form, therfore he is being directed to the instructor page
                return RedirectToAction("Get180InstructorsForm", new { model.SchoolData.SchoolID, model.GroupID });
            }

            model.InstructorFormDetails.FormStatus = (int)eGroupFormStatus.WaitingApproval;

            //Update the form status
            bool results = _dbDapper.UpdateOperation(QueriesRepository.UpdateGroupInstructorFormStepStatus, new
            {
                model.InstructorFormDetails.FormStatus,
                UpdateDate = DateTime.Now,
                model.InstructorFormDetails.ID
            });

            if (results)
            {
                CreatingGroupFormStatusUpdateNotification((int)model.InstructorFormDetails.ID,
                    model.InstructorFormDetails.GroupFormStatusEnum, eFormChapterType.Instructors);
            }

            return RedirectToAction("Get180PlanForm", new { model.SchoolData.SchoolID, model.GroupID });
        }

        [HttpGet]
        [PermissionAuthorization(PermissionBitEnum.Form180_Instructors_Editing)]
        public PartialViewResult AddInstructor(int GroupID, int SchoolID)
        {
            AddInstructorViewModel model = new AddInstructorViewModel();
            model.GroupID = GroupID;
            model.SchoolID = SchoolID;

            return PartialView("_AddInstructor", model);
        }

        [HttpPost]
        [PermissionAuthorization(PermissionBitEnum.Form180_Instructors_Editing)]
        public ActionResult AddInstructor(AddInstructorViewModel model)
        {
            int RowID = _dbDapper.GetRecords<int>(QueriesRepository.AddInstructorToGroupWithSchoolID, new
            {
                model.GroupID,
                model.InstructorID,
                RoleID = eInstructorRoleType.AccompanyingMentor,
                model.Comments,
                model.IsGroupManager,
                model.SchoolID,
                model.IsDeputyDirector
            }).FirstOrDefault();

            return RedirectToAction("Get180InstructorsForm", new { model.SchoolID, model.GroupID });
        }

        [HttpGet]
        public ActionResult RemoveInstructor(int ID, string title, int GroupID, int SchoolID)
        {
            bool results = _dbDapper.ExecuteCommand(QueriesRepository.RemoveInstructorFromGroupByID, new { ID });

            return RedirectToAction("Get180InstructorsForm", new { SchoolID, GroupID });
        }

        [HttpPost]
        [PermissionAuthorization(PermissionBitEnum.Form180_Instructors_Approval)]
        public JsonResult ChangeInstructorFormStatus(int GroupFormID, bool FormStatus)
        {
            dynamic json = new ExpandoObject();
            json.Success = true;
            //if (!((FunctionHelpers.User.RoleID == (int)eRole.CountyManager) || (FunctionHelpers.User.RoleID == (int)eRole.Administrator)))
            //{
            //    json.Sucess = false;
            //    json.ErrorText = "The user does not have permission to change the form status";

            //    return json;
            //}

            var results = _dbDapper.UpdateOperation(QueriesRepository.UpdateGroupInstructorFormStepStatus,
                new
                {
                    ID = GroupFormID, UpdateDate = DateTime.Now,
                    FormStatus = FormStatus ? (int)eGroupFormStatus.Approved : (int)eGroupFormStatus.Rejected
                });

            if (results)
            {
                json.FormStatus = true;
                CreatingGroupFormStatusUpdateNotification(GroupFormID,
                    FormStatus ? eGroupFormStatus.Approved : eGroupFormStatus.Rejected, eFormChapterType.Instructors);
            }
            else
            {
                json.FormStatus = false;
            }

            return Json(JsonConvert.SerializeObject(json), JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Group Plan Section

        [HttpGet]
        [PermissionAuthorization(PermissionBitEnum.Form180_Plan_View)]
        public ActionResult Get180PlanForm(int SchoolID, int GroupID)
        {
            bool permissionAllowed = false;

            //Initiate new group plan object
            GroupPlanForm180ViewModel model = new GroupPlanForm180ViewModel();

            model.GroupID = GroupID;
            model.GroupDetails.GroupID = GroupID;
            //Load the generic details
            model.SchoolData = _dbDapper
                .GetRecords<GroupFormSchoolData>(QueriesRepository.GetAdministrativeFormSchoolDetails,
                    new { SchoolID, GroupID }).FirstOrDefault();


            ////TODO: Check that the logged on user is a school manager and have access to the page.
            //if (User.IsInRole(eRole.SchoolManager.ToString()) && SchoolID == _dbDapper.GetSchoolIDByUserID(FunctionHelpers.User.UserID))
            //{
            //    permissionAllowed = true;
            //    // The user is not allowed to edit the form, therfore he is being directed to the login
            //}
            //else if (User.IsInRole(eRole.Staf.ToString()) && (FunctionHelpers.User.RoleTypeEnum == eRoleType.PreparationPlan || FunctionHelpers.User.RoleTypeEnum == eRoleType.Management))
            //{
            //    //Check if the user is county manager with the right region
            //    permissionAllowed = true;

            //}
            //else if (User.IsInRole(eRole.Administrator.ToString()))
            //{
            //    permissionAllowed = true;

            //}

            //if (!permissionAllowed)
            //{
            //    return Redirect("~/");
            //}

            // Check if a form has been created, if not it will be created
            model.GeneralFormData = _dbDapper.GetRecords<GroupFormViewModel>(QueriesRepository.GetGroup180FormDetails,
                new { model.GroupDetails.GroupID, model.SchoolData.SchoolID }).FirstOrDefault();

            //Check if the group plan form has been created, if not we will create it

            model.GroupPlanFormDetails = _dbDapper
                .GetRecords<GroupPlanViewModel>(QueriesRepository.GetGroupPlan180FormDetails,
                    new { FormID = model.GeneralFormData.ID }).FirstOrDefault();

            if (model.GroupPlanFormDetails == null)
            {
                model.GroupPlanFormDetails = new GroupPlanViewModel();
                model.GroupPlanFormDetails.GroupFormID = model.GeneralFormData.ID;
                model.GroupPlanFormDetails.FormStatus = (int)eGroupFormStatus.New;
                model.GroupPlanFormDetails.CreationDate = DateTime.Now;
                model.GroupPlanFormDetails.UpdateDate = model.GroupPlanFormDetails.CreationDate;

                model.GroupPlanFormDetails.ID = _dbDapper.GetRecords<int>(
                    QueriesRepository.CreateGroupPlan180FormDetails, new
                    {
                        model.GroupPlanFormDetails.GroupFormID,
                        model.GroupPlanFormDetails.FormStatus,
                        model.GroupPlanFormDetails.CreationDate,
                        model.GroupPlanFormDetails.UpdateDate
                    }).FirstOrDefault();
            }

            model.GroupDetails = _dbDapper
                .GetRecords<GroupDetailsViewModel>(QueriesRepository.GetAdministrativeFormGroupDetails, new { GroupID })
                .FirstOrDefault();

            //Load Group Plans
            model.Subjects =
                _dbDapper.GetRecords<GroupPlanSubjectViewModel>(QueriesRepository.GetGroupPlanSubjects, null);

            //Load group plan details
            model.GroupPlansDetails = _dbDapper.GetRecords<GroupPlanDetails>(
                QueriesRepository.GetGroupPlanDetailsByFormID, new { FormID = model.GroupPlanFormDetails.ID });

            //Load form messages
            model.FormComments.Comments = _dbDapper.GetRecords<FormCommentViewModel>(QueriesRepository.GetFormComments,
                new { FormID = model.GeneralFormData.ID, FormChapterType = (int)eFormChapterType.Plan });
            model.FormComments.FormID = (int)model.GeneralFormData.ID;
            model.FormComments.FormChapterType = eFormChapterType.Plan;

            //Perform Validation
            model.IsValid = true;
            foreach (var _subject in model.Subjects)
            {
                if (model.IsValid)
                {
                    model.IsValid = _subject.HoursRequired <= model.GroupPlansDetails
                        .Where(x => x.GroupPlanSubjectID == _subject.ID).Sum(x => x.PlanHours);
                }
            }

            //Check approval mode - this enabled the option to approve the form
            if ((model.GroupPlanFormDetails.GroupFormStatusEnum != eGroupFormStatus.Approved ||
                 model.GroupPlanFormDetails.GroupFormStatusEnum != eGroupFormStatus.New) &&
                PermissionLoader.CheckUserPermissions(PermissionBitEnum.Form180_Plan_Approval))
            {
                model.ApprovalMode = true;
            }

            //Set form disable
            if (model.GroupPlanFormDetails.GroupFormStatusEnum == eGroupFormStatus.WaitingApproval ||
                model.GroupPlanFormDetails.GroupFormStatusEnum == eGroupFormStatus.Approved)
            {
                model.IsDisabled = true;
            }

            return View("Get180PlanForm", model);
        }

        [HttpGet]
        [PermissionAuthorization(PermissionBitEnum.Form180_Plan_Editing)]
        public PartialViewResult AddPlan(int ID, int GroupID, int SchoolID)
        {
            GroupPlanDetails model = new GroupPlanDetails();
            model.GroupPlanID = ID;
            model.PlanDate = DateTime.Now;
            model.Subjects.Items = _dbDapper
                .GetRecords<CustomSelectListItem>(QueriesRepository.GetGroupPlanSubjects, null).Select(x =>
                    new CustomSelectListItem
                    {
                        ID = x.ID,
                        Name = x.Name
                    }).ToList();

            return PartialView("_AddPlan", model);
        }

        [HttpPost]
        [PermissionAuthorization(PermissionBitEnum.Form180_Plan_Editing)]
        public ActionResult AddPlan(GroupPlanDetails model)
        {
            if (ModelState.IsValid)
            {
                model.CreationDate = DateTime.Now;
                model.UpdateDate = model.CreationDate;

                var results = _dbDapper.UpdateOperation(QueriesRepository.CreateGroupPlanDetails, new
                {
                    model.GroupPlanID,
                    model.Instructor,
                    model.PlanHours,
                    model.PlanDate,
                    model.PlanLocation,
                    model.CreationDate,
                    model.UpdateDate,
                    model.GroupPlanSubjectID,
                    model.PlanName
                });

                return RedirectToAction("Get180PlanForm", new { model.SchoolID, model.GroupID });
            }

            return PartialView("_AddPlan", model);
        }

        [HttpGet]
        [PermissionAuthorization(PermissionBitEnum.Form180_Plan_Editing)]
        public PartialViewResult UpdatePlan(int ID, int GroupID, int SchoolID)
        {
            GroupPlanDetails model = new GroupPlanDetails();
            model = _dbDapper.GetRecords<GroupPlanDetails>(QueriesRepository.GetGroupPlanDetailsByID, new { ID })
                .FirstOrDefault();
            model.Subjects.Items = _dbDapper
                .GetRecords<CustomSelectListItem>(QueriesRepository.GetGroupPlanSubjects, null).Select(x =>
                    new CustomSelectListItem
                    {
                        ID = x.ID,
                        Name = x.Name
                    }).ToList();
            model.IsUpdate = true;
            model.GroupID = GroupID;
            model.SchoolID = SchoolID;

            return PartialView("_UpdatePlan", model);
        }

        [HttpPost]
        [PermissionAuthorization(PermissionBitEnum.Form180_Plan_Editing)]
        public ActionResult UpdatePlan(GroupPlanDetails model)
        {
            if (ModelState.IsValid)
            {
                model.UpdateDate = DateTime.Now;

                var results = _dbDapper.UpdateOperation(QueriesRepository.UpdateGroupPlanDetailsByID, new
                {
                    model.ID,
                    model.Instructor,
                    model.PlanHours,
                    model.PlanDate,
                    model.PlanLocation,
                    model.UpdateDate,
                    model.GroupPlanSubjectID,
                    model.PlanName
                });

                return RedirectToAction("Get180PlanForm", new { model.SchoolID, model.GroupID });
            }

            return PartialView("_UpdatePlan", model);
        }

        [HttpGet]
        [PermissionAuthorization(PermissionBitEnum.Form180_Plan_Editing)]
        public ActionResult DeletePlan(int ID, string title, int GroupID, int SchoolID)
        {
            var results = _dbDapper.UpdateOperation(QueriesRepository.DeleteGroupPlanDetailsByID, new { ID });
            return RedirectToAction("Get180PlanForm", new { SchoolID, GroupID });
        }

        [HttpPost]
        [PermissionAuthorization(PermissionBitEnum.Form180_Plan_Editing)]
        public ActionResult Save180GroupPlanForm(GroupPlanForm180ViewModel model)
        {
            if (!(User.IsInRole(eRole.Staf.ToString()) || User.IsInRole(eRole.Administrator.ToString()) ||
                  User.IsInRole(eRole.SchoolManager.ToString())))
            {
                // The user is not allowed to edit the form, therfore he is being directed to the instructor page
                return RedirectToAction("Get180PlanForm", new { model.SchoolData.SchoolID, model.GroupID });
            }

            model.GroupPlanFormDetails.FormStatus = (int)eGroupFormStatus.WaitingApproval;

            //Update the form status
            bool results = _dbDapper.UpdateOperation(QueriesRepository.UpdateGroupPlanForm, new
            {
                model.GroupPlanFormDetails.FormStatus,
                UpdateDate = DateTime.Now,
                model.GroupPlanFormDetails.EdicationPlanDetails,
                model.GroupPlanFormDetails.ID
            });

            if (results)
            {
                CreatingGroupFormStatusUpdateNotification((int)model.GroupPlanFormDetails.ID,
                    model.GroupPlanFormDetails.GroupFormStatusEnum, eFormChapterType.Plan);
            }


            return RedirectToAction("GetGroup180Forms", new { model.GroupID });
        }

        [HttpPost]
        [PermissionAuthorization(PermissionBitEnum.Form180_Plan_Approval)]
        public JsonResult ChangePlanFormStatus(int GroupFormID, bool FormStatus)
        {
            dynamic json = new ExpandoObject();
            json.Success = true;
            if (!((FunctionHelpers.User.RoleID == (int)eRole.Staf &&
                   (FunctionHelpers.User.RoleTypeEnum == eRoleType.PreparationPlan ||
                    FunctionHelpers.User.RoleTypeEnum == eRoleType.Management)) ||
                  (FunctionHelpers.User.RoleID == (int)eRole.Administrator)))
            {
                json.Sucess = false;
                json.ErrorText = "The user does not have permission to change the form status";

                return json;
            }

            var results = _dbDapper.UpdateOperation(QueriesRepository.UpdateGroupPlanFormStatus,
                new
                {
                    ID = GroupFormID, UpdateDate = DateTime.Now,
                    FormStatus = FormStatus ? (int)eGroupFormStatus.Approved : (int)eGroupFormStatus.Rejected
                });

            if (results)
            {
                json.FormStatus = true;
                CreatingGroupFormStatusUpdateNotification(GroupFormID,
                    FormStatus ? eGroupFormStatus.Approved : eGroupFormStatus.Rejected, eFormChapterType.Plan);
            }
            else
            {
                json.FormStatus = false;
            }

            return Json(JsonConvert.SerializeObject(json), JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Form 60 General

        [HttpGet]
        [PermissionAuthorization(PermissionBitEnum.Form60_View)]
        public ActionResult GetForm60(int GroupID)
        {
            // Get Form 60
            GroupForm60ViewModel model = new GroupForm60ViewModel();
            model = _dbDapper.GetRecords<GroupForm60ViewModel>(QueriesRepository.GetGroupForm60, new { GroupID })
                .FirstOrDefault();

            // Check if the Form exists, if not create it
            if (model == null || model.ID < 1)
            {
                model = new GroupForm60ViewModel();
                model.GroupID = GroupID;
                model.CreationDate = DateTime.Now;
                model.UpdateDate = model.CreationDate;
                model.FormStatus = (int)eGroupForm60Status.New;
                model.AgencyApprovalStatus = (int)eGroupFormStatus.New;
                model.PlanApprovalStatus = (int)eGroupFormStatus.New;
                model.ID = _dbDapper.InsertOperationReturnsID(QueriesRepository.CreateGroupForm60, new
                {
                    model.GroupID,
                    model.CreationDate,
                    model.FormStatus,
                    model.UpdateDate,
                    model.PlanApprovalStatus,
                    model.AgencyApprovalStatus
                });
            }

            // Get Data For Form 60
            model.GroupFolderData.GroupFolderData = _dbDapper.GetGroupFolderDataByID(GroupID);
            model.GroupFolderData.AssignedParticipants = _dbDapper.GetNumberOfAssignedParticipantsInGroup(GroupID);
            model.ManagerData = _dbDapper.GetRecords<GroupManagerData>(QueriesRepository.GetGroupManagerDataByGroupID,
                new { model.GroupID, RoleID = eInstructorRoleType.ExpeditionManager }).FirstOrDefault();

            // Get schools data
            model.Schools =
                _dbDapper.GetRecords<GroupSchoolsViewModel>(QueriesRepository.GetForm60Schools, new { GroupID });

            return View("Form60", model);
        }

        [PermissionAuthorization(PermissionBitEnum.Form60_EditManagerDetails)]
        [HttpGet]
        public PartialViewResult UpdateGroupManagerDetails(int InstructorID, int GroupID)
        {
            Form60GroupManagerViewModel model = _dbDapper
                .GetRecords<Form60GroupManagerViewModel>(QueriesRepository.GetGroupManagerBasicInfoByID,
                    new { InstructorID }).FirstOrDefault();
            model.GroupID = GroupID;
            return PartialView("_UpdateManagerEmail", model);
        }

        [HttpPost]
        [PermissionAuthorization(PermissionBitEnum.Form60_EditManagerDetails)]
        public ActionResult UpdateManagerDetails(Form60GroupManagerViewModel model)
        {
            bool results = _dbDapper.UpdateOperation(QueriesRepository.UpdateGroupManagerBasicData, new
            {
                model.ID,
                model.CellPhone,
                model.HomePhone,
                model.Email
            });

            return RedirectToAction("GetForm60", new { model.GroupID });
        }

        [HttpGet]
        [PermissionAuthorization(PermissionBitEnum.Form60_View)]
        public ActionResult Form60Report(int GroupID)
        {
            ReportViewerViewModel report = new ReportViewerViewModel();
            report.ReportUrl = ReportsRepository.GetReportFullPath(ReportsRepository.Form60);
            report.ReportParameters.Add("GroupID", GroupID.ToString());

            // Creating Report Viewer Controller Instanse and passing the report model
            var controller = DependencyResolver.Current.GetService<ReportViewerReportsController>();
            controller.ControllerContext = new ControllerContext(this.Request.RequestContext, controller);
            var results = controller.ExecuteReport(report);
            return results;
        }

        [HttpPost]
        [PermissionAuthorization(PermissionBitEnum.Form60_View, PermissionBitEnum.Form60_FormStatus)]
        public JsonResult UpdateForm60FinalStatus(int GroupID, bool FormStatus)
        {
            dynamic json = new ExpandoObject();
            json.Success = true;

            var results = _dbDapper.UpdateOperation(QueriesRepository.UpdateForm60FinalStatus, new
            {
                GroupID,
                Status = FormStatus ? (int)eGroupForm60Status.Approved : (int)eGroupForm60Status.Rejected,
                FunctionHelpers.User.UserID
            });

            if (results)
            {
                json.FormStatus = true;
                CreatingForm60UpdateNotification(GroupID);
            }
            else
            {
                json.FormStatus = false;
            }

            return Json(JsonConvert.SerializeObject(json), JsonRequestBehavior.AllowGet);
        }

        public void CreatingForm60UpdateNotification(int GroupID)
        {
            GroupForm60ViewModel GroupFormData = _dbDapper
                .GetRecords<GroupForm60ViewModel>(QueriesRepository.GetGroupForm60, new { GroupID }).FirstOrDefault();
            NotificationForm60StatusUpdate notificationsData = new NotificationForm60StatusUpdate();
            notificationsData.FormStatus = GroupFormData.FormStatusEnum.GetDescription();
            notificationsData.GroupExternalID = GroupFormData.GroupExternalID.ToString();
            notificationsData.GroupID = GroupFormData.GroupID;
            notificationsData.UpdateDate = GroupFormData.UpdateDate;
            notificationsData.FormUrl =
                Url.Action("GetForm60", "GroupForms", new { GroupFormData.GroupID }, Request.Url.Scheme);


            List<NotificationTemplate> _notificationTemplates = new List<NotificationTemplate>();
            _notificationTemplates = _dbDapper.GetRecords<NotificationTemplate>(
                QueriesRepository.GetNotificationTemplatesByTrrigerType,
                new { TriggerType = (int)eNotificationTriggerType.GroupForm60Updated });
            IEnumerable<UserViewModel> _UsersToSend;
            foreach (var _notification in _notificationTemplates)
            {
                // IF THE NOTIFICATION IS FOR GROUP MANAGER THAN WE GET THE MANAGER OF THE GROUP ONLY
                if (_notification.UserTypeEnum == eRole.GroupManager)
                {
                    _UsersToSend = _dbDapper.GetRecords<UserViewModel>(
                        QueriesRepository.GetGroupManagersforNotifications, new { GroupFormData.GroupID });
                }
                // IF THE NOTIFICATION IS FOR AGENCY MANAGER THAN WE GET THE AGENCY MANAGERS OF THE GROUP ONLY
                else if (_notification.UserTypeEnum == eRole.Agent)
                {
                    _UsersToSend = _dbDapper.GetRecords<UserViewModel>(
                        QueriesRepository.GetGroupAgencyUsersforNotifications, new { GroupFormData.GroupID });
                }
                else
                {
                    _UsersToSend = _dbDapper.GetRecords<UserViewModel>(
                        QueriesRepository.GetUsersByAllNotificationParameters,
                        new { UserType = _notification.UserType, _notification.UserPermissionRole });
                }

                foreach (var _user in _UsersToSend)
                {
                    if (NotificationHelper.IsValidEmail(_user.Email))
                    {
                        Notifications _notificationToSend = new Notifications();
                        _notificationToSend.NotificationTempateID = _notification.ID;
                        _notificationToSend.MessageSubject = _notification.Subject;
                        _notificationToSend.UserID = _user.ID;
                        _notificationToSend.EmailAddress = _user.Email;
                        _notificationToSend.CallToAction =
                            Url.Action("GetForm60", "GroupForms", new { GroupFormData.GroupID });
                        _notificationToSend.CreationDate = DateTime.Now;
                        _notificationToSend.IsRead = false;

                        // Complete the Custom Notification Details
                        notificationsData.UserID = _user.ID;
                        notificationsData.UserFirstName = _user.FirstName;
                        notificationsData.UserLastName = _user.LastName;

                        _notificationToSend.NotificationStatus = eNotificationStatus.Pendding;
                        _notificationToSend.MessageBody = notificationsData.ConvertProperties(_notification.Message);

                        // TODO: To insert all of the notifications at once. 
                        bool _result = _dbDapper.ExecuteCommand(QueriesRepository.InsertNotification, new
                        {
                            _notificationToSend.NotificationTempateID,
                            _notificationToSend.UserID,
                            _notificationToSend.EmailAddress,
                            _notificationToSend.NotificationStatus,
                            _notificationToSend.MessageSubject,
                            _notificationToSend.MessageBody,
                            _notificationToSend.CallToAction,
                            _notificationToSend.CreationDate,
                            _notificationToSend.IsRead
                        });
                    }
                }
            }
        }

        private void UpdateForm60FinalStatusAccordingToOtherStatuses(int GroupID)
        {
            // Get the group approvals and update the final approval accordigly

            var approvals = _dbDapper.GetRecords<dynamic>(QueriesRepository.GetForm60Statuses, new { GroupID })
                .FirstOrDefault();
            eGroupForm60Status Status = eGroupForm60Status.InApprovalProcess;
            if (approvals.AgencyApprovalStatus == (int)eGroupFormStatus.Approved &&
                approvals.PlanApprovalStatus == (int)eGroupFormStatus.Approved)
            {
                Status = eGroupForm60Status.WaitingFinalApproval;
            }
            else if (approvals.AgencyApprovalStatus != (int)eGroupFormStatus.Approved &&
                     approvals.PlanApprovalStatus == (int)eGroupFormStatus.Approved)
            {
                Status = eGroupForm60Status.WaitingApprovalAgency;
            }
            else if (approvals.AgencyApprovalStatus == (int)eGroupFormStatus.Approved &&
                     approvals.PlanApprovalStatus != (int)eGroupFormStatus.Approved)
            {
                Status = eGroupForm60Status.WaitingPlanApproval;
            }

            var results = _dbDapper.UpdateOperation(QueriesRepository.UpdateForm60FinalStatusOnly, new
            {
                GroupID,
                Status
            });
        }

        [HttpPost]
        [PermissionAuthorization(PermissionBitEnum.Form60_View, PermissionBitEnum.Form60_AgencyApprovalStatus)]
        public JsonResult UpdateForm60AgencyStatus(int GroupID, bool FormStatus)
        {
            dynamic json = new ExpandoObject();
            json.Success = true;

            var results = _dbDapper.UpdateOperation(QueriesRepository.UpdateForm60AgencyApprovalStatus, new
            {
                GroupID,
                Status = FormStatus ? (int)eGroupFormStatus.Approved : (int)eGroupFormStatus.Rejected,
                FunctionHelpers.User.UserID
            });


            if (results)
            {
                json.FormStatus = true;
                UpdateForm60FinalStatusAccordingToOtherStatuses(GroupID);
                CreatingForm60UpdateNotification(GroupID);
            }
            else
            {
                json.FormStatus = false;
            }

            return Json(JsonConvert.SerializeObject(json), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [PermissionAuthorization(PermissionBitEnum.Form60_View, PermissionBitEnum.Form60_PlanApprovalStatus)]
        public JsonResult UpdateForm60PlanStatus(int GroupID, bool FormStatus)
        {
            dynamic json = new ExpandoObject();
            json.Success = true;

            var results = _dbDapper.UpdateOperation(QueriesRepository.UpdateForm60PlanApprovalStatus, new
            {
                GroupID,
                Status = FormStatus ? (int)eGroupFormStatus.Approved : (int)eGroupFormStatus.Rejected,
                FunctionHelpers.User.UserID
            });

            if (results)
            {
                json.FormStatus = true;
                UpdateForm60FinalStatusAccordingToOtherStatuses(GroupID);
                CreatingForm60UpdateNotification(GroupID);
            }
            else
            {
                json.FormStatus = false;
            }

            return Json(JsonConvert.SerializeObject(json), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [PermissionAuthorization(PermissionBitEnum.Form60_View, PermissionBitEnum.Form60_FormSubmit)]
        public ActionResult SubmitForm60(int GroupID)
        {
            var results = _dbDapper.UpdateOperation(QueriesRepository.UpdateForm60FinalStatus, new
            {
                GroupID,
                Status = (int)eGroupFormStatus.WaitingApproval,
                FunctionHelpers.User.UserID
            });

            // Update Plan Status
            var Planresults = _dbDapper.UpdateOperation(QueriesRepository.UpdateForm60PlanApprovalStatus, new
            {
                GroupID,
                Status = (int)eGroupFormStatus.WaitingApproval,
                FunctionHelpers.User.UserID
            });

            // Update fligth Status
            var FLightresults = _dbDapper.UpdateOperation(QueriesRepository.UpdateForm60AgencyApprovalStatus, new
            {
                GroupID,
                Status = (int)eGroupFormStatus.WaitingApproval,
                FunctionHelpers.User.UserID
            });

            var FormUpdateDate = _dbDapper.UpdateOperation(QueriesRepository.UpdateForm60UpdateDate, new
            {
                Date = DateTime.Now,
                GroupID
            });

            CreatingForm60UpdateNotification(GroupID);
            return RedirectToAction("GetForm60", new { GroupID });
        }

        #endregion

        #region Form60 Flights

        [HttpGet]
        [PermissionAuthorization(PermissionBitEnum.Form60_Flights_View)]
        public ActionResult Get60Flights(int GroupID)
        {
            // Load Form 60 data
            GroupActualFlightsViewModel model = new GroupActualFlightsViewModel();

            model.Form60Data = _dbDapper
                .GetRecords<GroupForm60ViewModel>(QueriesRepository.GetGroupForm60, new { GroupID }).FirstOrDefault();
            model.Form60Data.ChapterType = eFormChapterType.Flights;

            // Load Group Flights Data
            model.Flights =
                _dbDapper.GetRecords<GroupActualFlightViewModel>(QueriesRepository.GetGroupActualFlights,
                    new { GroupID });

            // Load group general details
            model.GroupDetails = _dbDapper
                .GetRecords<GroupGeneralDetailsViewModel>(QueriesRepository.GetForm60GroupGeneralDetails,
                    new { GroupID }).FirstOrDefault();
            // GET THE ACTUAL NUMBER OF PARTICIPANTS
            model.GroupDetails.NumberofParticipants = _dbDapper.GetNumberOfAssignedParticipantsInGroup(GroupID);
            model.GroupDetails.Trip = _dbDapper
                .GetRecords<GroupTripDetailsViewModel>(QueriesRepository.GetForm60GroupTripDetails, new { GroupID })
                .FirstOrDefault();

            // UPDATE FORM 60 STATUS TO IN PROGRESS
            if (model.Form60Data.FormStatusEnum == eGroupForm60Status.New || model.Form60Data.FormStatus == null)
            {
                var results = _dbDapper.UpdateOperation(QueriesRepository.UpdateForm60FinalStatusOnly, new
                {
                    GroupID,
                    Status = eGroupForm60Status.InApprovalProcess
                });
            }

            // SHOW AN ALTERT IF THE NUMBER OF PARTICIPANTS IS NOT ASSIGNED TO FLIGHTS
            if (model.Flights.Sum(f => f.NumberOfPassengers) < (model.GroupDetails.NumberofParticipants * 2))
            {
                model.Warnings.Add(new WarningModel()
                {
                    Message = GroupFormsStrings.AssignedParticipatsToActualFlightsWarnings,
                    WarningType = WarningType.Warning
                });
            }

            //Load form messages
            model.FormComments.Comments = _dbDapper.GetRecords<FormCommentViewModel>(QueriesRepository.GetFormComments,
                new { FormID = model.Form60Data.ID, FormChapterType = (int)eFormChapterType.Flights });
            model.FormComments.FormID = model.Form60Data.ID;
            model.FormComments.FormChapterType = eFormChapterType.Flights;

            //SET FORM ON VIEW MODE - IF THE FORM IS APPROVED AND THE USER IS NOT ADMIN THAN THE FORM IS IN VIEW MODE
            if (model.Form60Data.AgencyApprovalStatusEnum == eGroupFormStatus.Approved)
            {
                model.Form60Data.EditMode = false;
            }
            else if (model.Form60Data.AgencyApprovalStatusEnum == eGroupFormStatus.WaitingApproval &&
                     !PermissionLoader.CheckUserPermissions(PermissionBitEnum.Form60_AgencyApprovalStatus))
            {
                model.Form60Data.EditMode = false;
            }

            // SET THE FORM AS VIEW IF THE USER IS MANAGER AND NOT NEW

            return View("GroupForm60ActualFlights", model);
        }

        [HttpGet]
        [PermissionAuthorization(PermissionBitEnum.Form60_Flights_Edit)]
        public PartialViewResult AddFlight(int GroupID)
        {
            GroupActualFlightViewModel model = new GroupActualFlightViewModel();
            model.GroupID = GroupID;

            model.GroupDetails = _dbDapper
                .GetRecords<GroupGeneralDetailsViewModel>(QueriesRepository.GetForm60GroupGeneralDetails,
                    new { GroupID }).FirstOrDefault();
            model.TripDetails = _dbDapper
                .GetRecords<TripLightDetailsViewModel>(QueriesRepository.GetGroupTripDetails, new { GroupID })
                .FirstOrDefault();
            model.GroupDetails.NumberofParticipants = _dbDapper.GetNumberOfAssignedParticipantsInGroup(GroupID);
            // Set the number of required passengers by direction
            model.TotalLoggedPaggengers =
                _dbDapper.GetRecords<GroupTotalPassengersByDirection>(
                    QueriesRepository.GetNumberOfPassengersByDirection, new { GroupID });
            model.GoingNumberOfPassgengersRequired = model.GroupDetails.NumberofParticipants -
                                                     model.TotalLoggedPaggengers
                                                         .Where(t => t.DirectionEnum == eFlightDirection.Going)
                                                         .Sum(t => t.NumberOfPassengers);
            model.ReturningNumberOfPassgengersRequired = model.GroupDetails.NumberofParticipants -
                                                         model.TotalLoggedPaggengers
                                                             .Where(t => t.DirectionEnum == eFlightDirection.Return)
                                                             .Sum(t => t.NumberOfPassengers);

            model.Departure = model.TripDetails.DepartureDate;
            model.Arrival = model.TripDetails.ReturningDate;

            return PartialView("_ManageFlight", model);
        }

        [HttpGet]
        [PermissionAuthorization(PermissionBitEnum.Form60_Flights_Edit)]
        public PartialViewResult EditFlight(int FlightID)
        {
            GroupActualFlightViewModel model = new GroupActualFlightViewModel();
            model = _dbDapper
                .GetRecords<GroupActualFlightViewModel>(QueriesRepository.GetActualFlightByID, new { FlightID })
                .FirstOrDefault();

            model.GroupDetails = _dbDapper
                .GetRecords<GroupGeneralDetailsViewModel>(QueriesRepository.GetForm60GroupGeneralDetails,
                    new { model.GroupID }).FirstOrDefault();
            model.TripDetails = _dbDapper
                .GetRecords<TripLightDetailsViewModel>(QueriesRepository.GetGroupTripDetails, new { model.GroupID })
                .FirstOrDefault();

            // Set the number of required passengers by direction
            model.TotalLoggedPaggengers =
                _dbDapper.GetRecords<GroupTotalPassengersByDirection>(
                    QueriesRepository.GetNumberOfPassengersByDirection, new { model.GroupDetails.GroupID });
            model.GoingNumberOfPassgengersRequired = model.GroupDetails.NumberofParticipants -
                                                     model.TotalLoggedPaggengers
                                                         .Where(t => t.DirectionEnum == eFlightDirection.Going)
                                                         .Sum(t => t.NumberOfPassengers);
            model.ReturningNumberOfPassgengersRequired = model.GroupDetails.NumberofParticipants -
                                                         model.TotalLoggedPaggengers
                                                             .Where(t => t.DirectionEnum == eFlightDirection.Return)
                                                             .Sum(t => t.NumberOfPassengers);

            model.ArrivalTimeValue = model.Arrival.ToString("HH:mm");
            model.DepartureTimeValue = model.Departure.ToString("HH:mm");
            model.IsUpdate = true;
            return PartialView("_ManageFlight", model);
        }

        [HttpPost]
        [PermissionAuthorization(PermissionBitEnum.Form60_Flights_Edit)]
        public ActionResult ManageFlight(GroupActualFlightViewModel model)
        {
            if (ModelState.IsValid)
            {
                if (model.IsUpdate)
                {
                    string departure = $"{model.Departure.ToString("dd/MM/yyyy")} {model.DepartureTimeValue}";
                    model.Departure = DateTime.ParseExact(departure, AppConfig.DateTimeWithTime24FormatParsing,
                        System.Globalization.CultureInfo.CurrentCulture, System.Globalization.DateTimeStyles.None);

                    string arrival = $"{model.Arrival.ToString("dd/MM/yyyy")} {model.ArrivalTimeValue}";
                    model.Arrival = DateTime.ParseExact(arrival, AppConfig.DateTimeWithTime24FormatParsing,
                        System.Globalization.CultureInfo.CurrentCulture, System.Globalization.DateTimeStyles.None);
                    model.UpdateDate = DateTime.Now;

                    bool results = _dbDapper.UpdateOperation(QueriesRepository.UpdateActualFlight, new
                    {
                        model.Direction,
                        model.FlightNumber,
                        model.Airline,
                        model.Departure,
                        model.Arrival,
                        model.NumberOfPassengers,
                        model.UpdateDate,
                        FlightID = model.ID,
                        model.DestinationLocationID,
                        model.ArrivalLocationID
                    });
                }
                else
                {
                    //Setup new entry

                    string departure = $"{model.Departure.ToString("dd/MM/yyyy")} {model.DepartureTimeValue}";
                    model.Departure = DateTime.ParseExact(departure, AppConfig.DateTimeWithTime24FormatParsing,
                        System.Globalization.CultureInfo.CurrentCulture, System.Globalization.DateTimeStyles.None);

                    string arrival = $"{model.Arrival.ToString("dd/MM/yyyy")} {model.ArrivalTimeValue}";
                    model.Arrival = DateTime.ParseExact(arrival, AppConfig.DateTimeWithTime24FormatParsing,
                        System.Globalization.CultureInfo.CurrentCulture, System.Globalization.DateTimeStyles.None);

                    model.CreationDate = DateTime.Now;
                    model.UpdateDate = model.CreationDate;

                    model.ID = _dbDapper.InsertOperationReturnsID(QueriesRepository.AddNewActualFlight, new
                    {
                        model.Direction,
                        model.FlightNumber,
                        model.Airline,
                        model.Departure,
                        model.Arrival,
                        model.NumberOfPassengers,
                        model.GroupID,
                        model.CreationDate,
                        model.UpdateDate,
                        model.DestinationLocationID,
                        model.ArrivalLocationID
                    });

                    return RedirectToAction("Get60Flights", new { model.GroupID });
                }
            }

            return RedirectToAction("Get60Flights", new { model.GroupID });
        }

        [HttpGet]
        [PermissionAuthorization(PermissionBitEnum.Form60_Flights_Edit)]
        public PartialViewResult DeleteActualFlight(int ID, string name, int GroupID)
        {
            DeleteViewModel model = new DeleteViewModel();
            model.ID = ID;
            model.IsDeletable = true;
            model.Title = name;
            model.Action = "DeleteActualFlight";
            model.Controller = "GroupForms";
            model.Header = GroupFormsStrings.DeleteActualFlight;

            model.ReturnRefirectProperties = new List<int>();
            model.ReturnRefirectProperties.Add(GroupID);

            return PartialView("_DeleteItem", model);
        }

        [HttpPost]
        [PermissionAuthorization(PermissionBitEnum.Form60_Flights_Edit)]
        public ActionResult DeleteActualFlight(DeleteViewModel model)
        {
            // Delete the flight from the database
            bool results = _dbDapper.UpdateOperation(QueriesRepository.DeleteActualFlight, new { model.ID });

            return RedirectToAction("Get60Flights", new { GroupID = model.ReturnRefirectProperties[0] });
        }

        [HttpGet]
        [PermissionAuthorization(PermissionBitEnum.Form60_Contacts_View)]
        public ActionResult PrintGet60Flights(int GroupID)
        {
            return new ActionAsPdf("Get60Flights", new { GroupID })
            {
                FileName = "60Flights.pdf"
            };
        }

        #endregion


        #region Form60 Plan

        [HttpGet]
        [PermissionAuthorization(PermissionBitEnum.Form60_TripPlan_View)]
        public ActionResult GetForm80TripPlan(int GroupID, DateTime? TripDayDate)
        {
            GroupTripPlanForDay model = new GroupTripPlanForDay();

            // Load Form 60 Data
            model.Form60Data = _dbDapper
                .GetRecords<GroupForm60ViewModel>(QueriesRepository.GetGroupForm60, new { GroupID }).FirstOrDefault();
            model.Form60Data.ChapterType = eFormChapterType.TripPlan;

            // Load group general details
            model.GroupDetails = _dbDapper
                .GetRecords<GroupGeneralDetailsViewModel>(QueriesRepository.GetForm60GroupGeneralDetails,
                    new { GroupID }).FirstOrDefault();
            model.GroupDetails.Trip = _dbDapper
                .GetRecords<GroupTripDetailsViewModel>(QueriesRepository.GetForm60GroupTripDetails, new { GroupID })
                .FirstOrDefault();

            // Initiate Dates for trip array
            DateTime? LandingDateAndTime = _dbDapper
                .GetRecords<DateTime?>(QueriesRepository.GetLastArrivalTimeOnDayOfArrivalByGroupIDActual,
                    new { GroupID }).FirstOrDefault();
            DateTime? DepartureDateAndTime = _dbDapper
                .GetRecords<DateTime?>(QueriesRepository.GetLastArrivalTimeOnDayOfDepartureByGroupIDActual,
                    new { GroupID }).FirstOrDefault();
            bool HasActualFlights = LandingDateAndTime != null && DepartureDateAndTime != null;
            if (HasActualFlights)
            {
                model.setTripDates((DateTime)LandingDateAndTime, (DateTime)DepartureDateAndTime);
            }

            // Set Current Date
            if (TripDayDate == null)
            {
                model.CurrentDate = LandingDateAndTime == null
                    ? model.GroupDetails.Trip.DepartureDate
                    : (DateTime)LandingDateAndTime;
                model.DayNumber = 1;
            }
            else
            {
                model.CurrentDate = (DateTime)TripDayDate;
                model.DayNumber = model.TripDates.Where(t => t.Key == model.CurrentDate.Date).FirstOrDefault().Value;
            }

            // Get All Acticities for a date
            model.Activities = GetActicities(model.CurrentDate, model.GroupDetails.GroupID, null)
                .OrderBy(m => m.EndTime);

            // CHECK IF ACTUAL FLIHTS HAS BEEN DEFINED, IF NOT THEN RETURN AN ERROR
            if (HasActualFlights)
            {
                // Check if the first day and check if mandatory acticities has been created
                if (model.DayNumber == 1)
                {
                    IEnumerable<TripActicity> resultsFirstActivity =
                        model.Activities.Where(a => a.ActivityTypeEnum == eActivityType.PreparationTime);
                    bool HasPreparationActicity = resultsFirstActivity.Count() > 0 &&
                                                  resultsFirstActivity.Count(a =>
                                                      a.ActivityTypeEnum == eActivityType.PreparationTime) > 0 &&
                                                  resultsFirstActivity.FirstOrDefault().ActivityTypeEnum ==
                                                  eActivityType.PreparationTime;
                    // CURRENTLY IF NO FLIGHT AVIALABLE NO PLAN - WE MIGHT CHANGE THAT.
                    if (LandingDateAndTime == null)
                    {
                        //LandingDateAndTime = _dbDapper.GetRecords<DateTime>(QueriesRepository.GetLastArrivalTimeOnDayOfArrivalByTripID, new { TripID = model.GroupDetails.Trip.ID }).FirstOrDefault();

                        // REMOVE THE OPTION TO ADD ANY ACTIVITY
                        model.ActivitiesMenu.Clear();
                        model.Warnings.Add(new WarningModel()
                        {
                            WarningType = WarningType.Warning,
                            Message = GroupFormsStrings.WarningNoActualFlights
                        });
                    }
                    else
                    {
                        // Check landing time and set the travel limit accordiglly
                        TimeSpan _LandinghoursCheck = new TimeSpan(18, 00, 00);
                        model.TravelWithinCityOnly = ((DateTime)LandingDateAndTime).TimeOfDay >= _LandinghoursCheck;

                        // Insert preparation Time
                        if (!HasPreparationActicity)
                        {
                            // Get Actual Flight Departure on first day
                            GroupActualFlightViewModel _departureFlight = _dbDapper
                                .GetRecords<GroupActualFlightViewModel>(QueriesRepository.GetFlightByGroupAndDirection,
                                    new
                                    {
                                        GroupID,
                                        Direction = 1
                                    }).FirstOrDefault();

                            TripBasicActicityViewModel _PreparationTime = new TripBasicActicityViewModel();
                            _PreparationTime.ActivityTypeEnum = eActivityType.PreparationTime;
                            _PreparationTime.StartTime = Endilo.Helpers.Helpers.RoundUp((DateTime)LandingDateAndTime,
                                TimeSpan.FromMinutes(30));
                            _PreparationTime.EndTime = _PreparationTime.StartTime.AddHours(2);
                            dynamic _Site = _dbDapper.GetRecords<dynamic>(QueriesRepository.GetSiteIDByTypeAndName,
                                new
                                {
                                    SiteType = eSiteType.Airport,
                                    Name = _departureFlight.ArrivalLocationEnum.GetDescription()
                                }).FirstOrDefault();
                            _PreparationTime.SiteID = _Site.ID;
                            _PreparationTime.CityID = _Site.CityID;
                            _PreparationTime.GroupID = model.GroupDetails.GroupID;
                            _PreparationTime.CreationDate = DateTime.Now;
                            _PreparationTime.UpdateDate = _PreparationTime.CreationDate;

                            int results = _dbDapper.InsertOperation(QueriesRepository.InsertBasicTripActivity, new
                            {
                                _PreparationTime.ActivityType,
                                _PreparationTime.SiteID,
                                _PreparationTime.CityID,
                                _PreparationTime.Name,
                                _PreparationTime.StartTime,
                                _PreparationTime.EndTime,
                                _PreparationTime.GroupID,
                                _PreparationTime.CreationDate,
                                _PreparationTime.UpdateDate,
                                _PreparationTime.Comments
                            });

                            // Get All acticities again
                            model.Activities = GetActicities(model.CurrentDate, model.GroupDetails.GroupID, null)
                                .OrderBy(m => m.EndTime);
                        }
                    }
                }
                // CHECK IF IT IS THE END OF THE TRIP AND PREPARATION ACTIVITY HAS BEEN SET
                else if (model.DayNumber == model.TripDates.Last().Value)
                {
                    IEnumerable<TripActicity> resultsFirstActivity =
                        model.Activities.Where(a => a.ActivityTypeEnum == eActivityType.PreparationTime);
                    IEnumerable<TripActicity> _previousDayActivities =
                        GetActicities(model.CurrentDate.AddDays(-1), model.GroupDetails.GroupID, null)
                            .OrderBy(m => m.EndTime);
                    bool HasPreparationActicity = resultsFirstActivity.Count() > 0 &&
                                                  resultsFirstActivity.Count(a =>
                                                      a.ActivityTypeEnum == eActivityType.PreparationTime) > 0 &&
                                                  resultsFirstActivity.FirstOrDefault().ActivityTypeEnum ==
                                                  eActivityType.PreparationTime;
                    if (DepartureDateAndTime == null)
                    {
                        //DepartureDateAndTime = _dbDapper.GetRecords<DateTime>(QueriesRepository.GetLastArrivalTimeOnDayOfDepartureByTripID, new { TripID = model.GroupDetails.Trip.ID }).FirstOrDefault();
                        // REMOVE THE OPTION TO ADD ANY ACTIVITY
                        model.ActivitiesMenu.Clear();
                        model.Warnings.Add(new WarningModel()
                        {
                            WarningType = WarningType.Warning,
                            Message = GroupFormsStrings.WarningNoActualFlights
                        });
                    }
                    else if (_previousDayActivities.Count(a => a.ActivityTypeEnum == eActivityType.LightsOut) == 0)
                    {
                        model.ActivitiesMenu.Clear();
                        model.ActivitiesMenu.Add(eActivityType.WakeUp);
                        model.Warnings.Add(new WarningModel()
                        {
                            Message = GroupFormsStrings.NoLightsOutWarningMessage,
                            WarningType = WarningType.Warning
                        });
                    }
                    else
                    {
                        //Check if Departure time is on midnight, if so we will add 23:59 minutes
                        DateTime DepartureDateTimeAfterCheck = (DateTime)DepartureDateAndTime;
                        TimeSpan _midnight = new TimeSpan(00, 00, 00);
                        if (DepartureDateTimeAfterCheck.TimeOfDay == _midnight)
                        {
                            DepartureDateAndTime = DepartureDateTimeAfterCheck.AddDays(1);
                        }

                        // Insert preparation Time
                        if (!HasPreparationActicity)
                        {
                            // Get Actual Flight Departure on first day
                            GroupActualFlightViewModel _returningFlight = _dbDapper
                                .GetRecords<GroupActualFlightViewModel>(QueriesRepository.GetFlightByGroupAndDirection,
                                    new
                                    {
                                        GroupID,
                                        Direction = 2
                                    }).LastOrDefault();

                            TripBasicActicityViewModel _PreparationTime = new TripBasicActicityViewModel();
                            _PreparationTime.ActivityTypeEnum = eActivityType.PreparationTime;

                            // Rounding down the departure Date and time
                            DepartureDateAndTime = Endilo.Helpers.Helpers.RoundDown(DepartureDateTimeAfterCheck,
                                TimeSpan.FromMinutes(30));
                            // IF THE RETURNING FROM FIELD IS WARSAW THEN THE PREPARATION ACTIVITY SHOULD BE 3 HOURS ELSE 2.5 HOURS
                            if (model.GroupDetails.Trip.ReturnedFromEnum ==
                                Models.Flights.FlightViewModel.FlightsFields.Warsaw)
                            {
                                _PreparationTime.StartTime = DepartureDateTimeAfterCheck.AddHours(-3);
                            }
                            else
                            {
                                _PreparationTime.StartTime = DepartureDateTimeAfterCheck.AddHours(-2).AddMinutes(-30);
                            }

                            _PreparationTime.EndTime = DepartureDateTimeAfterCheck;
                            dynamic _Site = _dbDapper.GetRecords<dynamic>(QueriesRepository.GetSiteIDByTypeAndName,
                                new
                                {
                                    SiteType = eSiteType.Airport,
                                    Name = _returningFlight.DestanationLocationEnum.GetDescription()
                                }).FirstOrDefault();
                            _PreparationTime.SiteID = _Site.ID;
                            _PreparationTime.CityID = _Site.CityID;
                            _PreparationTime.GroupID = model.GroupDetails.GroupID;
                            _PreparationTime.CreationDate = DateTime.Now;
                            _PreparationTime.UpdateDate = _PreparationTime.CreationDate;

                            int results = _dbDapper.InsertOperation(QueriesRepository.InsertBasicTripActivity, new
                            {
                                _PreparationTime.ActivityType,
                                _PreparationTime.SiteID,
                                _PreparationTime.CityID,
                                _PreparationTime.Name,
                                _PreparationTime.StartTime,
                                _PreparationTime.EndTime,
                                _PreparationTime.GroupID,
                                _PreparationTime.CreationDate,
                                _PreparationTime.UpdateDate,
                                _PreparationTime.Comments
                            });

                            // Get All acticities again
                            model.Activities = GetActicities(model.CurrentDate, model.GroupDetails.GroupID, null)
                                .OrderBy(m => m.EndTime);
                        }
                    }
                }
                else // Any other day in between the trip start and end date
                {
                    // Check if the day has no activities
                    if (model.Activities == null || model.Activities.Count() == 0)
                    {
                        // Set the menu to have only a wakeup activity
                        model.ActivitiesMenu.Clear();

                        // GET ALL PERVIUS DAY ACTIVITIES
                        IEnumerable<TripActicity> _previousDayActivities =
                            GetActicities(model.CurrentDate.AddDays(-1), model.GroupDetails.GroupID, null)
                                .OrderBy(m => m.EndTime);
                        // GET THE LAST ACTIVITY FROM THE PREVIOUS DAY, IF SO ALLOW ADDING WAKEUP ACTIVITY
                        // CHECK IF PREVIUS DAY HAS HOTEL ACTIVITY
                        if (_previousDayActivities.Where(a => a.ActivityTypeEnum == eActivityType.Hotel).Count() == 0)
                        {
                            model.Warnings.Add(new WarningModel()
                            {
                                Message = GroupFormsStrings.WarningNoHotelActivityMessage,
                                WarningType = WarningType.Warning
                            });
                            model.DisableMenu = true;
                        }

                        if (_previousDayActivities.Count(a => a.ActivityTypeEnum == eActivityType.LightsOut) == 0)
                        {
                            model.Warnings.Add(new WarningModel()
                            {
                                Message = GroupFormsStrings.NoLightsOutWarningMessage,
                                WarningType = WarningType.Warning
                            });
                        }

                        TripActicity LastActivity =
                            GetActicities(model.CurrentDate.AddDays(-1), model.GroupDetails.GroupID, null)
                                .LastOrDefault();
                        if (LastActivity != null)
                        {
                            model.ActivitiesMenu.Add(eActivityType.WakeUp);
                            model.LastActivityID = LastActivity.ID;
                        }
                    }
                }
            }
            else
            {
                // REMOVE THE OPTION TO ADD ANY ACTIVITY
                model.ActivitiesMenu.Clear();
                model.Warnings.Add(new WarningModel()
                {
                    WarningType = WarningType.Warning,
                    Message = GroupFormsStrings.WarningNoActualFlights
                });
            }

            if (HasActualFlights && model.DayNumber == model.TripDates.Last().Value && model.Activities
                    .Where(a => a.ActivityTypeEnum != eActivityType.PreparationTime).LastOrDefault() != null)
            {
                model.LastActivityID = model.Activities.Where(a => a.ActivityTypeEnum != eActivityType.PreparationTime)
                    .LastOrDefault().ID;
            }
            else if (model.Activities.LastOrDefault() != null)
            {
                model.LastActivityID = model.Activities.LastOrDefault().ID;
            }

            //Load form messages
            model.FormComments.Comments = _dbDapper.GetRecords<FormCommentViewModel>(QueriesRepository.GetFormComments,
                new { FormID = model.Form60Data.ID, FormChapterType = (int)eFormChapterType.TripPlan });
            model.FormComments.FormID = (int)model.Form60Data.ID;
            model.FormComments.FormChapterType = eFormChapterType.TripPlan;

            //SET FORM ON VIEW MODE - IF THE FORM IS APPROVED AND THE USER IS NOT ADMIN THAN THE FORM IS IN VIEW MODE
            if (model.Form60Data.PlanApprovalStatusEnum == eGroupFormStatus.Approved)
            {
                model.Form60Data.EditMode = false;
            }
            else if (model.Form60Data.PlanApprovalStatusEnum == eGroupFormStatus.WaitingApproval &&
                     !PermissionLoader.CheckUserPermissions(PermissionBitEnum.Form60_PlanApprovalStatus))
            {
                model.Form60Data.EditMode = false;
            }


            return View("Form80TripPlanPerDay", model);
        }

        /// <summary>
        /// Get Sites by City ID and SiteType using ajex
        /// </summary>
        /// <param name="CityID"></param>
        /// <param name="SiteType"></param>
        /// <returns></returns>
        [HttpGet]
        public JsonResult GetSitesByCityAndSiteType(int CityID, int SiteType)
        {
            dynamic json =
                _dbDapper.GetRecords<dynamic>(QueriesRepository.GetSitesByCityandSiteType, new { CityID, SiteType });

            return Json(JsonConvert.SerializeObject(json), JsonRequestBehavior.AllowGet);
        }

        private IEnumerable<TripActicity> GetActicities(DateTime Date, int GroupID, int? ActivityType)
        {
            IEnumerable<dynamic> results = _dbDapper.GetRecords<dynamic>(QueriesRepository.GetActivityByDate,
                new { StartDate = Date.Date, EndDate = Date.AddDays(1).Date, ActivityType, GroupID });
            List<TripActicity> _activities = new List<TripActicity>();

            foreach (var activity in results)
            {
                TripActicity _act = new TripActicity();
                _act.ID = activity.ID;
                _act.ActivityType = activity.ActivityType;
                _act.Name = activity.Name;
                _act.StartTime = activity.StartTime;
                _act.EndTime = activity.EndTime;
                _act.Comments = activity.Comments;
                _act.UpdateDate = activity.UpdateDate;
                _act.CreationDate = activity.CreationDate;
                _act.CityID = activity.CityID;

                // CHECK IF EXTENDED ACTIVITY
                if (!String.IsNullOrEmpty(activity.SchoolName))
                {
                    _act.SchoolName = activity.SchoolName;
                    _act.SchoolAddress = activity.SchoolAddress;
                    _act.ContactName = activity.ContactName;
                    _act.ContactEmail = activity.ContactEmail;
                    _act.ContactCell = activity.ContactCell;
                    _act.ContactPhone = activity.ContactPhone;
                }

                // Initiate City
                _act.City = new City()
                {
                    CityID = activity.CityID,
                    CityName = activity.CityName,
                    CityExternalID = activity.CityExternalID,
                    RegionID = activity.RegionID,
                    RegionName = activity.RegionName,
                    DistrictID = activity.DistrictID,
                    DistrictName = activity.DistrictName
                };

                // Site
                if (activity.SiteID != null || activity.SiteID < 1)
                {
                    _act.SiteID = activity.SiteID;
                    _act.Site = new Site()
                    {
                        ID = activity.SiteID,
                        Name = activity.SiteName,
                        ExternalID = activity.ExternalID,
                        SiteType = activity.SiteType,
                        LocalName = activity.LocalName,
                        LocalRegion = activity.LocalRegion,
                        Limitations = activity.Limitations,
                        CreationDate = activity.CreationDate,
                        UpdateDate = activity.UpdateDate,
                        City = new City()
                        {
                            CityID = activity.CityID,
                            CityName = activity.CityName,
                            CityExternalID = activity.CityExternalID,
                            RegionID = activity.RegionID,
                            RegionName = activity.RegionName,
                            DistrictID = activity.DistrictID,
                            DistrictName = activity.DistrictName
                        }
                    };
                }

                _activities.Add(_act);
            }

            return _activities;
        }

        /// <summary>
        /// Get the activity by ID, usuaaly used to get the last acticity
        /// </summary>
        /// <param name="ID"></param>
        /// <returns>TripActicity object</returns>
        private TripActicity GetActicityByID(int ID)
        {
            dynamic results = _dbDapper.GetRecords<dynamic>(QueriesRepository.GetActivityByID, new { ID })
                .FirstOrDefault();
            TripActicity _act = new TripActicity();
            _act.ID = results.ID;
            _act.ActivityType = results.ActivityType;
            _act.Name = results.Name;
            _act.StartTime = results.StartTime;
            _act.EndTime = results.EndTime;
            _act.Comments = results.Comments;
            _act.UpdateDate = results.UpdateDate;
            _act.CreationDate = results.CreationDate;
            _act.CityID = results.CityID;
            _act.GroupID = results.GroupID;

            // Initiate City
            _act.City = new City()
            {
                CityID = results.CityID,
                CityName = results.CityName,
                CityExternalID = results.CityExternalID,
                RegionID = results.RegionID,
                RegionName = results.RegionName,
                DistrictID = results.DistrictID,
                DistrictName = results.DistrictName
            };

            // Site
            if (results.SiteID != null || results.SiteID < 1)
            {
                _act.SiteID = results.SiteID;
                _act.Site = new Site()
                {
                    ID = results.SiteID,
                    Name = results.SiteName,
                    ExternalID = results.ExternalID,
                    SiteType = results.SiteType,
                    LocalName = results.LocalName,
                    LocalRegion = results.LocalRegion,
                    Limitations = results.Limitations,
                    CreationDate = results.CreationDate,
                    UpdateDate = results.UpdateDate,
                    City = new City()
                    {
                        CityID = results.CityID,
                        CityName = results.CityName,
                        CityExternalID = results.CityExternalID,
                        RegionID = results.RegionID,
                        RegionName = results.RegionName,
                        DistrictID = results.DistrictID,
                        DistrictName = results.DistrictName
                    }
                };
            }

            return _act;
        }

        [HttpGet]
        [PermissionAuthorization(PermissionBitEnum.Form60_TripPlan_Edit)]
        public PartialViewResult ManageDriveActivity(int GroupID, int LastActivityID, bool TravelWithinCityOnly,
            eActivityType ActivityType, bool IsFirstDay = false)
        {
            TripTravelActivityViewModel model = new TripTravelActivityViewModel();
            TripActicity _activity = GetActicityByID(LastActivityID);

            // Check if the activity is the last activity in the trip (preparation)
            model.StartTimeValue = _activity.EndTime.ToString("HH:mm");


            model.GroupID = GroupID;
            model.StartTime = _activity.StartTime;
            model.ActivityTypeEnum = ActivityType;

            // Limit the start times
            //if (IsFirstDay)
            //{
            //    model.SetStartTimeDropDown(_activity.StartTime.Hour, _activity.StartTime.Minute, 22, 00);
            //}
            //else
            //{
            //    model.SetStartTimeDropDown(5, 00, 22, 00);
            //}
            model.SetStartTimeDropDown(5, 00, 23, 30);

            // GET CITY OBJECT
            _activity.City = _dbDapper.GetRecords<City>(QueriesRepository.GetCityByID, new { _activity.CityID })
                .FirstOrDefault();

            int? RegionID = _activity.City.RegionID;
            int? CityID = _activity.City.CityID;

            if (TravelWithinCityOnly || model.ActivityTypeEnum == eActivityType.Walking)
            {
                RegionID = null;
            }
            else
            {
                CityID = null;
            }

            model.Cities.Items = _dbDapper
                .GetRecords<dynamic>(QueriesRepository.GetCitiesByRegionOrCity, new { RegionID, CityID }).Select(c =>
                    new CustomSelectListItem()
                    {
                        ID = c.CityID,
                        Name = c.CityName,
                        Group = String.Format("{0} - {1}", c.DistrictName, c.RegionName)
                    }).OrderBy(c => c.Name).ToList();


            return PartialView("_ManageDriveActivity", model);
        }

        [HttpPost]
        [PermissionAuthorization(PermissionBitEnum.Form60_TripPlan_Edit)]
        public ActionResult ManageDriveActivity(TripTravelActivityViewModel model)
        {
            if (model.IsUpdate)
            {
                ModelState.Remove("SiteID");
                ModelState.Remove("durationValue");
                ModelState.Remove("nextActivityDurationValue");
            }

            if (ModelState.IsValid)
            {
                TripActicity _activity = new TripActicity();
                _activity.ActivityTypeEnum = model.ActivityTypeEnum;
                _activity.Name = _activity.ActivityTypeEnum.GetDescription();
                string starttime = String.Format("{0} {1}", ((DateTime)model.StartTime).ToString("dd/MM/yyyy"),
                    model.StartTimeValue);
                _activity.StartTime = DateTime.ParseExact(starttime, AppConfig.DateTimeWithTime24FormatParsing,
                    System.Globalization.CultureInfo.CurrentCulture, System.Globalization.DateTimeStyles.None);
                _activity.EndTime =
                    _activity.StartTime.AddMinutes(Endilo.Helpers.Helpers.GetDuration(model.durationValue));
                _activity.GroupID = model.GroupID;
                _activity.SiteID = model.SiteID;
                // GET CITY ID FROM A SITE ID
                _activity.CityID = _dbDapper
                    .GetRecords<int>(QueriesRepository.GetCityIDBySiteID, new { ID = _activity.SiteID })
                    .FirstOrDefault();


                if (model.IsUpdate)
                {
                    _activity.UpdateDate = DateTime.Now;
                    starttime = String.Format("{0} {1}", ((DateTime)model.StartTime).ToString("dd/MM/yyyy"),
                        model.StartTimeValue);
                    _activity.StartTime = DateTime.ParseExact(starttime, AppConfig.DateTimeWithTime24FormatParsing,
                        System.Globalization.CultureInfo.CurrentCulture, System.Globalization.DateTimeStyles.None);
                    _activity.EndTime =
                        _activity.StartTime.AddMinutes(Endilo.Helpers.Helpers.GetDuration(model.durationValue));
                    _activity.ID = model.ID;

                    bool updateResults = _dbDapper.UpdateOperation(QueriesRepository.UpdateBasicTripActivity, new
                    {
                        _activity.ID,
                        _activity.StartTime,
                        _activity.EndTime,
                        _activity.Comments
                    });
                }
                else
                {
                    _activity.CreationDate = DateTime.Now;
                    _activity.UpdateDate = _activity.CreationDate;

                    // Create the driving activity
                    int results = _dbDapper.InsertOperation(QueriesRepository.InsertBasicTripActivity, new
                    {
                        _activity.ActivityType,
                        _activity.SiteID,
                        _activity.CityID,
                        _activity.Name,
                        _activity.StartTime,
                        _activity.EndTime,
                        _activity.GroupID,
                        _activity.CreationDate,
                        _activity.UpdateDate,
                        _activity.Comments
                    });

                    // Create the next activity
                    TripActicity _nextActivity = new TripActicity();
                    switch (model.SiteTypes)
                    {
                        case eSiteType.Hotel:
                            _nextActivity.ActivityTypeEnum = eActivityType.Hotel;
                            break;
                        case eSiteType.Site:
                        case eSiteType.Cemetery:
                        case eSiteType.Camp:
                        case eSiteType.Hall:
                        case eSiteType.Mall:
                            _nextActivity.ActivityTypeEnum = eActivityType.Site;
                            break;
                        case eSiteType.Resturant:
                            _nextActivity.ActivityTypeEnum = eActivityType.Resturant;
                            break;
                        case eSiteType.Synagogue:
                            _nextActivity.ActivityTypeEnum = eActivityType.Synagogue;
                            break;
                        default:
                            break;
                    }

                    _nextActivity.StartTime = _activity.EndTime;
                    _nextActivity.EndTime =
                        _nextActivity.StartTime.AddMinutes(
                            Endilo.Helpers.Helpers.GetDuration(model.nextActivityDurationValue));
                    _nextActivity.GroupID = _activity.GroupID;
                    _nextActivity.SiteID = _activity.SiteID;
                    _nextActivity.CityID = _activity.CityID;
                    _nextActivity.CreationDate = DateTime.Now;
                    _nextActivity.UpdateDate = _nextActivity.CreationDate;

                    int resultsnextactivity = _dbDapper.InsertOperation(QueriesRepository.InsertBasicTripActivity, new
                    {
                        _nextActivity.ActivityType,
                        _nextActivity.SiteID,
                        _nextActivity.Name,
                        _nextActivity.StartTime,
                        _nextActivity.CityID,
                        _nextActivity.EndTime,
                        _nextActivity.GroupID,
                        _nextActivity.CreationDate,
                        _nextActivity.UpdateDate,
                        _nextActivity.Comments
                    });
                }
            }

            return RedirectToAction("GetForm80TripPlan",
                new { GroupID = model.GroupID, TripDayDate = model.StartTime });
        }

        [HttpGet]
        [PermissionAuthorization(PermissionBitEnum.Form60_TripPlan_Edit)]
        public PartialViewResult EditDriveActivity(int ActivityID)
        {
            TripTravelActivityViewModel model = new TripTravelActivityViewModel();
            TripActicity _activity = GetActicityByID(ActivityID);

            model.ID = _activity.ID;
            model.ActivityType = _activity.ActivityType;
            model.CityID = _activity.CityID;
            model.StartTime = _activity.StartTime;
            model.EndTime = _activity.EndTime;
            model.GroupID = _activity.GroupID;

            model.StartTimeValue = _activity.StartTime.ToString(AppConfig.FormatTime);
            TimeSpan _duration = _activity.EndTime - _activity.StartTime;
            model.durationValue = _duration.ToString("hh\\:mm");

            // GET CITY OBJECT
            _activity.City = _dbDapper.GetRecords<City>(QueriesRepository.GetCityByID, new { _activity.CityID })
                .FirstOrDefault();

            int? RegionID = _activity.City.RegionID;
            int? CityID = _activity.City.CityID;

            model.Cities.Items = _dbDapper
                .GetRecords<dynamic>(QueriesRepository.GetCitiesByRegionOrCity, new { RegionID, CityID }).Select(c =>
                    new CustomSelectListItem()
                    {
                        ID = c.CityID,
                        Name = c.CityName,
                        Group = String.Format("{0} - {1}", c.DistrictName, c.RegionName)
                    }).OrderBy(c => c.Name).ToList();

            model.IsUpdate = true;

            return PartialView("_ManageDriveActivity", model);
        }

        [HttpGet]
        [PermissionAuthorization(PermissionBitEnum.Form60_TripPlan_Edit)]
        public PartialViewResult ManageBasicActivity(int GroupID, int LastActivityID, eSiteType SiteType,
            eActivityType ActivityType, bool IsFirstDay = false)
        {
            TripCommonActivityViewModel model = new TripCommonActivityViewModel();
            TripActicity _activity = GetActicityByID(LastActivityID);

            model.StartTimeValue = _activity.EndTime.ToString("HH:mm");
            model.GroupID = GroupID;
            model.StartTime = _activity.StartTime;
            model.ActivityTypeEnum = ActivityType;
            // Limit the start times
            //if (IsFirstDay)
            //{
            //    model.SetStartTimeDropDown(_activity.StartTime.Hour, _activity.StartTime.Minute, 22, 00);
            //}
            //else if (ActivityType != eActivityType.Hotel)
            //{
            //    model.SetStartTimeDropDown(7, 00, 22, 00);

            //}
            //else
            //{
            //    model.SetStartTimeDropDown(5, 00, 22, 00);
            //}

            model.SetStartTimeDropDown(5, 00, 23, 30);

            // GET CITY OBJECT
            _activity.City = _dbDapper.GetRecords<City>(QueriesRepository.GetCityByID, new { _activity.CityID })
                .FirstOrDefault();
            int? RegionID = _activity.City.RegionID;


            model.Sites.Items = _dbDapper
                .GetRecords<dynamic>(QueriesRepository.GetSitesByTypeAndRegion, new { RegionID, SiteType }).Select(s =>
                    new CustomSelectListItem()
                    {
                        ID = s.ID,
                        Name = String.Format("{0} - {1}", ((eSiteType)s.SiteType).GetDescription(), s.Name),
                        Group = s.CityName
                    }).OrderBy(c => c.Name).ToList();


            return PartialView("_ManageBasicActivity", model);
        }

        [HttpPost]
        [PermissionAuthorization(PermissionBitEnum.Form60_TripPlan_Edit)]
        public ActionResult ManageBasicActivity(TripCommonActivityViewModel model)
        {
            if (ModelState.IsValid)
            {
                TripActicity _activity = new TripActicity();
                _activity.ActivityTypeEnum = model.ActivityTypeEnum;
                string starttime = String.Format("{0} {1}", ((DateTime)model.StartTime).ToString("dd/MM/yyyy"),
                    model.StartTimeValue);
                _activity.StartTime = DateTime.ParseExact(starttime, AppConfig.DateTimeWithTime24FormatParsing,
                    System.Globalization.CultureInfo.CurrentCulture, System.Globalization.DateTimeStyles.None);
                _activity.EndTime =
                    _activity.StartTime.AddMinutes(Endilo.Helpers.Helpers.GetDuration(model.durationValue));
                _activity.GroupID = model.GroupID;
                _activity.SiteID = model.SiteID;
                _activity.CityID = _dbDapper
                    .GetRecords<int>(QueriesRepository.GetCityIDBySiteID, new { ID = _activity.SiteID })
                    .FirstOrDefault();
                _activity.CreationDate = DateTime.Now;
                _activity.UpdateDate = _activity.CreationDate;
                _activity.Comments = model.Comments;

                int results = _dbDapper.InsertOperation(QueriesRepository.InsertBasicTripActivity, new
                {
                    _activity.ActivityType,
                    _activity.SiteID,
                    _activity.Name,
                    _activity.StartTime,
                    _activity.CityID,
                    _activity.EndTime,
                    _activity.GroupID,
                    _activity.CreationDate,
                    _activity.UpdateDate,
                    _activity.Comments
                });
            }

            return RedirectToAction("GetForm80TripPlan",
                new { GroupID = model.GroupID, TripDayDate = model.StartTime });
        }

        [HttpGet]
        [PermissionAuthorization(PermissionBitEnum.Form60_TripPlan_Edit)]
        public PartialViewResult ManageNoSiteActivity(int GroupID, int LastActivityID, eActivityType ActivityType,
            string CurrentDate, bool IsFirstDay = false)
        {
            TripBasicActicityViewModel model = new TripBasicActicityViewModel();
            model.CurrentDate = DateTime.ParseExact(CurrentDate, AppConfig.DateTimeFormatParsing,
                System.Globalization.CultureInfo.CurrentCulture, System.Globalization.DateTimeStyles.None);

            if (ActivityType == eActivityType.WakeUp)
            {
                LastActivityID = _dbDapper.GetRecords<int>(QueriesRepository.GetPreviusActivityIDByActivityType,
                    new
                    {
                        StartTime = model.CurrentDate, GroupID, ActivityType = (int)eActivityType.Drive,
                        SiteType = (int)eSiteType.Hotel
                    }).FirstOrDefault();
            }

            TripActicity _activity = GetActicityByID(LastActivityID);

            model.StartTimeValue = _activity.EndTime.ToString("HH:mm");
            model.GroupID = GroupID;
            model.StartTime = _activity.StartTime;
            model.ActivityTypeEnum = ActivityType;
            model.SiteID = _activity.SiteID;
            model.CityID = _activity.CityID;

            // Limit the start times
            if (IsFirstDay)
            {
                model.SetStartTimeDropDown(5, 00, 23, 30);
            }
            else if (ActivityType == eActivityType.WakeUp)
            {
                model.SetStartTimeDropDown(05, 30, 08, 00);

                // Reset the start hour to 05:30
                model.StartTime = new DateTime(model.StartTime.Year, model.StartTime.Month, model.StartTime.Day, 05, 30,
                    00);
            }
            else
            {
                model.SetStartTimeDropDown(5, 00, 23, 30);
            }

            if (ActivityType == eActivityType.LightsOut)
            {
                model.Duration.Items.Clear();
                model.Duration.Items.AddRange(FunctionHelpers.GetTimes(minuteInterval: 30, StartHour: 7.5,
                    specialFormat: true));

                model.SetStartTimeDropDown(07, 30, 23, 30);
            }

            return PartialView("_ManageNoSiteActivity", model);
        }

        [HttpGet]
        [PermissionAuthorization(PermissionBitEnum.Form60_TripPlan_Edit)]
        public PartialViewResult EditNoSiteActivity(int ActivityID, string CurrentDate, bool IsFirstDay)
        {
            TripActicity _activity = GetActicityByID(ActivityID);
            TripBasicActicityViewModel model = new TripBasicActicityViewModel();
            model.CurrentDate = DateTime.ParseExact(CurrentDate, AppConfig.DateTimeFormatParsing,
                System.Globalization.CultureInfo.CurrentCulture, System.Globalization.DateTimeStyles.None);

            model.ID = _activity.ID;
            model.ActivityType = _activity.ActivityType;
            model.CityID = _activity.CityID;
            model.StartTime = _activity.StartTime;
            model.EndTime = _activity.EndTime;
            model.GroupID = _activity.GroupID;

            model.StartTimeValue = _activity.StartTime.ToString(AppConfig.FormatTime);
            TimeSpan _duration = _activity.EndTime - _activity.StartTime;
            model.durationValue = _duration.ToString("hh\\:mm");

            if (IsFirstDay)
            {
                model.SetStartTimeDropDown(5, 00, 23, 30);
            }
            else if (_activity.ActivityTypeEnum == eActivityType.WakeUp)
            {
                model.SetStartTimeDropDown(05, 30, 08, 00);

                // Reset the start hour to 05:30
                model.StartTime = new DateTime(model.StartTime.Year, model.StartTime.Month, model.StartTime.Day, 05, 30,
                    00);
            }
            else
            {
                model.SetStartTimeDropDown(5, 00, 23, 30);
            }

            if (_activity.ActivityTypeEnum == eActivityType.LightsOut)
            {
                model.Duration.Items.Clear();
                model.Duration.Items.AddRange(FunctionHelpers.GetTimes(30, 7.5));
            }


            // Deal with duration
            model.Comments = _activity.Comments;

            model.IsUpdate = true;

            return PartialView("_ManageNoSiteActivity", model);
        }

        [HttpGet]
        [PermissionAuthorization(PermissionBitEnum.Form60_TripPlan_Edit)]
        public PartialViewResult EditSiteActivity(int ActivityID, string CurrentDate, bool IsFirstDay)
        {
            TripActicity _activity = GetActicityByID(ActivityID);
            TripBasicActicityViewModel model = new TripBasicActicityViewModel();
            model.CurrentDate = DateTime.ParseExact(CurrentDate, AppConfig.DateTimeFormatParsing,
                System.Globalization.CultureInfo.CurrentCulture, System.Globalization.DateTimeStyles.None);

            model.ID = _activity.ID;
            model.ActivityType = _activity.ActivityType;
            model.CityID = _activity.CityID;
            model.StartTime = _activity.StartTime;
            model.EndTime = _activity.EndTime;
            model.GroupID = _activity.GroupID;

            model.StartTimeValue = _activity.StartTime.ToString(AppConfig.FormatTime);
            TimeSpan _duration = _activity.EndTime - _activity.StartTime;
            model.durationValue = _duration.ToString("hh\\:mm");

            model.SetStartTimeDropDown(5, 00, 23, 30);


            // Deal with duration
            model.Comments = _activity.Comments;

            model.IsUpdate = true;

            return PartialView("_ManageSiteActivity", model);
        }

        [HttpPost]
        [PermissionAuthorization(PermissionBitEnum.Form60_TripPlan_Edit)]
        public ActionResult ManageNoSiteActivity(TripBasicActicityViewModel model)
        {
            ModelState.Remove("SiteID");
            if (ModelState.IsValid)
            {
                TripActicity _activity = new TripActicity();

                if (model.IsUpdate)
                {
                    string starttime = String.Format("{0} {1}", ((DateTime)model.CurrentDate).ToString("dd/MM/yyyy"),
                        model.StartTimeValue);
                    _activity.StartTime = DateTime.ParseExact(starttime, AppConfig.DateTimeWithTime24FormatParsing,
                        System.Globalization.CultureInfo.CurrentCulture, System.Globalization.DateTimeStyles.None);
                    _activity.EndTime =
                        _activity.StartTime.AddMinutes(Endilo.Helpers.Helpers.GetDuration(model.durationValue));
                    _activity.ID = model.ID;
                    _activity.Comments = model.Comments;

                    bool updateResults = _dbDapper.UpdateOperation(QueriesRepository.UpdateBasicTripActivity, new
                    {
                        _activity.ID,
                        _activity.StartTime,
                        _activity.EndTime,
                        _activity.Comments
                    });
                }
                else
                {
                    _activity.ActivityTypeEnum = model.ActivityTypeEnum;
                    string starttime = String.Format("{0} {1}", ((DateTime)model.CurrentDate).ToString("dd/MM/yyyy"),
                        model.StartTimeValue);
                    _activity.StartTime = DateTime.ParseExact(starttime, AppConfig.DateTimeWithTime24FormatParsing,
                        System.Globalization.CultureInfo.CurrentCulture, System.Globalization.DateTimeStyles.None);
                    _activity.EndTime =
                        _activity.StartTime.AddMinutes(Endilo.Helpers.Helpers.GetDuration(model.durationValue));

                    _activity.GroupID = model.GroupID;

                    _activity.CityID = model.CityID;
                    if (model.ActivityTypeEnum == eActivityType.WakeUp)
                    {
                        _activity.SiteID = model.SiteID;
                    }
                    else
                    {
                        _activity.SiteID = null;
                    }

                    _activity.CreationDate = DateTime.Now;
                    _activity.UpdateDate = _activity.CreationDate;
                    _activity.Comments = model.Comments;

                    int results = _dbDapper.InsertOperation(QueriesRepository.InsertBasicTripActivity, new
                    {
                        _activity.ActivityType,
                        _activity.CityID,
                        _activity.SiteID,
                        _activity.Name,
                        _activity.StartTime,
                        _activity.EndTime,
                        _activity.GroupID,
                        _activity.CreationDate,
                        _activity.UpdateDate,
                        _activity.Comments
                    });
                }
            }

            return RedirectToAction("GetForm80TripPlan", new { model.GroupID, TripDayDate = model.CurrentDate });
        }

        [HttpGet]
        [PermissionAuthorization(PermissionBitEnum.Form60_TripPlan_Edit)]
        public PartialViewResult ManageExtendedActivity(int GroupID, int LastActivityID, string CurrentDate,
            bool IsFirstDay = false)
        {
            TripExtendedActicityViewModel model = new TripExtendedActicityViewModel();
            TripActicity _activity = GetActicityByID(LastActivityID);
            model.CurrentDate = DateTime.ParseExact(CurrentDate, AppConfig.DateTimeFormatParsing,
                System.Globalization.CultureInfo.CurrentCulture, System.Globalization.DateTimeStyles.None);

            model.StartTimeValue = _activity.EndTime.ToString("HH:mm");
            model.GroupID = GroupID;
            model.StartTime = _activity.StartTime;
            model.ActivityTypeEnum = eActivityType.Activity;
            model.CityID = _activity.CityID;
            model.SetStartTimeDropDown(5, 00, 23, 30);

            return PartialView("_ManageExtendedActivity", model);
        }

        [HttpPost]
        [PermissionAuthorization(PermissionBitEnum.Form60_TripPlan_Edit)]
        public ActionResult ManageExtendedActivity(TripExtendedActicityViewModel model)
        {
            ModelState.Remove("SiteID");
            if (ModelState.IsValid)
            {
                TripActicity _activity = new TripActicity();
                _activity.ActivityTypeEnum = model.ActivityTypeEnum;
                string starttime = String.Format("{0} {1}", ((DateTime)model.CurrentDate).ToString("dd/MM/yyyy"),
                    model.StartTimeValue);
                _activity.StartTime = DateTime.ParseExact(starttime, AppConfig.DateTimeWithTime24FormatParsing,
                    System.Globalization.CultureInfo.CurrentCulture, System.Globalization.DateTimeStyles.None);
                _activity.EndTime =
                    _activity.StartTime.AddMinutes(Endilo.Helpers.Helpers.GetDuration(model.durationValue));

                _activity.GroupID = model.GroupID;

                _activity.CityID = model.CityID;

                // SET EXTENDED DETAILS
                _activity.Name = model.Name;

                if (model.IsExtendedActivity)
                {
                    _activity.SchoolName = model.SchoolName;
                    _activity.SchoolAddress = model.SchoolAddress;
                    _activity.ContactName = model.ContactName;
                    _activity.ContactPhone = model.ContactPhone;
                    _activity.ContactCell = model.ContactCell;
                    _activity.ContactEmail = _activity.ContactEmail;
                }

                _activity.CreationDate = DateTime.Now;
                _activity.UpdateDate = _activity.CreationDate;
                _activity.Comments = model.Comments;

                int results = _dbDapper.InsertOperation(QueriesRepository.InsertExtendedActivity, new
                {
                    _activity.ActivityType,
                    _activity.SiteID,
                    _activity.CityID,
                    _activity.Name,
                    _activity.StartTime,
                    _activity.EndTime,
                    _activity.GroupID,
                    _activity.CreationDate,
                    _activity.UpdateDate,
                    _activity.Comments,
                    _activity.SchoolName,
                    _activity.SchoolAddress,
                    _activity.ContactName,
                    _activity.ContactEmail,
                    _activity.ContactPhone,
                    _activity.ContactCell
                });
            }

            return RedirectToAction("GetForm80TripPlan", new { model.GroupID, TripDayDate = model.CurrentDate });
        }

        [HttpGet]
        [PermissionAuthorization(PermissionBitEnum.Form60_TripPlan_Edit)]
        public PartialViewResult DeleteTripActivity(int ID, string name)
        {
            DeleteViewModel model = new DeleteViewModel();
            model.ID = ID;
            model.IsDeletable = true;
            model.Title = name;
            model.Action = "DeleteTripActivity";
            model.Controller = "GroupForms";
            model.Header = GroupFormsStrings.DeleteTripActivity;

            return PartialView("_DeleteItem", model);
        }

        [HttpPost]
        [PermissionAuthorization(PermissionBitEnum.Form60_TripPlan_Edit)]
        public ActionResult DeleteTripActivity(DeleteViewModel model)
        {
            // Get activity by ID
            TripActicity _activity = GetActicityByID(model.ID);

            // Delete the flight from the database
            bool results = _dbDapper.UpdateOperation(QueriesRepository.DeleteTripActivity, new { model.ID });

            return RedirectToAction("GetForm80TripPlan", new { _activity.GroupID, TripDayDate = _activity.StartTime });
        }

        [HttpGet]
        [PermissionAuthorization(PermissionBitEnum.Form60_TripPlan_Edit)]
        public PartialViewResult DeleteTripDay(int ID, string Title, DateTime day, int GroupID)
        {
            DeleteViewModel model = new DeleteViewModel();
            model.ID = ID;
            model.IsDeletable = true;
            model.Title = Title;
            model.Action = "DeleteTripDay";
            model.Controller = "GroupForms";
            model.Header = GroupFormsStrings.DeleteActualFlight;

            // Add Custom Property for model
            model.CustomProperties = new List<string>();
            model.CustomProperties.Add(day.ToString(AppConfig.FormatDate));

            model.ReturnRefirectProperties = new List<int>();
            model.ReturnRefirectProperties.Add(GroupID);

            return PartialView("_DeleteItem", model);
        }

        [HttpPost]
        [PermissionAuthorization(PermissionBitEnum.Form60_TripPlan_Edit)]
        public ActionResult DeleteTripDay(DeleteViewModel model)
        {
            // GetTripDay
            var TripDay = model.CustomProperties[0];
            DateTime StartDate = Convert.ToDateTime(TripDay);
            DateTime EndDate = StartDate.AddDays(1);

            var results = _dbDapper.UpdateOperation(QueriesRepository.DeleteGroupTripDay,
                new { GroupID = model.ID, StartDate, EndDate });

            return RedirectToAction("GetForm80TripPlan", new { GroupID = model.ID, TripDayDate = StartDate });
        }

        #endregion


        #region Form60 Contact

        [HttpGet]
        [PermissionAuthorization(PermissionBitEnum.Form60_Contacts_View)]
        public ActionResult PrintContantsPage(int GroupID)
        {
            return new ActionAsPdf("GetContacts", new { GroupID })
            {
                FileName = "ContactPage.pdf"
            };
        }

        [HttpGet]
        [PermissionAuthorization(PermissionBitEnum.Form60_Contacts_View)]
        public ActionResult GetContacts(int GroupID)
        {
            ContactsViewModel model = new ContactsViewModel();

            // Load Form 60 Data
            model.Form60Data = _dbDapper
                .GetRecords<GroupForm60ViewModel>(QueriesRepository.GetGroupForm60, new { GroupID }).FirstOrDefault();
            model.Form60Data.ChapterType = eFormChapterType.Contacts;

            // Load group general details
            model.GroupDetails = _dbDapper
                .GetRecords<GroupGeneralDetailsViewModel>(QueriesRepository.GetForm60GroupGeneralDetails,
                    new { GroupID }).FirstOrDefault();
            model.GroupDetails.Trip = _dbDapper
                .GetRecords<GroupTripDetailsViewModel>(QueriesRepository.GetForm60GroupTripDetails, new { GroupID })
                .FirstOrDefault();

            model.BasicContacts = _dbDapper.GetRecords<ParticipantViewModel>(
                QueriesRepository.GetGroupParticipantsForm60,
                new { GroupID, ParticipantType = model.ParticipantTypeFilter });

            //GET INSTRUCTORS
            model.Instructors = _dbDapper.GetRecords<InstructorsViewModel>(QueriesRepository.GetInstructorForm60,
                new { GroupID, RoleID = AppConfig.SearchForInstructors });

            // GET MANAGER STATEMENT FILE
            model.ManagerStatement = _dbDapper.GetRecords<GroupFileViewModel>(QueriesRepository.GetManagerStetementFile,
                new { GroupID, FileType = (int)eGroupFileType.ManagerStatement }).LastOrDefault();

            //GET PARTICIPANT TYPE REPORT DATA
            model.ParticipantTypeReport = new List<GroupByParticipantTypeReport>();
            model.ParticipantTypeReport.AddRange(
                _dbDapper.GetRecords<dynamic>(QueriesRepository.GetGroupParticipantReportBasicParticipant,
                    new { GroupID }).Select(p => new GroupByParticipantTypeReport()
                {
                    GroupID = p.GroupID,
                    NumberOfParticipants = p.NumberOfParticipants,
                    ParticipantType = ((eParticipantType)((int)p.ParticipantType)).GetDescription()
                })
            );

            model.ParticipantTypeReport.AddRange(
                _dbDapper.GetRecords<dynamic>(QueriesRepository.GetGroupParticipantReportInstructors, new { GroupID })
                    .Select(p => new GroupByParticipantTypeReport()
                    {
                        GroupID = p.GroupID,
                        NumberOfParticipants = p.NumberOfParticipants,
                        ParticipantType = ((eInstructorRoleType)p.RoleID).GetDescription()
                    })
            );

            //Load form messages
            model.FormComments.Comments = _dbDapper.GetRecords<FormCommentViewModel>(QueriesRepository.GetFormComments,
                new { FormID = model.Form60Data.ID, FormChapterType = (int)eFormChapterType.Contacts });
            model.FormComments.FormID = (int)model.Form60Data.ID;
            model.FormComments.FormChapterType = eFormChapterType.Contacts;

            //SET FORM ON VIEW MODE - IF THE FORM IS APPROVED AND THE USER IS NOT ADMIN THAN THE FORM IS IN VIEW MODE
            if (model.Form60Data.FormStatusEnum == eGroupForm60Status.Approved)
            {
                model.Form60Data.EditMode = false;
            }
            //else if ((model.Form60Data.FormStatusEnum != eGroupForm60Status.New || model.Form60Data.FormStatusEnum != eGroupForm60Status.Rejected) && !PermissionLoader.CheckUserPermissions(PermissionBitEnum.Form60_FormStatus))

            //{
            //    model.Form60Data.EditMode = false;

            //}

            return View("Get60Logistics", model);
        }

        [HttpGet]
        [PermissionAuthorization(PermissionBitEnum.Form60_Contacts_Edit)]
        public ActionResult BasicContact(int GroupID, eParticipantType ParticipantType)
        {
            ParticipantViewModel model = new ParticipantViewModel();
            model.GroupID = GroupID;
            model.ParticipantTypeEnum = ParticipantType;

            return PartialView("_BasicContact", model);
        }

        [HttpGet]
        [PermissionAuthorization(PermissionBitEnum.Form60_Contacts_Edit)]
        public ActionResult UpdateBasicContact(int ID)
        {
            ParticipantViewModel model = new ParticipantViewModel();
            model = _dbDapper.GetRecords<ParticipantViewModel>(QueriesRepository.GetSingleParticipantBasic, new { ID })
                .FirstOrDefault();
            model.UpdateOperation = true;

            return PartialView("_BasicContact", model);
        }

        [HttpPost]
        [PermissionAuthorization(PermissionBitEnum.Form60_Contacts_Edit)]
        public ActionResult BasicContact(ParticipantViewModel model)
        {
            if (model.ParticipantTypeEnum == eParticipantType.Polani ||
                model.ParticipantTypeEnum == eParticipantType.PolaniPhotographer)
            {
                ModelState.Remove("FirstNameHe");
                ModelState.Remove("LastNameHe");
            }

            if (ModelState.IsValid)
            {
                model.UserID = FunctionHelpers.User.UserID;
                model.ParticipantStatusEnum = eParticipantStatus.Approved;
                model.CreationDate = DateTime.Now;
                model.UpdateDate = model.CreationDate;

                // Check if user is already exists
                int? ID = _dbDapper.GetRecords<int?>(QueriesRepository.SearchForGroupParticipant, new
                {
                    model.GroupID,
                    model.ParticipantID,
                    model.PassportNumber
                }).FirstOrDefault();

                if (ID != null)
                {
                    model.UpdateOperation = true;
                    model.ID = (int)ID;
                }

                if (model.UpdateOperation)
                {
                    bool results = _dbDapper.UpdateOperation(QueriesRepository.UpdatePacrticipantBasic, new
                    {
                        model.ParticipantID,
                        model.FirstNameHe,
                        model.LastNameHe,
                        model.FirstNameEn,
                        model.LastNameEn,
                        model.PassportNumber,
                        model.PassportExperationDate,
                        model.Birthday,
                        model.SEX,
                        model.UpdateDate,
                        model.UserID,
                        model.ContactNumber,
                        model.Speciality,
                        model.ParticipantType,
                        model.ID,
                        model.MedicalType
                    });
                }
                else
                {
                    int results = _dbDapper.InsertOperation(QueriesRepository.AddParticipantBasic, new
                    {
                        model.GroupID,
                        model.ParticipantID,
                        model.FirstNameHe,
                        model.LastNameHe,
                        model.FirstNameEn,
                        model.LastNameEn,
                        model.PassportNumber,
                        model.PassportExperationDate,
                        model.Birthday,
                        SEX = model.SEX,
                        Status = (int)model.ParticipantStatusEnum,
                        model.CreationDate,
                        model.UpdateDate,
                        model.UserID,
                        model.ParticipantType,
                        model.ContactNumber,
                        model.Speciality,
                        model.MedicalType
                    });
                }
            }

            return RedirectToAction("GetContacts", new { model.GroupID });
        }

        [HttpGet]
        [PermissionAuthorization(PermissionBitEnum.Form60_Contacts_Edit)]
        public PartialViewResult DeleteContact(int ID, string name)
        {
            DeleteViewModel model = new DeleteViewModel();
            model.ID = ID;
            model.IsDeletable = true;
            model.Title = name;
            model.Action = "DeleteContact";
            model.Controller = "GroupForms";
            model.Header = GroupFormsStrings.DeleteContact;

            return PartialView("_DeleteItem", model);
        }

        [HttpPost]
        [PermissionAuthorization(PermissionBitEnum.Form60_Contacts_Edit)]
        public ActionResult DeleteContact(DeleteViewModel model)
        {
            // Get Group Contact
            GroupParticipants item = _dbDapper.GetGroupParticipant(model.ID);

            // Get activity by ID
            bool operation = _dbDapper.DeleteParticipant(item.ID);

            return RedirectToAction("GetContacts", new { item.GroupID });
        }

        [HttpGet]
        [PermissionAuthorization(PermissionBitEnum.Form60_Contacts_Edit)]
        public PartialViewResult AddInstructorForm60(int GroupID)
        {
            AddGuideViewModel model = new AddGuideViewModel();
            model.GroupID = GroupID;

            return PartialView("_AddGuide", model);
        }

        [HttpPost]
        [PermissionAuthorization(PermissionBitEnum.Form60_Contacts_Edit)]
        public ActionResult AddInstructorForm60(AddGuideViewModel model)
        {
            if (ModelState.IsValid)
            {
                int RowID = _dbDapper.GetRecords<int>(QueriesRepository.AddInstructorForm60, new
                {
                    model.GroupID,
                    model.InstructorID,
                    RoleID = (int)eInstructorRoleType.Instructor,
                    model.Comments,
                    model.IsGroupManager,
                    model.ContactNumber
                }).FirstOrDefault();

                // Update Instrctor email
                bool resutls = _dbDapper.UpdateOperation(QueriesRepository.UpdateInstructorEmail,
                    new { model.Email, model.InstructorID });
            }

            return RedirectToAction("GetContacts", new { model.GroupID });
        }

        [HttpGet]
        [PermissionAuthorization(PermissionBitEnum.Form60_Contacts_Edit)]
        public PartialViewResult RemoveInstructorForm60(int ID, string title, int GroupID)
        {
            DeleteViewModel model = new DeleteViewModel();
            model.ID = ID;
            model.IsDeletable = true;
            model.Title = title;
            model.Action = "UnassignInstructor";
            model.Controller = "GroupForms";
            model.ReturnRefirectProperties = new List<int>();
            model.ReturnRefirectProperties.Add(GroupID);
            model.Header = GroupsStrings.UnAssignInstructor;

            return PartialView("_DeleteItem", model);
        }

        [HttpPost]
        [PermissionAuthorization(PermissionBitEnum.Form60_Contacts_Edit)]
        public ActionResult UnassignInstructor(DeleteViewModel model)
        {
            bool results = _dbDapper.ExecuteCommand(QueriesRepository.RemoveInstructorFromGroupByID, new { model.ID });

            return RedirectToAction("GetContacts", new { GroupID = model.ReturnRefirectProperties[0] });
        }

        [HttpGet]
        [PermissionAuthorization(PermissionBitEnum.Form60_Contacts_Edit)]
        public JsonResult SearchForGuide(string SearchTerm)
        {
            List<int> _instructors = new List<int>();
            _instructors.Add(AppConfig.SearchForInstructors);
            List<AutoCompleteItem> items =
                _dbDapper.GetRecords<AutoCompleteItem>(QueriesRepository.SearchForManager,
                    new { SearchTerm, RoleID = _instructors });

            return Json(JsonConvert.SerializeObject(items), JsonRequestBehavior.AllowGet);
        }


        [PermissionAuthorization(PermissionBitEnum.Form60_Contacts_Edit)]
        [HttpGet]
        public PartialViewResult UpdateGroupContactDetails(int ID)
        {
            GroupContactDetailsViewMode model = new GroupContactDetailsViewMode();
            model = _dbDapper
                .GetRecords<GroupContactDetailsViewMode>(QueriesRepository.GetContactInCasOfEmergency,
                    new { GroupID = ID }).FirstOrDefault();
            return PartialView("_UpdateGroupContactDetails", model);
        }

        [PermissionAuthorization(PermissionBitEnum.Form60_Contacts_Edit)]
        [HttpPost]
        public ActionResult UpdateGroupContactDetails(GroupContactDetailsViewMode model)
        {
            if (ModelState.IsValid)
            {
                bool result = _dbDapper.UpdateGroupContactDetails(model.GroupID, model.ContactName, model.ContactPhone);
            }

            return RedirectToAction("GetContacts", new { model.GroupID });
        }

        [HttpGet]
        [PermissionAuthorization(PermissionBitEnum.Form60_Contacts_Edit)]
        public PartialViewResult AddManagerStatementFile(int ID)
        {
            GroupFileViewModel model = new GroupFileViewModel();
            model.GroupID = ID;
            model.UserID = FunctionHelpers.User.UserID;

            return PartialView("_AddManagerStatementFile", model);
        }

        [PermissionAuthorization(PermissionBitEnum.Form60_Contacts_Edit)]
        [HttpPost]
        public ActionResult AddManagerStatementFile(GroupFileViewModel model)
        {
            if (ModelState.IsValid)
            {
                GroupFiles file = new GroupFiles();

                file.Name = model.Name;
                file.GroupID = model.GroupID;
                file.GroupFileStatusEnum = eGroupFileStatus.New;

                file.FileType = (int)eGroupFileType.ManagerStatement;
                file.GroupFileTypeEnum = model.FileType;

                file.CreationDate = DateTime.Now;
                file.UserID = model.UserID;

                //Save file
                if (model.Attachment.HasFile())
                {
                    string filename = String.Format("{0}{1}{2}{3}", model.GroupID, model.FileType.ToString(),
                        file.CreationDate.ToString("ddMMyyyyhhmm"), Path.GetExtension(model.Attachment.FileName));
                    model.Attachment.SaveAs(Server.MapPath(String.Format("{0}{1}", AppConfig.GroupFilesPathRootRelative,
                        filename)));
                    file.Url = filename;

                    int fileID = _dbDapper.InsertGroupFile(file);
                }
            }

            return RedirectToAction("GetContacts", new { model.GroupID });
        }

        #endregion
    }
}