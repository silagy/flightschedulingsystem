﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Diagnostics;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using AutoMapper;
using Endilo.Emails;
using Flight.DAL.FlightDB.Methods;
using Flight.Membership;
using FlightScheduling.DAL;
using FlightScheduling.Language;
using FlightScheduling.Web.Areas.Administration.Models.Common;
using FlightScheduling.Web.Areas.Administration.Models.Schools;
using Otakim.Website.Infrastructure;
using FlightScheduling.Website.Infrastructure;
using FlightScheduling.DAL.CustomEntities.Permissions;
using StackExchange.Profiling.Helpers.Dapper;
using FlightScheduling.Web.Areas.Administration.Models.Shared;

namespace FlightScheduling.Web.Areas.Administration.Controllers
{
    //[EndiloAuthorization(eRole.Administrator, eRole.Staf, eRole.Agent, eRole.CountyManager)]
    [PermissionAuthorization(PermissionBitEnum.Administration_SchoolView)]
    public class SchoolsController : Controller
    {
        private FlightDB _db = new FlightDB();
        private DBRepository _dbDapper = new DBRepository();

        protected override void Dispose(bool disposing)
        {
            _db.Dispose();
            _dbDapper.Dispose();
            base.Dispose(disposing);
        }

        //
        // GET: /Administration/Schools/
        //[EndiloAuthorization(eRole.Administrator, eRole.Staf)]

        //[EndiloAuthorization(eRole.Administrator, eRole.Staf)]
        [HttpGet]
        public ActionResult GetSchools()
        {
            SchoolsViewModel model = new SchoolsViewModel();

            //model.NumberOfPages = (_dbDapper.GetNumberOfRows("Schools")/model.NumberOfRowstoFetch) + 1;

            //model.SchoolViewModels = (from sc in _db.Schools
            //                          join us in _db.Users on sc.InstituteID.ToString() equals us.Email
            //                          where sc.IsActive == true
            //                          select new SchoolViewModel()
            //                          {
            //                              ID = sc.ID,
            //                              Name = sc.Name,
            //                              Email = sc.Email,
            //                              Fax = sc.Fax,
            //                              Phone = sc.Phone,
            //                              InstitueID = sc.InstituteID,
            //                              Manager = sc.Manager,
            //                              ManagerEmail = sc.ManagerEmail,
            //                              ManagerPhone = sc.ManagerPhone,
            //                              CreationDate = sc.CreationDate,
            //                              UpdatenDate = sc.UpdateDate,
            //                              IsActivated = us.IsActivate
            //                          }).ToList();



            //model.SchoolViewModels = _db.Schools.RemoveInActive().Join(_db.Users, s => s.InstituteID, u => u.Email, (s, u) => new SchoolViewModel()
            //    {
            //        ID = s.ID,
            //        Name = s.Name,
            //        Email = s.Email,
            //        Fax = s.Fax,
            //        Phone = s.Phone,
            //        InstitueID = s.InstituteID,
            //        Manager = s.Manager,
            //        ManagerEmail = s.ManagerEmail,
            //        ManagerPhone = s.ManagerPhone,
            //        CreationDate = s.CreationDate,
            //        UpdatenDate = s.UpdateDate,
            //        IsActivated = u.IsActivate
            //    }).ToList();

            return View(model);
        }

        //[EndiloAuthorization(eRole.Administrator, eRole.Staf)]
        [HttpPost]
        public ActionResult GetSchools(SchoolsViewModel model)
        {
            //model.SchoolViewModels =
            //    Mapper.Map<List<Schools>, List<SchoolViewModel>>(_dbDapper.GetSchools(model.InstituteID, model.Name,
            //        model.Email));

            model.SchoolViewModels = _dbDapper.GetRecords<SchoolViewModel>(QueriesRepository.GetAllSchools, new { model.InstituteID, model.Name, model.Email });

            return View(model);
        }

        //[EndiloAuthorization(eRole.Administrator, eRole.Staf)]
        [HttpGet]
        public ActionResult AddSchool()
        {
            SchoolViewModel model = new SchoolViewModel();
            return View(model);
        }

        //[EndiloAuthorization(eRole.Administrator, eRole.Staf)]
        [PermissionAuthorization(PermissionBitEnum.Administration_SchoolEdit)]
        [HttpPost]
        public ActionResult AddSchool(SchoolViewModel model)
        {
            if (ModelState.IsValid)
            {
                using (var transaction = _db.Database.BeginTransaction())
                {

                    try
                    {
                        Schools _school = new Schools();

                        // Check if Insitute ID Exists
                        _school = _dbDapper.GetRecords<Schools>(QueriesRepository.GetSchoolByIntituteID, new { model.InstituteID }).FirstOrDefault();
                        // School doesn't exists
                        bool schoolAdded = false;
                        if (!(_school != null && _school.InstituteID > 0))
                        {
                            //Does not exists
                            _school = new Schools();
                            _school.IsActive = true;
                            _school.Name = model.Name;
                            _school.Email = model.Email;
                            _school.InstituteID = model.InstituteID;
                            _school.Phone = model.Phone;
                            _school.Fax = model.Fax;
                            _school.Manager = model.Manager;
                            _school.ManagerEmail = model.ManagerEmail;
                            _school.ManagerPhone = model.ManagerPhone;
                            _school.CreationDate = DateTime.Now;
                            _school.UpdateDate = DateTime.Now;
                            _school.City = model.City;
                            _school.Region = (int)model.RegionEnum;
                            _school.IsAdministrativeInstitude = model.IsAdministrative;

                            int results = _dbDapper.InsertOperationReturnsID(QueriesRepository.AddSchool, new
                            {
                                _school.InstituteID,
                                _school.Name,
                                _school.Email,
                                _school.Fax,
                                _school.Phone,
                                _school.Manager,
                                _school.ManagerPhone,
                                _school.ManagerEmail,
                                _school.IsActive,
                                _school.CreationDate,
                                _school.UpdateDate,
                                _school.City,
                                _school.Region,
                                _school.IsAdministrativeInstitude
                            });
                            schoolAdded = results > 0;
                            _school.ID = results;
                        }



                        //Add user from school
                        Users _userToAdd = new Users();
                        string email = _school.InstituteID.ToString();
                        // Check if User Exists
                        _userToAdd = _dbDapper.GetRecords<Users>(QueriesRepository.GetUserByInstituteID, new
                        {
                            InstituteID = email
                        }).FirstOrDefault();

                        if (!(_userToAdd != null && _userToAdd.ID > 0))
                        {
                            //User doesn't exits
                            _userToAdd = new Users();
                            _userToAdd.Firstname = _school.Name;
                            _userToAdd.Lastname = "בית ספר";
                            _userToAdd.Email = _school.InstituteID.ToString();
                            _userToAdd.IsActive = true;
                            _userToAdd.IsActivate = true;
                            _userToAdd.CreationDate = DateTime.Now;
                            _userToAdd.SchoolID = _school.ID;
                            _userToAdd.UserType = (int)eRole.SchoolManager;
                            //Update user password
                            _userToAdd.Password = Membership.GeneratePassword(6, 1);
                            _userToAdd.Password = Regex.Replace(_userToAdd.Password, @"[^a-zA-Z0-9]", m => "9");
                            var _newPassword = CryptoHelpers.EncryptPassword(_userToAdd.Password, CryptoHelpers.k_SHA256CryptoAlgorithm);
                            _userToAdd.Password = _newPassword;

                            //Add user
                            int results = _dbDapper.InsertOperation(QueriesRepository.AddUserFromSchoolCreation, new
                            {
                                _userToAdd.Firstname,
                                _userToAdd.Lastname,
                                _userToAdd.Email,
                                _userToAdd.Password,
                                _userToAdd.Phone,
                                _userToAdd.UserType,
                                _userToAdd.IsActive,
                                _userToAdd.IsActivate,
                                _userToAdd.SchoolID,
                                _userToAdd.CreationDate
                            });

                            //Need to send email to the user
                            string str = String.Format(@"<h4>שלום רב, מנהל בית ספר {0}
            </h4><p>חשבונך במערכת הטיסות לפולין אושר, על ידי מנהל המערכת. סיסמתך למערכת היא <strong>{1}</strong></p>
            <p>הכניסה למערכת מאובטחת. יש לשמור את פרטי הכניסה והסיסמה במקום בטוח.</p>
            <p>בברכה, מנהלת פולין</p>", _school.Name, _userToAdd.Password);

                            str += "<p>אין להשיב למיל זה <br />בכל נושא יש לפנות למינהלת פולין</p>";
                            try
                            {
                                SendMail.SendSingleMail(_school.ManagerEmail, "התחברות למערכת הטיסות לפולין", str);
                            }
                            catch (Exception ex)
                            {

                            }


                        }


                        return RedirectToAction("GetSchools");
                    }
                    catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
                    {
                        transaction.Rollback();
                        //Exception raise = dbEx;
                        //foreach (var validationErrors in dbEx.EntityValidationErrors)
                        //{
                        //    foreach (var validationError in validationErrors.ValidationErrors)
                        //    {
                        //        string message = string.Format("{0}:{1}", validationErrors.Entry.Entity.ToString(),
                        //            validationError.ErrorMessage);
                        //        //raise a new exception inserting the current one as the InnerException
                        //        raise = new InvalidOperationException(message, raise);
                        //    }
                        //}
                        //throw raise;
                    }
                    catch (Exception e)
                    {
                        transaction.Rollback();
                        //EventLog elog = new EventLog();
                        //elog.Source = "Polin Application";
                        //elog.WriteEntry(e.Message);
                        //throw new Exception("Mail.Send: " + e.Message);
                    }
                }

            }

            return View(model);
        }

        //[EndiloAuthorization(eRole.Administrator, eRole.Staf, eRole.Agent, eRole.CountyManager)]
        [HttpGet]
        public ActionResult EditSchool(int ID, int? groupID = null)
        {
            if (ID != null)
            {
                var school = _db.Schools.FirstOrDefault(s => s.ID.Equals(ID));

                if (FunctionHelpers.User.RoleID == (int)eRole.CountyManager && FunctionHelpers.User.RegionID != school.Region)
                {
                    return RedirectToAction("GetGroupsDetails", "Groups", new { area = "Administration" });
                }
                else if (FunctionHelpers.User.RoleID == (int)eRole.SchoolManager)
                {
                    // Get the school manager
                    int _SchoolId = _dbDapper.GetSchoolIDByUserID(FunctionHelpers.User.UserID);
                    if (_SchoolId == 0)
                    {
                        return RedirectToAction("GetTripsListView", "Expeditions", new { area = "" });
                    }
                }

                SchoolViewModel model = new SchoolViewModel();
                model.ID = school.ID;
                model.InstituteID = school.InstituteID;
                model.IsActive = school.IsActive;
                model.Manager = school.Manager;
                model.ManagerEmail = school.ManagerEmail;
                model.ManagerPhone = school.ManagerPhone;
                model.Name = school.Name;
                model.Fax = school.Fax;
                model.Phone = school.Phone;
                model.Email = school.Email;
                model.City = school.City;
                model.IsAdministrative = school.IsAdministrativeInstitude == null ? false : (bool)school.IsAdministrativeInstitude;
                if (school.Region != null)
                {
                    model.RegionEnum = school.RegionEnum;
                }


                model.IsActivated = _db.Users.FirstOrDefault(u => u.Email == school.InstituteID.ToString()).IsActivate;

                model.GroupID = groupID;
                return View(model);
            }

            return RedirectToAction("GetSchools");
        }

        [HttpGet]
        public ActionResult EditSchoolManager()
        {
            int _SchoolId = _dbDapper.GetSchoolIDByUserID(FunctionHelpers.User.UserID);
            if (_SchoolId != 0)
            {
                var school = _db.Schools.FirstOrDefault(s => s.ID.Equals(_SchoolId));

                SchoolViewModel model = new SchoolViewModel();
                model.ID = school.ID;
                model.InstituteID = school.InstituteID;
                model.IsActive = school.IsActive;
                model.Manager = school.Manager;
                model.ManagerEmail = school.ManagerEmail;
                model.ManagerPhone = school.ManagerPhone;
                model.Name = school.Name;
                model.Fax = school.Fax;
                model.Phone = school.Phone;
                model.Email = school.Email;
                model.City = school.City;
                model.IsAdministrative = school.IsAdministrativeInstitude == null ? false : (bool)school.IsAdministrativeInstitude;
                if (school.Region != null)
                {
                    model.RegionEnum = school.RegionEnum;
                }


                model.IsActivated = _db.Users.FirstOrDefault(u => u.Email == school.InstituteID.ToString()).IsActivate;

                return View("EditSchool", model);
            }

            return RedirectToAction("GetTripsListView", "Expeditions", new { area = "" });
        }


        //[EndiloAuthorization(eRole.Administrator, eRole.Staf)]
        [PermissionAuthorization(PermissionBitEnum.Administration_SchoolEdit)]
        [HttpPost]
        public ActionResult EditSchool(SchoolViewModel model)
        {
            if (ModelState.IsValid)
            {
                var school = _db.Schools.FirstOrDefault(s => s.ID.Equals(model.ID));

                school.Name = model.Name;
                school.InstituteID = model.InstituteID;
                school.Email = model.Email;
                school.Manager = model.Manager;
                school.ManagerEmail = model.ManagerEmail;
                school.ManagerPhone = model.ManagerPhone;
                school.Phone = model.Phone;
                school.Fax = model.Fax;
                school.IsActive = model.IsActive;
                school.UpdateDate = DateTime.Now;
                school.City = model.City;
                school.Region = (int)model.RegionEnum;
                school.IsAdministrativeInstitude = model.IsAdministrative;

                _db.Schools.AddOrUpdate(school);

                int results = _db.SaveChanges();

            }

            if (model.GroupID != null)
            {
                return RedirectToAction("GroupFolderManagement", "Groups", new { ID = model.GroupID });
            }
            return RedirectToAction("GetSchools");
        }

        [PermissionAuthorization(PermissionBitEnum.Administration_SchoolEdit)]
        [HttpPost]
        public ActionResult EditSchoolManager(SchoolViewModel model)
        {
            if (ModelState.IsValid)
            {
                var school = _db.Schools.FirstOrDefault(s => s.ID.Equals(model.ID));

                school.Name = model.Name;
                school.InstituteID = model.InstituteID;
                school.Email = model.Email;
                school.Manager = model.Manager;
                school.ManagerEmail = model.ManagerEmail;
                school.ManagerPhone = model.ManagerPhone;
                school.Phone = model.Phone;
                school.Fax = model.Fax;
                school.IsActive = model.IsActive;
                school.UpdateDate = DateTime.Now;
                school.City = model.City;
                school.Region = (int)model.RegionEnum;
                school.IsAdministrativeInstitude = model.IsAdministrative;

                _db.Schools.AddOrUpdate(school);

                int results = _db.SaveChanges();

            }

            return RedirectToAction("GetTripsListView", "Expeditions", new { area = "" });
        }


        //[EndiloAuthorization(eRole.Administrator, eRole.Staf)]
        [PermissionAuthorization(PermissionBitEnum.Administration_SchoolEdit)]
        [HttpGet]
        public ActionResult GrantAccess(int InstituteID)
        {
            try
            {
                //Get User Details
                Users _user = _db.Users.FirstOrDefault(u => u.Email.Equals(InstituteID.ToString()));
                Schools _school = _db.Schools.FirstOrDefault(s => s.InstituteID.Equals(InstituteID));

                _user.IsActivate = true;

                //Update user password
                _user.Password = GetRandomNumber();
                //_user.Password = Membership.GeneratePassword(6, 1);
                //_user.Password = Regex.Replace(_user.Password, @"^(?=.*\d).{4,8}", m => "9");

                //Need to send email to the user
                string str = String.Format(@"<h4>שלום רב, מנהל בית ספר {0}
            </h4><p>חשבונך במערכת הטיסות לפולין אושר, על ידי מנהל המערכת. סיסמתך למערכת היא <strong>{1}</strong></p>
            <p>הכניסה למערכת מאובטחת. יש לשמור את פרטי הכניסה והסיסמה במקום בטוח.</p>
            <p>בברכה, מנהלת פולין</p>", _school.Name, _user.Password);

                str += "<p>אין להשיב למיל זה <br />בכל נושא יש לפנות למינהלת פולין</p>";

                SendMail.SendSingleMail(_school.ManagerEmail, "התחברות למערכת הטיסות לפולין", str);

                FlightMembershipProvider membership = new FlightMembershipProvider();
                _user = membership.UpdateUser(_user);
            }
            catch (Exception e)
            {

                EventLog elog = new EventLog();
                elog.Source = "Polin Application";
                elog.WriteEntry(e.Message);
                throw new Exception("Mail.Send: " + e.Message);
            }

            return RedirectToAction("GetSchools");
        }

        private string GetRandomNumber()
        {
            var chars = "0123456789";
            var random = new Random();
            var result = new string(
                Enumerable.Repeat(chars, 6)
                          .Select(s => s[random.Next(s.Length)])
                          .ToArray());
            return result;
        }

        [PermissionAuthorization(PermissionBitEnum.Administration_SchoolDelete)]
        [HttpGet]
        public PartialViewResult DeleteSchool(int ID, string Title)
        {
            DeleteViewModel model = new DeleteViewModel();
            model.ID = ID;
            model.Title = Title;
            model.Controller = "Schools";
            model.Action = "DeleteSchool";
            model.Header = SchoolsStrings.DeletingSchool;
            model.IsDeletable = true;

            return PartialView("_DeleteItem", model);
        }

        [PermissionAuthorization(PermissionBitEnum.Administration_SchoolDelete)]
        [HttpPost]
        public ActionResult DeleteSchool(DeleteViewModel model)
        {
            bool result = _dbDapper.DeactivateSchool(model.ID);

            return RedirectToAction("GetSchools");
        }

        [PermissionAuthorization(PermissionBitEnum.Administration_School_Remarks_View)]
        [HttpGet]
        public PartialViewResult GetSchoolRemarks(int ID)
        {
            List<SchoolRemarksViewModel> model = new List<SchoolRemarksViewModel>();

            model = _dbDapper.GetRecords<SchoolRemarksViewModel>(QueriesRepository.GetSchoolAllGroupRemarks, new { SchoolID = ID }).ToList();

            return PartialView("_SchoolRemaks", model);
        }

        [PermissionAuthorization(PermissionBitEnum.Administration_School_Remarks_View)]
        [HttpGet]
        public PartialViewResult GetGenerelRemarks(int ID)
        {
            EntityRemarksViewModel model = new EntityRemarksViewModel();
            model.EntityType = eEntityType.School;
            model.Remarks = _dbDapper.GetRecords<EntityRemarkViewModel>(QueriesRepository.GetGEnericRemarksByEntityID, new { EntityType = (int)model.EntityType, ID });
            model.EntityID = ID;

            return PartialView("_GenericRemarks", model);
        }

        [PermissionAuthorization(PermissionBitEnum.Administration_School_Remarks_View)]
        [HttpGet]
        public PartialViewResult ManageGenericRemark(int EntityID)
        {
            EntityRemarkViewModel model = new EntityRemarkViewModel();
            model.EntityID = EntityID;
            model.EntityTypeEnum = eEntityType.School;

            return PartialView("_ManageGenericRemark", model);
        }

        [PermissionAuthorization(PermissionBitEnum.Administration_School_Remarks_View)]
        [HttpPost]
        public ActionResult ManageGenericRemark(EntityRemarkViewModel model)
        {
            if (ModelState.IsValid)
            {
                model.UserID = FunctionHelpers.User.UserID;
                model.CreationDate = DateTime.Now;
                model.UpdateDate = model.CreationDate;
                int results = _dbDapper.InsertOperation(QueriesRepository.AddGenericRemarkByEntityID, new
                {
                    model.EntityID,
                    model.EntityType,
                    model.Notes,
                    model.UserID,
                    model.CreationDate,
                    model.UpdateDate
                });
            }

            return RedirectToAction("EditSchool", new { ID = model.EntityID });
        }

        [PermissionAuthorization(PermissionBitEnum.Administration_School_Remarks_Delete)]
        [HttpGet]
        public PartialViewResult DeleteGenericRemark(int ID, int EntityID)
        {
            DeleteViewModel model = new DeleteViewModel();
            model.ID = ID;
            model.Title = CommonStrings.Delete + " " + CommonStrings.Remark;
            model.Controller = "Schools";
            model.Action = "DeleteGenericRemark";
            model.Header = CommonStrings.Delete + " " + CommonStrings.Remark;
            model.IsDeletable = true;

            model.ReturnRefirectProperties = new List<int>();
            model.ReturnRefirectProperties.Add(EntityID);

            return PartialView("_DeleteItem", model);
        }

        [PermissionAuthorization(PermissionBitEnum.Administration_School_Remarks_Delete)]
        [HttpPost]
        public ActionResult DeleteGenericRemark(DeleteViewModel model)
        {
            if (ModelState.IsValid)
            {
                bool results = _dbDapper.UpdateOperation(QueriesRepository.DeleteGenericRemark, new { model.ID });
            }

            return RedirectToAction("EditSchool", new { ID = model.ReturnRefirectProperties[0] });

        }
    }
}