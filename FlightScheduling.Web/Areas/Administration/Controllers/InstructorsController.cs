﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using AutoMapper;
using Endilo.ExportToExcel;
using Flight.DAL.FlightDB.Methods;
using FlightScheduling.DAL;
using FlightScheduling.DAL.CustomEntities;
using FlightScheduling.Language;
using FlightScheduling.Web.Areas.Administration.Models.Agencies;
using FlightScheduling.Web.Areas.Administration.Models.Common;
using FlightScheduling.Web.Areas.Administration.Models.Instructors;
using FlightScheduling.Web.Infrastructure;
using FlightScheduling.Website.Infrastructure;
using LinqToExcel;
using Otakim.Website.Infrastructure;
using FlightScheduling.DAL.CustomEntities.Permissions;
using Flight.Membership;
using FlightScheduling.DAL.Abstractions;
using FlightScheduling.DAL.DomainEvents;

namespace FlightScheduling.Web.Areas.Administration.Controllers
{
    [Authorize]
    public class InstructorsController : Controller
    {
        private DBRepository _dbDapper = new DBRepository();

        [PermissionAuthorization(PermissionBitEnum.Instructors_Editing)]
        [HttpGet]
        public ActionResult CreateActualUsersForInstructors()
        {
            List<Result<InstructorToOpenUserModel>> results = new List<Result<InstructorToOpenUserModel>>();
            // Get all instructors who are active and has no user
            var instructors = _dbDapper.GetRecords<InstructorToOpenUserModel>(
                QueriesRepository.Instructor.GetAllInstructorsToOpenUsers, new { });

            foreach (var instructor in instructors)
            {
                var instructorCreatedDomainEvent = new InstructorCreatedDomainEvent(instructor.ID);
                var outboxMessage = DomainEventExtensions.GetOutboxFromDomainEvent(instructorCreatedDomainEvent);
                bool outboxResults = _dbDapper.ExecuteCommand(QueriesRepository.Outbox.InsertOutbox, outboxMessage);
                var result = new Result<InstructorToOpenUserModel>();
                result.IsSuccuss = outboxResults;
                result.Value = instructor;
                results.Add(result);
            }

            return View("InstructorToUsers", results);
        }

        [PermissionAuthorization(PermissionBitEnum.Instructors_Viewing)]
        [HttpGet]
        public ActionResult GetInstructors()
        {
            InstructorsViewModel model = new InstructorsViewModel();


            if (FunctionHelpers.User.RoleID == (int)eRole.TrainingManager)
            {
                model.AllowFilterByRole = false;
                model.RoleID = (int)eInstructorRoleType.AccompanyingMentor;
            }

            model.Roles.Items =
                _dbDapper.GetAllInstructorRoles(AppConfig.CurrentLangruageTwoLetters)
                    .Select(r => new CustomSelectListItem()
                    {
                        ID = r.ID,
                        Name = r.Name
                    }).ToList();

            //if (FunctionHelpers.User.RoleID != (int)eRole.TrainingManager)
            //{
            //    model.Instructors =
            //    Mapper.Map<List<Instructors>, List<InstructorViewModel>>(_dbDapper.GetAllInstructors(model.RoleID, null,
            //        null));
            //}

            // Save Instructor state in session
            //Session.Add("InstructorDetailList", model.Instructors);

            return View(model);
        }

        [PermissionAuthorization(PermissionBitEnum.Instructors_Viewing)]
        [HttpPost]
        public ActionResult GetFilteredInstructors(InstructorsViewModel model)
        {
            if (FunctionHelpers.User.RoleID == (int)eRole.TrainingManager)
            {
                model.AllowFilterByRole = false;
                model.RoleID = (int)eInstructorRoleType.AccompanyingMentor;
            }

            model.Roles.Items =
                _dbDapper.GetAllInstructorRoles(AppConfig.CurrentLangruageTwoLetters)
                    .Select(r => new CustomSelectListItem()
                    {
                        ID = r.ID,
                        Name = r.Name
                    }).ToList();

            if (FunctionHelpers.User.RoleID == (int)eRole.TrainingManager)
            {
                if (!String.IsNullOrEmpty(model.InstructorID) || !String.IsNullOrEmpty(model.LastName))
                {
                    model.Instructors =
                        Mapper.Map<List<Instructors>, List<InstructorViewModel>>(_dbDapper.GetAllInstructors(null,
                            model.InstructorID, model.LastName));
                }
            }
            else
            {
                model.Instructors =
                    Mapper.Map<List<Instructors>, List<InstructorViewModel>>(_dbDapper.GetAllInstructors(model.RoleID,
                        model.InstructorID, model.LastName));
            }

            // Save Instructor state in session
            Session.Add("InstructorDetailList", model.Instructors);

            return View("GetInstructors", model);
        }

        [PermissionAuthorization(PermissionBitEnum.Instructors_Viewing)]
        [HttpGet]
        public ActionResult DownloadInstructorList()
        {
            List<InstructorViewModel> _instructors = (List<InstructorViewModel>)Session["InstructorDetailList"];
            ExportToExcel<InstructorViewModel> exc =
                new ExportToExcel<InstructorViewModel>(_instructors, InstructorsStrings.InstructorsReport);
            var stream = exc.GetFile();
            string fileName = String.Format("{0} {1}.xlsx", InstructorsStrings.InstructorsReport,
                DateTime.Now.ToString("dd-MM-yyyy hh:ss"));
            string contentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";

            stream.Position = 0;
            return File(stream, contentType, fileName);
        }

        [PermissionAuthorization(PermissionBitEnum.Instructors_Editing, PermissionBitEnum.Training_Viewing)]
        [HttpGet]
        public PartialViewResult AddInstructor()
        {
            InstructorViewModel model = new InstructorViewModel();
            model.Roles.Items =
                _dbDapper.GetAllInstructorRoles(AppConfig.CurrentLangruageTwoLetters)
                    .Select(r => new CustomSelectListItem()
                    {
                        ID = r.ID,
                        Name = r.Name
                    }).ToList();
            model.UpdateSensativeData = true;

            return PartialView("_AddInstructor", model);
        }

        [PermissionAuthorization(PermissionBitEnum.Instructors_Editing, PermissionBitEnum.Training_Viewing)]
        [HttpGet]
        public PartialViewResult AddNewInstructor(int TrainingID)
        {
            InstructorViewModel model = new InstructorViewModel();
            model.Roles.Items =
                _dbDapper.GetAllInstructorRoles(AppConfig.CurrentLangruageTwoLetters)
                    .Select(r => new CustomSelectListItem()
                    {
                        ID = r.ID,
                        Name = r.Name
                    }).ToList();
            model.TrainingID = TrainingID;
            model.UpdateSensativeData = true;
            return PartialView("_AddInstructor", model);
        }

        [PermissionAuthorization(PermissionBitEnum.Instructors_Editing, PermissionBitEnum.Training_Viewing)]
        [HttpGet]
        public PartialViewResult UpdateInstructor(int ID)
        {
            InstructorViewModel model = Mapper.Map<Instructors, InstructorViewModel>(_dbDapper.GetInstructor(ID));

            model.UpdateOperation = true;

            //Allowing administrators and Staf to update the Passport and ID
            if (User.IsInRole(eRole.Administrator.ToString()) || User.IsInRole(eRole.Staf.ToString()))
            {
                model.UpdateSensativeData = true;
            }

            return PartialView("_AddInstructor", model);
        }

        [PermissionAuthorization(PermissionBitEnum.Instructors_Editing, PermissionBitEnum.Training_Viewing)]
        [HttpPost]
        public ActionResult AddInstructor(InstructorViewModel model)
        {
            if (ModelState.IsValid)
            {
                Instructors instructor = Mapper.Map<InstructorViewModel, Instructors>(model);

                Trainings training = new Trainings();
                if (model.TrainingID != null)
                {
                    training = _dbDapper.GetTraining((int)model.TrainingID);
                    instructor.RoleID = training.RoleID;
                }


                instructor.UpdateDate = DateTime.Now;
                instructor.UserID = FunctionHelpers.User.UserID;


                if (model.UpdateOperation)
                {
                    _dbDapper.UpdateInstructor(instructor);
                }
                else
                {
                    if (model.TrainingID != null)
                    {
                        bool updatingExistingInstructor = false;
                        if (_dbDapper.CheckInstructorExistByInstructorID(model.InstructorID))
                        {
                            Instructors _existInstructor = _dbDapper.GetInstructorByInstructorID(model.InstructorID);

                            if (_existInstructor.InstructorRoles.AllowChangingRole)
                            {
                                instructor.ID = _existInstructor.ID;
                                instructor.ValidityStartDate = null;
                                instructor.ValidityEndTime = null;

                                updatingExistingInstructor = true;
                            }
                            else
                            {
                                //TODO: returing a message that istructor is already exist in the sytem and it can't be added
                                return RedirectToAction("TrainingFolder", new { ID = model.TrainingID });
                            }
                        }

                        instructor.CreationDate = DateTime.Now;

                        InstructorTrainings instTrainings = new InstructorTrainings();
                        instTrainings.TrainingID = (int)model.TrainingID;
                        instTrainings.CountToValidity = false;

                        if (updatingExistingInstructor)
                        {
                            int result = _dbDapper.UpdatingInstructorAndTraining(instructor, instTrainings, true);
                        }
                        else
                        {
                            int instructorID = _dbDapper.InsertNewInstructorAndTraining(instructor, instTrainings);
                        }
                    }
                }


                if (model.TrainingID != null)
                {
                    return RedirectToAction("TrainingFolder", new { ID = model.TrainingID });
                }

                return RedirectToAction("GetInstructors");
            }

            return View("_AddInstructor", model);
        }

        [PermissionAuthorization(PermissionBitEnum.Instructors_Deleting, PermissionBitEnum.Training_Viewing)]
        [HttpGet]
        public PartialViewResult UnRegisterInstructor(int ID, string Title)
        {
            DeleteViewModel model = new DeleteViewModel();
            model.ID = ID;
            model.Title = Title;
            model.Header = InstructorsStrings.UnregisterInstructor;
            model.Action = "UnRegisterInstructor";
            model.Controller = "Instructors";
            model.IsDeletable = true;

            return PartialView("_DeleteItem", model);
        }

        [PermissionAuthorization(PermissionBitEnum.Instructors_Deleting, PermissionBitEnum.Training_Viewing)]
        [HttpPost]
        public ActionResult UnRegisterInstructor(DeleteViewModel model)
        {
            var record = _dbDapper.GetInstructorTrainingRecordByID(model.ID);
            _dbDapper.UnregisterInstructorTraining(model.ID);
            return RedirectToAction("TrainingFolder", new { ID = record.TrainingID });
        }

        [PermissionAuthorization(PermissionBitEnum.Instructors_Deleting, PermissionBitEnum.Training_Viewing)]
        [HttpGet]
        public PartialViewResult DeleteInstructor(int ID, string Title)
        {
            DeleteViewModel model = new DeleteViewModel();
            model.ID = ID;
            model.Title = Title;
            model.Header = InstructorsStrings.DeleteInstructor;
            model.Action = "DeleteInstructor";
            model.Controller = "Instructors";
            model.IsDeletable = true;

            return PartialView("_DeleteItem", model);
        }

        [PermissionAuthorization(PermissionBitEnum.Instructors_Deleting, PermissionBitEnum.Training_Viewing)]
        [HttpPost]
        public ActionResult DeleteInstructor(DeleteViewModel model)
        {
            _dbDapper.SetInstructorInActive(model.ID, false);
            return RedirectToAction("GetInstructors");
        }


        [PermissionAuthorization(PermissionBitEnum.Training_Viewing)]
        [HttpGet]
        public ActionResult GetTrainings()
        {
            TrainingsViewModel model = new TrainingsViewModel();

            if (SystemConfigurationRepository.TrainingsFilterStartDate != null)
            {
                model.FilterFromDate = SystemConfigurationRepository.TrainingsFilterStartDate;
            }

            if (SystemConfigurationRepository.TrainingsFilterEndDate != null)
            {
                model.FilterToDate = SystemConfigurationRepository.TrainingsFilterEndDate;
            }

            model.Roles.Items =
                _dbDapper.GetAllInstructorRoles(AppConfig.CurrentLangruageTwoLetters)
                    .Select(r => new CustomSelectListItem()
                    {
                        ID = r.ID,
                        Name = r.Name
                    }).ToList();

            model.TrainingTypes.Items =
                _dbDapper.GetRecords<CustomSelectListItem>(QueriesRepository.GetTrainingTypes, null).ToList();


            if (FunctionHelpers.User.RoleID == (int)eRole.TrainingManager)
            {
                model.Region = (eSchoolRegion)FunctionHelpers.User.RegionID;
                model.Trainings =
                    Mapper.Map<List<Training>, List<TrainingViewModel>>(
                        _dbDapper.GetAllTrainings(null, model.FilterFromDate, model.FilterToDate,
                                model.CompletedTraining, model.TrainingLocation, model.Region, model.TrainingTypeID)
                            .OrderBy(m => m.Name).ToList());

                //model.Trainings =
                //    Mapper.Map<List<Trainings>, List<TrainingViewModel>>(
                //        _dbDapper.GetAllTrainings(null, FunctionHelpers.User.UserID, model.FilterFromDate, model.FilterToDate, model.CompletedTraining, model.TrainingLocation, null).OrderBy(m => m.Name).ToList());
            }
            else
            {
                model.Trainings =
                    Mapper.Map<List<Training>, List<TrainingViewModel>>(_dbDapper
                        .GetAllTrainings(null, model.FilterFromDate, model.FilterToDate, model.CompletedTraining,
                            model.TrainingLocation, null, model.TrainingTypeID).OrderBy(m => m.Name).ToList());
                //model.Trainings =
                //    Mapper.Map<List<Trainings>, List<TrainingViewModel>>(_dbDapper.GetAllTrainings(null, null, model.FilterFromDate, model.FilterToDate, model.CompletedTraining, model.TrainingLocation, null).OrderBy(m => m.Name).ToList());
            }


            return View(model);
        }

        [PermissionAuthorization(PermissionBitEnum.Training_Viewing)]
        [HttpPost]
        public ActionResult GetTrainings(TrainingsViewModel model)
        {
            if (FunctionHelpers.User.RoleID == (int)eRole.TrainingManager)
            {
                model.Region = (eSchoolRegion)FunctionHelpers.User.RegionID;

                model.Trainings =
                    Mapper.Map<List<Training>, List<TrainingViewModel>>(
                        _dbDapper.GetAllTrainings(null, model.FilterFromDate, model.FilterToDate,
                                model.CompletedTraining, model.TrainingLocation, model.Region, model.TrainingTypeID)
                            .OrderBy(m => m.Name).ToList());

                //    model.Trainings =
                //        Mapper.Map<List<Trainings>, List<TrainingViewModel>>(
                //            _dbDapper.GetAllTrainings(model.RoleID, FunctionHelpers.User.UserID, model.FilterFromDate, model.FilterToDate, model.CompletedTraining, model.TrainingLocation, model.Region).OrderBy(m => m.Name).ToList());
            }
            else
            {
                model.Trainings =
                    Mapper.Map<List<Training>, List<TrainingViewModel>>(
                        _dbDapper.GetAllTrainings(model.RoleID, model.FilterFromDate, model.FilterToDate,
                                model.CompletedTraining, model.TrainingLocation, model.Region, model.TrainingTypeID)
                            .OrderBy(m => m.Name).ToList());

                //model.Trainings =
                //    Mapper.Map<List<Trainings>, List<TrainingViewModel>>(
                //        _dbDapper.GetAllTrainings(model.RoleID, null, model.FilterFromDate, model.FilterToDate, model.CompletedTraining, model.TrainingLocation, model.Region).OrderBy(m => m.Name).ToList());
            }

            model.Roles.Items =
                _dbDapper.GetAllInstructorRoles(AppConfig.CurrentLangruageTwoLetters)
                    .Select(r => new CustomSelectListItem()
                    {
                        ID = r.ID,
                        Name = r.Name
                    }).ToList();

            model.TrainingTypes.Items =
                _dbDapper.GetRecords<CustomSelectListItem>(QueriesRepository.GetTrainingTypes, null).ToList();


            return View(model);
        }

        [PermissionAuthorization(PermissionBitEnum.Training_Editing)]
        [HttpGet]
        public PartialViewResult AddTraining()
        {
            TrainingViewModel model = new TrainingViewModel();
            model.Roles.Items =
                _dbDapper.GetAllInstructorRoles(AppConfig.CurrentLangruageTwoLetters)
                    .Select(r => new CustomSelectListItem()
                    {
                        ID = r.ID,
                        Name = r.Name
                    }).ToList();

            model.TrainingTypes.Items =
                _dbDapper.GetRecords<CustomSelectListItem>(QueriesRepository.GetTrainingTypes, null).ToList();

            model.TrainingDate = DateTime.Now;

            return PartialView("_ManageTraining", model);
        }

        [PermissionAuthorization(PermissionBitEnum.Training_Editing)]
        [HttpGet]
        public PartialViewResult UpdateTraining(int ID)
        {
            TrainingViewModel model = new TrainingViewModel();
            model = Mapper.Map<Trainings, TrainingViewModel>(_dbDapper.GetTraining(ID));
            model.Roles.Items = _dbDapper.GetAllInstructorRoles(AppConfig.CurrentLangruageTwoLetters)
                .Select(r => new CustomSelectListItem()
                {
                    ID = r.ID,
                    Name = r.Name
                }).ToList();

            model.TrainingTypes = new CustomSelectList();
            model.TrainingTypes.Items =
                _dbDapper.GetRecords<CustomSelectListItem>(QueriesRepository.GetTrainingTypes, null).ToList();

            model.IsUpdate = true;

            return PartialView("_ManageTraining", model);
        }

        [PermissionAuthorization(PermissionBitEnum.Training_Editing)]
        [HttpPost]
        public ActionResult AddTraining(TrainingViewModel model)
        {
            if (ModelState.IsValid)
            {
                Training data = Mapper.Map<TrainingViewModel, Training>(model);
                data.CreatorID = FunctionHelpers.User.UserID;
                bool results = false;

                if (model.IsUpdate)
                {
                    //results = _dbDapper.UpdateTraining(data);
                    results = _dbDapper.UpdateOperation(QueriesRepository.UpdateTraining, new
                    {
                        data.Name,
                        data.TrainingDate,
                        TrainingLocation = data.TrainingLocationID,
                        data.TrainingHours,
                        TrainingType = data.TrainingValidationID,
                        data.RoleID,
                        data.CreatorID,
                        data.RegionID,
                        data.TrainingTypeID,
                        data.ID
                    });
                }
                else
                {
                    // results = _dbDapper.InsertTraining(data);
                    results = _dbDapper.UpdateOperation(QueriesRepository.InsertTraining, new
                    {
                        data.Name,
                        data.TrainingDate,
                        TrainingLocation = data.TrainingLocationID,
                        data.TrainingHours,
                        TrainingType = data.TrainingValidationID,
                        data.RoleID,
                        data.CreatorID,
                        data.RegionID,
                        data.TrainingTypeID
                    });
                }
            }

            return RedirectToAction("GetTrainings");
        }

        [PermissionAuthorization(PermissionBitEnum.Training_Viewing)]
        [HttpGet]
        public ActionResult TrainingFolder(int ID)
        {
            TrainingFolderViewModel model = new TrainingFolderViewModel();
            model.TrainingDetails = Mapper.Map<Trainings, TrainingViewModel>(_dbDapper.GetTraining(ID));
            model.Instructors =
                Mapper.Map<List<InstructorTrainings>, List<InstructorTrainingViewModel>>(
                    _dbDapper.GetInstructorTrainingBtTrainingID(ID));

            if (model.TrainingDetails.TrainingType == eTrainingType.RenewalTraining)
            {
                model.InstructorRegistrationToTraining.InstructorsRegistration.Items =
                    _dbDapper.GetInstructorsByRoleNotInTraining((int)model.TrainingDetails.RoleID, ID)
                        .Select(i => new CustomSelectListItem()
                        {
                            ID = i.ID,
                            Name = String.Format("{0} - {1} {2}", i.InstructorID, i.FirstNameHE, i.LastNameHE)
                        }).ToList();
                model.InstructorRegistrationToTraining.TrainingID = model.TrainingDetails.ID;
            }

            return View("TrainingFolder", model);
        }

        [PermissionAuthorization(PermissionBitEnum.Training_Deleting)]
        [HttpGet]
        public PartialViewResult DeleteTraining(int ID, string Title)
        {
            DeleteViewModel model = new DeleteViewModel();
            model.ID = ID;
            model.Title = Title;
            model.Header = InstructorsStrings.DeleteTraining;
            model.Action = "DeleteTraining";
            model.Controller = "Instructors";
            model.IsDeletable = _dbDapper.GetCountOfInstructorsInTraining(ID) == 0;

            if (!model.IsDeletable)
            {
                model.CustomMessage = InstructorsStrings.TrainingDeletionLimitAlert;
            }

            return PartialView("_DeleteItem", model);
        }

        [PermissionAuthorization(PermissionBitEnum.Training_Deleting)]
        [HttpPost]
        public ActionResult DeleteTraining(int ID)
        {
            if (ModelState.IsValid)
            {
                bool results = _dbDapper.DeleteTraining(ID);
            }

            return RedirectToAction("GetTrainings");
        }

        [PermissionAuthorization(PermissionBitEnum.Instructors_Editing, PermissionBitEnum.Training_Viewing)]
        [HttpGet]
        public PartialViewResult ImportNewInstructors(int TrainingID)
        {
            ImportInstructorsViewModel model = new ImportInstructorsViewModel();
            model.TrainingID = TrainingID;
            model.UserID = FunctionHelpers.User.UserID;

            return PartialView("_UploadImportInstructorsTraining", model);
        }

        [PermissionAuthorization(PermissionBitEnum.Instructors_Editing, PermissionBitEnum.Training_Viewing)]
        [HttpPost]
        public ActionResult ImportNewInstructors(ImportInstructorsViewModel model)
        {
            ImportRegistrationLogList logs = new ImportRegistrationLogList();
            if (ModelState.IsValid)
            {
                Trainings training = _dbDapper.GetTraining((int)model.TrainingID);

                string filename = String.Format("{0}{1}", DateTime.Now.ToString("ddMMyyyyhhmm"),
                    Path.GetExtension(model.Attachment.FileName));
                model.Attachment.SaveAs(
                    Server.MapPath(String.Format("{0}/{1}", AppConfig.TempImportRelativePath, filename)));

                var excel = new ExcelQueryFactory(Path.Combine(AppConfig.TempImportRoot, filename));
                excel.AddTransformation<InstructorViewModel>(x => x.OrganizationalStatus, c =>
                {
                    var organizationLStatus = eOrganizationalStatus.NotRelevant;
                    switch (c)
                    {
                        case "לא רלוונטי":
                        {
                            organizationLStatus = eOrganizationalStatus.NotRelevant;
                            break;
                        }
                        case "עוז לתמורה":
                        {
                            organizationLStatus = eOrganizationalStatus.Oz;
                            break;
                        }
                        case "אופק חדש":
                        {
                            organizationLStatus = eOrganizationalStatus.NewHorizon;
                            break;
                        }
                    }

                    return organizationLStatus;
                });

                excel.AddTransformation<InstructorViewModel>(x => x.InstructorID, c =>
                {
                    string NumID = c;

                    if (NumID.Length < 9)
                    {
                        while (NumID.Length < 9)
                        {
                            NumID = '0' + NumID;
                        }
                    }

                    return NumID;
                });

                excel.AddTransformation<InstructorViewModel>(x => x.SEXeum, c =>
                {
                    var sex = eParticipantsSex.Female;
                    switch (c)
                    {
                        case "זכר":
                        {
                            sex = eParticipantsSex.Male;
                            break;
                        }
                        case "נקבה":
                        {
                            sex = eParticipantsSex.Female;
                            break;
                        }
                    }

                    return sex;
                });


                var instructors = from c in excel.Worksheet<InstructorViewModel>("Instructors")
                    select c;


                foreach (var item in instructors)
                {
                    if (!String.IsNullOrEmpty(item.InstructorID))
                    {
                        ImportRegisterInstructorLog log = new ImportRegisterInstructorLog();
                        if (_dbDapper.CheckInstructorExistByInstructorID(item.InstructorID))
                        {
                            Instructors _existInstructor = _dbDapper.GetInstructorByInstructorID(item.InstructorID);
                            if (_dbDapper.CheckIfInstructorIsRegisteredToTrainingID(_existInstructor.ID,
                                    model.TrainingID))
                            {
                                log.Instructor = item;
                                log.OperationLog = eImportInstructorOperationLog.Fail;
                                log.OperationReason = eImportInstructorOperationReason.InstructorIsRegisteredToTraining;
                                logs.InstructorsLog.Add(log);
                            }
                            else
                            {
                                if (_existInstructor.InstructorRoles.AllowChangingRole)
                                {
                                    item.ID = _existInstructor.ID;
                                    item.ValidityStartDate = null;
                                    item.ValidityEndTime = null;

                                    Instructors instructor = Mapper.Map<InstructorViewModel, Instructors>(item);
                                    instructor.UpdateDate = DateTime.Now;
                                    instructor.RoleID = training.RoleID;


                                    InstructorTrainings instTrainings = new InstructorTrainings();
                                    instTrainings.TrainingID = (int)model.TrainingID;
                                    instTrainings.CountToValidity = false;

                                    int result =
                                        _dbDapper.UpdatingInstructorAndTraining(instructor, instTrainings, true);

                                    log.Instructor = item;
                                    log.OperationLog = eImportInstructorOperationLog.Success;
                                }
                                else
                                {
                                    log.Instructor = item;
                                    log.OperationLog = eImportInstructorOperationLog.Fail;
                                    log.OperationReason = eImportInstructorOperationReason.InstructorExists;
                                    logs.InstructorsLog.Add(log);
                                }
                            }
                        }
                        else
                        {
                            Instructors instructor = Mapper.Map<InstructorViewModel, Instructors>(item);
                            instructor.CreationDate = DateTime.Now;
                            instructor.UpdateDate = DateTime.Now;
                            instructor.RoleID = training.RoleID;

                            //Check content validity
                            bool validitycheck = true;
                            if (String.IsNullOrEmpty(instructor.FirstNameHE))
                            {
                                log.Instructor = item;
                                log.OperationLog = eImportInstructorOperationLog.Fail;
                                log.OperationReason = eImportInstructorOperationReason.FirstNameHeIsNull;
                                logs.InstructorsLog.Add(log);
                                validitycheck = false;
                            }
                            else if (String.IsNullOrEmpty(instructor.LastNameHE))
                            {
                                log.Instructor = item;
                                log.OperationLog = eImportInstructorOperationLog.Fail;
                                log.OperationReason = eImportInstructorOperationReason.LastNameNameHeIsNull;
                                logs.InstructorsLog.Add(log);
                                validitycheck = false;
                            }
                            else if (String.IsNullOrEmpty(instructor.FirstNameEN))
                            {
                                log.Instructor = item;
                                log.OperationLog = eImportInstructorOperationLog.Fail;
                                log.OperationReason = eImportInstructorOperationReason.FirstNameNameEnIsNull;
                                logs.InstructorsLog.Add(log);
                                validitycheck = false;
                            }
                            else if (String.IsNullOrEmpty(instructor.LastNameEN))
                            {
                                log.Instructor = item;
                                log.OperationLog = eImportInstructorOperationLog.Fail;
                                log.OperationReason = eImportInstructorOperationReason.LastNameNameEnIsNull;
                                logs.InstructorsLog.Add(log);
                                validitycheck = false;
                            }
                            else if (String.IsNullOrEmpty(instructor.InstructorID))
                            {
                                log.Instructor = item;
                                log.OperationLog = eImportInstructorOperationLog.Fail;
                                log.OperationReason = eImportInstructorOperationReason.InstructorIDIsNull;
                                logs.InstructorsLog.Add(log);
                                validitycheck = false;
                            }
                            else if (!Endilo.Helpers.Helpers.CheckIsraeliID(instructor.InstructorID))
                            {
                                log.Instructor = item;
                                log.OperationLog = eImportInstructorOperationLog.Fail;
                                log.OperationReason = eImportInstructorOperationReason.InstructorIDIsNotValid;
                                logs.InstructorsLog.Add(log);
                                validitycheck = false;
                            }

                            //else if (String.IsNullOrEmpty(instructor.PassportID))
                            //{
                            //    log.Instructor = item;
                            //    log.OperationLog = eImportInstructorOperationLog.Fail;
                            //    log.OperationReason = eImportInstructorOperationReason.PassportIDIsNull;
                            //    logs.InstructorsLog.Add(log);
                            //    validitycheck = false;
                            //}

                            if (validitycheck)
                            {
                                InstructorTrainings instTrainings = new InstructorTrainings();
                                instTrainings.TrainingID = (int)model.TrainingID;
                                instTrainings.CountToValidity = false;


                                int instructorID = _dbDapper.InsertNewInstructorAndTraining(instructor, instTrainings);

                                if (instructorID > 0)
                                {
                                    log.Instructor = item;
                                    log.OperationLog = eImportInstructorOperationLog.Success;
                                }
                                else
                                {
                                    log.Instructor = item;
                                    log.OperationLog = eImportInstructorOperationLog.Fail;
                                    log.OperationReason = eImportInstructorOperationReason.Other;
                                }
                            }

                            logs.InstructorsLog.Add(log);
                        }
                    }
                }

                System.IO.File.Delete(Path.Combine(AppConfig.TempImportRoot, filename));
            }

            return View("ImportNewInstructorsLog", logs);
        }

        [PermissionAuthorization(PermissionBitEnum.Training_ClosingTraining)]
        [HttpGet]
        public PartialViewResult CompleteTraining(int ID)
        {
            CompleteTrainingViewModel model = new CompleteTrainingViewModel();
            model.TrainingID = ID;
            return PartialView("_CompleteTraining", model);
        }

        [PermissionAuthorization(PermissionBitEnum.Training_ClosingTraining)]
        [HttpPost]
        public ActionResult CompleteTraining(CompleteTrainingViewModel model)
        {
            Trainings training = _dbDapper.GetTraining((int)model.TrainingID);

            List<InstructorTrainings> instructors = _dbDapper.GetInstructorTrainingBtTrainingID(model.TrainingID);

            foreach (var item in instructors)
            {
                var instructorTraining = item;
                var instructor = item.Instructors;
                InstructorRecordsHistory historyRecord = new InstructorRecordsHistory();
                bool results = false;

                // Check if Instructor Training is count for updating his validity
                if (training.TrainingTypeEnum == eTrainingType.InitialTraining)
                {
                    instructorTraining.CountToValidity = false;
                }
                else
                {
                    if (instructor.InstructorRoles.StartingRenewal == null)
                    {
                        if (training.TrainingDate >= instructor.ValidityStartDate &&
                            training.TrainingDate <= instructor.ValidityEndTime)
                        {
                            instructorTraining.CountToValidity = true;
                        }
                        else
                        {
                            instructorTraining.CountToValidity = false;
                        }
                    }
                    else
                    {
                        if (instructor.ValidityStartDate == null || instructor.ValidityEndTime == null)
                        {
                            instructorTraining.CountToValidity = true;
                        }
                        else if (training.TrainingDate >=
                                 ((DateTime)instructor.ValidityEndTime).AddDays(
                                     (int)instructor.InstructorRoles.StartingRenewal * -1))
                        {
                            instructorTraining.CountToValidity = true;
                        }
                        else
                        {
                            instructorTraining.CountToValidity = false;
                        }
                    }
                }


                if (instructor.InstructorRoles.AutomaticRenewal ||
                    training.TrainingTypeEnum == eTrainingType.InitialTraining)
                {
                    bool updatingInstructorTraining = false;
                    bool updateCertificate = true;


                    //Checking if to update the instructor validity
                    if (training.TrainingTypeEnum == eTrainingType.InitialTraining)
                    {
                        updatingInstructorTraining = true;
                        instructor.UpdateDate = DateTime.Now;

                        // Check if the instructor is already valid check
                        // Also check if the instructor has no validity by checking if the start or end date are null
                        if ((instructor.ValidityStartDate == null && instructor.ValidityEndTime == null) ||
                            (training.TrainingDate >= instructor.ValidityStartDate &&
                             training.TrainingDate <= instructor.ValidityEndTime))
                        {
                            instructor.ValidityStartDate = training.TrainingDate;

                            if (training.InstructorRoles.StartingValidityEnum == eStartingValidity.BeginningOfYear)
                            {
                                instructor.ValidityEndTime =
                                    new DateTime(training.TrainingDate.Year + 1, 1, 1).AddDays(
                                        training.InstructorRoles.ExpirationDaysRule);
                            }
                            else
                            {
                                instructor.ValidityEndTime =
                                    training.TrainingDate.AddDays(training.InstructorRoles.ExpirationDaysRule);
                            }
                        }
                    }
                    else
                    {
                        updatingInstructorTraining = _dbDapper.UpdateInstructorTraining(instructorTraining);

                        int TotalTrainingHours =
                            _dbDapper.GetTotalHoursOfTrainingForInstructor(instructor.ValidityStartDate,
                                instructor.ID);


                        if (instructor.InstructorRoles.RequiredHours <= TotalTrainingHours)
                        {
                            //Check if the instructor has done 'למידת עמיתים'
                            if (SystemConfigurationRepository.RolesToIncludeSpecialValidationRule.Any(x =>
                                    x == instructor.RoleID))
                            {
                                // Check if the role require special training, then check if this training is defined as special training
                                if (training.TrainingTypeID != null)
                                {
                                    int TotalTrainingByType = _dbDapper.GetRecords<int>(
                                        QueriesRepository.GetTotalHoursOfTrainingForInstructorbyTrainingType,
                                        new
                                        {
                                            Date = instructor.ValidityStartDate != null
                                                ? (DateTime)instructor.ValidityStartDate
                                                : training.TrainingDate,
                                            InstructorID = instructor.ID, training.TrainingTypeID
                                        }).FirstOrDefault();
                                    if (TotalTrainingByType < SystemConfigurationRepository
                                            .NumberOfRequiredHoursForSpecialTraining)
                                    {
                                        updateCertificate = false;
                                    }
                                }
                                else
                                {
                                    updateCertificate = false;
                                }
                            }

                            if (updateCertificate)
                            {
                                instructor.UpdateDate = DateTime.Now;
                                instructor.ValidityStartDate = training.TrainingDate;
                                if (training.InstructorRoles.StartingValidityEnum == eStartingValidity.BeginningOfYear)
                                {
                                    instructor.ValidityEndTime =
                                        new DateTime(training.TrainingDate.Year + 1, 1, 1).AddDays(
                                            training.InstructorRoles.ExpirationDaysRule);
                                }
                                else
                                {
                                    instructor.ValidityEndTime =
                                        training.TrainingDate.AddDays(training.InstructorRoles.ExpirationDaysRule);
                                }
                            }
                        }
                        else
                        {
                            updateCertificate = false;
                        }
                    }

                    if (updateCertificate)
                    {
                        historyRecord.CreationDate = DateTime.Now;
                        historyRecord.ValidityStartDate = (DateTime)instructor.ValidityStartDate;
                        historyRecord.ValidityEndDate = (DateTime)instructor.ValidityEndTime;
                        historyRecord.UserID = FunctionHelpers.User.UserID;
                        historyRecord.InstructorID = instructor.ID;

                        if (updatingInstructorTraining)
                        {
                            results = _dbDapper.UpdateInstructorQualification(instructor, historyRecord);
                        }
                        else
                        {
                            results = _dbDapper.TrainingCompletion(instructorTraining, instructor, historyRecord, true);
                        }
                    }
                    else
                    {
                        results = _dbDapper.TrainingCompletion(instructorTraining, null, null, false);
                    }
                }
                else
                {
                    results = _dbDapper.TrainingCompletion(instructorTraining, null, null, false);
                }
            }

            training.IsCompleted = true;
            bool result = _dbDapper.CloseTraining(training);
            return RedirectToAction("TrainingFolder", new { ID = model.TrainingID });
        }


        [PermissionAuthorization(PermissionBitEnum.Training_Viewing)]
        [HttpGet]
        public FileResult DownloadFileTemplate(string FilePath, string FileName)
        {
            byte[] fileBytes = System.IO.File.ReadAllBytes(Path.Combine(AppConfig.GroupsImportTemplateRoot, FilePath));
            var response = new FileContentResult(fileBytes, "application/octet-stream");
            response.FileDownloadName = FileName;
            return response;
        }

        [PermissionAuthorization(PermissionBitEnum.Instructors_Editing, PermissionBitEnum.Training_Viewing)]
        [HttpPost]
        public ActionResult RegisterInstructor(TrainingFolderViewModel model)
        {
            if (ModelState.IsValid)
            {
                Trainings training = _dbDapper.GetTraining(model.InstructorRegistrationToTraining.TrainingID);
                Instructors instructor = _dbDapper.GetInstructor(model.InstructorRegistrationToTraining.InstructorID);


                InstructorTrainings instTrainings = new InstructorTrainings();
                instTrainings.TrainingID = training.ID;
                instTrainings.InstructorID = instructor.ID;
                instTrainings.CountToValidity = false;

                //if (instructor.InstructorRoles.StartingRenewal == null)
                //{
                //    if (training.TrainingDate >= instructor.ValidityStartDate &&
                //        training.TrainingDate <= instructor.ValidityEndTime)
                //    {
                //        instTrainings.CountToValidity = true;
                //    }
                //    else
                //    {
                //        instTrainings.CountToValidity = false;
                //    }
                //}
                //else
                //{
                //    if (training.TrainingDate >=
                //        ((DateTime)instructor.ValidityEndTime).AddDays(
                //            (int)instructor.InstructorRoles.StartingRenewal * -1) &&
                //        training.TrainingDate <= instructor.ValidityEndTime)
                //    {
                //        instTrainings.CountToValidity = true;
                //    }
                //    else
                //    {
                //        instTrainings.CountToValidity = false;
                //    }
                //}

                bool RegistrationOperation = _dbDapper.RegisterInstructorToTraining(instTrainings);
            }

            return RedirectToAction("TrainingFolder", new { ID = model.InstructorRegistrationToTraining.TrainingID });
        }

        [PermissionAuthorization(PermissionBitEnum.Instructors_Viewing)]
        public ActionResult InstructorFolder(int ID, bool myFile = false)
        {
            // Checking if the user request his own personal file, if so, then setting the Id to logged on user
            if (myFile && FunctionHelpers.User.InstructorID != null)
            {
                ID = (int)FunctionHelpers.User.InstructorID;
            }

            InstructorFolderViewModel model = new InstructorFolderViewModel();

            model.InstructorDetails = Mapper.Map<Instructors, InstructorViewModel>(_dbDapper.GetInstructor(ID));

            // Check if User Exists
            Users _userToAdd = _dbDapper.GetRecords<Users>(QueriesRepository.GetUserByInstructorID, new
            {
                ID
            }).FirstOrDefault();

            model.UserIsCreated = _userToAdd != null && _userToAdd.ID > 0;

            model.Files =
                Mapper.Map<List<InstructorFiles>, List<InstructorFileViewModel>>(_dbDapper.GetInstructorFiles(ID));
            model.Notes =
                Mapper.Map<List<InstructorRemarks>, List<InstructorNoteViewModel>>(_dbDapper.GetInstructorNotes(ID));

            model.Trainings =
                Mapper.Map<List<InstructorTrainings>, List<InstructorTrainingViewModel>>(
                    _dbDapper.GetInstructorTrainings(ID));

            model.InstructorGroups =
                _dbDapper.GetRecords<InstructorGroupsViewModel>(QueriesRepository.GetInstructorGroups,
                    new { InstructorId = ID });

            if (model.InstructorDetails.ValidityStartDate != null)
            {
                model.HoursAcquired =
                    _dbDapper.GetTotalHoursOfTrainingForInstructor(
                        ((DateTime)model.InstructorDetails.ValidityStartDate).AddDays(1), model.InstructorDetails.ID);
                model.Percentange =
                    Convert.ToInt32(((decimal)model.HoursAcquired /
                                     (decimal)model.InstructorDetails.InstructorRoleModel.RequiredHours) * 100);
            }


            return View("InstructorFolder", model);
        }

        [PermissionAuthorization(PermissionBitEnum.Instructors_TrackingNotes_Editing)]
        [HttpGet]
        public PartialViewResult AddNote(int ID)
        {
            InstructorNoteViewModel model = new InstructorNoteViewModel();
            model.InstructorID = ID;
            model.UserID = FunctionHelpers.User.UserID;
            model.UpdateOperation = false;

            return PartialView("_ManageNote", model);
        }

        [PermissionAuthorization(PermissionBitEnum.Instructors_TrackingNotes_Editing)]
        [HttpPost]
        public ActionResult AddNote(InstructorNoteViewModel model)
        {
            InstructorRemarks data = Mapper.Map<InstructorNoteViewModel, InstructorRemarks>(model);

            data.CreationDate = DateTime.Now;
            data.UpdateDate = DateTime.Now;
            data.UserID = FunctionHelpers.User.UserID;

            if (model.UpdateOperation)
            {
                bool results = _dbDapper.UpdateInstructorRemark(data);
            }
            else
            {
                int returnedID = _dbDapper.InsertInstructorRemark(data);
            }


            return RedirectToAction("InstructorFolder", new { ID = data.InstructorID });
        }

        [PermissionAuthorization(PermissionBitEnum.Instructors_TrackingNotes_Editing)]
        [HttpGet]
        public PartialViewResult UpdateNote(int ID)
        {
            InstructorNoteViewModel model =
                Mapper.Map<InstructorRemarks, InstructorNoteViewModel>(_dbDapper.GetInstructorRemark(ID));

            model.UpdateOperation = true;

            return PartialView("_ManageNote", model);
        }

        [PermissionAuthorization(PermissionBitEnum.Instructors_TrackingNotes_Deleting)]
        [HttpGet]
        public PartialViewResult DeleteNote(int ID, string title)
        {
            DeleteViewModel model = new DeleteAgencyViewModel();

            model.ID = ID;
            model.Title = title;
            model.Action = "DeleteNote";
            model.Controller = "Instructors";
            model.IsDeletable = true;
            model.Header = InstructorsStrings.DeleteInstructorRemark;

            return PartialView("_DeleteItem", model);
        }

        [PermissionAuthorization(PermissionBitEnum.Instructors_TrackingNotes_Deleting)]
        [HttpPost]
        public ActionResult DeleteNote(DeleteViewModel model)
        {
            if (ModelState.IsValid)
            {
                bool results = _dbDapper.DeleteInstructorRemark(model.ID);
            }

            return RedirectToAction("InstructorFolder", new { ID = model.ID });
        }


        [PermissionAuthorization(PermissionBitEnum.Instructors_Files_Editing)]
        [HttpGet]
        public PartialViewResult AddFile(int ID)
        {
            InstructorFileViewModel model = new InstructorFileViewModel();
            model.InstructorID = ID;

            return PartialView("_ManageFile", model);
        }

        [PermissionAuthorization(PermissionBitEnum.Instructors_Files_Editing)]
        [HttpPost]
        public ActionResult AddFile(InstructorFileViewModel model)
        {
            if (model.IsUpdate)
            {
                ModelState["Attachment"].Errors.Clear();
            }

            if (ModelState.IsValid)
            {
                InstructorFiles file = Mapper.Map<InstructorFileViewModel, InstructorFiles>(model);
                file.CreationDate = DateTime.Now;
                file.UserID = FunctionHelpers.User.UserID;

                if (model.IsUpdate)
                {
                    bool result = _dbDapper.UpdateInstructorFile(file);
                }
                else
                {
                    //Save file
                    if (model.Attachment.HasFile())
                    {
                        //Check if directory exists
                        if (
                            !Directory.Exists(Path.Combine(AppConfig.InstructorFilesRelativePath,
                                model.InstructorID.ToString())))
                        {
                            Directory.CreateDirectory(
                                Server.MapPath(Path.Combine(AppConfig.InstructorFilesRelativePath,
                                    model.InstructorID.ToString())));
                        }

                        string filename = String.Format("{0}{1}{2}", model.InstructorID,
                            file.CreationDate.ToString("ddMMyyyyhhmm"), Path.GetExtension(model.Attachment.FileName));
                        model.Attachment.SaveAs(
                            Server.MapPath(String.Format("{0}/{1}/{2}", AppConfig.InstructorFilesRelativePath,
                                model.InstructorID, filename)));
                        file.Url = filename;

                        bool results = _dbDapper.InsertInstructorFile(file);
                    }
                }
            }

            return RedirectToAction("InstructorFolder", new { ID = model.InstructorID });
        }

        [PermissionAuthorization(PermissionBitEnum.Instructors_Files_Viewing)]
        [HttpGet]
        public FileResult DownloadFile(int InstructorID, string FilePath, string FileName)
        {
            byte[] fileBytes =
                System.IO.File.ReadAllBytes(Path.Combine(AppConfig.InstructorsFilesStorageRoot, InstructorID.ToString(),
                    FilePath));
            var response = new FileContentResult(fileBytes, "application/octet-stream");
            response.FileDownloadName = FileName;
            return response;
        }

        [PermissionAuthorization(PermissionBitEnum.Instructors_Files_Editing)]
        [HttpGet]
        public PartialViewResult UpdateFile(int ID)
        {
            InstructorFileViewModel model =
                Mapper.Map<InstructorFiles, InstructorFileViewModel>(_dbDapper.GetInstructorFile(ID));
            model.IsUpdate = true;

            return PartialView("_ManageFile", model);
        }

        [PermissionAuthorization(PermissionBitEnum.Instructors_UpdateCetificate)]
        [HttpGet]
        public PartialViewResult UpdateInstructorCertificate(int ID)
        {
            UpdateCertificateViewModel model = new UpdateCertificateViewModel();
            model.InstructorID = ID;

            return PartialView("_UpdateInstructorCertification", model);
        }

        [PermissionAuthorization(PermissionBitEnum.Instructors_UpdateCetificate)]
        [HttpPost]
        public ActionResult UpdateInstructorCertificate(UpdateCertificateViewModel model)
        {
            Instructors instructor = _dbDapper.GetInstructor(model.InstructorID);

            // Checking if this is not intial aquirering of the qualificaion
            if (!String.IsNullOrEmpty(instructor.CertificateID))
            {
                instructor.ValidityStartDate = instructor.ValidityEndTime;
                instructor.ValidityEndTime =
                    ((DateTime)instructor.ValidityEndTime).AddDays(instructor.InstructorRoles.ExpirationDaysRule);
            }

            instructor.CertificateID = model.CertificateID;
            instructor.UpdateDate = DateTime.Now;

            InstructorRecordsHistory historyRecord = new InstructorRecordsHistory();
            historyRecord.InstructorID = model.InstructorID;
            historyRecord.CreationDate = DateTime.Now;
            historyRecord.ValidityStartDate = (DateTime)instructor.ValidityStartDate;
            historyRecord.ValidityEndDate = (DateTime)instructor.ValidityEndTime;
            historyRecord.UserID = FunctionHelpers.User.UserID;

            bool results = _dbDapper.UpdateInstructorQualification(instructor, historyRecord);

            return RedirectToAction("InstructorFolder", new { ID = model.InstructorID });
        }

        [PermissionAuthorization(PermissionBitEnum.Instructors_Files_Deleting)]
        [HttpGet]
        public PartialViewResult DeleteFile(int ID, string fileName)
        {
            DeleteViewModel model = new DeleteViewModel();
            model.ID = ID;
            model.IsDeletable = true;
            model.Title = fileName;
            model.Action = "DeleteFile";
            model.Controller = "Instructors";
            model.Header = GroupsStrings.DeleteFile;

            return PartialView("_DeleteItem", model);
        }

        [PermissionAuthorization(PermissionBitEnum.Instructors_Files_Deleting)]
        [HttpPost]
        public ActionResult DeleteFile(DeleteViewModel model)
        {
            // Get the file info from the DB
            InstructorFiles file = _dbDapper.GetInstructorFile(model.ID);

            //Delete the file from the file server
            System.IO.File.Delete(Path.Combine(AppConfig.InstructorsFilesStorageRoot, file.InstructorID.ToString(),
                file.Url));

            bool operation = _dbDapper.DeleteInstrucotrFile(model.ID);

            return RedirectToAction("InstructorFolder", new { ID = file.InstructorID });
        }

        [PermissionAuthorization(PermissionBitEnum.Instructors_Editing)]
        [HttpGet]
        public PartialViewResult UpdateProfilePicture(int ID)
        {
            InstructorProfilePictureViewModel model = new InstructorProfilePictureViewModel();
            model.InstructorID = ID;

            return PartialView("_UpdateInstructorProfilePicture", model);
        }

        [PermissionAuthorization(PermissionBitEnum.Instructors_Editing)]
        [HttpPost]
        public ActionResult UpdateProfilePicture(InstructorProfilePictureViewModel model)
        {
            Instructors instructor = _dbDapper.GetInstructor(model.InstructorID);

            if (model.Attachment.HasFile())
            {
                //Check if directory exists
                if (
                    !Directory.Exists(Path.Combine(AppConfig.InstructorProfilePictureRelativePath,
                        model.InstructorID.ToString())))
                {
                    Directory.CreateDirectory(
                        Server.MapPath(Path.Combine(AppConfig.InstructorProfilePictureRelativePath,
                            model.InstructorID.ToString())));
                }

                string filename = String.Format("{0}{1}{2}", model.InstructorID,
                    "_ProfilePicture_", Path.GetExtension(model.Attachment.FileName));
                model.Attachment.SaveAs(
                    Server.MapPath(String.Format("{0}/{1}/{2}", AppConfig.InstructorProfilePictureRelativePath,
                        model.InstructorID, filename)));
                instructor.ImageURL = filename;

                bool results = _dbDapper.UpdateInstructorProfilePicture(model.InstructorID, filename);
            }

            return RedirectToAction("InstructorFolder", new { ID = model.InstructorID });
        }

        [PermissionAuthorization(PermissionBitEnum.Instructors_UpdateExpiration)]
        [HttpGet]
        public PartialViewResult UpdateInstructorExpiration(int ID, string CertificateID)
        {
            UpdateInstructorExpiation model = new UpdateInstructorExpiation();
            model.CertificateID = CertificateID;
            model.InstructorID = ID;
            model.InstructorRoleID = _dbDapper.GetInstructor(ID).RoleID;

            model.Roles.Items =
                _dbDapper.GetAllInstructorRoles(AppConfig.CurrentLangruageTwoLetters)
                    .Select(r => new CustomSelectListItem()
                    {
                        ID = r.ID,
                        Name = r.Name
                    }).ToList();

            return PartialView("_UpdateInstructorExpiration", model);
        }

        [PermissionAuthorization(PermissionBitEnum.Instructors_UpdateExpiration)]
        [HttpPost]
        public ActionResult UpdateInstructorExpiration(UpdateInstructorExpiation model)
        {
            InstructorRecordsHistory historyRecord = new InstructorRecordsHistory();
            historyRecord.InstructorID = model.InstructorID;
            historyRecord.UserID = FunctionHelpers.User.UserID;
            historyRecord.ValidityEndDate = model.ValidityEndTime;
            historyRecord.ValidityStartDate = model.ValidityStartDate;
            historyRecord.CreationDate = DateTime.Now;

            Instructors _instructor = new Instructors();
            _instructor.ID = model.InstructorID;
            _instructor.ValidityStartDate = model.ValidityStartDate;
            _instructor.ValidityEndTime = model.ValidityEndTime;
            _instructor.UpdateDate = DateTime.Now;
            _instructor.CertificateID = model.CertificateID;

            var results = _dbDapper.UpdateInstructorQualification(_instructor, historyRecord);

            // Check if instrcutor Role was assigned
            if (model.InstructorRoleID > -1)
            {
                bool updateInstructorRoleResults = _dbDapper.UpdateOperation(
                    QueriesRepository.UpdateInstructorRoleByInstructorID, new
                    {
                        RoleID = model.InstructorRoleID,
                        ID = _instructor.ID
                    });
            }

            return RedirectToAction("InstructorFolder", new { ID = model.InstructorID });
        }

        [PermissionAuthorization(PermissionBitEnum.Training_UpdateInstructorCertificate)]
        [HttpGet]
        public PartialViewResult UpdateInstructorsExpirationDates(int TrainingID)
        {
            UpdateTrainingInstructorsExpirationDates model = new UpdateTrainingInstructorsExpirationDates();
            model.TrainingID = TrainingID;

            return PartialView("_UpdateTrainingInstructorsExpirationDates", model);
        }

        [PermissionAuthorization(PermissionBitEnum.Training_UpdateInstructorCertificate)]
        [HttpPost]
        public ActionResult UpdateInstructorsExpirationDates(UpdateTrainingInstructorsExpirationDates model)
        {
            if (FunctionHelpers.User.RoleID == (int)eRole.Staf &&
                FunctionHelpers.User.RoleTypeEnum != eRoleType.Management)
            {
                return RedirectToAction("TrainingFolder", new { ID = model.TrainingID });
            }

            IEnumerable<InstructorTrainings>
                Instructors = _dbDapper.GetInstructorTrainingBtTrainingID(model.TrainingID);

            foreach (var instructor in Instructors)
            {
                InstructorRecordsHistory historyRecord = new InstructorRecordsHistory();
                historyRecord.InstructorID = instructor.InstructorID;
                historyRecord.UserID = FunctionHelpers.User.UserID;
                historyRecord.ValidityEndDate = model.ValidityEndTime;
                historyRecord.ValidityStartDate = model.ValidityStartDate;
                historyRecord.CreationDate = DateTime.Now;

                Instructors _instructor = new Instructors();
                _instructor.ID = instructor.InstructorID;
                _instructor.ValidityStartDate = model.ValidityStartDate;
                _instructor.ValidityEndTime = model.ValidityEndTime;
                _instructor.UpdateDate = DateTime.Now;

                var results = _dbDapper.UpdateInstructorQualification(_instructor, historyRecord);
            }

            return RedirectToAction("TrainingFolder", new { ID = model.TrainingID });
        }

        [PermissionAuthorization(PermissionBitEnum.Instructors_CreateUser)]
        [HttpGet]
        public ActionResult CreateActualUser(int ID)
        {
            InstructorViewModel model = Mapper.Map<Instructors, InstructorViewModel>(_dbDapper.GetInstructor(ID));

            //Add user from school
            Users _userToAdd = new Users();
            string email = model.Email;

            // Check if User Exists
            _userToAdd = _dbDapper.GetRecords<Users>(QueriesRepository.GetUserByEmail, new
            {
                Email = model.Email
            }).FirstOrDefault();

            if (!(_userToAdd != null && _userToAdd.ID > 0))
            {
                //User doesn't exits
                _userToAdd = new Users();
                _userToAdd.Firstname = model.FirstNameHE;
                _userToAdd.Lastname = model.LastNameHE;
                _userToAdd.Email = model.Email;
                _userToAdd.IsActive = true;
                _userToAdd.IsActivate = true;
                _userToAdd.CreationDate = DateTime.Now;
                _userToAdd.UserType = (int)eRole.GroupManager;
                //Update user password
                _userToAdd.Password = model.InstructorID;
                var _newPassword =
                    CryptoHelpers.EncryptPassword(_userToAdd.Password, CryptoHelpers.k_SHA256CryptoAlgorithm);
                _userToAdd.Password = _newPassword;

                // Set permission Role ID
                _userToAdd.PermissionRoleID = SystemConfigurationRepository.GroupManagerRoleId;

                // Set the instructor internal ID as the instructorID in the users table
                _userToAdd.InstructorID = model.ID;

                //Add user
                int results = _dbDapper.InsertOperation(QueriesRepository.AddUserFromInstructorFolder, new
                {
                    _userToAdd.Firstname,
                    _userToAdd.Lastname,
                    _userToAdd.Email,
                    _userToAdd.Password,
                    _userToAdd.Phone,
                    _userToAdd.UserType,
                    _userToAdd.IsActive,
                    _userToAdd.IsActivate,
                    _userToAdd.SchoolID,
                    _userToAdd.CreationDate,
                    _userToAdd.PermissionRoleID,
                    _userToAdd.InstructorID
                });

                //Need to send email to the user
                string str = String.Format(@"<h4>שלום רב, מנהל משלחת {0}
            </h4><p>חשבונך במערכת הטיסות לפולין אושר, על ידי מנהל המערכת. סיסמתך למערכת היא <strong>{1}</strong></p>
            <p>הכניסה למערכת מאובטחת. יש לשמור את פרטי הכניסה והסיסמה במקום בטוח.</p>
            <p>בברכה, מנהלת פולין</p>", model.FirstNameHE, _userToAdd.Password);

                str += "<p>אין להשיב למיל זה <br />בכל נושא יש לפנות למינהלת פולין</p>";
                try
                {
                    //SendMail.SendSingleMail(model.Email, "התחברות למערכת הטיסות לפולין", str);
                }
                catch (Exception ex)
                {
                }
            }
            else // If the user exists we need to update its Instructor ID in the users table
            {
                bool resutls = _dbDapper.UpdateOperation(QueriesRepository.UpdateInstructorIDInUsers,
                    new { InstructorID = model.ID, _userToAdd.ID });
            }

            return RedirectToAction("InstructorFolder", new { ID });
        }


        [PermissionAuthorization(PermissionBitEnum.Instructors_Viewing)]
        [HttpGet]
        public PartialViewResult UpdateMyFileInstructor()
        {
            int id = (int)FunctionHelpers.User.InstructorID;
            InstructorViewModel model = Mapper.Map<Instructors, InstructorViewModel>(_dbDapper.GetInstructor(id));

            model.UpdateOperation = true;


            return PartialView("_UpdateMyFile", model);
        }

        [PermissionAuthorization(PermissionBitEnum.Instructors_Viewing)]
        [HttpPost]
        public ActionResult UpdateMyFile(InstructorViewModel model)
        {
            if (ModelState.IsValid)
            {
                Instructors instructor = Mapper.Map<InstructorViewModel, Instructors>(model);
                instructor.UpdateDate = DateTime.Now;
                _dbDapper.UpdateInstructor(instructor);

                return RedirectToAction("InstructorFolder", new { ID = instructor.ID, myFile = true });
            }

            return RedirectToAction("InstructorFolder", new { ID = model.ID, myFile = true });
        }
    }
}