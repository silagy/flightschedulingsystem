﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using Flight.DAL.FlightDB.Methods;
using Flight.Membership;
using FlightScheduling.DAL;
using FlightScheduling.Language;
using FlightScheduling.Web.Areas.Administration.Models;
using FlightScheduling.Web.Areas.Administration.Models.Users;
using FlightScheduling.Website.Models.Account;
using Otakim.Website.Infrastructure;
using FlightScheduling.Web.Areas.Administration.Models.Schools;
using FlightScheduling.Web.Areas.Administration.Models.Common;
using FlightScheduling.Web.Areas.Administration.Models.Administration;
using FlightScheduling.DAL.CustomEntities.Notifications;
using Endilo.Emails;
using System.Net.Mail;

namespace FlightScheduling.Web.Areas.Administration.Controllers
{
    [EndiloAuthorization(eRole.Administrator)]
    public class UsersController : Controller
    {
        private FlightDB _db = new FlightDB();
        DBRepository _dbDapper = new DBRepository();

        protected override void Dispose(bool disposing)
        {
            _db.Dispose();
            base.Dispose(disposing);
        }
        //
        // GET: /Administration/Users/
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult GetUsers(int? PageNumber = 1, int? NumberOfRows = 20)
        {
            UsersViewModel model = new UsersViewModel();
            return View(model);
        }

        [HttpPost]
        public ActionResult GetUsers(UsersViewModel model)
        {
            if (model.Roles != null)
            {
                model.RoleID = (int)model.Roles;
            }
            model.ShowDeletedUsers = !model.ShowDeletedUsers;

            model.UserModels = _dbDapper.GetRecords<UserModel>(QueriesRepository.GetAllUsers, new
            {
                model.RoleID,
                model.Email,
                model.Name,
                model.ShowDeletedUsers,
                model.SelectedActivation
            }).ToList();

            return View(model);
        }

        [HttpGet]
        public ActionResult Edit(int UserID)
        {
            //Users user = _db.Users.FirstOrDefault(u => u.ID.Equals(UserID));
            Users user = _dbDapper.GetUserByID(UserID);

            switch (user.RuleEnum)
            {
                case eRole.Agent:
                    {
                        return RedirectToAction("EditAgent", new { ID = user.ID });
                        break;
                    }
                case eRole.Staf:
                    {
                        return RedirectToAction("EditStaf", new { ID = user.ID });
                        break;
                    }
                case eRole.Administrator:
                case eRole.Viewer:
                case eRole.GroupManager:
                    {
                        return RedirectToAction("EditUser", new { ID = user.ID });
                        break;
                    }
                case eRole.SchoolManager:
                    {
                        return RedirectToAction("EditSchoolUser", new { ID = user.ID });
                        break;
                    }
                case eRole.TrainingManager:
                    {
                        return RedirectToAction("EditTrainingManager", new { ID = user.ID });
                        break;
                    }
                case eRole.CountyManager:
                    {
                        return RedirectToAction("EditCountyManager", new { ID = user.ID });
                        break;
                    }

            }

            return null;
        }

        [HttpGet]
        public ActionResult AddAgent()
        {
            AgentViewModel model = new AgentViewModel();
            model.AgenciesModel.AddRange(_db.Agencies.Select(a => new AgencyViewModelBasic()
            {
                ID = a.ID,
                Name = a.Name
            }).ToList());

            model.LoadPermissionRoles();

            return View(model);
        }

        public ActionResult AddAgent(AgentViewModel model)
        {
            if (ModelState.IsValid)
            {
                //Check if user exsist and retun false
                if (_db.CheckIfUsersExists(model.Email))
                {
                    ModelState.AddModelError("UsersExsits", ValidationStrings.UserExistWithEmail);
                    return View(model);
                }

                Users _userToAdd = new Users();
                _userToAdd.RuleEnum = eRole.Agent;
                _userToAdd.UserType = (int)eRole.Agent;
                _userToAdd.CreationDate = DateTime.Now;
                _userToAdd.Firstname = model.FirstName;
                _userToAdd.Lastname = model.LastName;
                if (model.AgencyID == -1)
                {
                    //TODO: Validation
                }
                _userToAdd.AgencyID = model.AgencyID;
                _userToAdd.IsActive = true;
                _userToAdd.Password = model.Password;
                _userToAdd.Phone = model.Phone;
                _userToAdd.Email = model.Email;

                FlightMembershipProvider membership = new FlightMembershipProvider();

                _userToAdd = membership.CreateUser(_userToAdd);


                return RedirectToAction("GetUsers");
            }

            return View(model);
        }

        [HttpGet]
        public ActionResult EditAgent(int ID)
        {
            //Users user = _db.Users.FirstOrDefault(u => u.ID.Equals(ID));
            Users user = _dbDapper.GetUserByID(ID);

            AgentViewModel model = new AgentViewModel();

            model.UserID = user.ID;
            model.CreationDate = user.CreationDate;
            model.Email = user.Email;
            model.FirstName = user.Firstname;
            model.LastName = user.Lastname;
            model.Phone = user.Phone;
            model.Role = user.RuleEnum;
            model.AgencyID = (int)user.AgencyID;

            model.AgenciesModel.AddRange(_db.Agencies.Select(a => new AgencyViewModelBasic()
            {
                ID = a.ID,
                Name = a.Name
            }).ToList());

            return View(model);
        }

        [HttpPost]
        public ActionResult EditAgent(AgentViewModel model)
        {
            ModelState.Remove("Email");
            if (ModelState.IsValid)
            {



                Users user = _db.Users.FirstOrDefault(u => u.ID.Equals(model.UserID));

                if (!String.IsNullOrEmpty(model.Email) && model.Email != user.Email)
                {
                    if (_db.CheckIfUsersExists(model.Email))
                    {
                        ModelState.AddModelError("UsersExsits", ValidationStrings.UserExistWithEmail);
                        return View(model);
                    }
                }

                user.AgencyID = model.AgencyID;
                //user.Email = model.Email;
                user.Firstname = model.FirstName;
                user.Lastname = model.LastName;
                user.Password = model.Password;
                user.Phone = model.Phone;
                user.UserType = (int)model.Role;
                user.Email = model.Email;

                FlightMembershipProvider membership = new FlightMembershipProvider();
                membership.UpdateUser(user);

                return RedirectToAction("GetUsers");
            }

            return View(model);
        }


        [HttpGet]
        public ActionResult AddStaf()
        {

            StafViewModel model = new StafViewModel();
            model.LoadPermissionRoles();

            return View(model);
        }

        [HttpPost]
        public ActionResult AddStaf(StafViewModel model)
        {
            if (ModelState.IsValid)
            {
                //Check if user exsist and retun false
                if (_db.CheckIfUsersExists(model.Email))
                {
                    ModelState.AddModelError("UsersExsits", ValidationStrings.UserExistWithEmail);
                    return View(model);
                }

                Users _userToAdd = new Users();
                _userToAdd.RuleEnum = eRole.Staf;
                _userToAdd.UserType = (int)eRole.Staf;
                _userToAdd.CreationDate = DateTime.Now;
                _userToAdd.Firstname = model.FirstName;
                _userToAdd.Lastname = model.LastName;
                _userToAdd.IsActive = true;
                _userToAdd.Password = model.Password;
                _userToAdd.Phone = model.Phone;
                _userToAdd.Email = model.Email;
                _userToAdd.RoleTypeEnum = model.RoleType;
                _userToAdd.PermissionRoleID = model.PermissionRoleID;

                FlightMembershipProvider membership = new FlightMembershipProvider();

                _userToAdd = membership.CreateUser(_userToAdd);

                return RedirectToAction("GetUsers");
            }

            return View(model);
        }

        [HttpGet]
        public ActionResult AddTrainingManager()
        {

            TrainingManagerViewModel model = new TrainingManagerViewModel();
            model.LoadPermissionRoles();

            return View(model);
        }

        [HttpPost]
        public ActionResult AddTrainingManager(TrainingManagerViewModel model)
        {
            if (ModelState.IsValid)
            {
                //Check if user exsist and retun false
                if (_db.CheckIfUsersExists(model.Email))
                {
                    ModelState.AddModelError("UsersExsits", ValidationStrings.UserExistWithEmail);
                    return View(model);
                }

                Users _userToAdd = new Users();
                _userToAdd.RuleEnum = eRole.TrainingManager;
                _userToAdd.UserType = (int)eRole.TrainingManager;
                _userToAdd.CreationDate = DateTime.Now;
                _userToAdd.Firstname = model.FirstName;
                _userToAdd.Lastname = model.LastName;
                _userToAdd.IsActive = true;
                _userToAdd.Password = model.Password;
                _userToAdd.Phone = model.Phone;
                _userToAdd.Email = model.Email;
                _userToAdd.Region = (int)model.Region;
                _userToAdd.PermissionRoleID = model.PermissionRoleID;

                FlightMembershipProvider membership = new FlightMembershipProvider();

                _userToAdd = membership.CreateUser(_userToAdd);

                return RedirectToAction("GetUsers");
            }

            return View(model);
        }

        [HttpGet]
        public ActionResult EditTrainingManager(int ID)
        {
            Users _user = _dbDapper.GetUserByID(ID);

            TrainingManagerViewModel model = new TrainingManagerViewModel();
            model.Active = _user.IsActive;
            model.CreationDate = _user.CreationDate;
            model.Email = _user.Email;
            model.FirstName = _user.Firstname;
            model.LastName = _user.Lastname;
            model.Phone = _user.Phone;
            model.Role = _user.RuleEnum;
            model.UserID = _user.ID;
            model.PermissionRoleID = _user.PermissionRoleID;
            if (_user.Region != null)
            {
                model.Region = (eSchoolRegion)_user.Region;
            }

            model.LoadPermissionRoles();

            return View("EditTrainingManager", model);
        }

        [HttpPost]
        public ActionResult EditTrainingManager(TrainingManagerViewModel model)
        {
            ModelState.Remove("Email");
            ModelState.Remove("Password");
            if (ModelState.IsValid)
            {



                Users user = _db.Users.FirstOrDefault(u => u.ID.Equals(model.UserID));

                if (!String.IsNullOrEmpty(model.Email) && model.Email != user.Email)
                {
                    if (_db.CheckIfUsersExists(model.Email))
                    {
                        ModelState.AddModelError("UsersExsits", ValidationStrings.UserExistWithEmail);
                        return View(model);
                    }
                }

                user.Firstname = model.FirstName;
                user.Lastname = model.LastName;
                user.Password = model.Password;
                user.Phone = model.Phone;
                user.UserType = (int)model.Role;
                user.Region = (int)model.Region;
                user.Email = model.Email;
                user.Region = (int)model.Region;
                user.PermissionRoleID = model.PermissionRoleID;

                FlightMembershipProvider membership = new FlightMembershipProvider();

                user = membership.UpdateUser(user);

                return RedirectToAction("GetUsers");
            }

            return View(model);
        }


        [HttpGet]
        public ActionResult AddViewerUser()
        {
            ViewerViewModel model = new ViewerViewModel();
            model.LoadPermissionRoles();

            return View(model);
        }

        [HttpPost]
        public ActionResult AddViewerUser(ViewerViewModel model)
        {
            if (ModelState.IsValid)
            {
                //Check if user exsist and retun false
                if (_db.CheckIfUsersExists(model.Email))
                {
                    ModelState.AddModelError("UsersExsits", ValidationStrings.UserExistWithEmail);
                    return View(model);
                }

                Users _userToAdd = new Users();
                _userToAdd.RuleEnum = eRole.Viewer;
                _userToAdd.UserType = (int)eRole.Viewer;
                _userToAdd.CreationDate = DateTime.Now;
                _userToAdd.Firstname = model.FirstName;
                _userToAdd.Lastname = model.LastName;
                _userToAdd.IsActive = true;
                _userToAdd.Password = model.Password;
                _userToAdd.Phone = model.Phone;
                _userToAdd.Email = model.Email;
                _userToAdd.PermissionRoleID = model.PermissionRoleID;

                FlightMembershipProvider membership = new FlightMembershipProvider();

                _userToAdd = membership.CreateUser(_userToAdd);

                return RedirectToAction("GetUsers");
            }

            return View(model);
        }

        [HttpGet]
        public ActionResult EditUser(int ID)
        {
            //Users user = _db.Users.FirstOrDefault(u => u.ID.Equals(ID));
            Users user = _dbDapper.GetUserByID(ID);

            UserModel model = new UserModel();

            model.UserID = user.ID;
            model.CreationDate = user.CreationDate;
            model.Email = user.Email;
            model.FirstName = user.Firstname;
            model.LastName = user.Lastname;
            model.Phone = user.Phone;
            model.Role = user.RuleEnum;
            model.PermissionRoleID = user.PermissionRoleID;

            model.LoadPermissionRoles();

            return View(model);
        }

        [HttpPost]
        public ActionResult EditUser(UserModel model)
        {
            ModelState.Remove("Email");
            if (ModelState.IsValid)
            {

                Users user = _db.Users.FirstOrDefault(u => u.ID.Equals(model.UserID));

                if (!String.IsNullOrEmpty(model.Email) && model.Email != user.Email)
                {
                    if (_db.CheckIfUsersExists(model.Email))
                    {
                        ModelState.AddModelError("UsersExsits", ValidationStrings.UserExistWithEmail);
                        return View(model);
                    }
                }

                //user.Email = model.Email;
                user.Firstname = model.FirstName;
                user.Lastname = model.LastName;
                user.Password = model.Password;
                user.Phone = model.Phone;
                user.UserType = (int)model.Role;
                user.Email = model.Email;
                user.PermissionRoleID = model.PermissionRoleID;

                FlightMembershipProvider membership = new FlightMembershipProvider();
                membership.UpdateUser(user);

                return RedirectToAction("GetUsers");
            }

            return View(model);
        }

        [HttpGet]
        public ActionResult EditSchoolUser(int ID)
        {
            //Users user = _db.Users.FirstOrDefault(u => u.ID.Equals(ID));
            Users user = _dbDapper.GetUserByID(ID);

            UserModel model = new UserModel();

            model.UserID = user.ID;
            model.CreationDate = user.CreationDate;
            model.Email = user.Email;
            model.FirstName = user.Firstname;
            model.LastName = user.Lastname;
            model.Phone = user.Phone;
            model.Role = user.RuleEnum;
            model.PermissionRoleID = user.PermissionRoleID;

            model.LoadPermissionRoles();


            return View(model);
        }

        [HttpPost]
        public ActionResult EditSchoolUser(UserModel model)
        {
            ModelState.Remove("Email");
            if (ModelState.IsValid)
            {


                Users user = _db.Users.FirstOrDefault(u => u.ID.Equals(model.UserID));

                if (!String.IsNullOrEmpty(model.Email) && model.Email != user.Email)
                {
                    if (_db.CheckIfUsersExists(model.Email))
                    {
                        ModelState.AddModelError("UsersExsits", ValidationStrings.UserExistWithEmail);
                        return View(model);
                    }
                }

                //user.Email = model.Email;
                user.Firstname = model.FirstName;
                user.Lastname = model.LastName;
                user.Password = model.Password;
                user.Phone = model.Phone;
                user.UserType = (int)model.Role;
                user.Email = model.Email;
                user.PermissionRoleID = model.PermissionRoleID;

                FlightMembershipProvider membership = new FlightMembershipProvider();
                membership.UpdateUser(user);

                return RedirectToAction("GetUsers");
            }

            return View(model);
        }


        [HttpGet]
        public ActionResult AddAdministrator()
        {
            AdministratorViewModel model = new AdministratorViewModel();
            model.LoadPermissionRoles();

            return View(model);
        }

        [HttpPost]
        public ActionResult AddAdministrator(AdministratorViewModel model)
        {
            ModelState.Remove("Email");
            if (ModelState.IsValid)
            {
                //Check if user exsist and retun false
                if (_db.CheckIfUsersExists(model.Email))
                {
                    ModelState.AddModelError("UsersExsits", ValidationStrings.UserExistWithEmail);
                    return View(model);
                }

                Users _userToAdd = new Users();
                _userToAdd.RuleEnum = eRole.Administrator;
                _userToAdd.UserType = (int)eRole.Administrator;
                _userToAdd.CreationDate = DateTime.Now;
                _userToAdd.Firstname = model.FirstName;
                _userToAdd.Lastname = model.LastName;
                _userToAdd.IsActive = true;
                _userToAdd.Password = model.Password;
                _userToAdd.Phone = model.Phone;
                //_userToAdd.Email = model.Email;
                _userToAdd.PermissionRoleID = model.PermissionRoleID;

                FlightMembershipProvider membership = new FlightMembershipProvider();

                _userToAdd = membership.CreateUser(_userToAdd);

                return RedirectToAction("GetUsers");
            }

            return View(model);
        }

        [HttpPost]
        public ActionResult DeleteUser(UserViewModel user)
        {
            Users _user = _db.Users.FirstOrDefault(u => u.ID.Equals(user.ID));

            _user.IsActive = false;

            int results = _db.SaveChanges();

            return RedirectToAction("GetUsers");
        }

        [HttpGet]
        public PartialViewResult DeleteUserConfirm(int ID)
        {
            UserViewModel model = _db.Users.Where(u => u.ID.Equals(ID)).Select(u => new UserViewModel()
            {
                ID = u.ID,
                FirstName = u.Firstname,
                LastName = u.Lastname
            }).FirstOrDefault();

            return PartialView("_DeleteUserConfirm", model);
        }

        [HttpGet]
        public ActionResult AddCountyManager()
        {
            CountyManagerViewModel model = new CountyManagerViewModel();
            model.LoadPermissionRoles();

            return View("AddCountyManager", model);
        }

        [HttpPost]
        public ActionResult AddCountyManager(CountyManagerViewModel model)
        {
            if (ModelState.IsValid)
            {
                //Check if user exsist and retun false
                if (_db.CheckIfUsersExists(model.Email))
                {
                    ModelState.AddModelError("UsersExsits", ValidationStrings.UserExistWithEmail);
                    return View(model);
                }

                Users _userToAdd = new Users();
                _userToAdd.RuleEnum = eRole.CountyManager;
                _userToAdd.UserType = (int)eRole.CountyManager;
                _userToAdd.CreationDate = DateTime.Now;
                _userToAdd.Firstname = model.FirstName;
                _userToAdd.Lastname = model.LastName;
                _userToAdd.IsActive = true;
                _userToAdd.Password = model.Password;
                _userToAdd.Phone = model.Phone;
                _userToAdd.Email = model.Email;
                _userToAdd.Region = (int)model.Region;
                _userToAdd.PermissionRoleID = model.PermissionRoleID;

                FlightMembershipProvider membership = new FlightMembershipProvider();

                _userToAdd = membership.CreateUser(_userToAdd);

                return RedirectToAction("GetUsers");
            }

            return View(model);
        }


        [HttpGet]
        public ActionResult EditCountyManager(int ID)
        {
            Users _user = _dbDapper.GetUserByID(ID);

            CountyManagerViewModel model = new CountyManagerViewModel();
            model.Active = _user.IsActive;
            model.CreationDate = _user.CreationDate;
            model.Email = _user.Email;
            model.FirstName = _user.Firstname;
            model.LastName = _user.Lastname;
            model.Phone = _user.Phone;
            model.Role = _user.RuleEnum;
            model.UserID = _user.ID;
            model.Region = (eSchoolRegion)_user.Region;
            model.PermissionRoleID = _user.PermissionRoleID;

            model.LoadPermissionRoles();

            return View("EditCountyManager", model);
        }

        [HttpPost]
        public ActionResult EditCountyManager(CountyManagerViewModel model)
        {
            ModelState.Remove("Email");
            ModelState.Remove("Password");
            if (ModelState.IsValid)
            {



                Users user = _db.Users.FirstOrDefault(u => u.ID.Equals(model.UserID));

                if (!String.IsNullOrEmpty(model.Email) && model.Email != user.Email)
                {
                    if (_db.CheckIfUsersExists(model.Email))
                    {
                        ModelState.AddModelError("UsersExsits", ValidationStrings.UserExistWithEmail);
                        return View(model);
                    }
                }

                user.Firstname = model.FirstName;
                user.Lastname = model.LastName;
                user.Password = model.Password;
                user.Phone = model.Phone;
                user.UserType = (int)model.Role;
                user.Region = (int)model.Region;
                user.Email = model.Email;
                user.PermissionRoleID = model.PermissionRoleID;

                FlightMembershipProvider membership = new FlightMembershipProvider();

                user = membership.UpdateUser(user);

                return RedirectToAction("GetUsers");
            }

            return View(model);
        }


        [HttpGet]
        public ActionResult EditStaf(int ID)
        {
            //Users user = _db.Users.FirstOrDefault(u => u.ID.Equals(ID));
            Users user = _dbDapper.GetUserByID(ID);

            StafViewModel model = new StafViewModel();

            model.UserID = user.ID;
            model.CreationDate = user.CreationDate;
            model.Email = user.Email;
            model.FirstName = user.Firstname;
            model.LastName = user.Lastname;
            model.Phone = user.Phone;
            model.Role = user.RuleEnum;
            if (user.RoleTypeEnum != null)
            {
                model.RoleType = (eRoleType)user.RoleTypeEnum;
            }
            model.PermissionRoleID = user.PermissionRoleID;

            model.LoadPermissionRoles();


            return View(model);
        }

        [HttpGet]
        public ActionResult RestoreUser(int UserID)
        {
            bool results = _dbDapper.UpdateOperation(QueriesRepository.RestoreUser, new { UserID });

            return RedirectToAction("GetUsers");
        }

        [HttpPost]
        public ActionResult EditStaf(StafViewModel model)
        {
            ModelState.Remove("Email");
            if (ModelState.IsValid)
            {

                Users user = _db.Users.FirstOrDefault(u => u.ID.Equals(model.UserID));

                if (!String.IsNullOrEmpty(model.Email) && model.Email != user.Email)
                {
                    if (_db.CheckIfUsersExists(model.Email))
                    {
                        ModelState.AddModelError("UsersExsits", ValidationStrings.UserExistWithEmail);
                        return View(model);
                    }
                }

                //user.Email = model.Email;
                user.Firstname = model.FirstName;
                user.Lastname = model.LastName;
                user.Password = model.Password;
                user.Phone = model.Phone;
                user.UserType = (int)model.Role;
                user.RoleTypeEnum = model.RoleType;
                user.Email = model.Email;
                user.PermissionRoleID = model.PermissionRoleID;

                FlightMembershipProvider membership = new FlightMembershipProvider();
                membership.UpdateUser(user);

                return RedirectToAction("GetUsers");
            }

            return View(model);
        }

        [HttpPost]
        public PartialViewResult UpdateUserPermissionsRole(IEnumerable<int> Items)
        {
            UpdateUserPermissionRoleViewModel model = new UpdateUserPermissionRoleViewModel();
            model.SelectedUsers = Items.ToList();

            model.Roles.Items = _dbDapper.GetPermissionsRoles().Select(r => new CustomSelectListItem()
            {
                ID = r.ID,
                Name = r.Name
            }).ToList();

            return PartialView("_UpdatePermissionRole", model);
        }

        [HttpPost]
        public ActionResult UpdateUsersPermissionRole(UpdateUserPermissionRoleViewModel model)
        {

            if (ModelState.IsValid)
            {
                bool results = _dbDapper.UpdateOperation(QueriesRepository.UpdateUsersPermissionRole, new { model.RoleID, Users = model.SelectedUsers });
            }

            return RedirectToAction("GetUsers");
        }

        [HttpPost]
        public PartialViewResult ComposeGlobalNotification(IEnumerable<int> Items)
        {
            GlobalNotification model = new GlobalNotification();
            model.Recipients = _dbDapper.GetRecords<RecipentModel>(QueriesRepository.GetEmailsByUserIDs, new { Recipients = Items }).ToList();

            return PartialView("_GlobalNotification", model);
        }

        [HttpPost]
        public ActionResult SendGlobalNotification(GlobalNotification model)
        {
            if (ModelState.IsValid)
            {
                foreach (var _user in model.Recipients)
                {
                    Notifications _notification = new Notifications();
                    _notification.CreationDate = DateTime.Now;
                    _notification.EmailAddress = _user.Email;
                    _notification.IsRead = false;
                    _notification.MessageBody = model.Message;
                    _notification.MessageSubject = model.Subject;
                    _notification.NotificationStatus = eNotificationStatus.Pendding;
                    _notification.UserID = _user.ID;

                    bool _result = _dbDapper.ExecuteCommand(QueriesRepository.InsertGlobalNotification, new
                    {
                        _notification.UserID,
                        _notification.EmailAddress,
                        _notification.NotificationStatus,
                        _notification.MessageSubject,
                        _notification.MessageBody,
                        _notification.CreationDate,
                        _notification.IsRead
                    });
                }
            }
            return RedirectToAction("GetUsers");

        }

        [HttpPost]
        public PartialViewResult UpdateUsersAccessToSystem(IEnumerable<int> Items)
        {
            UpdateUsersAccessToSystem model = new UpdateUsersAccessToSystem();
            model.SelectedUsers = Items.ToList();

            return PartialView("_UpdateUserAccessToSystem", model);
        }

        [HttpPost]
        public ActionResult ChangeUsersAccessToSystem(UpdateUsersAccessToSystem model)
        {
            if (ModelState.IsValid)
            {
                bool results = _dbDapper.ExecuteCommand(QueriesRepository.UpdateUserAccessToSystem, new
                {
                    Access = model.SelectedAccess,
                    Users = model.SelectedUsers
                });
            }

            return RedirectToAction("GetUsers");
        }

    }
}