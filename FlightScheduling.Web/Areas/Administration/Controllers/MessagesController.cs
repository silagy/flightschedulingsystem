﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Services.Description;
using AutoMapper;
using FlightScheduling.DAL;
using FlightScheduling.DAL.CustomEntities;
using FlightScheduling.Language;
using FlightScheduling.Web.Areas.Administration.Models.Common;
using FlightScheduling.Web.Areas.Administration.Models.Messages;
using FlightScheduling.Website.Infrastructure;

namespace FlightScheduling.Web.Areas.Administration.Controllers
{
    [Authorize(Roles = "Administrator,Staf")]
    public class MessagesController : Controller
    {
        private FlightDB _db = new FlightDB();
        DBRepository _dbDapper = new DBRepository();

        protected override void Dispose(bool disposing)
        {
            _db.Dispose();
            _dbDapper.Dispose();
            base.Dispose(disposing);
        }

        [HttpGet]
        public ActionResult AddOrUodateMessage(int? ID, bool IsUpdate = false)
        {
            MessageViewModel model = new MessageViewModel();

            if (IsUpdate)
            {
                model = _dbDapper.GetRecords<MessageViewModel>(QueriesRepository.GetSystemMessage, new { ID }).FirstOrDefault();
                model.IsUpdate = true;
            }
            else
            {
                model.UserID = FunctionHelpers.User.UserID;
            }
            return View("UpdateMessage", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddOrUodateMessage(MessageViewModel model)
        {
            if (ModelState.IsValid)
            {
                model.Message = HttpUtility.HtmlEncode(model.Message);
                Messages _message = Mapper.Map<MessageViewModel, Messages>(model);

                if (model.IsUpdate)
                {
                    _dbDapper.ExecuteCommand(QueriesRepository.UpdateMessage, new { _message.Id, _message.Title, _message.Message, _message.UpdateDate, _message.ActiveStartDate, _message.ExpirationDate });
                }
                else
                {
                    int MessageID = _dbDapper.InsertMessage(_message);
                }


                return RedirectToAction("Messages");
            }

            return View("UpdateMessage", model);
        }

        public ActionResult Messages()
        {
            List<MessagesDetailsViewModel> model =
                Mapper.Map<List<MessagesDetails>, List<MessagesDetailsViewModel>>(_dbDapper.GetAllMassages().ToList());
            return View("Messages", model);

        }

        [HttpGet]
        public PartialViewResult DeleteMessage(int ID, string Title)
        {
            DeleteViewModel model = new DeleteViewModel();
            model.ID = ID;
            model.IsDeletable = true;
            model.Title = Title;
            model.Action = "DeleteMessage";
            model.Controller = "Messages";
            model.Header = AdministrationStrings.DeleteMessageConfirmation;

            return PartialView("_DeleteItem", model);

        }

        [HttpPost]
        public ActionResult DeleteMessage(DeleteViewModel model)
        {
            bool results = _dbDapper.UpdateOperation(QueriesRepository.DeleteMessageById, new { model.ID });

            return RedirectToAction("Messages");
        }
        
    }
}