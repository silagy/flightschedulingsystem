﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity.Core.Mapping;
using System.Dynamic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using FlightScheduling.DAL;
using FlightScheduling.DAL.CustomEntities.Groups;
using FlightScheduling.Language;
using FlightScheduling.Web.Areas.Administration.Models.Groups;
using FlightScheduling.Website.Infrastructure;
using Flight.DAL.FlightDB.Methods;
using System.IO;
using System.Text;
using CsvHelper;
using Endilo.ExportToExcel;
using FlightScheduling.Web.Areas.Administration.Models.Common;
using FlightScheduling.Web.Areas.Administration.Models.Instructors;
using FlightScheduling.Web.Areas.Administration.Models.Reports;
using FlightScheduling.Web.Infrastructure;
using LinqToExcel;
using Newtonsoft.Json;
using Otakim.Website.Infrastructure;
using FlightScheduling.Web.Infrastructure.Notifications;
using MvcContrib;
using FlightScheduling.DAL.CustomEntities.Notifications;
using FlightScheduling.Web.Areas.Administration.Models.Users;
using FlightScheduling.DAL.CustomEntities.Permissions;
using FlightScheduling.Web.Areas.Administration.Models.Trips;
using FlightScheduling.Web.App_Start;
using FlightScheduling.DAL.CustomEntities;

namespace FlightScheduling.Web.Areas.Administration.Controllers
{
    [Authorize]
    public class GroupsController : Controller
    {
        DBRepository _dbDapper = new DBRepository();

        [PermissionAuthorization(PermissionBitEnum.Expedition_Edit)]
        [HttpGet]
        public ActionResult CreateGroups(int ID, bool updateGroups = false)
        {
            GroupsCreationViewModel model = new GroupsCreationViewModel();
            model.Order = _dbDapper.GetOrderByID(ID);
            model.GroupColors = _dbDapper.GetGroupsColors();

            DateTime startYear = new DateTime(model.Order.Trips.DepartureDate.Year, 1, 1);
            DateTime endYear = new DateTime(model.Order.Trips.DepartureDate.Year, 12, 31);

            model.LastGroupID = _dbDapper.GetLastGroupExternalID(startYear, endYear);

            //internal number of buses
            int numberOfBuses = model.Order.BasesPerDay;

            // number of students per bus
            int numberOfStudentsPerBus = model.Order.NumberOFpassengers / model.Order.BasesPerDay;

            int groupNumberOfBuses = 0;

            //Check if Order has expedition
            model.Groups =
                    Mapper.Map<List<Group>, List<GroupViewModel>>(_dbDapper.GetGroupsByOrderID(model.Order.ID));

            if (model.Groups.Count() == 0)
            {
                while (numberOfBuses > 0)
                {
                    groupNumberOfBuses = 0;

                    if (numberOfBuses < 4)
                    {
                        groupNumberOfBuses = numberOfBuses;
                        numberOfBuses = 0;
                    }
                    else if (numberOfBuses == 5)
                    {
                        groupNumberOfBuses = 3;
                        numberOfBuses -= 3;
                    }
                    else
                    {
                        groupNumberOfBuses = 4;
                        numberOfBuses -= 4;
                    }

                    var group = new GroupViewModel();
                    group.GroupExternalID = model.LastGroupID + 1;
                    model.LastGroupID++;

                    group.OrderID = model.Order.ID;
                    group.NumberOfBuses = groupNumberOfBuses;
                    group.CreationDate = DateTime.Now;
                    group.UpdateDate = DateTime.Now;
                    group.GroupStatus = GroupStatusEnum.Approved;

                    group.NumberOfParticipants = numberOfStudentsPerBus * group.NumberOfBuses;

                    group.Color = FunctionHelpers.GetRandomColorID(model.GroupColors);
                    group.AgencyID = model.Order.AgencyID;

                    model.Groups.Add(group);

                }


                //Check if we need to add to the last bus additional students
                if (model.Groups.Sum(g => g.NumberOfParticipants) < model.Order.NumberOFpassengers)
                {
                    var group = model.Groups.Last();
                    group.NumberOfParticipants += model.Order.NumberOFpassengers -
                                                 model.Groups.Sum(g => g.NumberOfParticipants);

                    model.Groups[model.Groups.LastIndexOf(group)] = group;
                }

                model.UpdateGroup = false;
            }
            else
            {
                model.UpdateGroup = true;
            }

            return View("CreateGroups", model);
        }

        [PermissionAuthorization(PermissionBitEnum.Expedition_Edit)]
        [HttpPost]
        public ActionResult CreateGroups(GroupsCreationViewModel model)
        {
            bool modelvalidation = true;
            // Check of validation errors
            // 1. Check if number of buses is more then what approved on the order
            if (model.Groups.Sum(g => g.NumberOfBuses) != model.Order.BasesPerDay)
            {
                ModelState.AddModelError("NumberOfBusesValidation", GroupsStrings.NumberOfBusesValidation);
                modelvalidation = false;
            }

            // 2. Check if the total number of participants is qual to the total number of participants in the order
            if (model.Groups.Sum(g => g.NumberOfParticipants) != model.Order.NumberOFpassengers)
            {
                ModelState.AddModelError("NumberOfParticipantsValidation", GroupsStrings.NumberOfParticipantsValidation);
                modelvalidation = false;
            }

            // 3. Check if there expedition more then 50 participants per bus or less then 40
            // This check has been removed as Kfir's request
            //foreach (var item in model.Groups)
            //{
            //    if (item.NumberOfParticipants / item.NumberOfBuses < 40 || item.NumberOfParticipants / item.NumberOfBuses > 50)
            //    {
            //        ModelState.AddModelError("NumberOfParticipantsPerBusValidation", String.Format(GroupsStrings.NumberOfParticipantsPerBus, item.GroupExternalID));
            //        modelvalidation = false;
            //    }
            //}


            if (modelvalidation)
            {
                List<Group> groups = Mapper.Map<List<GroupViewModel>, List<Group>>(model.Groups);
                IEnumerable<Group> newGroups;
                //Assign AgencyID 
                for (int i = 0; i < groups.Count(); i++)
                {
                    groups[i].AgencyID = model.Order.AgencyID;
                }

                if (model.UpdateGroup)
                {
                    // Check if there are new groups in additioanl to the ones require update
                    newGroups = groups.Where(g => g.ID == 0);
                    var updateGroups = groups.Where(g => g.ID > 0);
                    _dbDapper.InsertMultipleGroups(newGroups);
                    _dbDapper.UpdateMultipleGroups(updateGroups);

                    foreach (var group in newGroups)
                    {
                        CreateGroupForm180Notification(group);
                    }
                }
                else
                {
                    _dbDapper.InsertMultipleGroups(groups);

                    foreach (var group in groups)
                    {
                        CreateGroupForm180Notification(group);
                    }
                }

                return RedirectToAction("GetGroupsDetails");
            }

            //Prepare the model again
            model.Order = _dbDapper.GetOrderByID(model.Order.ID);
            model.GroupColors = _dbDapper.GetGroupsColors();

            DateTime startYear = new DateTime(model.Order.Trips.DepartureDate.Year, 1, 1);
            DateTime endYear = new DateTime(model.Order.Trips.DepartureDate.Year, 12, 31);

            model.LastGroupID = _dbDapper.GetLastGroupExternalID(startYear, endYear);

            return View(model);
        }

        public void CreateGroupForm180Notification(Group group)
        {
            IEnumerable<NotificationForm180Creation> notificationsData = _dbDapper.GetRecords<NotificationForm180Creation>(QueriesRepository.GetGroup180FormNotificationDataCreation, new { GroupExternalID = group.GroupExternalID, group.OrderID });

            List<NotificationTemplate> _notificationTemplates = _dbDapper.GetRecords<NotificationTemplate>(QueriesRepository.GetNotificationTemplatesByTrrigerType, new { TriggerType = (int)eNotificationTriggerType.GroupForm180Creation });

            foreach (var _notificationData in notificationsData)
            {
                _notificationData.FormUrl = String.Format("{0}{1}", AppConfig.GetSiteRootUrl, Url.Action("GetGroup180Forms", "GroupForms", new { ID = _notificationData.GroupID }));

                foreach (var _notification in _notificationTemplates)
                {
                    List<UserViewModel> _UsersToSend = _dbDapper.GetRecords<UserViewModel>(QueriesRepository.GetUsersForBySchoolIDAndOrderID, new { _notificationData.SchoolID, group.OrderID });

                    foreach (var _user in _UsersToSend)
                    {
                        if (NotificationHelper.IsValidEmail(_user.Email))
                        {
                            Notifications _notificationToSend = new Notifications();
                            _notificationToSend.NotificationTempateID = _notification.ID;
                            _notificationToSend.MessageSubject = _notification.Subject;
                            _notificationToSend.UserID = _user.ID;
                            _notificationToSend.EmailAddress = _user.Email;
                            _notificationToSend.CallToAction = Url.Action("GetGroup180Forms", "GroupForms", new { ID = _notificationData.GroupID });
                            _notificationToSend.CreationDate = DateTime.Now;
                            _notificationToSend.IsRead = false;

                            //Complete the Custom Notification Details
                            _notificationData.UserID = _user.ID;
                            _notificationData.UserFirstName = _user.FirstName;
                            _notificationData.UserLastName = _user.LastName;

                            _notificationToSend.NotificationStatus = eNotificationStatus.Pendding;
                            _notificationToSend.MessageBody = _notificationData.ConvertProperties(_notification.Message);

                            // TODO: To insert all of the notifications at once. 
                            bool _result = _dbDapper.ExecuteCommand(QueriesRepository.InsertNotification, new { _notificationToSend.NotificationTempateID, _notificationToSend.UserID, _notificationToSend.EmailAddress, _notificationToSend.NotificationStatus, _notificationToSend.MessageSubject, _notificationToSend.MessageBody, _notificationToSend.CallToAction, _notificationToSend.CreationDate, _notificationToSend.IsRead });
                        }

                    }
                }




            }
        }

        public void CreateFileUploadNotification(GroupFiles file)
        {
            //Adding Notifications to be sent
            NotificationFile180CustomDetails notificationCustomDetails = new NotificationFile180CustomDetails();
            notificationCustomDetails.FileID = file.ID;
            notificationCustomDetails.FileName = file.Name;
            notificationCustomDetails.GroupID = file.GroupID;
            Groups group = _dbDapper.GetGroupByID(file.GroupID);
            notificationCustomDetails.GroupExternalID = group.GroupExternalID.ToString();
            notificationCustomDetails.GroupUrl = Url.Action("GroupFolderManagement", "Groups", new { ID = group.ID }, Request.Url.Scheme);
            notificationCustomDetails.FileUrl = Url.Action("DownloadFile", "Groups", new { FilePath = file.Url, FileName = file.Name }, Request.Url.Scheme);

            //Get the notification Templates
            List<NotificationTemplate> _notificationTemplates = _dbDapper.GetRecords<NotificationTemplate>(QueriesRepository.GetNotificationTemplatesByTrrigerType, new { TriggerType = (int)eNotificationTriggerType.NewGroupFile });

            foreach (var _notification in _notificationTemplates)
            {
                List<UserViewModel> _UsersToSend = _dbDapper.GetRecords<UserViewModel>(QueriesRepository.GetUsersByAllNotificationParameters, new { UserType = _notification.UserType, _notification.UserPermissionRole });

                foreach (var _user in _UsersToSend)
                {
                    if (NotificationHelper.IsValidEmail(_user.Email))
                    {
                        Notifications _notificationToSend = new Notifications();
                        _notificationToSend.NotificationTempateID = _notification.ID;
                        _notificationToSend.MessageSubject = _notification.Subject;
                        _notificationToSend.UserID = _user.ID;
                        _notificationToSend.EmailAddress = _user.Email;
                        _notificationToSend.CallToAction = Url.Action("GroupFolderManagement", "Groups", new { ID = group.ID });
                        _notificationToSend.CreationDate = DateTime.Now;
                        _notificationToSend.IsRead = false;

                        //Complete the Custom Notification Details
                        notificationCustomDetails.UserID = _user.ID;
                        notificationCustomDetails.UserFirstName = _user.FirstName;
                        notificationCustomDetails.UserLastName = _user.LastName;

                        _notificationToSend.NotificationStatus = eNotificationStatus.Pendding;
                        _notificationToSend.MessageBody = notificationCustomDetails.ConvertProperties(_notification.Message);

                        // TODO: To insert all of the notifications at once. 
                        bool _result = _dbDapper.ExecuteCommand(QueriesRepository.InsertNotification, new
                        {
                            _notificationToSend.NotificationTempateID,
                            _notificationToSend.UserID,
                            _notificationToSend.EmailAddress,
                            _notificationToSend.NotificationStatus,
                            _notificationToSend.MessageSubject,
                            _notificationToSend.MessageBody,
                            _notificationToSend.CallToAction,
                            _notificationToSend.CreationDate,
                            _notificationToSend.IsRead
                        });
                    }

                }

            }
        }

        [PermissionAuthorization(PermissionBitEnum.Expedition_View)]
        [HttpGet]
        public ActionResult GroupFolderManagement(int ID)
        {
            GroupFolderViewModel model = new GroupFolderViewModel();
            model.GroupID = ID;
            model.GroupFolderData.AssignedParticipants = _dbDapper.GetNumberOfAssignedParticipantsInGroup(model.GroupID);
            model.Files = _dbDapper.GetRecords<GroupFileViewModel>(QueriesRepository.GetGroupFilesByGroupID, new { model.GroupID, GroupFileTypes = model.MainFilesList }).ToList();
            model.OtherFiles = _dbDapper.GetRecords<GroupFileViewModel>(QueriesRepository.GetGroupFilesByGroupID, new { model.GroupID, GroupFileTypes = model.OtherFilesList }).ToList();
            model.Notes =
                Mapper.Map<List<GroupRemarks>, List<GroupNoteViewModel>>(_dbDapper.GetGroupRemarks(model.GroupID));

            model.Notes = model.Notes.OrderByDescending(n => n.UpdateDate).ToList();
            //Get folder data
            model.GroupFolderData.GroupFolderData = _dbDapper.GetGroupFolderDataByID(ID);

            model.Instructors.Instructors = _dbDapper.GetRecords<GroupInstructorsViewModelShortData>(QueriesRepository.GetGroupInstructorShortData, new { GroupID = ID });
            model.Instructors.GroupID = ID;

            // Create Instructor Roles for grouping all instructors by roles
            model.Instructors.Roles.Items = model.Instructors.Instructors.GroupBy(i => i.RoleID).Select(i => new CustomSelectListItem()
            {
                ID = i.Key,
                Name = i.FirstOrDefault().RoleEnum.GetEnumDescription()
            }).ToList();

            model.Schools = _dbDapper.GetRecords<GroupSchoolsViewModel>(QueriesRepository.GetGroupSubSchools, new { GroupID = ID });

            return View(model);
        }

        [PermissionAuthorization(PermissionBitEnum.Expedition_AddStaff)]
        [HttpPost]
        public ActionResult RegisterInstructor(GroupInstructorsViewModel model)
        {
            if (ModelState.IsValid)
            {
                //Need to check if instructor is not registered to the group already
                if (!_dbDapper.CheckInstructorExistByInstructorID(model.InstructorID))
                {
                    return RedirectToAction("GroupFolderManagement", new { ID = model.GroupID });
                }

                if (_dbDapper.CheckInstructorRegisteredToGroup(model.InstructorID, model.GroupID))
                {
                    return RedirectToAction("GroupFolderManagement", new { ID = model.GroupID });
                }


                Instructors instructor = _dbDapper.GetInstructorByInstructorID(model.InstructorID);

                bool results = _dbDapper.RegisterInstructorToGroup(model.GroupID, instructor.ID, instructor.RoleID);
            }

            return RedirectToAction("GroupFolderManagement", new { ID = model.GroupID });
        }

        [HttpGet]
        [PermissionAuthorization(PermissionBitEnum.Expedition_AddStaff)]
        public PartialViewResult AddorUpdateStafToGroup(int GroupID)
        {
            AddorUpdateStafToGroupViewModel model = new AddorUpdateStafToGroupViewModel();
            model.GroupID = GroupID;

            model.Schools.Items = _dbDapper.GetRecords<GroupSchoolsViewModel>(QueriesRepository.GetGroupSubSchools, new { GroupID }).Select(s => new CustomSelectListItem()
            {
                ID = s.SchoolID,
                Name = $"{s.InstituteID} - {s.SchoolName}"
            }).ToList();

            return PartialView("_AddStaf", model);
        }

        [HttpGet]
        [PermissionAuthorization(PermissionBitEnum.Expedition_AddStaff)]
        public PartialViewResult UpdateStafToGroup(int ID)
        {
            AddorUpdateStafToGroupViewModel model = new AddorUpdateStafToGroupViewModel();

            model = _dbDapper.GetRecords<AddorUpdateStafToGroupViewModel>(QueriesRepository.GetInstructorGroup, new { ID }).FirstOrDefault();
            model.IsUpdate = true;

            model.Schools.Items = _dbDapper.GetRecords<GroupSchoolsViewModel>(QueriesRepository.GetGroupSubSchools, new { model.GroupID }).Select(s => new CustomSelectListItem()
            {
                ID = s.SchoolID,
                Name = $"{s.InstituteID} - {s.SchoolName}"
            }).ToList();

            return PartialView("_AddStaf", model);
        }

        [HttpPost]
        [PermissionAuthorization(PermissionBitEnum.Expedition_AddStaff)]
        public ActionResult AddorUpdateStafToGroup(AddorUpdateStafToGroupViewModel model)
        {
            if (ModelState.IsValid)
            {

                if (!model.IsUpdate)
                {
                    //Need to check if instructor is not registered to the group already
                    if (!_dbDapper.CheckInstructorExistByInstructorID(model.InstructorExternalID))
                    {
                        return RedirectToAction("GroupFolderManagement", new { ID = model.GroupID });
                    }

                    if (_dbDapper.CheckInstructorRegisteredToGroup(model.InstructorExternalID, model.GroupID))
                    {
                        return RedirectToAction("GroupFolderManagement", new { ID = model.GroupID });
                    }


                    int RowID = _dbDapper.GetRecords<int>(QueriesRepository.AddStafToGroup, new
                    {
                        model.GroupID,
                        model.InstructorID,
                        model.RoleID,
                        model.Comments,
                        model.IsGroupManager,
                        model.IsDeputyDirector,
                        model.SchoolID
                    }).FirstOrDefault();
                }
                else
                {
                    bool results = _dbDapper.UpdateOperation(QueriesRepository.UpdateStafToGroup, new
                    {
                        model.RoleID,
                        model.Comments,
                        model.IsGroupManager,
                        model.IsDeputyDirector,
                        model.SchoolID,
                        model.ID
                    });
                }
            }

            return RedirectToAction("GroupFolderManagement", new { ID = model.GroupID });
        }

        [HttpGet]
        [PermissionAuthorization(PermissionBitEnum.Expedition_AddStaff)]
        public JsonResult SearchForStaf(string SearchTerm)
        {
            List<int> _instructors = new List<int>();
            _instructors.AddRange(AppConfig.SearchForAllStaf);
            List<AutoCompleteItem> items =
                _dbDapper.GetRecords<AutoCompleteItem>(QueriesRepository.SearchForManager, new { SearchTerm, RoleID = _instructors });

            return Json(JsonConvert.SerializeObject(items), JsonRequestBehavior.AllowGet);
        }

        [PermissionAuthorization(PermissionBitEnum.Expedition_DeleteStaff)]
        [HttpGet]
        public PartialViewResult UnassignInstructor(int ID, string title, int GroupID)
        {
            DeleteViewModel model = new DeleteViewModel();
            model.ID = ID;
            model.IsDeletable = true;
            model.Title = title;
            model.Action = "UnassignInstructor";
            model.Controller = "Groups";
            model.ReturnRefirectProperties = new List<int>();
            model.ReturnRefirectProperties.Add(GroupID);
            model.Header = GroupsStrings.UnAssignInstructor;

            return PartialView("_DeleteItem", model);
        }

        [PermissionAuthorization(PermissionBitEnum.Expedition_DeleteStaff)]
        [HttpPost]
        public ActionResult UnassignInstructor(DeleteViewModel model)
        {
            bool results = _dbDapper.ExecuteCommand(QueriesRepository.UnassignInstructorFromGroup, new { model.ID });

            return RedirectToAction("GroupFolderManagement", new { ID = model.ReturnRefirectProperties[0] });
        }

        [HttpGet]
        public JsonResult SearchForInstructor(string SearchTerm)
        {
            List<int> _menagers = new List<int>();
            _menagers.Add(2);
            _menagers.Add(5);
            List<AutoCompleteItem> items =
                _dbDapper.GetRecords<AutoCompleteItem>(QueriesRepository.SearchForInstructor, new { SearchTerm, RoleID = _menagers });

            return Json(JsonConvert.SerializeObject(items), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult CheckInstructorRegistration(string InstructorID, int GroupID)
        {
            dynamic json = new ExpandoObject();
            json.InstructorID = InstructorID;

            if (!_dbDapper.CheckInstructorExistByInstructorID(InstructorID))
            {
                json.Success = false;
                json.ErrorReason = InstructorsStrings.InstructorDoesntExist;
                json.AllowOverRide = false;
                return Json(JsonConvert.SerializeObject(json), JsonRequestBehavior.AllowGet);
            }

            if (_dbDapper.CheckInstructorRegisteredToGroup(InstructorID, GroupID))
            {
                json.Success = false;
                json.ErrorReason = InstructorsStrings.InstructorAlreadyAssigned;
                json.AllowOverRide = true;
                return Json(JsonConvert.SerializeObject(json), JsonRequestBehavior.AllowGet);
            }

            Trips trip = _dbDapper.GetTripByGroupID(GroupID);

            int? returnGroupID =
                _dbDapper.CheckIfInstructorIsInTrainingInTimeFrame(InstructorID, GroupID, trip.DepartureDate,
                    trip.ReturningDate);

            if (returnGroupID != null)
            {
                json.Success = false;
                json.ErrorReason = InstructorsStrings.InstrucorNotAvailable;
                json.AllowOverRide = true;
                return Json(JsonConvert.SerializeObject(json), JsonRequestBehavior.AllowGet);
            }

            if (!_dbDapper.CheckInstructorValidity(InstructorID))
            {
                json.Success = false;
                json.ErrorReason = InstructorsStrings.InstructorNotValidAlert;
                json.AllowOverRide = false;
                return Json(JsonConvert.SerializeObject(json), JsonRequestBehavior.AllowGet);
            }

            json.success = true;

            return Json(JsonConvert.SerializeObject(json), JsonRequestBehavior.AllowGet);
        }

        [PermissionAuthorization(PermissionBitEnum.Expedition_AddingFiles)]
        [HttpGet]
        public PartialViewResult AddFile(int ID)
        {
            GroupFileViewModel model = new GroupFileViewModel();
            model.GroupID = ID;
            model.UserID = FunctionHelpers.User.UserID;

            return PartialView("_GroupFolderAddFile", model);
        }

        [PermissionAuthorization(PermissionBitEnum.Expedition_UpdateFileStatus)]
        [HttpGet]
        public PartialViewResult UpdateFileStatus(int ID)
        {
            GroupFileViewModel model = Mapper.Map<GroupFiles, GroupFileViewModel>(_dbDapper.GetFileByID(ID));
            return PartialView("_UpdateFileStatus", model);
        }

        [PermissionAuthorization(PermissionBitEnum.Expedition_UpdateFileStatus)]
        [HttpPost]
        public ActionResult UpdateFileStatus(GroupFileViewModel model)
        {
            int resutls = 0;
            GroupFiles file = Mapper.Map<GroupFileViewModel, GroupFiles>(model);
            file.UserID = FunctionHelpers.User.UserID;
            if (User.IsInRole(eRole.CountyManager.ToString()))
            {
                resutls = _dbDapper.UpdateCountyManagerGroupFileApproval(model.CountyManagerApproval, model.CountyManagerComments, model.ID);
            }
            else
            {
                resutls = _dbDapper.UpdateGroupFileStatus(file);
            }

            return RedirectToAction("GroupFolderManagement", new { ID = model.GroupID });
        }

        [PermissionAuthorization(PermissionBitEnum.Expedition_AddingFiles)]
        [HttpPost]
        public ActionResult AddFile(GroupFileViewModel model)
        {
            if (ModelState.IsValid)
            {
                GroupFiles file = new GroupFiles();

                file.Name = model.Name;
                file.GroupID = model.GroupID;
                file.GroupFileStatusEnum = eGroupFileStatus.New;

                file.FileType = (int)eGroupFileType.Other;
                file.GroupFileTypeEnum = model.FileType;

                file.CreationDate = DateTime.Now;
                file.UserID = model.UserID;
                file.Comments = model.Comments;

                //Save file

                if (model.Attachment.HasFile())
                {
                    string filename = String.Format("{0}{1}{2}{3}", model.GroupID, model.FileType.ToString(), file.CreationDate.ToString("ddMMyyyyhhmm"), Path.GetExtension(model.Attachment.FileName));
                    model.Attachment.SaveAs(Server.MapPath(String.Format("~/App_Data/GroupsFiles/{0}", filename)));
                    file.Url = filename;

                    int fileID = _dbDapper.InsertGroupFile(file);

                    // SENT NOTIFICATION
                    if (fileID > 0)
                    {
                        file.ID = fileID;
                        CreateFileUploadNotification(file);
                    }
                }

            }

            return RedirectToAction("GroupFolderManagement", new { ID = model.GroupID });
        }

        [PermissionAuthorization(PermissionBitEnum.Expedition_ViewFiles)]
        [HttpGet]
        public FileResult DownloadFile(string FilePath, string FileName)
        {

            byte[] fileBytes = System.IO.File.ReadAllBytes(Path.Combine(AppConfig.GroupsFilesStorageRoot, FilePath));
            //FileContentResult response = new FileContentResult(fileBytes, MimeMapping.GetMimeMapping(FilePath));
            //response.FileDownloadName = FileName;
            //return response;
            return new FilePathResult(Path.Combine(AppConfig.GroupsFilesStorageRoot, FilePath), MimeMapping.GetMimeMapping(FilePath));
            //return File(fileBytes, MimeMapping.GetMimeMapping(FilePath), FileName);
        }

        [PermissionAuthorization(PermissionBitEnum.Expedition_ViewFiles)]
        [HttpGet]
        public FileResult DownloadFileTemplate(string FilePath, string FileName)
        {
            byte[] fileBytes = System.IO.File.ReadAllBytes(Path.Combine(AppConfig.GroupsImportTemplateRoot, FilePath));
            var response = new FileContentResult(fileBytes, "application/octet-stream");
            response.FileDownloadName = FileName;
            return response;
        }

        [PermissionAuthorization(PermissionBitEnum.Expedition_EditingFiles)]
        [HttpGet]
        public PartialViewResult DeleteFile(int ID, string fileName)
        {
            DeleteViewModel model = new DeleteViewModel();
            model.ID = ID;
            model.IsDeletable = true;
            model.Title = fileName;
            model.Action = "DeleteFile";
            model.Controller = "Groups";
            model.Header = GroupsStrings.DeleteFile;

            return PartialView("_DeleteItem", model);
        }

        [PermissionAuthorization(PermissionBitEnum.Expedition_EditingFiles)]
        [HttpPost]
        public ActionResult DeleteFile(DeleteViewModel model)
        {
            // Get the file info from the DB
            GroupFiles file = _dbDapper.GetGroupFileByID(model.ID);

            //Delete the file from the file server
            System.IO.File.Delete(Path.Combine(AppConfig.GroupsFilesStorageRoot, file.Url));

            bool operation = _dbDapper.DeleteGroupFile(model.ID);

            return RedirectToAction("GroupFolderManagement", new { ID = file.GroupID });
        }

        [PermissionAuthorization(PermissionBitEnum.Expedition_View)]
        public ActionResult GetGroupsDetails()
        {
            GroupsDetailsViewModel model = new GroupsDetailsViewModel();
            model.GroupsNewView = new List<GroupFolderCustomNewView>();
            bool runMainQuery = false;
            bool addGroupManagerExpeditions = false;

            if (PermissionLoader.CheckUserPermissions(PermissionBitEnum.Expedition_View))
            {
                runMainQuery = true;
            }

            if (FunctionHelpers.User.RoleID == (int)eRole.SchoolManager)
            {
                model.SchoolID = _dbDapper.GetSchoolIDByUserID(FunctionHelpers.User.UserID);
                model.IsPrimary = null;
                //model.ShowAdministrative = 2; // Hide administrative groups
            }

            if (FunctionHelpers.User.RegionID != null)
            {
                model.Region = FunctionHelpers.User.RegionID;
            }

            if (PermissionLoader.CheckUserPermissions(PermissionBitEnum.Form60_FormSubmit))
            {
                addGroupManagerExpeditions = true;
            }

            if (FunctionHelpers.User.AgencyID != null)
            {
                model.AgencyID = FunctionHelpers.User.AgencyID;
                //model.ShowAdministrative = 2; // Hide administrative groups
            }

            // Check if the logged on user is also a group manager

            if (model.NewLayout)
            {
                if (runMainQuery)
                {
                    model.GroupsNewView = _dbDapper.GetGroupDetailsNewView(QueriesRepository.GetGroupDetailsNew, model.StartDate, model.EndDate, model.OrderID, model.TripID, model.GroupStatusID, model.SchoolID, model.AgencyID, model.InstituteID, model.GroupID, model.Region, model.ShowAdministrative, model.IsPrimary, model.GroupIDs);
                }

                if (addGroupManagerExpeditions)
                {
                    int? groupManagerID = FunctionHelpers.User.InstructorID;

                    // This means that the actual user is not associated with the instructor
                    if (groupManagerID == null)
                    {

                        int resultID = _dbDapper.GetRecords<int>(QueriesRepository.GetInstructorIDByEmail, new { FunctionHelpers.User.Email }).FirstOrDefault();

                        if (resultID > 0)
                        {
                            groupManagerID = resultID;
                        }

                        if (groupManagerID != null)
                        {
                            bool updateoperation = _dbDapper.UpdateOperation(QueriesRepository.UpdateInstructorIDInUsers, new { ID = FunctionHelpers.User.UserID, InstructorID = groupManagerID });
                        }
                    }

                    if (groupManagerID != null)
                    {
                        model.GroupIDs = _dbDapper.GetRecords<int>(QueriesRepository.GetManagerGroupIDsByInstructorID, new { InstructorID = groupManagerID });

                        // Return only the GroupID that do not exists in the group details
                        model.GroupIDs = model.GroupIDs.Where(g => !model.GroupsNewView.Any(g2 => g2.GroupID == g));

                        model.GroupsNewView.AddRange(_dbDapper.GetGroupDetailsNewViewForGroupManager(QueriesRepository.GetGroupDetailsNewForGroupManager, model.StartDate, model.EndDate, model.ShowAdministrative, model.GroupIDs));
                    }
                }
            }
            else
            {
                model.Groups = _dbDapper.GetGroupDetails(FunctionHelpers.User.RoleID == (int)eRole.CountyManager ? QueriesRepository.SpGetGroupsDetailsCountyManager : QueriesRepository.SpGetGroupsDetails, model.StartDate, model.EndDate, model.OrderID, model.TripID, model.GroupStatusID, model.SchoolID, model.AgencyID, model.InstituteID, model.GroupID, model.Region);

            }

            return View(model.NewLayout ? "GetGroupsDetails" : "GetGroupsDetailsOldView", model);
        }

        [PermissionAuthorization(PermissionBitEnum.Expedition_View)]
        [HttpPost]
        public ActionResult GetGroupsDetails(GroupsDetailsViewModel model)
        {
            model.GroupsNewView = new List<GroupFolderCustomNewView>();
            bool runMainQuery = true;
            if (User.IsInRole(eRole.GroupManager.ToString()))
            {
                runMainQuery = false;
            }

            if (model.NewLayout)
            {
                if (runMainQuery)
                {
                    model.GroupsNewView = _dbDapper.GetGroupDetailsNewView(QueriesRepository.GetGroupDetailsNew, model.StartDate, model.EndDate, model.OrderID, model.TripID, model.GroupStatusID, model.SchoolID, model.AgencyID, model.InstituteID, model.GroupID, model.Region, model.ShowAdministrative, model.IsPrimary, model.GroupIDs);
                }

                if (FunctionHelpers.User.IsGroupManager)
                {
                    model.GroupIDs = _dbDapper.GetRecords<int>(QueriesRepository.GetManagerGroupIDsByInstructorID, new { FunctionHelpers.User.InstructorID });

                    // Return only the GroupID that do not exists in the group details
                    model.GroupIDs = model.GroupIDs.Where(g => !model.GroupsNewView.Any(g2 => g2.GroupID == g));

                    model.GroupsNewView.AddRange(_dbDapper.GetGroupDetailsNewViewForGroupManager(QueriesRepository.GetGroupDetailsNewForGroupManager, model.StartDate, model.EndDate, model.ShowAdministrative, model.GroupIDs));
                }
            }
            else
            {
                model.Groups = _dbDapper.GetGroupDetails(FunctionHelpers.User.RoleID == (int)eRole.CountyManager ? QueriesRepository.SpGetGroupsDetailsCountyManager : QueriesRepository.SpGetGroupsDetails, model.StartDate, model.EndDate, model.OrderID, model.TripID, model.GroupStatusID, model.SchoolID, model.AgencyID, model.InstituteID, model.GroupID, model.Region);

            }

            return View(model.NewLayout ? "GetGroupsDetails" : "GetGroupsDetailsOldView", model);
        }

        [PermissionAuthorization(PermissionBitEnum.Expedition_NotesEditing)]
        [HttpGet]
        public PartialViewResult AddNote(int ID)
        {
            GroupNoteViewModel model = new GroupNoteViewModel();
            model.GroupID = ID;
            model.UserID = FunctionHelpers.User.UserID;
            model.Schools.Items = _dbDapper.GetRecords<CustomSelectListItem>(QueriesRepository.GetSubSchoolsByGroupID, new { model.GroupID });

            return PartialView("_GroupAddNote", model);
        }

        [PermissionAuthorization(PermissionBitEnum.Expedition_NotesEditing)]
        [HttpPost]
        public ActionResult AddNote(GroupNoteViewModel model)
        {
            if (ModelState.IsValid)
            {
                GroupRemarks remark = Mapper.Map<GroupNoteViewModel, GroupRemarks>(model);
                remark.CreationDate = DateTime.Now;
                remark.UpdateDate = DateTime.Now;


                if (model.UpdateOperation)
                {
                    bool resultsOpr = _dbDapper.UpdateGroupNote(remark);
                }
                else
                {
                    int results = _dbDapper.InsertGroupRemark(remark);
                }



                return RedirectToAction("GroupFolderManagement", new { ID = model.GroupID });
            }

            return RedirectToAction("GroupFolderManagement", new { ID = model.GroupID });
        }

        [PermissionAuthorization(PermissionBitEnum.Expedition_NotesDeleting)]
        [HttpGet]
        public PartialViewResult DeleteNote(int ID, string Title)
        {
            DeleteViewModel model = new DeleteViewModel();
            model.ID = ID;
            model.IsDeletable = true;
            model.Title = Title;
            model.Action = "DeleteNote";
            model.Controller = "Groups";
            model.Header = GroupsStrings.DeleteNote;

            return PartialView("_DeleteItem", model);

        }

        [PermissionAuthorization(PermissionBitEnum.Expedition_NotesEditing)]
        [HttpGet]
        public PartialViewResult UpdateNote(int ID)
        {
            GroupNoteViewModel model = new GroupNoteViewModel();

            //model = Mapper.Map<GroupRemarks, GroupNoteViewModel>(_dbDapper.GetNoteByID(ID));
            model = _dbDapper.GetRecords<dynamic>(QueriesRepository.GetNoteByID, new { NoteID = ID }).Select(n => new GroupNoteViewModel()
            {
                ID = n.ID,
                GroupID = n.GroupID,
                Notes = n.Notes,
                UpdateDate = n.UpdateDate,
                CreationDate = n.CreationDate,
                UserID = n.UserID,
                UserFirstName = n.Firstname,
                UserLastName = n.Lastname,
                SchoolID = n.SchoolID,
                SchoolName = n.Name
            }).FirstOrDefault();
            model.UserID = FunctionHelpers.User.UserID;
            model.UpdateOperation = true;
            model.Schools.Items = _dbDapper.GetRecords<CustomSelectListItem>(QueriesRepository.GetSubSchoolsByGroupID, new { model.GroupID });


            return PartialView("_GroupAddNote", model);

        }

        [PermissionAuthorization(PermissionBitEnum.Expedition_NotesDeleting)]
        [HttpPost]
        public ActionResult DeleteNote(DeleteViewModel model)
        {
            GroupRemarks data = _dbDapper.GetNoteByID(model.ID);
            bool operation = _dbDapper.DeleteNote(data.ID);

            return RedirectToAction("GroupFolderManagement", new { ID = data.GroupID });
        }

        [PermissionAuthorization(PermissionBitEnum.Expedition_ViewParticipants)]
        [HttpGet]
        public ActionResult GetGroupParticipants(int ID)
        {
            GroupParticipantsViewModel model = new GroupParticipantsViewModel();
            model.GroupMainData.GroupFolderData = _dbDapper.GetGroupFolderDataByID(ID);
            model.GroupMainData.AssignedParticipants = _dbDapper.GetNumberOfAssignedParticipantsInGroup(ID);
            model.GroupID = ID;
            model.Participants =
                Mapper.Map<List<GroupParticipants>, List<GroupParticipantViewModel>>(_dbDapper.GetGroupParticipants(ID)).OrderBy(p => p.FullNameHe).ToList();

            return View("GroupParticipantList", model);
        }

        [PermissionAuthorization(PermissionBitEnum.Expedition_ViewParticipants)]
        [HttpGet]
        public ActionResult GetJoinedPaticipantList(int ID)
        {
            GroupJoinedPaticipantsViewModel model = new GroupJoinedPaticipantsViewModel();

            model.ParticipantList = _dbDapper.GetRecords<JoinedGroupParticipantViewModel>(QueriesRepository.GroupJoinedParticipantionList, new { ID });
            ExportToExcel<JoinedGroupParticipantViewModel> exc = new ExportToExcel<JoinedGroupParticipantViewModel>(model.ParticipantList, GroupsStrings.JoinedGroupParticipantList);
            var stream = exc.GetFile();
            string fileName = String.Format("{0} {1}.xlsx", GroupsStrings.JoinedGroupParticipantList, DateTime.Now.ToString("dd-MM-yyyy hh:ss"));
            string contentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";

            stream.Position = 0;
            return File(stream, contentType, fileName);
        }

        [PermissionAuthorization(PermissionBitEnum.Expedition_EditParticipants)]
        [HttpGet]
        public PartialViewResult AddParticipant(int ID)
        {
            GroupParticipantViewModel model = new GroupParticipantViewModel();
            model.GroupID = ID;

            model.Schools.Items = _dbDapper.GetRecords<GroupSchoolsViewModel>(QueriesRepository.GetGroupSubSchools, new { GroupID = ID }).Select(s => new CustomSelectListItem()
            {
                ID = s.InstituteID,
                Name = $"{s.InstituteID} - {s.SchoolName}"
            }).ToList();

            return PartialView("_GroupAddParticipant", model);
        }

        [PermissionAuthorization(PermissionBitEnum.Expedition_EditParticipants)]
        [HttpGet]
        public PartialViewResult UpdateParticipant(int ID)
        {
            GroupParticipantViewModel model = new GroupParticipantViewModel();
            model = Mapper.Map<GroupParticipants, GroupParticipantViewModel>(_dbDapper.GetGroupParticipant(ID));
            model.UpdateDate = DateTime.Today;
            model.UpdateOperation = true;
            model.Schools.Items = _dbDapper.GetRecords<GroupSchoolsViewModel>(QueriesRepository.GetGroupSubSchools, new { GroupID = model.GroupID }).Select(s => new CustomSelectListItem()
            {
                ID = s.InstituteID,
                Name = $"{s.InstituteID} - {s.SchoolName}"
            }).ToList();
            return PartialView("_GroupAddParticipant", model);
        }

        [PermissionAuthorization(PermissionBitEnum.Expedition_EditParticipants)]
        [HttpPost]
        public ActionResult AddParticipant(GroupParticipantViewModel model)
        {
            if (ModelState.IsValid)
            {
                GroupParticipants data = Mapper.Map<GroupParticipantViewModel, GroupParticipants>(model);
                data.CreationDate = DateTime.Now;
                data.UpdateDate = DateTime.Now;
                data.ParticipantStatusEnum = eParticipantStatus.Planned;
                // CHECK IF USER IS ALREADY EXISTS
                if (model.UpdateOperation)
                {
                    bool results = _dbDapper.UpdateOperation(QueriesRepository.UpdateGroupParticipant, new
                    {
                        data.Birthday,
                        data.Comments,
                        data.ContactNumber,
                        data.UpdateDate,
                        data.FirstNameEn,
                        data.FirstNameHe,
                        data.GroupID,
                        data.LastNameEn,
                        data.LastNameHe,
                        data.ParticipantID,
                        data.SEX,
                        data.ParticipantType,
                        data.PassportExperationDate,
                        data.PassportNumber,
                        data.Speciality,
                        data.Status,
                        data.ID,
                        data.UserID,
                        data.SchoolInstituteID,
                        data.ClassType
                    });
                }
                else
                {
                    int results = _dbDapper.InsertOperation(QueriesRepository.InsertNewGroupParticipant, new
                    {
                        data.Birthday,
                        data.Comments,
                        data.ContactNumber,
                        data.UpdateDate,
                        data.FirstNameEn,
                        data.FirstNameHe,
                        data.GroupID,
                        data.LastNameEn,
                        data.LastNameHe,
                        data.ParticipantID,
                        data.CreationDate,
                        data.SEX,
                        data.ParticipantType,
                        data.PassportExperationDate,
                        data.PassportNumber,
                        data.Speciality,
                        data.Status,
                        data.UserID,
                        data.SchoolInstituteID,
                        data.ClassType
                    });
                }

            }
            return RedirectToAction("GetGroupParticipants", new { ID = model.GroupID });
        }

        [PermissionAuthorization(PermissionBitEnum.Expedition_EditParticipants)]
        [HttpGet]
        public PartialViewResult UploadImportParticipants(int ID)
        {
            ImportParticipantsViewModel model = new ImportParticipantsViewModel();
            model.GroupID = ID;
            model.UserID = FunctionHelpers.User.UserID;


            return PartialView("_UploadImportParticipantFile", model);
        }

        [PermissionAuthorization(PermissionBitEnum.Expedition_EditParticipants)]
        [HttpPost]
        public ActionResult ImportParticipants(ImportParticipantsViewModel model)
        {
            if (ModelState.IsValid)
            {
                string filename = String.Format("{0}{1}", DateTime.Now.ToString("ddMMyyyyhhmm"),
                    Path.GetExtension(model.Attachment.FileName));
                model.Attachment.SaveAs(
                    Server.MapPath(String.Format("{0}/{1}", AppConfig.TempImportRelativePath, filename)));

                var excel = new ExcelQueryFactory(Path.Combine(AppConfig.TempImportRoot, filename));

                excel.AddTransformation<GroupParticipantViewModel>(x => x.PassportExperationDate, c =>
                {

                    DateTime ExperationDate;
                    if (DateTime.TryParse(c, out ExperationDate))
                    {
                        return DateTime.Parse(c);
                    }
                    if (DateTime.TryParseExact(c, AppConfig.DateTimeFormatParsing, System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out ExperationDate))
                    {
                        return DateTime.ParseExact(c, AppConfig.DateTimeFormatParsing, System.Globalization.CultureInfo.CurrentCulture, System.Globalization.DateTimeStyles.None);
                    }

                    return null;
                });

                excel.AddTransformation<GroupParticipantViewModel>(x => x.Birthday, c =>
                {
                    DateTime OutBirthDay;
                    if (DateTime.TryParse(c, out OutBirthDay))
                    {
                        return DateTime.Parse(c);
                    }
                    if (DateTime.TryParseExact(c, AppConfig.DateTimeFormatParsing, System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out OutBirthDay))
                    {
                        return DateTime.ParseExact(c, AppConfig.DateTimeFormatParsing, System.Globalization.CultureInfo.CurrentCulture, System.Globalization.DateTimeStyles.None);
                    }

                    return null;
                });

                excel.AddTransformation<GroupParticipantViewModel>(x => x.ParticipantTypeEnum, c =>
                {
                    var participantType = eParticipantType.Student;
                    switch (c.Trim())
                    {
                        case "תלמיד":
                            {
                                participantType = eParticipantType.Student;
                                break;
                            }
                        case "סייע":
                            {
                                participantType = eParticipantType.Teacher;
                                break;
                            }
                        case "הורה":
                            {
                                participantType = eParticipantType.Parent;
                                break;
                            }
                        case "אחר":
                            {
                                participantType = eParticipantType.Other;
                                break;
                            }
                        case "איש עדות":
                            {
                                participantType = eParticipantType.Testimony;
                                break;
                            }
                    }
                    return participantType;
                });



                excel.AddTransformation<GroupParticipantViewModel>(x => x.SEX, c =>
                {
                    var sex = eParticipantsSex.Female;
                    switch (c.Trim())
                    {
                        case "זכר":
                            {
                                sex = eParticipantsSex.Male;
                                break;
                            }
                        case "נקבה":
                            {
                                sex = eParticipantsSex.Female;
                                break;
                            }
                    }

                    return sex;
                });

                excel.AddTransformation<GroupParticipantViewModel>(x => x.ClassTypeEnum, c =>
                {
                    var classType = eParticipantClassType.None;
                    switch (c.Trim())
                    {
                        case "יא":
                            {
                                classType = eParticipantClassType.ElevenGrade;
                                break;
                            }
                        case "יב":
                            {
                                classType = eParticipantClassType.TwelveGrade;
                                break;
                            }
                    }

                    return classType;
                });




                var participants = from c in excel.WorksheetRange<GroupParticipantViewModel>("A1", "M1000", "Import")
                                   select c;


                List<GroupSchoolsViewModel> subSchools = _dbDapper.GetRecords<GroupSchoolsViewModel>(QueriesRepository.GetGroupSubSchools, new { GroupID = model.GroupID });
                List<GroupParticipantViewModel> participantsToImport = new List<GroupParticipantViewModel>();

                foreach (var item in participants)
                {
                    GroupParticipantViewModel participant = item;
                    participant.GroupID = model.GroupID;
                    participant.UserID = model.UserID;
                    participant.ParticipantStatusEnum = eParticipantStatus.Planned;
                    participant.CreationDate = DateTime.Now;
                    participant.UpdateDate = DateTime.Now;

                    //TODO: Add check if the Schools Institute ID exists
                    if (!String.IsNullOrEmpty(participant.ParticipantID) && subSchools.Any(s => s.InstituteID == participant.SchoolInstituteID))
                    {
                        participantsToImport.Add(participant);
                    }
                }

                _dbDapper.InsertMultipleParticipants(Mapper
                    .Map<List<GroupParticipantViewModel>, List<GroupParticipants>>(participantsToImport.ToList())
                    .AsEnumerable());

                System.IO.File.Delete(Path.Combine(AppConfig.TempImportRoot, filename));

            }
            return RedirectToAction("GetGroupParticipants", new { ID = model.GroupID });
        }

        [PermissionAuthorization(PermissionBitEnum.Expedition_DeleteParticipant)]
        [HttpGet]
        public PartialViewResult DeleteParticipant(int ID, string title)
        {
            DeleteViewModel model = new DeleteViewModel();
            model.ID = ID;
            model.IsDeletable = true;
            model.Title = title;
            model.Header = GroupsStrings.DeleteParticipant;
            model.Action = "DeleteParticipant";
            model.Controller = "Groups";

            return PartialView("_DeleteItem", model);
        }

        [PermissionAuthorization(PermissionBitEnum.Expedition_DeleteParticipant)]
        [HttpPost]
        public ActionResult DeleteParticipant(DeleteViewModel model)
        {
            GroupParticipants item = _dbDapper.GetGroupParticipant(model.ID);

            bool operation = _dbDapper.DeleteParticipant(item.ID);

            return RedirectToAction("GetGroupParticipants", new { ID = item.GroupID });
        }

        [PermissionAuthorization(PermissionBitEnum.Expedition_DeleteMultipleParticipants)]
        [HttpPost]
        public PartialViewResult DeleteParticipants(int ReturnID, IEnumerable<int> Items)
        {
            // TODO: Perform bulk delete of users
            int count = Items.Count();
            DeleteViewModel model = new DeleteViewModel();
            model.ID = 0;
            model.IsDeletable = true;
            model.Title = string.Format(GroupsStrings.DeleteMultipleParticipantsMessage, count);
            model.Header = GroupsStrings.DeleteParticipant;
            model.Action = "PerformDeleteParticipants";
            model.Controller = "Groups";

            model.BulkActionIDs = new List<int>();
            model.BulkActionIDs.AddRange(Items.ToList());

            model.ReturnRefirectProperties = new List<int>();
            model.ReturnRefirectProperties.Add(ReturnID);
            return PartialView("_DeleteItem", model);
        }

        [PermissionAuthorization(PermissionBitEnum.Expedition_DeleteMultipleParticipants)]
        [HttpPost]
        public ActionResult PerformDeleteParticipants(DeleteViewModel model)
        {
            if (ModelState.IsValid)
            {
                if (model.BulkActionIDs.Count() > 0)
                {
                    bool results = _dbDapper.UpdateOperation(QueriesRepository.DeleteMultipleParticipants, new { Items = model.BulkActionIDs.ToArray() });
                }
            }

            return RedirectToAction("GetGroupParticipants", new { ID = model.ReturnRefirectProperties[0] });
        }

        [PermissionAuthorization(PermissionBitEnum.Expedition_UpdateContact)]
        [HttpGet]
        public PartialViewResult UpdateGroupContactDetails(int ID)
        {
            GroupContactDetailsViewMode model = new GroupContactDetailsViewMode();
            model.GroupID = ID;

            return PartialView("_UpdateGroupContactDetails", model);
        }

        [PermissionAuthorization(PermissionBitEnum.Expedition_UpdateContact)]
        [HttpPost]
        public ActionResult UpdateGroupContactDetails(GroupContactDetailsViewMode model)
        {
            if (ModelState.IsValid)
            {
                bool result = _dbDapper.UpdateGroupContactDetails(model.GroupID, model.ContactName, model.ContactPhone);

            }

            return RedirectToAction("GroupFolderManagement", new { ID = model.GroupID });
        }

        [PermissionAuthorization(PermissionBitEnum.Expedition_Approval)]
        [HttpGet]
        public PartialViewResult UpdateGroupFinalStatus(int ID)
        {
            GroupFinalStatusViewMode model = new GroupFinalStatusViewMode();
            Groups group = _dbDapper.GetGroupByID(ID);

            model.GroupID = group.ID;
            model.FinalStatus = group.FinsalStatusEnum;

            return PartialView("_UpdateGroupFinalStatus", model);
        }

        [PermissionAuthorization(PermissionBitEnum.Expedition_Approval)]
        [HttpPost]
        public ActionResult UpdateGroupFinalStatus(GroupFinalStatusViewMode model)
        {
            if (ModelState.IsValid)
            {
                bool result = _dbDapper.UpdateGroupFinalStatus(model.GroupID, (int)model.FinalStatus);

            }

            return RedirectToAction("GroupFolderManagement", new { ID = model.GroupID });
        }

        [PermissionAuthorization(PermissionBitEnum.Expedition_ViewParticipants)]
        [HttpGet]
        public ActionResult DownloadGroupParticipants(int ID)
        {
            List<GroupParticipantViewModel> items = Mapper.Map<List<GroupParticipants>, List<GroupParticipantViewModel>>(_dbDapper.GetGroupParticipants(ID));
            ExportToExcel<GroupParticipantViewModel> exc = new ExportToExcel<GroupParticipantViewModel>(items, ReportsStrings.GroupParticipants);
            var stream = exc.GetFile();
            string fileName = String.Format("{0} {1}.xlsx", ReportsStrings.GroupParticipants, DateTime.Now.ToString("dd-MM-yyyy hh:ss"));
            string contentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";

            stream.Position = 0;
            return File(stream, contentType, fileName);
        }

        [PermissionAuthorization(PermissionBitEnum.Expedition_ViewStaff)]
        [HttpGet]
        public ActionResult DownloadGroupInstructors(int ID)
        {
            List<InstructorExcelViewMode> items = _dbDapper.GetGroupInstructors(ID)
                .Select(i => new InstructorExcelViewMode()
                {
                    InstructorID = i.Instructors.InstructorID,
                    FullNameHe = i.Instructors.FirstNameHE + ' ' + i.Instructors.LastNameHE,
                    FullNameEN = i.Instructors.FirstNameEN + ' ' + i.Instructors.LastNameEN,
                    InstructorRole = i.InstructorRoles.Name,
                    PassportID = i.Instructors.PassportID
                })
                .ToList();
            ExportToExcel<InstructorExcelViewMode> exc = new ExportToExcel<InstructorExcelViewMode>(items, ReportsStrings.GroupInstructors);
            var stream = exc.GetFile();
            string fileName = String.Format("{0} {1}.xlsx", ReportsStrings.GroupInstructors, DateTime.Now.ToString("dd-MM-yyyy hh:ss"));
            string contentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";

            stream.Position = 0;
            return File(stream, contentType, fileName);
        }

        [PermissionAuthorization(PermissionBitEnum.Expedition_Edit)]
        [HttpGet]
        public PartialViewResult UpdateExpeditionNumber(int ID)
        {

            GroupExternalIDViewMode model = new GroupExternalIDViewMode();

            var group = _dbDapper.GetGroupByID(ID);

            model.GroupID = group.ID;
            model.ExternalID = group.GroupExternalID;
            model.DepartureDate = _dbDapper.GetTripByGroupID(ID).DepartureDate;

            return PartialView("_UpdateGroupExternalID", model);
        }

        [PermissionAuthorization(PermissionBitEnum.Expedition_Edit)]
        [HttpPost]
        public ActionResult UpdateExpeditionNumber(GroupExternalIDViewMode model)
        {
            if (!_dbDapper.CheckGroupExternalIDExist(model.ExternalID, new DateTime(model.DepartureDate.Year, 1, 1), new DateTime(model.DepartureDate.Year, 12, 31)))
            {
                bool results = _dbDapper.UpdateGroupExternalID(model.ExternalID, model.GroupID);
            }

            return RedirectToAction("GroupFolderManagement", new { ID = model.GroupID });
        }

        [HttpGet]
        public JsonResult CheckGroupExternalIDExists(int GroupExternalID, DateTime DepartureDate)
        {
            dynamic json = new ExpandoObject();
            json.Exists = false;
            if (_dbDapper.CheckGroupExternalIDExist(GroupExternalID, new DateTime(DepartureDate.Year, 1, 1), new DateTime(DepartureDate.Year, 12, 31)))
            {
                json.Exists = true;
                json.ErrorMessage = GroupsStrings.GroupExternalIDExistMessage;
            }

            return Json(JsonConvert.SerializeObject(json), JsonRequestBehavior.AllowGet);
        }
        

        public ActionResult GetGroupBySchoolID(int ID)
        {
            IEnumerable<GroupViewModel> groups = _dbDapper.GetRecords<GroupViewModel>(QueriesRepository.GetGroupsBySchoolID, new { ID });

            return PartialView("_ObjectGroups", groups);
        }

        [PermissionAuthorization(PermissionBitEnum.Expedition_AddSubSchool)]
        [HttpGet]
        public PartialViewResult AddSubSchool(int GroupID)
        {
            GroupSubSchoolViewModel model = new GroupSubSchoolViewModel();
            model.GroupID = GroupID;

            model.Schools.Items = _dbDapper.GetSchools(null, null, null).Select(s => new CustomSelectListItem()
            {
                ID = s.ID,
                Name = $"{s.InstituteID} - {s.Name}"
            }).ToList();

            return PartialView("_AddSubSchool", model);
        }

        [PermissionAuthorization(PermissionBitEnum.Expedition_UpdateSubSchool)]
        [HttpGet]
        public PartialViewResult UpdateSubSchool(int ID)
        {
            GroupSubSchoolViewModel model = new GroupSubSchoolViewModel();
            model = _dbDapper.GetRecords<GroupSubSchoolViewModel>(QueriesRepository.GetSubSchoolByID, new { ID }).FirstOrDefault();
            model.IsUpdate = true;
            return PartialView("_AddSubSchool", model);

        }

        [PermissionAuthorization(PermissionBitEnum.Expedition_AddSubSchool)]
        [HttpPost]
        public ActionResult AddSubSchool(GroupSubSchoolViewModel model)
        {
            if (ModelState.IsValid)
            {
                if (model.IsUpdate)
                {
                    bool results = _dbDapper.UpdateOperation(QueriesRepository.UpdateSubSchool, new { model.NumberOfInstructors, model.ID });
                }
                else
                {
                    bool results = _dbDapper.AddSubSchool(model.GroupID, model.SchoolID, model.IsPrimary, model.NumberOfInstructors);
                }
            }

            return RedirectToAction("GroupFolderManagement", new { ID = model.GroupID });
        }

        [PermissionAuthorization(PermissionBitEnum.Expedition_DeleteSubSchool)]
        [HttpGet]
        public PartialViewResult DeleteSubSchool(int ID, string title, int GroupID, int SchoolID)
        {
            DeleteViewModel model = new DeleteViewModel();
            model.ID = ID;

            int? AllowedDeletion = _dbDapper.GetRecords<int>(QueriesRepository.CheckSubSchoolDeletionIsAllowed, new { SchoolID, GroupID }).FirstOrDefault();
            if (AllowedDeletion != null && AllowedDeletion != 0)
            {
                model.IsDeletable = false;
            }
            else
            {
                model.IsDeletable = true;
            }
            model.Title = title;
            model.Header = GroupsStrings.DeleteSubSchool;
            model.Action = "DeleteSubSchool";
            model.Controller = "Groups";
            model.ReturnRefirectProperties = new List<int>();
            model.ReturnRefirectProperties.Add(GroupID);

            return PartialView("_DeleteItem", model);
        }

        [PermissionAuthorization(PermissionBitEnum.Expedition_DeleteSubSchool)]
        [HttpPost]
        public ActionResult DeleteSubSchool(DeleteViewModel model)
        {

            int GroupFormID = _dbDapper.GetRecords<int>(QueriesRepository.GetGroupFormID, new { SubSchoolID = model.ID }).FirstOrDefault();

            bool deleteForms = _dbDapper.ExecuteCommand(QueriesRepository.DeleteGroupFormsBySchoolID, new { GroupFormID });
            bool results = _dbDapper.ExecuteCommand(QueriesRepository.DeleteSubSchool, new { model.ID });

            return RedirectToAction("GroupFolderManagement", new { ID = model.ReturnRefirectProperties[0] });
        }

        [PermissionAuthorization(PermissionBitEnum.Expedition_Delete)]
        [HttpGet]
        public PartialViewResult DeleteGroup(int ID, string title)
        {
            DeleteViewModel model = new DeleteViewModel();
            model.ID = ID;
            model.Title = title;
            model.IsDeletable = true;
            model.CustomMessage = CommonStrings.IReversableAction;
            model.Header = GroupsStrings.DeleteGroup;
            model.Action = "DeleteGroup";
            model.Controller = "Groups";
            model.ReturnRefirectProperties = new List<int>();
            return PartialView("_DeleteItem", model);
        }

        [PermissionAuthorization(PermissionBitEnum.Expedition_Delete)]
        [HttpPost]
        public ActionResult DeleteGroup(DeleteViewModel model)
        {
            // Get the file info from the DB
            //List<int> MainFilesList = new List<int>();
            //MainFilesList.Add((int)eGroupFileType.F180170);
            //MainFilesList.Add((int)eGroupFileType.F60);
            //MainFilesList.Add((int)eGroupFileType.FinalPDF);
            //MainFilesList.Add((int)eGroupFileType.Other);

            //var files = _dbDapper.GetGroupFilesBuGroupID(model.ID, MainFilesList);

            //var SubSchools = _dbDapper.GetRecords<GroupSchoolsViewModel>(QueriesRepository.GetGroupSubSchools, new { GroupID = model.ID });

            ////Load group forms
            //IEnumerable<int> GroupForms = _dbDapper.GetRecords<int>(QueriesRepository.GetGroupFormsIDs, new { GroupSchoolIDs = SubSchools.Select(a => a.ID).ToList() });

            ////Delete Groups Forms steps
            //bool deleteGroupFormsSetpsResults = _dbDapper.UpdateOperation(QueriesRepository.PermenentDeleteGroupFormsSteps, new { GroupForms = GroupForms.ToList() });

            ////Delete Group Forms
            //bool deletegroupforms = _dbDapper.UpdateOperation(QueriesRepository.PermenetDeleteGroupForms, new { GroupSchoolIDs = SubSchools.Select(a => a.ID).ToList() });

            ////Delete the group
            //bool results = _dbDapper.PermanentDeleteGroup(model.ID);

            //if (results)
            //{
            //    foreach (var file in files)
            //    {
            //        System.IO.File.Delete(Path.Combine(AppConfig.GroupsFilesStorageRoot, file.Url));
            //    }
            //}
            //Delete the file from the file server

            int maxGroupExternalID = _dbDapper.GetRecords<int>(QueriesRepository.GetMaxGroupIDForInactive, null).FirstOrDefault();

            if (maxGroupExternalID < 1500)
            {
                maxGroupExternalID = 1500;
            }

            bool updateGroupExtenalID = _dbDapper.UpdateGroupExternalID(maxGroupExternalID, model.ID);
            bool results = _dbDapper.UpdateOperation(QueriesRepository.SetGroupInActive, new { GroupID = model.ID });

            return RedirectToAction("GetGroupsDetails");
        }

        [PermissionAuthorization(PermissionBitEnum.ExpeditionDashbard_View)]
        [HttpGet]
        public ActionResult GetDailyDashboard(DateTime? CurrentTime)
        {
            DailyDashboardViewModel model = new DailyDashboardViewModel();

            model.StartDate = CurrentTime != null ? (DateTime)CurrentTime : DateTime.Now;


            model.GroupsRawData = _dbDapper.GetRecords<DailyDashboardRawData>(QueriesRepository.GetDailyDashboardSummary, new { StartTime = model.StartDate }).ToList();

            model.NumberOfBuses = model.GroupsRawData.Sum(g => g.NumberOfBuses);
            model.NumberOfGroups = model.GroupsRawData.Count();
            model.NumberOfParticipants = model.GroupsRawData.Sum(g => g.NumberOfParticipants);

            model.GroupsData = _dbDapper.GetRecords<DailyDashboardGroupsData>(QueriesRepository.GetdailyDashboardGroupsData, new { StartTime = model.StartDate }).ToList();

            return View("GetDailyDashboard", model);
        }

        [PermissionAuthorization(PermissionBitEnum.Expedition_UpdateAgency)]
        [HttpGet]
        public PartialViewResult UpdateGroupAgency(int ID)
        {

            UpdateGroupAgencyViewModel model = new UpdateGroupAgencyViewModel();

            model.GroupID = ID;
            model.AgencyID = _dbDapper.GetRecords<int>(QueriesRepository.GetGroupAgencyID, new { ID }).FirstOrDefault();
            model.Agencies.Items = _dbDapper.GetRecords<CustomSelectListItem>(QueriesRepository.GetAllAgencies, null).Select(a => new CustomSelectListItem()
            {
                ID = a.ID,
                Name = a.Name
            }).ToList();

            return PartialView("_UpdateGroupAgencyID", model);
        }

        [PermissionAuthorization(PermissionBitEnum.Expedition_UpdateAgency)]
        [HttpPost]
        public ActionResult UpdateGroupAgency(UpdateGroupAgencyViewModel model)
        {
            if (ModelState.IsValid)
            {
                bool results = _dbDapper.UpdateGroupAgencyID(model.AgencyID, model.GroupID);
            }

            return RedirectToAction("GroupFolderManagement", new { ID = model.GroupID });
        }


        #region Move Group

        [PermissionAuthorization(PermissionBitEnum.Expedition_MoveGroup)]
        [HttpGet]
        public ActionResult MoveOrderSelectTripStep(int GroupID)
        {
            var model = new MoveGroupSelectTrip();

            model.GroupID = GroupID;

            dynamic result = _dbDapper.GetRecords<dynamic>(QueriesRepository.GetOrderandSchoolIDFromGroupID, new { GroupID }).SingleOrDefault();

            if (result != null)
            {
                model.SchoolID = result.SchoolID;
                model.OrderID = result.OrderID;
            }

            model.Orders = _dbDapper.GetRecords<TripandOrderViewModel>(QueriesRepository.GetAvailableTripsForMovingOrder, new
            {
                model.SchoolID,
                model.OrderID
            });

            model.OriginalOrder = _dbDapper.GetRecords<OrderDetailsViewModel>(QueriesRepository.GetOriginalOrderDetails, new { model.OrderID }).FirstOrDefault();
            model.OriginalTrip = _dbDapper.GetRecords<TripDetailsViewModel>(QueriesRepository.GetOriginalTripDetails, new {TripID = model.OriginalOrder.TripID}).FirstOrDefault();


            return View("MoveOrderSelectTrip", model);
        }


        [PermissionAuthorization(PermissionBitEnum.Expedition_MoveGroup)]
        [HttpGet]
        public ActionResult MoveGroupSelectOrder(int OriginalOrderID, int OriginalGroupID, int SelectedOrderID)
        {
            // Cancel the original Order and release the buses and paggengers

            // Move the group to the new order

            MoveGroupFinalStepViewModel model = new MoveGroupFinalStepViewModel();
            model.OriginalGroupID = OriginalGroupID;
            model.OriginalGroupID = OriginalGroupID;
            model.SelectedOrderID = SelectedOrderID;

            model.transactionResult = _dbDapper.MoveGroupToAnotherOrder(OriginalGroupID, OriginalOrderID, SelectedOrderID);

            return View("MoveGroupFinalResult", model);
        }
        #endregion


    }
}
