﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Endilo.GenericSearch;
using Flight.DAL.FlightDB.Methods;
using FlightScheduling.DAL;
using FlightScheduling.Web.Areas.Administration.Models.Common;
using FlightScheduling.Web.Areas.Administration.Models.Flights;
using FlightScheduling.Web.Areas.Administration.Models.Trips;
using Otakim.Website.Infrastructure;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using FlightScheduling.Web.Models.Expeditions;
using FlightScheduling.Web.Infrastructure;
using Endilo.Helpers;
using System.IO;
using LinqToExcel;
using AutoMapper;
using FlightScheduling.DAL.CustomEntities.Permissions;

namespace FlightScheduling.Web.Areas.Administration.Controllers
{
    [PermissionAuthorization(PermissionBitEnum.Trip_View)]
    public class TripsController : Controller
    {
        private FlightDB _db = new FlightDB();
        DBRepository _dbDapper = new DBRepository();

        protected override void Dispose(bool disposing)
        {
            _db.Dispose();
            base.Dispose(disposing);
        }

        public ActionResult GetTrips()
        {
            TripsViewModel model = new TripsViewModel();

            if (Session["TripFilterFromDate"] != null)
            {
                model.FilterFromDate = (DateTime)Session["TripFilterFromDate"];
            }
            else
            {
                model.FilterFromDate = SystemConfigurationRepository.TripsFilterStartDate;
            }

            if (Session["TripFilterToDate"] != null)
            {
                model.FilterToDate = (DateTime)Session["TripFilterToDate"];
            }
            else
            {
                model.FilterToDate = SystemConfigurationRepository.TripsFilterEndDate;
            }




            model.TripViewModelBasics = _db.Trips.Include(t => t.Expeditions).RemoveInActive().Where(t => t.DepartureDate >= DateTime.Now && t.DepartureDate <= DateTime.Now.AddYears(1)).Select(t => new TripViewModelBasic()
            {
                ID = t.ID,
                DepartureDate = t.DepartureDate,
                UpdatenDate = t.UpdateDate,
                ReturnDate = t.ReturningDate,
                Destenation = (FlightViewModel.FlightsFields)t.DestinationID,
                ReturnedFrom = (FlightViewModel.FlightsFields)t.ReturingField,
                Comments = t.Comments,
                CreationDate = t.CreationDate,
                NumberOfExpeditions = t.Expeditions.Count(e => e.Status.Equals((int)eExpeditionStatus.PendingApproval))
            }).ToList();

            return View(model);
        }

        [HttpPost]
        public ActionResult GetTrips(DateTime? FilterFromDate, DateTime? FilterToDate)
        {
            TripsViewModel model = new TripsViewModel();


            if (FilterFromDate == null && Session["TripFilterFromDate"] == null)
            {
                FilterFromDate = SystemConfigurationRepository.TripsFilterStartDate;
            }
            else if (Session["TripFilterFromDate"] != null && FilterFromDate == null)
            {
                FilterFromDate = (DateTime)Session["TripFilterFromDate"];
            }

            if (FilterToDate == null && Session["TripFilterToDate"] == null)
            {
                FilterToDate = SystemConfigurationRepository.TripsFilterEndDate;
            }
            else if (Session["TripFilterToDate"] != null && FilterToDate == null)
            {
                FilterToDate = (DateTime)Session["TripFilterToDate"];
            }

            //Set Filters session
            Session["TripFilterFromDate"] = FilterFromDate;
            Session["TripFilterToDate"] = FilterToDate;

            model.TripViewModelBasics = _db.Trips.Include(t => t.Expeditions).RemoveInActive().Where(t => t.DepartureDate >= FilterFromDate && t.DepartureDate <= FilterToDate).Select(t => new TripViewModelBasic()
            {
                ID = t.ID,
                DepartureDate = t.DepartureDate,
                UpdatenDate = t.UpdateDate,
                ReturnDate = t.ReturningDate,
                Destenation = (FlightViewModel.FlightsFields)t.DestinationID,
                ReturnedFrom = (FlightViewModel.FlightsFields)t.ReturingField,
                Comments = t.Comments,
                CreationDate = t.CreationDate,
                NumberOfExpeditions = t.Expeditions.Count(e => e.Status.Equals((int)eExpeditionStatus.PendingApproval) && e.Status.Equals((int)eExpeditionStatus.InProcess))
            }).ToList();

            return View(model);
        }

        [PermissionAuthorization(PermissionBitEnum.Trip_Edit)]
        [HttpGet]
        public ActionResult AddTrip()
        {
            TripViewModelBasic model = new TripViewModelBasic();

            if (Request.IsAjaxRequest())
            {
                return PartialView("_AddTrip", model);
            }
            return View(model);
        }

        [PermissionAuthorization(PermissionBitEnum.Trip_Edit)]
        [HttpPost]
        public ActionResult AddTrip(TripViewModelBasic model)
        {
            if (ModelState.IsValid)
            {
                Trips _trip = new Trips();
                _trip.CreationDate = DateTime.Now;
                _trip.UpdateDate = DateTime.Now;
                _trip.DepartureDate = (DateTime)model.DepartureDate;
                _trip.DestinationID = (int)model.Destenation;
                _trip.ReturingField = (int)model.ReturnedFrom;
                _trip.ReturningDate = model.ReturnDate;
                _trip.Comments = model.Comments;
                _trip.IsActive = true;

                _db.Trips.Add(_trip);

                int results = _db.SaveChanges();

                return RedirectToAction("GetTrips");
            }

            return View(model);
        }

        [PermissionAuthorization(PermissionBitEnum.Trip_Edit)]
        [HttpGet]
        public ActionResult EditTrip(int ID)
        {
            Trips trip = _db.Trips.FirstOrDefault(t => t.ID.Equals(ID));
            TripViewModelBasic model = new TripViewModelBasic();
            model.ID = trip.ID;
            model.IsActive = trip.IsActive;
            model.Comments = trip.Comments;
            model.DepartureDate = trip.DepartureDate;
            model.Destenation = (FlightViewModel.FlightsFields)trip.DestinationID;
            model.ReturnDate = trip.ReturningDate;
            model.ReturnedFrom = (FlightViewModel.FlightsFields)trip.ReturingField;
            model.UpdatenDate = trip.UpdateDate;


            return View(model);
        }

        [PermissionAuthorization(PermissionBitEnum.Trip_Edit)]
        [HttpPost]
        public ActionResult EditTrip(TripViewModelBasic model)
        {
            if (ModelState.IsValid)
            {

                Trips trip = _db.Trips.FirstOrDefault(t => t.ID.Equals(model.ID));

                trip.Comments = model.Comments;
                trip.DepartureDate = (DateTime)model.DepartureDate;
                trip.DestinationID = (int)model.Destenation;
                trip.ReturingField = (int)model.ReturnedFrom;
                trip.ReturningDate = model.ReturnDate;
                trip.UpdateDate = DateTime.Now;

                int results = _db.SaveChanges();

                return RedirectToAction("GetTrips");
            }

            return View(model);
        }

        [PermissionAuthorization(PermissionBitEnum.Trip_Edit)]
        [HttpGet]
        public ActionResult TripManagement(int ID)
        {
            TripManagementViewModel model = new TripManagementViewModel();

            Trips trip =
                _db.Trips.Include(t => t.Expeditions).Include(t => t.Flights).Include(t => t.Flights).Include(t => t.Flights.Select(f => f.ExpeditionsFlights)).FirstOrDefault(t => t.ID.Equals(ID));

            //Load Basic trip information
            model.TripViewModelBasic = new TripViewModelBasic()
            {
                ID = trip.ID,
                DepartureDate = trip.DepartureDate,
                UpdatenDate = trip.UpdateDate,
                ReturnDate = trip.ReturningDate,
                Destenation = (FlightViewModel.FlightsFields)trip.DestinationID,
                ReturnedFrom = (FlightViewModel.FlightsFields)trip.ReturingField,
                Comments = trip.Comments,
                CreationDate = trip.CreationDate
            };




            // Loading Summary information
            model.TripSummaryDetailsViewModel.NumOfBuses =
                trip.Expeditions.Where(e => e.TripID.Equals(ID) && e.IsActive && (e.Status.Equals((int)eExpeditionStatus.Approved) || e.Status.Equals((int)eExpeditionStatus.InProcess))).Sum(e => e.BasesPerDay);

            model.TripSummaryDetailsViewModel.NumberOfOutGoingFlights =
                trip.Flights.Count(f => f.IsActive && f.TakeOffFromIsrael == true);

            model.TripSummaryDetailsViewModel.NumberOfInComingFlights =
                trip.Flights.Count(f => f.IsActive && f.DestinationID.Equals((int)FlightViewModel.FlightsFields.Israel));

            model.TripSummaryDetailsViewModel.NumOfExpeditions =
                trip.Expeditions.Count(e => e.IsActive && e.TripID.Equals(ID) && (e.Status.Equals((int)eExpeditionStatus.Approved) || e.Status.Equals((int)eExpeditionStatus.InProcess)));

            //Load expedition Flights


            // Load out going flights
            model.TripOutGoingFlightsViewModel.FlightViewModels =
                trip.Flights.Where(f => f.IsActive && f.TakeOffFromIsrael == true).Select(f => new FlightManagementViewModel()
                {
                    ID = f.ID,
                    AirlineName = f.AirlineName,
                    CreationDate = f.CreationDate,
                    UpdatenDate = f.UpdateDate,
                    TakeOffFromIsrael = (bool)f.TakeOffFromIsrael,
                    DepartureDate = f.StartDate,
                    Comments = f.Comments,
                    DepartureField = (FlightViewModel.FlightsFields)f.DepartureFieldID,
                    DestenationField = (FlightViewModel.FlightsFields)f.DestinationID,
                    FlightNumber = f.FlightNumber,
                    NumberOfPassengers = f.NumberOfPassengers,
                    RemainingSeats = f.ExpeditionsFlights.Count() != 0 ? f.NumberOfPassengers - f.ExpeditionsFlights.Sum(e => e.NumberOfPassengers) : f.NumberOfPassengers
                }).ToList();
            model.TripOutGoingFlightsViewModel.TripID = ID;

            //Load expeditions

            model.Expeditions = trip.Expeditions.Where(e => e.IsActive == true).Select(e => new ExpeditionsViewModel()
            {
                ID = e.ID,
                BusesRequiredPerDay = e.BasesPerDay,
                Comments = e.Comments,
                ExpeditionManager = e.Manager,
                ExpeditionManagerCell = e.ManagerCell,
                ExpeditionManagerEmail = e.ManagerEmail,
                ExpeditionManagerPhone = e.ManagerPhone,
                NumOfPassangersRequired = e.NumberOFpassengers,
                School = e.Schools,
                Status = (eExpeditionStatus)e.Status,
                TripID = e.TripID,
                RegisteredPassengers = e.ExpeditionsFlights.Count() != 0 ? e.ExpeditionsFlights.Where(f => f.Flights.DepartureFieldID.Equals((int)(FlightViewModel.FlightsFields.Israel))).Sum(f => f.NumberOfPassengers) : 0,
                AgentStatus = e.AgencyID == 6 ? eAgentExpeditionStatus.NotAssigned : e.AgentApproval == true ? eAgentExpeditionStatus.Approved : eAgentExpeditionStatus.Assigned,
                CreationDate = e.CreationDate
            }).OrderByDescending(t => t.ID).ToList();

            //Calculating remaining seats 


            // Load incoming Flights
            model.TripInComingFlightsViewModel.FlightViewModels =
               trip.Flights.Where(f => f.IsActive && f.DestinationID.Equals((int)FlightViewModel.FlightsFields.Israel)).Select(f => new FlightManagementViewModel()
               {
                   ID = f.ID,
                   AirlineName = f.AirlineName,
                   CreationDate = f.CreationDate,
                   UpdatenDate = f.UpdateDate,
                   TakeOffFromIsrael = (bool)f.TakeOffFromIsrael,
                   DepartureDate = f.StartDate,
                   Comments = f.Comments,
                   DepartureField = (FlightViewModel.FlightsFields)f.DepartureFieldID,
                   DestenationField = (FlightViewModel.FlightsFields)f.DestinationID,
                   FlightNumber = f.FlightNumber,
                   NumberOfPassengers = f.NumberOfPassengers,
                   RemainingSeats = f.ExpeditionsFlights.Count() != 0 ? f.NumberOfPassengers - f.ExpeditionsFlights.Sum(e => e.NumberOfPassengers) : f.NumberOfPassengers
               }).ToList();
            model.TripInComingFlightsViewModel.TripID = ID;

            return View(model);
        }

        [PermissionAuthorization(PermissionBitEnum.Trip_Edit)]
        [HttpGet]
        public PartialViewResult LinkReturningFlights(int TripID)
        {
            LinkFlightSelectionViewModel model = new LinkFlightSelectionViewModel();

            model.TripID = TripID;
            model.DepartureFlight = false; // Returning Flight

            Trips trip = _db.Trips.FirstOrDefault(t => t.ID.Equals(TripID));

            model.FlightViewModels =
                _db.Flights.Where(f => DbFunctions.TruncateTime(f.StartDate) == trip.ReturningDate.Date && !f.Trips.Any(ft => ft.ID.Equals(TripID)) && f.DestinationID.Equals((int)FlightViewModel.FlightsFields.Israel) && f.DepartureFieldID.Equals((int)trip.ReturingField)).Select(f => new FlightManagementViewModel()
                {
                    ID = f.ID,
                    AirlineName = f.AirlineName,
                    CreationDate = f.CreationDate,
                    UpdatenDate = f.UpdateDate,
                    TakeOffFromIsrael = (bool)f.TakeOffFromIsrael,
                    DepartureDate = f.StartDate,
                    Comments = f.Comments,
                    DepartureField = (FlightViewModel.FlightsFields)f.DepartureFieldID,
                    DestenationField = (FlightViewModel.FlightsFields)f.DestinationID,
                    FlightNumber = f.FlightNumber,
                    NumberOfPassengers = f.NumberOfPassengers,
                    RemainingSeats = f.ExpeditionsFlights.Count() != 0 ? f.ExpeditionsFlights.Sum(e => e.NumberOfPassengers) : f.NumberOfPassengers
                }).ToList();

            return PartialView("_LinkReturningFlights", model);
        }


        [PermissionAuthorization(PermissionBitEnum.Trip_Edit)]
        [HttpGet]
        public PartialViewResult LinkDepartureFlights(int TripID)
        {
            LinkFlightSelectionViewModel model = new LinkFlightSelectionViewModel();

            model.TripID = TripID;
            model.DepartureFlight = true; // Returning Flight

            Trips trip = _db.Trips.FirstOrDefault(t => t.ID.Equals(TripID));

            model.FlightViewModels =
                _db.Flights.Where(f => DbFunctions.TruncateTime(f.StartDate) == trip.DepartureDate.Date && !f.Trips.Any(ft => ft.ID.Equals(TripID)) && f.DepartureFieldID.Equals((int)FlightViewModel.FlightsFields.Israel) && f.DestinationID.Equals((int)trip.DestinationID)).Select(f => new FlightManagementViewModel()
                {
                    ID = f.ID,
                    AirlineName = f.AirlineName,
                    CreationDate = f.CreationDate,
                    UpdatenDate = f.UpdateDate,
                    TakeOffFromIsrael = (bool)f.TakeOffFromIsrael,
                    DepartureDate = f.StartDate,
                    Comments = f.Comments,
                    DepartureField = (FlightViewModel.FlightsFields)f.DepartureFieldID,
                    DestenationField = (FlightViewModel.FlightsFields)f.DestinationID,
                    FlightNumber = f.FlightNumber,
                    NumberOfPassengers = f.NumberOfPassengers,
                    RemainingSeats = f.ExpeditionsFlights.Count() != 0 ? f.ExpeditionsFlights.Sum(e => e.NumberOfPassengers) : f.NumberOfPassengers
                }).ToList();

            return PartialView("_LinkReturningFlights", model);
        }

        [PermissionAuthorization(PermissionBitEnum.Trip_Edit)]
        [HttpGet]
        public ActionResult LinkFlight(int ID, int TripID, bool DepartureFlight)
        {
            Trips trip = _db.Trips.FirstOrDefault(t => t.ID.Equals(TripID));
            Flights flight = _db.Flights.FirstOrDefault(f => f.ID.Equals(ID));
            trip.Flights.Add(flight);

            _db.Trips.AddOrUpdate(trip);

            int results = _db.SaveChanges();

            return RedirectToAction("TripManagement", new { ID = TripID });


        }

        [PermissionAuthorization(PermissionBitEnum.Expedition_Manage)]
        [HttpGet]
        public PartialViewResult ManageExpedition(int ExpeditionID)
        {
            Expeditions expedition = _db.Expeditions.Include(e => e.ExpeditionsFlights).Include(e => e.Schools).FirstOrDefault(e => e.ID.Equals(ExpeditionID));

            ExpeditionFlightViewModel model = new ExpeditionFlightViewModel();

            model.ExpeditionID = expedition.ID;

            model.Expedition.ID = expedition.ID;
            model.Expedition.TripID = expedition.TripID;
            model.Expedition.Status = (eExpeditionStatus)expedition.Status;
            model.Expedition.ExpeditionManagerPhone = expedition.ManagerPhone;
            model.Expedition.ExpeditionManagerEmail = expedition.ManagerEmail;
            model.Expedition.ExpeditionManager = expedition.Manager;
            model.Expedition.BusesRequiredPerDay = expedition.BasesPerDay;
            model.Expedition.NumOfPassangersRequired = expedition.NumberOFpassengers;
            model.Expedition.School = expedition.Schools;

            Trips trip = _db.Trips.Include(t => t.Expeditions).Include(t => t.Flights).Include(t => t.Flights).Include(t => t.Flights.Select(f => f.ExpeditionsFlights)).FirstOrDefault(t => t.ID.Equals(expedition.TripID));

            model.DepartureFLights = trip.Flights.Where(f => f.TakeOffFromIsrael == true).Select(f => new FlightDetailsViewModel()
            {
                ID = f.ID,
                AirlineName = f.AirlineName,
                TakeOffFromIsrael = (bool)f.TakeOffFromIsrael,
                DepartureDate = f.StartDate,
                Comments = f.Comments,
                DepartureField = (FlightViewModel.FlightsFields)f.DepartureFieldID,
                DestenationField = (FlightViewModel.FlightsFields)f.DestinationID,
                FlightNumber = f.FlightNumber,
                NumberOfPassengers = f.NumberOfPassengers,
                RemainingSeats = f.ExpeditionsFlights.Count() != 0 ? f.NumberOfPassengers - f.ExpeditionsFlights.Sum(e => e.NumberOfPassengers) : f.NumberOfPassengers
            }).ToList();


            model.ReturningFLights = trip.Flights.Where(f => f.DestinationID.Equals((int)FlightViewModel.FlightsFields.Israel)).Select(f => new FlightDetailsViewModel()
            {
                ID = f.ID,
                AirlineName = f.AirlineName,
                TakeOffFromIsrael = (bool)f.TakeOffFromIsrael,
                DepartureDate = f.StartDate,
                Comments = f.Comments,
                DepartureField = (FlightViewModel.FlightsFields)f.DepartureFieldID,
                DestenationField = (FlightViewModel.FlightsFields)f.DestinationID,
                FlightNumber = f.FlightNumber,
                NumberOfPassengers = f.NumberOfPassengers,
                RemainingSeats = f.ExpeditionsFlights.Count() != 0 ? f.NumberOfPassengers - f.ExpeditionsFlights.Sum(e => e.NumberOfPassengers) : f.NumberOfPassengers
            }).ToList();

            foreach (var flight in expedition.ExpeditionsFlights)
            {
                var fli = model.DepartureFLights.Find(f => f.ID.Equals(flight.FlightID));

                if (fli != null)
                {
                    fli.AssignPassengers = flight.NumberOfPassengers;
                }

                fli = model.ReturningFLights.Find(f => f.ID.Equals(flight.FlightID));

                if (fli != null)
                {
                    fli.AssignPassengers = flight.NumberOfPassengers;
                }
            }




            return PartialView("_ManageExpedition", model);


        }

        [PermissionAuthorization(PermissionBitEnum.Expedition_Manage)]
        [HttpPost]
        public ActionResult ManageExpedition(ExpeditionFlightViewModel model)
        {
            Expeditions expedition = _db.Expeditions.Include(e => e.ExpeditionsFlights).FirstOrDefault(e => e.ID.Equals(model.ExpeditionID));



            expedition.Status = (int)model.Expedition.Status;
            expedition.BasesPerDay = model.Expedition.BusesRequiredPerDay;
            expedition.NumberOFpassengers = model.Expedition.NumOfPassangersRequired;

            if (expedition.Status.Equals((int)eExpeditionStatus.Rejected))
            {
                foreach (var item in expedition.ExpeditionsFlights.ToList())
                {
                    ExpeditionsFlights expeditionsFlight =
                            expedition.ExpeditionsFlights.FirstOrDefault(f => f.FlightID.Equals(item.ID));
                    _db.ExpeditionsFlights.Remove(expeditionsFlight);
                }
            }
            else
            {

                foreach (var deparureFlight in model.DepartureFLights)
                {
                    if (deparureFlight.AssignPassengers != 0 && deparureFlight.AssignPassengers != null)
                    {
                        bool update = true;

                        ExpeditionsFlights expeditionsFlight =
                            expedition.ExpeditionsFlights.FirstOrDefault(f => f.FlightID.Equals(deparureFlight.ID));

                        if (expeditionsFlight == null)
                        {
                            update = false;
                            expeditionsFlight = new ExpeditionsFlights();
                        }
                        expeditionsFlight.FlightID = deparureFlight.ID;
                        expeditionsFlight.ExpeditionID = model.ExpeditionID;
                        expeditionsFlight.NumberOfPassengers = deparureFlight.AssignPassengers;

                        if (!update)
                        {
                            expedition.ExpeditionsFlights.Add(expeditionsFlight);
                        }

                    }
                }

                foreach (var returnFlight in model.ReturningFLights)
                {


                    if (returnFlight.AssignPassengers != 0 && returnFlight.AssignPassengers != null)
                    {
                        bool update = true;
                        ExpeditionsFlights expeditionsFlight =
                            expedition.ExpeditionsFlights.FirstOrDefault(f => f.FlightID.Equals(returnFlight.ID));
                        if (expeditionsFlight == null)
                        {
                            update = false;
                            expeditionsFlight = new ExpeditionsFlights();
                        }

                        expeditionsFlight.FlightID = returnFlight.ID;
                        expeditionsFlight.ExpeditionID = model.ExpeditionID;
                        expeditionsFlight.NumberOfPassengers = returnFlight.AssignPassengers;

                        if (!update)
                        {
                            expedition.ExpeditionsFlights.Add(expeditionsFlight);
                        }
                    }


                }
            }

            _db.Expeditions.AddOrUpdate(expedition);
            int results = _db.SaveChanges();

            return RedirectToAction("TripManagement", new { @ID = model.Expedition.TripID });
        }

        [PermissionAuthorization(PermissionBitEnum.Expedition_Manage)]
        public ActionResult ResetExpadition(int ExpaditionID)
        {
            Expeditions expedition = _db.Expeditions.Include(e => e.ExpeditionsFlights).FirstOrDefault(e => e.ID.Equals(ExpaditionID));
            expedition.Status = (int)eExpeditionStatus.InProcess;

            foreach (var item in expedition.ExpeditionsFlights.ToList())
            {
                ExpeditionsFlights expeditionsFlight =
                            expedition.ExpeditionsFlights.FirstOrDefault(f => f.ID.Equals(item.ID));
                _db.ExpeditionsFlights.Remove(expeditionsFlight);
            }

            _db.Expeditions.AddOrUpdate(expedition);

            int results = _db.SaveChanges();

            return RedirectToAction("TripManagement", new { @ID = expedition.TripID });
        }

        [PermissionAuthorization(PermissionBitEnum.Trip_Edit)]
        public ActionResult DeleteLinkedFlight(int ID, int TripID)
        {
            Trips _trip = _db.Trips.Include(t => t.Flights).Include(t => t.Expeditions).Include(e => e.Expeditions.Select(f => f.ExpeditionsFlights)).FirstOrDefault(t => t.ID.Equals(TripID));



            List<ExpeditionsFlights> flights = new List<ExpeditionsFlights>();

            foreach (var item in _trip.Expeditions.ToList())
            {
                flights.AddRange(item.ExpeditionsFlights.Where(f => f.FlightID.Equals(ID)));
            }

            //Remove the expedition flights
            _db.ExpeditionsFlights.RemoveRange(flights);

            //Remove the flight
            Flights flight = _trip.Flights.FirstOrDefault(f => f.ID.Equals(ID));

            _trip.Flights.Remove(flight);

            int results = _db.SaveChanges();

            return RedirectToAction("TripManagement", new { ID = TripID });
        }

        [PermissionAuthorization(PermissionBitEnum.Trip_Delete)]
        [HttpGet]
        public ActionResult DeleteTrip(int ID)
        {
            DeleteViewModel model = new DeleteViewModel();

            Trips trip = _db.Trips.FirstOrDefault(t => t.ID.Equals(ID));

            int NumberOfAsociatedFlights = _dbDapper.GetRecords<int>(QueriesRepository.CheckIfTripHasFlights, new { ID }).FirstOrDefault();

            model.ID = trip.ID;

            model.IsDeletable = true;

            if (NumberOfAsociatedFlights > 0)
            {
                model.IsDeletable = false;
            }

            return PartialView("_DeleteTripConfrim", model);
        }

        [PermissionAuthorization(PermissionBitEnum.Trip_Delete)]
        [HttpPost]
        public ActionResult DeleteTrip(DeleteViewModel model)
        {
            Trips trip = _db.Trips.FirstOrDefault(t => t.ID.Equals(model.ID));

            trip.IsActive = false;

            _db.Trips.AddOrUpdate(trip);

            _db.SaveChanges();

            return RedirectToAction("GetTrips");
        }

        [PermissionAuthorization(PermissionBitEnum.Expedition_Manage)]
        [HttpGet]
        public PartialViewResult MoveOrder(int OrderID)
        {
            MoveOrderViewModel model = new MoveOrderViewModel();
            model.OrderID = OrderID;

            model.Trips.Items = _dbDapper.GetRecords<FutureTripsViewModel>(QueriesRepository.GetFutureTrips, new { Date = DateTime.Now }).OrderBy(x => x.DepartureDate).Select(x => new CustomSelectListItem()
            {
                ID = x.ID,
                Name = x.ID + " - " + x.DepartureDate.ToString(AppConfig.FormatDate) + " - " + ((FlightViewModel.FlightsFields)x.DestinationID).GetDescription()
            }).ToList();

            return PartialView("_MoveOrder", model);
        }

        [PermissionAuthorization(PermissionBitEnum.Expedition_Manage)]
        [HttpPost]
        public ActionResult MoveOrder(MoveOrderViewModel model)
        {
            if (ModelState.IsValid)
            {
                bool result = _dbDapper.ExecuteCommand(QueriesRepository.MoveOrderToTripID, new { model.OrderID, model.TripID });
            }

            return RedirectToAction("TripManagement", new { ID = model.TripID });
        }

        [PermissionAuthorization(PermissionBitEnum.Trip_Edit)]
        [HttpGet]
        public PartialViewResult UploadImportTrips()
        {
            ImportTripsViewModel model = new ImportTripsViewModel();

            return PartialView("_UploadImportTrips", model);
        }

        [PermissionAuthorization(PermissionBitEnum.Trip_Edit)]
        [HttpPost]
        public ActionResult ImportTrips(ImportTripsViewModel model)
        {
            if (ModelState.IsValid)
            {
                string filename = String.Format("{0}{1}", DateTime.Now.ToString("ddMMyyyyhhmm"),
                    Path.GetExtension(model.Attachment.FileName));
                model.Attachment.SaveAs(
                    Server.MapPath(String.Format("{0}/{1}", AppConfig.TempImportRelativePath, filename)));

                var excel = new ExcelQueryFactory(Path.Combine(AppConfig.TempImportRoot, filename));


                excel.AddTransformation<TripViewModelBasic>(x => x.DepartureDate, c =>
                {
                    DateTime OutBirthDay;
                    if (DateTime.TryParse(c, out OutBirthDay))
                    {
                        return DateTime.Parse(c);
                    }
                    if (DateTime.TryParseExact(c, AppConfig.DateTimeFormatParsing, System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out OutBirthDay))
                    {
                        return DateTime.ParseExact(c, AppConfig.DateTimeFormatParsing, System.Globalization.CultureInfo.CurrentCulture, System.Globalization.DateTimeStyles.None);
                    }

                    return null;
                });

                excel.AddTransformation<TripViewModelBasic>(x => x.ReturnDate, c =>
                {
                    DateTime OutBirthDay;
                    if (DateTime.TryParse(c, out OutBirthDay))
                    {
                        return DateTime.Parse(c);
                    }
                    if (DateTime.TryParseExact(c, AppConfig.DateTimeWithTimeFormatParsing, System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out OutBirthDay))
                    {
                        return DateTime.ParseExact(c, AppConfig.DateTimeWithTimeFormatParsing, System.Globalization.CultureInfo.CurrentCulture, System.Globalization.DateTimeStyles.None);
                    }

                    return null;
                });

                excel.AddTransformation<TripViewModelBasic>(x => x.Destenation, c =>
                {
                    var field = FlightViewModel.FlightsFields.Israel;
                    switch (c)
                    {
                        case "ישראל":
                            {
                                field = FlightViewModel.FlightsFields.Israel;
                                break;
                            }
                        case "ורשה":
                            {
                                field = FlightViewModel.FlightsFields.Warsaw;
                                break;
                            }
                        case "קראקוב":
                            {
                                field = FlightViewModel.FlightsFields.Krakow;
                                break;
                            }
                        case "קטוביץ":
                            {
                                field = FlightViewModel.FlightsFields.Katowice;
                                break;
                            }
                        case "זשוב":
                            {
                                field = FlightViewModel.FlightsFields.Rzeszow;
                                break;
                            }
                        case "וורוצלב":
                            {
                                field = FlightViewModel.FlightsFields.Wroclaw;
                                break;
                            }
                        case "לודז":
                            {
                                field = FlightViewModel.FlightsFields.Lodge;
                                break;
                            }
                        case "לובלין":
                            {
                                field = FlightViewModel.FlightsFields.Loblin;
                                break;
                            }
                    }
                    return field;
                });

                excel.AddTransformation<TripViewModelBasic>(x => x.ReturnedFrom, c =>
                {
                    var field = FlightViewModel.FlightsFields.Israel;
                    switch (c)
                    {
                        case "ישראל":
                            {
                                field = FlightViewModel.FlightsFields.Israel;
                                break;
                            }
                        case "ורשה":
                            {
                                field = FlightViewModel.FlightsFields.Warsaw;
                                break;
                            }
                        case "קראקוב":
                            {
                                field = FlightViewModel.FlightsFields.Krakow;
                                break;
                            }
                        case "קטוביץ":
                            {
                                field = FlightViewModel.FlightsFields.Katowice;
                                break;
                            }
                        case "זשוב":
                            {
                                field = FlightViewModel.FlightsFields.Rzeszow;
                                break;
                            }
                        case "וורוצלב":
                            {
                                field = FlightViewModel.FlightsFields.Wroclaw;
                                break;
                            }
                        case "לודז":
                            {
                                field = FlightViewModel.FlightsFields.Lodge;
                                break;
                            }
                        case "לובלין":
                            {
                                field = FlightViewModel.FlightsFields.Loblin;
                                break;
                            }
                    }
                    return field;
                });

                var trips = from c in excel.WorksheetRange<TripViewModelBasic>("A1", "F1000", "Import")
                            select c;



                List<Trips> tripsToImport = Mapper.Map<List<TripViewModelBasic>, List<Trips>>(trips.ToList());


                foreach (Trips _trip in tripsToImport)
                {
                    _trip.CreationDate = DateTime.Now;
                    _trip.UpdateDate = _trip.CreationDate;
                    _trip.IsActive = true;

                    bool results = _dbDapper.InsertOrUpdateTrip(_trip);

                }

                System.IO.File.Delete(Path.Combine(AppConfig.TempImportRoot, filename));

            }
            return RedirectToAction("GetTrips");
        }

        [HttpGet]
        public FileResult DownloadFileTemplate(string FilePath, string FileName)
        {
            byte[] fileBytes = System.IO.File.ReadAllBytes(Path.Combine(AppConfig.GroupsImportTemplateRoot, FilePath));
            var response = new FileContentResult(fileBytes, "application/octet-stream");
            response.FileDownloadName = FileName;
            return response;
        }
    }
}