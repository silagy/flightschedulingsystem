﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Flight.DAL.FlightDB.Methods;
using FlightScheduling.DAL;
using FlightScheduling.Web.Areas.Administration.Models.Flights;
using Otakim.Website.Infrastructure;
using System.Data.Entity;
using System.IO;
using AutoMapper;
using Endilo.ExportToExcel;
using FlightScheduling.Language;
using FlightScheduling.Web.Infrastructure;
using LinqToExcel;
using FlightScheduling.DAL.CustomEntities.Permissions;

namespace FlightScheduling.Web.Areas.Administration.Controllers
{
    [PermissionAuthorization(PermissionBitEnum.Flight_View)]
    public class FlightsController : Controller
    {
        private FlightDB _db = new FlightDB();
        DBRepository _dbDapper = new DBRepository();

        protected override void Dispose(bool disposing)
        {
            _db.Dispose();
            base.Dispose(disposing);
        }


        //
        // GET: /Administration/Flights/

        [HttpGet]
        public ActionResult GetFlights(DateTime? FilterFromDate, DateTime? FilterToDate)
        {
            FlightsViewModel model = new FlightsViewModel();

            if (Session["FlightFilterFromDate"] != null)
            {
                model.FilterFromDate = (DateTime)Session["FlightFilterFromDate"];
                FilterFromDate = model.FilterFromDate;
            }
            else if (FilterFromDate == null)
            {
                FilterFromDate = SystemConfigurationRepository.FlightsFilterStartDate;
                model.FilterFromDate = (DateTime)FilterFromDate;

                Session.Add("FlightFilterFromDate", model.FilterFromDate);
            }

            if (Session["FlightFilterToDate"] != null)
            {
                model.FilterToDate = (DateTime)Session["FlightFilterToDate"];
                FilterToDate = model.FilterToDate;
            }
            else if (FilterToDate == null)
            {
                FilterToDate = SystemConfigurationRepository.FlightsFilterEndDate;
                model.FilterToDate = (DateTime)FilterToDate;

                Session.Add("FlightFilterToDate", model.FilterToDate);
            }

            model.FlightViewModels = _db.Flights.RemoveInActive().Where(f => f.StartDate >= FilterFromDate && f.StartDate <= FilterToDate).Select(f => new FlightViewModel()
            {
                ID = f.ID,
                DepartureDate = (DateTime)f.StartDate,
                DepartureField = (FlightViewModel.FlightsFields)f.DepartureFieldID,
                DestenationField = (FlightViewModel.FlightsFields)f.DestinationID,
                NumberOfPassengers = f.NumberOfPassengers,
                AirlineName = f.AirlineName,
                FlightNumber = f.FlightNumber,
                Comments = f.Comments,
                CreationDate = f.CreationDate,
                UpdatenDate = f.UpdateDate,
                TakeOffFromIsrael = f.TakeOffFromIsrael != null && (bool)f.TakeOffFromIsrael,
                ArrivalDate = f.ArrivalTime
            }).ToList();

            return View(model);
        }

        [HttpPost]
        public ActionResult GetFilteredFlights(DateTime? FilterFromDate, DateTime? FilterToDate)
        {
            FlightsViewModel model = new FlightsViewModel();

            if (FilterFromDate == null)
            {
                FilterFromDate = SystemConfigurationRepository.FlightsFilterStartDate;
            }

            if (FilterToDate == null)
            {
                FilterToDate = SystemConfigurationRepository.FlightsFilterEndDate;
            }

            Session.Add("FlightFilterFromDate", FilterFromDate);
            Session.Add("FlightFilterToDate", FilterToDate);


            model.FlightViewModels = _db.Flights.RemoveInActive().Where(f => f.StartDate >= FilterFromDate && f.StartDate <= FilterToDate).Select(f => new FlightViewModel()
            {
                ID = f.ID,
                DepartureDate = (DateTime)f.StartDate,
                DepartureField = (FlightViewModel.FlightsFields)f.DepartureFieldID,
                DestenationField = (FlightViewModel.FlightsFields)f.DestinationID,
                NumberOfPassengers = f.NumberOfPassengers,
                AirlineName = f.AirlineName,
                FlightNumber = f.FlightNumber,
                Comments = f.Comments,
                CreationDate = f.CreationDate,
                UpdatenDate = f.UpdateDate,
                TakeOffFromIsrael = f.TakeOffFromIsrael != null && (bool)f.TakeOffFromIsrael,
                ArrivalDate = f.ArrivalTime
            }).ToList();

            return View("GetFlights", model);
        }

        public ActionResult ExportToExcel()
        {
            FlightsViewModel model = new FlightsViewModel();

            if (Session["FlightFilterFromDate"] != null)
            {
                model.FilterFromDate = (DateTime)Session["FlightFilterFromDate"];
            }
            else
            {
                model.FilterFromDate = SystemConfigurationRepository.FlightsFilterStartDate;

                Session.Add("FlightFilterFromDate", model.FilterFromDate);
            }

            if (Session["FlightFilterToDate"] != null)
            {
                model.FilterToDate = (DateTime)Session["FlightFilterToDate"];
            }
            else
            {
                model.FilterToDate = SystemConfigurationRepository.FlightsFilterEndDate;

                Session.Add("FlightFilterToDate", model.FilterToDate);
            }

            model.FlightViewModels = _db.Flights.RemoveInActive().Where(f => f.StartDate >= model.FilterFromDate && f.StartDate <= model.FilterToDate).Select(f => new FlightViewModel()
            {
                ID = f.ID,
                DepartureDate = (DateTime)f.StartDate,
                DepartureField = (FlightViewModel.FlightsFields)f.DepartureFieldID,
                DestenationField = (FlightViewModel.FlightsFields)f.DestinationID,
                NumberOfPassengers = f.NumberOfPassengers,
                AirlineName = f.AirlineName,
                FlightNumber = f.FlightNumber,
                Comments = f.Comments,
                CreationDate = f.CreationDate,
                UpdatenDate = f.UpdateDate,
                TakeOffFromIsrael = f.TakeOffFromIsrael != null && (bool)f.TakeOffFromIsrael,
                ArrivalDate = f.ArrivalTime

            }).ToList();

            ExportToExcel<FlightViewModel> exc = new ExportToExcel<FlightViewModel>(model.FlightViewModels, ReportsStrings.FlightsReport);
            var stream = exc.GetFile();
            string fileName = String.Format("{0} {1}.xlsx", ReportsStrings.FlightsReport, DateTime.Now.ToString("dd-MM-yyyy hh:ss"));
            string contentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";

            stream.Position = 0;
            return File(stream, contentType, fileName);
        }

        [PermissionAuthorization(PermissionBitEnum.Flight_Edit)]
        [HttpGet]
        public ActionResult AddFlight()
        {
            FlightViewModel model = new FlightViewModel();
            model.DepartureDate = null;

            if (Request.IsAjaxRequest())
            {
                return PartialView("_AddFlight", model);
            }
            return View(model);
        }

        [PermissionAuthorization(PermissionBitEnum.Flight_Edit)]
        [HttpPost]
        public ActionResult AddFlight(FlightViewModel model)
        {
            if (ModelState.IsValid)
            {
                Flights _flightToAdd = new Flights();
                _flightToAdd.DepartureFieldID = (int)model.DepartureField;
                _flightToAdd.TakeOffFromIsrael = model.DepartureField == FlightViewModel.FlightsFields.Israel;
                string ArrivalDateTime = String.Format("{0} {1}", ((DateTime)model.ArrivalDate).ToString("dd/MM/yyyy"), model.ArrivalHours);
                _flightToAdd.ArrivalTime = DateTime.ParseExact(ArrivalDateTime, AppConfig.DateTimeWithTimeFormatParsing, System.Globalization.CultureInfo.CurrentCulture, System.Globalization.DateTimeStyles.None);
                _flightToAdd.DestinationID = (int)model.DestenationField;
                string departureDateTime = String.Format("{0} {1}", ((DateTime)model.DepartureDate).ToString("dd/MM/yyyy"), model.DepartureTime);
                _flightToAdd.StartDate = DateTime.ParseExact(departureDateTime, AppConfig.DateTimeWithTimeFormatParsing, System.Globalization.CultureInfo.CurrentCulture, System.Globalization.DateTimeStyles.None);
                _flightToAdd.IsActive = true;
                _flightToAdd.NumberOfPassengers = model.NumberOfPassengers;
                _flightToAdd.AirlineName = model.AirlineName;
                _flightToAdd.FlightNumber = model.FlightNumber;
                _flightToAdd.Comments = model.Comments;
                _flightToAdd.CreationDate = DateTime.Now;
                _flightToAdd.UpdateDate = DateTime.Now;

                _db.Flights.Add(_flightToAdd);

                int results = _db.SaveChanges();

                return RedirectToAction("GetFlights");

            }
            return View(model);
        }

        [PermissionAuthorization(PermissionBitEnum.Flight_Edit)]
        [HttpGet]
        public ActionResult EditFlight(int ID)
        {
            if (ID != null)
            {
                Flights flight = _db.Flights.FirstOrDefault(f => f.ID.Equals(ID));

                FlightViewModel model = new FlightViewModel();

                model.DepartureField = (FlightViewModel.FlightsFields)flight.DepartureFieldID;
                model.TakeOffFromIsrael = (bool)flight.TakeOffFromIsrael;
                model.DestenationField = (FlightViewModel.FlightsFields)flight.DestinationID;
                model.DepartureDate = (DateTime)flight.StartDate;
                model.NumberOfPassengers = flight.NumberOfPassengers;
                model.AirlineName = flight.AirlineName;
                model.FlightNumber = flight.FlightNumber;
                model.Comments = flight.Comments;
                if (flight.StartDate != null)
                {
                    model.DepartureTime = flight.StartDate.ToString("HH:mm");
                }

                if (flight.ArrivalTime != null)
                {
                    model.ArrivalDate = flight.ArrivalTime;
                    model.ArrivalHours = ((DateTime)flight.ArrivalTime).ToString("HH:mm");
                }



                return View(model);
            }

            return RedirectToAction("GetFlights");
        }

        [PermissionAuthorization(PermissionBitEnum.Flight_Edit)]
        [HttpPost]
        public ActionResult EditFlight(FlightViewModel model)
        {
            if (ModelState.IsValid)
            {
                Flights _flightToUpdate = _db.Flights.FirstOrDefault(f => f.ID.Equals(model.ID));

                _flightToUpdate.DepartureFieldID = (int)model.DepartureField;
                _flightToUpdate.TakeOffFromIsrael = model.DepartureField == FlightViewModel.FlightsFields.Israel;
                _flightToUpdate.DestinationID = (int)model.DestenationField;
                string departureDateTime = String.Format("{0} {1}", ((DateTime)model.DepartureDate).ToString("dd/MM/yyyy"), model.DepartureTime);
                _flightToUpdate.StartDate = DateTime.ParseExact(departureDateTime, AppConfig.DateTimeWithTimeFormatParsing, System.Globalization.CultureInfo.CurrentCulture, System.Globalization.DateTimeStyles.None);
                string ArrivalDateTime = String.Format("{0} {1}", ((DateTime)model.ArrivalDate).ToString("dd/MM/yyyy"), model.ArrivalHours);
                _flightToUpdate.ArrivalTime = DateTime.ParseExact(ArrivalDateTime, AppConfig.DateTimeWithTimeFormatParsing, System.Globalization.CultureInfo.CurrentCulture, System.Globalization.DateTimeStyles.None);
                _flightToUpdate.IsActive = true;
                _flightToUpdate.NumberOfPassengers = model.NumberOfPassengers;
                _flightToUpdate.AirlineName = model.AirlineName;
                _flightToUpdate.FlightNumber = model.FlightNumber;
                _flightToUpdate.Comments = model.Comments;
                _flightToUpdate.UpdateDate = DateTime.Now;

                _db.Flights.AddOrUpdate(_flightToUpdate);

                int results = _db.SaveChanges();

                return RedirectToAction("GetFlights");
            }


            return View(model);
        }

        [PermissionAuthorization(PermissionBitEnum.Flight_Delete)]
        [HttpGet]
        public ActionResult DeleteFlight(int ID)
        {
            Flights _flights = _db.Flights.Include(f => f.Trips).FirstOrDefault(f => f.ID.Equals(ID));

            DeleteFlightViewModel model = new DeleteFlightViewModel();
            model.ID = _flights.ID;

            model.IsDeletable = true;

            if (_flights.Trips != null && _flights.Trips.Count() > 0)
            {
                model.Trips = _flights.Trips;
                model.IsDeletable = false;
            }

            return PartialView("_DeleteFlights", model);
        }

        [PermissionAuthorization(PermissionBitEnum.Flight_Delete)]
        [HttpPost]
        public ActionResult DeleteFlight(DeleteFlightViewModel model)
        {
            if (model.ID != null)
            {
                Flights _flight = _db.Flights.FirstOrDefault(f => f.ID == model.ID);

                _flight.IsActive = false;

                _db.Flights.AddOrUpdate(_flight);

                int results = _db.SaveChanges();


            }

            return RedirectToAction("GetFlights");
        }

        [HttpPost]
        [PermissionAuthorization(PermissionBitEnum.Flight_Delete_Multiple)]
        public PartialViewResult DeleteMultipleFlights(IEnumerable<int> Items)
        {
            DeleteMultipleFlightsViewModel model = new DeleteMultipleFlightsViewModel();
            model.Flights = Items.ToList();

            return PartialView("_DeleteMultipleFlights", model);
        }

        [PermissionAuthorization(PermissionBitEnum.Flight_Delete_Multiple)]
        [HttpPost]
        public ActionResult DeleteMultipleFlightsPerform(DeleteMultipleFlightsViewModel model)
        {
            bool results = _dbDapper.UpdateOperation(QueriesRepository.DeleteMultipleFlights, new { IDs = model.Flights });

            return RedirectToAction("GetFlights");
        }

        [PermissionAuthorization(PermissionBitEnum.Flight_Edit)]
        [HttpGet]
        public PartialViewResult UploadImportFlights()
        {
            ImportFlightsViewModel model = new ImportFlightsViewModel();

            return PartialView("_UploadImportFlights", model);
        }

        [PermissionAuthorization(PermissionBitEnum.Flight_Edit)]
        [HttpPost]
        public ActionResult ImportFlights(ImportFlightsViewModel model)
        {
            if (ModelState.IsValid)
            {
                string filename = String.Format("{0}{1}", DateTime.Now.ToString("ddMMyyyyhhmm"),
                    Path.GetExtension(model.Attachment.FileName));
                model.Attachment.SaveAs(
                    Server.MapPath(String.Format("{0}/{1}", AppConfig.TempImportRelativePath, filename)));

                var excel = new ExcelQueryFactory(Path.Combine(AppConfig.TempImportRoot, filename));


                excel.AddTransformation<FlightViewModel>(x => x.DepartureDate, c =>
                {
                    DateTime OutBirthDay;
                    if (DateTime.TryParse(c, out OutBirthDay))
                    {
                        return DateTime.Parse(c);
                    }
                    if (DateTime.TryParseExact(c, AppConfig.DateTimeWithTimeFormatParsing, System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out OutBirthDay))
                    {
                        return DateTime.ParseExact(c, AppConfig.DateTimeWithTimeFormatParsing, System.Globalization.CultureInfo.CurrentCulture, System.Globalization.DateTimeStyles.None);
                    }

                    return null;
                });

                excel.AddTransformation<FlightViewModel>(x => x.ArrivalDate, c =>
                {
                    DateTime OutBirthDay;
                    if (DateTime.TryParse(c, out OutBirthDay))
                    {
                        return DateTime.Parse(c);
                    }
                    if (DateTime.TryParseExact(c, AppConfig.DateTimeWithTimeFormatParsing, System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out OutBirthDay))
                    {
                        return DateTime.ParseExact(c, AppConfig.DateTimeWithTimeFormatParsing, System.Globalization.CultureInfo.CurrentCulture, System.Globalization.DateTimeStyles.None);
                    }

                    return null;
                });

                excel.AddTransformation<FlightViewModel>(x => x.DepartureField, c =>
                {
                    var field = FlightViewModel.FlightsFields.Israel;
                    switch (c)
                    {
                        case "ישראל":
                            {
                                field = FlightViewModel.FlightsFields.Israel;
                                break;
                            }
                        case "ורשה":
                            {
                                field = FlightViewModel.FlightsFields.Warsaw;
                                break;
                            }
                        case "קראקוב":
                            {
                                field = FlightViewModel.FlightsFields.Krakow;
                                break;
                            }
                        case "קטוביץ":
                        case "קטוביץ'":
                            {
                                field = FlightViewModel.FlightsFields.Katowice;
                                break;
                            }
                        case "זשוב":
                            {
                                field = FlightViewModel.FlightsFields.Rzeszow;
                                break;
                            }
                        case "וורוצלב":
                            {
                                field = FlightViewModel.FlightsFields.Wroclaw;
                                break;
                            }
                        case "לודז":
                            {
                                field = FlightViewModel.FlightsFields.Lodge;
                                break;
                            }
                        case "לובלין":
                            {
                                field = FlightViewModel.FlightsFields.Loblin;
                                break;
                            }
                    }
                    return field;
                });

                excel.AddTransformation<FlightViewModel>(x => x.DestenationField, c =>
                {
                    var field = FlightViewModel.FlightsFields.Israel;
                    switch (c)
                    {
                        case "ישראל":
                            {
                                field = FlightViewModel.FlightsFields.Israel;
                                break;
                            }
                        case "ורשה":
                            {
                                field = FlightViewModel.FlightsFields.Warsaw;
                                break;
                            }
                        case "קראקוב":
                            {
                                field = FlightViewModel.FlightsFields.Krakow;
                                break;
                            }
                        case "קטוביץ":
                        case "קטוביץ'":
                            {
                                field = FlightViewModel.FlightsFields.Katowice;
                                break;
                            }
                        case "זשוב":
                            {
                                field = FlightViewModel.FlightsFields.Rzeszow;
                                break;
                            }
                        case "וורוצלב":
                            {
                                field = FlightViewModel.FlightsFields.Wroclaw;
                                break;
                            }
                        case "לודז":
                            {
                                field = FlightViewModel.FlightsFields.Lodge;
                                break;
                            }
                        case "לובלין":
                            {
                                field = FlightViewModel.FlightsFields.Loblin;
                                break;
                            }
                    }
                    return field;
                });

                var flights = excel.WorksheetRange<FlightViewModel>("A1", "J1000", "Import");

                // Removing empty rows.
                var resolvedFLights = flights.ToList().Where(f => f.NumberOfPassengers > 0 & f.ArrivalDate != null).ToList();

                List<Flights> flightsToImport = Mapper.Map<List<FlightViewModel>, List<Flights>>(resolvedFLights);

                //Todo: to report back which flight were not included in the import

                foreach (Flights _flight in flightsToImport)
                {
                    _flight.TakeOffFromIsrael = _flight.DepartureFieldID == (int)FlightViewModel.FlightsFields.Israel;
                    _flight.IsActive = true;
                    _flight.CreationDate = DateTime.Now;
                    _flight.UpdateDate = DateTime.Now;
                    bool results = _dbDapper.InsertOrUpdateFlight(_flight);

                }

                System.IO.File.Delete(Path.Combine(AppConfig.TempImportRoot, filename));

            }
            return RedirectToAction("GetFlights");
        }

        [HttpGet]
        public FileResult DownloadFileTemplate(string FilePath, string FileName)
        {
            byte[] fileBytes = System.IO.File.ReadAllBytes(Path.Combine(AppConfig.GroupsImportTemplateRoot, FilePath));
            var response = new FileContentResult(fileBytes, "application/octet-stream");
            response.FileDownloadName = FileName;
            return response;
        }
    }
}