﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Endilo.ExportToExcel;
using FlightScheduling.DAL;
using FlightScheduling.Language;
using FlightScheduling.Web.Areas.Administration.Models.Flights;
using FlightScheduling.Web.Areas.Administration.Models.Reports;
using System.Data.Entity;
using Endilo.Helpers;
using Flight.DAL.FlightDB.Methods;
using FlightScheduling.Website.Infrastructure;
using System.Text;
using System.IO;
using AutoMapper;
using FlightScheduling.DAL.CustomEntities;
using CsvHelper;
using FlightScheduling.Web.Areas.Administration.Models.Common;
using FlightScheduling.Web.Infrastructure;
using FlightScheduling.DAL.CustomEntities.Permissions;
using Otakim.Website.Infrastructure;

namespace FlightScheduling.Web.Areas.Administration.Controllers
{
    public class ReportsController : Controller
    {

        private FlightDB _db = new FlightDB();
        DBRepository _dbDapper = new DBRepository();

        protected override void Dispose(bool disposing)
        {
            _db.Dispose();
            base.Dispose(disposing);
        }

        public ActionResult GetReports()
        {



            return View();
        }

        [PermissionAuthorization(PermissionBitEnum.Reports_BusesPerDay)]
        public ActionResult GetBusesPerDay()
        {
            BusesPerDayReportViewModel model = new BusesPerDayReportViewModel();
            return View(model);
        }

        [PermissionAuthorization(PermissionBitEnum.Reports_BusesPerDay)]
        [HttpPost]
        public ActionResult GetBusesPerDay(BusesPerDayReportViewModel model)
        {
            if (ModelState.IsValid)
            {
                model.BusesPerDayList = _db.BusesPerDay(model.StartDate, model.EndDate).Select(r => new BusesPerDayViewModel()
                {
                    Day = (DateTime)r.Date,
                    BusesPerDay = (int)r.BusesPerDay,
                    ExpeditionPerDay = (int)r.ExpeditionsPerDay
                }).ToList();

                Session.Add("BusesPerDayReport", model.BusesPerDayList);

                return View(model);
            }



            return View(model);
        }


        [PermissionAuthorization(PermissionBitEnum.Reports_AvailableSeats)]
        public ActionResult GetFlightSeatsAvailability()
        {

            FlightSeatsAvailabilityReportViewModel model = new FlightSeatsAvailabilityReportViewModel();

            return View(model);
        }

        [PermissionAuthorization(PermissionBitEnum.Reports_AvailableSeats)]
        [HttpPost]
        public ActionResult GetFlightSeatsAvailability(FlightSeatsAvailabilityReportViewModel model)
        {

            if (ModelState.IsValid)
            {
                model.Data = _dbDapper.GetRecords<RemainingSeatsInFLightsViewModel>(QueriesRepository.GetTripAvilabilitySeats, new { model.StartDate, model.EndDate }).ToList();

                Session.Add("AvailableSeats", model);
            }


            return View(model);
        }

        [PermissionAuthorization(PermissionBitEnum.Reports_Agencies)]
        [HttpGet]
        public ActionResult AgencyReport()
        {
            AgencyReportContainerViewModel model = new AgencyReportContainerViewModel();

            Users _user = _db.GetUserByLoginName(User.Identity.Name);
            List<AgencyReportViewModel> reportdata = new List<AgencyReportViewModel>();
            if (_user.RuleEnum.Equals(eRole.Agent))
            {
                model.Data =
                    _dbDapper.GetAgencyReport<AgencyReportViewModel>(QueriesRepository.GetAgencyReport, model.StartDate, model.EndDate, _user.AgencyID, (int)eExpeditionStatus.Rejected).ToList();
            }
            else if (_user.RuleEnum.Equals(eRole.Viewer))
            {
                model.Data = _dbDapper.GetAgencyReport<AgencyReportViewModel>(QueriesRepository.GetAgencyReport, model.StartDate, model.EndDate, null, (int)eExpeditionStatus.Rejected).ToList();
            }
            else
            {
                model.Data = _dbDapper.GetAgencyReport<AgencyReportViewModel>(QueriesRepository.GetAgencyReport, model.StartDate, model.EndDate, null, (int)eExpeditionStatus.Rejected).ToList();
            }

            Session.Add("AgencyReportData", model.Data);

            return View("AgencyReport", model);


        }

        [PermissionAuthorization(PermissionBitEnum.Reports_Agencies)]
        [HttpPost]
        public ActionResult AgencyReport(AgencyReportContainerViewModel model)
        {
            Users _user = _db.GetUserByLoginName(User.Identity.Name);
            List<AgencyReportViewModel> reportdata = new List<AgencyReportViewModel>();
            if (_user.RuleEnum.Equals(eRole.Agent))
            {
                model.Data =
                    _dbDapper.GetAgencyReport<AgencyReportViewModel>(QueriesRepository.GetAgencyReport, model.StartDate, model.EndDate, _user.AgencyID, (int)eExpeditionStatus.Rejected).ToList();
            }
            else if (_user.RuleEnum.Equals(eRole.Viewer))
            {
                model.Data = _dbDapper.GetAgencyReport<AgencyReportViewModel>(QueriesRepository.GetAgencyReport, model.StartDate, model.EndDate, null, (int)eExpeditionStatus.Rejected).ToList();
            }
            else
            {
                model.Data = _dbDapper.GetAgencyReport<AgencyReportViewModel>(QueriesRepository.GetAgencyReport, model.StartDate, model.EndDate, null, (int)eExpeditionStatus.Rejected).ToList();
            }

            Session.Add("AgencyReportData", model.Data);

            return View("AgencyReport", model);
        }

        [PermissionAuthorization(PermissionBitEnum.Reports_Agencies)]
        [HttpGet]
        public ActionResult AgencyReportExport()
        {
            if (Session["AgencyReportData"] != null)
            {
                ExportToExcel<AgencyReportViewModel> exc = new ExportToExcel<AgencyReportViewModel>((List<AgencyReportViewModel>)Session["AgencyReportData"], ReportsStrings.AgencyReport);
                var stream = exc.GetFile();
                string fileName = String.Format("{0} {1}.xlsx", ReportsStrings.AgencyReport, DateTime.Now.ToString("dd-MM-yyyy hh:ss"));
                string contentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";

                stream.Position = 0;
                return File(stream, contentType, fileName);
            }

            return null;
        }

        [PermissionAuthorization(PermissionBitEnum.Reports_Flights)]
        public ActionResult FlightsReport()
        {
            FlightsReportMainViewModel model = new FlightsReportMainViewModel();

            model.FlightsReportViewModels = GetFlights(model.FilterFromDate, model.FilterToDate);

            Session.Add("FlightsReport", model.FlightsReportViewModels);

            return View(model);
        }

        [PermissionAuthorization(PermissionBitEnum.Reports_Flights)]
        private List<FlightsReportViewModel> GetFlights(DateTime FromDate, DateTime ToDate)
        {
            Users _user = _db.GetUserByLoginName(User.Identity.Name);



            var reportData = (from exflights in _db.ExpeditionsFlights
                              join flights in _db.Flights on exflights.FlightID equals flights.ID
                              join Expeditions in _db.Expeditions on exflights.ExpeditionID equals Expeditions.ID
                              join Trips in _db.Trips on Expeditions.TripID equals Trips.ID
                              where Expeditions.Status == 2 && Trips.DepartureDate >= FromDate && Trips.DepartureDate <= ToDate
                              select new
                              {
                                  TripID = Trips.ID,
                                  DepartureDate = Trips.DepartureDate,
                                  ReturnDate = Trips.ReturningDate,
                                  FlightID = exflights.FlightID,
                                  AirlineName = flights.AirlineName,
                                  NumberOfPassengers = exflights.NumberOfPassengers,
                                  TakeOffFromIsrael = flights.TakeOffFromIsrael,
                                  FlightDepartureFieldID = (FlightViewModel.FlightsFields)flights.DepartureFieldID,
                                  FlightDestinationID = (FlightViewModel.FlightsFields)flights.DestinationID,
                                  FlightDepartureDate = flights.StartDate,
                                  ExpeditionID = exflights.ExpeditionID,
                                  RegisteredPassengers = exflights.NumberOfPassengers,
                                  AgencyID = Expeditions.AgencyID
                              }).ToList();

            if (_user.RuleEnum.Equals(eRole.Agent))
            {
                reportData = reportData.Where(e => e.AgencyID.Equals(_user.AgencyID)).ToList();
            }


            List<FlightsReportViewModel> flightsList = reportData.Select(r => new FlightsReportViewModel()
            {
                TripID = r.TripID,
                DepartureDate = r.DepartureDate,
                ReturnDate = r.ReturnDate,
                FlightID = r.FlightID,
                AirlineName = r.AirlineName,
                FlightDestenation = r.FlightDepartureFieldID.GetDescription(),
                FlightReturnField = r.FlightDestinationID.GetDescription(),
                FlightDate = r.FlightDepartureDate,
                TakeOffIsrael = r.TakeOffFromIsrael.Value ? ReportsStrings.Yes : ReportsStrings.No,
                NumberOfPassengers = r.NumberOfPassengers,
                ExpeditionID = r.ExpeditionID,
                RegisteredPassengers = r.RegisteredPassengers
            }).ToList();

            return flightsList;
        }

        [PermissionAuthorization(PermissionBitEnum.Reports_Flights)]
        [HttpPost]
        public ActionResult FlightsReport(FlightsReportMainViewModel model)
        {

            model.FlightsReportViewModels = GetFlights(model.FilterFromDate, model.FilterToDate);

            Session.Add("FlightsReport", model.FlightsReportViewModels);

            return View(model);
        }

        [PermissionAuthorization(PermissionBitEnum.Reports_Flights)]
        public ActionResult ExportFlightReport()
        {
            List<FlightsReportViewModel> reportdata = (List<FlightsReportViewModel>)Session["FlightsReport"];
            ExportToExcel<FlightsReportViewModel> exc = new ExportToExcel<FlightsReportViewModel>(reportdata, ReportsStrings.FlightsReport);
            var stream = exc.GetFile();
            string fileName = String.Format("{0} {1}.xlsx", ReportsStrings.FlightsReport, DateTime.Now.ToString("dd-MM-yyyy hh:ss"));
            string contentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";

            stream.Position = 0;
            return File(stream, contentType, fileName);
        }

        [PermissionAuthorization(PermissionBitEnum.Reports_BusesPerDay)]
        public ActionResult ExportBusesPerDayReport()
        {
            List<BusesPerDayViewModel> reportdata = (List<BusesPerDayViewModel>)Session["BusesPerDayReport"];
            ExportToExcel<BusesPerDayViewModel> exc = new ExportToExcel<BusesPerDayViewModel>(reportdata, ReportsStrings.BusesPerDayReport);
            var stream = exc.GetFile();
            string fileName = String.Format("{0} {1}.xlsx", ReportsStrings.BusesPerDayReport, DateTime.Now.ToString("dd-MM-yyyy hh:ss"));
            string contentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";

            stream.Position = 0;
            return File(stream, contentType, fileName);
        }
        
        [PermissionAuthorization(PermissionBitEnum.Reports_Schools)]
        [HttpGet]
        public ActionResult GetSchoolExpeditionParticipantionReport()
        {
            SchoolExpeditionReportContainerViewModel model = new SchoolExpeditionReportContainerViewModel();

            Session.Add("SchoolExpeditionPaticipantionReport", model.Schools);

            return View("SchoolExpeditionPaticipantionReport", model);
        }

        [PermissionAuthorization(PermissionBitEnum.Reports_Schools)]
        [HttpPost]
        public ActionResult GetSchoolExpeditionParticipantionReport(SchoolExpeditionReportContainerViewModel model)
        {
            model.Schools = _dbDapper.GetRecords<SchoolReportViewModel>(QueriesRepository.GetAllSchoolsReport, new { model.GroupID, model.InstituteID, model.StartDate, model.EndDate });
            Session.Add("SchoolExpeditionPaticipantionReport", model.Schools);

            return View("SchoolExpeditionPaticipantionReport", model);
        }

        [PermissionAuthorization(PermissionBitEnum.Reports_Schools)]
        public ActionResult ExportSchoolExpeditionParticipantionReport()
        {
            List<SchoolReportViewModel> reportData = (List<SchoolReportViewModel>)Session["SchoolExpeditionPaticipantionReport"];


            ExportToExcel <SchoolReportViewModel> exc = new ExportToExcel<SchoolReportViewModel>(reportData, ReportsStrings.SchoolReport);
            var stream = exc.GetFile();
            string fileName = String.Format("{0} {1}.xlsx", ReportsStrings.SchoolReport, DateTime.Now.ToString("dd-MM-yyyy hh:ss"));
            string contentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";

            stream.Position = 0;
            return File(stream, contentType, fileName);
        }

        [PermissionAuthorization(PermissionBitEnum.Reports_Flights)]
        public ActionResult ExportFLightAvailability()
        {
            FlightSeatsAvailabilityReportViewModel model = (FlightSeatsAvailabilityReportViewModel)Session["AvailableSeats"];
            List<RemainingSeatsInFLightsViewModel> reportdata = (List<RemainingSeatsInFLightsViewModel>)model.Data;
            ExportToExcel<RemainingSeatsInFLightsViewModel> exc = new ExportToExcel<RemainingSeatsInFLightsViewModel>(reportdata, ReportsStrings.AvailableSeatsReport);
            var stream = exc.GetFile();
            string fileName = String.Format("{0} {1} - {2}.xlsx", ReportsStrings.AvailableSeatsReport, model.StartDate.ToString("dd-MM-yyyy"), model.EndDate.ToString("dd-MM-yyyy"));
            string contentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";

            stream.Position = 0;
            return File(stream, contentType, fileName);
        }

        [PermissionAuthorization(PermissionBitEnum.Reports_ExportGroupsForSecurity)]
        public ActionResult ExpeditionCsv()
        {
            ExpeditionsCsvViewModel model = new ExpeditionsCsvViewModel();

            model.Expeditions = Mapper.Map<List<ExpeditionToCSVIntegration>, List<ExpeditionToCsvIntegrationViewModel>>(_dbDapper.GetExpeditionToCsvIntegration(model.FilterFromDate, model.FilterToDate).ToList());

            Session.Add("ExpeditionsCsv", model.Expeditions);

            return View("ExpeditionsToCsv", model);
        }

        [PermissionAuthorization(PermissionBitEnum.Reports_ExportGroupsForSecurity)]
        [HttpPost]
        public ActionResult ExpeditionCsv(ExpeditionsCsvViewModel model)
        {
            model.Expeditions = Mapper.Map<List<ExpeditionToCSVIntegration>, List<ExpeditionToCsvIntegrationViewModel>>(_dbDapper.GetExpeditionToCsvIntegration(model.FilterFromDate, model.FilterToDate).ToList());
            Session["ExpeditionsCsv"] = model.Expeditions;

            return View("ExpeditionsToCsv", model);
        }

        [PermissionAuthorization(PermissionBitEnum.Reports_ExportGroupsForSecurity)]
        public ActionResult ExportExpeditionsToCsv()
        {

            List<ExpeditionToCsvIntegrationViewModel> data = (List<ExpeditionToCsvIntegrationViewModel>)Session["ExpeditionsCsv"];

            //using (MemoryStream nms = new MemoryStream())
            //{
            //    using (TextWriter nsw = new StreamWriter(nms, Encoding.UTF8))
            //    using (CsvWriter csv = new CsvWriter(nsw))
            //    {
            //        csv.Configuration.RegisterClassMap<ExpeditionToCsvIntegrationViewModelMap>();
            //        csv.Configuration.HasHeaderRecord = true;

            //        csv.WriteHeader<ExpeditionToCsvIntegrationViewModel>();

            //        foreach (var record in data)
            //        {
            //            csv.WriteRecord(record);
            //        }

            //        //nsw.Flush();
            //        //nms.Seek(0, SeekOrigin.Begin);


            //    }

            //    string fileName = String.Format("{0} {1}.csv", ReportsStrings.ExpeditionsToCsv, DateTime.Now.ToString("dd-MM-yyyy hh:ss"));
            //    string contentType = "text/csv";

            //    return File(nms, contentType, fileName);
            //}

            MemoryStream nms = new MemoryStream();
            StreamWriter nsw = new StreamWriter(nms, Encoding.UTF8);
            var csv = new CsvWriter(nsw);
            csv.Configuration.RegisterClassMap<ExpeditionToCsvIntegrationViewModelMap>();
            csv.Configuration.HasHeaderRecord = true;
            //csv.Configuration.Encoding = Encoding.UTF8;

            csv.WriteHeader<ExpeditionToCsvIntegrationViewModel>();
            csv.NextRecord();
            csv.WriteRecords(data);

            nsw.Flush();
            nms.Seek(0, SeekOrigin.Begin);

            string fileName = String.Format("{0} {1}.csv", ReportsStrings.ExpeditionsToCsv, DateTime.Now.ToString("dd-MM-yyyy hh:ss"));
            string contentType = "text/csv";

            return File(nms, contentType, fileName);
        }

        [PermissionAuthorization(PermissionBitEnum.Reports_Groups)]
        [HttpGet]
        public ActionResult GetGroupsReport()
        {
            GroupsReportViewModel model = new GroupsReportViewModel();

            if (User.IsInRole(eRole.Agent.ToString()))
            {
                model.AgencyID = _dbDapper.GetAgencyIDByUserID(FunctionHelpers.User.UserID);
            }

            model.Agencies.Items =
                _dbDapper.GetRecords<Agencies>(QueriesRepository.GetAllAgencies, null)
                    .Select(a => new CustomSelectListItem()
                    {
                        ID = a.ID,
                        Name = a.Name
                    }).ToList();

            model.Items = _dbDapper.GetRecords<GroupsReportItemViewModel>(QueriesRepository.GetGroupsReport, new
            {
                model.AgencyID,
                model.StartDate,
                model.EndDate,
                model.DepartureID,
                model.ReturningField
            });

            Session.Add("GroupsReport", model.Items);

            return View("GroupsReport", model);
        }

        [PermissionAuthorization(PermissionBitEnum.Reports_Groups)]
        [HttpPost]
        public ActionResult GetGroupsReport(GroupsReportViewModel model)
        {
            if (User.IsInRole(eRole.Agent.ToString()))
            {
                model.AgencyID = _dbDapper.GetAgencyIDByUserID(FunctionHelpers.User.UserID);
            }

            model.Agencies.Items =
                _dbDapper.GetRecords<Agencies>(QueriesRepository.GetAllAgencies, null)
                    .Select(a => new CustomSelectListItem()
                    {
                        ID = a.ID,
                        Name = a.Name
                    }).ToList();

            model.Items = _dbDapper.GetRecords<GroupsReportItemViewModel>(QueriesRepository.GetGroupsReport, new
            {
                model.AgencyID,
                model.StartDate,
                model.EndDate,
                model.DepartureID,
                model.ReturningField
            });

            Session.Add("GroupsReport", model.Items);

            return View("GroupsReport", model);
        }

        [PermissionAuthorization(PermissionBitEnum.Reports_Groups)]
        public ActionResult ExportGroupsReport()
        {
            List<GroupsReportItemViewModel> reportdata = (List<GroupsReportItemViewModel>)Session["GroupsReport"];
            ExportToExcel<GroupsReportItemViewModel> exc = new ExportToExcel<GroupsReportItemViewModel>(reportdata, ReportsStrings.GroupsReport);
            var stream = exc.GetFile();
            string fileName = String.Format("{0} {1}.xlsx", ReportsStrings.GroupsReport, DateTime.Now.ToString("dd-MM-yyyy hh:ss"));
            string contentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";

            stream.Position = 0;
            return File(stream, contentType, fileName);
        }

        [PermissionAuthorization(PermissionBitEnum.Reports_TrainingParticipation)]
        [HttpGet]
        public ActionResult GetInstructorTrainings(bool? OpenFromTraining)
        {
            InstructorTrainingReportViewModel model = new InstructorTrainingReportViewModel();
            model.OpenedFromTraining = OpenFromTraining != null ? (bool)OpenFromTraining : false;
            model.InstructorRoles = _dbDapper.GetAllInstructorRoles(AppConfig.CurrentLangruageTwoLetters);

            model.TrainingName.Items =
                _dbDapper.GetRecords<CustomSelectListItem>(QueriesRepository.GetTrainingForDropDown,
                    new { model.TrainingCompleted });

            //model.Items =
            //_dbDapper.GetRecords<InstructorTrainingReportItemViewMode>(
            //    QueriesRepository.GetInstructorTrainingReport, new { model.TrainingType, model.OrgStatus, model.TrainingLocation, model.TrainingCompleted, model.InstructorType, model.TrainingID });
            Session.Add("InstructorTrainingReport", model.Items);

            return View("InstructorTrainingReport", model);
        }

        [PermissionAuthorization(PermissionBitEnum.Reports_TrainingParticipation)]
        [HttpPost]
        public ActionResult GetInstructorTrainings(InstructorTrainingReportViewModel model)
        {
            model.InstructorRoles = _dbDapper.GetAllInstructorRoles(AppConfig.CurrentLangruageTwoLetters);

            model.TrainingName.Items =
                _dbDapper.GetRecords<CustomSelectListItem>(QueriesRepository.GetTrainingForDropDown,
                    new { model.TrainingCompleted });

            model.Items =
                _dbDapper.GetRecords<InstructorTrainingReportItemViewMode>(
                    QueriesRepository.GetInstructorTrainingReport, new { model.TrainingType, model.OrgStatus, model.TrainingLocation, model.TrainingCompleted, model.InstructorType, model.TrainingID });
            Session.Add("InstructorTrainingReport", model.Items);

            return View("InstructorTrainingReport", model);
        }

        [PermissionAuthorization(PermissionBitEnum.Reports_TrainingParticipation)]
        [HttpGet]
        public ActionResult ExportInstructorTrainings()
        {
            List<InstructorTrainingReportItemViewMode> reportdata = (List<InstructorTrainingReportItemViewMode>)Session["InstructorTrainingReport"];
            ExportToExcel<InstructorTrainingReportItemViewMode> exc = new ExportToExcel<InstructorTrainingReportItemViewMode>(reportdata, ReportsStrings.InstructorTrainingReport);
            var stream = exc.GetFile();
            string fileName = String.Format("{0} {1}.xlsx", ReportsStrings.InstructorTrainingReport, DateTime.Now.ToString("dd-MM-yyyy hh:ss"));
            string contentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";

            stream.Position = 0;
            return File(stream, contentType, fileName);
        }

        [PermissionAuthorization(PermissionBitEnum.Reports_ContactInUrgency)]
        [HttpGet]
        public ActionResult GetEmergencyContactReport()
        {
            EmerhencyContactReportViewModel model = new EmerhencyContactReportViewModel();

            model.Items = _dbDapper.GetRecords<EmergencyContactReport>(QueriesRepository.GetEmergencyContactReportByDate, new { model.TargetDate });

            Session.Add("EmergencyContactReport", model.Items);

            return View("EmergencyContactReport", model);
        }

        [PermissionAuthorization(PermissionBitEnum.Reports_ContactInUrgency)]
        [HttpPost]
        public ActionResult GetEmergencyContactReport(EmerhencyContactReportViewModel model)
        {

            model.Items = _dbDapper.GetRecords<EmergencyContactReport>(QueriesRepository.GetEmergencyContactReportByDate, new { model.TargetDate });

            Session.Add("EmergencyContactReport", model.Items);

            return View("EmergencyContactReport", model);
        }

        [PermissionAuthorization(PermissionBitEnum.Reports_ContactInUrgency)]
        [HttpGet]
        public ActionResult ExportEmergencyContactReport()
        {
            List<EmergencyContactReport> reportdata = (List<EmergencyContactReport>)Session["EmergencyContactReport"];
            ExportToExcel<EmergencyContactReport> exc = new ExportToExcel<EmergencyContactReport>(reportdata, ReportsStrings.EmerhencyContactReport);
            var stream = exc.GetFile();
            string fileName = String.Format("{0} {1}.xlsx", ReportsStrings.InstructorTrainingReport, DateTime.Now.ToString("dd-MM-yyyy hh:ss"));
            string contentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";

            stream.Position = 0;
            return File(stream, contentType, fileName);
        }

        [PermissionAuthorization(PermissionBitEnum.Reports_PresentsOnTrip)]
        [HttpGet]
        public ActionResult GetStudentsInExpedition()
        {
            StudentsInExpeditionViewReport model = new StudentsInExpeditionViewReport();

            // model.Items = _dbDapper.GetRecords<StudentsInExpeditionReport>(QueriesRepository.GetStudentsInExpedition, new { model.StartDate, model.EndDate }).ToList();
            // model.Items.AddRange(_dbDapper.GetRecords<StudentsInExpeditionReport>(QueriesRepository.GetInstructorsInExpedition, new { model.StartDate, model.EndDate }).ToList());
            // model.Items = model.Items.Distinct().OrderBy(m => m.GroupExternalID).ToList();

            Session.Add("StudentsInExpeditionReport", model.Items);

            return View("StudentsInExpeditionReport", model);

        }

        [PermissionAuthorization(PermissionBitEnum.Reports_PresentsOnTrip)]
        [HttpPost]
        public ActionResult GetStudentsInExpedition(StudentsInExpeditionViewReport model)
        {
            model.Items = _dbDapper.GetRecords<StudentsInExpeditionReport>(QueriesRepository.GetStudentsInExpedition, new { model.StartDate, model.EndDate, model.ParticipantID }).ToList();
            model.Items.AddRange(_dbDapper.GetRecords<StudentsInExpeditionReport>(QueriesRepository.GetInstructorsInExpedition, new { model.StartDate, model.EndDate, model.ParticipantID }).ToList());

            model.Items = model.Items.Distinct().OrderBy(m => m.GroupExternalID).ToList();

            // Set Participant Name Filter
            if (!String.IsNullOrEmpty(model.ParticipantName))
            {

                model.Items = model.Items.Where(p => p.FullName.Contains(model.ParticipantName)).ToList();
            }

            Session.Add("StudentsInExpeditionReport", model.Items);

            return View("StudentsInExpeditionReport", model);

        }

        [HttpGet]
        public ActionResult ExportStudentsInExpeditionReport()
        {
            List<StudentsInExpeditionReport> reportdata = (List<StudentsInExpeditionReport>)Session["StudentsInExpeditionReport"];
            ExportToExcel<StudentsInExpeditionReport> exc = new ExportToExcel<StudentsInExpeditionReport>(reportdata, ReportsStrings.StudentInExpeditionReport);
            var stream = exc.GetFile();
            string fileName = String.Format("{0} {1}.xlsx", ReportsStrings.StudentInExpeditionReport, DateTime.Now.ToString("dd-MM-yyyy hh:ss"));
            string contentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";

            stream.Position = 0;
            return File(stream, contentType, fileName);
        }

        [PermissionAuthorization(PermissionBitEnum.Reports_GroupAndDeputyManagers)]
        [HttpGet]
        public ActionResult GetGroupManagerReport()
        {
            GroupManagerReportViewModel model = new GroupManagerReportViewModel();

            model.Items = _dbDapper.GetRecords<GroupManagersReportItem>(QueriesRepository.GetGroupManagersReport, new { model.StartDate, model.EndDate, model.GroupExternalID, model.DepartureFieldID });
            Session.Add("GroupManagersreport", model.Items);

            return View("GetGroupManagers", model);
        }

        [PermissionAuthorization(PermissionBitEnum.Reports_GroupAndDeputyManagers)]
        [HttpPost]
        public ActionResult GetGroupManagerReport(GroupManagerReportViewModel model)
        {

            model.Items = _dbDapper.GetRecords<GroupManagersReportItem>(QueriesRepository.GetGroupManagersReport, new { model.StartDate, model.EndDate, model.GroupExternalID, model.DepartureFieldID });
            Session.Add("GroupManagersreport", model.Items);

            return View("GetGroupManagers", model);
        }

        [PermissionAuthorization(PermissionBitEnum.Reports_GroupAndDeputyManagers)]
        public ActionResult ExportGroupManagersReport()
        {
            List<GroupManagersReportItem> reportdata = (List<GroupManagersReportItem>)Session["GroupManagersreport"];
            ExportToExcel<GroupManagersReportItem> exc = new ExportToExcel<GroupManagersReportItem>(reportdata, ReportsStrings.GroupManagersReport);
            var stream = exc.GetFile();
            string fileName = String.Format("{0} {1}.xlsx", ReportsStrings.GroupManagersReport, DateTime.Now.ToString("dd-MM-yyyy hh:ss"));
            string contentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";

            stream.Position = 0;
            return File(stream, contentType, fileName);
        }

        [PermissionAuthorization(PermissionBitEnum.Reports_SchoolGlobalRemarks)]
        [HttpGet]
        public ActionResult GetSchoolGlobalRemarks()
        {
            SchoolGlobalRemarkReportViewModel model = new SchoolGlobalRemarkReportViewModel();

            model.Items = _dbDapper.GetRecords<SchoolGlobalRemarksViewModel>(QueriesRepository.GetSchoolGlobalRemarkReport, null);

            Session.Add("SchoolGlobalRemarks", model.Items);

            return View("GetSchoolGlobalRemarks", model);
        }

        [PermissionAuthorization(PermissionBitEnum.Reports_SchoolGlobalRemarks)]
        public ActionResult ExportSchoolGlobalRemarks()
        {
            List<SchoolGlobalRemarksViewModel> reportdata = (List<SchoolGlobalRemarksViewModel>)Session["SchoolGlobalRemarks"];
            ExportToExcel<SchoolGlobalRemarksViewModel> exc = new ExportToExcel<SchoolGlobalRemarksViewModel>(reportdata, ReportsStrings.SchoolGlobalRemarksReportTitle);
            var stream = exc.GetFile();
            string fileName = String.Format("{0} {1}.xlsx", ReportsStrings.SchoolGlobalRemarksReportTitle, DateTime.Now.ToString("dd-MM-yyyy hh:ss"));
            string contentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";

            stream.Position = 0;
            return File(stream, contentType, fileName);
        }

        [PermissionAuthorization(PermissionBitEnum.Reports_GroupsByRegion)]
        [HttpGet]
        public ActionResult GetGroupsByRegion()
        {
            GroupsByRegionsViewModel model = new GroupsByRegionsViewModel();

            Session.Add("GroupsByRegions", model.Items);

            return View("GetGroupsByRegion", model);
        }

        [PermissionAuthorization(PermissionBitEnum.Reports_GroupsByRegion)]
        [HttpPost]
        public ActionResult GetGroupsByRegion(GroupsByRegionsViewModel model)
        {

            // Check the user
            if (User.IsInRole(eRole.SchoolManager.ToString()))
            {
                model.InstituteID = _dbDapper.GetRecords<int>(QueriesRepository.GetInstiuteIDByUserID, FunctionHelpers.User.UserID).FirstOrDefault();
            }
            else if (User.IsInRole(eRole.CountyManager.ToString()))
            {
                model.Region = (eSchoolRegion)FunctionHelpers.User.RegionID;
            }

            model.Items = _dbDapper.GetRecords<GroupsByRegionItem>(QueriesRepository.GroupsByRegionReport, new { model.StartDate, model.EndDate, model.Region, model.InstituteID });

            Session.Add("GroupsByRegions", model.Items);

            return View("GetGroupsByRegion", model);
        }

        [PermissionAuthorization(PermissionBitEnum.Reports_GroupsByRegion)]
        public ActionResult ExportGetGroupsByRegion()
        {
            List<GroupsByRegionItem> reportdata = (List<GroupsByRegionItem>)Session["GroupsByRegions"];
            ExportToExcel<GroupsByRegionItem> exc = new ExportToExcel<GroupsByRegionItem>(reportdata, ReportsStrings.GroupsByRegionTitle);
            var stream = exc.GetFile();
            string fileName = String.Format("{0} {1}.xlsx", ReportsStrings.GroupsByRegionTitle, DateTime.Now.ToString("dd-MM-yyyy hh:ss"));
            string contentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";

            stream.Position = 0;
            return File(stream, contentType, fileName);
        }


        [PermissionAuthorization(PermissionBitEnum.Reports_InstructorsAssignmentsForGroups)]
        [HttpGet]
        public ActionResult GetInstructorsAssignmentsForGroups(bool? OpenFromTraining)
        {
            InstructorAssignmentsInGroupsViewModel model = new InstructorAssignmentsInGroupsViewModel();
            model.OpenedFromTraining = OpenFromTraining != null ? (bool)OpenFromTraining : false;

            //model.Roles.Items = _dbDapper.GetRecords<CustomSelectListItem>(QueriesRepository.GetInstructorRoles, new { Language = "he" });

            //Session.Add("InstructorsAssignmentsForGroups", model.Items);

            return View("InstructorsAssignmentsForGroups", model);
        }

        [PermissionAuthorization(PermissionBitEnum.Reports_InstructorsAssignmentsForGroups)]
        [HttpPost]
        public ActionResult GetInstructorsAssignmentsForGroups(InstructorAssignmentsInGroupsViewModel model)
        {

            // Check the user
            if (User.IsInRole(eRole.CountyManager.ToString()))
            {
                model.Region = (eSchoolRegion)FunctionHelpers.User.RegionID;
            }


            model.Items = _dbDapper.GetRecords<InstructorAssignmentsInGroupsItem>(QueriesRepository.GetInstructorsAssignmentsForGroups, new { model.StartDate, model.EndDate, model.Region, model.GroupExternalID, Roles = model.SelectedRoles, model.InstructorID, model.CountRoles });

            Session.Add("InstructorsAssignmentsForGroups", model.Items);

            return View("InstructorsAssignmentsForGroups", model);
        }

        [PermissionAuthorization(PermissionBitEnum.Reports_InstructorsAssignmentsForGroups)]
        public ActionResult ExportInstructorsAssignmentsForGroups()
        {
            List<InstructorAssignmentsInGroupsItem> reportdata = (List<InstructorAssignmentsInGroupsItem>)Session["InstructorsAssignmentsForGroups"];
            ExportToExcel<InstructorAssignmentsInGroupsItem> exc = new ExportToExcel<InstructorAssignmentsInGroupsItem>(reportdata, ReportsStrings.InstructorsAssignmentsForGroupsTitle);
            var stream = exc.GetFile();
            string fileName = String.Format("{0} {1}.xlsx", ReportsStrings.InstructorsAssignmentsForGroupsTitle, DateTime.Now.ToString("dd-MM-yyyy hh:ss"));
            string contentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";

            stream.Position = 0;
            return File(stream, contentType, fileName);
        }


        #region Expedition Status Report

        [PermissionAuthorization(PermissionBitEnum.Reports_Groups)]
        [HttpGet]
        public ActionResult GetExpeditionStatusReport()
        {
            ExpeditionReportViewModel model = new ExpeditionReportViewModel();

            model.Items = _dbDapper.GetRecords<ExpeditionItemReportViewModel>(QueriesRepository.GetExpeditionStatusReport, new
            {
                model.StartDate,
                model.EndDate,
                model.StatusId
            });

            Session.Add("ExpeditionStatusReport", model.Items);

            return View("ExpeditionStatusReport", model);
        }

        [PermissionAuthorization(PermissionBitEnum.Reports_Groups)]
        [HttpPost]
        public ActionResult GetExpeditionStatusReport(ExpeditionReportViewModel model)
        {

            model.Items = _dbDapper.GetRecords<ExpeditionItemReportViewModel>(QueriesRepository.GetExpeditionStatusReport, new
            {
                model.StartDate,
                model.EndDate,
                model.StatusId
            });

            Session.Add("ExpeditionStatusReport", model.Items);

            return View("ExpeditionStatusReport", model);
        }

        [PermissionAuthorization(PermissionBitEnum.Reports_Groups)]
        public ActionResult ExportExpeditionStatusReport()
        {
            List<ExpeditionItemReportViewModel> reportdata = (List<ExpeditionItemReportViewModel>)Session["ExpeditionStatusReport"];
            ExportToExcel<ExpeditionItemReportViewModel> exc = new ExportToExcel<ExpeditionItemReportViewModel>(reportdata, ReportsStrings.ExpeditionStatusReport);
            var stream = exc.GetFile();
            string fileName = String.Format("{0} {1}.xlsx", ReportsStrings.ExpeditionStatusReport, DateTime.Now.ToString("dd-MM-yyyy hh:ss"));
            string contentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";

            stream.Position = 0;
            return File(stream, contentType, fileName);
        }

        #endregion


    }
}