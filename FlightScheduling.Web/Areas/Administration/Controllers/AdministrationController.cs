﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using Flight.DAL.FlightDB.Methods;
using FlightScheduling.DAL;
using FlightScheduling.DAL.CustomEntities;
using FlightScheduling.Web.Areas.Administration.Models.Administration;
using FlightScheduling.Web.Areas.Administration.Models.Instructors;
using Otakim.Website.Infrastructure;
using FlightScheduling.Web.Infrastructure;
using FlightScheduling.DAL.CustomEntities.Notifications;
using FlightScheduling.Web.Areas.Administration.Models.Common;
using FlightScheduling.Language;
using FlightScheduling.DAL.CustomEntities.Permissions;
using System.Web.Caching;
using FlightScheduling.Web.App_Start;

namespace FlightScheduling.Web.Areas.Administration.Controllers
{
    [Authorize]
    public class AdministrationController : Controller
    {
        DBRepository _dbDapper = new DBRepository();

        // GET: Administration/Administration

        [EndiloAuthorization(eRole.Administrator)]
        [HttpGet]
        public ActionResult UpdateSystemConfiguration()
        {

            SystemConfigurationViewModel model = new SystemConfigurationViewModel();

            model.Model =
                Mapper.Map<List<SystemConfiguration>, List<SystemConfigurationItemViewModel>>(
                    _dbDapper.GetSystemConfiguraion().ToList());


            return View("UpdateSystemConfiguration", model);
        }

        [EndiloAuthorization(eRole.Administrator)]
        [HttpPost]
        public ActionResult UpdateSystemConfiguration(SystemConfigurationViewModel model)
        {
            List<SystemConfiguration> data = model.Model.Select(d => new SystemConfiguration()
            {
                PropertyID = d.PropertyID,
                PropertyValue = d.PropertyValue
            }).ToList();

            int results = _dbDapper.UpdateSystemConfigurationProperties(data);

            SystemConfigurationConfig.SetSystemConfigurationSettings(true);

            return RedirectToAction("UpdateSystemConfiguration");
        }

        [EndiloAuthorization(eRole.Administrator)]
        [HttpGet]
        public ActionResult GetInstructorRoles()
        {
            InstructorRolesViewModel model = new InstructorRolesViewModel();
            model.Roles =
                Mapper.Map<List<InstructorRoles>, List<InstructorRoleViewModel>>(_dbDapper.GetAllInstructorRoles(AppConfig.CurrentLangruageTwoLetters).ToList());

            return View(model);
        }

        [EndiloAuthorization(eRole.Administrator)]
        [HttpGet]
        public PartialViewResult AddInstructorRole()
        {
            InstructorRoleViewModel model = new InstructorRoleViewModel();
            model.IsUpdate = false;

            return PartialView("_ManageInstructorRole", model);
        }

        [EndiloAuthorization(eRole.Administrator)]
        [HttpPost]
        public ActionResult AddInstructorRole(InstructorRoleViewModel model)
        {
            if (ModelState.IsValid)
            {
                InstructorRoles data = Mapper.Map<InstructorRoleViewModel, InstructorRoles>(model);
                data.Language = AppConfig.CurrentLangruageTwoLetters;
                bool results = false;
                if (model.IsUpdate)
                {
                    results = _dbDapper.UpdateInstructorRole(data);
                }
                else
                {
                    results = _dbDapper.InsertInstructorRole(data);
                }


                return RedirectToAction("GetInstructorRoles");
            }

            return RedirectToAction("GetInstructorRoles");
        }

        [EndiloAuthorization(eRole.Administrator)]
        [HttpGet]
        public PartialViewResult UpdateInstructorRole(int ID)
        {
            InstructorRoleViewModel model = Mapper.Map<InstructorRoles, InstructorRoleViewModel>(_dbDapper.GetAllInstructorRole(ID));

            model.IsUpdate = true;

            return PartialView("_ManageInstructorRole", model);
        }

        [EndiloAuthorization(eRole.Administrator)]
        [HttpGet]
        public ActionResult GetAllNotificationTemplates()
        {
            NotificationsTemplatesViewModel model = new NotificationsTemplatesViewModel();
            string[] param = null;
            model.Notifications = Mapper.Map<List<NotificationTemplate>, List<NotificationTemplateViewModel>>(_dbDapper.GetRecords<NotificationTemplate>(QueriesRepository.GetAllNotificationTemplage, param));

            return View("NotificationTemplates", model);
        }

        [EndiloAuthorization(eRole.Administrator)]
        [HttpGet]
        [ActionName("UpdateNotificationTemplate")]
        public ActionResult AddNotificationTemplate(eNotificationTriggerType NotificationType, int? ID = null)
        {
            NotificationTemplateViewModel model = new NotificationTemplateViewModel();
            model.NotificationTriggerType = NotificationType;

            if (ID != null)
            {
                model = Mapper.Map<NotificationTemplate, NotificationTemplateViewModel>(_dbDapper.GetRecords<NotificationTemplate>(QueriesRepository.GetNotificationTempalteByID, new { ID }).SingleOrDefault());
                model.IsUpdate = true;
            }

            // Get all permission roles
            model.UserPermissionRoles.Items = _dbDapper.GetRecords<PermissionRole>(QueriesRepository.GetPermissionRoles, null).Select(r => new CustomSelectListItem()
            {
                ID = r.ID,
                Name = r.Name
            }).ToList();
            model.NotificationTriggerType = NotificationType;
            model.SetCustomProperties();
            return View("UpdateNotificationTemplate", model);
        }

        [EndiloAuthorization(eRole.Administrator)]
        [HttpPost]
        [ActionName("UpdateNotificationTemplate")]
        public ActionResult AddNotificationTemplate(NotificationTemplateViewModel model)
        {
            if (ModelState.IsValid)
            {
                NotificationTemplate _template = Mapper.Map<NotificationTemplateViewModel, NotificationTemplate>(model);
                bool results = false;

                if (model.IsUpdate)
                {
                    results = _dbDapper.ExecuteCommand(QueriesRepository.UpdateNotificationTemplate, _template);
                }
                else
                {
                    string[] param = null;
                    results = _dbDapper.ExecuteCommand(QueriesRepository.AddNotificationTemplate, _template);
                }

                if (results)
                {
                    return RedirectToAction("GetAllNotificationTemplates");
                }
            }

            return View("UpdateNotificationTemplate", model);

        }

        [EndiloAuthorization(eRole.Administrator)]
        [HttpGet]
        public ActionResult GetGroupPlanSubjects()
        {

            GroupPlanSubjectsViewModel model = new GroupPlanSubjectsViewModel();
            model.Subjects = _dbDapper.GetRecords<GroupPlanSubjectViewModel>(QueriesRepository.GetGroupPlanSubjects, null);

            return View("GetGroupPlanSubjects", model);
        }

        [EndiloAuthorization(eRole.Administrator)]
        [HttpGet]
        public PartialViewResult AddGroupPlan()
        {
            GroupPlanSubjectViewModel model = new GroupPlanSubjectViewModel();
            model.IsUpdate = false;

            return PartialView("_ManageGroupPlanSubject", model);
        }

        [EndiloAuthorization(eRole.Administrator)]
        [HttpGet]
        public PartialViewResult UpdateGroupSubject(int ID)
        {
            GroupPlanSubjectViewModel model = new GroupPlanSubjectViewModel();
            model = _dbDapper.GetRecords<GroupPlanSubjectViewModel>(QueriesRepository.GetGroupPlanSubjectByID, new { ID }).FirstOrDefault();
            model.IsUpdate = true;

            return PartialView("_ManageGroupPlanSubject", model);
        }

        [EndiloAuthorization(eRole.Administrator)]
        [HttpPost]
        public ActionResult ManageGroupPlanSubject(GroupPlanSubjectViewModel model)
        {
            if (ModelState.IsValid)
            {
                bool results = false;
                if (model.IsUpdate)
                {
                    model.UpdateDate = DateTime.Now
;
                    results = _dbDapper.UpdateOperation(QueriesRepository.UpdateGroupPlanSubject, new
                    {
                        model.Name,
                        model.HoursRequired,
                        model.CreationDate,
                        model.UpdateDate,
                        model.ID
                    });
                }
                else
                {
                    model.CreationDate = DateTime.Now;
                    model.UpdateDate = DateTime.Now;
                    results = _dbDapper.UpdateOperation(QueriesRepository.AddGroupPlanSubject, new
                    {
                        model.Name,
                        model.HoursRequired,
                        model.CreationDate,
                        model.UpdateDate
                    });
                }

                return RedirectToAction("GetGroupPlanSubjects");
            }

            return PartialView("_ManageGroupPlanSubject", model);
        }

        [EndiloAuthorization(eRole.Administrator)]
        [HttpGet]
        public PartialViewResult DeleteGroupSubject(int ID, string title)
        {
            DeleteViewModel model = new DeleteViewModel();
            model.ID = ID;
            model.IsDeletable = true;
            model.Title = title;
            model.Action = "DeleteGroupSubject";
            model.Controller = "Administration";
            model.Header = AdministrationStrings.DeleteGroupSubject;

            return PartialView("_DeleteItem", model);
        }

        [EndiloAuthorization(eRole.Administrator)]
        [HttpPost]
        public ActionResult DeleteGroupSubject(DeleteViewModel model)
        {
            var results = _dbDapper.UpdateOperation(QueriesRepository.DeleteGroupPlanSubject, new { model.ID });
            return RedirectToAction("GetGroupPlanSubjects");
        }

        [PermissionAuthorization(PermissionBitEnum.Administration_Sites_View)]
        [HttpGet]
        public ActionResult GetSites()
        {
            SitesViewModel model = new SitesViewModel();

            model.Cities.Items = _dbDapper.GetRecords<CityViewModel>(QueriesRepository.GetAllCities, null).Select(c => new CustomSelectListItem()
            {
                ID = c.CityID,
                Name = c.CityName,
                Group = String.Format("{0} - {1}", c.DistrictName, c.RegionName)
            }).ToList();

            return View("GetSites", model);
        }

        [PermissionAuthorization(PermissionBitEnum.Administration_Sites_View)]
        [HttpPost]
        public ActionResult GetSites(SitesViewModel model)
        {

            model.Sites = _dbDapper.GetRecords<Site>(QueriesRepository.GetAllSites, new
            {
                model.SiteName,
                model.SelectedCities,
                model.SelectedSiteTypes
            }).Select(s => new SiteViewModel()
            {
                ID = s.ID,
                Name = s.Name,
                Address = s.Address,
                CityID = s.CityID,
                CreationDate = s.CreationDate,
                ExternalID = s.ExternalID,
                IsActive = s.IsActive,
                Limitations = s.Limitations,
                SiteType = s.SiteType,
                UpdateDate = s.UpdateDate,
                City = new CityViewModel()
                {
                    CityID = s.CityID,
                    CityExternalID = s.CityExternalID,
                    CityName = s.CityName,
                    DistrictID = s.DistrictID,
                    DistrictName = s.DistrictName,
                    RegionID = s.RegionID,
                    RegionName = s.RegionName
                }
            }).ToList();

            model.Cities.Items = _dbDapper.GetRecords<CityViewModel>(QueriesRepository.GetAllCities, null).Select(c => new CustomSelectListItem()
            {
                ID = c.CityID,
                Name = c.CityName,
                Group = String.Format("{0} - {1}", c.DistrictName, c.RegionName)
            }).ToList();

            return View("GetSites", model);
        }

        [PermissionAuthorization(PermissionBitEnum.Administration_Sites_Edit)]
        [HttpGet]
        public PartialViewResult AddSite()
        {
            SiteViewModel model = new SiteViewModel();
            model.Cities.Items = _dbDapper.GetRecords<CityViewModel>(QueriesRepository.GetAllCities, null).Select(c => new CustomSelectListItem()
            {
                ID = c.CityID,
                Name = c.CityName,
                Group = String.Format("{0} - {1}", c.DistrictName, c.RegionName)
            }).ToList();

            return PartialView("_AddSite", model);
        }

        [PermissionAuthorization(PermissionBitEnum.Administration_Sites_Edit)]
        [HttpGet]
        public PartialViewResult UpdateSite(int Id)
        {
            SiteViewModel model = new SiteViewModel();
            model = _dbDapper.GetRecords<SiteViewModel>(QueriesRepository.GetSiteById, new { ID = Id }).FirstOrDefault();
            model.Cities.Items = _dbDapper.GetRecords<CityViewModel>(QueriesRepository.GetAllCities, null).Select(c => new CustomSelectListItem()
            {
                ID = c.CityID,
                Name = c.CityName,
                Group = String.Format("{0} - {1}", c.DistrictName, c.RegionName)
            }).ToList();
            model.IsUpdate = true;
            return PartialView("_AddSite", model);
        }


        [PermissionAuthorization(PermissionBitEnum.Administration_Sites_Edit)]
        [HttpPost]
        public ActionResult ManageSite(SiteViewModel model)
        {
            bool result = false;
            if (ModelState.IsValid)
            {
                if (model.IsUpdate)
                {
                    model.UpdateDate = DateTime.Now;
                    result = _dbDapper.UpdateOperation(QueriesRepository.UpdateSite, new
                    {
                        model.SiteType,
                        model.ExternalID,
                        model.Name,
                        model.LocalName,
                        model.CityID,
                        model.Address,
                        model.IsActive,
                        model.Limitations,
                        model.UpdateDate,
                        model.ID
                    });
                }
                else
                {
                    model.CreationDate = DateTime.Now;
                    model.UpdateDate = model.CreationDate;
                    model.IsActive = true;
                    result = _dbDapper.ExecuteCommand(QueriesRepository.AddSite, new
                    {
                        model.SiteType,
                        model.ExternalID,
                        model.Name,
                        model.LocalName,
                        model.CityID,
                        model.Address,
                        model.IsActive,
                        model.Limitations,
                        model.UpdateDate,
                        model.CreationDate
                    });
                }
            }

            return RedirectToAction("GetSites");

        }

        [PermissionAuthorization(PermissionBitEnum.Administration_Sites_Delete)]
        [HttpGet]
        public PartialViewResult DeleteSite(int ID, string title)
        {
            DeleteViewModel model = new DeleteViewModel();
            model.ID = ID;
            model.IsDeletable = true;
            model.Title = title;
            model.Action = "DeleteSite";
            model.Controller = "Administration";
            model.Header = AdministrationStrings.DeleteGroupSubject;

            return PartialView("_DeleteItem", model);
        }

        [PermissionAuthorization(PermissionBitEnum.Administration_Sites_Delete)]
        [HttpPost]
        public ActionResult DeleteSite(DeleteViewModel model)
        {
            var results = _dbDapper.UpdateOperation(QueriesRepository.DeleteSite, new { model.ID });
            return RedirectToAction("GetSites");
        }

        [EndiloAuthorization(eRole.Administrator)]
        [HttpGet]
        public ActionResult CreatePermissionRole()
        {
            PermissionRoleViewModel model = new PermissionRoleViewModel();
            model.LoadPotentialPermission();

            return View("ManagePermissionRole", model);
        }


        [EndiloAuthorization(eRole.Administrator)]
        [HttpGet]
        public ActionResult UpdatePermissionRole(int RoleID)
        {
            PermissionRoleViewModel model = new PermissionRoleViewModel();
            PermissionRole role = _dbDapper.GetPermissionsRole(RoleID);

            model.ID = role.ID;
            model.Name = role.Name;
            model.LoadPotentialPermission();

            foreach (PermissionBitViewModel permission in model.Permissions)
            {
                if (role.Permissions.Any(p => p.PermissionBitEnum == permission.PermissionBitEnum))
                {
                    permission.Enabled = true;
                }
            }

            model.IsUpdate = true;
            return View("ManagePermissionRole", model);
        }

        [EndiloAuthorization(eRole.Administrator)]
        [HttpPost]
        public ActionResult CreatePermissionRole(PermissionRoleViewModel model)
        {
            if (model.IsUpdate)
            {
                PermissionRole _permissionRole = new PermissionRole();
                _permissionRole.ID = model.ID;
                _permissionRole.Name = model.Name;
                _permissionRole.UpdateDate = DateTime.Now;

                IEnumerable<PermissionBits> _permissions = model.Permissions.Where(p => p.Enabled == true).Select(p => new PermissionBits()
                {
                    PermissionBit = p.PermissionBitID,
                    CreationDate = DateTime.Now,
                    UpdateDate = DateTime.Now
                }).AsEnumerable();

                bool results = _dbDapper.UpdatePermissionRole(_permissionRole, _permissions);
            }
            else
            {
                PermissionRole _permissionRole = new PermissionRole();
                _permissionRole.Name = model.Name;
                _permissionRole.CreationDate = DateTime.Now;
                _permissionRole.UpdateDate = _permissionRole.CreationDate;

                IEnumerable<PermissionBits> _permissions = model.Permissions.Where(p => p.Enabled == true).Select(p => new PermissionBits()
                {
                    PermissionBit = p.PermissionBitID,
                    CreationDate = DateTime.Now,
                    UpdateDate = DateTime.Now
                }).AsEnumerable();

                bool results = _dbDapper.InsertPermissionRole(_permissionRole, _permissions);

                if (results)
                {
                    IEnumerable<PermissionRole> Roles = _dbDapper.GetPermissionsRoles();
                    HttpContext.Application["Roles"] = Roles;
                }
            }
            // Update Application Permission catch
            PermissionLoader.LoadRolesAndPermissions();
            return RedirectToAction("GetPermissionRoles");
        }

        [EndiloAuthorization(eRole.Administrator)]
        [HttpGet]
        public ActionResult GetPermissionRoles()
        {
            List<PermissionRoleViewModel> model = new List<PermissionRoleViewModel>();
            IEnumerable<PermissionRole> roles = _dbDapper.GetPermissionsRoles();

            foreach (var role in roles)
            {
                PermissionRoleViewModel _Role = new PermissionRoleViewModel();
                _Role.ID = role.ID;
                _Role.Name = role.Name;
                _Role.CreationDate = role.CreationDate;
                _Role.UpdateDate = role.UpdateDate;
                _Role.LoadPotentialPermission();

                foreach (PermissionBitViewModel _pb in _Role.Permissions)
                {
                    if (role.Permissions.Any(p => p.PermissionBitEnum == _pb.PermissionBitEnum))
                    {
                        _pb.Enabled = true;
                    }
                }

                model.Add(_Role);
            }

            return View("GetPermissionRoles", model);
        }

        #region Cities
        [PermissionAuthorization(PermissionBitEnum.Administration_City_View)]
        [HttpGet]
        public ActionResult Cities()
        {
            CitiesViewModel model = new CitiesViewModel();
            // model.Cities = _dbDapper.GetRecords<CityViewModel>(QueriesRepository.GetCitiesWithFilters, null).OrderBy(c => c.CityName);

            //Load filters
            model.Districts.Items = _dbDapper.GetRecords<dynamic>(QueriesRepository.GetDistricts, null).Select(d => new CustomSelectListItem
            {
                ID = d.ID,
                Name = d.Name
            }).ToList();

            model.Regions.Items = _dbDapper.GetRecords<dynamic>(QueriesRepository.GetRegions, null).Select(d => new CustomSelectListItem
            {
                ID = d.ID,
                Name = d.Name
            }).ToList();

            return View("Cities", model);
        }

        [PermissionAuthorization(PermissionBitEnum.Administration_City_View)]
        [HttpPost]
        public ActionResult Cities(CitiesViewModel model)
        {
            model.Cities = _dbDapper.GetRecords<CityViewModel>(QueriesRepository.GetCitiesWithFilters, new
            {
                model.CityName,
                model.SelectedRegions,
                model.SelectedDistricts
            }).OrderBy(c => c.CityName);

            //Load filters
            model.Districts.Items = _dbDapper.GetRecords<dynamic>(QueriesRepository.GetDistricts, null).Select(d => new CustomSelectListItem
            {
                ID = d.ID,
                Name = d.Name
            }).ToList();

            model.Regions.Items = _dbDapper.GetRecords<dynamic>(QueriesRepository.GetRegions, null).Select(d => new CustomSelectListItem
            {
                ID = d.ID,
                Name = d.Name
            }).ToList();

            return View("Cities", model);
        }

        [PermissionAuthorization(PermissionBitEnum.Administration_City_Edit)]
        [HttpGet]
        public PartialViewResult AddCity()
        {
            CityViewModel model = new CityViewModel();

            model.Districts.Items = _dbDapper.GetRecords<dynamic>(QueriesRepository.GetDistricts, null).Select(d => new CustomSelectListItem
            {
                ID = d.ID,
                Name = d.Name
            }).ToList();

            model.Regions.Items = _dbDapper.GetRecords<dynamic>(QueriesRepository.GetRegions, null).Select(d => new CustomSelectListItem
            {
                ID = d.ID,
                Name = d.Name
            }).ToList();

            return PartialView("_ManageCity", model);

        }

        [PermissionAuthorization(PermissionBitEnum.Administration_City_Edit)]
        [HttpGet]
        public PartialViewResult UpdateCity(int CityID)
        {
            CityViewModel model = new CityViewModel();
            model = _dbDapper.GetRecords<CityViewModel>(QueriesRepository.GetCityByID, new { CityID }).FirstOrDefault();

            model.Districts.Items = _dbDapper.GetRecords<dynamic>(QueriesRepository.GetDistricts, null).Select(d => new CustomSelectListItem
            {
                ID = d.ID,
                Name = d.Name
            }).ToList();

            model.Regions.Items = _dbDapper.GetRecords<dynamic>(QueriesRepository.GetRegions, null).Select(d => new CustomSelectListItem
            {
                ID = d.ID,
                Name = d.Name
            }).ToList();

            model.IsUpdate = true;

            return PartialView("_ManageCity", model);
        }

        [PermissionAuthorization(PermissionBitEnum.Administration_City_Edit)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ManageCity(CityViewModel model)
        {
            if (ModelState.IsValid)
            {
                if (model.IsUpdate)
                {
                    model.UpdateDate = DateTime.Now;
                    bool results = _dbDapper.UpdateOperation(QueriesRepository.UpdateCity, new
                    {
                        Name = model.CityName,
                        ExternalID = model.CityExternalID,
                        model.RegionID,
                        model.DistrictID,
                        model.UpdateDate,
                        model.CityID

                    });
                }
                else
                {
                    model.CreationDate = DateTime.Now;
                    model.UpdateDate = model.CreationDate;

                    int results = _dbDapper.InsertOperation(QueriesRepository.AddCity, new
                    {
                        Name = model.CityName,
                        ExternalID = model.CityExternalID,
                        model.RegionID,
                        model.DistrictID,
                        model.CreationDate,
                        model.UpdateDate
                    });
                }
            }

            return RedirectToAction("Cities");
        }


        #endregion


    }


}
