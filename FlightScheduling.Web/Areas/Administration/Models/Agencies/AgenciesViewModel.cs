﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using FlightScheduling.Language;
using FlightScheduling.Web.Areas.Administration.Models.Common;
using FlightScheduling.DAL;

namespace FlightScheduling.Web.Areas.Administration.Models.Agencies
{
    public class AgenciesViewModel
    {
        public List<AgencyViewModel> AgencyViewModels { get; set; }

        public AgenciesViewModel()
        {
            AgencyViewModels = new List<AgencyViewModel>();
        }
    }

    public class AgencyViewModel
    {
        [Display(ResourceType = typeof(AgenciesStrings), Name = "ID")]
        public int ID { get; set; }

        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(AgenciesStrings), Name = "Name")]
        public string Name { get; set; }

        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(AgenciesStrings), Name = "Phone")]
        public string Phone { get; set; }

        [Display(ResourceType = typeof(AgenciesStrings), Name = "Fax")]
        public string Fax { get; set; }

        [DataType(DataType.EmailAddress, ErrorMessageResourceType = typeof(AgenciesStrings), ErrorMessageResourceName = "Email")]
        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        public string Email { get; set; }

        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(AgenciesStrings), Name = "Manager")]
        public string Manager { get; set; }

        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(AgenciesStrings), Name = "ManagerPhone")]
        public string ManagerPhone { get; set; }

        [Display(ResourceType = typeof(AgenciesStrings), Name = "IsActive")]
        public bool IsActive { get; set; }
    }

    public class DeleteAgencyViewModel : DeleteViewModel
    {
        public List<FlightScheduling.DAL.Users> Users { get; set; }

        public DeleteAgencyViewModel()
        {
            Users = new List<DAL.Users>();
        }
    }

}