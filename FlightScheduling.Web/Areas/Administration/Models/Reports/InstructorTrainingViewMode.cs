﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Endilo.ExportToExcel;
using FlightScheduling.DAL;
using FlightScheduling.Language;
using FlightScheduling.Web.Areas.Administration.Models.Common;

namespace FlightScheduling.Web.Areas.Administration.Models.Reports
{
    public class InstructorTrainingReportItemViewMode
    {
        [ExportToExcel(ResourceType = typeof(InstructorsStrings), HeaderName = "InstructorID")]
        public string InstructorID { get; set; }

        [ExportToExcel(ResourceType = typeof(InstructorsStrings), HeaderName = "FullNameHE")]
        public string FullNameHe { get; set; }

        [ExportToExcel(ResourceType = typeof(InstructorsStrings), HeaderName = "City")]
        public string City { get; set; }

        [ExportToExcel(ResourceType = typeof(InstructorsStrings), HeaderName = "CellPhone")]
        public string CellPhone { get; set; }

        [ExportToExcel(ResourceType = typeof(InstructorsStrings), HeaderName = "Email")]
        public string Email { get; set; }

        [ExportToExcel(ResourceType = typeof(InstructorsStrings), HeaderName = "RoleName")]
        public string InstructorRole { get; set; }


        public int OrganizationalStatus { get; set; }

        [ExportToExcel(ResourceType = typeof(InstructorsStrings), HeaderName = "OrganizationalStatus")]
        public eOrganizationalStatus OrganizationalStatusEnum => (eOrganizationalStatus)this.OrganizationalStatus;

        [ExportToExcel(ResourceType = typeof(InstructorsStrings), HeaderName = "TrainingName")]
        public string TrainingName { get; set; }

        [ExportToExcel(ResourceType = typeof(InstructorsStrings), HeaderName = "TrainingDate")]
        public DateTime TrainingDate { get; set; }

        public int TrainingType { get; set; }

        [ExportToExcel(ResourceType = typeof(InstructorsStrings), HeaderName = "TrainingType")]
        public eTrainingType TrainingTypeEnum => (eTrainingType)this.TrainingType;

        [ExportToExcel(ResourceType = typeof(InstructorsStrings), HeaderName = "TrainingHours")]
        public int TrainingHours { get; set; }

        [ExportToExcel(ResourceType = typeof(InstructorsStrings), HeaderName = "TrainingCompleted")]
        public bool IsCompleted { get; set; }
        
        public int TrainingLocation { get; set; }

        [ExportToExcel(ResourceType = typeof(InstructorsStrings), HeaderName = "TrainingLocation")]
        public eTrainingLocation TrainingLocationEnum => (eTrainingLocation) this.TrainingLocation;

    }

    public class InstructorTrainingReportViewModel
    {
        //Filters
        [Display(ResourceType = typeof(InstructorsStrings), Name = "TrainingName")]
        public CustomSelectList TrainingName { get; set; }

        [Display(ResourceType = typeof(InstructorsStrings), Name = "TrainingType")]
        public eTrainingType? eTrainingType { get; set; }

        public int? TrainingType
        {
            get
            {
                if (eTrainingType != null)
                {
                    return (int)eTrainingType;
                }
                return null;
            }
        }

        public int? InstructorType { get; set; }

        [Display(ResourceType = typeof(InstructorsStrings), Name = "InstructorRole")]
        public IEnumerable<InstructorRoles> InstructorRoles { get; set; }

        public int? TrainingID { get; set; }

        [Display(ResourceType = typeof(InstructorsStrings), Name = "OrganizationalStatus")]
        public eOrganizationalStatus? eOrganizationalStatus { get; set; }

        public int? OrgStatus
        {
            get
            {
                if (eOrganizationalStatus != null)
                {
                    return (int) eOrganizationalStatus;
                }
                return null;
            }
        }

        public bool OpenedFromTraining { get; set; }

        [Display(ResourceType = typeof(InstructorsStrings), Name = "TrainingLocation")]
        public eTrainingLocation? TrainingLocationEnum { get; set; }

        public int? TrainingLocation
        {
            get
            {
                if (TrainingLocationEnum != null)
                {
                    return (int)TrainingLocationEnum;
                }
                return null;
            }
        }

        [Display(ResourceType = typeof(InstructorsStrings), Name = "CompleteTraining")]
        public bool TrainingCompleted { get; set; }

        public IEnumerable<InstructorTrainingReportItemViewMode> Items { get; set; }

        public InstructorTrainingReportViewModel()
        {
            Items = new List<InstructorTrainingReportItemViewMode>();
            TrainingCompleted = true;
            TrainingName = new CustomSelectList();
        }

    }
}