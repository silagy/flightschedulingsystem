﻿using FlightScheduling.Language;
using System;
using System.Collections.Generic;
using Endilo.ExportToExcel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FlightScheduling.DAL;

namespace FlightScheduling.Web.Areas.Administration.Models.Reports
{
    public class ExpeditionReportViewModel
    {
        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(CommonStrings), Name = "FromDate")]
        public DateTime StartDate { get; set; }

        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(CommonStrings), Name = "To")]
        public DateTime EndDate { get; set; }

        [Display(ResourceType = typeof(ReportsStrings), Name = "csvExpeditionStatus")]
        public eExpeditionStatus? Status { get; set; }

        public int? StatusId => Status != null? (int)Status : (int?)null;

        public IEnumerable<ExpeditionItemReportViewModel> Items { get; set; }

        public ExpeditionReportViewModel()
        {
            Items = new List<ExpeditionItemReportViewModel>();
            StartDate = DateTime.Now.AddMonths(-1);
            EndDate = DateTime.Now.AddMonths(1);
        }
    }

    public class ExpeditionItemReportViewModel
    {
        [ExportToExcel(ResourceType = typeof(ReportsStrings), HeaderName = "CsvOrderID")]
        [Display(ResourceType = typeof(ReportsStrings), Name = "CsvOrderID")]
        public int ExpeditionId { get; set; }

        public int StatusId { get; set; }

        [ExportToExcel(ResourceType = typeof(ReportsStrings), HeaderName = "csvExpeditionStatus")]
        [Display(ResourceType = typeof(ReportsStrings), Name = "csvExpeditionStatus")]
        public string Status => ((eExpeditionStatus)StatusId).GetEnumDescription();

        [ExportToExcel(ResourceType = typeof(ReportsStrings), HeaderName = "CsvSchoolName")]
        [Display(ResourceType = typeof(ReportsStrings), Name = "CsvSchoolName")]
        public string SchoolName { get; set; }

        [ExportToExcel(ResourceType = typeof(ReportsStrings), HeaderName = "CsvInstituteID")]
        [Display(ResourceType = typeof(ReportsStrings), Name = "CsvInstituteID")]
        public string InstituteID { get; set; }

        public int RegionID { get; set; }

        [ExportToExcel(ResourceType = typeof(ReportsStrings), HeaderName = "csvSchoolRegion")]
        [Display(ResourceType = typeof(ReportsStrings), Name = "csvSchoolRegion")]
        public string Region => ((eSchoolRegion)RegionID).GetEnumDescription();

        [ExportToExcel(ResourceType = typeof(ReportsStrings), HeaderName = "CsvNumberOfPassengers")]
        [Display(ResourceType = typeof(ReportsStrings), Name = "CsvNumberOfPassengers")]
        public int NumOfPassenges { get; set; }

        [ExportToExcel(ResourceType = typeof(ReportsStrings), HeaderName = "CsvNumberOfBuses")]
        [Display(ResourceType = typeof(ReportsStrings), Name = "CsvNumberOfBuses")]
        public int NumOfBuses { get; set; }

        [ExportToExcel(ResourceType = typeof(ReportsStrings), HeaderName = "CsvDepartureDate")]
        [Display(ResourceType = typeof(ReportsStrings), Name = "CsvDepartureDate")]
        public DateTime DepartureDate { get; set; }

        [ExportToExcel(ResourceType = typeof(ReportsStrings), HeaderName = "CsvSchoolMangerName")]
        [Display(ResourceType = typeof(ReportsStrings), Name = "CsvSchoolMangerName")]
        public string Manager { get; set; }

        [ExportToExcel(ResourceType = typeof(ReportsStrings), HeaderName = "CsvSchoolMangerPhone")]
        [Display(ResourceType = typeof(ReportsStrings), Name = "CsvSchoolMangerPhone")]
        public string ManagerPhone { get; set; }

        [ExportToExcel(ResourceType = typeof(ReportsStrings), HeaderName = "CsvSchoolMangerCell")]
        [Display(ResourceType = typeof(ReportsStrings), Name = "CsvSchoolMangerCell")]
        public string ManagerCell { get; set; }

        [ExportToExcel(ResourceType = typeof(ReportsStrings), HeaderName = "CsvSchoolMangerEmail")]
        [Display(ResourceType = typeof(ReportsStrings), Name = "CsvSchoolMangerEmail")]
        public string ManagerEmail { get; set; }


    }
}
