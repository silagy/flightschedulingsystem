﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Endilo.ExportToExcel;
using FlightScheduling.Language;

namespace FlightScheduling.Web.Areas.Administration.Models.Reports
{
    public class BusesPerDayViewModel
    {
       

        [ExportToExcel(ResourceType = typeof(ReportsStrings), HeaderName = "Day")]
        public DateTime Day { get; set; }

        [ExportToExcel(ResourceType = typeof(ReportsStrings), HeaderName = "BusesPerDayCount")]
        public int BusesPerDay { get; set; }

        [ExportToExcel(ResourceType = typeof(ReportsStrings), HeaderName = "ExpeditionPerDayCount")]
        public int ExpeditionPerDay { get; set; }


    }

    public class BusesPerDayReportViewModel
    {
        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(CommonStrings), Name = "FromDate")]
        public DateTime StartDate { get; set; }

        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(CommonStrings), Name = "To")]
        public DateTime EndDate { get; set; }

        public List<BusesPerDayViewModel> BusesPerDayList { get; set; }

        public BusesPerDayReportViewModel()
        {
            StartDate = new DateTime(DateTime.Now.Year,1,1);
            EndDate = new DateTime(DateTime.Now.Year,12,31);
            BusesPerDayList = new List<BusesPerDayViewModel>();
        }
    }

    

    
}