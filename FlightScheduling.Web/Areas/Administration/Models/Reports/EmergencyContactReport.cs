﻿using Endilo.ExportToExcel;
using FlightScheduling.DAL;
using FlightScheduling.Language;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FlightScheduling.Web.Areas.Administration.Models.Reports
{
    public class EmergencyContactReport
    {
        [ExportToExcel(ResourceType = typeof(ReportsStrings), HeaderName = "CsvGroupID")]
        [Display(ResourceType = typeof(ReportsStrings), Name = "CsvGroupID")]
        public string GroupID { get; set; }

        [ExportToExcel(ResourceType = typeof(ReportsStrings), HeaderName = "CsvSchoolName")]
        [Display(ResourceType = typeof(ReportsStrings), Name = "CsvSchoolName")]
        public string SchoolName { get; set; }

        public int? Region { get; set; }

        [ExportToExcel(ResourceType = typeof(ReportsStrings), HeaderName = "csvSchoolRegion")]
        [Display(ResourceType = typeof(ReportsStrings), Name = "csvSchoolRegion")]
        public string RegionEnum
        {
            get {
                if (this.Region != null)
                {
                    return ((eSchoolRegion)this.Region).GetEnumDescription();
                }
                return String.Empty;
            }
        }

        [ExportToExcel(ResourceType = typeof(ReportsStrings), HeaderName = "csvGroupEmergancyContactName")]
        [Display(ResourceType = typeof(ReportsStrings), Name = "csvGroupEmergancyContactName")]
        public string EmergencyContactName { get; set; }

        [ExportToExcel(ResourceType = typeof(ReportsStrings), HeaderName = "csvGroupEmergancyContactPhone")]
        [Display(ResourceType = typeof(ReportsStrings), Name = "csvGroupEmergancyContactPhone")]
        public string EmergencyContactPhone { get; set; }

        [ExportToExcel(ResourceType = typeof(ReportsStrings), HeaderName = "CsvSchoolMangerName")]
        [Display(ResourceType = typeof(ReportsStrings), Name = "CsvSchoolMangerName")]
        public string SchoolManagerName { get; set; }

        [ExportToExcel(ResourceType = typeof(ReportsStrings), HeaderName = "CsvSchoolMangerPhone")]
        [Display(ResourceType = typeof(ReportsStrings), Name = "CsvSchoolMangerPhone")]
        public string SchoolManagerPhone { get; set; }



    }

    public class EmerhencyContactReportViewModel
    {
        [Display(ResourceType = typeof(CommonStrings), Name = "TargetDate")]
        public DateTime TargetDate { get; set; }

        public List<EmergencyContactReport> Items { get; set; }

        public EmerhencyContactReportViewModel()
        {
            Items = new List<EmergencyContactReport>();
            TargetDate = DateTime.Now;
        }
    }
}