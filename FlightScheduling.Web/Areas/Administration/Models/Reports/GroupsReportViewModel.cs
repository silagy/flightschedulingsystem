﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Endilo.ExportToExcel;
using FlightScheduling.DAL;
using FlightScheduling.Language;
using FlightScheduling.Web.Areas.Administration.Models.Common;

namespace FlightScheduling.Web.Areas.Administration.Models.Reports
{
    public class GroupsReportViewModel
    {
        //Filters

        [Display(ResourceType = typeof(CommonStrings), Name = "FromDate")]
        public DateTime StartDate { get; set; }

        [Display(ResourceType = typeof(CommonStrings), Name = "To")]
        public DateTime EndDate { get; set; }

        public int? AgencyID { get; set; }

        [Display(ResourceType = typeof(AgenciesStrings), Name = "Name")]
        public CustomSelectList Agencies { get; set; }

        [Display(ResourceType = typeof(TripsStrings), Name = "Destenation")]
        public eFlightsFields? DepartureIDEnum { get; set; }

        public int? DepartureID
        {
            get {
                if (this.DepartureIDEnum != null)
                {
                    return (int)this.DepartureIDEnum;
                }

                return null;
            }
        }

        [Display(ResourceType = typeof(TripsStrings), Name = "TakeOffField")]
        public eFlightsFields? ReturningFieldEnum { get; set; }

        public int? ReturningField
        {
            get
            {
                if (this.ReturningFieldEnum != null)
                {
                    return (int)this.ReturningFieldEnum;
                }

                return null;
            }
        }

        //Items
        public List<GroupsReportItemViewModel> Items { get; set; }

        public GroupsReportViewModel()
        {
            Items = new List<GroupsReportItemViewModel>();
            Agencies = new CustomSelectList();
            StartDate = new DateTime(DateTime.Now.Year, 1, 1);
            EndDate = new DateTime(DateTime.Now.Year, 12, 31);
        }
    }
    public class GroupsReportItemViewModel
    {
        //Properties
        public int GroupID { get; set; }

        [ExportToExcel(ResourceType = typeof(GroupsStrings), HeaderName = "ExpeditionID")]
        [Display(ResourceType = typeof(GroupsStrings), Name = "ExpeditionID")]
        public string GroupExternalID { get; set; }

        [ExportToExcel(ResourceType = typeof(GroupsStrings), HeaderName = "NumberOfBuses")]
        [Display(ResourceType = typeof(GroupsStrings), Name = "NumberOfBuses")]
        public int NumberOfBuses { get; set; }

        [ExportToExcel(ResourceType = typeof(GroupsStrings), HeaderName = "NumberOfParticipants")]
        [Display(ResourceType = typeof(GroupsStrings), Name = "NumberOfParticipants")]
        public int NumberOfParticipants { get; set; }

        [ExportToExcel(ResourceType = typeof(SchoolsStrings), HeaderName = "InstituteID")]
        [Display(ResourceType = typeof(SchoolsStrings), Name = "InstituteID")]
        public string SchoolInsititueID { get; set; }

        [ExportToExcel(ResourceType = typeof(SchoolsStrings), HeaderName = "Name")]
        [Display(ResourceType = typeof(SchoolsStrings), Name = "Name")]
        public string SchoolName { get; set; }

        [ExportToExcel(ResourceType = typeof(AgenciesStrings), HeaderName = "Name")]
        [Display(ResourceType = typeof(AgenciesStrings), Name = "Name")]
        public string AgencyName { get; set; }

        [ExportToExcel(ResourceType = typeof(TripsStrings), HeaderName = "DepartureDate")]
        [Display(ResourceType = typeof(TripsStrings), Name = "DepartureDate")]
        public DateTime DepartureDate { get; set; }

        public int DestinationID { get; set; }

        [ExportToExcel(ResourceType = typeof(TripsStrings), HeaderName = "Destenation")]
        [Display(ResourceType = typeof(TripsStrings), Name = "Destenation")]
        public eFlightsFields DestinationFieldEnum
        {
            get { return (eFlightsFields)this.DestinationID; }
        }

        [ExportToExcel(ResourceType = typeof(TripsStrings), HeaderName = "ReturnDate")]
        [Display(ResourceType = typeof(TripsStrings), Name = "ReturnDate")]
        public DateTime ReturningDate { get; set; }

        public int ReturingField { get; set; }

        [ExportToExcel(ResourceType = typeof(TripsStrings), HeaderName = "TakeOffField")]
        [Display(ResourceType = typeof(TripsStrings), Name = "TakeOffField")]
        public eFlightsFields ReturningFieldEnum
        {
            get { return (eFlightsFields)this.ReturingField; }
        }
    }
}