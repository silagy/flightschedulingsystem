﻿using CsvHelper.Configuration;
using FlightScheduling.Language;
using FlightScheduling.Web.Areas.Administration.Models.Flights;
using FlightScheduling.Web.Infrastructure;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Endilo.ExportToExcel;

namespace FlightScheduling.Web.Areas.Administration.Models.Reports
{
    public class ExpeditionToCsvIntegrationViewModel
    {

        //Properties
        [ExportToExcel(ResourceType = typeof(ReportsStrings), HeaderName = "CsvOrderID")]
        [Display(ResourceType = typeof(ReportsStrings), Name = "CsvOrderID")]
        public int ExpeditionID { get; set; }

        [Display(ResourceType = typeof(ReportsStrings), Name = "CsvDepartureDate")]
        public DateTime DepartureDate { get; set; }

        [ExportToExcel(ResourceType = typeof(ReportsStrings), HeaderName = "CsvDepartureDate")]
        [Display(ResourceType = typeof(ReportsStrings), Name = "CsvDepartureDate")]
        public string departureDateExport => this.DepartureDate.ToString(AppConfig.FormatDate);

        [Display(ResourceType = typeof(ReportsStrings), Name = "CsvReturnDate")]
        public DateTime ReturningDate { get; set; }

        [ExportToExcel(ResourceType = typeof(ReportsStrings), HeaderName = "CsvReturnDate")]
        [Display(ResourceType = typeof(ReportsStrings), Name = "CsvReturnDate")]
        public string ReturnDate => this.ReturningDate.ToString(AppConfig.FormatDate);

        [ExportToExcel(ResourceType = typeof(ReportsStrings), HeaderName = "CsvDestenationID")]
        [Display(ResourceType = typeof(ReportsStrings), Name = "CsvDestenationID")]
        public FlightViewModel.FlightsFields DestinationID { get; set; }

        [ExportToExcel(ResourceType = typeof(ReportsStrings), HeaderName = "CsvReturingField")]
        [Display(ResourceType = typeof(ReportsStrings), Name = "CsvReturingField")]
        public FlightViewModel.FlightsFields ReturingField { get; set; }

        [ExportToExcel(ResourceType = typeof(ReportsStrings), HeaderName = "CsvSchoolName")]
        [Display(ResourceType = typeof(ReportsStrings), Name = "CsvSchoolName")]
        public string SchoolName { get; set; }

        [ExportToExcel(ResourceType = typeof(ReportsStrings), HeaderName = "CsvInstituteID")]
        [Display(ResourceType = typeof(ReportsStrings), Name = "CsvInstituteID")]
        public int InstituteID { get; set; }

        [ExportToExcel(ResourceType = typeof(ReportsStrings), HeaderName = "CsvNumberOfBuses")]
        [Display(ResourceType = typeof(ReportsStrings), Name = "CsvNumberOfBuses")]
        public int BasesPerDay { get; set; }

        [ExportToExcel(ResourceType = typeof(ReportsStrings), HeaderName = "CsvNumberOfPassengers")]
        [Display(ResourceType = typeof(ReportsStrings), Name = "CsvNumberOfPassengers")]
        public int NumberOfpassengers { get; set; }

        [ExportToExcel(ResourceType = typeof(ReportsStrings), HeaderName = "CsvAgencyID")]
        [Display(ResourceType = typeof(ReportsStrings), Name = "CsvAgencyID")]
        public string AgencyName { get; set; }

        [ExportToExcel(ResourceType = typeof(ReportsStrings), HeaderName = "CsvExpeditionMangerName")]
        [Display(ResourceType = typeof(ReportsStrings), Name = "CsvExpeditionMangerName")]
        public string Manager { get; set; }

        [ExportToExcel(ResourceType = typeof(ReportsStrings), HeaderName = "CsvExpeditionMangerCell")]
        [Display(ResourceType = typeof(ReportsStrings), Name = "CsvExpeditionMangerCell")]
        public string ManagerCell { get; set; }

        [ExportToExcel(ResourceType = typeof(ReportsStrings), HeaderName = "CsvExpeditionMangerEmail")]
        [Display(ResourceType = typeof(ReportsStrings), Name = "CsvExpeditionMangerEmail")]
        public string ManagerEmail { get; set; }

        [ExportToExcel(ResourceType = typeof(ReportsStrings), HeaderName = "CsvExpeditionMangerPhone")]
        [Display(ResourceType = typeof(ReportsStrings), Name = "CsvExpeditionMangerPhone")]
        public string ManagerPhone { get; set; }

        [ExportToExcel(ResourceType = typeof(ReportsStrings), HeaderName = "CsvSchoolMangerName")]
        [Display(ResourceType = typeof(ReportsStrings), Name = "CsvSchoolMangerName")]
        public string SchoolManager { get; set; }

        [ExportToExcel(ResourceType = typeof(ReportsStrings), HeaderName = "CsvSchoolMangerEmail")]
        [Display(ResourceType = typeof(ReportsStrings), Name = "CsvSchoolMangerEmail")]
        public string SchoolManagerEmail { get; set; }

        [ExportToExcel(ResourceType = typeof(ReportsStrings), HeaderName = "CsvSchoolMangerPhone")]
        [Display(ResourceType = typeof(ReportsStrings), Name = "CsvSchoolMangerPhone")]
        public string SchoolManagerPhone { get; set; }

        [ExportToExcel(ResourceType = typeof(ReportsStrings), HeaderName = "CsvGroupID")]
        [Display(ResourceType = typeof(ReportsStrings), Name = "CsvGroupID")]
        public int GroupID { get; set; }
    }

    public class ExpeditionsCsvViewModel
    {
        //Properties
        public List<ExpeditionToCsvIntegrationViewModel> Expeditions { get; set; }

        //Fliters
        [Display(ResourceType = typeof(CommonStrings), Name = "FromDate")]
        public DateTime FilterFromDate { get; set; }

        [Display(ResourceType = typeof(CommonStrings), Name = "To")]
        public DateTime FilterToDate { get; set; }

        public ExpeditionsCsvViewModel()
        {
            Expeditions = new List<ExpeditionToCsvIntegrationViewModel>();

            FilterFromDate = new DateTime(DateTime.Now.Year, 01, 01);
            FilterToDate = new DateTime(DateTime.Now.Year, 12, 31);
        }
    }

    public sealed class ExpeditionToCsvIntegrationViewModelMap : ClassMap<ExpeditionToCsvIntegrationViewModel>
    {
        public ExpeditionToCsvIntegrationViewModelMap()
        {
            Map(m => m.ExpeditionID).Name(ReportsStrings.CsvOrderID);
            Map(m => m.departureDateExport).Name(ReportsStrings.CsvDepartureDate).TypeConverter<CsvConverterHelper.CsvDateConverter>();
            Map(m => m.ReturnDate).Name(ReportsStrings.CsvReturnDate).TypeConverter<CsvConverterHelper.CsvDateConverter>();
            Map(m => m.DestinationID).Name(ReportsStrings.CsvDestenationID).TypeConverter<CsvConverterHelper.CsvFlightsFieldsConverter>();
            Map(m => m.ReturingField).Name(ReportsStrings.CsvReturingField).TypeConverter<CsvConverterHelper.CsvFlightsFieldsConverter>();
            Map(m => m.SchoolName).Name(ReportsStrings.CsvSchoolName);
            Map(m => m.InstituteID).Name(ReportsStrings.CsvInstituteID);
            Map(m => m.BasesPerDay).Name(ReportsStrings.CsvNumberOfBuses);
            Map(m => m.NumberOfpassengers).Name(ReportsStrings.CsvNumberOfPassengers);
            Map(m => m.AgencyName).Name(ReportsStrings.CsvAgencyID);
            Map(m => m.Manager).Name(ReportsStrings.CsvExpeditionMangerName);
            Map(m => m.ManagerCell).Name(ReportsStrings.CsvExpeditionMangerCell);
            Map(m => m.ManagerEmail).Name(ReportsStrings.CsvExpeditionMangerEmail);
            Map(m => m.ManagerPhone).Name(ReportsStrings.CsvExpeditionMangerPhone);
            Map(m => m.SchoolManager).Name(ReportsStrings.CsvSchoolMangerName);
            Map(m => m.SchoolManagerEmail).Name(ReportsStrings.CsvSchoolMangerEmail);
            Map(m => m.SchoolManagerPhone).Name(ReportsStrings.CsvSchoolMangerPhone);
            Map(m => m.GroupID).Name(ReportsStrings.CsvGroupID);
        }
    }
}