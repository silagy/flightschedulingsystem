﻿using Endilo.ExportToExcel;
using FlightScheduling.DAL;
using FlightScheduling.Language;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Security.AccessControl;
using System.Web;

namespace FlightScheduling.Web.Areas.Administration.Models.Reports
{
    public class GroupsByRegionsViewModel
    {
        [Display(ResourceType = typeof(CommonStrings), Name = "FromDate")]
        public DateTime StartDate { get; set; }

        [Display(ResourceType = typeof(CommonStrings), Name = "To")]
        public DateTime EndDate { get; set; }

        [Display(ResourceType = typeof(ReportsStrings), Name = "csvSchoolRegion")]
        public eSchoolRegion? Region { get; set; }

        [Display(ResourceType = typeof(ReportsStrings), Name = "CsvInstituteID")]
        public int? InstituteID { get; set; }

        public IEnumerable<GroupsByRegionItem> Items { get; set; }

        public GroupsByRegionsViewModel()
        {
            Items = new List<GroupsByRegionItem>();
            StartDate = new DateTime(DateTime.Now.Year, 01, 01);
            EndDate = new DateTime(DateTime.Now.Year, 12, 31);
        }
    }

    public class GroupsByRegionItem
    {
        [ExportToExcel(ResourceType = typeof(ReportsStrings), HeaderName = "CsvSchoolName")]
        [Display(ResourceType = typeof(ReportsStrings), Name = "CsvSchoolName")]
        public string SchoolName { get; set; }

        [ExportToExcel(ResourceType = typeof(ReportsStrings), HeaderName = "CsvInstituteID")]
        [Display(ResourceType = typeof(ReportsStrings), Name = "CsvInstituteID")]
        public int InstituteID { get; set; }

        public int? Region { get; set; }

        [ExportToExcel(ResourceType = typeof(ReportsStrings), HeaderName = "csvSchoolRegion")]
        [Display(ResourceType = typeof(ReportsStrings), Name = "csvSchoolRegion")]
        public string RegionEnum
        {
            get
            {
                if (this.Region != null)
                {
                    return ((eSchoolRegion)this.Region).GetEnumDescription();
                }
                return String.Empty;
            }
        }

        [ExportToExcel(ResourceType = typeof(TripsStrings), HeaderName = "DepartureDate")]
        [Display(ResourceType = typeof(TripsStrings), Name = "DepartureDate")]
        public DateTime DepartureDate { get; set; }

        [ExportToExcel(ResourceType = typeof(ReportsStrings), HeaderName = "CsvGroupID")]
        [Display(ResourceType = typeof(ReportsStrings), Name = "CsvGroupID")]
        public int GroupExternalID { get; set; }

        public string Year => DepartureDate.ToString("yyyy");

        [ExportToExcel(ResourceType = typeof(ReportsStrings), HeaderName = "CsvParticipantsCount")]
        [Display(ResourceType = typeof(ReportsStrings), Name = "CsvParticipantsCount")]
        public int NumberOfParticipants { get; set; }

        [ExportToExcel(ResourceType = typeof(ReportsStrings), HeaderName = "CsvInstructorsCount")]
        [Display(ResourceType = typeof(ReportsStrings), Name = "CsvInstructorsCount")]
        public int CountInstructors { get; set; }
    }
}