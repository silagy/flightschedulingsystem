﻿using Endilo.ExportToExcel;
using FlightScheduling.DAL;
using FlightScheduling.Language;
using OfficeOpenXml.Table.PivotTable;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Security.AccessControl;
using System.Web;

namespace FlightScheduling.Web.Areas.Administration.Models.Reports
{
    public class GroupManagerReportViewModel
    {
        [Display(ResourceType = typeof(CommonStrings), Name = "FromDate")]
        public DateTime StartDate { get; set; }

        [Display(ResourceType = typeof(CommonStrings), Name = "To")]
        public DateTime EndDate { get; set; }

        [Display(ResourceType = typeof(GroupsStrings), Name = "ExpeditionID")]
        public int? GroupExternalID { get; set; }

        [Display(ResourceType = typeof(TripsStrings), Name = "Destenation")]
        public eFlightsFields? DepartureField { get; set; }

        public int? DepartureFieldID
        {
            get
            {
                if (DepartureField != null)
                {
                    return (int)DepartureField;
                }

                return null;
            }
        }


        public IEnumerable<GroupManagersReportItem> Items { get; set; }

        public GroupManagerReportViewModel()
        {
            StartDate = new DateTime(DateTime.Now.Year, 1, 1);
            EndDate = StartDate.AddYears(1);
            Items = new List<GroupManagersReportItem>();
        }
    }
    public class GroupManagersReportItem
    {

        //Properties
        public int GroupID { get; set; }

        [ExportToExcel(ResourceType = typeof(GroupsStrings), HeaderName = "ExpeditionID")]
        [Display(ResourceType = typeof(GroupsStrings), Name = "ExpeditionID")]
        public string GroupExternalID { get; set; }

        [ExportToExcel(ResourceType = typeof(TripsStrings), HeaderName = "DepartureDate")]
        [Display(ResourceType = typeof(TripsStrings), Name = "DepartureDate")]
        public DateTime DepartureDate { get; set; }

        public int DestinationID { get; set; }

        [ExportToExcel(ResourceType = typeof(TripsStrings), HeaderName = "Destenation")]
        [Display(ResourceType = typeof(TripsStrings), Name = "Destenation")]
        public eFlightsFields DestinationFieldEnum
        {
            get { return (eFlightsFields)this.DestinationID; }
        }

        [ExportToExcel(ResourceType = typeof(GroupsStrings), HeaderName = "NumberOfBuses")]
        [Display(ResourceType = typeof(GroupsStrings), Name = "NumberOfBuses")]
        public int NumberOfBuses { get; set; }

        [ExportToExcel(ResourceType = typeof(GroupsStrings), HeaderName = "NumberOfParticipants")]
        [Display(ResourceType = typeof(GroupsStrings), Name = "NumberOfParticipants")]
        public int TotalParticipants { get; set; }

        [ExportToExcel(ResourceType = typeof(SchoolsStrings), HeaderName = "InstituteID")]
        [Display(ResourceType = typeof(SchoolsStrings), Name = "InstituteID")]
        public int InstituteID { get; set; }

        [ExportToExcel(ResourceType = typeof(SchoolsStrings), HeaderName = "Name")]
        [Display(ResourceType = typeof(SchoolsStrings), Name = "Name")]
        public string SchoolName { get; set; }

        public string GroupManagerFirstName { get; set; }
        public string GroupManagerLastName { get; set; }

        [ExportToExcel(ResourceType = typeof(ReportsStrings), HeaderName = "GroupManagerName")]
        [Display(ResourceType = typeof(ReportsStrings), Name = "GroupManagerName")]
        public string GroupManagerName => $"{GroupManagerFirstName} {GroupManagerLastName}";

        [ExportToExcel(ResourceType = typeof(ReportsStrings), HeaderName = "GroupManagerCell")]
        [Display(ResourceType = typeof(ReportsStrings), Name = "GroupManagerCell")]
        public string GroupManagerCell { get; set; }

        [ExportToExcel(ResourceType = typeof(ReportsStrings), HeaderName = "GroupManagerEmail")]
        [Display(ResourceType = typeof(ReportsStrings), Name = "GroupManagerEmail")]
        public string GroupManagerEmail { get; set; }

        public string DeputyManagerFirstName { get; set; }
        public string DeputyManagerLastName { get; set; }

        [ExportToExcel(ResourceType = typeof(ReportsStrings), HeaderName = "DeputyManagerName")]
        [Display(ResourceType = typeof(ReportsStrings), Name = "DeputyManagerName")]
        public string DeputyManagerName => $"{DeputyManagerFirstName} {DeputyManagerLastName}";

        [ExportToExcel(ResourceType = typeof(ReportsStrings), HeaderName = "DeputyManagerCell")]
        [Display(ResourceType = typeof(ReportsStrings), Name = "DeputyManagerCell")]
        public string DeputyManagerCell { get; set; }

        [ExportToExcel(ResourceType = typeof(ReportsStrings), HeaderName = "DeputyManagerEmail")]
        [Display(ResourceType = typeof(ReportsStrings), Name = "DeputyManagerEmail")]
        public string DeputyManagerEmail { get; set; }

    }
}