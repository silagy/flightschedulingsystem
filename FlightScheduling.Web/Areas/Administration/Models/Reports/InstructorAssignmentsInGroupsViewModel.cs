﻿using Endilo.ExportToExcel;
using FlightScheduling.DAL;
using FlightScheduling.Language;
using FlightScheduling.Web.Areas.Administration.Models.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FlightScheduling.Web.Areas.Administration.Models.Reports
{
    public class InstructorAssignmentsInGroupsViewModel
    {
        [Display(ResourceType = typeof(CommonStrings), Name = "FromDate")]
        public DateTime StartDate { get; set; }

        [Display(ResourceType = typeof(CommonStrings), Name = "To")]
        public DateTime EndDate { get; set; }

        [Display(ResourceType = typeof(ReportsStrings), Name = "csvSchoolRegion")]
        public eSchoolRegion? Region { get; set; }

        [Display(ResourceType = typeof(ReportsStrings), Name = "CsvGroupID")]
        public int? GroupExternalID { get; set; }

        [Display(ResourceType = typeof(InstructorsStrings), Name = "InstructorID")]
        public string InstructorID { get; set; }

        [Display(ResourceType = typeof(ReportsStrings), Name = "csvInstructorGroupRoleName")]
        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        public CustomSelectList Roles { get; set; }

        public IEnumerable<int> SelectedRoles { get; set; }

        public int CountRoles => SelectedRoles != null ? SelectedRoles.Count() : 0;

        public bool OpenedFromTraining { get; set; }

        public IEnumerable<InstructorAssignmentsInGroupsItem> Items { get; set; }

        public InstructorAssignmentsInGroupsViewModel()
        {
            Items = new List<InstructorAssignmentsInGroupsItem>();
            StartDate = new DateTime(DateTime.Now.Year, 01, 01);
            EndDate = new DateTime(DateTime.Now.Year, 12, 31);
            OpenedFromTraining = false;
            Roles = new CustomSelectList();
            Roles.Items.Add(new CustomSelectListItem()
            {
                ID = (int)eInstructorRoleType.AccompanyingMentor,
                Name = eInstructorRoleType.AccompanyingMentor.GetEnumDescription()
            });

            Roles.Items.Add(new CustomSelectListItem()
            {
                ID = (int)eInstructorRoleType.Instructor,
                Name = eInstructorRoleType.Instructor.GetEnumDescription()
            });

            Roles.Items.Add(new CustomSelectListItem()
            {
                ID = (int)eInstructorRoleType.ExpeditionManager,
                Name = eInstructorRoleType.ExpeditionManager.GetEnumDescription()
            });
        }
    }

    public class InstructorAssignmentsInGroupsItem
    {
        public int ID { get; set; }

        [ExportToExcel(ResourceType = typeof(InstructorsStrings), HeaderName = "InstructorID")]
        [Display(ResourceType = typeof(InstructorsStrings), Name = "InstructorID")]
        public string InstructorID { get; set; }

        public string FirstName { get; set; }
        public string LastName { get; set; }

        [ExportToExcel(ResourceType = typeof(CommonStrings), HeaderName = "FullName")]
        [Display(ResourceType = typeof(CommonStrings), Name = "FullName")]
        public string InstructorName => $"{FirstName} {LastName}";

        [ExportToExcel(ResourceType = typeof(InstructorsStrings), HeaderName = "RoleName")]
        [Display(ResourceType = typeof(InstructorsStrings), Name = "RoleName")]
        public string RoleName { get; set; }

        public int InstructorGroupRoleID { get; set; }

        [ExportToExcel(ResourceType = typeof(InstructorsStrings), HeaderName = "csvInstructorGroupRoleName")]
        [Display(ResourceType = typeof(InstructorsStrings), Name = "csvInstructorGroupRoleName")]
        public string InstructorGroupRoleName => ((eInstructorRoleType)InstructorGroupRoleID).GetEnumDescription();

        public int SchoolID { get; set; }

        [ExportToExcel(ResourceType = typeof(ReportsStrings), HeaderName = "CsvSchoolName")]
        [Display(ResourceType = typeof(ReportsStrings), Name = "CsvSchoolName")]
        public string SchoolName { get; set; }

        [ExportToExcel(ResourceType = typeof(ReportsStrings), HeaderName = "CsvInstituteID")]
        [Display(ResourceType = typeof(ReportsStrings), Name = "CsvInstituteID")]
        public string InstituteID { get; set; }

        public int? Region { get; set; }

        [ExportToExcel(ResourceType = typeof(ReportsStrings), HeaderName = "csvSchoolRegion")]
        [Display(ResourceType = typeof(ReportsStrings), Name = "csvSchoolRegion")]
        public string RegionEnum
        {
            get
            {
                if (this.Region != null)
                {
                    return ((eSchoolRegion)this.Region).GetEnumDescription();
                }
                return String.Empty;
            }
        }

        [ExportToExcel(ResourceType = typeof(ReportsStrings), HeaderName = "CsvGroupID")]
        [Display(ResourceType = typeof(ReportsStrings), Name = "CsvGroupID")]
        public string GroupExternalID { get; set; }

        public int GroupID { get; set; }

        public DateTime DepartureDate { get; set; }

        [Display(ResourceType = typeof(ReportsStrings), Name = "csvYear")]
        public string Year => DepartureDate.ToString("yyyy");

        [ExportToExcel(ResourceType = typeof(ReportsStrings), HeaderName = "csvValidityEndTime")]
        [Display(ResourceType = typeof(ReportsStrings), Name = "csvValidityEndTime")]
        public DateTime ValidityEndTime { get; set; }
    }
}