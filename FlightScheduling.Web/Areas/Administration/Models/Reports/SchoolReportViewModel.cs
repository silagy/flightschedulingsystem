﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Endilo.ExportToExcel;
using FlightScheduling.DAL;
using FlightScheduling.Language;
using FlightScheduling.Web.Areas.Administration.Models.Common;
using Remotion.Mixins.Definitions;

namespace FlightScheduling.Web.Areas.Administration.Models.Reports
{
    public class SchoolExpeditionReportContainerViewModel
    {
        //Filters 
        //Properties
        [Display(ResourceType = typeof(CommonStrings), Name = "FromDate")]
        public DateTime StartDate { get; set; }
        [Display(ResourceType = typeof(CommonStrings), Name = "To")]
        public DateTime EndDate { get; set; }

        [Display(ResourceType = typeof(SchoolsStrings), Name = "InstituteID")]
        public string InstituteID { get; set; }

        [Display(ResourceType = typeof(GroupsStrings), Name = "ExpeditionID")]
        public string GroupID { get; set; }

        public List<SchoolReportViewModel> Schools { get; set; }


        public SchoolExpeditionReportContainerViewModel()
        {
            Schools = new List<SchoolReportViewModel>();
            StartDate = DateTime.Now.AddMonths(-3);
            EndDate = DateTime.Now.AddMonths(3);
        }
    }

    public class SchoolReportViewModel
    {
        //Properties

        [ExportToExcel(ResourceType = typeof(SchoolsStrings), HeaderName = "ID")]
        [Display(ResourceType = typeof(SchoolsStrings), Name = "ID")]
        public int ID { get; set; }

        [ExportToExcel(ResourceType = typeof(SchoolsStrings), HeaderName = "InstituteID")]
        [Display(ResourceType = typeof(SchoolsStrings), Name = "InstituteID")]
        public int InstituteID { get; set; }

        [ExportToExcel(ResourceType = typeof(SchoolsStrings), HeaderName = "Name")]
        [Display(ResourceType = typeof(SchoolsStrings), Name = "Name")]
        public string Name { get; set; }

        [ExportToExcel(ResourceType = typeof(SchoolsStrings), HeaderName = "Email")]
        [Display(ResourceType = typeof(SchoolsStrings), Name = "Email")]
        public string Email { get; set; }

        [ExportToExcel(ResourceType = typeof(SchoolsStrings), HeaderName = "Fax")]
        [Display(ResourceType = typeof(SchoolsStrings), Name = "Fax")]
        public string Fax { get; set; }

        [ExportToExcel(ResourceType = typeof(SchoolsStrings), HeaderName = "Phone")]
        [Display(ResourceType = typeof(SchoolsStrings), Name = "Phone")]
        public string Phone { get; set; }

        [ExportToExcel(ResourceType = typeof(SchoolsStrings), HeaderName = "Manager")]
        [Display(ResourceType = typeof(SchoolsStrings), Name = "Manager")]
        public string Manager { get; set; }

        public int RegionID { get; set; }

        [ExportToExcel(ResourceType = typeof(SchoolsStrings), HeaderName = "Region")]
        [Display(ResourceType = typeof(SchoolsStrings), Name = "Region")]
        public string Region => ((eSchoolRegion)RegionID).GetEnumDescription();

        [ExportToExcel(ResourceType = typeof(SchoolsStrings), HeaderName = "ManagerPhone")]
        [Display(ResourceType = typeof(SchoolsStrings), Name = "ManagerPhone")]
        public string ManagerPhone { get; set; }

        [ExportToExcel(ResourceType = typeof(SchoolsStrings), HeaderName = "ManagerEmail")]
        [Display(ResourceType = typeof(SchoolsStrings), Name = "ManagerEmail")]
        public string ManagerEmail { get; set; }

        [ExportToExcel(ResourceType = typeof(SchoolsStrings), HeaderName = "IsActive")]
        [Display(ResourceType = typeof(SchoolsStrings), Name = "IsActive")]
        public string IsActive { get; set; }

        [ExportToExcel(ResourceType = typeof (SchoolsStrings), HeaderName = "IsActivated")]
        [Display(ResourceType = typeof (SchoolsStrings), Name = "IsActivated")]
        public string IsActivatedString => IsActivated ? ReportsStrings.Yes : ReportsStrings.No;

        public bool IsActivated { get; set; }

        [ExportToExcel(ResourceType = typeof(CommonStrings), HeaderName = "CreationDate")]
        [Display(ResourceType = typeof(CommonStrings), Name = "CreationDate")]
        public DateTime CreationDate { get; set; }

        [ExportToExcel(ResourceType = typeof(CommonStrings), HeaderName = "UpdateDate")]
        [Display(ResourceType = typeof(CommonStrings), Name = "UpdateDate")]
        public DateTime? UpdateDate { get; set; }

        [ExportToExcel(ResourceType = typeof(TripsStrings), HeaderName = "DepartureDate")]
        [Display(ResourceType = typeof(TripsStrings), Name = "DepartureDate")]
        public DateTime? TripDepartureDate { get; set; }

        [ExportToExcel(ResourceType = typeof(ExpeditionStrings), HeaderName = "NumOfBuses")]
        [Display(ResourceType = typeof(ExpeditionStrings), Name = "NumOfBuses")]
        public int NumberOfBuses { get; set; }

        [ExportToExcel(ResourceType = typeof(ExpeditionStrings), HeaderName = "NumOfPassengers")]
        [Display(ResourceType = typeof(ExpeditionStrings), Name = "NumOfPassengers")]
        public int NumberOfPassengers { get; set; }

        [ExportToExcel(ResourceType = typeof(GroupsStrings), HeaderName = "ExpeditionID")]
        [Display(ResourceType = typeof(GroupsStrings), Name = "ExpeditionID")]
        public string GroupExternalID { get; set; }
    }
}