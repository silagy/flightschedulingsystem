﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Security.AccessControl;
using System.Web;
using Endilo.ExportToExcel;
using FlightScheduling.DAL;
using FlightScheduling.Language;
using FlightScheduling.Web.Areas.Administration.Models.Flights;

namespace FlightScheduling.Web.Areas.Administration.Models.Reports
{
    [Serializable]
    public class FlightsReportViewModel
    {

        //Properties
        [ExportToExcel(ResourceType = typeof(TripsStrings), HeaderName = "ID")]
        [Display(ResourceType = typeof(TripsStrings), Name = "ID")]
        public int TripID { get; set; }

        [ExportToExcel(ResourceType = typeof(ReportsStrings), HeaderName = "TripStartDate")]
        [Display(ResourceType = typeof(ReportsStrings), Name = "TripStartDate")]
        public DateTime DepartureDate { get; set; }

        [ExportToExcel(ResourceType = typeof(ReportsStrings), HeaderName = "TripEndDate")]
        [Display(ResourceType = typeof(ReportsStrings), Name = "TripEndDate")]
        public DateTime ReturnDate { get; set; }

        //[Display(ResourceType = typeof(TripsStrings), Name = "Destenation")]
        //public string Destenation { get; set; }

        //[Display(ResourceType = typeof(TripsStrings), Name = "TakeOffField")]
        //public string ReturnedFrom { get; set; }
        [ExportToExcel(ResourceType = typeof(FlightsStrings), HeaderName = "ID")]
        [Display(ResourceType = typeof(FlightsStrings), Name = "ID")]
        public int FlightID { get; set; }

        [ExportToExcel(ResourceType = typeof(FlightsStrings), HeaderName = "DepartureDate")]
        [Display(ResourceType = typeof(FlightsStrings), Name = "DepartureDate")]
        public DateTime FlightDate { get; set; }

        [ExportToExcel(ResourceType = typeof(FlightsStrings), HeaderName = "AirlineName")]
        [Display(ResourceType = typeof(FlightsStrings), Name = "AirlineName")]
        public string AirlineName { get; set; }

        [ExportToExcel(ResourceType = typeof(ReportsStrings), HeaderName = "FlightDeparture")]
        [Display(ResourceType = typeof(ReportsStrings), Name = "FlightDeparture")]
        public string FlightDestenation { get; set; }

        [ExportToExcel(ResourceType = typeof(ReportsStrings), HeaderName = "FlightLanded")]
        [Display(ResourceType = typeof(ReportsStrings), Name = "FlightLanded")]
        public string FlightReturnField { get; set; }

        [ExportToExcel(ResourceType = typeof(ReportsStrings), HeaderName = "TakeOfFromIsrael")]
        [Display(ResourceType = typeof(ReportsStrings), Name = "TakeOfFromIsrael")]
        public string TakeOffIsrael { get; set; }

        [ExportToExcel(ResourceType = typeof(ExpeditionStrings), HeaderName = "ID")]
        [Display(ResourceType = typeof(ExpeditionStrings), Name = "ID")]
        public int ExpeditionID { get; set; }

        [ExportToExcel(ResourceType = typeof(FlightsStrings), HeaderName = "NumberOfPassengers")]
        [Display(ResourceType = typeof(FlightsStrings), Name = "NumberOfPassengers")]
        public int NumberOfPassengers { get; set; }

        [ExportToExcel(ResourceType = typeof(ExpeditionStrings), HeaderName = "RegisteredPassengers")]
        [Display(ResourceType = typeof(ExpeditionStrings), Name = "RegisteredPassengers")]
        public int RegisteredPassengers { get; set; }

    }

    public class FlightsReportMainViewModel
    {
        //Properties
        public List<FlightsReportViewModel> FlightsReportViewModels { get; set; }

        //Fliters
        [Display(ResourceType = typeof(CommonStrings), Name = "FromDate")]
        public DateTime FilterFromDate { get; set; }

        [Display(ResourceType = typeof(CommonStrings), Name = "To")]
        public DateTime FilterToDate { get; set; }

        public FlightsReportMainViewModel()
        {
            FlightsReportViewModels = new List<FlightsReportViewModel>();

            FilterFromDate = DateTime.Now.AddMonths(-6);
            FilterToDate = DateTime.Now.AddMonths(6);
        }
    }
}