﻿using Endilo.ExportToExcel;
using FlightScheduling.Language;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FlightScheduling.Web.Areas.Administration.Models.Reports
{
    public class SchoolGlobalRemarksViewModel
    {
        public int ID { get; set; }

        public int SchoolID { get; set; }

        [ExportToExcel(ResourceType = typeof(ReportsStrings), HeaderName = "CsvComment")]
        [Display(ResourceType = typeof(ReportsStrings), Name = "CsvComment")]
        public string Notes { get; set; }

        [ExportToExcel(ResourceType = typeof(ReportsStrings), HeaderName = "CsvComment")]
        [Display(ResourceType = typeof(ReportsStrings), Name = "CsvComment")]
        public DateTime UpdateDate { get; set; }

        [ExportToExcel(ResourceType = typeof(SchoolsStrings), HeaderName = "InstituteID")]
        [Display(ResourceType = typeof(SchoolsStrings), Name = "InstituteID")]
        public int InstituteID { get; set; }

        [ExportToExcel(ResourceType = typeof(SchoolsStrings), HeaderName = "Name")]
        [Display(ResourceType = typeof(SchoolsStrings), Name = "Name")]
        public string SchoolName { get; set; }

        public string Firstname { get; set; }

        public string Lastname { get; set; }

        [ExportToExcel(ResourceType = typeof(CommonStrings), HeaderName = "FullName")]
        [Display(ResourceType = typeof(CommonStrings), Name = "FullName")]
        public string FullName => $"{Firstname} {Lastname}";

    }

    public class SchoolGlobalRemarkReportViewModel
    {
        public IEnumerable<SchoolGlobalRemarksViewModel> Items { get; set; }

        public SchoolGlobalRemarkReportViewModel()
        {
            Items = new List<SchoolGlobalRemarksViewModel>();
        }

    }
}