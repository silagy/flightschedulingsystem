﻿using Endilo.ExportToExcel;
using Flight.DAL.FlightDB.Methods;
using FlightScheduling.DAL;
using FlightScheduling.Language;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FlightScheduling.Web.Areas.Administration.Models.Reports
{
    public class StudentsInExpeditionReport
    {
        [ExportToExcel(ResourceType = typeof(ReportsStrings), HeaderName = "csvStudentFirstName")]
        [Display(ResourceType = typeof(ReportsStrings), Name = "csvStudentFirstName")]
        public string StudentFirstName { get; set; }

        [ExportToExcel(ResourceType = typeof(ReportsStrings), HeaderName = "csvStudentLastName")]
        [Display(ResourceType = typeof(ReportsStrings), Name = "csvStudentLastName")]
        public string StudentLastName { get; set; }

        public string FullName => $"{StudentFirstName} {StudentLastName}";

        [ExportToExcel(ResourceType = typeof(ReportsStrings), HeaderName = "csvPassportID")]
        [Display(ResourceType = typeof(ReportsStrings), Name = "csvPassportID")]
        public string PassportNumber { get; set; }

        [ExportToExcel(ResourceType = typeof(ReportsStrings), HeaderName = "csvParticipantID")]
        [Display(ResourceType = typeof(ReportsStrings), Name = "csvParticipantID")]
        public string ParticipantID { get; set; }

        public int? ParticipantType { get; set; }

        public string RoleName { get; set; }

        [ExportToExcel(ResourceType = typeof(ReportsStrings), HeaderName = "csvParticipantTypeType")]
        [Display(ResourceType = typeof(ReportsStrings), Name = "csvParticipantTypeType")]
        public string ParticipantTypeEnum
        {
            get
            {
                if (ParticipantType != null)
                {
                    return ((eParticipantType)this.ParticipantType).GetEnumDescription();
                }else if (!String.IsNullOrEmpty(this.RoleName))
                {
                    return this.RoleName;
                }
                return String.Empty;
            }
        }

        [ExportToExcel(ResourceType = typeof(ReportsStrings), HeaderName = "CsvGroupID")]
        [Display(ResourceType = typeof(ReportsStrings), Name = "CsvGroupID")]
        public string GroupExternalID { get; set; }

        [ExportToExcel(ResourceType = typeof(ReportsStrings), HeaderName = "CsvSchoolName")]
        [Display(ResourceType = typeof(ReportsStrings), Name = "CsvSchoolName")]
        public string SchoolName { get; set; }

        public int? Region { get; set; }

        [ExportToExcel(ResourceType = typeof(ReportsStrings), HeaderName = "csvSchoolRegion")]
        [Display(ResourceType = typeof(ReportsStrings), Name = "csvSchoolRegion")]
        public string RegionEnum
        {
            get
            {
                if (this.Region != null)
                {
                    return ((eSchoolRegion)this.Region).GetEnumDescription();
                }
                return String.Empty;
            }
        }
    }

    public class StudentsInExpeditionViewReport
    {
        [Display(ResourceType = typeof(CommonStrings), Name = "FromDate")]
        public DateTime StartDate { get; set; }

        [Display(ResourceType = typeof(CommonStrings), Name = "To")]
        public DateTime EndDate { get; set; }

        [Display(ResourceType = typeof(GroupsStrings), Name = "ParticipantID")]
        public string ParticipantID { get; set; }

        [Display(ResourceType = typeof(AccountStrings), Name = "FullName")]
        public string ParticipantName { get; set; }

        public List<StudentsInExpeditionReport> Items { get; set; }

        public StudentsInExpeditionViewReport()
        {
            Items = new List<StudentsInExpeditionReport>();
            StartDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            EndDate = StartDate.AddMonths(1);
        }
    }
}