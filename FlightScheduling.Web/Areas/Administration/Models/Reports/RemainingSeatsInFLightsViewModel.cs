﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Endilo.ExportToExcel;
using FlightScheduling.DAL;
using FlightScheduling.Language;

namespace FlightScheduling.Web.Areas.Administration.Models.Reports
{
    public class RemainingSeatsInFLightsViewModel
    {
        //Properties
        [ExportToExcel(ResourceType = typeof(TripsStrings), HeaderName = "ID")]
        [Display(ResourceType = typeof(TripsStrings), Name = "ID")]
        public int TripID { get; set; }

        [ExportToExcel(ResourceType = typeof(ReportsStrings), HeaderName = "TripStartDate")]
        [Display(ResourceType = typeof(ReportsStrings), Name = "TripStartDate")]
        public DateTime DepartureDate { get; set; }

        public int DestinationID { get; set; }

        [ExportToExcel(ResourceType = typeof(ReportsStrings), HeaderName = "TripDestination")]
        [Display(ResourceType = typeof(ReportsStrings), Name = "TripDestination")]
        public string TripDestination => ((eFlightsFields)DestinationID).GetEnumDescription();

        [ExportToExcel(ResourceType = typeof(ReportsStrings), HeaderName = "BusesPerDayCount")]
        [Display(ResourceType = typeof(ReportsStrings), Name = "BusesPerDayCount")]
        public int BasesPerDay { get; set; }

        [ExportToExcel(ResourceType = typeof(FlightsStrings), HeaderName = "NumberOfPassengers")]
        [Display(ResourceType = typeof(FlightsStrings), Name = "NumberOfPassengers")]
        public int TotalPassengers { get; set; }

        [ExportToExcel(ResourceType = typeof(FlightsStrings), HeaderName = "AssignPassengers")]
        [Display(ResourceType = typeof(FlightsStrings), Name = "AssignPassengers")]
        public int BookedPassengers { get; set; }

        [ExportToExcel(ResourceType = typeof(ReportsStrings), HeaderName = "AvailableSeats")]
        [Display(ResourceType = typeof(ReportsStrings), Name = "AvailableSeats")]
        public int AvailableSeats => TotalPassengers - BookedPassengers;
    }

    public class FlightSeatsAvailabilityReportViewModel
    {
        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(CommonStrings), Name = "FromDate")]
        public DateTime StartDate { get; set; }

        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(CommonStrings), Name = "To")]
        public DateTime EndDate { get; set; }

        public List<RemainingSeatsInFLightsViewModel> Data { get; set; }

        public FlightSeatsAvailabilityReportViewModel()
        {
            StartDate = new DateTime(DateTime.Now.Year, 1, 1);
            EndDate = new DateTime(DateTime.Now.Year, 12, 31);
            Data = new List<RemainingSeatsInFLightsViewModel>();
        }
    }
}