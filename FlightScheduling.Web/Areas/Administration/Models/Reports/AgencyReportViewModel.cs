﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Endilo.ExportToExcel;
using FlightScheduling.DAL;
using FlightScheduling.Language;
using Otakim.Language;

namespace FlightScheduling.Web.Areas.Administration.Models.Reports
{
    public class AgencyReportContainerViewModel
    {
        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(CommonStrings), Name = "FromDate")]
        public DateTime StartDate { get; set; }

        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(CommonStrings), Name = "To")]
        public DateTime EndDate { get; set; }

        public List<AgencyReportViewModel> Data { get; set; }

        public AgencyReportContainerViewModel()
        {
            StartDate = new DateTime(DateTime.Now.Year, 1, 1);
            EndDate = new DateTime(DateTime.Now.Year, 12, 31);
            Data = new List<AgencyReportViewModel>();
        }
    }


    public class AgencyReportViewModel
    {
        [ExportToExcel(ResourceType = typeof(SchoolsStrings), HeaderName = "ID")]
        public int SchoolID { get; set; }

        [ExportToExcel(ResourceType = typeof(SchoolsStrings), HeaderName = "InstituteID")]
        public int InstituteID { get; set; }

        [ExportToExcel(ResourceType = typeof(SchoolsStrings), HeaderName = "Name")]
        public string SchoolName { get; set;}

        public int RegionID { get; set; }

        [ExportToExcel(ResourceType = typeof(SchoolsStrings), HeaderName = "Region")]
        [Display(ResourceType = typeof(SchoolsStrings), Name = "Region")]
        public string Region => ((eSchoolRegion)RegionID).GetEnumDescription();

        [ExportToExcel(ResourceType = typeof(TripsStrings), HeaderName = "DepartureDate")]
        public DateTime DepartureDate { get; set; }

        [ExportToExcel(ResourceType = typeof(TripsStrings), HeaderName = "ReturnDate")]
        public DateTime ReturnDate { get; set; }

        [ExportToExcel(ResourceType = typeof(TripsStrings), HeaderName = "Destenation")]
        public string Destenation => ((eFlightsFields) this.DestinationID).GetDescription();

        public int DestinationID { get; set; }

        [ExportToExcel(ResourceType = typeof(TripsStrings), HeaderName = "TakeOffField")]
        public string ReturnedFrom => ((eFlightsFields)this.ReturingField).GetDescription();

        public int ReturingField { get; set; }

        [ExportToExcel(ResourceType = typeof(TripsStrings), HeaderName = "ID")]
        public int TripID { get; set; }

        [ExportToExcel(ResourceType = typeof(ExpeditionStrings), HeaderName = "BusesRequiredPerDay")]
        public int BusesRequiredPerDay { get; set; }

        [ExportToExcel(ResourceType = typeof(ExpeditionStrings), HeaderName = "RegisteredPassengers")]
        public int RegisteredPassengers { get; set; }

        [ExportToExcel(ResourceType = typeof(GroupFormsStrings), HeaderName = "CostPerStudent")]
        public double CostPerStudent { get; set; }

        [ExportToExcel(ResourceType = typeof(ExpeditionStrings), HeaderName = "Status")]
        public string Status => ((eExpeditionStatus)this.ExpeditionStatus).GetDescription();

        public int ExpeditionStatus { get; set; }
        public eExpeditionStatus ExpeditionStatusEnum => (eExpeditionStatus)this.ExpeditionStatus;

        [ExportToExcel(ResourceType = typeof(ExpeditionStrings), HeaderName = "ID")]
        public int ExpeditionID { get; set; }

        [ExportToExcel(ResourceType = typeof(ExpeditionStrings), HeaderName = "Agency")]
        public string AgencyName { get; set; }

        [ExportToExcel(ResourceType = typeof(GroupsStrings), HeaderName = "ExpeditionID")]
        public string GroupExternalID { get; set; }

    }
}