﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CsvHelper.Configuration;
using Endilo.ExportToExcel;
using Flight.DAL.FlightDB.Methods;
using FlightScheduling.DAL;
using FlightScheduling.Language;
using FlightScheduling.Language.CustomeExtentions;
using FlightScheduling.Web.Areas.Administration.Models.Common;
using FlightScheduling.Web.Areas.Administration.Models.Groups;
using FlightScheduling.Web.Infrastructure;
using LinqToExcel.Attributes;

namespace FlightScheduling.Web.Areas.Administration.Models.Instructors
{

    public class InstructorExcelViewMode
    {
        [ExportToExcel(ResourceType = typeof(InstructorsStrings), HeaderName = "InstructorID")]
        public string InstructorID { get; set; }

        [ExportToExcel(ResourceType = typeof(InstructorsStrings), HeaderName = "FullNameHE")]
        public string FullNameHe { get; set; }

        [ExportToExcel(ResourceType = typeof(InstructorsStrings), HeaderName = "FullNameEN")]
        public string FullNameEN { get; set; }

        [ExportToExcel(ResourceType = typeof(InstructorsStrings), HeaderName = "PassportID")]
        public string PassportID { get; set; }

        [ExportToExcel(ResourceType = typeof(InstructorsStrings), HeaderName = "InstructorRole")]
        public string InstructorRole { get; set; }
    }
    public class InstructorViewModel
    {
        public int ID { get; set; }

        [ExcelColumn("תעודת זהות")]
        [Display(ResourceType = typeof(InstructorsStrings), Name = "InstructorID")]
        [ExportToExcel(ResourceType = typeof(InstructorsStrings), HeaderName = "InstructorID")]
        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        [IsraeliIDValidator]
        public string InstructorID { get; set; }

        [ExcelColumn("שם פרטי בעברית")]
        [Display(ResourceType = typeof(InstructorsStrings), Name = "FirstNameHE")]
        [ExportToExcel(ResourceType = typeof(InstructorsStrings), HeaderName = "FirstNameHE")]
        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        public string FirstNameHE { get; set; }

        [ExcelColumn("שם משפחה בעברית")]
        [ExportToExcel(ResourceType = typeof(InstructorsStrings), HeaderName = "LastNameHE")]
        [Display(ResourceType = typeof(InstructorsStrings), Name = "LastNameHE")]
        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        public string LastNameHE { get; set; }

        [ExcelColumn("שם פרטי באנגלית")]
        [ExportToExcel(ResourceType = typeof(InstructorsStrings), HeaderName = "FirstNameEN")]
        [Display(ResourceType = typeof(InstructorsStrings), Name = "FirstNameEN")]
        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        public string FirstNameEN { get; set; }

        [ExcelColumn("שם משפחה באנגלית")]
        [ExportToExcel(ResourceType = typeof(InstructorsStrings), HeaderName = "LastNameEN")]
        [Display(ResourceType = typeof(InstructorsStrings), Name = "LastNameEN")]
        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        public string LastNameEN { get; set; }

        [ExcelColumn("מספר דרכון")]
        [ExportToExcel(ResourceType = typeof(InstructorsStrings), HeaderName = "PassportID")]
        [Display(ResourceType = typeof(InstructorsStrings), Name = "PassportID")]
        public string PassportID { get; set; }

        [ExcelColumn("כתובת מגורים")]
        [ExportToExcel(ResourceType = typeof(InstructorsStrings), HeaderName = "Address")]
        [Display(ResourceType = typeof(InstructorsStrings), Name = "Address")]
        public string Address { get; set; }

        [ExcelColumn("עיר")]
        [ExportToExcel(ResourceType = typeof(InstructorsStrings), HeaderName = "City")]
        [Display(ResourceType = typeof(InstructorsStrings), Name = "City")]
        public string City { get; set; }

        [ExcelColumn("מיקוד")]
        [ExportToExcel(ResourceType = typeof(InstructorsStrings), HeaderName = "ZipCode")]
        [Display(ResourceType = typeof(InstructorsStrings), Name = "ZipCode")]
        public string ZipCode { get; set; }

        [ExcelColumn("טלפון בבית")]
        [ExportToExcel(ResourceType = typeof(InstructorsStrings), HeaderName = "HomePhone")]
        [Display(ResourceType = typeof(InstructorsStrings), Name = "HomePhone")]
        public string HomePhone { get; set; }

        [ExcelColumn("טלפון נייד")]
        [ExportToExcel(ResourceType = typeof(InstructorsStrings), HeaderName = "CellPhone")]
        [Display(ResourceType = typeof(InstructorsStrings), Name = "CellPhone")]
        public string CellPhone { get; set; }

        [ExcelColumn("כתובת מייל")]
        [ExportToExcel(ResourceType = typeof(AccountStrings), HeaderName = "EmailAddress")]
        [Display(ResourceType = typeof(AccountStrings), Name = "EmailAddress")]
        public string Email { get; set; }

        [Display(ResourceType = typeof(InstructorsStrings), Name = "InstructorRole")]
        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        public int InstructorRole { get; set; }

        [ExportToExcel(ResourceType = typeof(InstructorsStrings), HeaderName = "InstructorRole")]
        public string InstructorRoleType => InstructorRoleModel.Name;

        public string ImageURL { get; set; }

        public CustomSelectList Roles { get; set; }

        [ExcelColumn("סטאטוס ארגוני")]
        [ExportToExcel(ResourceType = typeof(InstructorsStrings), HeaderName = "OrganizationalStatus")]
        [Display(ResourceType = typeof(InstructorsStrings), Name = "OrganizationalStatus")]
        public eOrganizationalStatus OrganizationalStatus { get; set; }

        public InstructorRoleViewModel InstructorRoleModel { get; set; }

        [Display(ResourceType = typeof(CommonStrings), Name = "CreationDate")]
        public DateTime CreationDate { get; set; }

        [ExportToExcel(ResourceType = typeof(InstructorsStrings), HeaderName = "ValidityStartDate")]
        [Display(ResourceType = typeof(InstructorsStrings), Name = "ValidityStartDate")]
        public DateTime? ValidityStartDate { get; set; }

        [ExportToExcel(ResourceType = typeof(InstructorsStrings), HeaderName = "ValidityEndTime")]
        [Display(ResourceType = typeof(InstructorsStrings), Name = "ValidityEndTime")]
        public DateTime? ValidityEndTime { get; set; }

        [ExportToExcel(ResourceType = typeof(InstructorsStrings), HeaderName = "InstructorValidity")]
        [Display(ResourceType = typeof(InstructorsStrings), Name = "InstructorValidity")]
        public eInstructorValidity InstructorValidity { get; set; }

        [ExportToExcel(ResourceType = typeof(InstructorsStrings), HeaderName = "CertificateID")]
        [Display(ResourceType = typeof(InstructorsStrings), Name = "CertificateID")]
        public string CertificateID { get; set; }

        public int? SEX { get; set; }

        [ExcelColumn("מיגדר")]
        [Display(ResourceType = typeof(GroupsStrings), Name = "SEX")]
        [ExportToExcel(ResourceType = typeof(GroupsStrings), HeaderName = "SEX")]
        public eParticipantsSex? SEXeum {
            get => SEX != null? (eParticipantsSex)SEX: eParticipantsSex.None;
            set => SEX = (int)value; 
}

        public bool UpdateOperation { get; set; }
        public bool UpdateSensativeData { get; set; }

        public int? TrainingID { get; set; }

        public string FullNameHE
        {
            get { return String.Format("{0} {1}", this.FirstNameHE, this.LastNameHE); }
        }

        public string FullNameEN
        {
            get { return String.Format("{0} {1}", this.FirstNameEN, this.LastNameEN); }
        }

        public InstructorViewModel()
        {
            UpdateOperation = false;
            UpdateSensativeData = false;
            Roles = new CustomSelectList();
        }
    }

    public class ImportRegisterInstructorLog
    {
        public eImportInstructorOperationLog OperationLog { get; set; }
        public eImportInstructorOperationReason? OperationReason { get; set; }
        public InstructorViewModel Instructor { get; set; }
    }

    public enum eImportInstructorOperationLog
    {
        [Display(ResourceType = typeof(CommonStrings), Name = "Success")]
        [LocalizedDescription("Success", typeof(CommonStrings))]
        Success,
        [Display(ResourceType = typeof(CommonStrings), Name = "Fail")]
        [LocalizedDescription("Fail", typeof(CommonStrings))]
        Fail
    }

    public enum eImportInstructorOperationReason
    {
        [Display(ResourceType = typeof(InstructorsStrings), Name = "InstructorExists")]
        [LocalizedDescription("InstructorExists", typeof(InstructorsStrings))]
        InstructorExists,
        [Display(ResourceType = typeof(CommonStrings), Name = "Other")]
        [LocalizedDescription("Other", typeof(CommonStrings))]
        Other,
        [Display(ResourceType = typeof(InstructorsStrings), Name = "FirstNameHeNull")]
        [LocalizedDescription("FirstNameHeNull", typeof(InstructorsStrings))]
        FirstNameHeIsNull,
        [Display(ResourceType = typeof(InstructorsStrings), Name = "LastNameHeNull")]
        [LocalizedDescription("LastNameHeNull", typeof(InstructorsStrings))]
        LastNameNameHeIsNull,
        [Display(ResourceType = typeof(InstructorsStrings), Name = "FirstNameNameEnNull")]
        [LocalizedDescription("FirstNameNameEnNull", typeof(InstructorsStrings))]
        FirstNameNameEnIsNull,
        [Display(ResourceType = typeof(InstructorsStrings), Name = "LastNameNameEnNull")]
        [LocalizedDescription("LastNameNameEnNull", typeof(InstructorsStrings))]
        LastNameNameEnIsNull,
        [Display(ResourceType = typeof(InstructorsStrings), Name = "PassportIDIsNull")]
        [LocalizedDescription("PassportIDIsNull", typeof(InstructorsStrings))]
        PassportIDIsNull,
        [Display(ResourceType = typeof(InstructorsStrings), Name = "InstructorIsRegisteredToTraining")]
        [LocalizedDescription("InstructorIsRegisteredToTraining", typeof(InstructorsStrings))]
        InstructorIsRegisteredToTraining,
        [Display(ResourceType = typeof(InstructorsStrings), Name = "InstructorIDIsNull")]
        [LocalizedDescription("InstructorIDIsNull", typeof(InstructorsStrings))]
        InstructorIDIsNull,
        [Display(ResourceType = typeof(InstructorsStrings), Name = "InstructorIDIsNotValid")]
        [LocalizedDescription("InstructorIDIsNotValid", typeof(InstructorsStrings))]
        InstructorIDIsNotValid,

    }

    public class ImportRegistrationLogList
    {
        public List<ImportRegisterInstructorLog> InstructorsLog { get; set; }

        public ImportRegistrationLogList()
        {
            InstructorsLog = new List<ImportRegisterInstructorLog>();
        }
    }


    public class InstructorViewModelCsv : ClassMap<InstructorViewModel>
    {
        public InstructorViewModelCsv()
        {
            Map(m => m.InstructorID).Index(0);
            Map(m => m.FirstNameHE).Index(1);
            Map(m => m.LastNameHE).Index(2);
            Map(m => m.FirstNameEN).Index(3);
            Map(m => m.LastNameEN).Index(4);
            Map(m => m.PassportID).Index(5);
            Map(m => m.Address).Index(6);
            Map(m => m.City).Index(7);
            Map(m => m.ZipCode).Index(8);
            Map(m => m.HomePhone).Index(9);
            Map(m => m.CellPhone).Index(10);
            Map(m => m.Email).Index(11);
        }
    }

    public class ImportInstructorsViewModel
    {
        public int TrainingID { get; set; }
        public int UserID { get; set; }

        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(GroupsStrings), Name = "Attachment")]
        [FileTypeValidation("XLS,XLSX")]
        public HttpPostedFileBase Attachment { get; set; }
    }

    public class InstructorsViewModel
    {
        //Filter Propeties
        [Display(ResourceType = typeof(InstructorsStrings), Name = "RoleName")]
        public int? RoleID { get; set; }

        public CustomSelectList Roles { get; set; }

        [Display(ResourceType = typeof(InstructorsStrings), Name = "InstructorID")]
        public string InstructorID { get; set; }

        [Display(ResourceType = typeof(InstructorsStrings), Name = "LastNameHE")]
        public string LastName { get; set; }

        public bool AllowFilterByRole { get; set; }

        public List<InstructorViewModel> Instructors { get; set; }

        public InstructorsViewModel()
        {
            Instructors = new List<InstructorViewModel>();
            Roles = new CustomSelectList();
            AllowFilterByRole = true;
        }
    }

    public class InstructorFolderViewModel
    {
        //Properties
        public InstructorViewModel InstructorDetails { get; set; }
        public List<InstructorFileViewModel> Files { get; set; }
        public List<InstructorNoteViewModel> Notes { get; set; }
        public List<InstructorTrainingViewModel> Trainings { get; set; }
        public List<InstructorGroupsViewModel> InstructorGroups { get; set; }

        public int HoursAcquired { get; set; }
        public int Percentange { get; set; }

        // Indication if user is already created
        public bool UserIsCreated { get; set; }

        public InstructorFolderViewModel()
        {
            Files = new List<InstructorFileViewModel>();
            Notes = new List<InstructorNoteViewModel>();
            Trainings = new List<InstructorTrainingViewModel>();
            InstructorGroups = new List<InstructorGroupsViewModel>();
            HoursAcquired = 0;
            Percentange = 0;
        }
    }

    public class InstructorGroupsViewModel
    {
        public int InstructorId { get; set; }

        public int GroupId { get; set; }

        [Display(ResourceType = typeof(GroupsStrings), Name = "ExpeditionID")]
        public int GroupExternalId { get; set; }

        [Display(ResourceType = typeof(TripsStrings), Name = "DepartureDate")]
        public DateTime GroupDepartureDate { get; set; }


        public int DestinationId { get; set; }

        [Display(ResourceType = typeof(TripsStrings), Name = "Destenation")]
        public eFlightsFields DestinationName
        {
            get { return (eFlightsFields) this.DestinationId; }
        }

        [Display(ResourceType = typeof(SchoolsStrings), Name = "InstituteID")]
        public int InstituteId { get; set; }

        [Display(ResourceType = typeof(SchoolsStrings), Name = "Name")]
        public string SchoolName { get; set; }
    }

    public class InstructorFileViewModel
    {
        //Properties

        public int ID { get; set; }
        public int InstructorID { get; set; }

        [Display(ResourceType = typeof(GroupsStrings), Name = "FileName")]
        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        public string Name { get; set; }

        public string Url { get; set; }

        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(GroupsStrings), Name = "Attachment")]
        [FileTypeValidation("DOC,DOCX,PDF,XLS,XLSX")]
        public HttpPostedFileBase Attachment { get; set; }

        [Display(ResourceType = typeof(GroupsStrings), Name = "FileComments")]
        public string Comments { get; set; }

        public System.DateTime CreationDate { get; set; }

        public int UserID { get; set; }

        public bool IsUpdate { get; set; }



        public InstructorFileViewModel()
        {
            CreationDate = DateTime.Now;
            IsUpdate = false;
        }
    }

    public class InstructorProfilePictureViewModel
    {
        public string Url { get; set; }

        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        [FileTypeValidation("JPG,JPEG,PNG")]
        [Display(ResourceType = typeof(GroupsStrings), Name = "Attachment")]
        public HttpPostedFileBase Attachment { get; set; }

        public int InstructorID { get; set; }
    }

    public class InstructorNoteViewModel
    {
        //Properties
        [Display(ResourceType = typeof(CommonStrings), Name = "ID")]
        public int ID { get; set; }
        public int UserID { get; set; }

        [Display(ResourceType = typeof(AccountStrings), Name = "FirstName")]
        public string UserFirstName { get; set; }

        [Display(ResourceType = typeof(AccountStrings), Name = "LastName")]
        public string UserLastName { get; set; }
        public int InstructorID { get; set; }

        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        [AllowHtml]
        [Display(ResourceType = typeof(GroupsStrings), Name = "Note")]
        public string Notes { get; set; }
        [Display(ResourceType = typeof(CommonStrings), Name = "UpdateDate")]
        public DateTime UpdateDate { get; set; }
        [Display(ResourceType = typeof(CommonStrings), Name = "CreationDate")]
        public DateTime CreationDate { get; set; }

        public bool UpdateOperation { get; set; }

        public InstructorNoteViewModel()
        {
            UpdateOperation = false;
        }
    }

    public class InstructorGroupViewModel
    {
        //Properties
        public int ID { get; set; }
        [Display(ResourceType = typeof(SchoolsStrings), Name = "Name")]
        public string SchoolName { get; set; }
        public InstructorRoleViewModel InstructorRole { get; set; }
        public InstructorViewModel Instructor { get; set; }
        public GroupViewModel Group { get; set; }

        


    }

    public class GroupInstructorsViewModelShortData
    {
        public int ID { get; set; }
        public int InstructorInternalID { get; set; }
        public string FirstNameHE { get; set; }

        public string LastNameHE { get; set; }

        public string FirstNameEN { get; set; }

        public string LastNameEN { get; set; }
        public string InstructorID { get; set; }
        public string PassportID { get; set; }
        public string RoleName { get; set; }
        public string SchoolName { get; set; }
        public DateTime? ValidityEndTime { get; set; }

        // Role ID as assigend to the group
        public int RoleID { get; set; }

        public eInstructorRoleType RoleEnum => (eInstructorRoleType)RoleID;

        public bool IsGroupManager { get; set; }
        public bool IsDeputyDirector { get; set; }

        [Display(ResourceType = typeof(InstructorsStrings), Name = "InstructorValidity")]
        public eInstructorValidity InstructorValidity
        {
            get
            {
                if (this.ValidityEndTime == null)
                {
                    return eInstructorValidity.NoRecords;
                }
                else if (this.ValidityEndTime >= DateTime.Now)
                {
                    return eInstructorValidity.Valid;
                }
                else
                {
                    return eInstructorValidity.NotValid;
                }
            }
        }

        public string FullNameHE
        {
            get { return String.Format("{0} {1}", this.FirstNameHE, this.LastNameHE); }
        }

        public string FullNameEN
        {
            get { return String.Format("{0} {1}", this.FirstNameEN, this.LastNameEN); }
        }

        public string InstructorPictureUrl
        {
            get
            {
                if (!String.IsNullOrEmpty(ImageURL))
                {
                    //TO DO deal with the image folder
                    return "InstructorProfilePictures/" + InstructorInternalID + "/" + ImageURL;
                }

                return "/images/icons8-user-25.png";
            }
        }
        public string ImageURL { get; set; }
    }

    public class UpdateCertificateViewModel
    {
        public int InstructorID { get; set; }
        [Display(ResourceType = typeof(InstructorsStrings), Name = "CertificateID")]
        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        public string CertificateID { get; set; }
    }

    public class UpdateInstructorExpiation
    {
        public int InstructorID { get; set; }

        [Display(ResourceType = typeof(InstructorsStrings), Name = "CertificateID")]
        public string CertificateID { get; set; }

        [Display(ResourceType = typeof(InstructorsStrings), Name = "ValidityStartDate")]
        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        public DateTime ValidityStartDate { get; set; }

        [Display(ResourceType = typeof(InstructorsStrings), Name = "ValidityEndTime")]
        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        public DateTime ValidityEndTime { get; set; }

        public CustomSelectList Roles { get; set; }

        [Display(ResourceType = typeof(InstructorsStrings), Name = "InstructorRole")]
        public int InstructorRoleID { get; set; }

        public UpdateInstructorExpiation()
        {
            Roles = new CustomSelectList();
        }
    }

    public class UpdateTrainingInstructorsExpirationDates
    {
        public int TrainingID { get; set; }

        [Display(ResourceType = typeof(InstructorsStrings), Name = "ValidityStartDate")]
        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        public DateTime ValidityStartDate { get; set; }

        [Display(ResourceType = typeof(InstructorsStrings), Name = "ValidityEndTime")]
        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        public DateTime ValidityEndTime { get; set; }


        public UpdateTrainingInstructorsExpirationDates()
        {
        }
    }

    


}