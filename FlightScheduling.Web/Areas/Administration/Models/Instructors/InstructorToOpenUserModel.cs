﻿namespace FlightScheduling.Web.Areas.Administration.Models.Instructors
{
    public class InstructorToOpenUserModel
    {
        public int ID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string InstructorID { get; set; }
        public string Email { get; set; }
    }
}