﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using CsvHelper.Configuration;
using FlightScheduling.DAL;
using FlightScheduling.Language;

namespace FlightScheduling.Web.Areas.Administration.Models.Instructors
{
    public class InstructorRoleViewModel
    {
        //Properties
        public int ID { get; set; }

        [Display(ResourceType = typeof(InstructorsStrings), Name = "RoleName")]
        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        public string Name { get; set; }

        [Display(ResourceType = typeof(InstructorsStrings), Name = "RequiredHours")]
        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        public int? RequiredHours { get; set; }

        [Display(ResourceType = typeof(InstructorsStrings), Name = "ExpirationDaysRule")]
        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        public int? ExpirationDaysRule { get; set; }

        [Display(ResourceType = typeof(InstructorsStrings), Name = "StartingValidity")]
        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        public eStartingValidity StartingValidity { get; set; }

        [Display(ResourceType = typeof(InstructorsStrings), Name = "StartingRenewal")]
        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        public int? StartingRenewal { get; set; }

        [Display(ResourceType = typeof(InstructorsStrings), Name = "AutomaticRenewal")]
        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        public bool AutomaticRenewal { get; set; }

        [Display(ResourceType = typeof(InstructorsStrings), Name = "IsManager")]
        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        public bool IsManager { get; set; }

        [Display(ResourceType = typeof(InstructorsStrings), Name = "AllowChangingRole")]
        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        public bool AllowChangingRole { get; set; }
        
        [Display(ResourceType = typeof(InstructorsStrings), Name = "AllowLogin")]
        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        public bool AllowLogin { get; set; }


        public string Language { get; set; }

        public bool IsUpdate { get; set; }
        public string ActionResult { get; set; }
    }

    public class InstructorRolesViewModel
    {
        public List<InstructorRoleViewModel> Roles { get; set; }

        public InstructorRolesViewModel()
        {
            this.Roles = new List<InstructorRoleViewModel>();
        }
    }


}