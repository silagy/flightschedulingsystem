﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FlightScheduling.DAL;
using FlightScheduling.Language;
using FlightScheduling.Web.Areas.Administration.Models.Common;

namespace FlightScheduling.Web.Areas.Administration.Models.Instructors
{
    public class TrainingViewModel
    {
        //Properties

        public int ID { get; set; }

        [Display(ResourceType = typeof(InstructorsStrings), Name = "TrainingName")]
        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        public string Name { get; set; }

        [Display(ResourceType = typeof(InstructorsStrings), Name = "TrainingDate")]
        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        public DateTime TrainingDate { get; set; }

        [Display(ResourceType = typeof(InstructorsStrings), Name = "TrainingLocation")]
        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        public eTrainingLocation TrainingLocation { get; set; }

        [Display(ResourceType = typeof(InstructorsStrings), Name = "TrainingHours")]
        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        public int? TrainingHours { get; set; }

        [Display(ResourceType = typeof(InstructorsStrings), Name = "ValidationType")]
        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        public eTrainingType TrainingType { get; set; }

        [Display(ResourceType = typeof(InstructorsStrings), Name = "IsCompleted")]
        public bool IsCompleted { get; set; }

        [Display(ResourceType = typeof(InstructorsStrings), Name = "RoleName")]
        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        public eInstructorRoleType RoleID { get; set; }

        [Display(ResourceType = typeof(SchoolsStrings), Name = "Region")]
        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        public eSchoolRegion? TrainingRegion { get; set; }

        [Display(ResourceType = typeof(InstructorsStrings), Name = "TrainingCategory")]
        public int? TrainingTypeID { get; set; }

        public string TrainingTypeName { get; set; }


        public CustomSelectList Roles { get; set; }

        public InstructorRoleViewModel InstructorRole { get; set; }

        public CustomSelectList TrainingTypes { get; set; }

        public int CreatorID { get; set; }

        public bool IsUpdate { get; set; }

        public TrainingViewModel()
        {
            Roles = new CustomSelectList();
            TrainingTypes = new CustomSelectList();
        }

    }

    public class TrainingsViewModel
    {
        [Display(ResourceType = typeof(InstructorsStrings), Name = "RoleName")]
        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        public int? RoleID { get; set; }

        public CustomSelectList Roles { get; set; }

        [Display(ResourceType = typeof(CommonStrings), Name = "FromDate")]
        public DateTime FilterFromDate { get; set; }

        [Display(ResourceType = typeof(CommonStrings), Name = "To")]
        public DateTime FilterToDate { get; set; }
        [Display(ResourceType = typeof(InstructorsStrings), Name = "ShowCompletedTraining")]
        public bool CompletedTraining { get; set; }

        [Display(ResourceType = typeof(InstructorsStrings), Name = "TrainingLocation")]
        public eTrainingLocation? TrainingLocation { get; set; }

        [Display(ResourceType = typeof(InstructorsStrings), Name = "TrainingCategory")]
        public int? TrainingTypeID { get; set; }

        public CustomSelectList TrainingTypes { get; set; }

        [Display(ResourceType = typeof(SchoolsStrings), Name = "Region")]
        public eSchoolRegion? Region { get; set; }

        public List<TrainingViewModel> Trainings { get; set; }

        public TrainingsViewModel()
        {
            Trainings = new List<TrainingViewModel>();
            Roles = new CustomSelectList();
            TrainingTypes = new CustomSelectList();
            FilterFromDate = new DateTime(DateTime.Now.Year - 1, 1, 1);
            FilterToDate = new DateTime(DateTime.Now.Year, 12, 31);
            CompletedTraining = false;
        }
    }

    public class TrainingFolderViewModel
    {
        //Properties
        public TrainingViewModel TrainingDetails { get; set; }

        public List<InstructorTrainingViewModel> Instructors { get; set; }

        //Quick registration for instructors
        public InstructorRegistrationToTraining InstructorRegistrationToTraining { get; set; }


        public TrainingFolderViewModel()
        {
            Instructors = new List<InstructorTrainingViewModel>();
            InstructorRegistrationToTraining = new InstructorRegistrationToTraining();
        }
    }

    public class InstructorTrainingViewModel
    {
        //Properties
        public int ID { get; set; }

        public int TrainingID { get; set; }

        public int InstructorID { get; set; }

        public bool CountToValidity { get; set; }

        public TrainingViewModel TrainingDetails { get; set; }
        public InstructorViewModel InstructorDetails { get; set; }

    }

    public class InstructorRegistrationToTraining
    {
        [Display(ResourceType = typeof(InstructorsStrings), Name = "RegisterInstructor")]
        public CustomSelectList InstructorsRegistration { get; set; }

        public int TrainingID { get; set; }
        public int InstructorID { get; set; }

        public InstructorRegistrationToTraining()
        {
            InstructorsRegistration = new CustomSelectList();
        }
    }

    public class CompleteTrainingViewModel
    {
        public int TrainingID { get; set; }
    }
}