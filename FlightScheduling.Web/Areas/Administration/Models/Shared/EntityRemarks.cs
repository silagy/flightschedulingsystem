﻿using FlightScheduling.DAL;
using FlightScheduling.Language;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FlightScheduling.Web.Areas.Administration.Models.Shared
{
    public class EntityRemarkViewModel
    {
        // Properties
        public int ID { get; set; }
        public int EntityID { get; set; }

        public int EntityType { get; set; }

        [Display(ResourceType = typeof(CommonStrings), Name = "EntityType")]
        public eEntityType EntityTypeEnum { get => (eEntityType)EntityType; set => EntityType = (int)value; }

        [Display(ResourceType = typeof(CommonStrings), Name = "UpdateDate")]
        public DateTime UpdateDate { get; set; }

        [Display(ResourceType = typeof(GroupsStrings), Name = "Note")]
        public string Notes { get; set; }

        public int UserID { get; set; }

        [Display(ResourceType = typeof(AccountStrings), Name = "FirstName")]
        public string Firstname { get; set; }

        [Display(ResourceType = typeof(AccountStrings), Name = "LastName")]
        public string Lastname { get; set; }

        public DateTime CreationDate { get; set; }

        public bool UpdateOperation { get; set; }

        public EntityRemarkViewModel()
        {
            UpdateOperation = false;
        }
    }

    public class EntityRemarksViewModel
    {
        public IEnumerable<EntityRemarkViewModel> Remarks { get; set; }
        public eEntityType EntityType { get; set; }
        public int EntityID { get; set; }
    }
}