﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Endilo.ExportToExcel;
using FlightScheduling.Language;
using FlightScheduling.Language.CustomeExtentions;
using FlightScheduling.Web.Areas.Administration.Models.Common;
using FlightScheduling.Web.Infrastructure;
using LinqToExcel.Attributes;

namespace FlightScheduling.Web.Areas.Administration.Models.Flights
{
    public class FlightsViewModel
    {
        //Properties
        public List<FlightViewModel> FlightViewModels { get; set; }

        //Fliters
        [Display(ResourceType = typeof(CommonStrings), Name = "FromDate")]
        public DateTime FilterFromDate { get; set; }

        [Display(ResourceType = typeof(CommonStrings), Name = "To")]
        public DateTime FilterToDate { get; set; }

        public FlightsViewModel()
        {
            FlightViewModels = new List<FlightViewModel>();
        }
    }

    public class FlightViewModel : BasicDetails 
    {
       
        
        //Properties

        [Display(ResourceType = typeof(FlightsStrings), Name = "ID")]
        [ExportToExcel(ResourceType = typeof(FlightsStrings), HeaderName = "ID")]
        [ExcelColumn("מזהה טיסה")]
        public int ID { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(FlightsStrings), Name = "DepartureDate")]
        [ExportToExcel(ResourceType = typeof(FlightsStrings), HeaderName = "DepartureDate")]
        [ExcelColumn("תאריך המראה")]
        public DateTime? DepartureDate { get; set; }

        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(FlightsStrings), Name = "DepartureTime")]
        public string DepartureTime { get; set; }

        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy hh:mm}", ApplyFormatInEditMode = true)]
        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(FlightsStrings), Name = "ArrivalDate")]
        [ExportToExcel(ResourceType = typeof(FlightsStrings), HeaderName = "ArrivalDate")]
        [ExcelColumn("תאריך הגעה")]
        public DateTime? ArrivalDate { get; set; }

        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(FlightsStrings), Name = "ArrivalTime")]
        public string ArrivalHours { get; set; }

        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(FlightsStrings), Name = "DepartureField")]
        [ExportToExcel(ResourceType = typeof(FlightsStrings), HeaderName = "DepartureField")]
        [ExcelColumn("שדה המראה")]
        public FlightViewModel.FlightsFields DepartureField { get; set; }

        public bool TakeOffFromIsrael { get; set; }

        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(FlightsStrings), Name = "DestenationField")]
        [ExportToExcel(ResourceType = typeof(FlightsStrings), HeaderName = "DestenationField")]
        [ExcelColumn("יעד טיסה")]
        public FlightsFields DestenationField { get; set; }

        [Range(1, int.MaxValue, ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "GreaterThanOne")]
        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(FlightsStrings), Name = "NumberOfPassengers")]
        [ExportToExcel(ResourceType = typeof(FlightsStrings), HeaderName = "NumberOfPassengers")]
        [ExcelColumn("מספר המושבים")]
        public int NumberOfPassengers { get; set; }

        [ExportToExcel(ResourceType = typeof(FlightsStrings), HeaderName = "AirlineName")]
        [Display(ResourceType = typeof(FlightsStrings), Name = "AirlineName")]
        [ExcelColumn("חברת תעופה")]
        public string AirlineName { get; set; }

        [Display(ResourceType = typeof(FlightsStrings), Name = "FlightNumber")]
        [ExportToExcel(ResourceType = typeof(FlightsStrings), HeaderName = "FlightNumber")]
        [ExcelColumn("מספר טיסה")]
        public string FlightNumber { get; set; }

        [Display(ResourceType = typeof(FlightsStrings), Name = "Comments")]
        [ExportToExcel(ResourceType = typeof(FlightsStrings), HeaderName = "Comments")]
        [ExcelColumn("הערות")]
        public string Comments { get; set; }


        public enum FlightsFields
        {
            [Display(ResourceType = typeof(FlightsStrings), Name = "Israel")]
            [LocalizedDescription("Israel", typeof(FlightsStrings))]
            Israel = 1,

            [Display(ResourceType = typeof(FlightsStrings), Name = "Warsaw")]
            [LocalizedDescription("Warsaw", typeof(FlightsStrings))]
            Warsaw = 2,

            [Display(ResourceType = typeof(FlightsStrings), Name = "Krakow")]
            [LocalizedDescription("Krakow", typeof(FlightsStrings))]
            Krakow = 3,

            [Display(ResourceType = typeof(FlightsStrings), Name = "Katowice")]
            [LocalizedDescription("Katowice", typeof(FlightsStrings))]
            Katowice = 4,

            [Display(ResourceType = typeof(FlightsStrings), Name = "Lodge")]
            [LocalizedDescription("Lodge", typeof(FlightsStrings))]
            Lodge = 5,

            [Display(ResourceType = typeof(FlightsStrings), Name = "Wroclaw")]
            [LocalizedDescription("Wroclaw", typeof(FlightsStrings))]
            Wroclaw = 6,

            [Display(ResourceType = typeof(FlightsStrings), Name = "Rzeszow")]
            [LocalizedDescription("Rzeszow", typeof(FlightsStrings))]
            Rzeszow = 7,

            [Display(ResourceType = typeof(FlightsStrings), Name = "Loblin")]
            [LocalizedDescription("Loblin", typeof(FlightsStrings))]
            Loblin = 8,

            [Display(ResourceType = typeof(FlightsStrings), Name = "Vilana")]
            [LocalizedDescription("Vilana", typeof(FlightsStrings))]
            Vilana = 9,

            [Display(ResourceType = typeof(FlightsStrings), Name = "Riga")]
            [LocalizedDescription("Riga", typeof(FlightsStrings))]
            Riga = 10,

            [Display(ResourceType = typeof(FlightsStrings), Name = "Saloniki")]
            [LocalizedDescription("Saloniki", typeof(FlightsStrings))]
            Saloniki = 11,

            [Display(ResourceType = typeof(FlightsStrings), Name = "Athens")]
            [LocalizedDescription("Athens", typeof(FlightsStrings))]
            Athens = 12,

            [Display(ResourceType = typeof(FlightsStrings), Name = "Munich")]
            [LocalizedDescription("Munich", typeof(FlightsStrings))]
            Munich = 13,

            [Display(ResourceType = typeof(FlightsStrings), Name = "Berlin")]
            [LocalizedDescription("Berlin", typeof(FlightsStrings))]
            Berlin = 14,
        }

    }

    public class FlightManagementViewModel : FlightViewModel
    {
        //Properties
        public int RemainingSeats { get; set; }

        public FlightManagementViewModel()
        {
            RemainingSeats = 0;
        }
    }

    public class DeleteFlightViewModel : DeleteViewModel
    {
        public IEnumerable<DAL.Trips> Trips { get; set; }

        public DeleteFlightViewModel()
        {
            Trips = new List<DAL.Trips>();
        }
    }

    public class DeleteMultipleFlightsViewModel
    {
        public List<int> Flights { get; set; }

        public DeleteMultipleFlightsViewModel()
        {
            Flights = new List<int>();
        }
    }

    public class ImportFlightsViewModel
    {
        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(GroupsStrings), Name = "Attachment")]
        [FileTypeValidation("XLS,XLSX")]
        public HttpPostedFileBase Attachment { get; set; }
    }


}