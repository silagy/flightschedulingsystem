﻿using FlightScheduling.Language;
using FlightScheduling.Web.Areas.Administration.Models.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Security.AccessControl;
using System.Web;

namespace FlightScheduling.Web.Areas.Administration.Models.Users
{
    public class UserViewModel
    {
        //Properties

        public int ID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }

    }

    public class UpdateUserPermissionRoleViewModel
    {
        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(PermissionStrings), Name = "RoleName")]
        public CustomSelectList Roles { get; set; }
        public List<int> SelectedUsers { get; set; }
        public int RoleID { get; set; }

        public UpdateUserPermissionRoleViewModel()
        {
            Roles = new CustomSelectList();
        }
    }

    public class UpdateUsersAccessToSystem
    {
        public List<int> SelectedUsers { get; set; }

        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(SchoolsStrings), Name = "IsActive")]
        public CustomSelectList AccessList { get; set; }

        public int SelectedAccess { get; set; }

        public UpdateUsersAccessToSystem()
        {
            AccessList = new CustomSelectList();
            AccessList.Items = new List<CustomSelectListItem>();
            AccessList.Items.Add(new CustomSelectListItem()
            {
                ID = 0,
                Name = CommonStrings.AccessDenied
            });
            AccessList.Items.Add(new CustomSelectListItem()
            {
                ID = 1,
                Name = CommonStrings.AccessAllowed
            });
        }
    }

}