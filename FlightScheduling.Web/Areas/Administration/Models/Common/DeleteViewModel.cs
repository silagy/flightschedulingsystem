﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FlightScheduling.Web.Areas.Administration.Models.Common
{
    public class DeleteViewModel
    {
        public int ID { get; set; }

        public bool IsDeletable { get; set; }

        public string Title { get; set; }

        public string Controller { get; set; }

        public string Action { get; set; }

        public string Header { get; set; }

        public string CustomMessage { get; set; }

        public List<int> BulkActionIDs { get; set; }

        public List<int> ReturnRefirectProperties { get; set; }

        public List<string> CustomProperties { get; set; }


    }

}