﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using FlightScheduling.Language;

namespace FlightScheduling.Web.Areas.Administration.Models.Common
{
    public class BasicDetails
    {

        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [Display(ResourceType = typeof(CommonStrings), Name = "CreationDate")]
        public DateTime? CreationDate { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [Display(ResourceType = typeof(CommonStrings), Name = "UpdateDate")]
        public DateTime? UpdatenDate { get; set; }
    }
}