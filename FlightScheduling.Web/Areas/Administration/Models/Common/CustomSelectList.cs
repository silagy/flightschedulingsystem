﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FlightScheduling.Language;
using FlightScheduling.Web.Infrastructure;

namespace FlightScheduling.Web.Areas.Administration.Models.Common
{
    public class CustomSelectListItem
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Group { get; set; }

    }

    public class CustomSelectList
    {
        public List<CustomSelectListItem> Items { get; set; }

        public CustomSelectList()
        {
            Items = new List<CustomSelectListItem>();

        }
    }

    public class TimeSelectListItem
    {
        public string ID { get; set; }
        public string Name { get; set; }
    }

    public class TimeSelectList
    {
        public List<TimeSelectListItem> Items { get; set; }

        public TimeSelectList()
        {
            Items = new List<TimeSelectListItem>();
        }
    }

    public class AutoCompleteItem
    {
        public string ID { get; set; }
        public string Name { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int InternalID { get; set; }
        public string Email { get; set; }
        public string RoleName { get; set; }
        public DateTime? ValidityEndTime { get; set; }
        public bool IsValid
        {
            get
            {
                if (this.ValidityEndTime == null)
                {
                    return false;
                }
                else if (this.ValidityEndTime >= DateTime.Now)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public string ValidityLabel
        {
            get
            {
                string str = "<span class='label ";
                str += IsValid ? "label-success'>" : "label-danger'>";
                str += IsValid ? InstructorsStrings.Valid : InstructorsStrings.NotValid;
                str += "</span>";

                return str;
            }
        }

        public string InstructorPictureUrl { get
            {
                if (!String.IsNullOrEmpty(ImageURL))
                {
                    //TO DO deal with the image folder
                    return "InstructorProfilePictures/" + InternalID + "/" + ImageURL;
                }

                return "/images/icons8-user-25.png";
            }
        }
        public string ImageURL { get; set; }

        
    }
}