﻿using FlightScheduling.DAL;
using FlightScheduling.Language;
using FlightScheduling.Web.Areas.Administration.Models.Common;
using FlightScheduling.Web.Areas.Administration.Models.Flights;
using FlightScheduling.Web.Areas.Administration.Models.Groups;
using FlightScheduling.Web.Infrastructure;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Web;
using static FlightScheduling.Web.Areas.Administration.Models.Flights.FlightViewModel;

namespace FlightScheduling.Web.Areas.Administration.Models.GroupForms
{
    public class BasicForm
    {
        //Form Sumbit and save
        public bool SaveAndSubmit { get; set; }
        public bool IsDisabled { get; set; }
        public bool ApprovalMode { get; set; }
        public int GroupID { get; set; }
        public GroupFormViewModel GeneralFormData { get; set; }
        public GroupFormSchoolData SchoolData { get; set; }
        public GroupDetailsViewModel GroupDetails { get; set; }

        //Form Messages
        public FormCommentsViewModel FormComments { get; set; }

        public BasicForm()
        {
            GeneralFormData = new GroupFormViewModel();
            SchoolData = new GroupFormSchoolData();
            GroupDetails = new GroupDetailsViewModel();
            SaveAndSubmit = false;
            IsDisabled = false;
            ApprovalMode = false;
            FormComments = new FormCommentsViewModel();
        }

    }

    public class GroupFormFullStatus
    {
        public int GroupID { get; set; }
        public int SchoolID { get; set; }
        public string SchoolName { get; set; }
        public int InstituteID { get; set; }
        public int GroupSchoolID { get; set; }
        public int GroupFormID { get; set; }
        public int GroupFormType { get; set; }
        public eGroupFormType GroupFormTypeEnum => (eGroupFormType)this.GroupFormType;
        public DateTime GroupFormUpdateDate { get; set; }
        public int? AdministrationFormID { get; set; }
        public int? AdministrationFormStatus { get; set; }
        public DateTime? AdministrationFormUpdateDate { get; set; }
        public int AdministrationNumberOfComments { get; set; }
        public eGroupFormStatus AdministrationFormStatusEnum => AdministrationFormStatus == null ? eGroupFormStatus.New : (eGroupFormStatus)(int)AdministrationFormStatus;
        public int? InstructorFormID { get; set; }
        public int? InstructorFormStatus { get; set; }
        public DateTime? InstructorFormUpdateDate { get; set; }
        public int InstructorsNumberOfComments { get; set; }
        public eGroupFormStatus InstructorFormStatusEnum => InstructorFormStatus == null ? eGroupFormStatus.New : (eGroupFormStatus)(int)InstructorFormStatus;
        public int? GroupPlanFormID { get; set; }
        public int? GroupPlanFormStatus { get; set; }
        public DateTime? GroupPlanFormUpdateDate { get; set; }
        public int PlanNumberOfComments { get; set; }
        public eGroupFormStatus GroupPlanFormStatusEnum => GroupPlanFormStatus == null ? eGroupFormStatus.New : (eGroupFormStatus)(int)GroupPlanFormStatus;
        public bool SchoolIsPrimary { get; set; }

        public eFormChapterType CurrentGroupFormStep { get; set; }
    }

    public class Group180FormsViewModel
    {
        public IEnumerable<GroupFormFullStatus> Forms { get; set; }
        public GroupFolderDataViewModel GroupFolderData { get; set; }

        public Group180FormsViewModel()
        {
            GroupFolderData = new GroupFolderDataViewModel();
        }
    }

    public class AdministrationForm180ViewModel : BasicForm
    {
        //Properties

        public AdministrativeFormViewModel AdministrativeFormData { get; set; }

        //Trip Details
        public TripDetailsViewModel TripDetails { get; set; }

        //Manager Data
        public GroupManagerData ManagerData { get; set; }

        public bool BusesEditingDisabled { get; set; }

        public AdministrationForm180ViewModel()
        {
            
            AdministrativeFormData = new AdministrativeFormViewModel();
            
            TripDetails = new TripDetailsViewModel();
            
            ManagerData = new GroupManagerData();

            BusesEditingDisabled = false;
            
        }


    }

    public class TripDetailsViewModel
    {
        //Properties
        public int ID { get; set; }

        public DateTime DepartureDate { get; set; }

        public DateTime ReturnDate { get; set; }

        public FlightViewModel.FlightsFields Destenation { get; set; }

        public FlightViewModel.FlightsFields ReturnedFrom { get; set; }
    }

    public class GroupFormSchoolData
    {
        //School Details
        public int SchoolID { get; set; }
        public int SchoolGroupAssosicationID { get; set; }
        public int IsAdministrativeInstitude { get; set; }
        public int? NumberOfInstructors { get; set; }

        public string SchoolName { get; set; }
        public int RegionID { get; set; }

        [DataType(DataType.EmailAddress, ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Email")]
        [Display(ResourceType = typeof(SchoolsStrings), Name = "Email")]
        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        public string SchoolEmail { get; set; }

        [Display(ResourceType = typeof(SchoolsStrings), Name = "Stream")]
        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        public int SchoolStream { get; set; }

        //TODO: To consider to add school streams
        public eSchoolStreams SchoolStreamEnum { get; set; }

        [Display(ResourceType = typeof(SchoolsStrings), Name = "InstituteID")]
        public string InstituteID { get; set; }

        [Display(ResourceType = typeof(SchoolsStrings), Name = "Fax")]
        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        public string SchoolFax { get; set; }

        [Display(ResourceType = typeof(SchoolsStrings), Name = "Phone")]
        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        public string SchoolPhone { get; set; }

        [Display(ResourceType = typeof(SchoolsStrings), Name = "IsSpecialEducation")]
        public bool IsSpecialEducation { get; set; }

        //School Manager Details
        [Display(ResourceType = typeof(SchoolsStrings), Name = "Manager")]
        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        public string Manager { get; set; }

        [Display(ResourceType = typeof(SchoolsStrings), Name = "ManagerPhone")]
        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        public string ManagerPhone { get; set; }

        [Display(ResourceType = typeof(SchoolsStrings), Name = "ManagerCell")]
        public string ManagerCell { get; set; }

        [DataType(DataType.EmailAddress, ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Email")]
        [Display(ResourceType = typeof(SchoolsStrings), Name = "ManagerEmail")]
        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        public string ManagerEmail { get; set; }

        public GroupFormSchoolData()
        {
            IsSpecialEducation = false;
        }

        // Group Manager
    }

    public class GroupDetailsViewModel
    {
        public int GroupID { get; set; }
        //Agency Details and Additional Details
        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        public int AgencyID { get; set; }

        [Display(ResourceType = typeof(AgenciesStrings), Name = "Name")]
        public CustomSelectList Agencies { get; set; }

        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(GroupsStrings), Name = "NumberOfBuses")]
        public int NumberofBuses { get; set; }

        public GroupDetailsViewModel()
        {
            Agencies = new CustomSelectList();
        }
    }

    public class GroupFormViewModel
    {
        public int? ID { get; set; }
        public int GroupSchoolID { get; set; }
        public int? FormType { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime UpdateDate { get; set; }
    }

    public class AdministrativeFormViewModel
    {
        public int? ID { get; set; }
        public int? GroupFormID { get; set; }
        public string ContractUrl { get; set; }
        public string SignatureUrl { get; set; }
        public int? FormStatus { get; set; }
        public eGroupFormStatus GroupFormStatusEnum
        {
            get
            {
                return this.FormStatus == null ? eGroupFormStatus.New : (eGroupFormStatus)this.FormStatus;
            }
        }

        public int ClassType { get; set; }

        [Display(ResourceType = typeof(GroupFormsStrings), Name = "ClassType")]
        public eClassType ClassTypeEnum { get => (eClassType)ClassType; set => ClassType = (int)value; }

        [Display(ResourceType = typeof(GroupFormsStrings), Name = "Contract")]
        [FileTypeValidation("PDF")]
        public HttpPostedFileBase Contract { get; set; }


        [Display(ResourceType = typeof(GroupFormsStrings), Name = "SignaturePage")]
        [FileTypeValidation("PDF")]
        public HttpPostedFileBase SignaturePage { get; set; }

        //Adding cost per student
        [Display(ResourceType = typeof(GroupFormsStrings), Name = "CostPerStudent")]
        [Range(1, int.MaxValue, ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "GreaterThanOne")]
        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        public int CostPerStudent { get; set; }

        public DateTime CreationDate { get; set; }
        public DateTime UpdateDate { get; set; }
        public int UserID { get; set; }
    }

    public class GroupManagerData
    {
        public int? InstructorID { get; set; }
        public string FirstNameHE { get; set; }
        public string LastNameHE { get; set; }

        [Display(ResourceType = typeof(AccountStrings), Name = "EmailAddress")]
        public string Email { get; set; }

        public string HomePhone { get; set; }
        public string CellPhone { get; set; }
        public string ImageUrl { get; set; }
        public DateTime ValidityEndTime { get; set; }

        public string PictureProfile
        {
            get
            {
                if (ImageUrl != null)
                {
                    return Path.Combine(AppConfig.InstructorProfilePictureRelativePath, ImageUrl);
                }
                else
                {
                    return Path.Combine("~/Content/images/icons8-user-80.png");
                }
            }
        }
    }

    public class AdministrativeFormFiles
    {
        public string SignatureUrl { get; set; }
        public string ContractUrl { get; set; }
    }

    /// <summary>
    /// This class is used to move 180 form from one group to another
    /// </summary>
    public class Move180ViewModel
    {
        public int GroupFormID { get; set; }
        public int SchoolID { get; set; }
        public int GroupID { get; set; }

        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(GroupsStrings), Name = "ExpeditionID")]
        public int TargetGroupID { get; set; }
        public int TargetGroupSchoolID { get; set; }

        public IEnumerable<SchoolGroupsViewModel> Groups { get; set; }

    }

    /// <summary>
    /// This class is used for getting all the groups in which a signle School is assosicated with
    /// It used for moving the form 180 from one group to another
    /// </summary>
    public class SchoolGroupsViewModel
    {

        public int GroupID { get; set; }
        public int GroupExternalID { get; set; }
        public DateTime DepartureDate { get; set; }
        public DateTime ReturningDate { get; set; }
        public int SchoolID { get; set; }
        public int DestinationID { get; set; }
        public FlightsFields DestinationEnum => (FlightsFields)DestinationID;

        public int ReturingField { get; set; }
        public FlightsFields ReturningFieldEnum => (FlightsFields)ReturingField;
    }
}