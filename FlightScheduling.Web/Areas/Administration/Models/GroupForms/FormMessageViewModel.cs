﻿using FlightScheduling.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FlightScheduling.Web.Areas.Administration.Models.GroupForms
{
    public class FormCommentViewModel
    {
        public int ID { get; set; }
        public int FormChapterType { get; set; }
        public eFormChapterType FormChapterTypeEnum { get; set; }
        public int FormID { get; set; }
        public string Comment { get; set; }
        public int UserID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime UpdateDate { get; set; }
    }

    public class FormCommentsViewModel
    {
        public int FormID { get; set; }
        public eFormChapterType FormChapterType { get; set; }
        public IEnumerable<FormCommentViewModel> Comments { get; set; }
    }
}