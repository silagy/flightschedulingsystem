﻿using FlightScheduling.DAL;
using FlightScheduling.DAL.CustomEntities.Groups;
using FlightScheduling.Language;
using FlightScheduling.Web.Areas.Administration.Models.Common;
using FlightScheduling.Web.Areas.Administration.Models.Groups;
using FlightScheduling.Website.Infrastructure;
using Flight.DAL.FlightDB.Methods;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using FlightScheduling.Web.Areas.Administration.Models.Trips;
using FlightScheduling.Web.Areas.Administration.Models.Flights;
using FlightScheduling.DAL.CustomEntities.GroupForms;
using FlightScheduling.Web.Infrastructure;
using FlightScheduling.Helpers;

namespace FlightScheduling.Web.Areas.Administration.Models.GroupForms
{
    public enum Form60StatusContext
    {
        FinalStatus = 1,
        AgencyStatus = 2,
        PlanStatus = 3
    }
    public class GroupForm60ViewModel
    {
        public int ID { get; set; }
        public int FormStatus { get; set; }
        public eGroupForm60Status FormStatusEnum => (eGroupForm60Status)FormStatus;
        public int FormApprovalUserID { get; set; }
        public string FormApprovalUserName { get; set; }
        public int AgencyApprovalStatus { get; set; }
        public eGroupFormStatus AgencyApprovalStatusEnum => (eGroupFormStatus)AgencyApprovalStatus;
        public int AgencyApprovalUserID { get; set; }
        public string AgencyApprovalUserName { get; set; }
        public int PlanApprovalStatus
        {
            get;
            set;
        }
        public eGroupFormStatus PlanApprovalStatusEnum => (eGroupFormStatus)PlanApprovalStatus;
        public int PlanApprovalUserID { get; set; }
        public string PlanApprovalUserName { get; set; }
        public int GroupID { get; set; }
        public int GroupExternalID { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime UpdateDate { get; set; }
        public eFormChapterType ChapterType { get; set; }

        public GroupFolderDataViewModel GroupFolderData { get; set; }
        public GroupManagerData ManagerData { get; set; }

        public IEnumerable<GroupSchoolsViewModel> Schools { get; set; }

        public bool EditMode { get; set; }



        public GroupForm60ViewModel()
        {
            GroupFolderData = new GroupFolderDataViewModel();
            EditMode = true;
        }
    }

    public class GroupGeneralDetailsViewModel
    {
        [Display(ResourceType = typeof(GroupsStrings), Name = "GroupInternalID")]
        public int GroupID { get; set; }
        [Display(ResourceType = typeof(GroupsStrings), Name = "ExpeditionID")]
        public int GroupExternalID { get; set; }
        [Display(ResourceType = typeof(GroupsStrings), Name = "NumberOfBuses")]
        public int NumberOfBuses { get; set; }
        [Display(ResourceType = typeof(GroupsStrings), Name = "NumberOfParticipants")]
        public int NumberofParticipants { get; set; }

        public int NumberOfMinInstructors => NumberOfBuses;

        public int ColorID { get; set; }

        //  CONTACT DETAILS IN CASE OF ERGENCY
        [Display(ResourceType = typeof(GroupsStrings), Name = "ContactName")]
        public string ContactName { get; set; }
        [Display(ResourceType = typeof(GroupsStrings), Name = "ContactPhone")]
        public string ContactPhone { get; set; }

        public GroupColor GroupColor
        {
            get
            {
                return new GroupColor
                {
                    ID = ColorID
                };
            }
        }

        public GroupTripDetailsViewModel Trip { get; set; }
    }

    public class GroupTripDetailsViewModel
    {
        [Display(ResourceType = typeof(TripsStrings), Name = "ID")]
        public int ID { get; set; }

        [Display(ResourceType = typeof(TripsStrings), Name = "DepartureDate")]
        public DateTime DepartureDate { get; set; }

        [Display(ResourceType = typeof(TripsStrings), Name = "ReturnDate")]
        public DateTime ReturningDate { get; set; }

        public int DestinationID { get; set; }
        [Display(ResourceType = typeof(TripsStrings), Name = "Destenation")]
        public FlightViewModel.FlightsFields DestenationEnum => (FlightViewModel.FlightsFields)DestinationID;

        public int ReturingField { get; set; }
        [Display(ResourceType = typeof(TripsStrings), Name = "TakeOffField")]
        public FlightViewModel.FlightsFields ReturnedFromEnum => (FlightViewModel.FlightsFields)ReturingField;
    }

    public class GroupSchoolsViewModel
    {
        public int SchoolID { get; set; }
        public string Name { get; set; }
        public int InstituteID { get; set; }
        public int SchoolGroupID { get; set; }
        public int GroupID { get; set; }
        public bool IsPrimary { get; set; }
        public bool IsAdministrativeInstitude { get; set; }
        public int? Stream { get; set; }
        public eSchoolStreams? StramEnum
        {
            get
            {
                if (Stream != null)
                {
                    return (eSchoolStreams)Stream;
                }

                return null;
            }
        }
        public bool IsSpecialEducation { get; set; }
    }

    public class GroupActualFlightsViewModel
    {
        public int GroupID { get; set; }
        public IEnumerable<GroupActualFlightViewModel> Flights { get; set; }
        public GroupForm60ViewModel Form60Data { get; set; }
        public GroupGeneralDetailsViewModel GroupDetails { get; set; }
        //Form Messages
        public FormCommentsViewModel FormComments { get; set; }

        // Form Alrets
        public List<WarningModel> Warnings { get; set; }



        public GroupActualFlightsViewModel()
        {
            Form60Data = new GroupForm60ViewModel();
            Form60Data.ChapterType = eFormChapterType.Flights;
            GroupDetails = new GroupGeneralDetailsViewModel();
            FormComments = new FormCommentsViewModel();
            Warnings = new List<WarningModel>();
        }
    }

    public class TripLightDetailsViewModel
    {
        public DateTime DepartureDate { get; set; }
        public DateTime ReturningDate { get; set; }
        public int DestinationID { get; set; }
        public eFlightsFields DestanationEnum => (eFlightsFields)DestinationID;

        public int ReturingField { get; set; }
        public eFlightsFields ArrivalEnum => (eFlightsFields)ReturingField;
    }

    public class GroupTotalPassengersByDirection
    {
        public int Direction { get; set; }
        public eFlightDirection DirectionEnum => (eFlightDirection)Direction;
        public int NumberOfPassengers { get; set; }
    }

    public class GroupActualFlightViewModel
    {
        public int ID { get; set; }
        public int GroupID { get; set; }
        public int Direction { get; set; }
        public bool IsUpdate { get; set; }

        public GroupGeneralDetailsViewModel GroupDetails { get; set; }
        public IEnumerable<GroupTotalPassengersByDirection> TotalLoggedPaggengers { get; set; }
        public int GoingNumberOfPassgengersRequired { get; set; }
        public int ReturningNumberOfPassgengersRequired { get; set; }

        public TripLightDetailsViewModel TripDetails { get; set; }

        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(GroupFormsStrings), Name = "FlightDirection")]
        public eFlightDirection DirectionEnum
        {
            get => (eFlightDirection)Direction;
            set => Direction = (int)value;
        }

        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(FlightsStrings), Name = "FlightNumber")]
        public string FlightNumber { get; set; }

        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(FlightsStrings), Name = "AirlineName")]
        public string Airline { get; set; }

        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(FlightsStrings), Name = "DepartureDate")]
        public DateTime Departure { get; set; }

        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(FlightsStrings), Name = "ArrivalDate")]
        public DateTime Arrival { get; set; }

        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(FlightsStrings), Name = "NumberOfPassengers")]
        public int NumberOfPassengers { get; set; }

        public string DepartureTimeValue { get; set; }
        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(FlightsStrings), Name = "DepartureTime")]
        public TimeSelectList DepartureTime { get; set; }

        public string ArrivalTimeValue { get; set; }

        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(FlightsStrings), Name = "ArrivalTime")]
        public TimeSelectList ArrivalTime { get; set; }

        public int DestinationLocationID { get; set; }

        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(FlightsStrings), Name = "DepartureField")]
        public eFlightsFields DestanationLocationEnum { get => (eFlightsFields)DestinationLocationID; set => DestinationLocationID = (int)value; }

        public int ArrivalLocationID { get; set; }

        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(FlightsStrings), Name = "DestenationField")]
        public eFlightsFields ArrivalLocationEnum { get => (eFlightsFields)ArrivalLocationID; set => ArrivalLocationID = (int)value; }

        public DateTime CreationDate { get; set; }
        public DateTime UpdateDate { get; set; }

        public GroupActualFlightViewModel()
        {
            IsUpdate = false;
            DepartureTime = new TimeSelectList();
            DepartureTime.Items.AddRange(FunctionHelpers.GetTimes());

            ArrivalTime = new TimeSelectList();
            ArrivalTime.Items.AddRange(FunctionHelpers.GetTimes());

        }
    }

    public class GroupTripPlanForDay
    {
        public DateTime CurrentDate { get; set; }
        public int DayNumber { get; set; }
        public GroupForm60ViewModel Form60Data { get; set; }
        public GroupGeneralDetailsViewModel GroupDetails { get; set; }
        public Dictionary<DateTime, int> TripDates { get; set; }
        public List<eActivityType> ActivitiesMenu { get; set; }
        public IEnumerable<TripActicity> Activities { get; set; }
        //Form Messages
        public FormCommentsViewModel FormComments { get; set; }

        // Determine if it is allowed to drive within the city
        public bool TravelWithinCityOnly { get; set; }

        public int? LastActivityID { get; set; }

        // SET WARNING LIST
        public List<WarningModel> Warnings { get; set; }

        public bool DisableMenu { get; set; }

        public GroupTripPlanForDay()
        {
            Form60Data = new GroupForm60ViewModel();
            GroupDetails = new GroupGeneralDetailsViewModel();

            // Setup the activity menu
            ActivitiesMenu = new List<eActivityType>();
            ActivitiesMenu.Add(eActivityType.Activity);
            ActivitiesMenu.Add(eActivityType.Drive);
            ActivitiesMenu.Add(eActivityType.Walking);
            ActivitiesMenu.Add(eActivityType.Ceremony);
            ActivitiesMenu.Add(eActivityType.Lunch);
            ActivitiesMenu.Add(eActivityType.RestroomBreak);
            ActivitiesMenu.Add(eActivityType.WakeUp);
            ActivitiesMenu.Add(eActivityType.NightDiscussion);
            ActivitiesMenu.Add(eActivityType.LightsOut);

            TravelWithinCityOnly = false;
            DisableMenu = false;
            FormComments = new FormCommentsViewModel();

            Warnings = new List<WarningModel>();
        }

        public void setTripDates(DateTime StartDate, DateTime EndDate)
        {
            int TripDayNumber = 1;
            TripDates = new Dictionary<DateTime, int>();
            while (StartDate <= EndDate)
            {
                TripDates.Add(StartDate.Date, TripDayNumber);
                StartDate = StartDate.AddDays(1).Date;
                TripDayNumber++;
            }
        }
    }





    public class TripBasicActicityViewModel
    {
        public int ID { get; set; }

        public int ActivityType { get; set; }

        [Display(ResourceType = typeof(GroupFormsStrings), Name = "ActivityType")]
        public eActivityType ActivityTypeEnum { get => (eActivityType)ActivityType; set => ActivityType = (int)value; }

        [Display(ResourceType = typeof(GroupFormsStrings), Name = "ActivityName")]
        public string Name { get; set; }


        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public double ActivityDuration => (EndTime - StartTime).TotalMinutes;


        public TimeSelectList StartTimeHours { get; set; }

        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(GroupFormsStrings), Name = "ActivityStartTime")]
        public string StartTimeValue { get; set; }

        [Display(ResourceType = typeof(CommonStrings), Name = "Comments")]
        public string Comments { get; set; }

        public int CityID { get; set; }

        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(GroupFormsStrings), Name = "SiteID")]
        public int? SiteID { get; set; }
        public int GroupID { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime UpdateDate { get; set; }
        public DateTime CurrentDate { get; set; }

        // Define the Models for city and site type
        public CustomSelectList Cities { get; set; }
        public eSiteType SiteTypes { get; set; }

        // TODO: To create a duration dropdown component

        public TimeSelectList Duration { get; set; }

        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(GroupFormsStrings), Name = "Duration")]
        public string durationValue { get; set; }



        // Site
        public SiteViewMode Site { get; set; }

        public bool IsUpdate { get; set; }

        public void SetStartTimeDropDown(int startHour, int StartMinutes, int EndHours, int EndMinutes)
        {
            StartTimeHours.Items.Clear();
            StartTimeHours.Items.AddRange(FunctionHelpers.GetTimes(startHour, StartMinutes, EndHours, EndMinutes));
        }

        public TripBasicActicityViewModel()
        {
            StartTimeHours = new TimeSelectList();
            StartTimeHours.Items.AddRange(FunctionHelpers.GetTimes());

            Duration = new TimeSelectList();
            Duration.Items.AddRange(FunctionHelpers.GetTimes(minuteInterval: 30, StartHour: 0, specialFormat: true));

            Cities = new CustomSelectList();

            Site = new SiteViewMode();
            IsUpdate = false;
        }
    }

    public class TripTravelActivityViewModel : TripBasicActicityViewModel
    {

        public CustomSelectList Sites { get; set; }

        public TimeSelectList NexActivityDuration { get; set; }

        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(GroupFormsStrings), Name = "DurationInNextActivity")]
        public string nextActivityDurationValue { get; set; }

        public TripTravelActivityViewModel()
        {
            this.ActivityTypeEnum = eActivityType.Drive;
            Sites = new CustomSelectList();

            NexActivityDuration = new TimeSelectList();
            NexActivityDuration.Items.AddRange(FunctionHelpers.GetTimes(minuteInterval: 30, specialFormat: true, StartHour: 0));
        }
    }

    public class TripCommonActivityViewModel : TripBasicActicityViewModel
    {
        public CustomSelectList Sites { get; set; }

        public TripCommonActivityViewModel()
        {
            Sites = new CustomSelectList();
        }
    }

    public class TripExtendedActicityViewModel : TripBasicActicityViewModel
    {
        [Display(ResourceType = typeof(GroupFormsStrings), Name = "SchoolName")]
        public string SchoolName { get; set; }

        [Display(ResourceType = typeof(GroupFormsStrings), Name = "SchoolAddress")]
        public string SchoolAddress { get; set; }

        [Display(ResourceType = typeof(GroupFormsStrings), Name = "ContactName")]
        public string ContactName { get; set; }

        [Display(ResourceType = typeof(GroupFormsStrings), Name = "ContactEmail")]
        public string ContactEmail { get; set; }

        [Display(ResourceType = typeof(GroupFormsStrings), Name = "ContactCell")]
        public string ContactCell { get; set; }

        [Display(ResourceType = typeof(GroupFormsStrings), Name = "ContactPhone")]
        public string ContactPhone { get; set; }

        [Display(ResourceType = typeof(GroupFormsStrings), Name = "IsExtendedActivity")]
        public bool IsExtendedActivity { get; set; }

        public TripExtendedActicityViewModel()
        {
            IsExtendedActivity = false;
        }
    }

    public class SiteViewMode
    {
        public int ID { get; set; }
        public int ExternalID { get; set; }
        public int SiteType { get; set; }
        public eSiteType SiteTypeEnum { get => (eSiteType)SiteType; set => SiteType = (int)value; }
        public string Name { get; set; }
        public string LocalName { get; set; }
        public string LocalRegion { get; set; }
        public string Limitations { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime UpdateDate { get; set; }
        public CityViewModel City { get; set; }

        public SiteViewMode()
        {
            City = new CityViewModel();
        }
    }

    public class CityViewModel
    {
        public int CityID { get; set; }
        public string CityName { get; set; }
        public int CityExternalID { get; set; }
        public int RegionID { get; set; }
        public string RegionName { get; set; }
        public int DistrictID { get; set; }
        public string DistrictName { get; set; }

    }

    /// <summary>
    /// This is the overall class that holds all the data regarding the contacts of the expedition
    /// </summary>
    public class ContactsViewModel
    {
        public DateTime CurrentDate { get; set; }
        public int DayNumber { get; set; }
        public GroupForm60ViewModel Form60Data { get; set; }
        public GroupGeneralDetailsViewModel GroupDetails { get; set; }
        public IEnumerable<ParticipantViewModel> BasicContacts { get; set; }
        public List<int> ParticipantTypeFilter { get; set; }
        public IEnumerable<InstructorsViewModel> Instructors { get; set; }
        public GroupFileViewModel ManagerStatement { get; set; }
        public List<GroupByParticipantTypeReport> ParticipantTypeReport { get; set; }
        //Form Messages
        public FormCommentsViewModel FormComments { get; set; }

        public ContactsViewModel()
        {
            Form60Data = new GroupForm60ViewModel();
            GroupDetails = new GroupGeneralDetailsViewModel();
            ParticipantTypeFilter = new List<int>();
            ParticipantTypeFilter.Add((int)eParticipantType.MedicalPersonal);
            ParticipantTypeFilter.Add((int)eParticipantType.Testimony);
            ParticipantTypeFilter.Add((int)eParticipantType.Polani);
            ParticipantTypeFilter.Add((int)eParticipantType.PolaniPhotographer);
            FormComments = new FormCommentsViewModel();
        }
    }

    /// <summary>
    /// This class is used to add participants into the expedition (basic participants)
    /// </summary>
    public class ParticipantViewModel
    {
        //Properties
        public int ID { get; set; }
        public int GroupID { get; set; }
        [Display(ResourceType = typeof(GroupsStrings), Name = "ParticipantID")]
        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        public string ParticipantID { get; set; }

        [Display(ResourceType = typeof(GroupsStrings), Name = "FirstNameHe")]
        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        public string FirstNameHe { get; set; }

        [Display(ResourceType = typeof(GroupsStrings), Name = "LastNameHe")]
        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        public string LastNameHe { get; set; }

        [Display(ResourceType = typeof(GroupsStrings), Name = "FirstNameEn")]
        public string FirstNameEn { get; set; }

        [Display(ResourceType = typeof(GroupsStrings), Name = "LastNameEn")]
        public string LastNameEn { get; set; }

        [Display(ResourceType = typeof(GroupsStrings), Name = "PassportNumber")]
        public string PassportNumber { get; set; }

        [Display(ResourceType = typeof(GroupsStrings), Name = "PassportExperationDate")]
        public DateTime? PassportExperationDate { get; set; }

        [Display(ResourceType = typeof(GroupsStrings), Name = "Birthday")]
        public DateTime? Birthday { get; set; }

        public int SEX { get; set; }

        [Display(ResourceType = typeof(GroupsStrings), Name = "SEX")]
        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        public eParticipantsSex SEXeum { get => (eParticipantsSex)SEX; set => SEX = (int)value; }

        //[Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        public eParticipantStatus ParticipantStatusEnum { get; set; }

        public int ParticipantType { get; set; }

        [Display(ResourceType = typeof(GroupsStrings), Name = "ParticipantType")]
        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        public eParticipantType ParticipantTypeEnum { get => (eParticipantType)ParticipantType; set => ParticipantType = (int)value; }

        public int? MedicalType { get; set; }

        [Display(ResourceType = typeof(GroupFormsStrings), Name = "MedicalType")]
        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        public eMedicalStuffType MedicalTypeEnum { get => MedicalType == null ? eMedicalStuffType.None : (eMedicalStuffType)MedicalType; set => MedicalType = (int)value; }

        [Display(ResourceType = typeof(GroupFormsStrings), Name = "ContactPhone")]
        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        public string ContactNumber { get; set; }

        [Display(ResourceType = typeof(GroupsStrings), Name = "Speciality")]
        public string Speciality { get; set; }

        public DateTime CreationDate { get; set; }
        public DateTime UpdateDate { get; set; }

        public int UserID { get; set; }

        public bool UpdateOperation { get; set; }

        public ParticipantViewModel()
        {
            UpdateOperation = false;
        }
    }

    public class AddGuideViewModel
    {
        public int GroupID { get; set; }
        [Display(ResourceType = typeof(GroupFormsStrings), Name = "IsGuideManager")]
        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        public bool IsGroupManager { get; set; }

        [Display(ResourceType = typeof(GroupFormsStrings), Name = "ContactPhone")]
        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        public string ContactNumber { get; set; }

        [Display(ResourceType = typeof(AccountStrings), Name = "EmailAddress")]
        public string Email { get; set; }


        [Display(ResourceType = typeof(CommonStrings), Name = "Comments")]
        public string Comments { get; set; }
        public int InstructorID { get; set; }
    }

    public class InstructorsViewModel
    {
        public int ID { get; set; }
        public int GroupID { get; set; }
        public int InstructorID { get; set; }
        public string ContactNumber { get; set; }
        public bool IsGroupManager { get; set; }
        public string FirstNameHE { get; set; }
        public string LastNameHE { get; set; }
        public string FullNameHe => $"{FirstNameHE} {LastNameHE}";
        public string FirstNameEN { get; set; }
        public string LastNameEN { get; set; }
        public string FullNameEn => $"{FirstNameEN} {LastNameEN}";
        public string PassportID { get; set; }
        public string Email { get; set; }
        public DateTime ValidityEndTime { get; set; }
    }

    public class GroupContactDetailsViewMode
    {
        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(GroupsStrings), Name = "ContactName")]
        public string ContactName { get; set; }

        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(GroupsStrings), Name = "ContactPhone")]
        public string ContactPhone { get; set; }

        public int GroupID { get; set; }
    }

    public class Form60GroupManagerViewModel
    {
        public int ID { get; set; }
        public int GroupID { get; set; }

        [Display(ResourceType = typeof(AccountStrings), Name = "EmailAddress")]
        public string Email { get; set; }

        [Display(ResourceType = typeof(InstructorsStrings), Name = "HomePhone")]
        public string HomePhone { get; set; }

        [Display(ResourceType = typeof(InstructorsStrings), Name = "CellPhone")]
        public string CellPhone { get; set; }

        public Form60GroupManagerViewModel()
        {
        }

    }

    public class GroupFileViewModel
    {
        //Properties

        public int ID { get; set; }
        public int GroupID { get; set; }

        [Display(ResourceType = typeof(GroupsStrings), Name = "FileName")]
        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        public string Name { get; set; }

        public string Url { get; set; }

        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(GroupsStrings), Name = "Attachment")]
        [FileTypeValidation("PDF,doc,docx")]
        public HttpPostedFileBase Attachment { get; set; }

        public System.DateTime CreationDate { get; set; }
        public int UserID { get; set; }

        [Display(ResourceType = typeof(GroupsStrings), Name = "FileType")]
        public eGroupFileType FileType { get; set; }

        [Display(ResourceType = typeof(GroupsStrings), Name = "FileStatus")]
        public eGroupFileStatus FileStatus { get; set; }


        public GroupFileViewModel()
        {
            CreationDate = DateTime.Now;
            FileType = eGroupFileType.ManagerStatement;
        }
    }

    public class GroupByParticipantTypeReport
    {
        public int NumberOfParticipants { get; set; }
        public string ParticipantType { get; set; }
        public int GroupID { get; set; }
    }

}