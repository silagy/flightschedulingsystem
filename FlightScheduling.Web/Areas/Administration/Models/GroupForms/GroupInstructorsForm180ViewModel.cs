﻿using FlightScheduling.DAL;
using FlightScheduling.Language;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FlightScheduling.Web.Areas.Administration.Models.GroupForms
{
    public class GroupInstructorsForm180ViewModel : BasicForm
    {
        //Form Validity
        public bool IsValid { get; set; }

        //Instructor Form
        public InstructorFormViewModel InstructorFormDetails { get; set; }

        //General Details
        public GroupInstructorsGeneralDetails GeneralDetails { get; set; }

        //Instructors
        public IEnumerable<GroupInstructorViewModel> Instructors { get; set; }


        public GroupInstructorsForm180ViewModel()
        {
            GeneralDetails = new GroupInstructorsGeneralDetails();
            InstructorFormDetails = new InstructorFormViewModel();
        }


    }

    public class GroupInstructorsGeneralDetails
    {
        public int NumberOfBuses { get; set; }
        public int? SchoolNumberOfInstructors { get; set; }

        public int MinimumNumberOfInstructors
        {
            get
            {
                if (SchoolNumberOfInstructors != null)
                {
                    return (int)SchoolNumberOfInstructors;
                }
                else
                {
                    if (NumberOfBuses > 0)
                    {
                        return NumberOfBuses * 2;
                    }
                }

                return 0;
            }
        }

        public int ActualNumberOfInstructors { get; set; }

    }

    public class GroupInstructorViewModel
    {
        public int AssociationID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string InstructorID { get; set; }
        public int ID { get; set; }
        public string PhoneNumber { get; set; }
        public bool IsGroupManager { get; set; }
        public bool IsDeputyDirector { get; set; }
        public string RoleName { get; set; }
        public string Comments { get; set; }
        public DateTime? ValidityEndDate { get; set; }
        public bool IsValid
        {
            get
            {
                if (this.ValidityEndDate == null)
                {
                    return false;
                }else if (this.ValidityEndDate >= DateTime.Now)
                {
                    return true;
                }else
                {
                    return false;
                }
            }
        }

        public string ImageURL { get; set; }
    }

    public class InstructorFormViewModel
    {
        public int? ID { get; set; }
        public int? GroupFormID { get; set; }
        public int? FormStatus { get; set; }
        public eGroupFormStatus GroupFormStatusEnum
        {
            get
            {
                return this.FormStatus == null ? eGroupFormStatus.New : (eGroupFormStatus)this.FormStatus;
            }
        }

        public DateTime CreationDate { get; set; }
        public DateTime UpdateDate { get; set; }
    }

    public class AddInstructorViewModel
    {
        public int GroupID { get; set; }
        public int SchoolID { get; set; }
        [Display(ResourceType = typeof(GroupFormsStrings), Name = "IstructorActingAsGroupManager")]
        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        public bool IsGroupManager { get; set; }

        [Display(ResourceType = typeof(GroupFormsStrings), Name = "IstructorActingAsDeputyDirector")]
        public bool IsDeputyDirector { get; set; }

        [Display(ResourceType = typeof(CommonStrings), Name = "Comments")]
        public string Comments { get; set; }
        public int InstructorID { get; set; }
    }

}