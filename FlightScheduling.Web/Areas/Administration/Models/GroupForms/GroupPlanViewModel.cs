﻿using FlightScheduling.DAL;
using FlightScheduling.Language;
using FlightScheduling.Web.Areas.Administration.Models.Administration;
using FlightScheduling.Web.Areas.Administration.Models.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FlightScheduling.Web.Areas.Administration.Models.GroupForms
{
    public class GroupPlanForm180ViewModel : BasicForm
    {
        //Form Validity
        public bool IsValid { get; set; }

        public GroupPlanViewModel GroupPlanFormDetails { get; set; }

        public IEnumerable<GroupPlanSubjectViewModel> Subjects { get; set; }

        public IEnumerable<GroupPlanDetails> GroupPlansDetails { get; set; }

        public GroupPlanForm180ViewModel()
        {
        }


    }


    public class GroupPlanViewModel
    {
        public int? ID { get; set; }
        public int? GroupFormID { get; set; }
        public int? FormStatus { get; set; }
        public eGroupFormStatus GroupFormStatusEnum
        {
            get
            {
                return this.FormStatus == null ? eGroupFormStatus.New : (eGroupFormStatus)this.FormStatus;
            }
        }

        [Display(ResourceType = typeof(GroupFormsStrings), Name = "EdicationPlanDetails")]
        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        public string EdicationPlanDetails { get; set; }

        public DateTime CreationDate { get; set; }
        public DateTime UpdateDate { get; set; }
    }

    public class GroupPlanDetails
    {
        //properties
        public int ID { get; set; }
        public int GroupPlanID { get; set; }

        [Display(ResourceType = typeof(GroupFormsStrings), Name = "PlanName")]
        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        public string PlanName { get; set; }

        [Display(ResourceType = typeof(AdministrationStrings), Name = "PlanSubjectName")]
        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        public int GroupPlanSubjectID { get; set; }

        [Display(ResourceType = typeof(AdministrationStrings), Name = "PlanSubjectName")]
        public string GroupPlanSubjectName { get; set; }

        [Display(ResourceType = typeof(GroupFormsStrings), Name = "Instructor")]
        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        public string Instructor { get; set; }

        [Display(ResourceType = typeof(GroupFormsStrings), Name = "PlanHours")]
        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        [Range(1, int.MaxValue, ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "GreaterThanOne")]
        public int PlanHours { get; set; }

        [Display(ResourceType = typeof(GroupFormsStrings), Name = "PlanDate")]
        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        public DateTime PlanDate { get; set; }

        [Display(ResourceType = typeof(GroupFormsStrings), Name = "PlanLocation")]
        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        public string PlanLocation { get; set; }

        

        public DateTime CreationDate { get; set; }
        public DateTime UpdateDate { get; set; }

        //Used only when form
        public bool IsUpdate { get; set; }
        public CustomSelectList Subjects { get; set; }

        //Data Required form returning to the page
        public int GroupID { get; set; }
        public int SchoolID { get; set; }

        public GroupPlanDetails()
        {
            Subjects = new CustomSelectList();
        }
    }
}