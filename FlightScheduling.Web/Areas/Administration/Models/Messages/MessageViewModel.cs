﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FlightScheduling.Language;

namespace FlightScheduling.Web.Areas.Administration.Models.Messages
{
    public class MessageViewModel
    {
        //Properties
        [Display(ResourceType = typeof(AdministrationStrings), Name = "MessageID")]
        public int Id { get; set; }

        [Required]
        [Display(ResourceType = typeof(AdministrationStrings), Name = "Title")]
        public string Title { get; set; }

        [Required]
        [AllowHtml]
        [Display(ResourceType = typeof(AdministrationStrings), Name = "Message")]
        public string Message { get; set; }

        [Required]
        [Display(ResourceType = typeof(AdministrationStrings), Name = "ActiveStartDate")]
        public DateTime ActiveStartDate { get; set; }

        [Required]
        [Display(ResourceType = typeof(AdministrationStrings), Name = "ExpirationDate")]
        public DateTime ExpirationDate { get; set; }

        [Display(ResourceType = typeof(CommonStrings), Name = "UpdateDate")]
        public DateTime UpdateDate { get; set; }

        [Display(ResourceType = typeof(CommonStrings), Name = "CreationDate")]
        public DateTime CreationDate { get; set; }

        public int UserID { get; set; }

        public bool IsUpdate { get; set; }

        public MessageViewModel()
        {
            ActiveStartDate = DateTime.Now;
            ExpirationDate = DateTime.Now.AddDays(14);
            CreationDate = DateTime.Now;
            UpdateDate = DateTime.Now;
            IsUpdate = false;
        }

    }

}