﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Endilo.ExportToExcel;
using FlightScheduling.DAL;
using FlightScheduling.Language;
using FlightScheduling.Web.Areas.Administration.Models.Common;
using FlightScheduling.Web.Areas.Administration.Models.Groups;

namespace FlightScheduling.Web.Areas.Administration.Models.Schools
{
    public class SchoolsViewModel
    {
        //Filters
        [Display(ResourceType = typeof(SchoolsStrings), Name = "InstituteID")]
        public string InstituteID { get; set; }

        [Display(ResourceType = typeof(SchoolsStrings), Name = "Name")]
        public string Name { get; set; }

        [Display(ResourceType = typeof(SchoolsStrings), Name = "Email")]
        public string Email { get; set; }


        //public int NumberOfPages { get; set; }
        //public int NumberOfRowstoFetch { get; set; }
        //public int PageNumber { get; set; }
        //Properties
        public List<SchoolViewModel> SchoolViewModels { get; set; }

        public SchoolsViewModel()
        {
            SchoolViewModels = new List<SchoolViewModel>();
            //NumberOfPages = 1;
            //NumberOfRowstoFetch = 50;
            //PageNumber = 1;
        }
    }

    public class SchoolRemarksViewModel
    {
        public int GroupID { get; set; }

        [Display(ResourceType = typeof(GroupsStrings), Name = "ExpeditionID")]
        public string GroupExternalID { get; set; }

        [Display(ResourceType = typeof(CommonStrings), Name = "UpdateDate")]
        public DateTime UpdateDate { get; set; }

        [Display(ResourceType = typeof(GroupsStrings), Name = "Note")]
        public string Notes { get; set; }

        public int UserID { get; set; }

        [Display(ResourceType = typeof(AccountStrings), Name = "FirstName")]
        public string Firstname { get; set; }

        [Display(ResourceType = typeof(AccountStrings), Name = "LastName")]
        public string Lastname { get; set; }
    }

    public class SchoolViewModel : BasicDetails
    {
        //Properties

        [Display(ResourceType = typeof(SchoolsStrings), Name = "ID")]
        public int ID { get; set; }

        [Range(100000, 9999999, ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "InstituteLenght")]
        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(SchoolsStrings), Name = "InstituteID")]
        [ExportToExcel(ResourceType = typeof(SchoolsStrings), HeaderName = "InstituteID")]
        public int InstituteID { get; set; }

        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        [ExportToExcel(ResourceType = typeof(SchoolsStrings), HeaderName = "Name")]
        [Display(ResourceType = typeof(SchoolsStrings), Name = "Name")]
        public string Name { get; set; }

        [DataType(DataType.EmailAddress, ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Email")]
        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(SchoolsStrings), Name = "Email")]
        public string Email { get; set; }

        [Display(ResourceType = typeof(SchoolsStrings), Name = "Fax")]
        public string Fax { get; set; }

        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(SchoolsStrings), Name = "Phone")]
        public string Phone { get; set; }

        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(SchoolsStrings), Name = "Manager")]
        public string Manager { get; set; }

        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(SchoolsStrings), Name = "ManagerPhone")]
        public string ManagerPhone { get; set; }

        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        [DataType(DataType.EmailAddress, ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Email")]
        [Display(ResourceType = typeof(SchoolsStrings), Name = "ManagerEmail")]
        public string ManagerEmail { get; set; }

        [Display(ResourceType = typeof(SchoolsStrings), Name = "IsActive")]
        public bool IsActive { get; set; }

        [Display(ResourceType = typeof(SchoolsStrings), Name = "IsActivated")]
        public bool IsActivated { get; set; }

        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(SchoolsStrings), Name = "City")]
        public string City { get; set; }

        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(SchoolsStrings), Name = "Region")]
        public eSchoolRegion? RegionEnum { get; set; }

        [Display(ResourceType = typeof(SchoolsStrings), Name = "IsAdministrative")]
        public bool IsAdministrative { get; set; }

        // Navigation parameters
        /// <summary>
        /// This parametar is used when the user gets to edit school from the Group Management. I store the Group ID so he can go back to the same place
        /// </summary>
        public int? GroupID { get; set; }


        public SchoolViewModel()
        {
            IsActive = true;

        }

        public SchoolViewModel(FlightScheduling.DAL.Schools _school)
        {
            this.ID = _school.ID;
            this.CreationDate = _school.CreationDate;
            this.Email = _school.Email;
            this.Fax = _school.Fax;
            this.InstituteID = _school.InstituteID;
            this.IsAdministrative = (bool)_school.IsAdministrativeInstitude;
            this.Manager = _school.Manager;
            this.ManagerEmail = _school.ManagerEmail;
            this.ManagerPhone = _school.ManagerPhone;
            this.Name = _school.Name;
            this.Phone = _school.Phone;
        }
    }
}