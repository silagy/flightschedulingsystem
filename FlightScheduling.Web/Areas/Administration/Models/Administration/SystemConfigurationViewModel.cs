﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using FlightScheduling.DAL.CustomEntities;
using FlightScheduling.Language;

namespace FlightScheduling.Web.Areas.Administration.Models.Administration
{
    public class SystemConfigurationItemViewModel
    {
        [Display(ResourceType = typeof(AdministrationStrings), Name = "PropertyID")]
        public int PropertyID { get; set; }

        [Display(ResourceType = typeof(AdministrationStrings), Name = "PropertyName")]
        public string PropertyName { get; set; }

        [Display(ResourceType = typeof(AdministrationStrings), Name = "PropertyType")]
        public ePropertyType PropertyType { get; set; }

        [Display(ResourceType = typeof(AdministrationStrings), Name = "PropertyValue")]
        public string PropertyValue { get; set; }

        [Display(ResourceType = typeof(AdministrationStrings), Name = "PropertyDescription")]
        public string PropertyDescription { get; set; }

    }

    public class SystemConfigurationViewModel
    {
        public List<SystemConfigurationItemViewModel> Model { get; set; }

        public SystemConfigurationViewModel()
        {
            Model = new List<SystemConfigurationItemViewModel>();
        }
    }
}