﻿using FlightScheduling.DAL.CustomEntities.Permissions;
using FlightScheduling.Language;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FlightScheduling.Web.Areas.Administration.Models.Administration
{
    public class PermissionRoleViewModel
    {
        public int ID { get; set; }
        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(PermissionStrings), Name = "RoleName")]
        public string Name { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime UpdateDate { get; set; }
        public bool IsUpdate { get; set; }

        public List<PermissionBitViewModel> Permissions { get; set; }

        public PermissionRoleViewModel()
        {
            Permissions = new List<PermissionBitViewModel>();

            
        }

        public void LoadPotentialPermission()
        {
            Permissions.Add(new PermissionBitViewModel()
            {
                PermissionBitEnum = PermissionBitEnum.ExpeditionDashbard_View,
                Group = DailyDashboardStrings.PageTitle,
                CreationDate = DateTime.Now,
                UpdateDate = CreationDate
            });

            // Trips
            Permissions.Add(new PermissionBitViewModel()
            {
                PermissionBitEnum = PermissionBitEnum.Trip_View,
                Group = AdministrationStrings.TripManagementTitle,
                CreationDate = DateTime.Now,
                UpdateDate = CreationDate
            });

            Permissions.Add(new PermissionBitViewModel()
            {
                PermissionBitEnum = PermissionBitEnum.Trip_Edit,
                Group = AdministrationStrings.TripManagementTitle,
                CreationDate = DateTime.Now,
                UpdateDate = CreationDate
            });

            Permissions.Add(new PermissionBitViewModel()
            {
                PermissionBitEnum = PermissionBitEnum.Trip_Delete,
                Group = AdministrationStrings.TripManagementTitle,
                CreationDate = DateTime.Now,
                UpdateDate = CreationDate
            });

            // Flights
            Permissions.Add(new PermissionBitViewModel()
            {
                PermissionBitEnum = PermissionBitEnum.Flight_View,
                Group = AdministrationStrings.FlightManagement,
                CreationDate = DateTime.Now,
                UpdateDate = CreationDate
            });

            Permissions.Add(new PermissionBitViewModel()
            {
                PermissionBitEnum = PermissionBitEnum.Flight_Edit,
                Group = AdministrationStrings.FlightManagement,
                CreationDate = DateTime.Now,
                UpdateDate = CreationDate
            });

            Permissions.Add(new PermissionBitViewModel()
            {
                PermissionBitEnum = PermissionBitEnum.Flight_Delete,
                Group = AdministrationStrings.FlightManagement,
                CreationDate = DateTime.Now,
                UpdateDate = CreationDate
            });

            Permissions.Add(new PermissionBitViewModel()
            {
                PermissionBitEnum = PermissionBitEnum.Flight_Delete_Multiple,
                Group = AdministrationStrings.FlightManagement,
                CreationDate = DateTime.Now,
                UpdateDate = CreationDate
            });

            //Orders
            Permissions.Add(new PermissionBitViewModel()
            {
                PermissionBitEnum = PermissionBitEnum.Order_View,
                Group = ExpeditionStrings.ManageExpedition,
                CreationDate = DateTime.Now,
                UpdateDate = CreationDate
            });

            Permissions.Add(new PermissionBitViewModel()
            {
                PermissionBitEnum = PermissionBitEnum.Order_ViewBusAvailability,
                Group = ExpeditionStrings.ManageExpedition,
                CreationDate = DateTime.Now,
                UpdateDate = CreationDate
            });

            Permissions.Add(new PermissionBitViewModel()
            {
                PermissionBitEnum = PermissionBitEnum.Order_View_Available_Slots,
                Group = ExpeditionStrings.ManageExpedition,
                CreationDate = DateTime.Now,
                UpdateDate = CreationDate
            });

            Permissions.Add(new PermissionBitViewModel()
            {
                PermissionBitEnum = PermissionBitEnum.Order_Creation,
                Group = ExpeditionStrings.ManageExpedition,
                CreationDate = DateTime.Now,
                UpdateDate = CreationDate
            });

            Permissions.Add(new PermissionBitViewModel()
            {
                PermissionBitEnum = PermissionBitEnum.Expeditions_AddQuickExpedition,
                Group = ExpeditionStrings.ManageExpedition,
                CreationDate = DateTime.Now,
                UpdateDate = CreationDate
            });

            Permissions.Add(new PermissionBitViewModel()
            {
                PermissionBitEnum = PermissionBitEnum.Order_Edit,
                Group = ExpeditionStrings.ManageExpedition,
                CreationDate = DateTime.Now,
                UpdateDate = CreationDate
            });

            Permissions.Add(new PermissionBitViewModel()
            {
                PermissionBitEnum = PermissionBitEnum.Order_AgencyApproval,
                Group = ExpeditionStrings.ManageExpedition,
                CreationDate = DateTime.Now,
                UpdateDate = CreationDate
            });

            Permissions.Add(new PermissionBitViewModel()
            {
                PermissionBitEnum = PermissionBitEnum.Order_Manage,
                Group = ExpeditionStrings.ManageExpedition,
                CreationDate = DateTime.Now,
                UpdateDate = CreationDate
            });

            Permissions.Add(new PermissionBitViewModel()
            {
                PermissionBitEnum = PermissionBitEnum.Order_Delete,
                Group = ExpeditionStrings.ManageExpedition,
                CreationDate = DateTime.Now,
                UpdateDate = CreationDate
            });

            //Group Management
            Permissions.Add(new PermissionBitViewModel()
            {
                PermissionBitEnum = PermissionBitEnum.Expedition_View,
                Group = GroupsStrings.GroupsManagement,
                CreationDate = DateTime.Now,
                UpdateDate = CreationDate
            });

            Permissions.Add(new PermissionBitViewModel()
            {
                PermissionBitEnum = PermissionBitEnum.Expedition_Edit,
                Group = GroupsStrings.GroupsManagement,
                CreationDate = DateTime.Now,
                UpdateDate = CreationDate
            });

            Permissions.Add(new PermissionBitViewModel()
            {
                PermissionBitEnum = PermissionBitEnum.Expedition_Delete,
                Group = GroupsStrings.GroupsManagement,
                CreationDate = DateTime.Now,
                UpdateDate = CreationDate
            });

            Permissions.Add(new PermissionBitViewModel()
            {
                PermissionBitEnum = PermissionBitEnum.Expedition_MoveGroup,
                Group = GroupsStrings.GroupsManagement,
                CreationDate = DateTime.Now,
                UpdateDate = CreationDate
            });

            Permissions.Add(new PermissionBitViewModel()
            {
                PermissionBitEnum = PermissionBitEnum.Expedition_Manage,
                Group = GroupsStrings.GroupsManagement,
                CreationDate = DateTime.Now,
                UpdateDate = CreationDate
            });

            Permissions.Add(new PermissionBitViewModel()
            {
                PermissionBitEnum = PermissionBitEnum.Expedition_UpdateAgency,
                Group = GroupsStrings.GroupsManagement,
                CreationDate = DateTime.Now,
                UpdateDate = CreationDate
            });

            Permissions.Add(new PermissionBitViewModel()
            {
                PermissionBitEnum = PermissionBitEnum.Expedition_UpdateContact,
                Group = GroupsStrings.GroupsManagement,
                CreationDate = DateTime.Now,
                UpdateDate = CreationDate
            });

            Permissions.Add(new PermissionBitViewModel()
            {
                PermissionBitEnum = PermissionBitEnum.Expedition_ViewStaff,
                Group = GroupsStrings.GroupsManagement,
                CreationDate = DateTime.Now,
                UpdateDate = CreationDate
            });

            Permissions.Add(new PermissionBitViewModel()
            {
                PermissionBitEnum = PermissionBitEnum.Expedition_AddStaff,
                Group = GroupsStrings.GroupsManagement,
                CreationDate = DateTime.Now,
                UpdateDate = CreationDate
            });

            Permissions.Add(new PermissionBitViewModel()
            {
                PermissionBitEnum = PermissionBitEnum.Expedition_DeleteStaff,
                Group = GroupsStrings.GroupsManagement,
                CreationDate = DateTime.Now,
                UpdateDate = CreationDate
            });

            Permissions.Add(new PermissionBitViewModel()
            {
                PermissionBitEnum = PermissionBitEnum.Expedition_Approval,
                Group = GroupsStrings.GroupsManagement,
                CreationDate = DateTime.Now,
                UpdateDate = CreationDate
            });

            Permissions.Add(new PermissionBitViewModel()
            {
                PermissionBitEnum = PermissionBitEnum.Expedition_ViewFiles,
                Group = GroupsStrings.GroupsManagement,
                CreationDate = DateTime.Now,
                UpdateDate = CreationDate
            });

            Permissions.Add(new PermissionBitViewModel()
            {
                PermissionBitEnum = PermissionBitEnum.Expedition_AddingFiles,
                Group = GroupsStrings.GroupsManagement,
                CreationDate = DateTime.Now,
                UpdateDate = CreationDate
            });

            Permissions.Add(new PermissionBitViewModel()
            {
                PermissionBitEnum = PermissionBitEnum.Expedition_EditingFiles,
                Group = GroupsStrings.GroupsManagement,
                CreationDate = DateTime.Now,
                UpdateDate = CreationDate
            });

            Permissions.Add(new PermissionBitViewModel()
            {
                PermissionBitEnum = PermissionBitEnum.Expedition_UpdateFileStatus,
                Group = GroupsStrings.GroupsManagement,
                CreationDate = DateTime.Now,
                UpdateDate = CreationDate
            });

            Permissions.Add(new PermissionBitViewModel()
            {
                PermissionBitEnum = PermissionBitEnum.Expedtion_DeletingFiles,
                Group = GroupsStrings.GroupsManagement,
                CreationDate = DateTime.Now,
                UpdateDate = CreationDate
            });

            Permissions.Add(new PermissionBitViewModel()
            {
                PermissionBitEnum = PermissionBitEnum.Expedition_NotesViewing,
                Group = GroupsStrings.GroupsManagement,
                CreationDate = DateTime.Now,
                UpdateDate = CreationDate
            });

            Permissions.Add(new PermissionBitViewModel()
            {
                PermissionBitEnum = PermissionBitEnum.Expedition_NotesEditing,
                Group = GroupsStrings.GroupsManagement,
                CreationDate = DateTime.Now,
                UpdateDate = CreationDate
            });

            Permissions.Add(new PermissionBitViewModel()
            {
                PermissionBitEnum = PermissionBitEnum.Expedition_NotesDeleting,
                Group = GroupsStrings.GroupsManagement,
                CreationDate = DateTime.Now,
                UpdateDate = CreationDate
            });

            Permissions.Add(new PermissionBitViewModel()
            {
                PermissionBitEnum = PermissionBitEnum.Expedition_ViewSubSchool,
                Group = GroupsStrings.GroupsManagement,
                CreationDate = DateTime.Now,
                UpdateDate = CreationDate
            });

            Permissions.Add(new PermissionBitViewModel()
            {
                PermissionBitEnum = PermissionBitEnum.Expedition_AddSubSchool,
                Group = GroupsStrings.GroupsManagement,
                CreationDate = DateTime.Now,
                UpdateDate = CreationDate
            });

            Permissions.Add(new PermissionBitViewModel()
            {
                PermissionBitEnum = PermissionBitEnum.Expedition_UpdateSubSchool,
                Group = GroupsStrings.GroupsManagement,
                CreationDate = DateTime.Now,
                UpdateDate = CreationDate
            });

            Permissions.Add(new PermissionBitViewModel()
            {
                PermissionBitEnum = PermissionBitEnum.Expedition_DeleteSubSchool,
                Group = GroupsStrings.GroupsManagement,
                CreationDate = DateTime.Now,
                UpdateDate = CreationDate
            });

            Permissions.Add(new PermissionBitViewModel()
            {
                PermissionBitEnum = PermissionBitEnum.Expedition_ViewParticipants,
                Group = GroupsStrings.GroupsManagement,
                CreationDate = DateTime.Now,
                UpdateDate = CreationDate
            });

            Permissions.Add(new PermissionBitViewModel()
            {
                PermissionBitEnum = PermissionBitEnum.Expedition_EditParticipants,
                Group = GroupsStrings.GroupsManagement,
                CreationDate = DateTime.Now,
                UpdateDate = CreationDate
            });

            Permissions.Add(new PermissionBitViewModel()
            {
                PermissionBitEnum = PermissionBitEnum.Expedition_DeleteParticipant,
                Group = GroupsStrings.GroupsManagement,
                CreationDate = DateTime.Now,
                UpdateDate = CreationDate
            });

            Permissions.Add(new PermissionBitViewModel()
            {
                PermissionBitEnum = PermissionBitEnum.Expedition_DeleteMultipleParticipants,
                Group = GroupsStrings.GroupsManagement,
                CreationDate = DateTime.Now,
                UpdateDate = CreationDate
            });

            Permissions.Add(new PermissionBitViewModel()
            {
                PermissionBitEnum = PermissionBitEnum.Expedition_FormsViewing,
                Group = GroupsStrings.GroupsManagement,
                CreationDate = DateTime.Now,
                UpdateDate = CreationDate
            });

            Permissions.Add(new PermissionBitViewModel()
            {
                PermissionBitEnum = PermissionBitEnum.Form180_Adimistrative_View,
                Group = GroupsStrings.GroupsManagement,
                CreationDate = DateTime.Now,
                UpdateDate = CreationDate
            });

            Permissions.Add(new PermissionBitViewModel()
            {
                PermissionBitEnum = PermissionBitEnum.Form180_Adimistrative_Editing,
                Group = GroupsStrings.GroupsManagement,
                CreationDate = DateTime.Now,
                UpdateDate = CreationDate
            });

            Permissions.Add(new PermissionBitViewModel()
            {
                PermissionBitEnum = PermissionBitEnum.Form180_Adimistrative_Approval,
                Group = GroupsStrings.GroupsManagement,
                CreationDate = DateTime.Now,
                UpdateDate = CreationDate
            });

            Permissions.Add(new PermissionBitViewModel()
            {
                PermissionBitEnum = PermissionBitEnum.Form180_Instructors_View,
                Group = GroupsStrings.GroupsManagement,
                CreationDate = DateTime.Now,
                UpdateDate = CreationDate
            });

            Permissions.Add(new PermissionBitViewModel()
            {
                PermissionBitEnum = PermissionBitEnum.Form180_Instructors_Editing,
                Group = GroupsStrings.GroupsManagement,
                CreationDate = DateTime.Now,
                UpdateDate = CreationDate
            });

            Permissions.Add(new PermissionBitViewModel()
            {
                PermissionBitEnum = PermissionBitEnum.Form180_Instructors_Approval,
                Group = GroupsStrings.GroupsManagement,
                CreationDate = DateTime.Now,
                UpdateDate = CreationDate
            });

            Permissions.Add(new PermissionBitViewModel()
            {
                PermissionBitEnum = PermissionBitEnum.Form180_Plan_View,
                Group = GroupsStrings.GroupsManagement,
                CreationDate = DateTime.Now,
                UpdateDate = CreationDate
            });

            Permissions.Add(new PermissionBitViewModel()
            {
                PermissionBitEnum = PermissionBitEnum.Form180_Plan_Editing,
                Group = GroupsStrings.GroupsManagement,
                CreationDate = DateTime.Now,
                UpdateDate = CreationDate
            });

            Permissions.Add(new PermissionBitViewModel()
            {
                PermissionBitEnum = PermissionBitEnum.Form180_Plan_Approval,
                Group = GroupsStrings.GroupsManagement,
                CreationDate = DateTime.Now,
                UpdateDate = CreationDate
            });

            Permissions.Add(new PermissionBitViewModel()
            {
                PermissionBitEnum = PermissionBitEnum.Form180_MoveForm,
                Group = GroupsStrings.GroupsManagement,
                CreationDate = DateTime.Now,
                UpdateDate = CreationDate
            });


            //Manage Training and Instructors

            Permissions.Add(new PermissionBitViewModel()
            {
                PermissionBitEnum = PermissionBitEnum.Training_Viewing,
                Group = InstructorsStrings.TrainingsManagement,
                CreationDate = DateTime.Now,
                UpdateDate = CreationDate
            });

            Permissions.Add(new PermissionBitViewModel()
            {
                PermissionBitEnum = PermissionBitEnum.Training_Editing,
                Group = InstructorsStrings.TrainingsManagement,
                CreationDate = DateTime.Now,
                UpdateDate = CreationDate
            });

            Permissions.Add(new PermissionBitViewModel()
            {
                PermissionBitEnum = PermissionBitEnum.Training_Deleting,
                Group = InstructorsStrings.TrainingsManagement,
                CreationDate = DateTime.Now,
                UpdateDate = CreationDate
            });

            Permissions.Add(new PermissionBitViewModel()
            {
                PermissionBitEnum = PermissionBitEnum.Training_ClosingTraining,
                Group = InstructorsStrings.TrainingsManagement,
                CreationDate = DateTime.Now,
                UpdateDate = CreationDate
            });

            Permissions.Add(new PermissionBitViewModel()
            {
                PermissionBitEnum = PermissionBitEnum.Training_UpdateInstructorCertificate,
                Group = InstructorsStrings.TrainingsManagement,
                CreationDate = DateTime.Now,
                UpdateDate = CreationDate
            });


            Permissions.Add(new PermissionBitViewModel()
            {
                PermissionBitEnum = PermissionBitEnum.Reports_ParticipationInTraining,
                Group = InstructorsStrings.TrainingsManagement,
                CreationDate = DateTime.Now,
                UpdateDate = CreationDate
            });

            // Instructors permissions
            Permissions.Add(new PermissionBitViewModel()
            {
                PermissionBitEnum = PermissionBitEnum.Instructors_Viewing,
                Group = InstructorsStrings.InstructorsManagement,
                CreationDate = DateTime.Now,
                UpdateDate = CreationDate
            });

            Permissions.Add(new PermissionBitViewModel()
            {
                PermissionBitEnum = PermissionBitEnum.Instructors_Editing,
                Group = InstructorsStrings.InstructorsManagement,
                CreationDate = DateTime.Now,
                UpdateDate = CreationDate
            });

            Permissions.Add(new PermissionBitViewModel()
            {
                PermissionBitEnum = PermissionBitEnum.Instructors_Deleting,
                Group = InstructorsStrings.InstructorsManagement,
                CreationDate = DateTime.Now,
                UpdateDate = CreationDate
            });

            Permissions.Add(new PermissionBitViewModel()
            {
                PermissionBitEnum = PermissionBitEnum.Instructors_UpdateCetificate,
                Group = InstructorsStrings.InstructorsManagement,
                CreationDate = DateTime.Now,
                UpdateDate = CreationDate
            });

            Permissions.Add(new PermissionBitViewModel()
            {
                PermissionBitEnum = PermissionBitEnum.Instructors_UpdateExpiration,
                Group = InstructorsStrings.InstructorsManagement,
                CreationDate = DateTime.Now,
                UpdateDate = CreationDate
            });

            Permissions.Add(new PermissionBitViewModel()
            {
                PermissionBitEnum = PermissionBitEnum.Instructors_Files_Viewing,
                Group = InstructorsStrings.InstructorsManagement,
                CreationDate = DateTime.Now,
                UpdateDate = CreationDate
            });

            Permissions.Add(new PermissionBitViewModel()
            {
                PermissionBitEnum = PermissionBitEnum.Instructors_Files_Editing,
                Group = InstructorsStrings.InstructorsManagement,
                CreationDate = DateTime.Now,
                UpdateDate = CreationDate
            });

            Permissions.Add(new PermissionBitViewModel()
            {
                PermissionBitEnum = PermissionBitEnum.Instructors_Files_Deleting,
                Group = InstructorsStrings.InstructorsManagement,
                CreationDate = DateTime.Now,
                UpdateDate = CreationDate
            });

            Permissions.Add(new PermissionBitViewModel()
            {
                PermissionBitEnum = PermissionBitEnum.Instructors_TrackingNotes_Viewing,
                Group = InstructorsStrings.InstructorsManagement,
                CreationDate = DateTime.Now,
                UpdateDate = CreationDate
            });

            Permissions.Add(new PermissionBitViewModel()
            {
                PermissionBitEnum = PermissionBitEnum.Instructors_TrackingNotes_Editing,
                Group = InstructorsStrings.InstructorsManagement,
                CreationDate = DateTime.Now,
                UpdateDate = CreationDate
            });

            Permissions.Add(new PermissionBitViewModel()
            {
                PermissionBitEnum = PermissionBitEnum.Instructors_TrackingNotes_Deleting,
                Group = InstructorsStrings.InstructorsManagement,
                CreationDate = DateTime.Now,
                UpdateDate = CreationDate
            });

            Permissions.Add(new PermissionBitViewModel()
            {
                PermissionBitEnum = PermissionBitEnum.Instructors_CreateUser,
                Group = InstructorsStrings.InstructorsManagement,
                CreationDate = DateTime.Now,
                UpdateDate = CreationDate
            });

            //Reports viewing permission

            Permissions.Add(new PermissionBitViewModel()
            {
                PermissionBitEnum = PermissionBitEnum.Reports_ExpeditionStatus,
                Group = ReportsStrings.Reports,
                CreationDate = DateTime.Now,
                UpdateDate = CreationDate
            });

            Permissions.Add(new PermissionBitViewModel()
            {
                PermissionBitEnum = PermissionBitEnum.Reports_Viewing,
                Group = ReportsStrings.Reports,
                CreationDate = DateTime.Now,
                UpdateDate = CreationDate
            });

            Permissions.Add(new PermissionBitViewModel()
            {
                PermissionBitEnum = PermissionBitEnum.Reports_Agencies,
                Group = ReportsStrings.Reports,
                CreationDate = DateTime.Now,
                UpdateDate = CreationDate
            });

            Permissions.Add(new PermissionBitViewModel()
            {
                PermissionBitEnum = PermissionBitEnum.Reports_Groups,
                Group = ReportsStrings.Reports,
                CreationDate = DateTime.Now,
                UpdateDate = CreationDate
            });

            Permissions.Add(new PermissionBitViewModel()
            {
                PermissionBitEnum = PermissionBitEnum.Reports_Flights,
                Group = ReportsStrings.Reports,
                CreationDate = DateTime.Now,
                UpdateDate = CreationDate
            });

            Permissions.Add(new PermissionBitViewModel()
            {
                PermissionBitEnum = PermissionBitEnum.Reports_Schools,
                Group = ReportsStrings.Reports,
                CreationDate = DateTime.Now,
                UpdateDate = CreationDate
            });

            Permissions.Add(new PermissionBitViewModel()
            {
                PermissionBitEnum = PermissionBitEnum.Reports_AvailableSeats,
                Group = ReportsStrings.Reports,
                CreationDate = DateTime.Now,
                UpdateDate = CreationDate
            });

            Permissions.Add(new PermissionBitViewModel()
            {
                PermissionBitEnum = PermissionBitEnum.Reports_BusesPerDay,
                Group = ReportsStrings.Reports,
                CreationDate = DateTime.Now,
                UpdateDate = CreationDate
            });

            Permissions.Add(new PermissionBitViewModel()
            {
                PermissionBitEnum = PermissionBitEnum.Reports_ExportGroupsForSecurity,
                Group = ReportsStrings.Reports,
                CreationDate = DateTime.Now,
                UpdateDate = CreationDate
            });

            Permissions.Add(new PermissionBitViewModel()
            {
                PermissionBitEnum = PermissionBitEnum.Reports_PresentsOnTrip,
                Group = ReportsStrings.Reports,
                CreationDate = DateTime.Now,
                UpdateDate = CreationDate
            });

            Permissions.Add(new PermissionBitViewModel()
            {
                PermissionBitEnum = PermissionBitEnum.Reports_GroupAndDeputyManagers,
                Group = ReportsStrings.Reports,
                CreationDate = DateTime.Now,
                UpdateDate = CreationDate
            });

            Permissions.Add(new PermissionBitViewModel()
            {
                PermissionBitEnum = PermissionBitEnum.Reports_TrainingParticipation,
                Group = ReportsStrings.Reports,
                CreationDate = DateTime.Now,
                UpdateDate = CreationDate
            });

            Permissions.Add(new PermissionBitViewModel()
            {
                PermissionBitEnum = PermissionBitEnum.Reports_ContactInUrgency,
                Group = ReportsStrings.Reports,
                CreationDate = DateTime.Now,
                UpdateDate = CreationDate
            });

            Permissions.Add(new PermissionBitViewModel()
            {
                PermissionBitEnum = PermissionBitEnum.Reports_SchoolGlobalRemarks,
                Group = ReportsStrings.Reports,
                CreationDate = DateTime.Now,
                UpdateDate = CreationDate
            });

            Permissions.Add(new PermissionBitViewModel()
            {
                PermissionBitEnum = PermissionBitEnum.Reports_GroupsByRegion,
                Group = ReportsStrings.Reports,
                CreationDate = DateTime.Now,
                UpdateDate = CreationDate
            });

            Permissions.Add(new PermissionBitViewModel()
            {
                PermissionBitEnum = PermissionBitEnum.Reports_InstructorsAssignmentsForGroups,
                Group = ReportsStrings.Reports,
                CreationDate = DateTime.Now,
                UpdateDate = CreationDate
            });

            Permissions.Add(new PermissionBitViewModel()
            {
                PermissionBitEnum = PermissionBitEnum.Administration_AgencyViewing,
                Group = AdministrationStrings.SystemAdministration,
                CreationDate = DateTime.Now,
                UpdateDate = CreationDate
            });

            Permissions.Add(new PermissionBitViewModel()
            {
                PermissionBitEnum = PermissionBitEnum.Administration_AgencyEditing,
                Group = AdministrationStrings.SystemAdministration,
                CreationDate = DateTime.Now,
                UpdateDate = CreationDate
            });

            Permissions.Add(new PermissionBitViewModel()
            {
                PermissionBitEnum = PermissionBitEnum.Administration_AgencyDeleting,
                Group = AdministrationStrings.SystemAdministration,
                CreationDate = DateTime.Now,
                UpdateDate = CreationDate
            });

            Permissions.Add(new PermissionBitViewModel()
            {
                PermissionBitEnum = PermissionBitEnum.Administration_SchoolView,
                Group = AdministrationStrings.SystemAdministration,
                CreationDate = DateTime.Now,
                UpdateDate = CreationDate
            });

            Permissions.Add(new PermissionBitViewModel()
            {
                PermissionBitEnum = PermissionBitEnum.Administration_SchoolEdit,
                Group = AdministrationStrings.SystemAdministration,
                CreationDate = DateTime.Now,
                UpdateDate = CreationDate
            });

            Permissions.Add(new PermissionBitViewModel()
            {
                PermissionBitEnum = PermissionBitEnum.Administration_SchoolDelete,
                Group = AdministrationStrings.SystemAdministration,
                CreationDate = DateTime.Now,
                UpdateDate = CreationDate
            });

            Permissions.Add(new PermissionBitViewModel()
            {
                PermissionBitEnum = PermissionBitEnum.Administration_School_Remarks_View,
                Group = AdministrationStrings.SystemAdministration,
                CreationDate = DateTime.Now,
                UpdateDate = CreationDate
            });

            Permissions.Add(new PermissionBitViewModel()
            {
                PermissionBitEnum = PermissionBitEnum.Administration_School_Remarks_Edit,
                Group = AdministrationStrings.SystemAdministration,
                CreationDate = DateTime.Now,
                UpdateDate = CreationDate
            });

            Permissions.Add(new PermissionBitViewModel()
            {
                PermissionBitEnum = PermissionBitEnum.Administration_School_Remarks_Delete,
                Group = AdministrationStrings.SystemAdministration,
                CreationDate = DateTime.Now,
                UpdateDate = CreationDate
            });

            Permissions.Add(new PermissionBitViewModel()
            {
                PermissionBitEnum = PermissionBitEnum.Administration_Sites_View,
                Group = AdministrationStrings.SystemAdministration,
                CreationDate = DateTime.Now,
                UpdateDate = CreationDate
            });

            Permissions.Add(new PermissionBitViewModel()
            {
                PermissionBitEnum = PermissionBitEnum.Administration_Sites_Edit,
                Group = AdministrationStrings.SystemAdministration,
                CreationDate = DateTime.Now,
                UpdateDate = CreationDate
            });

            Permissions.Add(new PermissionBitViewModel()
            {
                PermissionBitEnum = PermissionBitEnum.Administration_Sites_Delete,
                Group = AdministrationStrings.SystemAdministration,
                CreationDate = DateTime.Now,
                UpdateDate = CreationDate
            });

            Permissions.Add(new PermissionBitViewModel()
            {
                PermissionBitEnum = PermissionBitEnum.Administration_City_View,
                Group = AdministrationStrings.SystemAdministration,
                CreationDate = DateTime.Now,
                UpdateDate = CreationDate
            });

            Permissions.Add(new PermissionBitViewModel()
            {
                PermissionBitEnum = PermissionBitEnum.Administration_City_Edit,
                Group = AdministrationStrings.SystemAdministration,
                CreationDate = DateTime.Now,
                UpdateDate = CreationDate
            });

            Permissions.Add(new PermissionBitViewModel()
            {
                PermissionBitEnum = PermissionBitEnum.Administration_City_Delete,
                Group = AdministrationStrings.SystemAdministration,
                CreationDate = DateTime.Now,
                UpdateDate = CreationDate
            });


            //Form 60
            Permissions.Add(new PermissionBitViewModel()
            {
                PermissionBitEnum = PermissionBitEnum.Form60_View,
                Group = GroupFormsStrings.Form60,
                CreationDate = DateTime.Now,
                UpdateDate = CreationDate
            });

            Permissions.Add(new PermissionBitViewModel()
            {
                PermissionBitEnum = PermissionBitEnum.Form60_EditManagerDetails,
                Group = GroupFormsStrings.Form60,
                CreationDate = DateTime.Now,
                UpdateDate = CreationDate
            });

            Permissions.Add(new PermissionBitViewModel()
            {
                PermissionBitEnum = PermissionBitEnum.Form60_Flights_View,
                Group = GroupFormsStrings.Form60,
                CreationDate = DateTime.Now,
                UpdateDate = CreationDate
            });

            Permissions.Add(new PermissionBitViewModel()
            {
                PermissionBitEnum = PermissionBitEnum.Form60_Flights_Edit,
                Group = GroupFormsStrings.Form60,
                CreationDate = DateTime.Now,
                UpdateDate = CreationDate
            });

            Permissions.Add(new PermissionBitViewModel()
            {
                PermissionBitEnum = PermissionBitEnum.Form60_Flights_EditDestinations,
                Group = GroupFormsStrings.Form60,
                CreationDate = DateTime.Now,
                UpdateDate = CreationDate
            });

            Permissions.Add(new PermissionBitViewModel()
            {
                PermissionBitEnum = PermissionBitEnum.Form60_TripPlan_View,
                Group = GroupFormsStrings.Form60,
                CreationDate = DateTime.Now,
                UpdateDate = CreationDate
            });

            Permissions.Add(new PermissionBitViewModel()
            {
                PermissionBitEnum = PermissionBitEnum.Form60_TripPlan_Edit,
                Group = GroupFormsStrings.Form60,
                CreationDate = DateTime.Now,
                UpdateDate = CreationDate
            });

            Permissions.Add(new PermissionBitViewModel()
            {
                PermissionBitEnum = PermissionBitEnum.Form60_Contacts_View,
                Group = GroupFormsStrings.Form60,
                CreationDate = DateTime.Now,
                UpdateDate = CreationDate
            });

            Permissions.Add(new PermissionBitViewModel()
            {
                PermissionBitEnum = PermissionBitEnum.Form60_Contacts_Edit,
                Group = GroupFormsStrings.Form60,
                CreationDate = DateTime.Now,
                UpdateDate = CreationDate
            });

            Permissions.Add(new PermissionBitViewModel()
            {
                PermissionBitEnum = PermissionBitEnum.Form60_FormStatus,
                Group = GroupFormsStrings.Form60,
                CreationDate = DateTime.Now,
                UpdateDate = CreationDate
            });

            Permissions.Add(new PermissionBitViewModel()
            {
                PermissionBitEnum = PermissionBitEnum.Form60_AgencyApprovalStatus,
                Group = GroupFormsStrings.Form60,
                CreationDate = DateTime.Now,
                UpdateDate = CreationDate
            });

            Permissions.Add(new PermissionBitViewModel()
            {
                PermissionBitEnum = PermissionBitEnum.Form60_PlanApprovalStatus,
                Group = GroupFormsStrings.Form60,
                CreationDate = DateTime.Now,
                UpdateDate = CreationDate
            });

            Permissions.Add(new PermissionBitViewModel()
            {
                PermissionBitEnum = PermissionBitEnum.Form60_FormSubmit,
                Group = GroupFormsStrings.Form60,
                CreationDate = DateTime.Now,
                UpdateDate = CreationDate
            });
        }
    }

    public class PermissionBitViewModel
    {
        public int ID { get; set; }
        public int PermissionBitID => (int)PermissionBitEnum;
        public PermissionBitEnum PermissionBitEnum { get; set; }
        public string Group { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime UpdateDate { get; set; }
        public bool Enabled { get; set; }

        public PermissionBitViewModel()
        {
        }
    }
}