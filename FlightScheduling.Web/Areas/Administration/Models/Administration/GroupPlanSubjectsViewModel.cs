﻿using FlightScheduling.Language;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FlightScheduling.Web.Areas.Administration.Models.Administration
{
    public class GroupPlanSubjectViewModel
    {

        //Properties
        public int ID { get; set; }
        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(AdministrationStrings ), Name = "PlanSubjectName")]
        public string Name { get; set; }
        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(AdministrationStrings), Name = "HoursRequired")]
        public int HoursRequired { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime UpdateDate { get; set; }

        public bool IsUpdate { get; set; }

        public GroupPlanSubjectViewModel()
        {
            IsUpdate = false;
        }
    }

    public class GroupPlanSubjectsViewModel
    {
        public IEnumerable<GroupPlanSubjectViewModel> Subjects { get; set; }

    }
}