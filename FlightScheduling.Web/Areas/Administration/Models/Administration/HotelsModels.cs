﻿using Endilo.Helpers;
using FlightScheduling.DAL;
using FlightScheduling.Language;
using FlightScheduling.Web.Areas.Administration.Models.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FlightScheduling.Web.Areas.Administration.Models.Administration
{
    public class SitesViewModel
    {
        // Filters parameters
        [Display(ResourceType = typeof(AdministrationStrings), Name = "SiteType")]
        public CustomSelectList SiteTypes { get; set; }

        public IEnumerable<int> SelectedSiteTypes { get; set; }

        public IEnumerable<int> SelectedCities { get; set; }

        [Display(ResourceType = typeof(AdministrationStrings), Name = "City")]
        public CustomSelectList Cities { get; set; }

        [Display(ResourceType = typeof(AdministrationStrings), Name = "SiteName")]
        public string SiteName { get; set; }

        public IEnumerable<SiteViewModel> Sites { get; set; }

        public SitesViewModel()
        {
            SiteTypes = new CustomSelectList();
            foreach (eSiteType siteType in (eSiteType[])Enum.GetValues(typeof(eSiteType)))
            {
                SiteTypes.Items.Add(new CustomSelectListItem()
                {
                    ID = (int)siteType,
                    Name = siteType.GetDescription()
                });
            }

            Cities = new CustomSelectList();
        }
    }

    public class CityViewModel
    {
        public int CityID { get; set; }

        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(AdministrationStrings), Name = "City")]
        public string CityName { get; set; }

        [Display(ResourceType = typeof(AdministrationStrings), Name = "CityExternalID")]
        public string CityExternalID { get; set; }

        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(AdministrationStrings), Name = "District")]
        public int DistrictID { get; set; }
        public string DistrictName { get; set; }

        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(AdministrationStrings), Name = "Region")]
        public int RegionID { get; set; }
        public string RegionName { get; set; }

        
        public CustomSelectList Districts { get; set; }
        public CustomSelectList Regions { get; set; }

        public bool IsUpdate { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime UpdateDate { get; set; }

        public CityViewModel()
        {
            Districts = new CustomSelectList();
            Regions = new CustomSelectList();
            IsUpdate = false;
        }
    }

    public class CitiesViewModel
    {

        //Filters
        [Display(ResourceType = typeof(AdministrationStrings), Name = "Region")]
        public CustomSelectList Regions { get; set; }

        public IEnumerable<int> SelectedRegions { get; set; }

        [Display(ResourceType = typeof(AdministrationStrings), Name = "District")]
        public CustomSelectList Districts { get; set; }

        public IEnumerable<int> SelectedDistricts { get; set; }

        [Display(ResourceType = typeof(AdministrationStrings), Name = "City")]
        public string CityName { get; set; }


        public IEnumerable<CityViewModel> Cities { get; set; }

        public CitiesViewModel()
        {
            Regions = new CustomSelectList();
            Districts = new CustomSelectList();
        }
    }

    public class SiteViewModel
    {
        public bool IsUpdate { get; set; }

        public int ID { get; set; }

        [Display(ResourceType = typeof(AdministrationStrings), Name = "SiteExtenalID")]
        public int? ExternalID { get; set; }

        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(AdministrationStrings), Name = "SiteType")]
        public eSiteType SiteTypeEnum { get => (eSiteType)SiteType; set => SiteType = (int)value; }

        public int SiteType { get; set; }

        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(AdministrationStrings), Name = "SiteName")]
        public string Name { get; set; }

        [Display(ResourceType = typeof(AdministrationStrings), Name = "LocalName")]
        public string LocalName { get; set; }

        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(AdministrationStrings), Name = "City")]
        public int CityID { get; set; }

        public CustomSelectList Cities { get; set; }

        [Display(ResourceType = typeof(AdministrationStrings), Name = "Address")]
        public string Address { get; set; }


        [Display(ResourceType = typeof(AdministrationStrings), Name = "Limitations")]
        public string Limitations { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime UpdateDate { get; set; }
        public bool IsActive { get; set; }

        public CityViewModel City { get; set; }

        public SiteViewModel()
        {
            IsUpdate = false;
            IsActive = true;
            Cities = new CustomSelectList();
        }
    }


}