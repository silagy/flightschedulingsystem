﻿using Flight.DAL.FlightDB.Methods;
using FlightScheduling.DAL.CustomEntities.Notifications;
using FlightScheduling.Language;
using FlightScheduling.Web.Areas.Administration.Models.Common;
using FlightScheduling.Web.Infrastructure.Notifications;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace FlightScheduling.Web.Areas.Administration.Models.Administration
{
    public class NotificationTemplateViewModel
    {
        //properties
        public int ID { get; set; }

        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(NotificationStrings), Name = "Name")]
        public string Name { get; set; }

        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(NotificationStrings), Name = "NotificationTriggerType")]
        public eNotificationTriggerType NotificationTriggerType { get; set; }

        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(NotificationStrings), Name = "Subject")]
        public string Subject { get; set; }

        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(NotificationStrings), Name = "Message")]
        [AllowHtml]
        public string Message { get; set; }

        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(NotificationStrings), Name = "NotificationSchedulingType")]
        public eNotificationSchedulingType NotificationSchedulingType { get; set; }

        [Display(ResourceType = typeof(NotificationStrings), Name = "IsActive")]
        public bool IsActive { get; set; }

        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(NotificationStrings), Name = "UserRole")]
        public eRole UserTypeEnum { get; set; }

        public CustomSelectList UserPermissionRoles { get; set; }

        [Display(ResourceType = typeof(NotificationStrings), Name = "UserPermmisionRole")]
        public int UserPermissionRole { get; set; }

        public Dictionary<string, string> CustomProperties { get; set; }

        public bool IsUpdate { get; set; }

        public NotificationTemplateViewModel()
        {
            IsActive = true;
            IsUpdate = false;
            CustomProperties = new Dictionary<string, string>();
            UserPermissionRoles = new CustomSelectList();
        }

        public void SetCustomProperties()
        {
            switch (NotificationTriggerType)
            {
                case eNotificationTriggerType.GroupForm180Creation:
                    {
                        CustomProperties = NotificationHelper.GetPropertiesNames<NotificationForm180Creation>();
                        break;
                    }
                case eNotificationTriggerType.ExpeditionApproval:
                    break;
                case eNotificationTriggerType.GroupFormAdministration180StatusChange:
                case eNotificationTriggerType.GroupFormInstructors180StatusChange:
                case eNotificationTriggerType.GroupFormPlan180StatusChange:
                    {
                        CustomProperties = NotificationHelper.GetPropertiesNames<NotificationForm180StatusUpdate>();
                        break;
                    }
                case eNotificationTriggerType.NewGroupFile:
                    {
                        CustomProperties = NotificationHelper.GetPropertiesNames<NotificationFile180CustomDetails>();
                        break;
                    }
                case eNotificationTriggerType.GroupForm60Updated:
                    {
                        CustomProperties = NotificationHelper.GetPropertiesNames<NotificationForm60StatusUpdate>();
                        break;
                    }
                default:
                    break;
            }
        }



    }

    public class NotificationsTemplatesViewModel
    {
        //Properties
        public List<NotificationTemplateViewModel> Notifications { get; set; }

        public NotificationsTemplatesViewModel()
        {
            Notifications = new List<NotificationTemplateViewModel>();
        }
    }

    public class GlobalNotification
    {
        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(NotificationStrings), Name = "Subject")]
        public string Subject { get; set; }

        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(NotificationStrings), Name = "Message")]
        [AllowHtml]
        public string Message { get; set; }

        public List<RecipentModel> Recipients { get; set; }

        public GlobalNotification()
        {
            Recipients = new List<RecipentModel>();
        }
    }

    public class RecipentModel
    {
        public int ID { get; set; }
        public string Email { get; set; }
    }

}