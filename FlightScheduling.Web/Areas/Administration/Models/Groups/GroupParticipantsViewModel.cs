﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using CsvHelper.Configuration;
using CsvHelper.TypeConversion;
using Endilo.ExportToExcel;
using FlightScheduling.DAL;
using FlightScheduling.DAL.CustomEntities.Groups;
using FlightScheduling.Language;
using FlightScheduling.Web.Areas.Administration.Models.Common;
using FlightScheduling.Web.Infrastructure;
using LinqToExcel.Attributes;

namespace FlightScheduling.Web.Areas.Administration.Models.Groups
{
    public class GroupParticipantsViewModel
    {
        public int GroupID { get; set; }
        public List<GroupParticipantViewModel> Participants { get; set; }
        public GroupFolderDataViewModel GroupMainData { get; set; }

        public GroupParticipantsViewModel()
        {
            Participants = new List<GroupParticipantViewModel>();
            GroupMainData = new GroupFolderDataViewModel();
        }
    }

    public class GroupParticipantViewModel
    {
        //Properties
        public int ID { get; set; }
        public int GroupID { get; set; }
        [ExportToExcel(ResourceType = typeof(GroupsStrings), HeaderName = "ParticipantID")]
        [ExcelColumn("תעודת זהות")]
        [Display(ResourceType = typeof(GroupsStrings), Name = "ParticipantID")]
        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        public string ParticipantID { get; set; }

        [ExportToExcel(ResourceType = typeof(GroupsStrings), HeaderName = "FirstNameHe")]
        [ExcelColumn("שם פרטי בעברית")]
        [Display(ResourceType = typeof(GroupsStrings), Name = "FirstNameHe")]
        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        public string FirstNameHe { get; set; }

        [ExportToExcel(ResourceType = typeof(GroupsStrings), HeaderName = "LastNameHe")]
        [ExcelColumn("שם משפחה בעברית")]
        [Display(ResourceType = typeof(GroupsStrings), Name = "LastNameHe")]
        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        public string LastNameHe { get; set; }

        public string FullNameHe => String.Format("{0} {1}", FirstNameHe, LastNameHe);

        [ExportToExcel(ResourceType = typeof(GroupsStrings), HeaderName = "FirstNameEn")]
        [ExcelColumn("שם פרטי באנגלית")]
        [Display(ResourceType = typeof(GroupsStrings), Name = "FirstNameEn")]
        public string FirstNameEn { get; set; }

        [ExportToExcel(ResourceType = typeof(GroupsStrings), HeaderName = "LastNameEn")]
        [ExcelColumn("שם משפחה באנגלית")]
        [Display(ResourceType = typeof(GroupsStrings), Name = "LastNameEn")]
        public string LastNameEn { get; set; }

        [ExportToExcel(ResourceType = typeof(GroupsStrings), HeaderName = "PassportNumber")]
        [ExcelColumn("מספר דרכון")]
        [Display(ResourceType = typeof(GroupsStrings), Name = "PassportNumber")]
        public string PassportNumber { get; set; }

        [ExportToExcel(ResourceType = typeof(GroupsStrings), HeaderName = "PassportExperationDate")]
        [ExcelColumn("תוקף דרכון")]
        [Display(ResourceType = typeof(GroupsStrings), Name = "PassportExperationDate")]
        public DateTime? PassportExperationDate { get; set; }

        [ExportToExcel(ResourceType = typeof(GroupsStrings), HeaderName = "Birthday")]
        [ExcelColumn("תאריך לידה")]
        [Display(ResourceType = typeof(GroupsStrings), Name = "Birthday")]
        public DateTime? Birthday { get; set; }

        [ExportToExcel(ResourceType = typeof(GroupsStrings), HeaderName = "SEX")]
        [ExcelColumn("מיגדר")]
        [Display(ResourceType = typeof(GroupsStrings), Name = "SEX")]
        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        public eParticipantsSex SEX { get; set; }

        [ExportToExcel(ResourceType = typeof(GroupsStrings), HeaderName = "ParticipantStatus")]
        [Display(ResourceType = typeof(GroupsStrings), Name = "ParticipantStatus")]
        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        public eParticipantStatus ParticipantStatusEnum { get; set; }

        [ExportToExcel(ResourceType = typeof(GroupsStrings), HeaderName = "ParticipantType")]
        [ExcelColumn("סוג משתתף")]
        [Display(ResourceType = typeof(GroupsStrings), Name = "ParticipantType")]
        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        public eParticipantType ParticipantTypeEnum { get; set; }

        public List<ParticipantTypeMenu> ParticipantTypeMenu { get; set; }


        [ExportToExcel(ResourceType = typeof(SchoolsStrings), HeaderName = "InstituteID")]
        [ExcelColumn("סמל מוסד")]
        [Display(ResourceType = typeof(SchoolsStrings), Name = "InstituteID")]
        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        public int SchoolInstituteID { get; set; }

        [Display(ResourceType = typeof(SchoolsStrings), Name = "Name")]
        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        public CustomSelectList Schools { get; set; }

        public int? ClassType { get; set; }

        [ExportToExcel(ResourceType = typeof(GroupsStrings), HeaderName = "ClassType")]
        [ExcelColumn("שכבת גיל")]
        [Display(ResourceType = typeof(GroupsStrings), Name = "ClassType")]
        public eParticipantClassType ClassTypeEnum
        {
            get => ClassType != null ? (eParticipantClassType)ClassType : eParticipantClassType.None;
            set => ClassType = (int)value;
        }

        [ExportToExcel(ResourceType = typeof(CommonStrings), HeaderName = "Comments")]
        [ExcelColumn("הערות")]
        [Display(ResourceType = typeof(CommonStrings), Name = "Comments")]
        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        public string Comments { get; set; }

        public DateTime CreationDate { get; set; }
        public DateTime UpdateDate { get; set; }

        public int UserID { get; set; }

        public bool UpdateOperation { get; set; }

        public GroupParticipantViewModel()
        {
            UpdateOperation = false;
            Schools = new CustomSelectList();
            ParticipantTypeMenu = new List<ParticipantTypeMenu>();
            ParticipantTypeMenu.Add(new Groups.ParticipantTypeMenu()
            {
                Description = eParticipantType.Teacher.GetEnumDescription(),
                ParticipantType = eParticipantType.Teacher
            });
            ParticipantTypeMenu.Add(new Groups.ParticipantTypeMenu()
            {
                Description = eParticipantType.Student.GetEnumDescription(),
                ParticipantType = eParticipantType.Student
            });
            ParticipantTypeMenu.Add(new Groups.ParticipantTypeMenu()
            {
                Description = eParticipantType.Parent.GetEnumDescription(),
                ParticipantType = eParticipantType.Parent
            });
        }
    }

    public class ParticipantTypeMenu
    {
        public string Description { get; set; }
        public eParticipantType ParticipantType { get; set; }
    }

    public sealed class GroupParticipantCsvMap : ClassMap<GroupParticipantViewModel>
    {
        public GroupParticipantCsvMap()
        {
            Map(m => m.ParticipantID).Index(0);
            Map(m => m.FirstNameHe).Index(1);
            Map(m => m.LastNameHe).Index(2);
            Map(m => m.FirstNameEn).Index(3);
            Map(m => m.LastNameEn).Index(4);
            Map(m => m.PassportNumber).Index(5);
            Map(m => m.PassportExperationDate).Index(6).TypeConverter<CsvConverterHelper.CsvDateConverter>();
            Map(m => m.Birthday).Index(7).TypeConverter<CsvConverterHelper.CsvDateConverter>();
            Map(m => m.SEX)
                .Index(8)
                .ConvertUsing(row =>
                {
                    var sex = eParticipantsSex.Female;
                    switch (row.GetField(8))
                    {
                        case "זכר":
                            {
                                sex = eParticipantsSex.Male;
                                break;
                            }
                        case "נקבה":
                            {
                                sex = eParticipantsSex.Female;
                                break;
                            }
                    }

                    return sex;
                });

            Map(m => m.ParticipantTypeEnum)
                .Index(9)
                .ConvertUsing(row =>
                {
                    var participantType = eParticipantType.Student;
                    switch (row.GetField(9))
                    {
                        case "תלמיד":
                            {
                                participantType = eParticipantType.Student;
                                break;
                            }
                        case "מורה":
                            {
                                participantType = eParticipantType.Teacher;
                                break;
                            }
                        case "אחר":
                            {
                                participantType = eParticipantType.Other;
                                break;
                            }
                    }

                    return participantType;
                });


        }
    }

    public class ImportParticipantsViewModel
    {
        public int GroupID { get; set; }
        public int UserID { get; set; }

        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(GroupsStrings), Name = "Attachment")]
        [FileTypeValidation("XLS,XLSX")]
        public HttpPostedFileBase Attachment { get; set; }
    }

    /// <summary>
    /// This is a joined table of all the members that are going on the expedition.
    /// It is designed to report back on all participants in the expedition
    /// </summary>
    public class JoinedGroupParticipantViewModel
    {
        public int ID { get; set; }
        public int GroupID { get; set; }
        [ExportToExcel(ResourceType = typeof(GroupsStrings), HeaderName = "ParticipantID")]
        [ExcelColumn("תעודת זהות")]
        [Display(ResourceType = typeof(GroupsStrings), Name = "ParticipantID")]
        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        public string PersonalID { get; set; }

        [ExportToExcel(ResourceType = typeof(GroupsStrings), HeaderName = "FirstNameHe")]
        [ExcelColumn("שם פרטי בעברית")]
        [Display(ResourceType = typeof(GroupsStrings), Name = "FirstNameHe")]
        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        public string FirstNameHe { get; set; }

        [ExportToExcel(ResourceType = typeof(GroupsStrings), HeaderName = "LastNameHe")]
        [ExcelColumn("שם משפחה בעברית")]
        [Display(ResourceType = typeof(GroupsStrings), Name = "LastNameHe")]
        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        public string LastNameHe { get; set; }

        public string FullNameHe => String.Format("{0} {1}", FirstNameHe, LastNameHe);

        [ExportToExcel(ResourceType = typeof(GroupsStrings), HeaderName = "FirstNameEn")]
        [ExcelColumn("שם פרטי באנגלית")]
        [Display(ResourceType = typeof(GroupsStrings), Name = "FirstNameEn")]
        public string FirstNameEn { get; set; }

        [ExportToExcel(ResourceType = typeof(GroupsStrings), HeaderName = "LastNameEn")]
        [ExcelColumn("שם משפחה באנגלית")]
        [Display(ResourceType = typeof(GroupsStrings), Name = "LastNameEn")]
        public string LastNameEn { get; set; }

        [ExportToExcel(ResourceType = typeof(GroupsStrings), HeaderName = "PassportNumber")]
        [ExcelColumn("מספר דרכון")]
        [Display(ResourceType = typeof(GroupsStrings), Name = "PassportNumber")]
        public string PassportNumber { get; set; }

        [ExportToExcel(ResourceType = typeof(GroupsStrings), HeaderName = "SEX")]
        [ExcelColumn("מיגדר")]
        [Display(ResourceType = typeof(GroupsStrings), Name = "SEX")]
        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        public eParticipantsSex SEX { get; set; }

        public int RoleID { get; set; }

        public eParticipantIntenalType ParticipantInternalType { get; set; }

        [ExportToExcel(ResourceType = typeof(GroupsStrings), HeaderName = "ParticipantType")]
        [ExcelColumn("סוג משתתף")]
        [Display(ResourceType = typeof(GroupsStrings), Name = "ParticipantType")]
        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        public string RoleName
        {
            get
            {
                if (ParticipantInternalType == eParticipantIntenalType.Participant)
                {
                    return ((eParticipantType)RoleID).GetEnumDescription();
                }
                return ((eInstructorRoleType)RoleID).GetEnumDescription();
            }
        }
    }

    public class GroupJoinedPaticipantsViewModel
    {
        public List<JoinedGroupParticipantViewModel> ParticipantList { get; set; }

        public GroupJoinedPaticipantsViewModel()
        {
            ParticipantList = new List<JoinedGroupParticipantViewModel>();
        }
    }

    public enum eParticipantIntenalType
    {
        Stuff,
        Participant
    }
}