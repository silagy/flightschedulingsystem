﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Endilo.ExportToExcel;
using FlightScheduling.DAL;
using FlightScheduling.DAL.CustomEntities.Groups;
using FlightScheduling.Language;
using FlightScheduling.Web.Areas.Administration.Models.Common;
using FlightScheduling.Web.Areas.Administration.Models.Instructors;
using FlightScheduling.Web.Models.Expeditions;

namespace FlightScheduling.Web.Areas.Administration.Models.Groups
{

    public class GroupViewModel
    {
        //Properties
        public int ID { get; set; }
        public int OrderID { get; set; }
        //public ExpeditionViewModel Expedition { get; set; }
        public int GroupExternalID { get; set; }
        public int NumberOfBuses { get; set; }
        public int NumberOfParticipants { get; set; }
        public GroupStatusEnum GroupStatus { get; set; }
        public GroupColor Color { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime UpdateDate { get; set; }
        public DateTime DepartureDate { get; set; }
        public int AgencyID { get; set; }

        public GroupViewModel()
        {
            CreationDate = DateTime.Now;
            UpdateDate = DateTime.Now;
            Color = new GroupColor();
        }
    }

    public class GroupsCreationViewModel
    {
        //Properties
        public List<GroupViewModel> Groups { get; set; }
        public int LastGroupID { get; set; }
        public int ParticipantsPerBus { get; set; }
        public Expeditions Order { get; set; }
        public List<GroupColor> GroupColors { get; set; }
        public bool UpdateGroup { get; set; }

        public GroupsCreationViewModel()
        {
            Groups = new List<GroupViewModel>();
            GroupColors = new List<GroupColor>();
        }
    }

    public class GroupNoteViewModel
    {
        //Properties
        [Display(ResourceType = typeof(CommonStrings), Name = "ID")]
        public int ID { get; set; }
        public int UserID { get; set; }

        [Display(ResourceType = typeof(AccountStrings), Name = "FirstName")]
        public string UserFirstName { get; set; }

        [Display(ResourceType = typeof(AccountStrings), Name = "LastName")]
        public string UserLastName { get; set; }
        public int GroupID { get; set; }


        [Display(ResourceType = typeof(SchoolsStrings), Name = "Name")]
        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        public CustomSelectList Schools { get; set; }

        public int SchoolID { get; set; }

        public string SchoolName { get; set; }

        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        [AllowHtml]
        [Display(ResourceType = typeof(GroupsStrings), Name = "Note")]
        public string Notes { get; set; }
        [Display(ResourceType = typeof(CommonStrings), Name = "UpdateDate")]
        public DateTime UpdateDate { get; set; }
        [Display(ResourceType = typeof(CommonStrings), Name = "CreationDate")]
        public DateTime CreationDate { get; set; }

        public bool UpdateOperation { get; set; }

        public GroupNoteViewModel()
        {
            UpdateOperation = false;
            Schools = new CustomSelectList();
        }
    }

    public class GroupInstructorsViewModel
    {
        [Display(ResourceType = typeof(InstructorsStrings), Name = "SearchInstructor")]
        public string InstructorID { get; set; }


        public int GroupID { get; set; }
        public List<GroupInstructorsViewModelShortData> Instructors { get; set; }
        public CustomSelectList Roles { get; set; }

        public GroupInstructorsViewModel()
        {
            Instructors = new List<GroupInstructorsViewModelShortData>();
            Roles = new CustomSelectList();
        }
    }

    public class AddorUpdateStafToGroupViewModel
    {
        public int GroupID { get; set; }
        public int ID { get; set; }

        [Display(ResourceType = typeof(GroupsStrings), Name = "StafIsGroupManager")]
        public bool IsGroupManager { get; set; }

        [Display(ResourceType = typeof(GroupsStrings), Name = "StafGroupDeputy")]
        public bool IsDeputyDirector { get; set; }


        [Display(ResourceType = typeof(CommonStrings), Name = "Comments")]
        public string Comments { get; set; }

        public int SchoolID { get; set; }

        [Display(ResourceType = typeof(SchoolsStrings), Name = "Name")]
        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        public CustomSelectList Schools { get; set; }

        [Display(ResourceType = typeof(ReportsStrings), Name = "csvInstructorGroupRoleName")]
        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        public CustomSelectList Roles { get; set; }

        public int RoleID { get; set; }

        public int InstructorID { get; set; }
        public string InstructorExternalID { get; set; }

        public bool IsUpdate { get; set; }

        public AddorUpdateStafToGroupViewModel()
        {
            Schools = new CustomSelectList();
            Roles = new CustomSelectList();
            Roles.Items.Add(new CustomSelectListItem()
            {
                ID = (int)eInstructorRoleType.AccompanyingMentor,
                Name = eInstructorRoleType.AccompanyingMentor.GetEnumDescription()
            });

            Roles.Items.Add(new CustomSelectListItem()
            {
                ID = (int)eInstructorRoleType.Instructor,
                Name = eInstructorRoleType.Instructor.GetEnumDescription()
            });

            Roles.Items.Add(new CustomSelectListItem()
            {
                ID = (int)eInstructorRoleType.ExpeditionManager,
                Name = eInstructorRoleType.ExpeditionManager.GetEnumDescription()
            });
        }
    }

    public class UpdateGroupAgencyViewModel
    {
        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        public int AgencyID { get; set; }
        public int GroupID { get; set; }

        [Display(ResourceType = typeof(AgenciesStrings), Name = "Name")]
        public CustomSelectList Agencies { get; set; }

        public UpdateGroupAgencyViewModel()
        {
            Agencies = new CustomSelectList();
        }
    }

}