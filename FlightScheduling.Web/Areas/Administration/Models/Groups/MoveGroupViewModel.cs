﻿using FlightScheduling.DAL;
using FlightScheduling.DAL.CustomEntities;
using FlightScheduling.DAL.CustomEntities.Groups;
using FlightScheduling.Language;
using FlightScheduling.Web.Areas.Administration.Models.Common;
using FlightScheduling.Web.Areas.Administration.Models.Flights;
using FlightScheduling.Web.Areas.Administration.Models.Trips;
using FlightScheduling.Web.Models.Expeditions;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlightScheduling.Web.Areas.Administration.Models.Groups
{
    public class MoveGroupFinalStepViewModel
    {
        public int OriginalGroupID { get; set; }
        public int OriginalOrderID { get; set; }

        public int SelectedOrderID { get; set; }

        public TransactionResult transactionResult { get; set; }

    }

    public class MoveGroupSelectTrip
    {
        public int GroupID { get; set; }
        public int SchoolID { get; set; }

        public int OrderID { get; set; }

        public OrderDetailsViewModel OriginalOrder { get; set; }
        public TripDetailsViewModel OriginalTrip { get; set; }


        public IEnumerable<TripandOrderViewModel> Orders { get; set; }
    }


    public class TripandOrderViewModel
    {
        [Display(ResourceType = typeof(TripsStrings), Name = "ID")]
        public int TripID { get; set; }


        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [Display(ResourceType = typeof(TripsStrings), Name = "DepartureDate")]
        public DateTime DepartureDate { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [Display(ResourceType = typeof(TripsStrings), Name = "ReturnDate")]
        public DateTime ReturnDate { get; set; }

        public int DestinationID { get; set; }

        [Display(ResourceType = typeof(TripsStrings), Name = "Destenation")]
        public FlightViewModel.FlightsFields Destenation => (FlightViewModel.FlightsFields)DestinationID;

        public int ReturingField { get; set; }

        [Display(ResourceType = typeof(TripsStrings), Name = "TakeOffField")]
        public FlightViewModel.FlightsFields ReturnedFrom => (FlightViewModel.FlightsFields)ReturingField;


        [Display(ResourceType = typeof(ExpeditionStrings), Name = "ID")]
        public int OrderID { get; set; }

        [Display(ResourceType = typeof(ExpeditionStrings), Name = "NumOfPassangersRequired")]
        public int NumOfPassangersRequired { get; set; }

        [Display(ResourceType = typeof(ExpeditionStrings), Name = "BusesRequiredPerDay")]
        public int BusesRequiredPerDay { get; set; }

        [Display(ResourceType = typeof(ExpeditionStrings), Name = "ExpeditionManager")]
        public string ExpeditionManager { get; set; }

        public int Status { get; set; }

        [Display(ResourceType = typeof(ExpeditionStrings), Name = "Status")]
        public eExpeditionStatus eStatus => (eExpeditionStatus)Status;

        public int AgentStatus { get; set; }

        [Display(ResourceType = typeof(ExpeditionStrings), Name = "AgentStatus")]
        public eAgentExpeditionStatus eAgentStatus => (eAgentExpeditionStatus)AgentStatus;
    }

    public class OrderDetailsViewModel
    {
        [Display(ResourceType = typeof(ExpeditionStrings), Name = "ID")]
        public int ID { get; set; }

        public int TripID { get; set; }

        [Display(ResourceType = typeof(ExpeditionStrings), Name = "NumOfPassangersRequired")]
        public int NumOfPassangersRequired { get; set; }

        [Display(ResourceType = typeof(ExpeditionStrings), Name = "BusesRequiredPerDay")]
        public int BusesRequiredPerDay { get; set; }

        [Display(ResourceType = typeof(ExpeditionStrings), Name = "ExpeditionManager")]
        public string ExpeditionManager { get; set; }

        [Display(ResourceType = typeof(ExpeditionStrings), Name = "ExpeditionManagerPhone")]
        public string ExpeditionManagerPhone { get; set; }

        [Display(ResourceType = typeof(ExpeditionStrings), Name = "ExpeditionManagerCell")]
        public string ExpeditionManagerCell { get; set; }

        [Display(ResourceType = typeof(ExpeditionStrings), Name = "ExpeditionManagerEmail")]
        public string ExpeditionManagerEmail { get; set; }

        public int Status { get; set; }

        [Display(ResourceType = typeof(ExpeditionStrings), Name = "Status")]
        public eExpeditionStatus eStatus => (eExpeditionStatus)Status;

        public int AgentStatus { get; set; }

        [Display(ResourceType = typeof(ExpeditionStrings), Name = "AgentStatus")]
        public eAgentExpeditionStatus eAgentStatus => (eAgentExpeditionStatus) AgentStatus;

    }

    public class TripDetailsViewModel
    {
        [Display(ResourceType = typeof(TripsStrings), Name = "ID")]
        public int TripID { get; set; }


        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [Display(ResourceType = typeof(TripsStrings), Name = "DepartureDate")]
        public DateTime DepartureDate { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [Display(ResourceType = typeof(TripsStrings), Name = "ReturnDate")]
        public DateTime ReturningDate { get; set; }

        public int DestinationID { get; set; }

        [Display(ResourceType = typeof(TripsStrings), Name = "Destenation")]
        public FlightViewModel.FlightsFields Destenation => (FlightViewModel.FlightsFields)DestinationID;

        public int ReturingField { get; set; }

        [Display(ResourceType = typeof(TripsStrings), Name = "TakeOffField")]
        public FlightViewModel.FlightsFields ReturnedFrom => (FlightViewModel.FlightsFields)ReturingField;
    }

}
