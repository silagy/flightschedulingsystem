﻿using AutoMapper.QueryableExtensions;
using Flight.DAL.FlightDB.Methods;
using FlightScheduling.DAL;
using FlightScheduling.DAL.CustomEntities.Groups;
using FlightScheduling.Language;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using FlightScheduling.Web.Areas.Administration.Models.Common;
using FlightScheduling.Web.Infrastructure;

namespace FlightScheduling.Web.Areas.Administration.Models.Groups
{
    public class GroupFolderViewModel
    {
        //Properties
        public int GroupID { get; set; }
        public GroupFolderDataViewModel GroupFolderData { get; set; }
        public List<GroupFileViewModel> Files { get; set; }
        public List<GroupFileViewModel> OtherFiles { get; set; }
        public List<int> MainFilesList { get; set; }
        public List<int> OtherFilesList { get; set; }
        public List<GroupNoteViewModel> Notes { get; set; }
        public GroupInstructorsViewModel Instructors { get; set; }
        public IEnumerable<GroupSchoolsViewModel> Schools { get; set; }



        public GroupFolderViewModel()
        {
            Files = new List<GroupFileViewModel>();
            OtherFiles = new List<GroupFileViewModel>();
            MainFilesList = new List<int>();
            MainFilesList.Add((int)eGroupFileType.F180170);
            MainFilesList.Add((int)eGroupFileType.F60);
            MainFilesList.Add((int)eGroupFileType.FinalPDF);
            MainFilesList.Add((int)eGroupFileType.BusDistribution);
            MainFilesList.Add((int)eGroupFileType.RoomRoster);
            MainFilesList.Add((int)eGroupFileType.SpecialFood);
            MainFilesList.Add((int)eGroupFileType.SpecialRequests);
            MainFilesList.Add((int)eGroupFileType.SplitEducationActivity);
            MainFilesList.Add((int)eGroupFileType.SpecialRequests);
            OtherFilesList = new List<int>();
            OtherFilesList.Add((int)eGroupFileType.Other);
            Notes = new List<GroupNoteViewModel>();
            GroupFolderData = new GroupFolderDataViewModel();
            Instructors = new GroupInstructorsViewModel();

        }
    }

    public class GroupSchoolsViewModel
    {
        public int ID { get; set; }
        public int GroupID { get; set; }
        public int SchoolID { get; set; }
        public int InstituteID { get; set; }
        public string SchoolName { get; set; }
        public bool IsPrimary { get; set; }
        public bool IsAdministrativeInstitude { get; set; }
        public string Manager { get; set; }
        public string ManagerPhone { get; set; }
        public string ManagerEmail { get; set; }
        public int? Region { get; set; }
        public eSchoolRegion? RegionEnum => Region == null ? eSchoolRegion.None : (eSchoolRegion)Region;
    }

    public class GroupSubSchoolViewModel
    {
        public int ID { get; set; }
        public int GroupID { get; set; }
        public int SchoolID { get; set; }
        public bool IsPrimary { get; set; }

        public bool IsUpdate { get; set; }

        [Display(ResourceType = typeof(GroupsStrings), Name = "NumberOfInstructors")]
        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        public int NumberOfInstructors { get; set; }

        [Display(ResourceType = typeof(SchoolsStrings), Name = "Name")]
        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        public CustomSelectList Schools { get; set; }

        public GroupSubSchoolViewModel()
        {
            IsPrimary = false;
            Schools = new CustomSelectList();
            IsUpdate = false;
        }
    }

    public class GroupFolderDataViewModel
    {
        public GroupFolderCustom GroupFolderData { get; set; }
        public int AssignedParticipants { get; set; }
    }


    public class GroupFileViewModel
    {
        //Properties

        public int ID { get; set; }
        public int GroupID { get; set; }

        [Display(ResourceType = typeof(GroupsStrings), Name = "FileName")]
        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        public string Name { get; set; }

        public string Url { get; set; }

        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(GroupsStrings), Name = "Attachment")]
        [FileTypeValidation("PDF,doc,docx")]
        public HttpPostedFileBase Attachment { get; set; }

        [Display(ResourceType = typeof(GroupsStrings), Name = "FileComments")]
        public string Comments { get; set; }
        public System.DateTime CreationDate { get; set; }
        public int UserID { get; set; }

        [Display(ResourceType = typeof(GroupsStrings), Name = "FileType")]
        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        public eGroupFileType FileType { get; set; }

        [Display(ResourceType = typeof(GroupsStrings), Name = "FileStatus")]
        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        public eGroupFileStatus FileStatus { get; set; }

        [Display(ResourceType = typeof(GroupsStrings), Name = "CountyManagerApproval")]
        public bool CountyManagerApproval { get; set; }

        [Display(ResourceType = typeof(GroupsStrings), Name = "CountyManagerComments")]
        public string CountyManagerComments { get; set; }

        [Display(ResourceType = typeof(AccountStrings), Name = "FirstName")]
        public string UserFirstName { get; set; }

        [Display(ResourceType = typeof(AccountStrings), Name = "LastName")]
        public string UserLastName { get; set; }


        public GroupFileViewModel()
        {
            CreationDate = DateTime.Now;
        }
    }

    public class GroupsDetailsViewModel
    {
        //Properties
        [Display(ResourceType = typeof(CommonStrings), Name = "FromDate")]
        public DateTime StartDate { get; set; }
        [Display(ResourceType = typeof(CommonStrings), Name = "To")]
        public DateTime EndDate { get; set; }

        [Display(ResourceType = typeof(ExpeditionStrings), Name = "ID")]
        public int? OrderID { get; set; }

        [Display(ResourceType = typeof(GroupsStrings), Name = "ExpeditionID")]
        public int? GroupID { get; set; }

        [Display(ResourceType = typeof(TripsStrings), Name = "ID")]
        public int? TripID { get; set; }

        [Display(ResourceType = typeof(GroupsStrings), Name = "ExpeditionFinalStatus")]
        public eExpeditionStatus? GroupFinalStatus { get; set; }

        public int? SchoolID { get; set; }

        public int? AgencyID { get; set; }

        public int? Region { get; set; }

        public int? ShowAdministrative { get; set; }

        public bool? IsPrimary { get; set; }

        public IEnumerable<int> GroupIDs { get; set; }

        public List<int> GroupStatusID
        {
            get
            {
                var list = new List<int>();
                if (this.GroupFinalStatus != null)
                {
                    list.Add((int)this.GroupFinalStatus);
                    return list;
                }
                else
                {
                    list.Add((int)eExpeditionStatus.Approved);
                    list.Add((int)eExpeditionStatus.InProcess);
                    list.Add((int)eExpeditionStatus.PendingApproval);
                    return list;
                }
            }
        }

        [Display(ResourceType = typeof(SchoolsStrings), Name = "InstituteID")]
        public string InstituteID { get; set; }


        public List<GroupFolderCustom> Groups { get; set; }
        public List<GroupFolderCustomNewView> GroupsNewView { get; set; }

        public bool NewLayout { get; set; }

        public GroupsDetailsViewModel()
        {
            Groups = new List<GroupFolderCustom>();
            StartDate = SystemConfigurationRepository.GroupsFilterStartDate;
            EndDate = SystemConfigurationRepository.GroupsFilterEndDate;
            NewLayout = true;
            IsPrimary = true;
        }
    }

    public class GroupContactDetailsViewMode
    {
        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(GroupsStrings), Name = "ContactName")]
        public string ContactName { get; set; }

        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(GroupsStrings), Name = "ContactPhone")]
        public string ContactPhone { get; set; }

        public int GroupID { get; set; }
    }

    public class GroupFinalStatusViewMode
    {
        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(GroupsStrings), Name = "ExpeditionFinalStatus")]
        public eFinalStatus FinalStatus { get; set; }

        public int GroupID { get; set; }
    }

    public class GroupExternalIDViewMode
    {
        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(GroupsStrings), Name = "ExpeditionID")]
        public int ExternalID { get; set; }

        public int GroupID { get; set; }
        public DateTime DepartureDate { get; set; }
    }


}