﻿using FlightScheduling.DAL;
using FlightScheduling.Language;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FlightScheduling.Web.Areas.Administration.Models.Groups
{
    public class DailyDashboardViewModel
    {

        //Properties
        public DateTime EndDate { get; set; }

        [Display(ResourceType = typeof(DailyDashboardStrings), Name = "LastUpdateDate")]
        public DateTime LastUpdateDate { get; set; }

        [Display(ResourceType = typeof(DailyDashboardStrings), Name = "CurrentDate")]
        public DateTime StartDate { get; set; }

        [Display(ResourceType = typeof(DailyDashboardStrings), Name = "NumberOfBuses")]
        public int NumberOfBuses { get; set; }

        [Display(ResourceType = typeof(DailyDashboardStrings), Name = "NumberOfGroups")]
        public int NumberOfGroups { get; set; }

        [Display(ResourceType = typeof(DailyDashboardStrings), Name = "NumberOfParticipants")]
        public int NumberOfParticipants { get; set; }

        public IEnumerable<DailyDashboardRawData> GroupsRawData { get; set; }
        public IEnumerable<DailyDashboardGroupsData> GroupsData { get; set; }
        public DailyDashboardViewModel()
        {
            LastUpdateDate = DateTime.Now;
        }
    }

    public class DailyDashboardRawData
    {
        public int GroupID { get; set; }
        public int NumberOfBuses { get; set; }
        public int NumberOfParticipants { get; set; }
        public int GroupExternalID { get; set; }
        public DateTime ArrivalTime { get; set; }

    }

    public class DailyDashboardGroupsData
    {
        public int GroupID { get; set; }
        [Display(ResourceType = typeof(GroupsStrings), Name = "ExpeditionID")]
        public int GroupExternalID { get; set; }
        public int SchoolID { get; set; }
        [Display(ResourceType = typeof(SchoolsStrings), Name = "InstituteID")]
        public int InstituteID { get; set; }
        [Display(ResourceType = typeof(SchoolsStrings), Name = "Name")]
        public string SchoolName { get; set; }
        public int? Region { get; set; }

        [Display(ResourceType = typeof(SchoolsStrings), Name = "Region")]
        public eSchoolRegion? RegionEnum
        {
            get
            {
                if (Region != null)
                {
                    return (eSchoolRegion)Region;
                }

                return null;
            }
        }

        public string AgencyName { get; set; }
    }
}