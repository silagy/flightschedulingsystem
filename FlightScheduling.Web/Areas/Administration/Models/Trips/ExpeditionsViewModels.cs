﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using FlightScheduling.Language;
using FlightScheduling.DAL;
using FlightScheduling.Web.Areas.Administration.Models.Flights;
using FlightScheduling.Web.Models.Expeditions;
using FlightScheduling.Web.Areas.Administration.Models.Common;

namespace FlightScheduling.Web.Areas.Administration.Models.Trips
{
    public class ExpeditionFlightViewModel
    {
        
        [Display(ResourceType = typeof(ExpeditionStrings), Name = "ID")]
        public int ExpeditionID { get; set; }

        public ExpeditionsViewModel Expedition { get; set; }
        
        public List<FlightDetailsViewModel> DepartureFLights { get; set; }
        public List<FlightDetailsViewModel> ReturningFLights { get; set; }



        public ExpeditionFlightViewModel()
        {
            DepartureFLights = new List<FlightDetailsViewModel>();
            ReturningFLights = new List<FlightDetailsViewModel>();
            

            Expedition = new ExpeditionsViewModel();
        }

    }


    public class FlightDetailsViewModel
    {
        [Display(ResourceType = typeof(FlightsStrings), Name = "ID")]
        public int ID { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [Display(ResourceType = typeof(FlightsStrings), Name = "DepartureDate")]
        public DateTime? DepartureDate { get; set; }

        [Display(ResourceType = typeof(FlightsStrings), Name = "DepartureField")]
        public FlightViewModel.FlightsFields DepartureField { get; set; }

        public bool TakeOffFromIsrael { get; set; }

        [Display(ResourceType = typeof(FlightsStrings), Name = "DestenationField")]
        public FlightViewModel.FlightsFields DestenationField { get; set; }

        [Display(ResourceType = typeof(FlightsStrings), Name = "NumberOfPassengers")]
        public int NumberOfPassengers { get; set; }

        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(FlightsStrings), Name = "AirlineName")]
        public string AirlineName { get; set; }

        [Display(ResourceType = typeof(FlightsStrings), Name = "FlightNumber")]
        public string FlightNumber { get; set; }

        [Display(ResourceType = typeof(FlightsStrings), Name = "Comments")]
        public string Comments { get; set; }

        //Properties
        public int RemainingSeats { get; set; }

        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(FlightsStrings), Name = "AssignPassengers")]
        public int AssignPassengers { get; set; }

        public FlightDetailsViewModel()
        {
            RemainingSeats = 0;
        }
    }


    public class MoveOrderViewModel
    {
        public int OrderID { get; set; }
        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(TripsStrings), Name = "SelectTrip")]
        public CustomSelectList Trips { get; set; }

        public int TripID { get; set; }

        public MoveOrderViewModel()
        {
            Trips = new CustomSelectList();
        }
    }

    public class FutureTripsViewModel
    {
        public int ID { get; set; }
        public DateTime DepartureDate { get; set; }
        public int DestinationID { get; set; }
    }

}