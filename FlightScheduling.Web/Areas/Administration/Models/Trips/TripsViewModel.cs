﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Endilo.GenericSearch;
using FlightScheduling.DAL;
using FlightScheduling.Language;
using FlightScheduling.Web.Areas.Administration.Models.Common;
using FlightScheduling.Web.Areas.Administration.Models.Flights;
using FlightScheduling.Web.Infrastructure;
using FlightScheduling.Web.Models.Expeditions;
using LinqToExcel.Attributes;

namespace FlightScheduling.Web.Areas.Administration.Models.Trips
{
    public class TripsViewModel
    {
        public List<TripViewModelBasic> TripViewModelBasics { get; set; }

        [Display(ResourceType = typeof(CommonStrings), Name = "FromDate")]
        public DateTime FilterFromDate { get; set; }

        [Display(ResourceType = typeof(CommonStrings), Name = "To")]
        public DateTime FilterToDate { get; set; }

        //public ICollection<AbstractSearch> Filters
        //{
        //    get
        //    {
        //        var filters = new List<AbstractSearch>();
        //        filters.AddDefaultSearchCriteria<FlightScheduling.DAL.Trips>(m => m.DepartureDate, TripsStrings.DepartureDate);
        //        return filters;
        //    }
        //}

        public TripsViewModel()
        {
            TripViewModelBasics = new List<TripViewModelBasic>();
        }
    }

    public class TripManagementViewModel
    {
        public TripViewModelBasic TripViewModelBasic { get; set; }

        public TripSummaryDetailsViewModel TripSummaryDetailsViewModel { get; set; }

        public TripOutGoingFlightsViewModel TripOutGoingFlightsViewModel { get; set; }

        public TripInComingFlightsViewModel TripInComingFlightsViewModel { get; set; }

        public List<ExpeditionsViewModel> Expeditions { get; set; }

        public TripManagementViewModel()
        {
            TripViewModelBasic = new TripViewModelBasic();
            TripSummaryDetailsViewModel = new TripSummaryDetailsViewModel();
            TripOutGoingFlightsViewModel = new TripOutGoingFlightsViewModel();
            TripInComingFlightsViewModel = new TripInComingFlightsViewModel();
            Expeditions = new List<ExpeditionsViewModel>();
        }
    }

    public class TripViewModelBasic : BasicDetails
    {
        //Properties

        [Display(ResourceType = typeof(TripsStrings), Name = "ID")]
        [ExcelColumn("מזהה מסע")]
        public int ID { get; set; }


        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(TripsStrings), Name = "DepartureDate")]
        [ExcelColumn("תאריך יציאה")]
        public DateTime DepartureDate { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(TripsStrings), Name = "ReturnDate")]
        [ExcelColumn("תאריך חזרה")]
        public DateTime ReturnDate { get; set; }

        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(TripsStrings), Name = "Destenation")]
        [ExcelColumn("מיקום נחיתה")]
        public FlightViewModel.FlightsFields Destenation { get; set; }

        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(TripsStrings), Name = "TakeOffField")]
        [ExcelColumn("מיקום ההמראה")]
        public FlightViewModel.FlightsFields ReturnedFrom { get; set; }

        [DataType(DataType.MultilineText)]
        [Display(ResourceType = typeof(TripsStrings), Name = "Comments")]
        [ExcelColumn("הערות")]
        public string Comments { get; set; }

        [Display(ResourceType = typeof(TripsStrings), Name = "IsActive")]
        public bool IsActive { get; set; }

        [Display(ResourceType = typeof(TripsStrings), Name = "NumberOfExpeditions")]
        public int NumberOfExpeditions { get; set; }

        public TripViewModelBasic()
        {
            DepartureDate = DateTime.Now;
            ReturnDate = DateTime.Now;
        }

    }

    public class TripExtendedViewModel : TripViewModelBasic
    {

        //Properties

        public List<FlightViewModel> FlightsViewModels { get; set; }

        //Add expedations


        public TripExtendedViewModel()
        {
            FlightsViewModels = new List<FlightViewModel>();
        }

    }

    public class TripSummaryDetailsViewModel
    {
        public int NumberOfOutGoingFlights { get; set; }

        public int NumberOfInComingFlights { get; set; }

        public int NumOfExpeditions { get; set; }

        public int NumOfBuses { get; set; }

        public TripSummaryDetailsViewModel()
        {
            NumOfBuses = 0;
            NumOfExpeditions = 0;
            NumberOfOutGoingFlights = 0;
            NumberOfInComingFlights = 0;
        }
    }

    public class TripOutGoingFlightsViewModel
    {
        public int TripID { get; set; }

        public List<FlightManagementViewModel> FlightViewModels { get; set; }

        public TripOutGoingFlightsViewModel()
        {
            FlightViewModels = new List<FlightManagementViewModel>();
        }
    }

    public class TripInComingFlightsViewModel
    {
        public int TripID { get; set; }

        public List<FlightManagementViewModel> FlightViewModels { get; set; }

        public TripInComingFlightsViewModel()
        {
            FlightViewModels = new List<FlightManagementViewModel>();
        }
    }

    public class LinkFlightSelectionViewModel
    {
        public int TripID { get; set; }

        public bool DepartureFlight { get; set; }

        public List<FlightManagementViewModel> FlightViewModels { get; set; }

        public LinkFlightSelectionViewModel()
        {
            FlightViewModels = new List<FlightManagementViewModel>();
        }
    }

    public class ExpeditionsViewModel
    {
        //Properties
        [Display(ResourceType = typeof(ExpeditionStrings), Name = "ID")]
        public int ID { get; set; }

        public int TripID { get; set; }

        [Display(ResourceType = typeof(ExpeditionStrings), Name = "NumOfPassangersRequired")]
        public int NumOfPassangersRequired { get; set; }

        public DAL.Schools School { get; set; }

        [Display(ResourceType = typeof(ExpeditionStrings), Name = "BusesRequiredPerDay")]
        public int BusesRequiredPerDay { get; set; }

        [Display(ResourceType = typeof(ExpeditionStrings), Name = "ExpeditionManager")]
        public string ExpeditionManager { get; set; }

        [Display(ResourceType = typeof(ExpeditionStrings), Name = "ExpeditionManagerPhone")]
        public string ExpeditionManagerPhone { get; set; }

        [Display(ResourceType = typeof(ExpeditionStrings), Name = "ExpeditionManagerCell")]
        public string ExpeditionManagerCell { get; set; }

        [Display(ResourceType = typeof(ExpeditionStrings), Name = "ExpeditionManagerEmail")]
        public string ExpeditionManagerEmail { get; set; }

        [Display(ResourceType = typeof(ExpeditionStrings), Name = "Comments")]
        public string Comments { get; set; }

        [Display(ResourceType = typeof(ExpeditionStrings), Name = "Status")]
        public eExpeditionStatus Status { get; set; }

        [Display(ResourceType = typeof(ExpeditionStrings), Name = "RegisteredPassengers")]
        public int RegisteredPassengers { get; set; }

        [Display(ResourceType = typeof(ExpeditionStrings), Name = "AgentStatus")]
        public eAgentExpeditionStatus AgentStatus { get; set; }

        [Display(ResourceType = typeof(CommonStrings), Name = "CreationDate")]
        public DateTime CreationDate { get; set; }

        public bool HasGroups { get; set; }


    }

    public class ImportTripsViewModel
    {
        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(GroupsStrings), Name = "Attachment")]
        [FileTypeValidation("XLS,XLSX")]
        public HttpPostedFileBase Attachment { get; set; }
    }




}