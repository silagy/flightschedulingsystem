﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Flight.DAL.FlightDB.Methods;
using FlightScheduling.DAL;
using FlightScheduling.Language;
using FlightScheduling.Website.Models.Account;
using FlightScheduling.Web.Areas.Administration.Models.Common;

namespace FlightScheduling.Web.Areas.Administration.Models
{
    public class AgentViewModel : UserModel
    {

        [Display(ResourceType = typeof(AccountStrings), Name = "Agency")]
        public int AgencyID { get; set; }

        public List<AgencyViewModelBasic> AgenciesModel { get; set; }

        public AgentViewModel()
        {
            // init
            AgenciesModel = new List<AgencyViewModelBasic>();
            AgenciesModel.Add(new AgencyViewModelBasic()
                {
                    ID = -1 ,
                    Name = ValidationStrings.EnterValue
                });
            Role = eRole.Agent;

        }
        
    }

    public class CountyManagerViewModel : UserModel
    {
        [Display(ResourceType = typeof(SchoolsStrings), Name = "Region")]
        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        public eSchoolRegion Region { get; set; }

        public CountyManagerViewModel()
        {
            this.Role = eRole.CountyManager;
        }
    }


    public class CountyManagerAssosicateSchool
    {
        public int[] SchoolID { get; set; }
        public int UserID { get; set; }

        [Display(ResourceType = typeof(SchoolsStrings), Name = "Name")]
        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        public CustomSelectList Schools { get; set; }

        public CountyManagerAssosicateSchool()
        {
            Schools = new CustomSelectList();
        }
    }

    public class AgencyViewModelBasic
    {
        public int ID { get; set; }
        public string Name { get; set; }
    }

    public class AdministratorViewModel : UserModel
    {
        

        public AdministratorViewModel()
        {
            Role = eRole.Administrator;

        }

    }

    public class StafViewModel : UserModel
    {
        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(AccountStrings), Name = "UserRole")]
        public eRoleType? RoleType { get; set; }

        public StafViewModel()
        {
            Role = eRole.Staf;

        }

    }

    public class TrainingManagerViewModel : UserModel
    {
        [Display(ResourceType = typeof(SchoolsStrings), Name = "Region")]
        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        public eSchoolRegion? Region { get; set; }

        public TrainingManagerViewModel()
        {
            Role = eRole.TrainingManager;

        }

    }

    public class ViewerViewModel : UserModel
    {

        public ViewerViewModel()
        {
            Role = eRole.Viewer;
        }

    }

    public class UsersViewModel
    {
        //Properties
        [Display(ResourceType = typeof(AccountStrings), Name = "Role")]
        public eRole? Roles { get; set; }
        public int? RoleID { get; set; }

        [Display(ResourceType = typeof(AccountStrings), Name = "EmailAddress")]
        public string Email { get; set; }

        [Display(ResourceType = typeof(AccountStrings), Name = "FullName")]
        public string Name { get; set; }

        [Display(ResourceType = typeof(AccountStrings), Name = "ShowDeletedUsers")]
        public bool ShowDeletedUsers { get; set; }

        [Display(ResourceType = typeof(SchoolsStrings), Name = "IsActive")]
        public CustomSelectList Activation { get; set; }

        public IEnumerable<int> SelectedActivation { get; set; }

        public IEnumerable<UserModel> UserModels { get; set; }

        public UsersViewModel()
        {
            UserModels = new List<UserModel>();
            ShowDeletedUsers = false;
            Activation = new CustomSelectList();
            Activation.Items.Add(new CustomSelectListItem()
            {
                ID = 0,
                Name = CommonStrings.AccessDenied
            });

            Activation.Items.Add(new CustomSelectListItem()
            {
                ID = 1,
                Name = CommonStrings.AccessAllowed
            });
        }
    }


}