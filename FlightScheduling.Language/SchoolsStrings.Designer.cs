﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace FlightScheduling.Language {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "17.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class SchoolsStrings {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal SchoolsStrings() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("FlightScheduling.Language.SchoolsStrings", typeof(SchoolsStrings).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Add School.
        /// </summary>
        public static string AddNewSchool {
            get {
                return ResourceManager.GetString("AddNewSchool", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Center Region.
        /// </summary>
        public static string CenterRegion {
            get {
                return ResourceManager.GetString("CenterRegion", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to City.
        /// </summary>
        public static string City {
            get {
                return ResourceManager.GetString("City", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Country Region.
        /// </summary>
        public static string CountryRegion {
            get {
                return ResourceManager.GetString("CountryRegion", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Deleting School.
        /// </summary>
        public static string DeletingSchool {
            get {
                return ResourceManager.GetString("DeletingSchool", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Edit School.
        /// </summary>
        public static string EditSchool {
            get {
                return ResourceManager.GetString("EditSchool", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Email.
        /// </summary>
        public static string Email {
            get {
                return ResourceManager.GetString("Email", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Fax.
        /// </summary>
        public static string Fax {
            get {
                return ResourceManager.GetString("Fax", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Hifa Region.
        /// </summary>
        public static string HifaRegion {
            get {
                return ResourceManager.GetString("HifaRegion", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to School ID.
        /// </summary>
        public static string ID {
            get {
                return ResourceManager.GetString("ID", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Institute ID.
        /// </summary>
        public static string InstituteID {
            get {
                return ResourceManager.GetString("InstituteID", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Enabled Login.
        /// </summary>
        public static string IsActivated {
            get {
                return ResourceManager.GetString("IsActivated", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Is Active.
        /// </summary>
        public static string IsActive {
            get {
                return ResourceManager.GetString("IsActive", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Administrative School.
        /// </summary>
        public static string IsAdministrative {
            get {
                return ResourceManager.GetString("IsAdministrative", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Select it, if several expeditions are going under this organization.
        /// </summary>
        public static string IsAdministrativeNote {
            get {
                return ResourceManager.GetString("IsAdministrativeNote", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Special Education.
        /// </summary>
        public static string IsSpecialEducation {
            get {
                return ResourceManager.GetString("IsSpecialEducation", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Jerusalem Region.
        /// </summary>
        public static string JerusalemRegion {
            get {
                return ResourceManager.GetString("JerusalemRegion", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Schools&apos;s manager.
        /// </summary>
        public static string Manager {
            get {
                return ResourceManager.GetString("Manager", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Manager&apos;s cell phone.
        /// </summary>
        public static string ManagerCell {
            get {
                return ResourceManager.GetString("ManagerCell", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Manager&apos;s email.
        /// </summary>
        public static string ManagerEmail {
            get {
                return ResourceManager.GetString("ManagerEmail", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Manager&apos;s phone.
        /// </summary>
        public static string ManagerPhone {
            get {
                return ResourceManager.GetString("ManagerPhone", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Shcool Name.
        /// </summary>
        public static string Name {
            get {
                return ResourceManager.GetString("Name", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to North Region.
        /// </summary>
        public static string NorthRegion {
            get {
                return ResourceManager.GetString("NorthRegion", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Orthodox Region.
        /// </summary>
        public static string OrthodoxRegion {
            get {
                return ResourceManager.GetString("OrthodoxRegion", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Phone.
        /// </summary>
        public static string Phone {
            get {
                return ResourceManager.GetString("Phone", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Region.
        /// </summary>
        public static string Region {
            get {
                return ResourceManager.GetString("Region", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Religious State.
        /// </summary>
        public static string ReligiousState {
            get {
                return ResourceManager.GetString("ReligiousState", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to School Details.
        /// </summary>
        public static string SchoolDetails {
            get {
                return ResourceManager.GetString("SchoolDetails", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to School Remarks.
        /// </summary>
        public static string SchoolRemarks {
            get {
                return ResourceManager.GetString("SchoolRemarks", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to School Management.
        /// </summary>
        public static string SchoolsManagementTitle {
            get {
                return ResourceManager.GetString("SchoolsManagementTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Settlements Region.
        /// </summary>
        public static string SettlementsRegion {
            get {
                return ResourceManager.GetString("SettlementsRegion", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to South Region.
        /// </summary>
        public static string SouthRegion {
            get {
                return ResourceManager.GetString("SouthRegion", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to State.
        /// </summary>
        public static string State {
            get {
                return ResourceManager.GetString("State", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Stream.
        /// </summary>
        public static string Stream {
            get {
                return ResourceManager.GetString("Stream", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Tel-Aviv Region.
        /// </summary>
        public static string TelAvivRegion {
            get {
                return ResourceManager.GetString("TelAvivRegion", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Ulpana.
        /// </summary>
        public static string Ulpana {
            get {
                return ResourceManager.GetString("Ulpana", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Work Office.
        /// </summary>
        public static string WorkOffice {
            get {
                return ResourceManager.GetString("WorkOffice", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Yeshiva.
        /// </summary>
        public static string Yeshiva {
            get {
                return ResourceManager.GetString("Yeshiva", resourceCulture);
            }
        }
    }
}
