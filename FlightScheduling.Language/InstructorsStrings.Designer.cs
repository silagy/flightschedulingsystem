﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace FlightScheduling.Language {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "17.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class InstructorsStrings {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal InstructorsStrings() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("FlightScheduling.Language.InstructorsStrings", typeof(InstructorsStrings).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Accompanying Mentor.
        /// </summary>
        public static string AccompanyingMentor {
            get {
                return ResourceManager.GetString("AccompanyingMentor", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Add File.
        /// </summary>
        public static string AddFile {
            get {
                return ResourceManager.GetString("AddFile", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Add Instructor Details.
        /// </summary>
        public static string AddInstructor {
            get {
                return ResourceManager.GetString("AddInstructor", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Add Instructor Role.
        /// </summary>
        public static string AddInstructorRole {
            get {
                return ResourceManager.GetString("AddInstructorRole", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Add Note.
        /// </summary>
        public static string AddNote {
            get {
                return ResourceManager.GetString("AddNote", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Adress.
        /// </summary>
        public static string Address {
            get {
                return ResourceManager.GetString("Address", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Add Training.
        /// </summary>
        public static string AddTraining {
            get {
                return ResourceManager.GetString("AddTraining", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Allow Changing Role.
        /// </summary>
        public static string AllowChangingRole {
            get {
                return ResourceManager.GetString("AllowChangingRole", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Allow Login.
        /// </summary>
        public static string AllowLogin {
            get {
                return ResourceManager.GetString("AllowLogin", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Automatic renewal.
        /// </summary>
        public static string AutomaticRenewal {
            get {
                return ResourceManager.GetString("AutomaticRenewal", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Beginning Of the Year.
        /// </summary>
        public static string BeginningOfYear {
            get {
                return ResourceManager.GetString("BeginningOfYear", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Cell Phone.
        /// </summary>
        public static string CellPhone {
            get {
                return ResourceManager.GetString("CellPhone", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Certificate ID.
        /// </summary>
        public static string CertificateID {
            get {
                return ResourceManager.GetString("CertificateID", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to City.
        /// </summary>
        public static string City {
            get {
                return ResourceManager.GetString("City", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Report Training Completion.
        /// </summary>
        public static string CompleteTraining {
            get {
                return ResourceManager.GetString("CompleteTraining", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to You are about to report the completion of this training. Please note, the system will check if the registered instructors are entitle for license renewal..
        /// </summary>
        public static string CompleteTrainingInstructions {
            get {
                return ResourceManager.GetString("CompleteTrainingInstructions", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Training Completion Date.
        /// </summary>
        public static string CompletionDate {
            get {
                return ResourceManager.GetString("CompletionDate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Create actual user.
        /// </summary>
        public static string CreateActualUser {
            get {
                return ResourceManager.GetString("CreateActualUser", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Instructor group role name.
        /// </summary>
        public static string csvInstructorGroupRoleName {
            get {
                return ResourceManager.GetString("csvInstructorGroupRoleName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Delete Instructor.
        /// </summary>
        public static string DeleteInstructor {
            get {
                return ResourceManager.GetString("DeleteInstructor", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Delete Instructor Remark.
        /// </summary>
        public static string DeleteInstructorRemark {
            get {
                return ResourceManager.GetString("DeleteInstructorRemark", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Delete Training.
        /// </summary>
        public static string DeleteTraining {
            get {
                return ResourceManager.GetString("DeleteTraining", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Expedition Manager.
        /// </summary>
        public static string ExpeditionManager {
            get {
                return ResourceManager.GetString("ExpeditionManager", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Validity.
        /// </summary>
        public static string ExpirationDaysRule {
            get {
                return ResourceManager.GetString("ExpirationDaysRule", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to First Name English.
        /// </summary>
        public static string FirstNameEN {
            get {
                return ResourceManager.GetString("FirstNameEN", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to First Name Hebrew.
        /// </summary>
        public static string FirstNameHE {
            get {
                return ResourceManager.GetString("FirstNameHE", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to First Name in Hebrew was not provided.
        /// </summary>
        public static string FirstNameHeNull {
            get {
                return ResourceManager.GetString("FirstNameHeNull", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to First Name in English was not provided.
        /// </summary>
        public static string FirstNameNameEnNull {
            get {
                return ResourceManager.GetString("FirstNameNameEnNull", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Full Name English.
        /// </summary>
        public static string FullNameEN {
            get {
                return ResourceManager.GetString("FullNameEN", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Full Name Hebrew.
        /// </summary>
        public static string FullNameHE {
            get {
                return ResourceManager.GetString("FullNameHE", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Generate ID.
        /// </summary>
        public static string GenerateID {
            get {
                return ResourceManager.GetString("GenerateID", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Headquaters.
        /// </summary>
        public static string Headquaters {
            get {
                return ResourceManager.GetString("Headquaters", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Holocost Institute.
        /// </summary>
        public static string HolocostInstitute {
            get {
                return ResourceManager.GetString("HolocostInstitute", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Home Phone.
        /// </summary>
        public static string HomePhone {
            get {
                return ResourceManager.GetString("HomePhone", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to completed.
        /// </summary>
        public static string HoursCompleted {
            get {
                return ResourceManager.GetString("HoursCompleted", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to {0} hours has been completed out of {1} hours.
        /// </summary>
        public static string HoursCompletedOutOfRequired {
            get {
                return ResourceManager.GetString("HoursCompletedOutOfRequired", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Import Instructors Registration.
        /// </summary>
        public static string ImportInstructorsRegistration {
            get {
                return ResourceManager.GetString("ImportInstructorsRegistration", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Import New Instructors.
        /// </summary>
        public static string ImportNewInstructors {
            get {
                return ResourceManager.GetString("ImportNewInstructors", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Import Registration Log.
        /// </summary>
        public static string ImportRegistrationLog {
            get {
                return ResourceManager.GetString("ImportRegistrationLog", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Initial Training.
        /// </summary>
        public static string InitialTraining {
            get {
                return ResourceManager.GetString("InitialTraining", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The instructor is not available at that time, he is allocated to another expedition.
        /// </summary>
        public static string InstrucorNotAvailable {
            get {
                return ResourceManager.GetString("InstrucorNotAvailable", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Instructor.
        /// </summary>
        public static string Instructor {
            get {
                return ResourceManager.GetString("Instructor", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Instructor is already assinged to the group.
        /// </summary>
        public static string InstructorAlreadyAssigned {
            get {
                return ResourceManager.GetString("InstructorAlreadyAssigned", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Instructor does not exist.
        /// </summary>
        public static string InstructorDoesntExist {
            get {
                return ResourceManager.GetString("InstructorDoesntExist", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Instructor is already exist in the system.
        /// </summary>
        public static string InstructorExists {
            get {
                return ResourceManager.GetString("InstructorExists", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Files.
        /// </summary>
        public static string InstructorFiles {
            get {
                return ResourceManager.GetString("InstructorFiles", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to {0} Personal File.
        /// </summary>
        public static string InstructorFolder {
            get {
                return ResourceManager.GetString("InstructorFolder", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Instructor&apos;s Groups.
        /// </summary>
        public static string InstructorGroups {
            get {
                return ResourceManager.GetString("InstructorGroups", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to ID.
        /// </summary>
        public static string InstructorID {
            get {
                return ResourceManager.GetString("InstructorID", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Instructor ID Is not valid.
        /// </summary>
        public static string InstructorIDIsNotValid {
            get {
                return ResourceManager.GetString("InstructorIDIsNotValid", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Instructor ID can&apos;t be emply.
        /// </summary>
        public static string InstructorIDIsNull {
            get {
                return ResourceManager.GetString("InstructorIDIsNull", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The instructor is already registered to the training and it can be registered twice.
        /// </summary>
        public static string InstructorIsRegisteredToTraining {
            get {
                return ResourceManager.GetString("InstructorIsRegisteredToTraining", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The instructor is not valid.
        /// </summary>
        public static string InstructorNotValidAlert {
            get {
                return ResourceManager.GetString("InstructorNotValidAlert", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Instructor Profile Picture.
        /// </summary>
        public static string InstructorProfilePicture {
            get {
                return ResourceManager.GetString("InstructorProfilePicture", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Instructor Type.
        /// </summary>
        public static string InstructorRole {
            get {
                return ResourceManager.GetString("InstructorRole", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Instructor Roles Management.
        /// </summary>
        public static string InstructorRolesManagement {
            get {
                return ResourceManager.GetString("InstructorRolesManagement", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Instructors Management.
        /// </summary>
        public static string InstructorsManagement {
            get {
                return ResourceManager.GetString("InstructorsManagement", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Instructors Report.
        /// </summary>
        public static string InstructorsReport {
            get {
                return ResourceManager.GetString("InstructorsReport", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Instructor Trainings.
        /// </summary>
        public static string InstructorTrainings {
            get {
                return ResourceManager.GetString("InstructorTrainings", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Instructor Validity.
        /// </summary>
        public static string InstructorValidity {
            get {
                return ResourceManager.GetString("InstructorValidity", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Instructor Validity Status is until {0}.
        /// </summary>
        public static string InstructorValidityStatus {
            get {
                return ResourceManager.GetString("InstructorValidityStatus", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Completed.
        /// </summary>
        public static string IsCompleted {
            get {
                return ResourceManager.GetString("IsCompleted", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Expedition Manager.
        /// </summary>
        public static string IsManager {
            get {
                return ResourceManager.GetString("IsManager", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Last Name English.
        /// </summary>
        public static string LastNameEN {
            get {
                return ResourceManager.GetString("LastNameEN", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Last Name Hebrew.
        /// </summary>
        public static string LastNameHE {
            get {
                return ResourceManager.GetString("LastNameHE", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Last Name in Hebrew was not provided.
        /// </summary>
        public static string LastNameHeNull {
            get {
                return ResourceManager.GetString("LastNameHeNull", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to LastName in English was not provided.
        /// </summary>
        public static string LastNameNameEnNull {
            get {
                return ResourceManager.GetString("LastNameNameEnNull", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to My File.
        /// </summary>
        public static string MyFile {
            get {
                return ResourceManager.GetString("MyFile", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to New Horizon.
        /// </summary>
        public static string NewHorizon {
            get {
                return ResourceManager.GetString("NewHorizon", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to No Records.
        /// </summary>
        public static string NoRecords {
            get {
                return ResourceManager.GetString("NoRecords", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Not Relevant.
        /// </summary>
        public static string NotRelevant {
            get {
                return ResourceManager.GetString("NotRelevant", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Not Valid.
        /// </summary>
        public static string NotValid {
            get {
                return ResourceManager.GetString("NotValid", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Operation Result.
        /// </summary>
        public static string OperationLog {
            get {
                return ResourceManager.GetString("OperationLog", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Operation Result Reason.
        /// </summary>
        public static string OperationReason {
            get {
                return ResourceManager.GetString("OperationReason", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Organizational Status.
        /// </summary>
        public static string OrganizationalStatus {
            get {
                return ResourceManager.GetString("OrganizationalStatus", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Other.
        /// </summary>
        public static string Other {
            get {
                return ResourceManager.GetString("Other", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Oz.
        /// </summary>
        public static string Oz {
            get {
                return ResourceManager.GetString("Oz", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Passport Number.
        /// </summary>
        public static string PassportID {
            get {
                return ResourceManager.GetString("PassportID", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Password ID was not provided.
        /// </summary>
        public static string PassportIDIsNull {
            get {
                return ResourceManager.GetString("PassportIDIsNull", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Personal Details.
        /// </summary>
        public static string PersonalDetails {
            get {
                return ResourceManager.GetString("PersonalDetails", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Region.
        /// </summary>
        public static string Region {
            get {
                return ResourceManager.GetString("Region", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Register Instructor.
        /// </summary>
        public static string RegisterInstructor {
            get {
                return ResourceManager.GetString("RegisterInstructor", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Select to allow register the instructor.
        /// </summary>
        public static string RegisterOverideAction {
            get {
                return ResourceManager.GetString("RegisterOverideAction", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Renewal Training.
        /// </summary>
        public static string RenewalTraining {
            get {
                return ResourceManager.GetString("RenewalTraining", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Report Completion.
        /// </summary>
        public static string ReportCompletion {
            get {
                return ResourceManager.GetString("ReportCompletion", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Required Hours.
        /// </summary>
        public static string RequiredHours {
            get {
                return ResourceManager.GetString("RequiredHours", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Role Name.
        /// </summary>
        public static string RoleName {
            get {
                return ResourceManager.GetString("RoleName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Search Instructor.
        /// </summary>
        public static string SearchInstructor {
            get {
                return ResourceManager.GetString("SearchInstructor", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Show Completed Training.
        /// </summary>
        public static string ShowCompletedTraining {
            get {
                return ResourceManager.GetString("ShowCompletedTraining", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Starting renewal.
        /// </summary>
        public static string StartingRenewal {
            get {
                return ResourceManager.GetString("StartingRenewal", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Starting validity.
        /// </summary>
        public static string StartingValidity {
            get {
                return ResourceManager.GetString("StartingValidity", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Status for next renewal.
        /// </summary>
        public static string StatusForNextRenewal {
            get {
                return ResourceManager.GetString("StatusForNextRenewal", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Training Category.
        /// </summary>
        public static string TrainingCategory {
            get {
                return ResourceManager.GetString("TrainingCategory", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Counts for Renewal.
        /// </summary>
        public static string TrainingCounts {
            get {
                return ResourceManager.GetString("TrainingCounts", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Training Date.
        /// </summary>
        public static string TrainingDate {
            get {
                return ResourceManager.GetString("TrainingDate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to You can&apos;t delete this trainign because it has instructors assigned with records.
        /// </summary>
        public static string TrainingDeletionLimitAlert {
            get {
                return ResourceManager.GetString("TrainingDeletionLimitAlert", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Training Folder.
        /// </summary>
        public static string TrainingFolder {
            get {
                return ResourceManager.GetString("TrainingFolder", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Training Hours.
        /// </summary>
        public static string TrainingHours {
            get {
                return ResourceManager.GetString("TrainingHours", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Training Location.
        /// </summary>
        public static string TrainingLocation {
            get {
                return ResourceManager.GetString("TrainingLocation", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Training Name.
        /// </summary>
        public static string TrainingName {
            get {
                return ResourceManager.GetString("TrainingName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Trainings Management.
        /// </summary>
        public static string TrainingsManagement {
            get {
                return ResourceManager.GetString("TrainingsManagement", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Training Type.
        /// </summary>
        public static string TrainingType {
            get {
                return ResourceManager.GetString("TrainingType", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Unregister Instructor.
        /// </summary>
        public static string UnregisterInstructor {
            get {
                return ResourceManager.GetString("UnregisterInstructor", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Update Certificate.
        /// </summary>
        public static string UpdateCertificate {
            get {
                return ResourceManager.GetString("UpdateCertificate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Update Instructor Details.
        /// </summary>
        public static string UpdateInstructor {
            get {
                return ResourceManager.GetString("UpdateInstructor", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Update Qualification Expiration.
        /// </summary>
        public static string UpdateInstructorExpiration {
            get {
                return ResourceManager.GetString("UpdateInstructorExpiration", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Update Instructor Role.
        /// </summary>
        public static string UpdateInstructorRole {
            get {
                return ResourceManager.GetString("UpdateInstructorRole", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Update profile picture.
        /// </summary>
        public static string UpdateProfilePicture {
            get {
                return ResourceManager.GetString("UpdateProfilePicture", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Update Training.
        /// </summary>
        public static string UpdateTraining {
            get {
                return ResourceManager.GetString("UpdateTraining", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Updating Certification.
        /// </summary>
        public static string UpdatingCertification {
            get {
                return ResourceManager.GetString("UpdatingCertification", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Valid.
        /// </summary>
        public static string Valid {
            get {
                return ResourceManager.GetString("Valid", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Validation Type.
        /// </summary>
        public static string ValidationType {
            get {
                return ResourceManager.GetString("ValidationType", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Validity End Date.
        /// </summary>
        public static string ValidityEndTime {
            get {
                return ResourceManager.GetString("ValidityEndTime", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Validity Start Date.
        /// </summary>
        public static string ValidityStartDate {
            get {
                return ResourceManager.GetString("ValidityStartDate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Validity Status.
        /// </summary>
        public static string ValidityStatus {
            get {
                return ResourceManager.GetString("ValidityStatus", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Zip.
        /// </summary>
        public static string ZipCode {
            get {
                return ResourceManager.GetString("ZipCode", resourceCulture);
            }
        }
    }
}
