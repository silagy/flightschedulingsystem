﻿using HtmlAgilityPack;
using NLog;
using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace Endilo.Emails
{
    public static class SendMail
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        public static void SendToMany(IEnumerable<string> mailTo, string caption, string message)
        {
            foreach (var email in mailTo)
            {
                SendSingleMail(email, caption, message);
            }

        }

        public static void SendMultipleMails(List<MailAddress> addresses, string subject, string message, string basePath = null)
        {

            try
            {
                //in global web.config keys enter gmail and password.
                var smtpServer = ConfigurationManager.AppSettings["SmtpServer"];
                var from = ConfigurationManager.AppSettings["Email"];
                var password = ConfigurationManager.AppSettings["Password"];
                var port = ConfigurationManager.AppSettings["smtpPort"];


                MailMessage mail = new MailMessage();
                mail.From = new MailAddress(from);
                foreach (var mailAddress in addresses)
                {
                    mail.To.Add(mailAddress);
                }
                mail.Subject = subject;
                mail.IsBodyHtml = true;

                HtmlDocument doc = new HtmlDocument();
                doc.LoadHtml(message);
                var imgs = doc.DocumentNode.SelectNodes("//img");

                if (imgs != null)
                {

                    List<LinkedResource> resources = new List<LinkedResource>();
                    foreach (var img in imgs)
                    {
                        string orig = img.Attributes["src"].Value;

                        //string file = orig.Substring(orig.LastIndexOf('/'), orig.Length);
                        string file = orig.Substring(orig.LastIndexOf('/') + 1);
                        basePath += String.Format("\\");
                        string filePath = Path.Combine(basePath, file);
                        //do replacements on orig to a new string, newsrc
                        var inlineLogo = new LinkedResource(filePath);

                        inlineLogo.ContentId = Guid.NewGuid().ToString();

                        resources.Add(inlineLogo);

                        img.SetAttributeValue("src", String.Format("cid:{0}", inlineLogo.ContentId));
                    }

                    var view = AlternateView.CreateAlternateViewFromString(doc.DocumentNode.OuterHtml, null, "text/html");
                    foreach (var linkedResource in resources)
                    {
                        view.LinkedResources.Add(linkedResource);
                    }
                    mail.AlternateViews.Add(view);
                }

                mail.Body = ImplementRTLContent(doc.DocumentNode.OuterHtml);
                SmtpClient client = new SmtpClient();
                client.Host = smtpServer;
                client.Port = 587;
                client.EnableSsl = true;
                client.Credentials = new NetworkCredential(from, password);
                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                client.EnableSsl = true;
                client.Send(mail);
                mail.Dispose();
            }
            catch (Exception e)
            {
                throw new Exception("Mail.Send: " + e.Message);
            }
        }

        public static bool SendSimpleEmail(string mailto, string caption, string message)
        {
            try
            {
                //in global web.config keys enter gmail and password.
                var smtpServer = ConfigurationManager.AppSettings["SmtpServer"];
                var from = ConfigurationManager.AppSettings["Email"];
                var EmailCredential = ConfigurationManager.AppSettings["EmailCredential"];
                var password = ConfigurationManager.AppSettings["EmailPassword"];
                var port = ConfigurationManager.AppSettings["smtpPort"];
                bool ssl = Convert.ToBoolean(ConfigurationManager.AppSettings["SmtpSSL"]);


                MailMessage mail = new MailMessage();
                mail.From = new MailAddress(from);
                mail.To.Add(new MailAddress(mailto));
                mail.To.Add(new MailAddress(from));
                mail.Subject = caption;
                mail.Body = message;

                mail.IsBodyHtml = true;

                SmtpClient client = new SmtpClient();
                client.Host = smtpServer;
                client.Port = Convert.ToInt32(port);
                client.UseDefaultCredentials = false;
                client.Credentials = new NetworkCredential(EmailCredential, password);
                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                client.EnableSsl = ssl;
                client.Send(mail);
                mail.Dispose();
                return true;
            }
            catch (Exception ex)
            {
                EventLog elog = new EventLog();
                elog.Source = "Polin Application";
                //elog.WriteEntry(e.Message);
                return false;
            }
        }

        public static bool SendSingleMail(string mailto, string caption, string message, string attachFile = null, string basePath = null, bool onlineImages = true)
        {
            try
            {
                //in global web.config keys enter gmail and password.
                var smtpServer = ConfigurationManager.AppSettings["SmtpServer"];
                var from = ConfigurationManager.AppSettings["Email"];
                var password = ConfigurationManager.AppSettings["EmailPassword"];
                var port = ConfigurationManager.AppSettings["smtpPort"];
                bool ssl = Convert.ToBoolean(ConfigurationManager.AppSettings["SmtpSSL"]);
                var username = ConfigurationManager.AppSettings["Username"];

                Logger.Info($"SmtpServer {smtpServer}, From: {from}, Password: {password}, Port: {port}, SSL: {ssl}");

                MailMessage mail = new MailMessage();
                mail.From = new MailAddress(from);
                mail.To.Add(new MailAddress(mailto));
                mail.To.Add(new MailAddress(from));
                mail.Subject = caption;

                mail.IsBodyHtml = true;
                HtmlDocument doc = new HtmlDocument();
                doc.LoadHtml(message);
                var imgs = doc.DocumentNode.SelectNodes("//img");
                if (!onlineImages)
                {
                    if (imgs != null)
                    {

                        List<LinkedResource> resources = new List<LinkedResource>();
                        foreach (var img in imgs)
                        {
                            string orig = img.Attributes["src"].Value;

                            //string file = orig.Substring(orig.LastIndexOf('/'), orig.Length);
                            string file = orig.Substring(orig.LastIndexOf('/') + 1);
                            basePath += String.Format("\\");
                            string filePath = Path.Combine(basePath, file);
                            //do replacements on orig to a new string, newsrc
                            var inlineLogo = new LinkedResource(filePath);

                            inlineLogo.ContentId = Guid.NewGuid().ToString();

                            resources.Add(inlineLogo);

                            img.SetAttributeValue("src", String.Format("cid:{0}", inlineLogo.ContentId));
                        }

                        var view = AlternateView.CreateAlternateViewFromString(doc.DocumentNode.OuterHtml, null, "text/html");
                        foreach (var linkedResource in resources)
                        {
                            view.LinkedResources.Add(linkedResource);
                        }
                        mail.AlternateViews.Add(view);
                    }
                }

                mail.Body = ImplementRTLContent(doc.DocumentNode.OuterHtml);
                if (!string.IsNullOrEmpty(attachFile))
                    mail.Attachments.Add(new Attachment(attachFile));
                SmtpClient client = new SmtpClient();
                client.Host = smtpServer;
                client.Port = Convert.ToInt32(port);
                client.UseDefaultCredentials = false;
                client.Credentials = new NetworkCredential(username, password);
                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                client.EnableSsl = ssl;
                client.Send(mail);
                mail.Dispose();
                return true;
            }
            catch (Exception e)
            {
                Logger.Error(e, "Error accurd in sending email from function send Simple Email");
                return false;
            }
        }

        private static string ImplementRTLContent(string msg)
        {
            return String.Format(@"<div style='direction:rtl'>{0}</div>", msg);
        }
    }
}
