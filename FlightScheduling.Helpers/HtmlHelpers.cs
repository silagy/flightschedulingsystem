﻿using System;
using System.ComponentModel;
using System.Reflection;
using System.Web;
using System.Linq;
using System.Collections.Generic;
using System.Data;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;
using System.Web.Mvc.Html;
using System.Web.Routing;
using System.Linq.Expressions;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.Web.Security;
using System.Web.UI.WebControls;
using Flight.DAL.FlightDB.Methods;
using FlightScheduling.Language;
using MvcContrib.UI.Grid.Syntax;
using FlightScheduling.DAL;
using HtmlAgilityPack;
using FlightScheduling.Helpers;
using System.Web.Helpers;

namespace Endilo.Helpers
{

    public static class Helpers
    {
        public static DateTime StartOfWeek(this DateTime dt, DayOfWeek startOfWeek)
        {
            int diff = (7 + (dt.DayOfWeek - startOfWeek)) % 7;
            return dt.AddDays(-1 * diff).Date;
        }

        public static int GetWeekNumberOfMonth(DateTime date)
        {
            date = date.Date;
            DateTime firstMonthDay = new DateTime(date.Year, date.Month, 1);
            DateTime firstMonthMonday = firstMonthDay.AddDays((DayOfWeek.Monday + 7 - firstMonthDay.DayOfWeek) % 7);
            if (firstMonthMonday > date)
            {
                firstMonthDay = firstMonthDay.AddMonths(-1);
                firstMonthMonday = firstMonthDay.AddDays((DayOfWeek.Monday + 7 - firstMonthDay.DayOfWeek) % 7);
            }
            return (date - firstMonthMonday).Days / 7 + 1;
        }

        public static int GetDuration(string hoursandMinutes)
        {
            int duration = 0;

            if (hoursandMinutes != null)
            {
                if (hoursandMinutes.Contains(':'))
                {
                    string[] durationArr = hoursandMinutes.Split(':');
                    if (durationArr[0] != null)
                    {
                        duration = Convert.ToInt32(durationArr[0]) * 60;
                    }

                    if (durationArr[1] != null)
                    {
                        duration += Convert.ToInt32(durationArr[1]);
                    }

                    return duration;
                }
            }
            return duration;
        }


        public static bool CheckIsraeliID(string value)
        {
            if (value != null)
            {
                string IDNum = (string)value;

                if (!System.Text.RegularExpressions.Regex.IsMatch(IDNum, @"^\d{5,9}$"))
                    return false;

                // The number is too short - add leading 0000
                if (IDNum.Length < 9)
                {
                    while (IDNum.Length < 9)
                    {
                        IDNum = '0' + IDNum;
                    }
                }

                // CHECK THE ID NUMBER
                int mone = 0;
                int incNum;
                for (int i = 0; i < 9; i++)
                {
                    incNum = Convert.ToInt32(IDNum[i].ToString());
                    incNum *= (i % 2) + 1;
                    if (incNum > 9)
                        incNum -= 9;
                    mone += incNum;
                }
                if (mone % 10 == 0)
                    return true;
                else
                    return false;
            }

            return false;
        }

        public static IEnumerable<T> GetValues<T>()
        {
            return Enum.GetValues(typeof(T)).Cast<T>();
        }

        public static T ToEnum<T>(this string value)
        {
            return (T)Enum.Parse(typeof(T), value, true);
        }

        public static DateTime RoundUp(DateTime dt, TimeSpan d)
        {
            return new DateTime((dt.Ticks + d.Ticks - 1) / d.Ticks * d.Ticks, dt.Kind);
        }

        public static DateTime RoundDown(DateTime dt, TimeSpan d)
        {
            return new DateTime((dt.Ticks / d.Ticks) * d.Ticks);
        }

        public static T setIfNullOrDefault<T>(T i_ObjectToUpdate, T i_DefaultValue)
        {
            if (EqualityComparer<T>.Default.Equals(i_ObjectToUpdate, default(T)))
            {
                return i_DefaultValue;
            }

            return i_ObjectToUpdate;
        }

        public static MvcHtmlString RawActionLink(this AjaxHelper ajaxHelper, AjaxActionDetails i_AjaxActionDetails)
        {
            var repID = Guid.NewGuid().ToString();
            var lnk = ajaxHelper.ActionLink(
                repID,
                i_AjaxActionDetails.ActionName,
                i_AjaxActionDetails.ControllerName,
                i_AjaxActionDetails.RouteValues,
                i_AjaxActionDetails.AjaxOptions,
                i_AjaxActionDetails.HtmlAttributes);
            return MvcHtmlString.Create(lnk.ToString().Replace(repID, i_AjaxActionDetails.LinkText));
        }

        public static RouteValueDictionary Disable(this object htmlAttributes, bool disabled)
        {
            var attributes = HtmlHelper.AnonymousObjectToHtmlAttributes(htmlAttributes);
            if (disabled)
            {
                attributes["readonly"] = "readonly";
            }
            return attributes;
        }

        public static RouteValueDictionary AddTooltip(this object htmlAttributes, string title, TooltipPlancement placement = TooltipPlancement.Top)
        {
            var attributes = HtmlHelper.AnonymousObjectToHtmlAttributes(htmlAttributes);
            attributes["data-toggle"] = "tooltip";
            attributes["data-placement"] = placement.ToString();
            attributes["title"] = title;

            return attributes;
        }

        public static MvcHtmlString ActionLink(this AjaxHelper ajaxHelper, AjaxActionDetails i_AjaxActionDetails)
        {

            return ajaxHelper.ActionLink(
                i_AjaxActionDetails.LinkText,
                i_AjaxActionDetails.ActionName,
                i_AjaxActionDetails.ControllerName,
                i_AjaxActionDetails.RouteValues,
                i_AjaxActionDetails.AjaxOptions,
                i_AjaxActionDetails.HtmlAttributes
                );
        }

        public static MvcHtmlString Mailto(this HtmlHelper htmlHelper, string email)
        {
            return MvcHtmlString.Create($"<a href='mailto:{email}'>{email}");
        }

        public static MvcHtmlString RawActionLink(this HtmlHelper htmlHelper, string rawLabel, string actionName, string controllerName, RouteValueDictionary RouteValues, IDictionary<string, object> HtmlAttributes)
        {
            var repID = Guid.NewGuid().ToString();
            var lnk = htmlHelper.ActionLink(repID, actionName, controllerName, RouteValues, HtmlAttributes);
            return MvcHtmlString.Create(lnk.ToString().Replace(repID, rawLabel));
        }

        public static MvcHtmlString RawActionLink(this HtmlHelper htmlHelper, string rawLabel, string actionName, string controllerName, object RouteValues, object HtmlAttributes)
        {
            RouteValueDictionary _routeValues = new RouteValueDictionary(RouteValues);
            IDictionary<string, object> _htmlAttributes = HtmlHelper.AnonymousObjectToHtmlAttributes(HtmlAttributes);
            return RawActionLink(htmlHelper, rawLabel, actionName, controllerName, _routeValues, _htmlAttributes);
        }

        public static MvcHtmlString RawActionLink(this HtmlHelper htmlHelper, string rawLabel, string actionName, string controllerName, object RouteValues, RouteValueDictionary HtmlAttributes)
        {
            RouteValueDictionary _routeValues = new RouteValueDictionary(RouteValues);
            IDictionary<string, object> _htmlAttributes = HtmlHelper.AnonymousObjectToHtmlAttributes(HtmlAttributes);
            return RawActionLink(htmlHelper, rawLabel, actionName, controllerName, _routeValues, _htmlAttributes);
        }


        public static MvcHtmlString RawActionLink(this HtmlHelper htmlHelper, HtmlActionDetails i_HtmlActionDetails)
        {
            var repID = Guid.NewGuid().ToString();
            var lnk = htmlHelper.ActionLink(
                repID,
                i_HtmlActionDetails.ActionName,
                i_HtmlActionDetails.ControllerName,
                i_HtmlActionDetails.Protocol,
                i_HtmlActionDetails.HostName,
                i_HtmlActionDetails.Fragment,
                i_HtmlActionDetails.RouteValues,
                i_HtmlActionDetails.HtmlAttributes);
            return MvcHtmlString.Create(lnk.ToString().Replace(repID, i_HtmlActionDetails.LinkText));
        }

        public static MvcHtmlString AddInstitudeRetangle(this HtmlHelper htmlHelper, string color, string affiliateName)
        {
            string colorToAssing = color.StartsWith("#") ? color : "#" + color;

            return
                MvcHtmlString.Create(
                    (String.Format("<div style='background-color: {0}; width:20px; height: 20px; display:inline-block;' title='{1}'></div>", colorToAssing, affiliateName)));
        }

        public static MvcHtmlString GetLable(this HtmlHelper htmlHelper, bool state, string displayText)
        {
            string stateClass = "";

            stateClass = state == true ? "label label-success" : "label label-danger";

            return
                MvcHtmlString.Create(
                    (String.Format("<span class='{0}'>{1}</span>", stateClass, displayText)));
        }

        public static MvcHtmlString GetProgressBar(this HtmlHelper htmlHelper, int percentage, int aqcuiredHours,
            int requiredHours, bool showLongText, bool showNumbers = false, bool showGreenCompletion = false)
        {
            string progressBarColor = "";
            if (percentage > 0 && percentage < 100)
            {
                progressBarColor = "progress-bar-warning";
            }
            else if (percentage == 100)
            {
                if (showGreenCompletion)
                {
                    progressBarColor = "progress-bar-success";
                }
                else
                {
                    progressBarColor = "progress-bar-warning";

                }
            }

            string str = "<div class=\"progress\">";
            str +=
                String.Format(
                    @"<div class='progress-bar {0}' role='progressbar' aria-valuenow='{1}' aria-valuemin='0' aria-valuemax='{1}' style='width:{2}%;min-width:2em;>", progressBarColor, requiredHours, percentage);
            string classCss = percentage < 50 ? "black" : "white";
            if (showLongText)
            {
                str += $@"<span class='{classCss}'>{percentage}% {InstructorsStrings.HoursCompleted} - ";
                str += String.Format(InstructorsStrings.HoursCompletedOutOfRequired, aqcuiredHours, requiredHours);
            }
            else
            {
                if (showNumbers)
                {
                    str += $@"<span class='{classCss}'>{aqcuiredHours}/{requiredHours}";
                }
                else
                {
                    str += $@"<span class='{classCss}'>{percentage}%";
                }
            }

            str += @"</span>
                </div>
            </div>";

            return MvcHtmlString.Create(str);

        }



        public static string GetFormStatusColorClass(this HtmlHelper htmlHelper, eGroupFormStatus formStatus)
        {
            string str = "";

            switch (formStatus)
            {
                case eGroupFormStatus.New:
                    {
                        str = "blue";
                        break;
                    }
                case eGroupFormStatus.InProgress:
                    {
                        str = "blue";
                        break;
                    }
                case eGroupFormStatus.WaitingApproval:
                    {
                        str = "orange";
                        break;
                    }
                case eGroupFormStatus.Rejected:
                    {
                        str = "red";
                        break;
                    }
                case eGroupFormStatus.Approved:
                    {
                        str = "green";
                        break;
                    }
            }

            return str;
        }

        public static string GetTraficLightClassColor(this HtmlHelper htmlHelper, eGroupFormStatus formStatus)
        {
            string str = "";

            switch (formStatus)
            {
                case eGroupFormStatus.New:
                    {
                        str = "gray";
                        break;
                    }
                case eGroupFormStatus.InProgress:
                    {
                        str = "blue";
                        break;
                    }
                case eGroupFormStatus.WaitingApproval:
                    {
                        str = "orange";
                        break;
                    }
                case eGroupFormStatus.Rejected:
                    {
                        str = "red";
                        break;
                    }
                case eGroupFormStatus.Approved:
                    {
                        str = "green";
                        break;
                    }
            }

            return str;
        }

        public static string GetTraficLightClassColorForm60(this HtmlHelper htmlHelper, eGroupForm60Status formStatus)
        {
            string str = "";

            switch (formStatus)
            {
                case eGroupForm60Status.New:
                    {
                        str = "gray";
                        break;
                    }
                case eGroupForm60Status.InApprovalProcess:
                case eGroupForm60Status.WaitingApprovalAgency:
                case eGroupForm60Status.WaitingPlanApproval:
                case eGroupForm60Status.WaitingFinalApproval:
                    {
                        str = "orange";
                        break;
                    }
                case eGroupForm60Status.Rejected:
                    {
                        str = "red";
                        break;
                    }
                case eGroupForm60Status.Approved:
                    {
                        str = "green";
                        break;
                    }
            }

            return str;
        }

        public static MvcHtmlString GetFormStatusLable(this HtmlHelper htmlHelper, eGroupFormStatus formStatus)
        {
            string str = "";
            str += "<span class='label " + GetFormStatusClass(htmlHelper, formStatus) + "'>";
            str += formStatus.GetDescription();
            str += "</span>";
            return MvcHtmlString.Create(str);
        }

        public static MvcHtmlString GetFormStatusLable(this HtmlHelper htmlHelper, eGroupForm60Status formStatus)
        {
            string str = "";
            str += "<span class='label " + GetFormStatusClass(htmlHelper, formStatus) + "'>";
            str += formStatus.GetDescription();
            str += "</span>";
            return MvcHtmlString.Create(str);
        }

        public static MvcHtmlString GetIcon(this HtmlHelper htmkHelper, Icons icon)
        {
            string str = "";
            string iconpath = "";
            string title = "";
            switch (icon)
            {
                case Icons.Add:
                    {
                        iconpath = "glyphicon-plus";
                        title = CommonStrings.Add;
                        break;
                    }
                case Icons.Create:
                    {
                        iconpath = "glyphicon-plus";
                        title = CommonStrings.Create;
                        break;
                    }
                case Icons.Edit:
                    {
                        iconpath = "glyphicon-pencil";
                        title = CommonStrings.Edit;
                        break;
                    }
                case Icons.Delete:
                    {
                        iconpath = "glyphicon-trash";
                        title = CommonStrings.Delete;
                        break;
                    }
                case Icons.Ok:
                    {
                        iconpath = "glyphicon-ok";
                        title = CommonStrings.Delete;
                        break;
                    }
                case Icons.Remove:
                    {
                        iconpath = "glyphicon-remove";
                        title = CommonStrings.Delete;
                        break;
                    }
                case Icons.Yes:
                    {
                        iconpath = "glyphicon-ok green";
                        title = CommonStrings.Yes;
                        break;
                    }
                case Icons.YesBlack:
                    {
                        iconpath = "glyphicon-ok";
                        title = CommonStrings.Yes;
                        break;
                    }
                case Icons.No:
                    {
                        iconpath = "glyphicon-remove light-gray";
                        title = CommonStrings.No;
                        break;
                    }
                case Icons.Folder:
                    {
                        iconpath = "glyphicon-folder-open";
                        title = GroupsStrings.GroupFolder;
                        break;
                    }
                case Icons.Excel:
                    {
                        iconpath = "glyphicon-export";
                        title = CommonStrings.DownloadFile;
                        break;
                    }
                case Icons.Move:
                    {
                        iconpath = "glyphicon-share-alt";
                        title = CommonStrings.Move;
                        break;
                    }
                case Icons.Restore:
                    {
                        iconpath = "glyphicon-refresh";
                        title = CommonStrings.Restore;
                        break;
                    }
                case Icons.Print:
                    {
                        iconpath = "glyphicon-print";
                        title = CommonStrings.Print;
                        break;
                    }
            }

            str = String.Format("<span class='glyphicon {0}' title='{1}'></span>", iconpath, title);

            return MvcHtmlString.Create(str);
        }

        public static MvcHtmlString GetIcon(this HtmlHelper htmkHelper, Icons icon, string customText = "")
        {
            string str = "";
            string iconpath = "";
            string title = "";
            switch (icon)
            {
                case Icons.Add:
                    {
                        iconpath = "glyphicon-plus";
                        title = CommonStrings.Add;
                        break;
                    }
                case Icons.Create:
                    {
                        iconpath = "glyphicon-plus";
                        title = CommonStrings.Create;
                        break;
                    }
                case Icons.Edit:
                    {
                        iconpath = "glyphicon-pencil";
                        title = CommonStrings.Edit;
                        break;
                    }
                case Icons.Delete:
                    {
                        iconpath = "glyphicon-trash";
                        title = CommonStrings.Delete;
                        break;
                    }
                case Icons.Ok:
                    {
                        iconpath = "glyphicon-ok";
                        title = CommonStrings.Delete;
                        break;
                    }
                case Icons.Remove:
                    {
                        iconpath = "glyphicon-remove";
                        title = CommonStrings.Delete;
                        break;
                    }
                case Icons.Yes:
                    {
                        iconpath = "glyphicon-ok green";
                        title = CommonStrings.Yes;
                        break;
                    }
                case Icons.YesBlack:
                    {
                        iconpath = "glyphicon-ok";
                        title = CommonStrings.Yes;
                        break;
                    }
                case Icons.No:
                    {
                        iconpath = "glyphicon-remove light-gray";
                        title = CommonStrings.No;
                        break;
                    }
                case Icons.Folder:
                    {
                        iconpath = "glyphicon-folder-open";
                        title = GroupsStrings.GroupFolder;
                        break;
                    }
                case Icons.Excel:
                    {
                        iconpath = "glyphicon-export";
                        title = CommonStrings.DownloadFile;
                        break;
                    }
                case Icons.Move:
                    {
                        iconpath = "glyphicon-share-alt";
                        title = CommonStrings.Move;
                        break;
                    }
                case Icons.Restore:
                    {
                        iconpath = "glyphicon-refresh";
                        title = CommonStrings.Restore;
                        break;
                    }
                case Icons.Print:
                    {
                        iconpath = "glyphicon-print";
                        title = CommonStrings.Print;
                        break;
                    }
                case Icons.Plane:
                    {
                        iconpath = "glyphicon-plane";
                        title = CommonStrings.Print;
                        break;
                    }
            }

            if (customText != null || customText == "")
            {
                title = customText;
            }

            str = String.Format("<span class='glyphicon {0}' title='{1}'></span>", iconpath, title);

            return MvcHtmlString.Create(str);
        }

        public static MvcHtmlString GetIcon(this HtmlHelper htmkHelper, Icons icon, String customText = null, IconColor? color = null)
        {
            string str = "";
            string iconpath = "";
            string title = "";
            switch (icon)
            {
                case Icons.Add:
                    {
                        iconpath = "glyphicon-plus";
                        title = CommonStrings.Add;
                        break;
                    }
                case Icons.Create:
                    {
                        iconpath = "glyphicon-plus";
                        title = CommonStrings.Create;
                        break;
                    }
                case Icons.Edit:
                    {
                        iconpath = "glyphicon-pencil";
                        title = CommonStrings.Edit;
                        break;
                    }
                case Icons.Delete:
                    {
                        iconpath = "glyphicon-trash";
                        title = CommonStrings.Delete;
                        break;
                    }
                case Icons.Ok:
                    {
                        iconpath = "glyphicon-ok";
                        title = CommonStrings.Delete;
                        break;
                    }
                case Icons.Remove:
                    {
                        iconpath = "glyphicon-remove";
                        title = CommonStrings.Delete;
                        break;
                    }
                case Icons.Yes:
                    {
                        iconpath = "glyphicon-ok green";
                        title = CommonStrings.Yes;
                        break;
                    }
                case Icons.YesBlack:
                    {
                        iconpath = "glyphicon-ok";
                        title = CommonStrings.Yes;
                        break;
                    }
                case Icons.No:
                    {
                        iconpath = "glyphicon-remove light-gray";
                        title = CommonStrings.No;
                        break;
                    }
                case Icons.Folder:
                    {
                        iconpath = "glyphicon-folder-open";
                        title = GroupsStrings.GroupFolder;
                        break;
                    }
                case Icons.Excel:
                    {
                        iconpath = "glyphicon-export";
                        title = CommonStrings.DownloadFile;
                        break;
                    }
                case Icons.Move:
                    {
                        iconpath = "glyphicon-share-alt";
                        title = CommonStrings.Move;
                        break;
                    }
                case Icons.Restore:
                    {
                        iconpath = "glyphicon-refresh";
                        title = CommonStrings.Restore;
                        break;
                    }
                case Icons.Print:
                    {
                        iconpath = "glyphicon-print";
                        title = CommonStrings.Print;
                        break;
                    }
            }

            if (customText != null || customText == "")
            {
                title = customText;
            }

            string colorCss = "";

            switch (color)
            {
                case IconColor.Red:
                    {
                        colorCss = "icon-red";
                        break;
                    }
                case IconColor.Green:
                    {
                        colorCss = "icon-green";
                        break;
                    }
                default:
                    {
                        colorCss = String.Empty;
                        break;
                    }
            }

            str = String.Format("<span class='glyphicon {0} {1}' title='{2}'></span>", iconpath, colorCss, title);

            return MvcHtmlString.Create(str);
        }

        public static MvcHtmlString GetLinkWithHtml(this HtmlHelper htmlHelper, string rawHtml, string action, string controller, object routeValues, object htmlAttributes)
        {
            //string anchor = htmlHelper.ActionLink("##holder##", action, controller, routeValues, htmlAttributes).ToString();
            //return MvcHtmlString.Create(anchor.Replace("##holder##", rawHtml));
            /* Updated code to use a generated guid as from the suggestion of Phillip Haydon */
            string holder = Guid.NewGuid().ToString();
            string anchor = htmlHelper.ActionLink(holder, action, controller, routeValues, htmlAttributes).ToString();
            return MvcHtmlString.Create(anchor.Replace(holder, rawHtml));
        }

        //public static MvcHtmlString GetOrderTypeIcon(this HtmlHelper htmlHelper, Order.eOrderType orderType, string label)
        //{
        //    string str = "";
        //    string imgpath = "";
        //    switch (orderType)
        //    {
        //        case Order.eOrderType.Document:
        //            {
        //                imgpath = "/Content/images/page_word.png";
        //                break;
        //            }
        //        case Order.eOrderType.DigitalPrint:
        //            {
        //                imgpath = "/Content/images/digital_signature.png";
        //                break;
        //            }
        //        case Order.eOrderType.WideFormat:
        //            {
        //                imgpath = "/Content/images/document_vertical.png";
        //                break;
        //            }
        //    }

        //    str = String.Format("<img src='{0}' alt='{1}' title='{2}' />", imgpath, label, label);

        //    return
        //        MvcHtmlString.Create(str);
        //}

        public static MvcHtmlString NavigationTab(this HtmlHelper helper, string i_Url, string i_Lable)
        {
            var Request = System.Web.HttpContext.Current.Request;
            var activeCSS = (string.Equals(Request.RawUrl, i_Url, StringComparison.InvariantCultureIgnoreCase)) ? "active" : "";

            return MvcHtmlString.Create(String.Format("<li class='{0}'><a href='{1}'>{2}</a></li>", activeCSS, i_Url, i_Lable));

        }

        public static MvcHtmlString NavigationTab(this HtmlHelper helper, string i_Url, string i_Lable, string mixpanelEvent = "")
        {
            var Request = System.Web.HttpContext.Current.Request;
            var activeCSS = (string.Equals(Request.RawUrl, i_Url, StringComparison.InvariantCultureIgnoreCase)) ? "active" : "";

            return MvcHtmlString.Create(String.Format("<li class='{0}'><a href='{1}' mp-navigation-event='{2}'>{3}</a></li>", activeCSS, i_Url, mixpanelEvent, i_Lable));

        }

        public static MvcHtmlString BootstrapLiNav(this HtmlHelper helper, string i_Url, string i_Lable)
        {
            var Request = System.Web.HttpContext.Current.Request;
            var activeCSS = (string.Equals(Request.RawUrl, i_Url)) ? "active" : "";

            return MvcHtmlString.Create(String.Format("<li class='{0}'><a href='{1}'>{2}</a></li>", activeCSS, i_Url, i_Lable));

        }

        public static MvcHtmlString ModalLoadingAnimation(this HtmlHelper helper)
        {
            var loadingHtmlStr = @"
				<div class='stretched-to-margin ModalLoading'>
					<span class='image-container'><img src='/Content/Images/AjaxLoader.gif' /></span>
				</div>";
            return MvcHtmlString.Create(loadingHtmlStr);
        }

        public static IHtmlString CheckboxListFor<TModel, TKey>(this HtmlHelper<TModel> helper, Expression<Func<TModel, IEnumerable<TKey>>> ex, Dictionary<TKey, string> i_PossibleOptions)
            where TKey : struct, IConvertible
        {
            return CheckboxListFor(helper, ex, i_PossibleOptions, null);
        }

        public static IHtmlString CheckboxListFor<TModel, TKey>(this HtmlHelper<TModel> helper, Expression<Func<TModel, IEnumerable<TKey>>> ex, Dictionary<TKey, string> i_PossibleOptions, object i_LabelHtmlAttributes)
            where TKey : struct, IConvertible
        {
            var metadata = ModelMetadata.FromLambdaExpression(ex, helper.ViewData);
            var selectedValues = (IEnumerable<TKey>)metadata.Model;
            var name = ExpressionHelper.GetExpressionText(ex);
            return helper.CheckboxList(name, selectedValues, i_PossibleOptions, i_LabelHtmlAttributes);
        }

        private static IHtmlString CheckboxList<TKey>(this HtmlHelper helper, string name, IEnumerable<TKey> i_SelectedValues, Dictionary<TKey, string> i_PossibleOptions, object i_LabelHtmlAttributes)
            where TKey : struct, IConvertible
        {
            if (!typeof(TKey).IsEnum) throw new ArgumentException("T must be an enumerated type");

            var result = new StringBuilder();

            foreach (var option in i_PossibleOptions)
            {
                var label = new TagBuilder("label");
                label.MergeAttributes(new RouteValueDictionary(i_LabelHtmlAttributes));
                label.Attributes["for"] = string.Format("{0}", option.Key.ToString());
                label.InnerHtml = option.Value;

                var checkbox = new TagBuilder("input");
                checkbox.Attributes["type"] = "checkbox";
                checkbox.Attributes["name"] = name;
                checkbox.Attributes["id"] = string.Format("{0}", option.Key.ToString());
                checkbox.Attributes["value"] = option.Key.ToString();

                bool isChecked = ((i_SelectedValues != null) && (i_SelectedValues.Contains(option.Key)));
                if (isChecked)
                {
                    checkbox.Attributes["checked"] = "checked";
                }

                result.Append(checkbox);
                result.Append(label);
            }
            return new HtmlString(result.ToString());
        }

        public static MvcHtmlString ClientIdFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression)
        {
            return MvcHtmlString.Create(htmlHelper.ViewContext.ViewData.TemplateInfo.GetFullHtmlFieldId(ExpressionHelper.GetExpressionText(expression)));
        }

        public static IEnumerable<SelectListItem> SelectListItemsFor<T>(T selected) where T : struct
        {
            Type t = typeof(T);
            if (t.IsEnum)
            {
                return Enum.GetValues(t).Cast<Enum>().Select(e => new SelectListItem { Value = e != null ? (Convert.ToInt32(e)).ToString() : null, Text = e.GetDescription() });
            }
            return null;
        }


        public static MvcHtmlString GetAgentStatusTag(this HtmlHelper htmlhleper, eAgentExpeditionStatus status)
        {
            string classCss = "";

            switch (status)
            {
                case eAgentExpeditionStatus.Assigned:
                    {
                        classCss = "label-warning";
                        break;
                    }
                case eAgentExpeditionStatus.NotAssigned:
                    {
                        classCss = "label-danger";
                        break;
                    }
                case eAgentExpeditionStatus.Approved:
                    {
                        classCss = "label-success";
                        break;
                    }
            }

            return
                MvcHtmlString.Create(String.Format("<span class='label {0}'>{1}</span>", classCss,
                    status.GetDescription()));
        }

        public static MvcHtmlString GetStatusTag(this HtmlHelper htmlhleper, eExpeditionStatus status)
        {
            string classCss = "";

            switch (status)
            {
                case eExpeditionStatus.PendingApproval:
                    {
                        classCss = "label-primary";
                        break;
                    }
                case eExpeditionStatus.InProcess:
                    {
                        classCss = "label-warning";
                        break;
                    }
                case eExpeditionStatus.Rejected:
                    {
                        classCss = "label-danger";
                        break;
                    }
                case eExpeditionStatus.Approved:
                    {
                        classCss = "label-success";
                        break;
                    }
            }

            return
                MvcHtmlString.Create(String.Format("<span class='label {0}'>{1}</span>", classCss,
                    status.GetDescription()));
        }

        public static MvcHtmlString GetForm60StatusTag(this HtmlHelper htmlhleper, eGroupForm60Status status)
        {
            string classCss = "";

            switch (status)
            {
                case eGroupForm60Status.New:
                    {
                        classCss = "label-info";
                        break;
                    }
                case eGroupForm60Status.InApprovalProcess:
                    {
                        classCss = "label-warning";
                        break;
                    }
                case eGroupForm60Status.WaitingApprovalAgency:
                    {
                        classCss = "lable-dark-blue";
                        break;
                    }
                case eGroupForm60Status.WaitingPlanApproval:
                    {
                        classCss = "lable-dark-pink";
                        break;
                    }
                case eGroupForm60Status.WaitingFinalApproval:
                    {
                        classCss = "label-primary";
                        break;
                    }
                case eGroupForm60Status.Rejected:
                    {
                        classCss = "label-danger";
                        break;
                    }
                case eGroupForm60Status.Approved:
                    {
                        classCss = "label-success";
                        break;
                    }
            }

            return
                MvcHtmlString.Create(String.Format("<span class='label {0}'>{1}</span>", classCss,
                    status.GetDescription()));
        }

        public static MvcHtmlString GetFormStatusTag(this HtmlHelper htmlhleper, eGroupFormStatus status)
        {
            string classCss = "";

            switch (status)
            {
                case eGroupFormStatus.New:
                    {
                        classCss = "label-primary";
                        break;
                    }
                case eGroupFormStatus.InProgress:
                    {
                        classCss = "label-warning";
                        break;
                    }
                case eGroupFormStatus.Rejected:
                    {
                        classCss = "label-danger";
                        break;
                    }
                case eGroupFormStatus.WaitingApproval:
                    {
                        classCss = "label-warning";
                        break;
                    }
                case eGroupFormStatus.Approved:
                    {
                        classCss = "label-success";
                        break;
                    }
            }

            return
                MvcHtmlString.Create(String.Format("<span class='label {0}'>{1}</span>", classCss,
                    status.GetDescription()));
        }


        public static string GetNumberOfParticipantsAlertIndication(this HtmlHelper htmlhelper, int number, int totalnumber)
        {
            string classCss = "";


            if (number == totalnumber)
            {
                classCss = "Green";
            }
            else
            {
                classCss = "Red";
            }

            return classCss;
        }

        public static MvcHtmlString GetGroupFileStatusTag(this HtmlHelper htmlhleper, eGroupFileStatus status)
        {
            string classCss = "";

            switch (status)
            {
                case eGroupFileStatus.New:
                    {
                        classCss = "label-primary";
                        break;
                    }
                case eGroupFileStatus.InProgress:
                    {
                        classCss = "label-warning";
                        break;
                    }
                case eGroupFileStatus.Cancelled:
                    {
                        classCss = "label-danger";
                        break;
                    }
                case eGroupFileStatus.Approved:
                    {
                        classCss = "label-success";
                        break;
                    }
            }

            return
                MvcHtmlString.Create(String.Format("<span class='label {0}'>{1}</span>", classCss,
                    status.GetDescription()));
        }

        public static MvcHtmlString DropDownListFor<TModel, TProperty>(this HtmlHelper<TModel> html,
                                                                   Expression<Func<TModel, TProperty>> expression,
                                                                   IEnumerable<SelectListItem> selectList,
                                                                   string optionText, bool canEdit)
        {
            if (canEdit)
            {
                return html.DropDownListFor(expression, selectList, optionText);
            }
            return html.DropDownListFor(expression, selectList, optionText, new { disabled = "disabled" });
        }

        /// <summary>
        /// Returns an HTML select element for each value in the enumeration that is
        /// represented by the specified expression and predicate.
        /// </summary>
        /// <typeparam name="TModel">The type of the model.</typeparam>
        /// <typeparam name="TEnum">The type of the value.</typeparam>
        /// <param name="htmlHelper">The HTML helper instance that this method extends.</param>
        /// <param name="expression">An expression that identifies the object that contains the properties to display.</param>
        /// <param name="optionLabel">The text for a default empty item. This parameter can be null.</param>
        /// <param name="predicate">A <see cref="Func{TEnum, bool}"/> to filter the items in the enums.</param>
        /// <param name="htmlAttributes">An object that contains the HTML attributes to set for the element.</param>
        /// <returns>An HTML select element for each value in the enumeration that is represented by the expression and the predicate.</returns>
        /// <exception cref="ArgumentNullException">If expression is null.</exception>
        /// <exception cref="ArgumentException">If TEnum is not Enum Type.</exception>
        public static MvcHtmlString EnumDropDownListFor<TModel, TEnum>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TEnum>> expression, Func<TEnum, bool> predicate, string optionLabel, object htmlAttributes) where TEnum : struct, IConvertible
        {
            if (expression == null)
            {
                throw new ArgumentNullException("expression");
            }

            if (!typeof(TEnum).IsEnum)
            {
                throw new ArgumentException("TEnum");
            }

            ModelMetadata metadata = ModelMetadata.FromLambdaExpression(expression, htmlHelper.ViewData);
            IList<SelectListItem> selectList = Enum.GetValues(typeof(TEnum))
                .Cast<TEnum>()
                .Where(e => predicate(e))
                .Select(e => new SelectListItem
                {
                    Value = Convert.ToUInt64(e).ToString(),
                    Text = ((Enum)(object)e).GetDisplayName(),
                }).ToList();
            if (!string.IsNullOrEmpty(optionLabel))
            {
                selectList.Insert(0, new SelectListItem
                {
                    Text = optionLabel,
                });
            }

            return htmlHelper.DropDownListFor(expression, selectList, htmlAttributes);
        }

        //public static IEnumerable<SelectListItem> SelectListForOrderStatus(Order.eOrderStatus EOrder, Role.eRole userRole)
        //{
        //    List<SelectListItem> items = new List<SelectListItem>();


        //    switch (userRole)
        //    {
        //        case Role.eRole.Printer:
        //            {
        //                items.Add(new SelectListItem() { Selected = false, Text = Order.eOrderStatus.New.GetDescription(), Value = ((int)Order.eOrderStatus.New).ToString() });
        //                items.Add(new SelectListItem() { Selected = false, Text = Order.eOrderStatus.ProcessInitiated.GetDescription(), Value = ((int)Order.eOrderStatus.ProcessInitiated).ToString() });
        //                items.Add(new SelectListItem() { Selected = false, Text = Order.eOrderStatus.InProgress.GetDescription(), Value = ((int)Order.eOrderStatus.InProgress).ToString() });
        //                items.Add(new SelectListItem() { Selected = false, Text = Order.eOrderStatus.Done.GetDescription(), Value = ((int)Order.eOrderStatus.Done).ToString() });
        //                items.Add(new SelectListItem() { Selected = false, Text = Order.eOrderStatus.ManagerCare.GetDescription(), Value = ((int)Order.eOrderStatus.ManagerCare).ToString() });
        //                break;
        //            }
        //        case Role.eRole.Shipper:
        //            {
        //                items.Add(new SelectListItem() { Selected = false, Text = Order.eOrderStatus.Shipped.GetDescription(), Value = ((int)Order.eOrderStatus.Shipped).ToString() });
        //                items.Add(new SelectListItem() { Selected = false, Text = Order.eOrderStatus.ReturnedToAffiliate.GetDescription(), Value = ((int)Order.eOrderStatus.ReturnedToAffiliate).ToString() });
        //                items.Add(new SelectListItem() { Selected = false, Text = Order.eOrderStatus.ManagerCare.GetDescription(), Value = ((int)Order.eOrderStatus.ManagerCare).ToString() });
        //                break;
        //            }
        //        case Role.eRole.Affiliate:
        //            {
        //                items.Add(new SelectListItem() { Selected = false, Text = Order.eOrderStatus.ManagerCare.GetDescription(), Value = ((int)Order.eOrderStatus.ManagerCare).ToString() });
        //                break;
        //            }
        //        default:
        //            {

        //                items =
        //                    Enum.GetValues(typeof(Order.eOrderStatus))
        //                        .Cast<Enum>()
        //                        .Select(
        //                            e =>
        //                                new SelectListItem
        //                                {
        //                                    Value = e != null ? (Convert.ToInt32(e)).ToString() : null,
        //                                    Text = e.GetDescription()
        //                                }).ToList();
        //                break;
        //            }
        //    }

        //    return items;
        //}



        public static IEnumerable<Enum> EnumValuesFor<T>(T selected) where T : struct
        {
            Type t = typeof(T);
            if (t.IsEnum)
            {
                return Enum.GetValues(t).Cast<Enum>().ToList();
            }
            return null;
        }

        //public static IGridWithOptions<T> AddScroll<T>(this IGridWithOptions<T>) where T : struct
        //{

        //    return grid;
        //}

        public static string GetDescription<TEnum>(this TEnum value)
        {
            FieldInfo fi = value.GetType().GetField(value.ToString());

            if (fi != null)
            {
                var attributes = (DescriptionAttribute[])fi.GetCustomAttributes(typeof(DescriptionAttribute), false);

                if (attributes.Length > 0)
                    return attributes[0].Description;
            }

            return value.ToString();
        }

        public static string GetDisplayName<TEnum>(this TEnum value)
        {
            FieldInfo fi = value.GetType().GetField(value.ToString());

            if (fi != null)
            {
                var attributes = (DisplayAttribute[])fi.GetCustomAttributes(typeof(DisplayAttribute), false);

                if (attributes.Length > 0)
                    return attributes[0].GetName();
            }

            return value.ToString();
        }

        public static string GetDisplayDescription<TEnum>(this TEnum value)
        {
            FieldInfo fi = value.GetType().GetField(value.ToString());

            if (fi != null)
            {
                var attributes = (DisplayAttribute[])fi.GetCustomAttributes(typeof(DisplayAttribute), false);

                if (attributes.Length > 0)
                    return attributes[0].GetDescription();
            }

            return value.ToString();
        }

        public static SelectList PreAppend(this SelectList list, string dataTextField, string selectedValue, bool selected = false)
        {
            var items = new List<SelectListItem>();
            items.Add(new SelectListItem() { Selected = selected, Text = dataTextField, Value = selectedValue });
            items.AddRange(list.Items.Cast<SelectListItem>().ToList());
            return new SelectList(items, "Value", "Text");
        }
        public static SelectList Append(this SelectList list, string dataTextField, string selectedValue, bool selected = false)
        {
            var items = list.Items.Cast<SelectListItem>().ToList();
            items.Add(new SelectListItem() { Selected = selected, Text = dataTextField, Value = selectedValue });
            return new SelectList(items, "Value", "Text");
        }
        public static SelectList Default(this SelectList list, string DataTextField, string SelectedValue)
        {
            return list.PreAppend(DataTextField, SelectedValue, true);
        }

        public static MvcHtmlString GetInstructorValidityLabel(this HtmlHelper htmlHelper, bool validity)
        {
            string str = "";

            str += "<span class='label ";
            str += validity ? "label-success'>" : "label-danger'>";
            str += validity ? InstructorsStrings.Valid : InstructorsStrings.NotValid;
            str += "</span>";

            return MvcHtmlString.Create(str);
        }

        public static string GetFormStatusClass(this HtmlHelper htmlHelper, eGroupFormStatus status, string customText = "")
        {
            string str = "";
            string iconpath = "";
            string title = "";
            switch (status)
            {
                case eGroupFormStatus.New:
                    {
                        str = "label-new";
                        break;
                    }
                case eGroupFormStatus.InProgress:
                    {
                        str = "label-info";
                        break;
                    }
                case eGroupFormStatus.WaitingApproval:
                    {
                        str = "label-warning";
                        break;
                    }
                case eGroupFormStatus.Rejected:
                    {
                        str = "label-danger";
                        break;
                    }
                case eGroupFormStatus.Approved:
                    {
                        str = "label-success";
                        break;
                    }
                default:
                    {
                        str = "label-primary";
                        break;
                    }
            }

            if (customText != null || customText == "")
            {
                title = customText;
            }

            return str;
        }

        public static MvcHtmlString GetAlert(this HtmlHelper htmlHelper, WarningModel warning)
        {

            string cssClass = "";
            string faIcon = "";
            switch (warning.WarningType)
            {
                case WarningType.Info:
                    {
                        cssClass = "alert-info";
                        faIcon = "fa-info";
                        break;
                    }
                case WarningType.Warning:
                    {
                        cssClass = "alert-warning";
                        faIcon = "fa-warning";
                        break;
                    }
            }

            string str = $"<p class='alert {cssClass}'><i class='fa {faIcon} fa-lg'></i>{warning.Message}</p>";
            return MvcHtmlString.Create(str);
        }

        public static string GetFormStatusClass(this HtmlHelper htmlHelper, eGroupForm60Status status, string customText = "")
        {
            string str = "";
            string iconpath = "";
            string title = "";
            switch (status)
            {
                case eGroupForm60Status.New:
                    {
                        str = "label-info";
                        break;
                    }
                case eGroupForm60Status.InApprovalProcess:
                case eGroupForm60Status.WaitingApprovalAgency:
                case eGroupForm60Status.WaitingPlanApproval:
                    {
                        str = "label-warning";
                        break;
                    }
                case eGroupForm60Status.WaitingFinalApproval:
                    {
                        str = "label-primary";
                        break;
                    }
                case eGroupForm60Status.Rejected:
                    {
                        str = "label-danger";
                        break;
                    }
                case eGroupForm60Status.Approved:
                    {
                        str = "label-success";
                        break;
                    }
                default:
                    {
                        str = "label-primary";
                        break;
                    }
            }

            if (customText != null || customText == "")
            {
                title = customText;
            }

            return str;
        }

        /// <summary>
        /// Using HTML Agiliyu pack to remove HTML Content
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public static string RemoveUnwantedTags(string data)
        {
            if (string.IsNullOrEmpty(data)) return string.Empty;

            var document = new HtmlDocument();
            document.LoadHtml(data);

            var acceptableTags = new String[] { "strong", "em", "u" };

            var nodes = new Queue<HtmlNode>(document.DocumentNode.SelectNodes("./*|./text()"));
            while (nodes.Count > 0)
            {
                var node = nodes.Dequeue();
                var parentNode = node.ParentNode;

                if (!acceptableTags.Contains(node.Name) && node.Name != "#text")
                {
                    var childNodes = node.SelectNodes("./*|./text()");

                    if (childNodes != null)
                    {
                        foreach (var child in childNodes)
                        {
                            nodes.Enqueue(child);
                            parentNode.InsertBefore(child, node);
                        }
                    }

                    parentNode.RemoveChild(node);

                }
            }

            return document.DocumentNode.InnerHtml;
        }

    }

    public class AjaxActionDetails
    {
        public string LinkText { get; set; }
        public string ActionName { get; set; }
        public string ControllerName { get; set; }
        public object RouteValues { get; set; }
        public AjaxOptions AjaxOptions { get; set; }
        public object HtmlAttributes { get; set; }
    }

    public class HtmlActionDetails
    {
        public string LinkText { get; set; }
        public string ActionName { get; set; }
        public string ControllerName { get; set; }
        public string Protocol { get; set; }
        public string HostName { get; set; }
        public string Fragment { get; set; }
        public object RouteValues { get; set; }
        public object HtmlAttributes { get; set; }
    }

    public enum Icons
    {
        Edit,
        Add,
        Delete,
        Ok,
        Remove,
        Yes,
        No,
        Create,
        Folder,
        Excel,
        Move,
        Restore,
        YesBlack,
        Print,
        Plane
    }

    public enum TooltipPlancement
    {
        Top,
        Bottom
    }

    public enum IconColor
    {
        Green,
        Red
    }


}