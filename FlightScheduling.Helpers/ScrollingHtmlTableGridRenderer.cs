﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MvcContrib.UI.Grid;

namespace Endilo.Helpers
{
    public class ScrollingHtmlTableGridRenderer<T> : HtmlTableGridRenderer<T>
    where T : class
    {
        private readonly int _itemsPerPage;
        private readonly IDictionary<string, string> _attributes;
        private readonly string _text;

        public ScrollingHtmlTableGridRenderer(int itemsPerPage, IDictionary<string,string> attributes, string text)
        {
            _itemsPerPage = itemsPerPage;
            _attributes = attributes;
            _text = text;
        }

        protected override void RenderGridEnd(bool isEmpty)
        {
            base.RenderGridEnd(isEmpty);

            string attr = "";

            foreach (var attribute in _attributes)
            {
                attr += String.Format("{0}='{1}' ", attribute.Key, attribute.Value);
            }
            RenderText(String.Format("<div {0} data-itemsperpage='{1}'>{2}</div><div class='LoadingAnimation'></div>", attr, _itemsPerPage.ToString(), _text));
        }

    }
}
