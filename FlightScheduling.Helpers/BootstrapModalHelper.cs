﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using System.Web.WebPages;
using Endilo.Helpers;
using System.Web.Routing;
using System.Web.Mvc.Ajax;

namespace Endilo.Helpers
{
	public static class BootstrapModalHelper
	{
		/// <summary>
		/// Create an HTML tree from a recursive collection of items
		/// </summary>
		public static BootstrapModal LinkForBootstrapModal(this AjaxHelper ajax, string actionName, string controllerName, string rawLabel = "", string modalContainerID = null, object routeValues = null, object htmlAttributes = null)
		{
			return new BootstrapModal(ajax, rawLabel, actionName, controllerName, modalContainerID, routeValues, htmlAttributes);
		}

		public static BootstrapModal LinkForBootstrapModalNo(this AjaxHelper ajax, string actionName, string controllerName, string rawLabel, object routeValues, object htmlAttributes)
		{
			return new BootstrapModal(ajax, rawLabel, actionName, controllerName, null, routeValues, htmlAttributes);
		}
	}

	/// <summary>
	/// Create an HTML tree from a resursive collection of items
	/// </summary>
	public class BootstrapModal : IHtmlString
	{
		private string _modalContainerID;
		private string _loadingElementID;

		private bool _loadingElementIDsetByUser = false;
		private bool _modalContainerIDsetByUser = false;

		private readonly AjaxHelper _ajax;
		private readonly HtmlHelper _html;
		private IDictionary<string, object> _htmlAttributes;
		private string _rawLabel;
		private string _actionName;
		private string _controllerName;
		private RouteValueDictionary _routeValues;
		private string _loadingPanel;

		public BootstrapModal(AjaxHelper ajax, string rawLabel, string actionName, string modalContainerID, string controllerName, object routeValues, object htmlAttributes)
		{
			if (ajax == null) throw new ArgumentNullException("ajax");

			_modalContainerID = string.IsNullOrEmpty(modalContainerID) ? Guid.NewGuid().ToString("N") : modalContainerID;
			_loadingElementID = Guid.NewGuid().ToString("N");
			_ajax = ajax;
			_html = new HtmlHelper(_ajax.ViewContext, _ajax.ViewDataContainer, _ajax.RouteCollection);
			_htmlAttributes = HtmlHelper.AnonymousObjectToHtmlAttributes(htmlAttributes);
			_routeValues = new RouteValueDictionary(routeValues);
			_rawLabel = rawLabel;
			_actionName = actionName;
			_controllerName = controllerName;
		}



		/// <summary>
		/// Raw Label for the link to open the modal window, this html string will be wrapped by an anchor tag
		/// </summary>
		public BootstrapModal RawLabel(Func<object, HelperResult> rawLabel)
		{
			_rawLabel = rawLabel(null).ToHtmlString();
			return this;
		}

		/// <summary>
		/// Define the ID of the elemet which will contain the loading panel.
		/// </summary>
		/// <param name="i_LoadingPanelContainerID">The ID of the Loading Panel container</param>
		public BootstrapModal LoadingPanelID(string i_LoadingPanelContainerID)
		{
			_loadingElementID = i_LoadingPanelContainerID;
			_loadingElementIDsetByUser = true;
			return this;
		}

		/// <summary>
		/// Define the ID of the elemet which will contain the modal window.
		/// </summary>
		/// <param name="i_ModalWindowContainerID">The ID of the Modal Window container</param>
		public BootstrapModal ModalWindowContainerID(string i_ModalWindowContainerID)
		{
			_modalContainerID = i_ModalWindowContainerID;
			return this;
		}

		public BootstrapModal UseExsistingModalcontainer(bool i_UseExsisting)
		{
			_modalContainerIDsetByUser = i_UseExsisting;
			return this;
		}

		/// <summary>
		/// Set a custom string to be displayed during the loading of the modal window
		/// </summary>
		/// <param name="loadingMessage">Message to display</param>
		public BootstrapModal LoadingPanel(string loadingMessage)
		{
			return LoadingPanel(h => new HtmlString("<div class='modal-loading-panel'>" + loadingMessage + "</div>"));
		}

		/// <summary>
		/// Raw Label for the link to open the modal window, this html string will be wrapped by an anchor tag
		/// </summary>
		public BootstrapModal LoadingPanel(Func<object, IHtmlString> loadingPanel)
		{
			_loadingPanel = loadingPanel(null).ToHtmlString();
			return this;
		}

		

		/// <summary>
		/// HTML attributes appended to the root ul node
		/// </summary>
		public BootstrapModal HtmlAttributes(object htmlAttributes)
		{
			HtmlAttributes(HtmlHelper.AnonymousObjectToHtmlAttributes(htmlAttributes));
			return this;
		}

		/// <summary>
		/// HTML attributes appended to the root ul node
		/// </summary>
		public BootstrapModal HtmlAttributes(IDictionary<string, object> htmlAttributes)
		{
			if (htmlAttributes == null) throw new ArgumentNullException("htmlAttributes");
			_htmlAttributes = htmlAttributes;
			return this;
		}

		public string ToHtmlString()
		{
			return ToString();
		}

		public MvcHtmlString ToMVCHtmlString()
		{
			return MvcHtmlString.Create(ToString());
		}

		public override string ToString()
		{
			string html = "";

			var repID = Guid.NewGuid().ToString();
			var lnk = _ajax.ActionLink(repID, _actionName, _controllerName, _routeValues,
				new AjaxOptions
				{
					HttpMethod = "Get",
					InsertionMode = InsertionMode.Replace,
					UpdateTargetId = _modalContainerID,
					LoadingElementId = _loadingElementID,
					OnSuccess = "$('#" + _modalContainerID + "').appendTo('body').modal();",
					OnFailure = "alert('Error')", // TODO: Add some error handling
				}, _htmlAttributes);
			html += MvcHtmlString.Create(lnk.ToString().Replace(repID, _rawLabel)).ToHtmlString();

			if (!_modalContainerIDsetByUser)
			{
				var modalContainer = new TagBuilder("div");
				modalContainer.AddCssClass("modal fade");
				modalContainer.MergeAttribute("id", _modalContainerID);
				html += modalContainer.ToString();
			}

			if (!_loadingElementIDsetByUser)
			{
				var loadingPanel = new TagBuilder("div");
				loadingPanel.MergeAttribute("style", "display:none");
				loadingPanel.InnerHtml = _loadingPanel;
				loadingPanel.MergeAttribute("id", _loadingElementID);
				html += loadingPanel.ToString();
			}

			return html;
		}

	}
}