﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlightScheduling.Helpers
{
    public class WarningModel
    {
        public string Message { get; set; }
        public WarningType WarningType { get; set; }

    }

    public enum WarningType
    {
        Info,
        Warning
    }
}
