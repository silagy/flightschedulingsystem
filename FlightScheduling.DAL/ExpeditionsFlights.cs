//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace FlightScheduling.DAL
{
    using System;
    using System.Collections.Generic;
    
    public partial class ExpeditionsFlights
    {
        public int ID { get; set; }
        public int ExpeditionID { get; set; }
        public int FlightID { get; set; }
        public int NumberOfPassengers { get; set; }
    
        public virtual Expeditions Expeditions { get; set; }
        public virtual Flights Flights { get; set; }
    }
}
