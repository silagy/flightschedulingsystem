﻿using FlightScheduling.Language;
using FlightScheduling.Language.CustomeExtentions;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Security.AccessControl;
using System.Text;
using System.Threading.Tasks;

namespace FlightScheduling.DAL.CustomEntities.Permissions
{
    [Serializable]
    public class PermissionBits
    {
        public int ID { get; set; }
        public int PermissionRoleID { get; set; }
        public int PermissionBit { get; set; }
        public PermissionBitEnum PermissionBitEnum => (PermissionBitEnum)PermissionBit;
        public DateTime CreationDate { get; set; }
        public DateTime UpdateDate { get; set; }
        public PermissionRole PermissionRole { get; set; }
    }

    [Serializable]
    public class PermissionRole
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime UpdateDate { get; set; }

        public List<PermissionBits> Permissions { get; set; }

        public PermissionRole()
        {
            Permissions = new List<PermissionBits>();
        }
    }

    public enum PermissionBitEnum
    {
        [Display(ResourceType = typeof(PermissionStrings), Name = "ExpeditionDashbard_View")]
        [LocalizedDescription("ExpeditionDashbard_View", typeof(PermissionStrings))]
        ExpeditionDashbard_View = 1,
        [Display(ResourceType = typeof(PermissionStrings), Name = "Order_View")]
        [LocalizedDescription("Order_View", typeof(PermissionStrings))]
        Order_View = 2,
        [Display(ResourceType = typeof(PermissionStrings), Name = "Order_Edit")]
        [LocalizedDescription("Order_Edit", typeof(PermissionStrings))]
        Order_Edit = 3,
        [Display(ResourceType = typeof(PermissionStrings), Name = "Order_Delete")]
        [LocalizedDescription("Order_Delete", typeof(PermissionStrings))]
        Order_Delete = 4,
        [Display(ResourceType = typeof(PermissionStrings), Name = "Order_AgencyApproval")]
        [LocalizedDescription("Order_AgencyApproval", typeof(PermissionStrings))]
        Order_AgencyApproval = 5,
        [Display(ResourceType = typeof(PermissionStrings), Name = "Order_Manage")]
        [LocalizedDescription("Order_Manage", typeof(PermissionStrings))]
        Order_Manage = 6,
        [Display(ResourceType = typeof(PermissionStrings), Name = "Trip_View")]
        [LocalizedDescription("Trip_View", typeof(PermissionStrings))]
        Trip_View = 7,
        [Display(ResourceType = typeof(PermissionStrings), Name = "Trip_Edit")]
        [LocalizedDescription("Trip_Edit", typeof(PermissionStrings))]
        Trip_Edit = 8,
        [Display(ResourceType = typeof(PermissionStrings), Name = "Trip_Delete")]
        [LocalizedDescription("Trip_Delete", typeof(PermissionStrings))]
        Trip_Delete = 9,
        [Display(ResourceType = typeof(PermissionStrings), Name = "Flight_View")]
        [LocalizedDescription("Flight_View", typeof(PermissionStrings))]
        Flight_View = 10,
        [Display(ResourceType = typeof(PermissionStrings), Name = "Flight_Edit")]
        [LocalizedDescription("Flight_Edit", typeof(PermissionStrings))]
        Flight_Edit = 11,
        [Display(ResourceType = typeof(PermissionStrings), Name = "Flight_Delete")]
        [LocalizedDescription("Flight_Delete", typeof(PermissionStrings))]
        Flight_Delete = 12,
        [Display(ResourceType = typeof(PermissionStrings), Name = "Expedition_View")]
        [LocalizedDescription("Expedition_View", typeof(PermissionStrings))]
        Expedition_View = 13,
        [Display(ResourceType = typeof(PermissionStrings), Name = "Expedition_Edit")]
        [LocalizedDescription("Expedition_Edit", typeof(PermissionStrings))]
        Expedition_Edit = 14,
        [Display(ResourceType = typeof(PermissionStrings), Name = "Expedition_Delete")]
        [LocalizedDescription("Expedition_Delete", typeof(PermissionStrings))]
        Expedition_Delete = 15,
        [Display(ResourceType = typeof(PermissionStrings), Name = "Expedition_Manage")]
        [LocalizedDescription("Expedition_Manage", typeof(PermissionStrings))]
        Expedition_Manage = 16,
        [Display(ResourceType = typeof(PermissionStrings), Name = "Expedition_UpdateAgency")]
        [LocalizedDescription("Expedition_UpdateAgency", typeof(PermissionStrings))]
        Expedition_UpdateAgency = 17,
        [Display(ResourceType = typeof(PermissionStrings), Name = "Expedition_UpdateContact")]
        [LocalizedDescription("Expedition_UpdateContact", typeof(PermissionStrings))]
        Expedition_UpdateContact = 18,
        [Display(ResourceType = typeof(PermissionStrings), Name = "Expedition_ViewStaff")]
        [LocalizedDescription("Expedition_ViewStaff", typeof(PermissionStrings))]
        Expedition_ViewStaff = 19,
        [Display(ResourceType = typeof(PermissionStrings), Name = "Expedition_AddStaff")]
        [LocalizedDescription("Expedition_AddStaff", typeof(PermissionStrings))]
        Expedition_AddStaff = 20,
        [Display(ResourceType = typeof(PermissionStrings), Name = "Expedition_DeleteStaff")]
        [LocalizedDescription("Expedition_DeleteStaff", typeof(PermissionStrings))]
        Expedition_DeleteStaff = 21,
        [Display(ResourceType = typeof(PermissionStrings), Name = "Expedition_Approval")]
        [LocalizedDescription("Expedition_Approval", typeof(PermissionStrings))]
        Expedition_Approval = 22,
        [Display(ResourceType = typeof(PermissionStrings), Name = "Expedition_ViewFiles")]
        [LocalizedDescription("Expedition_ViewFiles", typeof(PermissionStrings))]
        Expedition_ViewFiles = 23,
        [Display(ResourceType = typeof(PermissionStrings), Name = "Expedition_AddingFiles")]
        [LocalizedDescription("Expedition_AddingFiles", typeof(PermissionStrings))]
        Expedition_AddingFiles = 24,
        [Display(ResourceType = typeof(PermissionStrings), Name = "Expedition_EditingFiles")]
        [LocalizedDescription("Expedition_EditingFiles", typeof(PermissionStrings))]
        Expedition_EditingFiles = 25,
        [Display(ResourceType = typeof(PermissionStrings), Name = "Expedition_UpdateFileStatus")]
        [LocalizedDescription("Expedition_UpdateFileStatus", typeof(PermissionStrings))]
        Expedition_UpdateFileStatus = 26,
        [Display(ResourceType = typeof(PermissionStrings), Name = "Expedtion_DeletingFiles")]
        [LocalizedDescription("Expedtion_DeletingFiles", typeof(PermissionStrings))]
        Expedtion_DeletingFiles = 27,
        [Display(ResourceType = typeof(PermissionStrings), Name = "Expedition_NotesViewing")]
        [LocalizedDescription("Expedition_NotesViewing", typeof(PermissionStrings))]
        Expedition_NotesViewing = 28,
        [Display(ResourceType = typeof(PermissionStrings), Name = "Expedition_NotesEditing")]
        [LocalizedDescription("Expedition_NotesEditing", typeof(PermissionStrings))]
        Expedition_NotesEditing = 29,
        [Display(ResourceType = typeof(PermissionStrings), Name = "Expedition_NotesDeleting")]
        [LocalizedDescription("Expedition_NotesDeleting", typeof(PermissionStrings))]
        Expedition_NotesDeleting = 30,
        [Display(ResourceType = typeof(PermissionStrings), Name = "Expedition_AddSubSchool")]
        [LocalizedDescription("Expedition_AddSubSchool", typeof(PermissionStrings))]
        Expedition_AddSubSchool = 31,
        [Display(ResourceType = typeof(PermissionStrings), Name = "Expedition_UpdateSubSchool")]
        [LocalizedDescription("Expedition_UpdateSubSchool", typeof(PermissionStrings))]
        Expedition_UpdateSubSchool = 32,
        [Display(ResourceType = typeof(PermissionStrings), Name = "Expedition_DeleteSubSchool")]
        [LocalizedDescription("Expedition_DeleteSubSchool", typeof(PermissionStrings))]
        Expedition_DeleteSubSchool = 33,
        [Display(ResourceType = typeof(PermissionStrings), Name = "Expedition_ViewParticipants")]
        [LocalizedDescription("Expedition_ViewParticipants", typeof(PermissionStrings))]
        Expedition_ViewParticipants = 34,
        [Display(ResourceType = typeof(PermissionStrings), Name = "Expedition_EditParticipants")]
        [LocalizedDescription("Expedition_EditParticipants", typeof(PermissionStrings))]
        Expedition_EditParticipants = 35,
        [Display(ResourceType = typeof(PermissionStrings), Name = "Expedition_DeleteParticipant")]
        [LocalizedDescription("Expedition_DeleteParticipant", typeof(PermissionStrings))]
        Expedition_DeleteParticipant = 36,
        [Display(ResourceType = typeof(PermissionStrings), Name = "Expedition_FormsViewing")]
        [LocalizedDescription("Expedition_FormsViewing", typeof(PermissionStrings))]
        Expedition_FormsViewing = 37,
        [Display(ResourceType = typeof(PermissionStrings), Name = "Form180_Adimistrative_View")]
        [LocalizedDescription("Form180_Adimistrative_View", typeof(PermissionStrings))]
        Form180_Adimistrative_View = 38,
        [Display(ResourceType = typeof(PermissionStrings), Name = "Form180_Adimistrative_Editing")]
        [LocalizedDescription("Form180_Adimistrative_Editing", typeof(PermissionStrings))]
        Form180_Adimistrative_Editing = 39,
        [Display(ResourceType = typeof(PermissionStrings), Name = "Form180_Adimistrative_Approval")]
        [LocalizedDescription("Form180_Adimistrative_Approval", typeof(PermissionStrings))]
        Form180_Adimistrative_Approval = 40,
        [Display(ResourceType = typeof(PermissionStrings), Name = "Form180_Instructors_View")]
        [LocalizedDescription("Form180_Instructors_View", typeof(PermissionStrings))]
        Form180_Instructors_View = 41,
        [Display(ResourceType = typeof(PermissionStrings), Name = "Form180_Instructors_Editing")]
        [LocalizedDescription("Form180_Instructors_Editing", typeof(PermissionStrings))]
        Form180_Instructors_Editing = 42,
        [Display(ResourceType = typeof(PermissionStrings), Name = "Form180_Instructors_Approval")]
        [LocalizedDescription("Form180_Instructors_Approval", typeof(PermissionStrings))]
        Form180_Instructors_Approval = 43,
        [Display(ResourceType = typeof(PermissionStrings), Name = "Form180_Plan_View")]
        [LocalizedDescription("Form180_Plan_View", typeof(PermissionStrings))]
        Form180_Plan_View = 44,
        [Display(ResourceType = typeof(PermissionStrings), Name = "Form180_Plan_Editing")]
        [LocalizedDescription("Form180_Plan_Editing", typeof(PermissionStrings))]
        Form180_Plan_Editing = 45,
        [Display(ResourceType = typeof(PermissionStrings), Name = "Form180_Plan_Approval")]
        [LocalizedDescription("Form180_Plan_Approval", typeof(PermissionStrings))]
        Form180_Plan_Approval = 46,
        [Display(ResourceType = typeof(PermissionStrings), Name = "Training_Viewing")]
        [LocalizedDescription("Training_Viewing", typeof(PermissionStrings))]
        Training_Viewing = 47,
        [Display(ResourceType = typeof(PermissionStrings), Name = "Training_Editing")]
        [LocalizedDescription("Training_Editing", typeof(PermissionStrings))]
        Training_Editing = 48,
        [Display(ResourceType = typeof(PermissionStrings), Name = "Training_Deleting")]
        [LocalizedDescription("Training_Deleting", typeof(PermissionStrings))]
        Training_Deleting = 49,
        [Display(ResourceType = typeof(PermissionStrings), Name = "Training_ClosingTraining")]
        [LocalizedDescription("Training_ClosingTraining", typeof(PermissionStrings))]
        Training_ClosingTraining = 50,
        [Display(ResourceType = typeof(PermissionStrings), Name = "Training_UpdateInstructorCertificate")]
        [LocalizedDescription("Training_UpdateInstructorCertificate", typeof(PermissionStrings))]
        Training_UpdateInstructorCertificate = 51,
        [Display(ResourceType = typeof(PermissionStrings), Name = "Reports_ParticipationInTraining")]
        [LocalizedDescription("Reports_ParticipationInTraining", typeof(PermissionStrings))]
        Reports_ParticipationInTraining = 52,
        [Display(ResourceType = typeof(PermissionStrings), Name = "Instructors_Viewing")]
        [LocalizedDescription("Instructors_Viewing", typeof(PermissionStrings))]
        Instructors_Viewing = 53,
        [Display(ResourceType = typeof(PermissionStrings), Name = "Instructors_Editing")]
        [LocalizedDescription("Instructors_Editing", typeof(PermissionStrings))]
        Instructors_Editing = 54,
        [Display(ResourceType = typeof(PermissionStrings), Name = "Instructors_Deleting")]
        [LocalizedDescription("Instructors_Deleting", typeof(PermissionStrings))]
        Instructors_Deleting = 55,
        [Display(ResourceType = typeof(PermissionStrings), Name = "Instructors_UpdateCetificate")]
        [LocalizedDescription("Instructors_UpdateCetificate", typeof(PermissionStrings))]
        Instructors_UpdateCetificate = 56,
        [Display(ResourceType = typeof(PermissionStrings), Name = "Instructors_Files_Viewing")]
        [LocalizedDescription("Instructors_Files_Viewing", typeof(PermissionStrings))]
        Instructors_Files_Viewing = 57,
        [Display(ResourceType = typeof(PermissionStrings), Name = "Instructors_Files_Editing")]
        [LocalizedDescription("Instructors_Files_Editing", typeof(PermissionStrings))]
        Instructors_Files_Editing = 58,
        [Display(ResourceType = typeof(PermissionStrings), Name = "Instructors_Files_Deleting")]
        [LocalizedDescription("Instructors_Files_Deleting", typeof(PermissionStrings))]
        Instructors_Files_Deleting = 59,
        [Display(ResourceType = typeof(PermissionStrings), Name = "Instructors_TrackingNotes_Viewing")]
        [LocalizedDescription("Instructors_TrackingNotes_Viewing", typeof(PermissionStrings))]
        Instructors_TrackingNotes_Viewing = 60,
        [Display(ResourceType = typeof(PermissionStrings), Name = "Instructors_TrackingNotes_Editing")]
        [LocalizedDescription("Instructors_TrackingNotes_Editing", typeof(PermissionStrings))]
        Instructors_TrackingNotes_Editing = 61,
        [Display(ResourceType = typeof(PermissionStrings), Name = "Instructors_TrackingNotes_Deleting")]
        [LocalizedDescription("Instructors_TrackingNotes_Deleting", typeof(PermissionStrings))]
        Instructors_TrackingNotes_Deleting = 62,
        [Display(ResourceType = typeof(PermissionStrings), Name = "Reports_Agencies")]
        [LocalizedDescription("Reports_Agencies", typeof(PermissionStrings))]
        Reports_Agencies = 63,
        [Display(ResourceType = typeof(PermissionStrings), Name = "Reports_Groups")]
        [LocalizedDescription("Reports_Groups", typeof(PermissionStrings))]
        Reports_Groups = 64,
        [Display(ResourceType = typeof(PermissionStrings), Name = "Reports_Flights")]
        [LocalizedDescription("Reports_Flights", typeof(PermissionStrings))]
        Reports_Flights = 65,
        [Display(ResourceType = typeof(PermissionStrings), Name = "Reports_Schools")]
        [LocalizedDescription("Reports_Schools", typeof(PermissionStrings))]
        Reports_Schools = 66,
        [Display(ResourceType = typeof(PermissionStrings), Name = "Reports_AvailableSeats")]
        [LocalizedDescription("Reports_AvailableSeats", typeof(PermissionStrings))]
        Reports_AvailableSeats = 67,
        [Display(ResourceType = typeof(PermissionStrings), Name = "Reports_BusesPerDay")]
        [LocalizedDescription("Reports_BusesPerDay", typeof(PermissionStrings))]
        Reports_BusesPerDay = 68,
        [Display(ResourceType = typeof(PermissionStrings), Name = "Reports_ExportGroupsForSecurity")]
        [LocalizedDescription("Reports_ExportGroupsForSecurity", typeof(PermissionStrings))]
        Reports_ExportGroupsForSecurity = 69,
        [Display(ResourceType = typeof(PermissionStrings), Name = "Reports_PresentsOnTrip")]
        [LocalizedDescription("Reports_PresentsOnTrip", typeof(PermissionStrings))]
        Reports_PresentsOnTrip = 70,
        [Display(ResourceType = typeof(PermissionStrings), Name = "Reports_GroupAndDeputyManagers")]
        [LocalizedDescription("Reports_GroupAndDeputyManagers", typeof(PermissionStrings))]
        Reports_GroupAndDeputyManagers = 71,
        [Display(ResourceType = typeof(PermissionStrings), Name = "Reports_TrainingParticipation")]
        [LocalizedDescription("Reports_TrainingParticipation", typeof(PermissionStrings))]
        Reports_TrainingParticipation = 72,
        [Display(ResourceType = typeof(PermissionStrings), Name = "Reports_ContactInUrgency")]
        [LocalizedDescription("Reports_ContactInUrgency", typeof(PermissionStrings))]
        Reports_ContactInUrgency = 73,
        [Display(ResourceType = typeof(PermissionStrings), Name = "Administration_AgencyViewing")]
        [LocalizedDescription("Administration_AgencyViewing", typeof(PermissionStrings))]
        Administration_AgencyViewing = 74,
        [Display(ResourceType = typeof(PermissionStrings), Name = "Administration_AgencyEditing")]
        [LocalizedDescription("Administration_AgencyEditing", typeof(PermissionStrings))]
        Administration_AgencyEditing = 75,
        [Display(ResourceType = typeof(PermissionStrings), Name = "Administration_AgencyDeleting")]
        [LocalizedDescription("Administration_AgencyDeleting", typeof(PermissionStrings))]
        Administration_AgencyDeleting = 76,
        [Display(ResourceType = typeof(PermissionStrings), Name = "Administration_SchoolView")]
        [LocalizedDescription("Administration_SchoolView", typeof(PermissionStrings))]
        Administration_SchoolView = 77,
        [Display(ResourceType = typeof(PermissionStrings), Name = "Administration_SchoolEdit")]
        [LocalizedDescription("Administration_SchoolEdit", typeof(PermissionStrings))]
        Administration_SchoolEdit = 78,
        [Display(ResourceType = typeof(PermissionStrings), Name = "Administration_SchoolDelete")]
        [LocalizedDescription("Administration_SchoolDelete", typeof(PermissionStrings))]
        Administration_SchoolDelete = 79,
        [Display(ResourceType = typeof(PermissionStrings), Name = "Form60_View")]
        [LocalizedDescription("Form60_View", typeof(PermissionStrings))]
        Form60_View = 80,
        [Display(ResourceType = typeof(PermissionStrings), Name = "Form60_Flights_View")]
        [LocalizedDescription("Form60_Flights_View", typeof(PermissionStrings))]
        Form60_Flights_View = 81,
        [Display(ResourceType = typeof(PermissionStrings), Name = "Form60_Flights_Edit")]
        [LocalizedDescription("Form60_Flights_Edit", typeof(PermissionStrings))]
        Form60_Flights_Edit = 82,
        [Display(ResourceType = typeof(PermissionStrings), Name = "Form60_TripPlan_View")]
        [LocalizedDescription("Form60_TripPlan_View", typeof(PermissionStrings))]
        Form60_TripPlan_View = 83,
        [Display(ResourceType = typeof(PermissionStrings), Name = "Form60_TripPlan_Edit")]
        [LocalizedDescription("Form60_TripPlan_Edit", typeof(PermissionStrings))]
        Form60_TripPlan_Edit = 84,
        [Display(ResourceType = typeof(PermissionStrings), Name = "Form60_Contacts_View")]
        [LocalizedDescription("Form60_Contacts_View", typeof(PermissionStrings))]
        Form60_Contacts_View = 85,
        [Display(ResourceType = typeof(PermissionStrings), Name = "Form60_Contacts_Edit")]
        [LocalizedDescription("Form60_Contacts_Edit", typeof(PermissionStrings))]
        Form60_Contacts_Edit = 86,
        [Display(ResourceType = typeof(PermissionStrings), Name = "Form60_FormStatus")]
        [LocalizedDescription("Form60_FormStatus", typeof(PermissionStrings))]
        Form60_FormStatus = 87,
        [Display(ResourceType = typeof(PermissionStrings), Name = "Form60_AgencyApprovalStatus")]
        [LocalizedDescription("Form60_AgencyApprovalStatus", typeof(PermissionStrings))]
        Form60_AgencyApprovalStatus = 88,
        [Display(ResourceType = typeof(PermissionStrings), Name = "Form60_PlanApprovalStatus")]
        [LocalizedDescription("Form60_PlanApprovalStatus", typeof(PermissionStrings))]
        Form60_PlanApprovalStatus = 89,
        [Display(ResourceType = typeof(PermissionStrings), Name = "Instructors_CreateUser")]
        [LocalizedDescription("Instructors_CreateUser", typeof(PermissionStrings))]
        Instructors_CreateUser = 90,
        [Display(ResourceType = typeof(PermissionStrings), Name = "Form60_FormSubmit")]
        [LocalizedDescription("Form60_FormSubmit", typeof(PermissionStrings))]
        Form60_FormSubmit = 91,
        [Display(ResourceType = typeof(PermissionStrings), Name = "Form60_Flights_EditDestinations")]
        [LocalizedDescription("Form60_Flights_EditDestinations", typeof(PermissionStrings))]
        Form60_Flights_EditDestinations = 92,
        [Display(ResourceType = typeof(PermissionStrings), Name = "Instructors_UpdateExpiration")]
        [LocalizedDescription("Instructors_UpdateExpiration", typeof(PermissionStrings))]
        Instructors_UpdateExpiration = 93,
        [Display(ResourceType = typeof(PermissionStrings), Name = "Expedition_ViewSubSchool")]
        [LocalizedDescription("Expedition_ViewSubSchool", typeof(PermissionStrings))]
        Expedition_ViewSubSchool = 94,
        [Display(ResourceType = typeof(PermissionStrings), Name = "Order_Creation")]
        [LocalizedDescription("Order_Creation", typeof(PermissionStrings))]
        Order_Creation = 95,
        [Display(ResourceType = typeof(PermissionStrings), Name = "Reports_Viewing")]
        [LocalizedDescription("Reports_Viewing", typeof(PermissionStrings))]
        Reports_Viewing = 96,
        [Display(ResourceType = typeof(PermissionStrings), Name = "Administration_School_Remarks_View")]
        [LocalizedDescription("Administration_School_Remarks_View", typeof(PermissionStrings))]
        Administration_School_Remarks_View = 97,
        [Display(ResourceType = typeof(PermissionStrings), Name = "Administration_School_Remarks_Edit")]
        [LocalizedDescription("Administration_School_Remarks_Edit", typeof(PermissionStrings))]
        Administration_School_Remarks_Edit = 98,
        [Display(ResourceType = typeof(PermissionStrings), Name = "Administration_School_Remarks_Delete")]
        [LocalizedDescription("Administration_School_Remarks_Delete", typeof(PermissionStrings))]
        Administration_School_Remarks_Delete = 99,
        [Display(ResourceType = typeof(PermissionStrings), Name = "Expedition_DeleteMultipleParticipants")]
        [LocalizedDescription("Expedition_DeleteMultipleParticipants", typeof(PermissionStrings))]
        Expedition_DeleteMultipleParticipants = 100,
        [Display(ResourceType = typeof(PermissionStrings), Name = "Form180_MoveForm")]
        [LocalizedDescription("Form180_MoveForm", typeof(PermissionStrings))]
        Form180_MoveForm = 101,
        [Display(ResourceType = typeof(PermissionStrings), Name = "Administration_Sites_View")]
        [LocalizedDescription("Administration_Sites_View", typeof(PermissionStrings))]
        Administration_Sites_View = 102,
        [Display(ResourceType = typeof(PermissionStrings), Name = "Administration_Sites_Edit")]
        [LocalizedDescription("Administration_Sites_Edit", typeof(PermissionStrings))]
        Administration_Sites_Edit = 103,
        [Display(ResourceType = typeof(PermissionStrings), Name = "Administration_Sites_Delete")]
        [LocalizedDescription("Administration_Sites_Delete", typeof(PermissionStrings))]
        Administration_Sites_Delete = 104,
        [Display(ResourceType = typeof(PermissionStrings), Name = "Administration_City_View")]
        [LocalizedDescription("Administration_City_View", typeof(PermissionStrings))]
        Administration_City_View = 105,
        [Display(ResourceType = typeof(PermissionStrings), Name = "Administration_City_Edit")]
        [LocalizedDescription("Administration_City_Edit", typeof(PermissionStrings))]
        Administration_City_Edit = 106,
        [Display(ResourceType = typeof(PermissionStrings), Name = "Administration_City_Delete")]
        [LocalizedDescription("Administration_City_Delete", typeof(PermissionStrings))]
        Administration_City_Delete = 107,
        [Display(ResourceType = typeof(PermissionStrings), Name = "Reports_SchoolGlobalRemarks")]
        [LocalizedDescription("Reports_SchoolGlobalRemarks", typeof(PermissionStrings))]
        Reports_SchoolGlobalRemarks = 108,
        [Display(ResourceType = typeof(PermissionStrings), Name = "Reports_GroupsByRegion")]
        [LocalizedDescription("Reports_GroupsByRegion", typeof(PermissionStrings))]
        Reports_GroupsByRegion = 109,
        [Display(ResourceType = typeof(PermissionStrings), Name = "Form60_EditManagerDetails")]
        [LocalizedDescription("Form60_EditManagerDetails", typeof(PermissionStrings))]
        Form60_EditManagerDetails = 110,
        [Display(ResourceType = typeof(PermissionStrings), Name = "Reports_InstructorsAssignmentsForGroups")]
        [LocalizedDescription("Reports_InstructorsAssignmentsForGroups", typeof(PermissionStrings))]
        Reports_InstructorsAssignmentsForGroups = 111,
        [Display(ResourceType = typeof(PermissionStrings), Name = "Flight_Delete_Multiple")]
        [LocalizedDescription("Flight_Delete_Multiple", typeof(PermissionStrings))]
        Flight_Delete_Multiple = 112,
        [Display(ResourceType = typeof(PermissionStrings), Name = "Expeditions_AddQuickExpedition")]
        [LocalizedDescription("Expeditions_AddQuickExpedition", typeof(PermissionStrings))]
        Expeditions_AddQuickExpedition = 113,
        [Display(ResourceType = typeof(PermissionStrings), Name = "Order_View_Available_Slots")]
        [LocalizedDescription("Order_View_Available_Slots", typeof(PermissionStrings))]
        Order_View_Available_Slots = 114,
        [Display(ResourceType = typeof(PermissionStrings), Name = "Reports_ExpeditionStatus")]
        [LocalizedDescription("Reports_ExpeditionStatus", typeof(PermissionStrings))]
        Reports_ExpeditionStatus = 115,
        [Display(ResourceType = typeof(PermissionStrings), Name = "Order_ViewBusAvailability")]
        [LocalizedDescription("Order_ViewBusAvailability", typeof(PermissionStrings))]
        Order_ViewBusAvailability = 116,
        [Display(ResourceType = typeof(PermissionStrings), Name = "Expedition_MoveGroup")]
        [LocalizedDescription("Expedition_MoveGroup", typeof(PermissionStrings))]
        Expedition_MoveGroup = 117,
    }
}
