﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FlightScheduling.Language;
using FlightScheduling.Language.CustomeExtentions;

namespace FlightScheduling.DAL.CustomEntities
{
    public class SystemConfiguration
    {
        public int PropertyID { get; set; }
        public string PropertyName { get; set; }
        public ePropertyType PropertyType { get; set; }
        public string PropertyValue { get; set; }
        public string PropertyDescription { get; set; }
    }

    public enum ePropertyType
    {
        [Display(ResourceType = typeof(AdministrationStrings), Name = "Text")]
        [LocalizedDescription("Text", typeof(AdministrationStrings))]
        Text = 1,
        [Display(ResourceType = typeof(AdministrationStrings), Name = "Date")]
        [LocalizedDescription("Date", typeof(AdministrationStrings))]
        Date = 2,
        [Display(ResourceType = typeof(AdministrationStrings), Name = "Checkbox")]
        [LocalizedDescription("Checkbox", typeof(AdministrationStrings))]
        Checkbox = 3,
        [Display(ResourceType = typeof(AdministrationStrings), Name = "Bool")]
        [LocalizedDescription("Bool", typeof(AdministrationStrings))]
        Bool = 4,
    }


}
