﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlightScheduling.DAL.CustomEntities
{
    public class Messages
    {

        //Properties
        public int Id { get; set; }
        public string Title { get; set; }
        public string Message { get; set; }
        public int UserID { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime UpdateDate { get; set; }
        public DateTime ActiveStartDate { get; set; }
        public DateTime ExpirationDate { get; set; }

        public Messages()
        {
            CreationDate = DateTime.Now;
        }
    }
}
