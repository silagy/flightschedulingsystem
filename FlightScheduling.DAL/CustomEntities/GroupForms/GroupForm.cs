﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlightScheduling.DAL.CustomEntities.GroupForms
{
    public class GroupForm
    {
        //Properties
        public int ID { get; set; }
        public int GroupSchoolID { get; set; }
        public int FormType { get; set; }
        public eGroupFormType GroupFormTypeEnum
        {
            get
            {
                return (eGroupFormType)this.FormType;
            }
        }
        public DateTime CreationDate { get; set; }
        public DateTime UpdateDate { get; set; }
    }
}
