﻿using FlightScheduling.Language;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlightScheduling.DAL.CustomEntities.GroupForms
{
    public class TripActicity
    {
        public int ID { get; set; }

        public int ActivityType { get; set; }

        [Display(ResourceType = typeof(GroupFormsStrings), Name = "ActivityType")]
        public eActivityType ActivityTypeEnum { get => (eActivityType)ActivityType; set => ActivityType = ((int)value); }

        [Display(ResourceType = typeof(GroupFormsStrings), Name = "ActivityName")]
        public string Name { get; set; }


        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public double ActivityDuration => (EndTime - StartTime).TotalMinutes;



        [Display(ResourceType = typeof(GroupFormsStrings), Name = "ActivityStartTime")]
        public string StartTimeValue { get; set; }

        public string Comments { get; set; }

        [Display(ResourceType = typeof(GroupFormsStrings), Name = "SiteID")]
        public int? SiteID { get; set; }
        public int GroupID { get; set; }
        public int CityID { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime UpdateDate { get; set; }

        // EXTENDED ACTIVITY
        [Display(ResourceType = typeof(GroupFormsStrings), Name = "SchoolName")]
        public string SchoolName { get; set; }

        [Display(ResourceType = typeof(GroupFormsStrings), Name = "SchoolAddress")]
        public string SchoolAddress { get; set; }

        [Display(ResourceType = typeof(GroupFormsStrings), Name = "ContactName")]
        public string ContactName { get; set; }

        [Display(ResourceType = typeof(GroupFormsStrings), Name = "ContactEmail")]
        public string ContactEmail { get; set; }

        [Display(ResourceType = typeof(GroupFormsStrings), Name = "ContactCell")]
        public string ContactCell { get; set; }

        [Display(ResourceType = typeof(GroupFormsStrings), Name = "ContactPhone")]
        public string ContactPhone { get; set; }

        // CHECK IF THERE IS A SCHOOL NAME, IF NOT IT IS REGUYLAR ACTIVITY ELSE IT IS EXTENDED ACTICITY
        public bool IsExtended => !String.IsNullOrEmpty(SchoolName);

        // Site
        public Site Site { get; set; }

        // City
        public City City { get; set; }


        public TripActicity()
        {
        }
    }


    public class Site
    {
        public int ID { get; set; }
        public int? ExternalID { get; set; }
        public int SiteType { get; set; }
        public eSiteType SiteTypeEnum { get => (eSiteType)SiteType; set => SiteType = (int)value; }
        public string Name { get; set; }
        public string LocalName { get; set; }
        public string LocalRegion { get; set; }
        public string Limitations { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime UpdateDate { get; set; }
        public City City { get; set; }

        public Site()
        {
            City = new City();
        }
    }

    public class City
    {
        public int CityID { get; set; }
        public string CityName { get; set; }
        public int? CityExternalID { get; set; }
        public int RegionID { get; set; }
        public string RegionName { get; set; }
        public int DistrictID { get; set; }
        public string DistrictName { get; set; }

    }
}
