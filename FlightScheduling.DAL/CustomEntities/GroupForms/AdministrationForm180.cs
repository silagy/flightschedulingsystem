﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlightScheduling.DAL.CustomEntities.GroupForms
{
    public class AdministrationForm180
    {
        //Properties
        public int ID { get; set; }
        public int GroupFormID { get; set; }
        public string ContractUrl { get; set; }
        public string SignatureUrl { get; set; }
        public int FormStatus { get; set; }
        public eGroupFormStatus GroupFormStatusEnum
        {
            get
            {
                return (eGroupFormStatus)this.FormStatus;
            }
        }
        public DateTime CreationDate { get; set; }
        public DateTime UpdateDate { get; set; }
    }
}
