﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FlightScheduling.Language;
using FlightScheduling.Language.CustomeExtentions;

namespace FlightScheduling.DAL.CustomEntities.Notifications
{
    public class Notifications
    {
        //Properties
        public int ID { get; set; }
        public int NotificationTempateID { get; set; }
        public NotificationTemplate NotificationTempate { get; set; }
        public int UserID { get; set; }
        public Users User { get; set; }
        public string EmailAddress { get; set; }
        public eNotificationStatus NotificationStatus { get; set; }
        public string MessageSubject { get; set; }
        public string MessageBody { get; set; }
        public DateTime? SendDate { get; set; }
        public string Remarks { get; set; }
        public string CallToAction { get; set; }
        public DateTime CreationDate { get; set; }
        public bool IsRead { get; set; }
    }

    
}
