﻿using FlightScheduling.Language.CustomeExtentions;
using System.ComponentModel.DataAnnotations;
using FlightScheduling.Language;


namespace FlightScheduling.DAL.CustomEntities.Notifications
{
    public enum eNotificationStatus
    {
        [Display(ResourceType = typeof(NotificationStrings), Name = "Pendding")]
        [LocalizedDescription("Pendding", typeof(NotificationStrings))]
        Pendding = 1,

        [Display(ResourceType = typeof(NotificationStrings), Name = "Sent")]
        [LocalizedDescription("Sent", typeof(NotificationStrings))]
        Sent = 2,

        [Display(ResourceType = typeof(NotificationStrings), Name = "Failed")]
        [LocalizedDescription("Failed", typeof(NotificationStrings))]
        Failed = 3,
    }

    public enum eNotificationTriggerType
    {
        [Display(ResourceType = typeof(NotificationStrings), Name = "GroupForm180Creation")]
        [LocalizedDescription("GroupForm180Creation", typeof(NotificationStrings))]
        GroupForm180Creation = 1,

        [Display(ResourceType = typeof(NotificationStrings), Name = "ExpeditionApproval")]
        [LocalizedDescription("ExpeditionApproval", typeof(NotificationStrings))]
        ExpeditionApproval = 2,

        [Display(ResourceType = typeof(NotificationStrings), Name = "GroupFormAdministration180StatusChange")]
        [LocalizedDescription("GroupFormAdministration180StatusChange", typeof(NotificationStrings))]
        GroupFormAdministration180StatusChange = 3,

        [Display(ResourceType = typeof(NotificationStrings), Name = "GroupFormInstructors180StatusChange")]
        [LocalizedDescription("GroupFormInstructors180StatusChange", typeof(NotificationStrings))]
        GroupFormInstructors180StatusChange = 4,

        [Display(ResourceType = typeof(NotificationStrings), Name = "GroupFormPlan180StatusChange")]
        [LocalizedDescription("GroupFormPlan180StatusChange", typeof(NotificationStrings))]
        GroupFormPlan180StatusChange = 5,

        [Display(ResourceType = typeof(NotificationStrings), Name = "NewGroupFile")]
        [LocalizedDescription("NewGroupFile", typeof(NotificationStrings))]
        NewGroupFile = 6,

        [Display(ResourceType = typeof(NotificationStrings), Name = "GroupForm60Updated")]
        [LocalizedDescription("GroupForm60Updated", typeof(NotificationStrings))]
        GroupForm60Updated = 7,

        [Display(ResourceType = typeof(NotificationStrings), Name = "GroupForm60Submitted")]
        [LocalizedDescription("GroupForm60Submitted", typeof(NotificationStrings))]
        GroupForm60Submitted = 8,
    }

    public enum eNotificationSchedulingType
    {
        [Display(ResourceType = typeof(NotificationStrings), Name = "Immediate")]
        [LocalizedDescription("Immediate", typeof(NotificationStrings))]
        Immediate = 1,

        [Display(ResourceType = typeof(NotificationStrings), Name = "ScheduledOnceDay")]
        [LocalizedDescription("ScheduledOnceDay", typeof(NotificationStrings))]
        ScheduledOnceDay = 2,
    }
}
