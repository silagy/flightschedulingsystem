﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FlightScheduling.Language;
using FlightScheduling.Language.CustomeExtentions;
using System.ComponentModel.DataAnnotations;
using Flight.DAL.FlightDB.Methods;
namespace FlightScheduling.DAL.CustomEntities.Notifications
{
    public class NotificationTemplate
    {

        //Properties
        public int ID { get; set; }
        public string Name { get; set; }
        public int TriggerType { get; set; }
        public eNotificationTriggerType NotificationTriggerType => (eNotificationTriggerType) TriggerType;
        public string Subject { get; set; }
        public string Message { get; set; }
        public int SchedulingType { get; set; }
        public eNotificationSchedulingType NotificationSchedulingType => (eNotificationSchedulingType) SchedulingType;
        public bool IsActive { get; set; }
        public int UserType { get; set; }
        public eRole UserTypeEnum => (eRole)UserType;
        public int UserPermissionRole { get; set; }


    }

    
}
