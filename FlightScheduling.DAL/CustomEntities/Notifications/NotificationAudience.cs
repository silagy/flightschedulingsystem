﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Flight.DAL.FlightDB.Methods;

namespace FlightScheduling.DAL.CustomEntities.Notifications
{
    public class NotificationAudience
    {

        //Properties
        public int ID { get; set; }
        public int NotificationID { get; set; }
        public NotificationTemplate NotificationTempate { get; set; }

        public int UserType { get; set; }

        public eRole UserRole
        {
            get { return (eRole) UserType; }
        }
    }
}
