﻿using FlightScheduling.Language;
using FlightScheduling.Language.CustomeExtentions;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlightScheduling.DAL.CustomEntities
{
    public class TransactionResult
    {
        public bool Result { get; set; }
        public List<TransactionErrors> Errors { get; set; }

        public TransactionResult()
        {
            Result = false;
            Errors = new List<TransactionErrors>();
        }
    }

    public enum TransactionErrors
    {
        [Display(ResourceType = typeof(TripsStrings), Name = "ExpeditionCannotBeSaved")]
        [LocalizedDescription("ExpeditionCannotBeSaved", typeof(TripsStrings))]
        ExpeditionCannotBeSaved,
        [Display(ResourceType = typeof(TripsStrings), Name = "NoDepartureFlightFound")]
        [LocalizedDescription("NoDepartureFlightFound", typeof(TripsStrings))]
        NoDepartureFlightFound,
        [Display(ResourceType = typeof(TripsStrings), Name = "NoAvailableSeatsOntheOutGoingFlight")]
        [LocalizedDescription("NoAvailableSeatsOntheOutGoingFlight", typeof(TripsStrings))]
        NoAvailableSeatsOntheOutGoingFlight,
        [Display(ResourceType = typeof(TripsStrings), Name = "CannotAddOutGoingFlight")]
        [LocalizedDescription("CannotAddOutGoingFlight", typeof(TripsStrings))]
        CannotAddOutGoingFlight,
        [Display(ResourceType = typeof(TripsStrings), Name = "NoReturnFlightFound")]
        [LocalizedDescription("NoReturnFlightFound", typeof(TripsStrings))]
        NoReturnFlightFound,
        [Display(ResourceType = typeof(TripsStrings), Name = "NoAvailableSeatsOnTheRetunedFlight")]
        [LocalizedDescription("NoAvailableSeatsOnTheRetunedFlight", typeof(TripsStrings))]
        NoAvailableSeatsOnTheRetunedFlight,
        [Display(ResourceType = typeof(TripsStrings), Name = "CannotAddReturningFlight")]
        [LocalizedDescription("CannotAddReturningFlight", typeof(TripsStrings))]
        CannotAddReturningFlight,
        [Display(ResourceType = typeof(TripsStrings), Name = "OtherError")]
        [LocalizedDescription("OtherError", typeof(TripsStrings))]
        OtherError,
        [Display(ResourceType = typeof(ExpeditionStrings), Name = "ErrorCancelOrder")]
        [LocalizedDescription("ErrorCancelOrder", typeof(ExpeditionStrings))]
        CannotCancelOrder
    }
}
