﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlightScheduling.DAL.CustomEntities
{
    public class ExpeditionToCSVIntegration
    {
        //Properties
        public int ExpeditionID { get; set; }
        public DateTime DepartureDate { get; set; }
        public DateTime ReturningDate { get; set; }
        public int DestinationID { get; set; }
        public int ReturingField { get; set; }
        public string SchoolName { get; set; }
        public int InstituteID { get; set; }
        public int BasesPerDay { get; set; }
        public int NumberOFpassengers { get; set; }
        public string AgencyName { get; set; }
        public string Manager { get; set; }
        public string ManagerCell { get; set; }
        public string ManagerEmail { get; set; }
        public string ManagerPhone { get; set; }
        public string SchoolManager { get; set; }
        public string SchoolManagerEmail { get; set; }
        public string SchoolManagerPhone { get; set; }
        public int GroupID { get; set; }
    }
}
