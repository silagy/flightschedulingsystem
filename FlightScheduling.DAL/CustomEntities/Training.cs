﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlightScheduling.DAL.CustomEntities
{
    public class Training
    {
        //Properties
        public int ID { get; set; }
        public string Name { get; set; }
        public int TrainingLocationID { get; set; }
        public DateTime TrainingDate { get; set; }
        public int TrainingHours { get; set; }
        public int TrainingValidationID { get; set; }
        public bool IsCompleted { get; set; }
        public int RoleID { get; set; }
        public int? RegionID { get; set; }
        public int? TrainingTypeID { get; set; }
        public string TrainingTypeName { get; set; }
        public int CreatorID { get; set; }
        public InstructorRoleEntity Role { get; set; }

        public eTrainingLocation TrainingLocationEnum
        {
            get { return (eTrainingLocation)this.TrainingLocationID; }

            set { this.TrainingLocationID = (int)value; }
        }

        public eTrainingType TrainingTypeEnum
        {
            get { return (eTrainingType)this.TrainingValidationID; }

            set { this.TrainingValidationID = (int)value; }
        }

        public eSchoolRegion? TrainingRegion
        {
            get
            {
                if (RegionID != null)
                {
                    return (eSchoolRegion)this.RegionID;
                }

                return null;
            }

            set { this.RegionID = (int)value; }
        }
    }

    public class InstructorRoleEntity
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public int RequiredHours { get; set; }
        public int StartingValidity { get; set; }
        public int StartingRenewal { get; set; }
        public bool AutomaticRenewal { get; set; }
        public string Language { get; set; }

        public eStartingValidity StartingValidityEnum
        {
            get { return (eStartingValidity)this.StartingValidity; }

            set { this.StartingValidity = (int)value; }
        }
    }
}
