﻿using System;

namespace FlightScheduling.DAL.CustomEntities.Outbox
{
    public class Outbox
    {
        public Outbox(
            string type,
            DateTime createdOnUtc,
            DateTime? processOnUtc,
            string content,
            string error)
        {
            Type = type;
            CreatedOnUtc = createdOnUtc;
            ProcessOnUtc = processOnUtc;
            Content = content;
            this.Error = error;
        }

        public Outbox()
        {
        }

        public int Id { get; set; }
        public string Type { get; private set; }
        public DateTime CreatedOnUtc { get; private set; }
        public DateTime? ProcessOnUtc { get; set; }
        public string Content { get; private set; }
        public string Error { get; set; }
    }
}