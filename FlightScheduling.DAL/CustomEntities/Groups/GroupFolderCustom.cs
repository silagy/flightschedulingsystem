﻿using Flight.DAL.FlightDB.Methods;
using FlightScheduling.Language;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlightScheduling.DAL.CustomEntities.Groups
{
    public class GroupFolderCustom
    {

        //Properties
        [Display(ResourceType = typeof(GroupsStrings), Name = "GroupInternalID")]
        public int GroupID { get; set; }
        [Display(ResourceType = typeof(GroupsStrings), Name = "ExpeditionID")]
        public int GroupExternalID { get; set; }
        [Display(ResourceType = typeof(GroupsStrings), Name = "NumberOfBuses")]
        public int GroupNumberOfBuses { get; set; }
        [Display(ResourceType = typeof(GroupsStrings), Name = "NumberOfParticipants")]
        public int GroupNumberofParticipants { get; set; }

        public int GroupStatus { get; set; }
        [Display(ResourceType = typeof(GroupsStrings), Name = "ExpeditionStatus")]
        public GroupStatusEnum GroupStatusEnum
        {
            get { return (GroupStatusEnum)GroupStatus; }
        }
        [Display(ResourceType = typeof(CommonStrings), Name = "CreationDate")]
        public DateTime GroupCreationDate { get; set; }
        [Display(ResourceType = typeof(CommonStrings), Name = "UpdateDate")]
        public DateTime GroupUpdateDate { get; set; }
        // File 180/170 Status
        public int GF180FileID { get; set; }
        [Display(ResourceType = typeof(CommonStrings), Name = "UpdateDate")]
        public int GF180FileStatus { get; set; }
        public eGroupFileStatus GF180FileStatusEnum
        {
            get { return (eGroupFileStatus)GF180FileStatus; }
        }

        [Display(ResourceType = typeof(GroupsStrings), Name = "ContactName")]
        public string ContactName { get; set; }
        [Display(ResourceType = typeof(GroupsStrings), Name = "ContactPhone")]
        public string ContactPhone { get; set; }

        public int FinalStatus { get; set; }

        [Display(ResourceType = typeof(GroupsStrings), Name = "ExpeditionFinalStatus")]
        public eFinalStatus FinalStatusEnum => (eFinalStatus)this.FinalStatus;

        public string GF180FileUrl { get; set; }
        public int GF180FileType { get; set; }
        public eGroupFileType GF180FileTypeEnum
        {
            get { return (eGroupFileType)GF180FileType; }
        }

        [Display(ResourceType = typeof(GroupsStrings), Name = "GF180CountyManagerApproval")]
        public bool GF180CountyManagerApproval { get; set; }

        // File 60 Status
        public int GF60FileStatusGF60FileID { get; set; }
        public int GF60FileStatus { get; set; }
        public eGroupFileStatus GF60FileStatusEnum
        {
            get { return (eGroupFileStatus)GF60FileStatus; }
        }
        public string GF60FileUrl { get; set; }
        public int GF60FileType { get; set; }
        public eGroupFileType GF60FileTypeEnum
        {
            get { return (eGroupFileType)GF60FileType; }
        }

        // File Final PDF Status
        public int GFFINPDFFileID { get; set; }
        public int GFFINPDFFileStatus { get; set; }
        public eGroupFileStatus GFFINPDFFileStatusEnum
        {
            get { return (eGroupFileStatus)GFFINPDFFileStatus; }
        }
        public string GFFINPDFFileUrl { get; set; }
        public int GFFINPDFFileType { get; set; }
        public eGroupFileType GFFINPDFFileTypeEnum
        {
            get { return (eGroupFileType)GFFINPDFFileType; }
        }

        //File Color
        public GroupColor GroupColor { get; set; }
        //Group Order
        public Expeditions Order { get; set; }
        //Group Trip
        public Trips Trip { get; set; }
        //Group School
        public Schools School { get; set; }

        [Display(ResourceType = typeof(GroupsStrings), Name = "SubGroupFilesExists")]
        public int CountSubOfFiles { get; set; }

    }

    public class GroupFolderCustomNewView
    {
        //Properties
        [Display(ResourceType = typeof(GroupsStrings), Name = "GroupInternalID")]
        public int GroupID { get; set; }
        [Display(ResourceType = typeof(GroupsStrings), Name = "ExpeditionID")]
        public int GroupExternalID { get; set; }
        [Display(ResourceType = typeof(GroupsStrings), Name = "NumberOfBuses")]
        public int GroupNumberOfBuses { get; set; }
        [Display(ResourceType = typeof(GroupsStrings), Name = "NumberOfParticipants")]
        public int GroupNumberofParticipants { get; set; }

        public int GroupStatus { get; set; }
        [Display(ResourceType = typeof(GroupsStrings), Name = "ExpeditionStatus")]
        public GroupStatusEnum GroupStatusEnum
        {
            get { return (GroupStatusEnum)GroupStatus; }
        }
        [Display(ResourceType = typeof(CommonStrings), Name = "CreationDate")]
        public DateTime GroupCreationDate { get; set; }
        [Display(ResourceType = typeof(CommonStrings), Name = "UpdateDate")]
        public DateTime GroupUpdateDate { get; set; }


        [Display(ResourceType = typeof(GroupsStrings), Name = "ContactName")]
        public string ContactName { get; set; }
        [Display(ResourceType = typeof(GroupsStrings), Name = "ContactPhone")]
        public string ContactPhone { get; set; }

        public int FinalStatus { get; set; }

        [Display(ResourceType = typeof(GroupsStrings), Name = "ExpeditionFinalStatus")]
        public eFinalStatus FinalStatusEnum => (eFinalStatus)FinalStatus;

        //Group 180 Forms
        public int NumberOf180Forms { get; set; }
        public int AdministrationFormCompletion { get; set; }
        public int AdministrationFormApproval { get; set; }
        public int InstructorFormCompletion { get; set; }
        public int InstructorFormApproval { get; set; }
        public int PlanFormCompletion { get; set; }
        public int PlanFormApproval { get; set; }

        public int AdministrationPercentageCompletion => AdministrationFormCompletion == 0 ? 0 : (int)Math.Round((Convert.ToDecimal(AdministrationFormCompletion) / Convert.ToDecimal(NumberOf180Forms)) * 100);
        public int AdministrationTotalApproval => AdministrationFormApproval == 0 ? 0 : (int)Math.Round((Convert.ToDecimal(AdministrationFormApproval) / Convert.ToDecimal(NumberOf180Forms)) * 100);
        public int InstructorPercentageCompletion => NumberOf180Forms != 0 ? (int)Math.Round((Convert.ToDecimal(InstructorFormCompletion) / Convert.ToDecimal(NumberOf180Forms)) * 100) : 0;
        public int InstructorTotalApproval => NumberOf180Forms != 0 ? (int)Math.Round((Convert.ToDecimal(InstructorFormApproval) / Convert.ToDecimal(NumberOf180Forms)) * 100) : 0;
        public int PlanPercentageCompletion => NumberOf180Forms != 0 ? (int)Math.Round((Convert.ToDecimal(PlanFormCompletion) / Convert.ToDecimal(NumberOf180Forms)) * 100) : 0;
        public int PlanTotalApproval => NumberOf180Forms != 0 ? (int)Math.Round((Convert.ToDecimal(PlanFormApproval) / Convert.ToDecimal(NumberOf180Forms)) * 100) : 0;

        // File 60 Status
        public int? GF60FormStatus { get; set; }
        public eGroupForm60Status GF60FormStatusEnum => GF60FormStatus != null ? (eGroupForm60Status)GF60FormStatus : eGroupForm60Status.New;
        public DateTime? GF60UpdateDate { get; set; }

        public int? AgencyApprovalStatus { get; set; }
        public eGroupFormStatus AgencyApprovalStatusEnum => AgencyApprovalStatus != null ? (eGroupFormStatus)AgencyApprovalStatus : eGroupFormStatus.New;

        public int? PlanApprovalStatus { get; set; }
        public eGroupFormStatus PlanApprovalStatusEnum => PlanApprovalStatus != null ? (eGroupFormStatus)PlanApprovalStatus : eGroupFormStatus.New;

        // Indication of File Form 60 has been updaloed - THIS MUST BE REMOVED WHEN THE NEW FORM 60 IS FULLY IMPLEMENTED
        public DateTime? Form60FileCreationDate { get; set; }

        public DateTime? Form60SubmitDate
        {
            get
            {
                if (AgencyApprovalStatusEnum != eGroupFormStatus.New && PlanApprovalStatusEnum != eGroupFormStatus.New && GF60FormStatusEnum != eGroupForm60Status.New)
                {
                    return GF60UpdateDate;
                }

                return null;
            }
        }

        //File Color
        public GroupColor GroupColor { get; set; }
        //Group Order
        public Expeditions Order { get; set; }
        //Group Trip
        public Trips Trip { get; set; }
        //Group School
        public Schools School { get; set; }


        [Display(ResourceType = typeof(GroupsStrings), Name = "SubGroupFilesExists")]
        public int CountSubOfFiles { get; set; }
    }
}
