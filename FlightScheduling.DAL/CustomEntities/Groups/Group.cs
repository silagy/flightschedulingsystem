﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FlightScheduling.Language;
using FlightScheduling.Language.CustomeExtentions;

namespace FlightScheduling.DAL.CustomEntities.Groups
{
    public class Group
    {
        //Properties
        public int ID { get; set; }
        public int OrderID { get; set; }
        public int GroupExternalID { get; set; }
        public int NumberOfBuses { get; set; }
        public int NumberOfParticipants { get; set; }
        public int Status { get; set; }
        public int ColorID { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime UpdateDate { get; set; }
        public string ContactName { get; set; }
        public string ContactPhone { get; set; }
        public int FinalStatus { get; set; }
        public int AgencyID { get; set; }

        //Additional Properties
        public GroupStatusEnum GroupStatus => (GroupStatusEnum)this.Status;

        public eFinalStatus GroupFinalStatus => (eFinalStatus) this.FinalStatus;
        public Expeditions Expedition { get; set; }
        public GroupColor GroupColor { get; set; }

    }

    public enum GroupStatusEnum
    {
        [Display(ResourceType = typeof(GroupsStrings), Name = "Approved")]
        [LocalizedDescription("Approved", typeof(GroupsStrings))]
        Approved = 1,
        [Display(ResourceType = typeof(GroupsStrings), Name = "Cancelled")]
        [LocalizedDescription("Cancelled", typeof(GroupsStrings))]
        Cancelled = 2
    }

    
}
