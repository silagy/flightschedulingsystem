﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FlightScheduling.Language;
using FlightScheduling.Language.CustomeExtentions;

namespace FlightScheduling.DAL.CustomEntities.Groups
{
    public class GroupColor
    {
        public int ID { get; set; }
        public string Code { get; set; }

        public ColorCodeEnum ColorCodeName
        {
            get { return (ColorCodeEnum) this.ID; }
            set { this.ID = (int)value; }
        }
    }

    public enum ColorCodeEnum
    {
        [Display(ResourceType = typeof(GroupsStrings), Name = "Green")]
        [LocalizedDescription("Green", typeof(GroupsStrings))]
        Green = 1,
        //[Display(ResourceType = typeof(GroupsStrings), Name = "Brown")]
        //[LocalizedDescription("Brown", typeof(GroupsStrings))]
        //Brown = 2,
        [Display(ResourceType = typeof(GroupsStrings), Name = "Red")]
        [LocalizedDescription("Red", typeof(GroupsStrings))]
        Red = 3,
        [Display(ResourceType = typeof(GroupsStrings), Name = "Orange")]
        [LocalizedDescription("Orange", typeof(GroupsStrings))]
        Orange = 4,
        [Display(ResourceType = typeof(GroupsStrings), Name = "Yellow")]
        [LocalizedDescription("Yellow", typeof(GroupsStrings))]
        Yellow = 5,
        [Display(ResourceType = typeof(GroupsStrings), Name = "Purpule")]
        [LocalizedDescription("Purpule", typeof(GroupsStrings))]
        Purpule = 6,
        [Display(ResourceType = typeof(GroupsStrings), Name = "White")]
        [LocalizedDescription("White", typeof(GroupsStrings))]
        White = 7,
        [Display(ResourceType = typeof(GroupsStrings), Name = "Blue")]
        [LocalizedDescription("Blue", typeof(GroupsStrings))]
        Blue = 8
    }
}
