﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlightScheduling.DAL.CustomEntities.Groups
{
    class GroupFileComments
    {

        public int ID { get; set; }
        public int FileID { get; set; }
        public string Comment { get; set; }
        public int UserID { get; set; }
        public bool IsActive { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime UpdateDate { get; set; }
    }
}
