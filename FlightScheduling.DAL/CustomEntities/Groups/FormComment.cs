﻿using FlightScheduling.Language;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlightScheduling.DAL.CustomEntities.Groups
{
    class FormComment
    {

        public int ID { get; set; }
        public int FormChapterType { get; set; }
        public int FormID { get; set; }
        [Display(ResourceType = typeof(CommonStrings), Name = "Comment")]
        [Required(ErrorMessageResourceType = typeof(ValidationStrings), ErrorMessageResourceName = "Required")]
        public string Comment { get; set; }
        public int UserID { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime UpdateDate { get; set; }
    }
}
