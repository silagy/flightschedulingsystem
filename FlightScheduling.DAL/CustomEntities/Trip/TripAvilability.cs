﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlightScheduling.DAL.CustomEntities.Trip
{
    public class TripFlightAvilability
    {
        public int ID { get; set; }
        public int FlightID { get; set; }

        public int TotalSeats { get; set; }

        public int BookedPassengers { get; set; }

        public int AvailableSeats => TotalSeats - BookedPassengers;
    }
}
