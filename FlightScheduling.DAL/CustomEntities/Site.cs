﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlightScheduling.DAL.CustomEntities
{
    public class Site
    {

        public int ID { get; set; }

        public int? ExternalID { get; set; }

        public eSiteType SiteTypeEnum { get => (eSiteType)SiteType; set => SiteType = (int)value; }

        public int SiteType { get; set; }

        public string Name { get; set; }

        public string LocalName { get; set; }

        public int CityID { get; set; }
        public string CityName { get; set; }
        public int MyProperty { get; set; }
        public string CityExternalID { get; set; }
        public int RegionID { get; set; }
        public string RegionName { get; set; }
        public int DistrictID { get; set; }
        public string DistrictName { get; set; }


        public string Address { get; set; }


        public string Limitations { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime UpdateDate { get; set; }
        public bool IsActive { get; set; }

    }
}
