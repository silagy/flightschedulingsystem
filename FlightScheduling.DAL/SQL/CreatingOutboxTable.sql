CREATE TABLE Outbox (
                        Id INT IDENTITY(1,1) PRIMARY KEY,
                        ProcessOnUtc DATETIME,
                        CreatedOnUtc DATETIME,
                        Content NVARCHAR(MAX),
                        Type NVARCHAR(500) NOT NULL,
                        Error NVARCHAR(2000)
)


insert into SystemConfiguration (PropertyName, PropertyType, PropertyValue, PropertyDescription)
values (N'InstructorPermissionRole', 1, N'23',N'Instructor permission role')