ALTER TABLE Instructors
ADD SEX INT


ALTER TABLE GroupParticipants
ADD ClassType INT

ALTER TABLE GroupParticipants
ADD MedicalType INT


USE [FlightSystemDB]
GO
/****** Object:  StoredProcedure [dbo].[InsertGroupParticipant]    Script Date: 10/02/2021 22:04:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[InsertGroupParticipant]
	@GroupID int,
	@ParticipantID nvarchar(15),
	@FirstNameHe nvarchar(100),
	@LastNameHe nvarchar(100),
	@FirstNameEn nvarchar(100),
	@LastNameEn nvarchar(100),
	@PassportNumber nvarchar(15),
	@PassportExperationDate DATETIME,
	@Birthday DATETIME,
	@SEX int,
	@Status int,
	@CreationDate DATETIME,
	@UpdateDate DATETIME,
	@UserID int,
	@ParticipantType int,
	@SchoolInstituteID INT,
	@ClassType INT,
	@Comments NVARCHAR(700)
AS
BEGIN
IF EXISTS(
			SELECT ID FROM GroupParticipants 
			WHERE GroupID = @GroupID AND 
			(ParticipantID = @ParticipantID)
			OR (PassportNumber = @PassportNumber)
		 )
	BEGIN
		UPDATE GroupParticipants
		SET  FirstNameHe = @FirstNameHe, LastNameHe = @LastNameHe, FirstNameEn = @FirstNameEn, LastNameEn = @LastNameEn, PassportNumber = @PassportNumber,
		PassportExperationDate = @PassportExperationDate, Birthday = @Birthday, SEX = @SEX, Status = @Status, UserID = @UserID, UpdateDate = @UpdateDate,
		ParticipantType = @ParticipantType, SchoolInstituteID = @SchoolInstituteID, Comments = @Comments, ClassType = @ClassType
		WHERE GroupID = @GroupID AND ParticipantID = @ParticipantID 
	END
ELSE
	BEGIN
		INSERT INTO GroupParticipants (GroupID,ParticipantID,FirstNameHe,LastNameHe,FirstNameEn,LastNameEn,PassportNumber,PassportExperationDate
										,Birthday,SEX,Status,CreationDate,UpdateDate,UserID,ParticipantType,SchoolInstituteID,Comments, ClassType)
		VALUES (@GroupID,@ParticipantID,@FirstNameHe,@LastNameHe,@FirstNameEn,@LastNameEn,@PassportNumber,@PassportExperationDate
										,@Birthday,@SEX,@Status,@CreationDate,@CreationDate,@UserID,@ParticipantType,@SchoolInstituteID, @Comments, @ClassType)
	END
END