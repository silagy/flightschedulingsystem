ALTER PROCEDURE dbo.GetGroupsDetails
	@StartDate DATETIME,
	@EndDate DATETIME,
	@OrderID int = NULL,
	@TripID int = NULL,
	@GroupStatus int = NULL,
	@SchoolID int = NULL,
	@AgencyID int = NULL
AS
;WITH GroupsCTE AS (
	SELECT 
		G.ID as GroupID,
		G.GroupExternalID,
		G.NumberOfBuses as GroupNumberOfBuses,
		G.NumberOfParticipants as GroupNumberofParticipants,
		G.Status as GroupStatus,
		G.CreationDate as GroupCreationDate,
		G.UpdateDate as GroupUpdateDate,
		GC.ID as ColorID,
		GC.Code,
		EX.ID as ExpeditionID,
		EX.Manager,
		EX.ManagerPhone,
		EX.ManagerEmail,
		EX.ManagerCell,
		TR.ID as TripID,
		TR.DepartureDate,
		TR.DestinationID,
		TR.ReturningDate,
		TR.ReturingField,
		SC.ID as SchoolID,
		SC.InstituteID,
		SC.Name
	FROM Groups as G
	INNER JOIN Expeditions as EX ON G.OrderID = EX.ID
	INNER JOIN Schools as SC ON EX.SchoolID = SC.ID
	INNER JOIN GroupColors as GC ON G.ColorID = GC.ID
	INNER JOIN Trips as TR ON Ex.TripID = TR.ID
	WHERE 
		TR.DepartureDate >= @StartDate AND TR.DepartureDate <= @EndDate
		AND (EX.AgencyID = @AgencyID OR @AgencyID IS NULL)
		AND (EX.SchoolID = @SchoolID OR @SchoolID IS NULL)
		AND (EX.ID = @OrderID OR @OrderID IS NULL)
		AND (TR.ID = @TripID OR @TripID IS NULL)
		AND (G.Status IN (@GroupStatus) OR @GroupStatus IS NULL)
),
GroupFilesCTE AS(
	SELECT 
		MainFiles.ID,
		MainFiles.GroupID,
		MainFiles.FileType,
		GFD.Name,
		GFD.FileStatus,
		GFD.Url,
		GFD.Comments,
		GFD.CreationDate
	FROM ( 
		SELECT 
			MAX(GF.ID) as ID,
			GF.GroupID,
			GF.FileType
		FROM GroupFiles as GF
		WHERE GF.GroupID IN (SELECT GroupID FROM GroupsCTE) AND GF.FileType != 5
		GROUP BY
			GF.GroupID,
			GF.FileType) as MainFiles
		INNER JOIN GroupFiles as GFD ON MainFiles.ID = GFD.ID
)

SELECT 
	G.GroupID,
	G.GroupExternalID,
	G.GroupNumberOfBuses,
	G.GroupNumberofParticipants,
	G.GroupStatus,
	G.GroupCreationDate,
	G.GroupUpdateDate,
	GF180.ID as GF180FileID,
	GF180.FileStatus as GF180FileStatus,
	GF180.Url as GF180FileUrl,
	GF180.FileType as GF180FileType,
	GF60.ID as GF60FileID,
	GF60.FileStatus as GF60FileStatus,
	GF60.Url as GF60FileUrl,
	GF60.FileType as GF60FileType,
	GFINIPDF.ID as GFINIPDFFileID,
	GFINIPDF.FileStatus as GFINIPDFFileStatus,
	GFINIPDF.Url as GFINIPDFFileUrl,
	GFINIPDF.FileType as GFINIPDFFileType,
	GFFINPDF.ID as GFFINPDFFileID,
	GFFINPDF.FileStatus as GFFINPDFFileStatus,
	GFFINPDF.Url as GFFINPDFFileUrl,
	GFFINPDF.FileType as GFFINPDFFileType,
	G.ColorID as ID,
	G.Code,
	G.ExpeditionID as ID,
	G.Manager,
	G.ManagerPhone,
	G.ManagerEmail,
	G.ManagerCell,
	G.TripID as ID,
	G.DepartureDate,
	G.DestinationID,
	G.ReturningDate,
	G.ReturingField,
	G.SchoolID as ID,
	G.InstituteID,
	G.Name
FROM GroupsCTE as G
LEFT JOIN GroupFilesCTE as GF180 ON G.GroupID = GF180.GroupID AND GF180.FileType = 1
LEFT JOIN GroupFilesCTE as GF60 ON G.GroupID = GF60.GroupID AND GF60.FileType = 2
LEFT JOIN GroupFilesCTE as GFINIPDF ON G.GroupID = GFINIPDF.GroupID AND GFINIPDF.FileType = 3
LEFT JOIN GroupFilesCTE as GFFINPDF ON G.GroupID = GFFINPDF.GroupID AND GFFINPDF.FileType = 4

--DECLARE @StartDate DATETIME = '2016-01-01'
--DECLARE @EndDate DATETIME = '2017-12-31'
--EXEC dbo.GetGroupsDetails @StartDate,@EndDate

