CREATE TABLE GroupColors (
	ID int IDENTITY(1,1) PRIMARY KEY,
	Code nvarchar(10) NOT NULL
)

CREATE TABLE Groups (
	ID int IDENTITY(1,1) PRIMARY KEY,
	OrderID int NOT NULL,
	GroupExternalID int NOT NULL,
	NumberOfBuses int,
	NumberOfParticipants int,
	[Status] int NOT NULL,
	ColorID int,
	CreationDate DATETIME,
	UpdateDate DATETIME
	CONSTRAINT GroupsExpeditionsIDUQFKF1 FOREIGN KEY (OrderID)
		REFERENCES Expeditions(ID)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION,
	CONSTRAINT GroupsColorsIDUQFKF1 FOREIGN KEY (ColorID)
		REFERENCES GroupColors(ID)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
)

INSERT INTO GroupColors (Code) VALUES (N'#00A0B0')
INSERT INTO GroupColors (Code) VALUES (N'#6A4A3C')
INSERT INTO GroupColors (Code) VALUES (N'#CC333F')
INSERT INTO GroupColors (Code) VALUES (N'#EB6841')
INSERT INTO GroupColors (Code) VALUES (N'#EDC951')

SELECT * FROM GroupColors



INSERT INTO Groups (OrderID,GroupExternalID,NumberOfBuses,NumberOfParticipatns,Status,ColorID,CreationDate,UpdateDate)
VALUES (@OrderID,@GroupExternalID,@NumberOfBuses,@NumberOfParticipatns,@Status,@ColorID,@CreationDate,@UpdateDate)


SELECT 
	G.ID,g.OrderID, 
	G.GroupExternalID, 
	G.NumberOfBuses, 
	G.NumberOfParticipants,
	G.Status,
	G.ColorID,
	G.CreationDate,
	G.UpdateDate  
FROM Groups as G
WHERE G.OrderID = @OrderID

SELECT * FROM Expeditions