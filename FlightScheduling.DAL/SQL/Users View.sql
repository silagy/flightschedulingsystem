CREATE VIEW UsersView AS
(
	SELECT 
		U.ID,
		U.Firstname,
		U.Email,
		U.Password,
		U.Phone,
		U.UserType,
		U.IsActive,
		U.IsActivate,
		U.AgencyID,
		U.SchoolID,
		U.CreationDate,
		U.Region,
		U.UserRole,
		U.PermissionRoleID
	FROM Users as U
	WHERE U.IsActive = 1 AND U.IsActivate = 1
)