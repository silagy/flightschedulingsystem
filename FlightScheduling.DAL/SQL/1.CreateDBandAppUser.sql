DECLARE @DBName nvarchar(200)
DECLARE @UserName nvarchar(200)
DECLARE @Password nvarchar(200)
DECLARE @SQLCmd_CreateAppUser nvarchar(max)
DECLARE @SQLCmd_CreateDB nvarchar(max)

SET @DBName = N'FlightSchedulingDB'
SET @UserName = N'admin'
SET @Password = N'123'




SET @SQLCmd_CreateAppUser = N'
CREATE LOGIN ['+@UserName+']
WITH PASSWORD = N'''+@Password+''',
DEFAULT_DATABASE=['+@DBName+'], 
DEFAULT_LANGUAGE=[us_english], 
CHECK_EXPIRATION=OFF, CHECK_POLICY=OFF

ALTER LOGIN ['+@UserName+'] ENABLE

USE ['+@DBName+']
CREATE USER ['+@UserName+'] FOR LOGIN ['+@UserName+'];

EXEC sp_addrolemember ''db_owner'', '''+@UserName+'''
'

SET @SQLCmd_CreateDB = N'CREATE DATABASE ['+@DBName+']'

exec(@SQLCmd_CreateDB)
exec(@SQLCmd_CreateAppUser)



