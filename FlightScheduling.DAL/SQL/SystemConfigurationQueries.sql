SELECT * FROM SystemConfiguration

SET IDENTITY_INSERT SystemConfiguration ON;  
INSERT INTO SystemConfiguration (PropertyID, PropertyName,PropertyType,PropertyDescription,PropertyValue)
VALUES (3,'TripsFilterStartDate',2,'This control the start date defualt value within the trips screen','01/01/2017')

INSERT INTO SystemConfiguration (PropertyID,PropertyName,PropertyType,PropertyDescription,PropertyValue)
VALUES (4,'TripsFilterEndDate',2,'This control the end date defualt value within the trips screen','31/12/2018')

INSERT INTO SystemConfiguration (PropertyID,PropertyName,PropertyType,PropertyDescription,PropertyValue)
VALUES (5,'FlightsFilterStartDate',2,'This control the start date defualt value within the flights screen','01/01/2017')

INSERT INTO SystemConfiguration (PropertyID,PropertyName,PropertyType,PropertyDescription,PropertyValue)
VALUES (6,'FlightsFilterEndDate',2,'This control the end date defualt value within the flights screen','31/12/2018')

INSERT INTO SystemConfiguration (PropertyID,PropertyName,PropertyType,PropertyDescription,PropertyValue)
VALUES (7,'GroupsFilterStartDate',2,'This control the start date defualt value within the groups screen','01/01/2017')

INSERT INTO SystemConfiguration (PropertyID,PropertyName,PropertyType,PropertyDescription,PropertyValue)
VALUES (8,'GroupsFilterEndDate',2,'This control the end date defualt value within the groups screen','31/12/2018')

SET IDENTITY_INSERT SystemConfiguration OFF; 


-- NEW SET OF CONFIGURAION
SELECT * FROM SystemConfiguration

SET IDENTITY_INSERT SystemConfiguration ON;  
INSERT INTO SystemConfiguration (PropertyID, PropertyName,PropertyType,PropertyDescription,PropertyValue)
VALUES (9,'TripAvilabilityBusesTreshold',1,'','5')

INSERT INTO SystemConfiguration (PropertyID, PropertyName,PropertyType,PropertyDescription,PropertyValue)
VALUES (10,'TripAvilabilityPassengerTreshold',1,'','40')

SET IDENTITY_INSERT SystemConfiguration OFF; 


-- NEW SET OF CONFIGURATIONS
SET IDENTITY_INSERT SystemConfiguration ON;  
INSERT INTO SystemConfiguration (PropertyID,PropertyName,PropertyType,PropertyDescription,PropertyValue)
VALUES (11,'TrainingsFilterStartDate',2,'This control the start date defualt value within the groups trainings','01/01/2017')

INSERT INTO SystemConfiguration (PropertyID,PropertyName,PropertyType,PropertyDescription,PropertyValue)
VALUES (12,'TrainingsFilterEndDate',2,'This control the end date defualt value within the trainings screen','31/12/2018')
SET IDENTITY_INSERT SystemConfiguration OFF; 


-- NEW PROPERTIES FOR INSTRUCTORS EXPIRATION LIMITATION
SET IDENTITY_INSERT SystemConfiguration ON;  
INSERT INTO SystemConfiguration (PropertyID,PropertyName,PropertyType,PropertyDescription,PropertyValue)
VALUES (13,'RolesToIncludeSpecialValidationRule',1,'This define the roles that the system will check special training','5')

INSERT INTO SystemConfiguration (PropertyID,PropertyName,PropertyType,PropertyDescription,PropertyValue)
VALUES (14,'NumberOfRequiredHoursForSpecialTraining',1,'This control the end date defualt value within the trainings screen','8')
SET IDENTITY_INSERT SystemConfiguration OFF; 