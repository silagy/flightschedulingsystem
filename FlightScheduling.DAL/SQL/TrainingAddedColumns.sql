﻿ALTER TABLE Trainings
ADD RegionID INT

CREATE TABLE TrainingTypes (
	ID int IDENTITY(1,1) NOT NULL PRIMARY KEY,
	Name nvarchar(250) NOT NULL,
	CreationDate DATETIME NOT NULL,
	UpdateDate DATETIME NOT NULL
)

ALTER TABLE Trainings
    ADD TrainingTypeID INTEGER,
    FOREIGN KEY(TrainingTypeID) REFERENCES TrainingTypes(ID);


INSERT INTO TrainingTypes(Name,CreationDate,UpdateDate)
VALUES (N'למידת עמיתים',GETDATE(),GETDATE())