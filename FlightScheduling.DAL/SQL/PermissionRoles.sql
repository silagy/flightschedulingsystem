
CREATE TABLE PermissionRole (
	ID INT IDENTITY(1,1) PRIMARY KEY,
	Name NVARCHAR(250) NOT NULL,
	CreationDate DATETIME NOT NULL,
	UpdateDate DATETIME
)

CREATE TABLE PermissionBits (
	ID INT IDENTITY(1,1) PRIMARY KEY,
	PermissionRoleID INT,
	PermissionBit INT NOT NULL,
	CreationDate DATETIME NOT NULL,
	UpdateDate DATETIME
	CONSTRAINT PermissionRoleFKF1 FOREIGN KEY (PermissionRoleID)
		REFERENCES PermissionRole(ID)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
)

ALTER TABLE Users
ADD PermissionRoleID INT
CONSTRAINT PermissionRoleUsersFKF1 FOREIGN KEY (PermissionRoleID)
		REFERENCES PermissionRole(ID)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION

SET IDENTITY_INSERT PermissionRole ON;  
INSERT INTO PermissionRole (ID,Name,CreationDate,UpdateDate)
VALUES (1, N'Admin',GETDATE(),GETDATE())
SET IDENTITY_INSERT PermissionRole OFF; 

INSERT INTO PermissionBits (PermissionRoleID,PermissionBit,CreationDate,UpdateDate)
VALUES (1,1,GETDATE(),GETDATE())

INSERT INTO PermissionBits (PermissionRoleID,PermissionBit,CreationDate,UpdateDate)
VALUES (1,2,GETDATE(),GETDATE())

INSERT INTO PermissionBits (PermissionRoleID,PermissionBit,CreationDate,UpdateDate)
VALUES (1,3,GETDATE(),GETDATE())

INSERT INTO PermissionBits (PermissionRoleID,PermissionBit,CreationDate,UpdateDate)
VALUES (1,4,GETDATE(),GETDATE())

INSERT INTO PermissionBits (PermissionRoleID,PermissionBit,CreationDate,UpdateDate)
VALUES (1,5,GETDATE(),GETDATE())

INSERT INTO PermissionBits (PermissionRoleID,PermissionBit,CreationDate,UpdateDate)
VALUES (1,6,GETDATE(),GETDATE())

INSERT INTO PermissionBits (PermissionRoleID,PermissionBit,CreationDate,UpdateDate)
VALUES (1,7,GETDATE(),GETDATE())

INSERT INTO PermissionBits (PermissionRoleID,PermissionBit,CreationDate,UpdateDate)
VALUES (1,8,GETDATE(),GETDATE())

INSERT INTO PermissionBits (PermissionRoleID,PermissionBit,CreationDate,UpdateDate)
VALUES (1,9,GETDATE(),GETDATE())

INSERT INTO PermissionBits (PermissionRoleID,PermissionBit,CreationDate,UpdateDate)
VALUES (1,10,GETDATE(),GETDATE())

INSERT INTO PermissionBits (PermissionRoleID,PermissionBit,CreationDate,UpdateDate)
VALUES (1,11,GETDATE(),GETDATE())

INSERT INTO PermissionBits (PermissionRoleID,PermissionBit,CreationDate,UpdateDate)
VALUES (1,12,GETDATE(),GETDATE())

INSERT INTO PermissionBits (PermissionRoleID,PermissionBit,CreationDate,UpdateDate)
VALUES (1,13,GETDATE(),GETDATE())


UPDATE Users SET PermissionRoleID = 1 WHERE ID = 0

SELECT 
	PR.ID,
	PR.Name,
	PR.CreationDate,
	PR.UpdateDate,
	PB.ID as PermissionBitID,
	PB.PermissionBit
FROM Users as US
INNER JOIN PermissionRole as PR ON US.PermissionRoleID = PR.ID
INNER JOIN PermissionBits as PB ON PR.ID = PB.PermissionRoleID
WHERE US.ID = 0
