BEGIN TRAN
CREATE TABLE InstructorRoles
(
	ID int IDENTITY(1,1) PRIMARY KEY,
	Name nvarchar(50) NOT NULL,
	RequiredHours int NOT NULL, --The hours required to renew the role qualification
	ExpirationDaysRule int NOT NULL,
	StartingValidity int NOT NULL, -- defining if the role validity start from the beggining of the year or from completion date
	StartingRenewal int, -- definign when the role can start to renew the is license from the expiration date
	AutomaticRenewal bit NOT NULL,
	[Language] nvarchar(5) NOT NULL
)

CREATE TABLE Instructors
(
	ID int IDENTITY(1,1) PRIMARY KEY,
	InstructorID nvarchar(15) NOT NULL,
	FirstNameHE nvarchar(50) NOT NULL,
	LastNameHE nvarchar(50) NOT NULL,
	FirstNameEN nvarchar(50) NOT NULL,
	LastNameEN nvarchar(50) NOT NULL,
	PassportID nvarchar(15) NOT NULL,
	[Address] nvarchar(150) NOT NULL,
	City nvarchar(50) NOT NULL,
	ZipCode nvarchar(10),
	HomePhone nvarchar(15),
	CellPhone nvarchar(15),
	Email nvarchar(50) NOT NULL UNIQUE,
	RoleID int NOT NULL,
	CreationDate DATETIME NOT NULL,
	UpdateDate DATETIME NOT NULL,
	UserID int,
	ValidityStartDate DATE,
	ValidityEndTime DATE,
	CONSTRAINT InstructorsUserIDUQFKF1 FOREIGN KEY (UserID)
		REFERENCES Users(ID)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION,
	CONSTRAINT InstructorsRoleIDUQFKF1 FOREIGN KEY (RoleID)
		REFERENCES InstructorRoles(ID)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
)

CREATE TABLE InstructorRecordsHistory
(
	ID int IDENTITY(1,1) PRIMARY KEY,
	InstructorID int NOT NULL,
	ValidityStartDate DATE NOT NULL,
	ValidityEndDate DATE NOT NULL,
	CreationDate DATETIME NOT NULL,
	UserID int NOT NULL,
	CONSTRAINT InstRecordsHisUserIDUQFKF1 FOREIGN KEY (UserID)
		REFERENCES Users(ID)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION,
	CONSTRAINT InstRecordsHisInstructorIDUQFKF1 FOREIGN KEY (InstructorID)
		REFERENCES Instructors(ID)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION,
)

CREATE TABLE Trainings
(
	ID int IDENTITY(1,1) PRIMARY KEY,
	Name nvarchar(150) NOT NULL,
	TrainingDate DATETIME NOT NULL,
	TrainingLocation int NOT NULL,
	TrainingHours int NOT NULL,
	TrainingType int NOT NULL,
	RoleID int NOT NULL,
	CreatorID int NOT NULL,
	CONSTRAINT creatorUsertrainingIDUQFKF1 FOREIGN KEY (CreatorID)
		REFERENCES Users(ID)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION,
	CONSTRAINT TrainingRoleIDUQFKF1 FOREIGN KEY (RoleID)
		REFERENCES InstructorRoles(ID)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
)

CREATE TABLE InstructorTrainings
(
	ID int IDENTITY(1,1) NOT NULL,
	TrainingID int NOT NULL,
	InstructorID int NOT NULL,
	CountToValidity bit NOT NULL,
	CONSTRAINT InstTrainingIDIDUQFKF1 FOREIGN KEY (TrainingID)
		REFERENCES Trainings(ID)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION,
	CONSTRAINT InstTrainingInstructorIDUQFKF1 FOREIGN KEY (InstructorID)
		REFERENCES Instructors(ID)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
)

CREATE TABLE InstructorRemarks (
ID INT IDENTITY(1,1) PRIMARY KEY,
UserID int NOT NULL,
InstructorID int NOT NULL,
Notes nvarchar(MAX),
UpdateDate DATETIME,
CreationDate DATETIME NOT NULL,
CONSTRAINT InstremarksIDUQFKF1 FOREIGN KEY (UserID)
		REFERENCES Users(ID)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION,
CONSTRAINT InstructorRemarksInstIDUQFKF2 FOREIGN KEY (InstructorID)
		REFERENCES Instructors(ID)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
)

CREATE TABLE InstructorFiles (
	ID int IDENTITY(1,1) PRIMARY KEY,
	InstructorID int NOT NULL,
	Name nvarchar(350) NOT NULL,
	Url nvarchar(2000) NOT NULL,
	Comments nvarchar(2000),
	CreationDate DATETIME NOT NULL,
	UserID int NOT NULL
	CONSTRAINT InstructorFilesInstIDUQFKF1 FOREIGN KEY (InstructorID)
		REFERENCES Instructors(ID)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION,
	CONSTRAINT InstructorUsersUserIDUQFKF1 FOREIGN KEY (UserID)
		REFERENCES Users(ID)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
)

CREATE TABLE InstructorGroups
(
	ID int IDENTITY(1,1) PRIMARY KEY,
	GroupID int NOT NULL,
	InstructorID int NOT NULL,
		CONSTRAINT InstructorgroupsIDUQFKF1 FOREIGN KEY (GroupID)
		REFERENCES Groups(ID)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION,
	CONSTRAINT instructorsGroupInstIDUQFKF1 FOREIGN KEY (InstructorID)
		REFERENCES Instructors(ID)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
)

COMMIT


ALTER TABLE Instructors ALTER COLUMN Email nvarchar(50) NULL

ALTER TABLE Instructors drop constraint UQ__Instruct__A9D10534C8F240F4

declare @table_name nvarchar(256)  
declare @col_name nvarchar(256)  
declare @Command  nvarchar(1000)  

set @table_name = N'Instructors'
set @col_name = N'Email'

select @Command = 'ALTER TABLE ' + @table_name + ' drop constraint ' + d.name
    from sys.tables t 
    join sys.indexes d on d.object_id = t.object_id  and d.type=2 and d.is_unique=1
    join sys.index_columns ic on d.index_id=ic.index_id and ic.object_id=t.object_id
    join sys.columns c on ic.column_id = c.column_id  and c.object_id=t.object_id
    where t.name = @table_name and c.name=@col_name

print @Command


--INSERT INTO Instructors (InstructorID,FirstNameHE,LastNameHE,FirstNameEN,LastNameEN,PassportID,[Address],City,ZipCode,HomePhone,CellPhone,RoleID,CreationDate,UpdateDate,UserID)
--VALUES (@InstructorID,@FirstNameHE,@LastNameHE,@FirstNameEN,@LastNameEN,@PassportID,@Address,@City,@ZipCode,@HomePhone,@CellPhone,@RoleID,@CreationDate,@UpdateDate,@UserID)


--UPDATE Instructors
--SET FirstNameHE = @FirstNameHE, LastNameHE = @LastNameHE, FirstNameEN = @FirstNameEN, LastNameEN = @LastNameEN,
--PassportID = @PassportID, [Address] = @Address, City = @City, ZipCode = @ZipCode, HomePhone = @HomePhone,
--CellPhone = @CellPhone, Email = @Email, UpdateDate = @UpdateDate, UserID = @UserID
--WHERE ID = @ID

---- Queries for Instructor Roles --

INSERT INTO InstructorRoles (Name,RequiredHours,ExpirationDaysRule,StartingValidity,StartingRenewal,AutomaticRenewal,[Language])
VALUES (@Name,@RequiredHours,@ExpirationDaysRule,@StartingValidity,@StartingRenewal,@AutomaticRenewal,@Language)

UPDATE InstructorRoles
SET Name = @Name, RequiredHours = @RequiredHours, ExpirationDaysRule = @ExpirationDaysRule, StartingValidity = @StartingValidity,
StartingRenewal = @StartingRenewal, AutomaticRenewal = @AutomaticRenewal
WHERE ID = @ID

SELECT * FROM InstructorRoles WHERE [Language] = @Language

SELECT * FROM InstructorRoles WHERE ID = @ID

DELETE FROM InstructorRoles WHERE ID = 2

SELECT COUNT(ID) FROM InstructorTrainings WHERE TrainingID = @TrainingID

DELETE FROM Trainings WHERE ID = @ID

SELECT 
	                                            TR.*,
	                                            IR.ID,
	                                            IR.Name,
	                                            IR.RequiredHours,
	                                            IR.StartingValidity,
	                                            IR.StartingRenewal,
	                                            IR.AutomaticRenewal,
	                                            IR.Language
                                            FROM Trainings as TR
                                            INNER JOIN InstructorRoles as IR ON TR.RoleID = IR.ID
											ORDER BY TR.TrainingDate DESC


SELECT 
	IT.*,
	TR.*,
	INS.*,
	ISTR.*
FROM InstructorTrainings as IT
INNER JOIN Trainings as TR ON IT.TrainingID = TR.ID
INNER JOIN Instructors as INS ON IT.InstructorID = INS.ID
INNER JOIN InstructorRoles as ISTR ON INS.RoleID = ISTR.ID
WHERE IT.TrainingID = @TrainingID

INSERT INTO InstructorTrainings (TrainingID,InstructorID,CountToValidity) VALUES (@TrainingID,@InstructorID,@CountToValidity)

INSERT INTO InstructorRecordsHistory (InstructorID,ValidityStartDate,ValidityEndDate,CreationDate,UserID)
VALUES (@InstructorID, @ValidityStartDate, @ValidityEndDate, @CreationDate, @UserID)

SELECT * FROM Instructors

SELECT ID FROM Instructors WHERE InstructorID = @InstructorID

SELECT * FROM Instructors WHERE RoleID = @RoleID

UPDATE Instructors
SET UpdateDate = @UpdateDate, ValidityStartDate = @ValidityStartDate, ValidityEndTime = @ValidityEndTime
WHERE ID = @ID


SELECT COUNT(TR.TrainingHours) as TotalHours 
FROM InstructorTrainings as IT
INNER JOIN Trainings as TR ON IT.TrainingID = TR.ID
INNER JOIN Instructors as IST ON IT.InstructorID = IST.ID
WHERE TR.TrainingDate >= @Date
AND IT.CountToValidity = 1
AND IT.InstructorID = @InstructorID


--SELECT 
--	IST.*,
--	IR.*
--FROM Instructors as IST
--INNER JOIN InstructorRoles as IR ON IST.RoleID = IR.ID
--WHERE IST.ID = @ID

SELECT * FROM InstructorRoles

DELETE FROM InstructorTrainings WHERE ID = 4

SELECT * FROM Instructors
WHERE RoleID = @RoleID AND ID NOT IN (SELECT InstructorID FROM InstructorTrainings WHERE TrainingID = @TrainingID)

DECLARE @RoleID int = NULL
DECLARE @InstructorID nvarchar(150) = '4124567'
DECLARE @LastName nvarchar(50) = NULL

SELECT 
	IST.*,
	IR.*
FROM Instructors as IST
INNER JOIN InstructorRoles as IR ON IST.RoleID = IR.ID
WHERE 
	(IST.RoleID = @RoleID OR @RoleID IS NULL)
	AND (IST.InstructorID = @InstructorID OR @InstructorID IS NULL)
	AND (IST.LastNameHE = @LastName OR @LastName IS NULL)


	INSERT INTO InstructorFiles (InstructorID,Name,Url,Comments,CreationDate,UserID)
	VALUES (@InstructorID,@Name,@Url,@Comments,@CreationDate,@UserID)


	SELECT IG.*, INST.*
	FROM InstructorGroups as IG
	INNER JOIN Instructors as INST ON IG.InstructorID = INST.ID
	WHERE IG.GroupID = @GroupID

	SELECT IG.ID 
	FROM InstructorGroups as IG
	INNER JOIN Instructors as INS ON IG.InstructorID = INS.ID
	WHERE INS.InstructorID = @InstructorID

	SELECT * FROM Instructors WHERE InstructorID = @InstructorID

	SELECT * FROM InstructorGroups


	DECLARE @GroupID int = 1
	DECLARE @InstructorID int = 7
	DECLARE @StartDate DATETIME = '2017-01-01'
	DECLARE @EndDate DATETIME = '2017-01-08'

	SELECT ISG.GroupID
	FROM InstructorGroups as ISG
	INNER JOIN Instructors as INST ON ISG.InstructorID = ISG.InstructorID
	INNER JOIN Groups as GP ON ISG.GroupID = GP.ID
	INNER JOIN Expeditions as EX ON GP.OrderID = Ex.ID
	INNER JOIN Trips as TR ON EX.TripID = TR.ID
	WHERE 
		INST.InstructorID = @InstructorID
		AND ISG.GroupID != @GroupID
		AND 
		(TR.DepartureDate >= @StartDate AND TR.DepartureDate <= @EndDate
		OR TR.ReturningDate >= @StartDate AND TR.ReturningDate <= @EndDate)



SELECT TR.*
FROM Groups as GP
INNER JOIN Expeditions as EX ON GP.OrderID = Ex.ID
INNER JOIN Trips as TR ON EX.TripID = TR.ID
WHERE GP.ID = @ID


UPDATE Instructors
SET UpdateDate = @UpdateDate, ValidityStartDate = @ValidityStartDate, ValidityEndTime = @ValidityEndTime, CertificateID = @CertificateID
WHERE ID = @ID


SELECT IG.*, INST.*, ISR.*
FROM InstructorGroups as IG
INNER JOIN Instructors as INST ON IG.InstructorID = INST.ID
INNER JOIN InstructorRoles as ISR ON INST.RoleID = ISR.ID
WHERE IG.GroupID = @GroupID

SELECT (SELECT COUNT(ID) as AssignedParticipants FROM GroupParticipants WHERE GroupID = @GroupID) + (SELECT COUNT(ID) as AssignedInstructors
FROM InstructorGroups as IG
WHERE IG.GroupID = @GroupID) as AssignedParticipants

FROM
(SELECT 
	COUNT(ID) as AssignedParticipants
FROM GroupParticipants as GP
WHERE GroupID = 7
) as GP
LEFT JOIN 
(SELECT COUNT(ID) as AssignedInstructors
FROM InstructorGroups as IG
WHERE IG.GroupID = 7
GROUP BY IG.GroupID) as IG ON IG.GroupID = GP.GroupID


SELECT ID FROM Instructors WHERE InstructorID = @InstructorID AND ValidityEndTime >= GETDATE()

UPDATE Instructors
SET ImageURL = @ImageURL
WHERE ID = @ID