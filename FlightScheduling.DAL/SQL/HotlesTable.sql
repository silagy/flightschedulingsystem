﻿BEGIN TRAN

CREATE TABLE Regions
(
	ID INT IDENTITY(1,1) PRIMARY KEY,
	Name nvarchar(50) NOT NULL,
	CreationDate DATETIME NOT NULL,
	UpdateDate DATETIME NOT NULL,
	IsActive bit DEFAULT 1 NOT NULL
)

CREATE TABLE Districts
(
	ID INT IDENTITY(1,1) PRIMARY KEY,
	Name nvarchar(50) NOT NULL,
	CreationDate DATETIME NOT NULL,
	UpdateDate DATETIME NOT NULL,
	IsActive bit DEFAULT 1 NOT NULL
)

CREATE TABLE Cities
(
	ID INT IDENTITY(1,1) PRIMARY KEY,
	Name nvarchar(50) NOT NULL,
	RegionID INT NOT NULL,
	DistrictID INT NOT NULL,
	CreationDate DATETIME NOT NULL,
	UpdateDate DATETIME NOT NULL,
	IsActive bit DEFAULT 1 NOT NULL,
	ExternalID INT NOT NULL,
	CONSTRAINT CityRegionIDUQFKF1 FOREIGN KEY (RegionID)
	REFERENCES Regions(ID)
	ON DELETE NO ACTION
	ON UPDATE NO ACTION,
	CONSTRAINT CityDistrictIDUQFKF1 FOREIGN KEY (DistrictID)
	REFERENCES Districts(ID)
	ON DELETE NO ACTION
	ON UPDATE NO ACTION,
)

CREATE TABLE Sites
(
	ID int IDENTITY(1,1) PRIMARY KEY,
	ExternalID INT,
	SiteType INT NOT NULL,
	Name nvarchar(150) NOT NULL,
	LocalName nvarchar(150),
	CityID INT NOT NULL,
	LocalRegion nvarchar(100),
	Address nvarchar(200),
	Limitations nvarchar(1500),
	CreationDate DATETIME NOT NULL,
	UpdateDate DATETIME NOT NULL,
	IsActive bit DEFAULT 1 NOT NULL,
	CONSTRAINT CitySiteIDUQFKF1 FOREIGN KEY (CityID)
	REFERENCES Cities(ID)
	ON DELETE NO ACTION
	ON UPDATE NO ACTION
)

CREATE TABLE GroupFormsSixty (
	ID int IDENTITY(1,1) PRIMARY KEY,
	GroupID INT NOT NULL,
	CreationDate DATETIME NOT NULL,
	UpdateDate DATETIME NOT NULL,
	FormStatus INT NOT NULL,
	FormApprovalUserID INT,
	AgencyApprovalStatus INT NOT NULL,
	AgencyApprovalUserID INT,
	PlanApprovalStatus INT NOT NULL,
	PlanApprovalUserID INT,
	RequestChangeNumberOfBuses INT,
	CONSTRAINT GroupFormGroupIDUQFKF1 FOREIGN KEY (GroupID)
		REFERENCES Cities(ID)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION,
	CONSTRAINT GroupFormApprovalUserIDUQFKF1 FOREIGN KEY (FormApprovalUserID)
		REFERENCES Users(ID)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION,
	CONSTRAINT GroupAgencyApprovalUserIDUQFKF1 FOREIGN KEY (AgencyApprovalUserID)
		REFERENCES Users(ID)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION,
	CONSTRAINT GroupPlanApprovalUserIDUQFKF1 FOREIGN KEY (PlanApprovalUserID)
		REFERENCES Users(ID)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
)

CREATE TABLE GroupActualFlights (
	ID int IDENTITY(1,1) PRIMARY KEY,
	Direction INT NOT NULL,
	FlightNumber nvarchar(50) NOT NULL,
	Airline nvarchar(50) NOT NULL,
	Departure DATETIME NOT NULL,
	Arrival DATETIME NOT NULL,
	NumberOfPassengers INT NOT NULL,
	DestinationLocationID INT NOT NULL,
	ArrivalLocationID INT NOT NULL,
	GroupID INT NOT NULL,
	CreationDate DATETIME NOT NULL,
	UpdateDate DATETIME NOT NULL,
	CONSTRAINT GroupFlightActialGroupIDUQFKF1 FOREIGN KEY (GroupID)
		REFERENCES Groups(ID)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
)

CREATE TABLE GroupTripPlan (
	ID INT IDENTITY(1,1) PRIMARY KEY,
	ActivityType INT NOT NULL,
	Name nvarchar(150),
	SiteID INT,
	StartTime DATETIME NOT NULL,
	EndTime DATETIME NOT NULL,
	Comments nvarchar(2000),
	SchoolName nvarchar(200),
	SchoolAddress  nvarchar(200),
	ContactName nvarchar(50),
	ContactEmail nvarchar(50),
	ContactCell nvarchar(50),
	ContactPhone nvarchar(50),
	GroupID INT NOT NULL,
	CityID INT NOT NULL,
	CreationDate DATETIME NOT NULL,
	UpdateDate DATETIME NOT NULL,
	CONSTRAINT GroupTripPlanIDUQFKF1 FOREIGN KEY (GroupID)
		REFERENCES Groups(ID)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION,
	CONSTRAINT GroupSitesIDUQFKF1 FOREIGN KEY (SiteID)
		REFERENCES Sites(ID)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION,
	CONSTRAINT CityActivityIDUQFKF1 FOREIGN KEY (CityID)
	REFERENCES Cities(ID)
	ON DELETE NO ACTION
	ON UPDATE NO ACTION
)

ALTER VIEW CitiesView AS 
SELECT 
	C.ID as CityID,
	C.Name as CityName,
	C.ExternalID as CityExternalID,
	C.RegionID,
	R.Name as RegionName,
	D.ID as DistrictID,
	D.Name as DistrictName
FROM Cities as C
INNER JOIN Districts as D ON C.DistrictID = D.ID
INNER JOIN Regions as R ON C.RegionID = R.ID
WHERE C.IsActive = 1


--********** LOGISTICS TABLES ALTER

ALTER TABLE GroupParticipants
ADD Speciality nvarchar(100)

ALTER TABLE GroupParticipants
ADD Comments nvarchar(2500) NULL

ALTER TABLE GroupParticipants
ADD ContactNumber nvarchar(30) NULL

ALTER TABLE InstructorGroups
ADD ContactNumber nvarchar(50)

ALTER TABLE NotificationTempates
ADD UserPermissionRole INT


-- *************  NOT TO INCLUDE ***********
ALTER TABLE GroupFormsSixty
ADD AgencyApprovalUserID INT
CONSTRAINT GroupAgencyApprovalUserIDUQFKF1 FOREIGN KEY (AgencyApprovalUserID)
		REFERENCES Users(ID)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION


ALTER TABLE GroupFormsSixty
ADD FormApprovalUserID INT
CONSTRAINT GroupFormApprovalUserIDUQFKF1 FOREIGN KEY (FormApprovalUserID)
		REFERENCES Users(ID)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION

ALTER TABLE GroupFormsSixty
ADD PlanApprovalUserID INT
CONSTRAINT GroupPlanApprovalUserIDUQFKF1 FOREIGN KEY (PlanApprovalUserID)
		REFERENCES Users(ID)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION

ALTER TABLE GroupFormsSixty
ADD AgencyApprovalStatus INT

ALTER TABLE GroupFormsSixty
ADD PlanApprovalStatus INT

ALTER TABLE GroupActualFlights
ADD DestinationLocationID INT

ALTER TABLE GroupActualFlights
ADD ArrivalLocationID INT



-- ********* ADD SYSTEM CONFIGURATION *************
SELECT * FROM SystemConfiguration

SET IDENTITY_INSERT SystemConfiguration ON

INSERT INTO SystemConfiguration (PropertyID, PropertyName, PropertyType, PropertyValue, PropertyDescription)
VALUES (15, N'GroupManagerRoleID', 1, N'3', N'This define the default role when creating a group manager user')

SET IDENTITY_INSERT SystemConfiguration OFF

--***** QUERIES **********



COMMIT
SELECT * FROM CitiesView
INSERT INTO Cities (ExternalID,Name,RegionID,DistrictID,CreationDate,UpdateDate,IsActive) VALUES (ExternalID,N'',RegionID,DistrictID,GETDATE(),GETDATE(),1)



INSERT INTO Sites (ExternalID, SiteType, Name, LocalName, CityID, Address, Limitations, CreationDate, UpdateDate, IsActive)
                                           VALUES (@ExternalID, @SiteType, @Name, @LocalName, @CityID, @Address, @Limitations, @CreationDate, @UpdateDate, @IsActive)

										   UPDATE Sites SET Name = @Name, LocalName = @LocalName, CityID = @CityID, Address = @Address, Limitations = @Limitations, UpdateDate = @UpdateDate, ExternalID = @ExternalID, SiteType = @SiteType
	                                            WHERE ID = @ID


-- Get Form 60
SELECT  
	GF.*,
	G.GroupExternalID
FROM GroupFormsSixty GF
INNER JOIN Groups as G ON GF.GroupID = G.ID	
WHERE GF.GroupID = @GroupID

INSERT INTO GroupFormsSixty (CreationDate, UpdateDate, GroupID, FormStatus)
VALUES (2, @CreationDate, @UpdateDate, @GroupID, @FormStatus)

-- Get Group Details

--DECLARE @GroupID INT = 1647
SELECT 
	G.ID,
	G.OrderID,
	G.ColorID,
	G.NumberOfParticipants as PlannedParticipants,
	G.NumberOfBuses,
	TR.ID as TripID,
	TR.DepartureDate
FROM Groups as G
INNER JOIN Expeditions as EX ON G.OrderID = EX.ID
INNER JOIN Trips as TR ON EX.TripID = TR.ID
WHERE 
	G.ID = @GroupID

DECLARE @GroupID INT = 1647

SELECT 
	SC.Name,
	SC.ID as SchoolID,
	SC.InstituteID,
	SG.ID as SchoolGroupID,
	SG.GroupID,
	SG.IsPrimary,
	SC.IsAdministrativeInstitude,
	SC.Stream,
	SC.IsSpecialEducation
FROM SubGroupSchools as SG
INNER JOIN Schools as SC ON SG.SchoolID = SC.ID
WHERE GroupID = @GroupID

SELECT *
FROM GroupActualFlights
WHERE GroupID = @GroupID


SELECT 
	TR.DepartureDate,
	TR.ReturningDate
FROM Groups as G
INNER JOIN Expeditions as O ON G.OrderID = O.ID
INNER JOIN Trips as TR ON O.TripID = TR.ID
WHERE G.ID = @GroupID

INSERT INTO GroupActualFlights (Direction, FlightNumber, Airline, Departure, Arrival, NumberOfPassengers, GroupID, CreationDate, UpdateDate)
VALUES (@Direction, @FlightNumber, @Airline, @Departure, @Arrival, @NumberOfPassengers, @GroupID, @CreationDate, @UpdateDate)
SELECT CAST(SCOPE_IDENTITY() as int)

UPDATE GroupActualFlights SET
	Direction = @Direction,
	FlightNumber = @FlightNumber,
	Airline = @Airline,
	Departure = @Departure,
	Arrival = @Arrival,
	NumberOfPassengers = @NumberOfPassengers,
	UpdateDate = @UpdateDate
WHERE ID = @FlightID

SELECT * FROM GroupActualFlights WHERE ID = @FlightID

SELECT 
	TR.ID as TripID,
	TR.DepartureDate,
	TR.DestinationID,
	TR.ReturningDate,
	TR.ReturingField
FROM Groups as G
INNER JOIN Expeditions as ORD ON ORD.ID = G.OrderID
INNER JOIN Trips as TR ON TR.ID = ORD.TripID
WHERE G.ID = @GroupID

DELETE FROM GroupActualFlights WHERE ID = @ID

SELECT 
	Direction,
	SUM(NumberOfPassengers) as NumberOfPassengers
FROM GroupActualFlights
WHERE GroupID = @GroupID
GROUP BY Direction

SELECT 
	S.ID,
	S.ExternalID,
	S.SiteType,
	S.Name,
	S.LocalName,
	S.Address,
	S.Limitations,
	S.CreationDate,
	S.UpdateDate,
	S.IsActive,
	C.CityID,
	C.CityName,
	C.CityExternalID,
	C.RegionID,
	C.RegionName,
	C.DistrictID,
	C.DistrictName
FROM Sites as S
INNER JOIN CitiesView as C ON S.CityID = C.CityID
WHERE S.IsActive = 1

SELECT 
	GP.ID,
	GP.ActivityType,
	GP.Name,
	GP.StartTime,
	GP.EndTime,
	GP.Comments,
	GP.GroupID,
	GP.CreationDate,
	GP.UpdateDate,
	S.ID as SiteID,
	S.ExternalID,
	S.SiteType,
	S.Name as SiteName,
	s.LocalName,
	S.LocalRegion,
	S.Limitations,
	S.CreationDate,
	S.UpdateDate,
	C.CityID,
	C.CityName,
	C.CityExternalID,
	C.RegionID,
	C.RegionName,
	C.DistrictID,
	C.DistrictName
FROM GroupTripPlan as GP
INNER JOIN Sites as S ON GP.SiteID = S.ID
INNER JOIN CitiesView as C ON S.CityID = C.CityID 
WHERE 
	StartTime BETWEEN @StartDate 
	AND @EndDate 
	AND ActivityType = @ActivityType

SELECT *
	--MAX(FL.ArrivalTime) as ArrivalTime 
FROM TripsFlightAssociations as TF
INNER JOIN Flights as FL ON TF.FlightID = FL.ID
WHERE 
	FL.TakeOffFromIsrael = 1 AND
	FL.IsActive = 1 AND
	TF.TripID = 3485 --5@TripID

SELECT * FROM CitiesView WHERE CityName like N'%וורוצלב%'

INSERT INTO Sites (ExternalID, SiteType, Name, LocalName, CityID, Address, Limitations, CreationDate, UpdateDate, IsActive)
                                           VALUES ('123', 5, N'ורשה', 'Warsqa', 15, 'dsdf', '', GETDATE(), GETDATE(), 1)

INSERT INTO Sites (ExternalID, SiteType, Name, LocalName, CityID, Address, Limitations, CreationDate, UpdateDate, IsActive)
                                           VALUES ('123', 5, N'קראקוב', 'Warsqa', 59, 'dsdf', '', GETDATE(), GETDATE(), 1)

INSERT INTO Sites (ExternalID, SiteType, Name, LocalName, CityID, Address, Limitations, CreationDate, UpdateDate, IsActive)
                                           VALUES ('123', 5, N'קטוביץ''', 'Warsqa', 54, 'dsdf', '', GETDATE(), GETDATE(), 1)

INSERT INTO Sites (ExternalID, SiteType, Name, LocalName, CityID, Address, Limitations, CreationDate, UpdateDate, IsActive)
                                           VALUES ('123', 5, N'לודז''', 'Warsqa', 28, 'dsdf', '', GETDATE(), GETDATE(), 1)

INSERT INTO Sites (ExternalID, SiteType, Name, LocalName, CityID, Address, Limitations, CreationDate, UpdateDate, IsActive)
                                           VALUES ('123', 5, N'ז''שוב', 'Warsqa', 21, 'dsdf', '', GETDATE(), GETDATE(), 1)

INSERT INTO Sites (ExternalID, SiteType, Name, LocalName, CityID, Address, Limitations, CreationDate, UpdateDate, IsActive)
                                           VALUES ('123', 5, N'לובלין', 'Warsqa', 27, 'dsdf', '', GETDATE(), GETDATE(), 1)


SELECT * FROM Sites WHERE SiteType = @SiteType AND Name = @Name

SELECT * FROM Sites WHERE SiteType = 5 AND Name = N'ורשה'


INSERT INTO GroupTripPlan (ActivityType, Name, SiteID, StartTime, Duration, EndTime, Comments, GroupID, CreationDate, UpdateDate)
VALUES (@ActivityType, @Name, @SiteID, @StartTime, @Duration, @EndTime, @Comments, @GroupID, @CreationDate, @UpdateDate)


SELECT 
	S.ID as SiteID,
	S.ExternalID,
	S.SiteType,
	S.Name as SiteName,
	s.LocalName,
	S.LocalRegion,
	S.Limitations,
	S.CreationDate,
	S.UpdateDate,
	C.CityID,
	C.CityName,
	C.CityExternalID,
	C.RegionID,
	C.RegionName,
	C.DistrictID,
	C.DistrictName 
FROM Sites as S
INNER JOIN CitiesView as C ON S.CityID = C.CityID
WHERE 
	S.IsActive = 1
	AND C.RegionID = @RegionID OR @RegionID IS NULL
	AND C.CityID = @CityID OR @CityID IS NULL

SELECT CityID FROM Sites WHERE ID = @ID

	SELECT 
		GP.ID,
		GP.ActivityType,
		GP.Name,
		GP.StartTime,
		GP.EndTime,
		GP.Comments,
		GP.GroupID,
		GP.CreationDate,
		GP.UpdateDate,
		C.CityID,
		C.CityName,
		C.CityExternalID,
		C.RegionID,
		C.RegionName,
		C.DistrictID,
		C.DistrictName,
		S.ID as SiteID,
		S.ExternalID,
		S.SiteType,
		S.Name as SiteName,
		s.LocalName,
		S.LocalRegion,
		S.Limitations,
		S.CreationDate,
		S.UpdateDate,
	FROM GroupTripPlan as GP
	INNER JOIN CitiesView as C ON GP.CityID = C.CityID 
	LEFT JOIN Sites as S ON GP.SiteID = S.ID
	WHERE 
		GP.ID = @ID

	SELECT * FROM GroupTripPlan

	SELECT * FROM CitiesView WHERE CityID = @CityID



INSERT INTO GroupTripPlan (ActivityType, Name, SiteID, CityID, StartTime, EndTime, Comments, GroupID, CreationDate, UpdateDate, ContactName, ContactEmail, ContactPhone, ContactCell, SchoolName, SchoolAddress, )
VALUES (@ActivityType, @Name, @SiteID, @CityID, @StartTime, @EndTime, @Comments, @GroupID, @CreationDate, @UpdateDate, @ContactName, @ContactEmail, @ContactPhone, @ContactCell, @SchoolName, @SchoolAddress)

SELECT * FROM GroupTripPlan

SELECT 
	                                                    GP.ID,
	                                                    GP.ActivityType,
	                                                    GP.Name,
	                                                    GP.StartTime,
	                                                    GP.EndTime,
	                                                    GP.Comments,
	                                                    GP.GroupID,
	                                                    GP.CreationDate,
	                                                    GP.UpdateDate,
														GP.SchoolName,
														GP.SchoolAddress,
														GP.ContactName,
														GP.ContactEmail,
														GP.ContactCell,
														GP.ContactPhone,
	                                                    S.ID as SiteID,
	                                                    S.ExternalID,
	                                                    S.SiteType,
	                                                    S.Name as SiteName,
	                                                    s.LocalName,
	                                                    S.LocalRegion,
	                                                    S.Limitations,
	                                                    S.CreationDate,
	                                                    S.UpdateDate,
	                                                    C.CityID,
	                                                    C.CityName,
	                                                    C.CityExternalID,
	                                                    C.RegionID,
	                                                    C.RegionName,
	                                                    C.DistrictID,
	                                                    C.DistrictName
                                                    FROM GroupTripPlan as GP
                                                    INNER JOIN CitiesView as C ON GP.CityID = C.CityID 
                                                    LEFT JOIN Sites as S ON GP.SiteID = S.ID
                                                    WHERE 
	                                                    (GP.StartTime BETWEEN @StartDate 
														AND @EndDate) 
	                                                    AND (GP.ActivityType = @ActivityType OR @ActivityType IS NULL)
														AND Gp.GroupID = @GroupID


UPDATE GroupFormsSixty
SET PlanApprovalStatus = @Status
WHERE GroupID = @GroupID

UPDATE GroupFormsSixty
SET AgencyApprovalStatus = 1, PlanApprovalStatus = 1