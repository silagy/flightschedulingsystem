CREATE TABLE GroupColors (
	ID int IDENTITY(1,1) PRIMARY KEY,
	Code nvarchar(10) NOT NULL
)

CREATE TABLE Groups (
	ID int IDENTITY(1,1) PRIMARY KEY,
	OrderID int NOT NULL,
	GroupExternalID int NOT NULL,
	NumberOfBuses int,
	NumberOfParticipants int,
	[Status] int NOT NULL,
	ColorID int,
	CreationDate DATETIME,
	UpdateDate DATETIME,
	CONSTRAINT GroupsExpeditionsIDUQFKF1 FOREIGN KEY (OrderID)
		REFERENCES Expeditions(ID)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION,
	CONSTRAINT GroupsColorsIDUQFKF1 FOREIGN KEY (ColorID)
		REFERENCES GroupColors(ID)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
)

CREATE INDEX idx_ExternalID ON Groups(GroupExternalID)

CREATE TABLE GroupFiles (
	ID int IDENTITY(1,1) PRIMARY KEY,
	GroupID int NOT NULL,
	Name nvarchar(350) NOT NULL,
	FileType int NOT NULL,
	FileStatus int NOT NULL,
	Url nvarchar(2000) NOT NULL,
	Comments nvarchar(2000),
	CreationDate DATETIME NOT NULL,
	UserID int NOT NULL
	CONSTRAINT GroupsFilesIDUQFKF1 FOREIGN KEY (GroupID)
		REFERENCES Groups(ID)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION,
	CONSTRAINT GroupsUsersIDUQFKF1 FOREIGN KEY (UserID)
		REFERENCES Users(ID)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
)

CREATE TABLE GroupRemarks (
ID INT IDENTITY(1,1) PRIMARY KEY,
UserID int NOT NULL,
GroupID int NOT NULL,
Notes nvarchar(MAX),
UpdateDate DATETIME,
CreationDate DATETIME NOT NULL,
CONSTRAINT GroupsremarksIDUQFKF1 FOREIGN KEY (UserID)
		REFERENCES Users(ID)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION,
CONSTRAINT GroupsremarksIDUQFKF2 FOREIGN KEY (GroupID)
		REFERENCES Groups(ID)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
)

CREATE TABLE GroupParticipants (
ID INT IDENTITY(1,1) PRIMARY KEY,
GroupID int NOT NULL,
ParticipantID nvarchar(15) NOT NULL,
FirstNameHe nvarchar(100),
LastNameHe nvarchar(100),
FirstNameEn nvarchar(100),
LastNameEn nvarchar(100),
PassportNumber nvarchar(15) NOT NULL,
PassportExperationDate DATETIME,
Birthday DATETIME,
SEX int,
Status int,
CreationDate DATETIME,
UpdateDate DATETIME,
UserID int,
CONSTRAINT GroupsParticipantsIDUQFKF1 FOREIGN KEY (UserID)
		REFERENCES Users(ID)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION,
CONSTRAINT GroupsParticipantsIDUQFKF2 FOREIGN KEY (GroupID)
		REFERENCES Groups(ID)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
)

ALTER TABLE GroupParticipants
ADD ParticipantType int

--  ADDING NEW FIELDS TO SCHOOL OBJECT --
ALTER TABLE Schools
ADD City nvarchar(300)

ALTER TABLE Schools
ADD Region int

ALTER TABLE Instructors
ADD CertificateID nvarchar(100)

ALTER TABLE InstructorRoles
Add IsManager BIT

ALTER TABLE Trainings
Add IsCompleted BIT

ALTER TABLE Instructors
Add IsActive bit DEFAULT(1)

UPDATE Instructors SET IsActive = 1

ALTER TABLE Instructors
ADD ImageURL nvarchar(150)

ALTER TABLE Instructors
ADD OrganizationalStatus int

UPDATE Instructors
SET OrganizationalStatus = 1

ALTER TABLE Groups
ADD ContactName nvarchar(150)

ALTER TABLE Groups
ADD ContactPhone nvarchar(150)

ALTER TABLE Groups
ADD FinalStatus int

UPDATE Groups SET FinalStatus = 1 

ALTER TABLE Schools DROP CONSTRAINT DF__Schools__IsDelet__756D6ECB

ALTER TABLE Schools
DROP COLUMN IsDeleted bit NOT NULl DEFAULT 0

DELETE InstructorGroups

ALTER TABLE InstructorGroups
ADD RoleID int NOT NULL
CONSTRAINT InstructorGroupRoleIDUQFKF1 FOREIGN KEY (RoleID)
		REFERENCES InstructorRoles(ID)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION

ALTER TABLE InstructorRoles
Add AllowChangingRole BIT NOT NULL DEFAULT 0


SELECT * FROM InstructorGroups

SELECT * FROM InstructorRoles



INSERT INTO GroupColors (Code) VALUES (N'#00A0B0')
INSERT INTO GroupColors (Code) VALUES (N'#6A4A3C')
INSERT INTO GroupColors (Code) VALUES (N'#CC333F')
INSERT INTO GroupColors (Code) VALUES (N'#EB6841')
INSERT INTO GroupColors (Code) VALUES (N'#EDC951')
INSERT INTO GroupColors (Code) VALUES (N'#4935A3')
INSERT INTO GroupColors (Code) VALUES (N'#EFEFD0')
INSERT INTO GroupColors (Code) VALUES (N'#004E89')


--SELECT * FROM GroupColors



--INSERT INTO Groups (OrderID,GroupExternalID,NumberOfBuses,NumberOfParticipatns,Status,ColorID,CreationDate,UpdateDate)
--VALUES (@OrderID,@GroupExternalID,@NumberOfBuses,@NumberOfParticipatns,@Status,@ColorID,@CreationDate,@UpdateDate)


--SELECT 
--	G.ID,g.OrderID, 
--	G.GroupExternalID, 
--	G.NumberOfBuses, 
--	G.NumberOfParticipants,
--	G.Status,
--	G.ColorID,
--	G.CreationDate,
--	G.UpdateDate  
--FROM Groups as G
--WHERE G.OrderID = @OrderID

--SELECT * FROM Expeditions


--SELECT 
--	GF.ID,
--	GF.GroupID,
--	GF.Name,
--	GF.FileType,
--	GF.FileStatus,
--	GF.Url,
--	GF.Comments,
--	GF.CreationDate,
--	GF.UserID
--FROM GroupFiles as GF
--WHERE GF.GroupID = 1
--ORDER BY GF.FileType ASC, GF.CreationDate ASC


--INSERT INTO GroupFiles (GroupID,Name,FileType,FileStatus,Url,Comments,CreationDate,UserID)
--VALUES (@GroupID,@Name,@FileType,@FileStatus,@Url,@Comments,@CreationDate,@UserID)

SELECT * FROM InstructorTrainings
DELETE InstructorTrainings WHERE TrainingID = 15

UPDATE Trainings
SET IsCompleted = 0
WHERE ID = 15



SELECT *
FROM Users
WHERE 
(UserType = @RoleID OR @RoleID IS NULL)
AND (Email = @Email OR @Email IS NULL)
AND (Firstname like '%' + @Name + '%' OR Lastname like '%' + @Name + '%' OR @Name IS NULL)

UPDATE Groups SET FinalStatus = @FinalStatus WHERE ID = @ID

SELECT * FROM Groups WHERE ID = @ID


SELECT InstructorID as ID, FirstNameHE + ' ' + LastNameHE as Name FROM Instructors
WHERE 
InstructorID like '%' + @SearchTerm + '%'
OR FirstNameHE like '%' + @SearchTerm + '%'
OR LastNameHE like '%' + @SearchTerm + '%'


SELECT IG.*, INST.*, ISR.*
                                                        FROM InstructorGroups as IG
                                                        INNER JOIN Instructors as INST ON IG.InstructorID = INST.ID
                                                        INNER JOIN InstructorRoles as ISR ON IG.RoleID = ISR.ID
                                                        WHERE IG.GroupID = @GroupID AND INST.IsActive = 1



SELECT INS.*, IR.*
FROM Instructors as INS
INNER JOIN InstructorRoles as IR ON INS.RoleID = IR.ID
WHERE InstructorID = @InstructorID

UPDATE Instructors SET IsActive = 1 WHERE ID = 6

DELETE InstructorTrainings WHERE TrainingID = 8

DELETE InstructorTrainings WHERE ID = @ID


SELECT * FROM Schools as SC
WHERE
	SC.IsActive = 1
	AND (
	(SC.InstituteID like '%' + @InstituteID + '%' OR @InstituteID IS NULL)
	AND (SC.Name like '%' + @Name + '%' OR @Name IS NULL)
	AND (SC.Email like '%' + @Email + '%' OR @Email IS NULL))
	