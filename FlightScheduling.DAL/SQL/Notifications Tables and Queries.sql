CREATE TABLE NotificationTempates (
	ID int IDENTITY(1,1) PRIMARY KEY,
	Name nvarchar(100) NOT NULL,
	TriggerType int NOT NULL,
	[Subject] nvarchar(2000) NOT NULL,
	[Message] nvarchar(4000) NOT NULL,
	SchedulingType int NOT NULL,
	IsActive bit DEFAULT 1 NOT NULL,
	UserType int NOT NULL
)

--CREATE TABLE NotificationAudience (
--	ID int IDENTITY(1,1) PRIMARY KEY,
--	NotificationID int NOT NULL,
--	UserType int NOT NULL,
--	CONSTRAINT NotificationIDUQFKF1 FOREIGN KEY (NotificationID)
--		REFERENCES NotificationTempates(ID)
--		ON DELETE NO ACTION
--		ON UPDATE NO ACTION
--)


CREATE TABLE Notifications (
	ID int IDENTITY(1,1) PRIMARY KEY,
	NotificationTempateID int NOT NULL,
	UserID int NOT NULL,
	EmailAddress nvarchar(30) NOT NULL,
	NotificationStatus int NOT NULL,
	MessageSubject nvarchar(2000) NOT NULL,
	MessageBody nvarchar(max) NOT NULL,
	SendDate DATETIME,
	Remark nvarchar(2500)
	CONSTRAINT NotificationTemplateIDUQFKF1 FOREIGN KEY (NotificationTempateID)
		REFERENCES NotificationTempates(ID)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION,
	CONSTRAINT NotificationusersIDUQFKF1 FOREIGN KEY (UserID)
		REFERENCES Users(ID)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
)

--- SELECT ALL NOTIFICATION TEMPLATES ---
SELECT 
	NT.ID,
	NT.Name,
	NT.TriggerType,
	NT.Subject,
	NT.Message,
	NT.SchedulingType,
	NT.IsActive,
	NT.UserType
FROM NotificationTempates as NT
WHERE NT.ID = @ID

INSERT INTO NotificationTempates (Name,TriggerType,Subject,Message,SchedulingType,IsActive,UserType)
VALUES (@Name, @TriggerType, @Subject, @Message, @SchedulingType, @IsActive, @UserType)

UPDATE NotificationTempates
SET Name = @Name, TriggerType = @TriggerType, Subject = @Subject, Message = @Message, SchedulingType = @SchedulingType, IsActive = @IsActive, UserType = @UserType
WHERE ID = @ID

SELECT * FROM NotificationTempates WHERE TriggerType = @TriggerType