ALTER Procedure FlightCountPerTrip
AS
BEGIN
SET NOCOUNT ON

;
WITH FlightCount
AS (SELECT
		FL.ID,
		FL.NumberOfPassengers,
		SUM(EF.NumberOfPassengers) AS AssignedPassengers
	FROM ExpeditionsFlights AS EF
	INNER JOIN Flights AS FL
		ON EF.FlightID = FL.ID
	GROUP BY	FL.ID,
				FL.NumberOfPassengers)

SELECT
	TFA.TripID,
	TR.DepartureDate,
	TR.DestinationID AS TripDestination,
	TFA.FlightID,
	FL.StartDate,
	FL.DestinationID AS FlightDestination,
	FL.AirlineName,
	FL.FlightNumber,
	FC.NumberOfPassengers,
	FC.AssignedPassengers,
	FC.NumberOfPassengers - FC.AssignedPassengers AS 'AvailableSeats'
FROM TripsFlightAssociations AS TFA
INNER JOIN Trips AS TR
	ON TFA.TripID = TR.ID
INNER JOIN Flights AS FL
	ON TFA.FlightID = FL.ID
INNER JOIN FlightCount AS FC
	ON FL.ID = FC.ID
WHERE FL.TakeOffFromIsrael = 1
ORDER BY TFA.TripID ASC
END



SELECT
	GP.ID,
	GP.GroupID,
	GP.ParticipantID,
	GP.FirstNameHe,
	GP.LastNameHe,
	GP.FirstNameEn,
	GP.LastNameEn,
	GP.PassportNumber,
	GP.PassportExperationDate,
	GP.Birthday,
	GP.SEX,
	GP.Status,
	GP.CreationDate,
	GP.UpdateDate,
	US.ID,
	US.Firstname,
	US.Lastname
FROM GroupParticipants AS GP
INNER JOIN Users AS US
	ON GP.UserID = US.ID
WHERE GP.ID = @ID