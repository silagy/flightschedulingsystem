-- Group File Comments --
CREATE TABLE GroupFileComments (
ID int identity(1,1) NOT NULL,
FileID int NOT NULL,
Comment nvarchar(4000) NOT NULL,
UserID int NOT NULL,
IsActive bit NOT NULL DEFAULT (1),
CreationDate datetime NOT NULL DEFAULT(GETDATE()),
UpdateDate datetime,
PRIMARY KEY (ID),
CONSTRAINT FileFilesFKF1 FOREIGN KEY (FileID)
		REFERENCES GroupFiles(ID)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION,
CONSTRAINT FileUsersFKF1 FOREIGN KEY (UserID)
		REFERENCES Users(ID)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
)


INSERT INTO GroupFileComments (FileID,Comment,UserID,IsActive,CreationDate,UpdateDate)
VALUES (@FileID,@Comment,@UserID,@IsActive,@CreationDate,@UpdateDate)

SELECT 
	GFC.ID,
	GFC.Comment,
	GFC.CreationDate,
	GFC.UpdateDate,
	GFC.FileID,
	GF.Name,
	GF.FileType,
	GF.FileStatus,
	GF.CountyManagerApproval,
	US.Firstname,
	US.Lastname,
	US.Email
FROM GroupFileComments as GFC
INNER JOIN GroupFiles as GF ON GFC.FileID = GF.ID
INNER JOIN Users as US ON GFC.UserID = US.ID
WHERE GFC.FileID = @FileID
ORDER BY GFC.UpdateDate DESC


SELECT GR.ID FROM GroupRemarks as GR WHERE GR.SchoolID = @SchoolID AND GR.GroupID = @GroupID

