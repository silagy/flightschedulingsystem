ALTER TABLE Notifications
ADD CallToAction nvarchar(250)

ALTER TABLE Notifications
ADD CreationDate DATETIME

ALTER TABLE Notifications
ADD IsRead bit