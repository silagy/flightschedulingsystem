ALTER TABLE Groups
ADD AgencyID INT
CONSTRAINT AgencyIDGroupsAgency NULL,
CONSTRAINT AgencyIDGroupsAgency FOREIGN KEY (AgencyID)
REFERENCES Agencies (ID)

BEGIN TRAN
UPDATE Groups
    SET AgencyID = (
		SELECT EX.AgencyID
		FROM Expeditions as EX
		WHERE Groups.OrderID = EX.ID 
    )
ROLLBACK

SELECT GP.AgencyID, EX.AgencyID FROM Groups as GP
INNER JOIN Expeditions as EX ON GP.OrderID = EX.ID

UPDATE Groups SET AgencyID = @AgencyID WHERE ID = @ID

