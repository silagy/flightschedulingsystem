USE [FlightSystemDB]
GO

/****** Object:  StoredProcedure [dbo].[FlightCountPerTrip]    Script Date: 19/04/2017 23:43:19 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


ALTER Procedure [dbo].[FlightCountPerTrip]
(
	@StartDate DATETIME,
	@EndDate DATETIME
)
AS
BEGIN
SET NOCOUNT ON

;WITH FlightCount AS (
SELECT 
	FL.ID,
	FL.NumberOfPassengers,
	SUM(EF.NumberOfPassengers) AS AssignedPassengers
	FROM Flights as FL
	LEFT JOIN ExpeditionsFlights AS EF ON FL.ID = EF.FlightID
	GROUP BY
		FL.ID,
		FL.NumberOfPassengers)

SELECT 
	TFA.TripID,
	TR.DepartureDate,
	TR.DestinationID AS TripDestination,
	TFA.FlightID, 
	FL.StartDate,
	FL.DestinationID AS FlightDestination,
	FL.AirlineName,
	FL.FlightNumber,
	FC.NumberOfPassengers,
	CASE 
	WHEN FC.AssignedPassengers IS NULL THEN 0
	ELSE FC.AssignedPassengers
	END as 'AssignedPassengers',
	FC.NumberOfPassengers - 
	CASE 
	WHEN FC.AssignedPassengers IS NULL THEN 0
	ELSE FC.AssignedPassengers
	END as 'AvailableSeats'
FROM TripsFlightAssociations AS TFA
INNER JOIN Trips AS TR ON TFA.TripID = TR.ID
INNER JOIN Flights AS FL ON TFA.FlightID = FL.ID
INNER JOIN FlightCount AS FC ON FL.ID = FC.ID
WHERE 
FL.TakeOffFromIsrael = 1
AND TR.DepartureDate >= @StartDate AND Tr.DepartureDate <= @EndDate
ORDER BY TR.DepartureDate ASC
END

GO