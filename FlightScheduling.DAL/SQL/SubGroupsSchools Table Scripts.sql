CREATE TABLE SubGroupSchools
(
	ID int IDENTITY(1,1) PRIMARY KEY,
	GroupID int NOT NULL,
	SchoolID int NOT NULL,
	IsPrimary bit NOT NULL DEFAULT 0
	CONSTRAINT SubGroupsGroupIDUQFKF1 FOREIGN KEY (GroupID)
		REFERENCES Groups(ID)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION,
	CONSTRAINT SubGroupsSchoolIDUQFKF1 FOREIGN KEY (SchoolID)
		REFERENCES Schools(ID)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
)

-- Enter the current school as primary to the table
INSERT INTO SubGroupSchools (GroupID, SchoolID, IsPrimary)
SELECT 
	GP.ID,
	EX.SchoolID,
	1
FROM Groups as GP
INNER JOIN Expeditions as EX ON GP.OrderID = EX.ID


-- Add SchoolID to group notes --

ALTER TABLE GroupRemarks
ADD SchoolID int 
CONSTRAINT GroupRemarksSchoolIDUQFKF1 FOREIGN KEY (SchoolID)
REFERENCES Schools(ID)
ON DELETE NO ACTION
ON UPDATE NO ACTION

BEGIN TRAN

UPDATE GR
SET GR.SchoolID = GP.SchoolID
FROM GroupRemarks GR
INNER JOIN SubGroupSchools as GP ON GR.GroupID = GP.GroupID
WHERE GP.IsPrimary = 1

COMMIT
