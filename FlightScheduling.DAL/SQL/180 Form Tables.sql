BEGIN TRAN
--Adding User sub Type field
ALTER TABLE Users
ADD UserRole INT NULL

-- Adding required fields for School Entity

-- Adding special education
ALTER TABLE Schools
ADD IsSpecialEducation bit

-- Adding Bellonging to stream
ALTER TABLE Schools
ADD Stream int

-- Adding Is Administrative Institure
ALTER TABLE Schools
ADD IsAdministrativeInstitude bit

-- Adding 180 tables

CREATE TABLE GroupForms (
	ID int IDENTITY(1,1) PRIMARY KEY,
	GroupSchoolID INT,
	FormType int NOT NULL,
	CreationDate DATETIME NOT NULL,
	UpdateDate DATETIME NOT NULL,
	CONSTRAINT GroupSchoolFormIDUQFKF1 FOREIGN KEY (GroupSchoolID)
		REFERENCES SubGroupSchools(ID)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION,
)

CREATE TABLE AdministrationForm180 (
	ID int IDENTITY(1,1) PRIMARY KEY,
	GroupFormID INT NOT NULL,
	ContractUrl nvarchar(2000),
	SignatureUrl nvarchar(2000),
	FormStatus INT NOT NULL,
	CreationDate DATETIME NOT NULL,
	UpdateDate DATETIME NOT NULL,
	UserID INT NOT NULL,
	CONSTRAINT GroupSchoolFormAdminIDUQFKF1 FOREIGN KEY (GroupFormID)
		REFERENCES GroupForms(ID)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION,
	CONSTRAINT GroupFormAdminUserIDUQFKF1 FOREIGN KEY (UserID)
		REFERENCES Users(ID)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
)

CREATE TABLE GroupInstructorsForm (
	ID int IDENTITY(1,1) PRIMARY KEY,
	GroupFormID INT NOT NULL,
	FormStatus INT NOT NULL,
	CreationDate DATETIME NOT NULL,
	UpdateDate DATETIME NOT NULL,
	CONSTRAINT GroupSchoolFormginstructorsIDUQFKF1 FOREIGN KEY (GroupFormID)
		REFERENCES GroupForms(ID)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
)

-- Adding commnets to the instructor group table

ALTER TABLE InstructorGroups
ADD Comments nvarchar(4000)

-- Adding indication that the assign instrctor is acting as manager
ALTER TABLE InstructorGroups
ADD IsGroupManager bit DEFAULT 0


-- Adding tables for the group plan form

-- Adding administration tables

CREATE TABLE GroupPlanSubjects (
	ID int IDENTITY(1,1) PRIMARY KEY,
	[Name] nvarchar(100) NOT NULL,
	HoursRequired INT NOT NULL,
	CreationDate DATETIME NOT NULL,
	UpdateDate DATETIME NOT NULL,
)

CREATE TABLE GroupPlanForm (
	ID int IDENTITY(1,1) PRIMARY KEY,
	GroupFormID INT NOT NULL,
	FormStatus INT NOT NULL,
	CreationDate DATETIME NOT NULL,
	UpdateDate DATETIME NOT NULL,
	CONSTRAINT GroupPlanFormIDUQFKF1 FOREIGN KEY (GroupFormID)
		REFERENCES GroupForms(ID)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
)

CREATE TABLE GroupPlanDetails (
	ID int IDENTITY(1,1) PRIMARY KEY,
	GroupPlanID INT,
	Instructor nvarchar(100) NOT NULL,
	PlanHours INT NOT NULL,
	PlanDate DATETIME NOT NULL,
	PlanLocation nvarchar(100),
	CreationDate DATETIME NOT NULL,
	UpdateDate DATETIME NOT NULL,
	CONSTRAINT GroupDetailPlanFormIDUQFKF1 FOREIGN KEY (GroupPlanID)
		REFERENCES GroupPlanForm(ID)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
)


--CREATE COMMENTS ON FORM
CREATE TABLE FormsComments (
	ID int IDENTITY(1,1) PRIMARY KEY,
	FormChapterType INT NOT NULL, -- This should hold the from chapter
	FormID INT NOT NULL,
	Comment nvarchar(max) NOT NULL,
	UserID INT NOT NULL,
	CreationDate DATETIME NOT NULL,
	UpdateDate DATETIME NOT NULL,
	CONSTRAINT CommentsUserIDUQFKF1 FOREIGN KEY (UserID)
		REFERENCES Users(ID)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
)

COMMIT


-- ADDING COLUMN TO GROUP PLAN --
ALTER TABLE GroupPlanSubjects
ADD IsActive bit DEFAULT 0

-- ADDING ANOTHER COLUMN TO GROUP PLAN DETAILS
ALTER TABLE GroupPlanDetails
ADD GroupPlanSubjectID INT
CONSTRAINT GroupDetailPlanSubjectIDUQFKF1 FOREIGN KEY (GroupPlanSubjectID)
		REFERENCES GroupPlanSubjects(ID)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION

ALTER TABLE GroupPlanDetails
ADD PlanName nvarchar(100)

ALTER TABLE GroupPlanForm
ADD EdicationPlanDetails nvarchar(max)

ALTER TABLE [dbo].[GroupForms]
ADD GroupID INT
CONSTRAINT GroupIDUQFKF1 FOREIGN KEY (GroupID)
		REFERENCES Groups(ID)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION

ALTER TABLE [dbo].[GroupForms]
DROP CONSTRAINT GroupIDUQFKF1

UPDATE Schools SET IsAdministrativeInstitude = 0 WHERE IsAdministrativeInstitude IS NULL