USE [FlightSystemDB]
GO
/****** Object:  StoredProcedure [dbo].[InsertGroupParticipant]    Script Date: 29/06/2018 09:36:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[InsertGroupParticipant]
	@GroupID int,
	@ParticipantID nvarchar(15),
	@FirstNameHe nvarchar(100),
	@LastNameHe nvarchar(100),
	@FirstNameEn nvarchar(100),
	@LastNameEn nvarchar(100),
	@PassportNumber nvarchar(15),
	@PassportExperationDate DATETIME,
	@Birthday DATETIME,
	@SEX int,
	@Status int,
	@CreationDate DATETIME,
	@UpdateDate DATETIME,
	@UserID int,
	@ParticipantType int,
	@SchoolInstituteID int
AS
BEGIN
IF EXISTS(
			SELECT ID FROM GroupParticipants 
			WHERE GroupID = @GroupID AND 
			(ParticipantID = @ParticipantID)
			OR (PassportNumber = @PassportNumber)
		 )
	BEGIN
		UPDATE GroupParticipants
		SET  FirstNameHe = @FirstNameHe, LastNameHe = @LastNameHe, FirstNameEn = @FirstNameEn, LastNameEn = @LastNameEn, PassportNumber = @PassportNumber,
		PassportExperationDate = @PassportExperationDate, Birthday = @Birthday, SEX = @SEX, Status = @Status, UserID = @UserID, UpdateDate = @UpdateDate,
		ParticipantType = @ParticipantType, SchoolInstituteID = @SchoolInstituteID
		WHERE GroupID = @GroupID AND ParticipantID = @ParticipantID 
	END
ELSE
	BEGIN
		INSERT INTO GroupParticipants (GroupID,ParticipantID,FirstNameHe,LastNameHe,FirstNameEn,LastNameEn,PassportNumber,PassportExperationDate
										,Birthday,SEX,Status,CreationDate,UpdateDate,UserID,ParticipantType,SchoolInstituteID)
		VALUES (@GroupID,@ParticipantID,@FirstNameHe,@LastNameHe,@FirstNameEn,@LastNameEn,@PassportNumber,@PassportExperationDate
										,@Birthday,@SEX,@Status,@CreationDate,@CreationDate,@UserID,@ParticipantType,@SchoolInstituteID)
	END
END

