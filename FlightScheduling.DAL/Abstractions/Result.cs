﻿namespace FlightScheduling.DAL.Abstractions
{
    public class Result<T>
    {
        public Result()
        {
        }

        public T Value { get; set; }
        public bool IsSuccuss { get; set; }
        public string Error { get; set; }
    }
}