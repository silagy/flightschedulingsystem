﻿using FlightScheduling.Language;
using FlightScheduling.Language.CustomeExtentions;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlightScheduling.DAL
{
    public enum eGroupFormType
    {
        [Display(ResourceType = typeof(NotificationStrings), Name = "FileType180170")]
        [LocalizedDescription("FileType180170", typeof(NotificationStrings))]
        Form180 = 1,

        [Display(ResourceType = typeof(NotificationStrings), Name = "FileType60")]
        [LocalizedDescription("FileType60", typeof(NotificationStrings))]
        Form60 = 2
    }

    public enum eGroupFormStatus
    {
        [Display(ResourceType = typeof(GroupFormsStrings), Name = "StatusNew")]
        [LocalizedDescription("StatusNew", typeof(GroupFormsStrings))]
        New = 1,

        [Display(ResourceType = typeof(GroupFormsStrings), Name = "StatusInProgress")]
        [LocalizedDescription("StatusInProgress", typeof(GroupFormsStrings))]
        InProgress = 2,

        [Display(ResourceType = typeof(GroupFormsStrings), Name = "StatusWaitingApproval")]
        [LocalizedDescription("StatusWaitingApproval", typeof(GroupFormsStrings))]
        WaitingApproval = 3,

        [Display(ResourceType = typeof(GroupFormsStrings), Name = "StatusRejected")]
        [LocalizedDescription("StatusRejected", typeof(GroupFormsStrings))]
        Rejected = 4,

        [Display(ResourceType = typeof(GroupFormsStrings), Name = "StatusApproved")]
        [LocalizedDescription("StatusApproved", typeof(GroupFormsStrings))]
        Approved = 5,
    }

    public enum eGroupForm60Status
    {
        [Display(ResourceType = typeof(GroupFormsStrings), Name = "StatusNew")]
        [LocalizedDescription("StatusNew", typeof(GroupFormsStrings))]
        New = 1,

        [Display(ResourceType = typeof(GroupFormsStrings), Name = "InApprovalProcess")]
        [LocalizedDescription("InApprovalProcess", typeof(GroupFormsStrings))]
        InApprovalProcess = 2,

        [Display(ResourceType = typeof(GroupFormsStrings), Name = "StatusWaitingApprovalAgency")]
        [LocalizedDescription("StatusWaitingApprovalAgency", typeof(GroupFormsStrings))]
        WaitingApprovalAgency = 3,

        [Display(ResourceType = typeof(GroupFormsStrings), Name = "StatusRejected")]
        [LocalizedDescription("StatusRejected", typeof(GroupFormsStrings))]
        Rejected = 4,

        [Display(ResourceType = typeof(GroupFormsStrings), Name = "WaitingFinalApproval")]
        [LocalizedDescription("WaitingFinalApproval", typeof(GroupFormsStrings))]
        WaitingFinalApproval = 5,

        [Display(ResourceType = typeof(GroupFormsStrings), Name = "StatusApproved")]
        [LocalizedDescription("StatusApproved", typeof(GroupFormsStrings))]
        Approved = 6,

        [Display(ResourceType = typeof(GroupFormsStrings), Name = "WaitingPlanApproval")]
        [LocalizedDescription("WaitingPlanApproval", typeof(GroupFormsStrings))]
        WaitingPlanApproval = 7,
    }
}
