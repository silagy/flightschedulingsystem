﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using DapperExtensions;
using FlightScheduling.DAL.CustomEntities;
using FlightScheduling.DAL.CustomEntities.Groups;
using FlightScheduling.DAL.CustomEntities.Outbox;
using FlightScheduling.DAL.CustomEntities.Permissions;
using FlightScheduling.DAL.CustomEntities.Trip;
using FlightScheduling.DAL.DomainEvents;

namespace FlightScheduling.DAL
{
    public class DBRepository : IDisposable
    {

        public IDbConnection _db = new SqlConnection(ConfigurationManager.ConnectionStrings["FlightDBDapper"].ToString());



        public DBRepository(string connectionString)
        {
            _db = new SqlConnection(connectionString);
        }

        public DBRepository()
        {

        }

        public void Dispose()
        {
            _db.Dispose();
        }

        public void OpenConnection()
        {

            _db.Open();
            // wrap the connection with a profiling connection that tracks timings 
            //return new StackExchange.Profiling.Data.ProfiledDbConnection(_db, MiniProfiler.Current);
        }

        public void CloseConnection()
        {
            _db.Close();
        }

        public IEnumerable<Users> GetAllUsers(int? RoleID, string Email, string Name, bool ShowDeletedUsers)
        {
            OpenConnection();
            IEnumerable<Users> users = _db.Query<Users>(QueriesRepository.GetAllUsers, new
            {
                RoleID,
                Email,
                Name,
                ShowDeletedUsers
            });
            CloseConnection();

            return users;
        }

        public IEnumerable<Users> GetUsersByPage(int _PageNumber, int _NumberOfRows)
        {
            int _startRecords = _PageNumber == 1 ? _PageNumber : (_PageNumber - 1) * _NumberOfRows;
            int _getRows = _PageNumber * _NumberOfRows;
            OpenConnection();
            IEnumerable<Users> users = _db.Query<Users>(QueriesRepository.GetUsersByPage,
                new { PageNumber = _startRecords, NumberOfRows = _getRows });
            CloseConnection();
            return users;
        }

        public Users GetUserByID(int _UserID)
        {
            OpenConnection();
            Users user = _db.Query<Users>(QueriesRepository.GetUserByID, new { UserID = _UserID }).FirstOrDefault();
            CloseConnection();
            return user;
        }

        public IEnumerable<ExpeditionToCSVIntegration> GetExpeditionToCsvIntegration(DateTime i_StartDate, DateTime i_EndDate, eExpeditionStatus i_ExpeditionStatus = eExpeditionStatus.Approved)
        {
            OpenConnection();
            IEnumerable<ExpeditionToCSVIntegration> data = _db.Query<ExpeditionToCSVIntegration>(QueriesRepository.GetExpeditionToCSV, new { StartDate = i_StartDate, EndDate = i_EndDate, Status = (int)i_ExpeditionStatus });
            CloseConnection();
            return data;
        }

        /// <summary>
        /// This method responsible for fatching all properties from the system configuraion table.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<SystemConfiguration> GetSystemConfiguraion()
        {
            OpenConnection();
            IEnumerable<SystemConfiguration> data =
                _db.Query<SystemConfiguration>(QueriesRepository.GetSystemConfiguration);
            CloseConnection();

            return data;
        }

        public int UpdateSystemConfigurationProperties(List<SystemConfiguration> properties)
        {
            OpenConnection();
            int results = 0;
            foreach (var property in properties)
            {
                results += _db.Execute(QueriesRepository.UpdateSystemConfigurationProperties, new
                {
                    property.PropertyID,
                    property.PropertyValue
                });
            }

            CloseConnection();
            return results;
        }

        public int InsertMessage(Messages _message)
        {
            try
            {
                OpenConnection();
                int returnId = _db.Query<int>(QueriesRepository.InsertNewMessage, _message).SingleOrDefault();

                CloseConnection();

                return returnId;
            }
            catch (Exception ex)
            {
                CloseConnection();
                throw;
            }
        }

        public IEnumerable<MessagesDetails> GetAllMassages()
        {
            try
            {
                OpenConnection();
                IEnumerable<MessagesDetails> data = _db.Query<MessagesDetails>(QueriesRepository.GetAllMessages);
                CloseConnection();

                return data;
            }
            catch (Exception)
            {
                CloseConnection();
                throw;
            }
        }

        public IEnumerable<MessagesDetails> GetMassagesByDates(DateTime StartDate, DateTime EndDate)
        {
            try
            {
                OpenConnection();
                IEnumerable<MessagesDetails> data = _db.Query<MessagesDetails>(QueriesRepository.GetAllMessagesByDates, new
                {
                    StartDate = StartDate.Date,
                    EndDate = EndDate.Date
                });

                CloseConnection();

                return data;
            }
            catch (Exception)
            {
                CloseConnection();
                throw;
            }
        }

        public Expeditions GetOrderByID(int OrderID)
        {
            try
            {
                OpenConnection();
                Expeditions data = _db.Query<Expeditions, Trips, Expeditions>(QueriesRepository.GetOrderByID,
                    (order, trip) =>
                    {
                        order.Trips = trip;
                        return order;
                    }, new { OrderID }).SingleOrDefault();
                CloseConnection();
                return data;
            }
            catch (Exception)
            {
                CloseConnection();
                throw;
            }
        }

        public int GetLastGroupExternalID(DateTime StartDate, DateTime EndDate)
        {
            try
            {
                OpenConnection();
                dynamic data = _db.Query<dynamic>(QueriesRepository.GetGroupMaxID, new { StartDate, EndDate }).FirstOrDefault();
                CloseConnection();

                if (data.GroupExternalID == null)
                {
                    return 0;
                }
                return (int)data.GroupExternalID;
            }
            catch (Exception ex)
            {
                CloseConnection();
                throw;
            }
        }

        public List<Group> GetGroupsByOrderID(int OrderID)
        {
            try
            {
                OpenConnection();
                List<Group> data = _db.Query<Group>(QueriesRepository.GetGroupsByOrderID, new { OrderID }).ToList();
                CloseConnection();
                return data;
            }
            catch (Exception ex)
            {
                CloseConnection();
                throw;
            }
        }

        public List<GroupColor> GetGroupsColors()
        {
            try
            {
                OpenConnection();
                List<GroupColor> data = _db.Query<GroupColor>(QueriesRepository.GetGroupsColors).ToList();
                CloseConnection();

                return data;
            }
            catch (Exception ex)
            {
                CloseConnection();
                throw;
            }
        }

        public void InsertMultipleGroups(IEnumerable<Group> groups)
        {

            OpenConnection();
            using (var transaction = _db.BeginTransaction())
            {
                try
                {
                    _db.Execute(QueriesRepository.InsertNewGroup, groups, transaction);
                    transaction.Commit();
                    CloseConnection();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    CloseConnection();
                    throw;
                }
            }

        }

        public void UpdateMultipleGroups(IEnumerable<Group> groups)
        {

            OpenConnection();
            using (var transaction = _db.BeginTransaction())
            {
                try
                {
                    foreach (var item in groups)
                    {
                        _db.Execute(QueriesRepository.UpdateGroup, new
                        {
                            item.NumberOfBuses,
                            item.NumberOfParticipants,
                            item.Status,
                            item.ColorID,
                            item.UpdateDate,
                            item.ID,
                            item.AgencyID
                        }, transaction);
                    }

                    transaction.Commit();
                    CloseConnection();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    CloseConnection();
                    throw;
                }
            }

        }

        public List<GroupFiles> GetGroupFilesBuGroupID(int GroupID, List<int> GroupFileTypes)
        {
            try
            {
                OpenConnection();
                List<GroupFiles> data = _db.Query<GroupFiles>(QueriesRepository.GetGroupFilesByGroupID, new { GroupID, GroupFileTypes }).ToList();
                CloseConnection();

                return data;
            }
            catch (Exception ex)
            {
                CloseConnection();
                throw;
            }
        }

        public int InsertGroupFile(GroupFiles _file)
        {
            try
            {
                OpenConnection();

                int returnId = _db.Query<int>(QueriesRepository.InsertNewGroupFile, _file).SingleOrDefault();

                CloseConnection();

                return returnId;
            }
            catch (Exception ex)
            {
                CloseConnection();
                throw;
            }
        }

        public GroupFolderCustom GetGroupFolderDataByID(int GroupID)
        {
            try
            {
                OpenConnection();
                GroupFolderCustom data = _db.Query<GroupFolderCustom, GroupColor, Expeditions, Trips, Schools, Agencies, GroupFolderCustom>(QueriesRepository.GetSingleGroupFolderData,
                    (groupfolder, groupcolor, order, trip, school, agency) =>
                    {
                        groupfolder.GroupColor = groupcolor;
                        groupfolder.Order = order;
                        groupfolder.Order.Agencies = agency;
                        groupfolder.Trip = trip;
                        groupfolder.School = school;
                        return groupfolder;
                    }, new { GroupID }).FirstOrDefault();
                CloseConnection();

                return data;
            }
            catch (Exception ex)
            {
                CloseConnection();
                throw;
            }
        }

        public GroupFiles GetFileByID(int ID)
        {
            try
            {
                OpenConnection();
                GroupFiles data = _db.Query<GroupFiles>(QueriesRepository.GetFileByID, new { ID }).FirstOrDefault();
                CloseConnection();

                return data;
            }
            catch (Exception e)
            {
                CloseConnection();
                throw;
            }
        }

        public int UpdateGroupFileStatus(GroupFiles file)
        {
            try
            {
                OpenConnection();
                int results = _db.Execute(QueriesRepository.UpdateGroupFileStatus, file);
                CloseConnection();

                return results;
            }
            catch (Exception ex)
            {
                CloseConnection();
                throw;
            }
        }

        public List<GroupFolderCustom> GetGroupDetails(string query, DateTime StartDate, DateTime EndDate, int? OrderID, int? TripID, List<int> GroupStatus, int? SchoolID, int? AgencyID, string InstituteID, int? GroupID, int? Region)
        {
            try
            {
                OpenConnection();

                var groupStatusArray = GroupStatus.Count() > 0 ? GroupStatus.ToArray() : null;

                DynamicParameters param = new DynamicParameters();
                param.Add("@StartDate", StartDate);
                param.Add("@EndDate", EndDate);
                param.Add("@OrderID", OrderID);
                param.Add("@TripID", TripID);
                param.Add("@GroupStatus", groupStatusArray);
                param.Add("@SchoolID", SchoolID);
                param.Add("@AgencyID", AgencyID);
                param.Add("@InstituteID", InstituteID);
                param.Add("@GroupID", GroupID);
                param.Add("@Region", Region);

                List<GroupFolderCustom> data = _db
                    .Query<GroupFolderCustom, GroupColor, Expeditions, Trips, Schools, GroupFolderCustom>(query,
                        (groupfolder, groupcolor, order, trip, school) =>
                        {
                            groupfolder.GroupColor = groupcolor;
                            groupfolder.Order = order;
                            groupfolder.Trip = trip;
                            groupfolder.School = school;

                            return groupfolder;
                        }, new { StartDate, EndDate, OrderID, TripID, GroupStatus = groupStatusArray, SchoolID, AgencyID, InstituteID, GroupID, Region }, commandTimeout: 600)
                    .ToList();
                CloseConnection();

                return data;
            }
            catch (Exception ex)
            {
                CloseConnection();
                throw;
            }
        }

        public List<GroupFolderCustomNewView> GetGroupDetailsNewView(string query, DateTime StartDate, DateTime EndDate, int? OrderID, int? TripID, List<int> GroupStatus, int? SchoolID, int? AgencyID, string InstituteID, int? GroupID, int? Region, int? ShowAdministrative, bool? IsPrimary, IEnumerable<int> GroupIDs)
        {
            try
            {
                OpenConnection();

                var groupStatusArray = GroupStatus.Count() > 0 ? GroupStatus.ToArray() : null;

                DynamicParameters param = new DynamicParameters();
                param.Add("@StartDate", StartDate);
                param.Add("@EndDate", EndDate);
                param.Add("@OrderID", OrderID);
                param.Add("@TripID", TripID);
                param.Add("@GroupStatus", groupStatusArray);
                param.Add("@SchoolID", SchoolID);
                param.Add("@AgencyID", AgencyID);
                param.Add("@InstituteID", InstituteID);
                param.Add("@GroupID", GroupID);
                param.Add("@Region", Region);
                param.Add("@ShowAdministrative", ShowAdministrative);
                param.Add("@IsPrimary", IsPrimary);
                //param.Add("@ManagerEmail", String.IsNullOrEmpty(ManagerEmail) ? null : ManagerEmail);

                // Check if GroupIDs has values if not return the number of values. This for applying this filter in the DB check query for more details
                int GroupIDsCount = GroupIDs == null ? 0 : GroupIDs.Count();

                List<GroupFolderCustomNewView> data = _db
                    .Query<GroupFolderCustomNewView, GroupColor, Expeditions, Trips, Schools, GroupFolderCustomNewView>(query,
                        (groupfolder, groupcolor, order, trip, school) =>
                        {
                            groupfolder.GroupColor = groupcolor;
                            groupfolder.Order = order;
                            groupfolder.Trip = trip;
                            groupfolder.School = school;
                            return groupfolder;
                        }, new { StartDate, EndDate, OrderID, TripID, GroupStatus = groupStatusArray, SchoolID, AgencyID, InstituteID, GroupID, Region, ShowAdministrative, IsPrimary, GroupIDs, GroupIDsCount }, commandTimeout: 600)
                    .ToList();
                CloseConnection();

                return data;
            }
            catch (Exception ex)
            {
                CloseConnection();
                throw;
            }
        }

        public List<GroupFolderCustomNewView> GetGroupDetailsNewViewForGroupManager(string query, DateTime StartDate, DateTime EndDate, int? ShowAdministrative, IEnumerable<int> GroupIDs)
        {
            try
            {
                OpenConnection();


                DynamicParameters param = new DynamicParameters();
                param.Add("@StartDate", StartDate);
                param.Add("@EndDate", EndDate);
                param.Add("@ShowAdministrative", ShowAdministrative);
                //param.Add("@ManagerEmail", String.IsNullOrEmpty(ManagerEmail) ? null : ManagerEmail);

                // Check if GroupIDs has values if not return the number of values. This for applying this filter in the DB check query for more details
                int GroupIDsCount = GroupIDs == null ? 0 : GroupIDs.Count();

                List<GroupFolderCustomNewView> data = _db
                    .Query<GroupFolderCustomNewView, GroupColor, Expeditions, Trips, Schools, GroupFolderCustomNewView>(query,
                        (groupfolder, groupcolor, order, trip, school) =>
                        {
                            groupfolder.GroupColor = groupcolor;
                            groupfolder.Order = order;
                            groupfolder.Trip = trip;
                            groupfolder.School = school;
                            return groupfolder;
                        }, new { StartDate, EndDate, ShowAdministrative, GroupIDs, GroupIDsCount }, commandTimeout: 600)
                    .ToList();
                CloseConnection();

                return data;
            }
            catch (Exception ex)
            {
                CloseConnection();
                throw;
            }
        }

        public List<GroupRemarks> GetGroupRemarks(int GroupID)
        {
            try
            {
                OpenConnection();
                List<GroupRemarks> data =
                    _db.Query<GroupRemarks, Users, Schools, GroupRemarks>(QueriesRepository.GetGroupNotesByGroupID, (remark, user, school) =>
                     {
                         remark.Users = user;
                         remark.Schools = school;
                         return remark;
                     }, new { GroupID }, splitOn: "ID, SchoolID").ToList();
                CloseConnection();
                return data;
            }
            catch (Exception ex)
            {
                CloseConnection();
                throw;
            }
        }

        public int InsertGroupRemark(GroupRemarks remark)
        {
            try
            {
                OpenConnection();
                int returnedID = _db.Query<int>(QueriesRepository.InsertGroupNote, remark).SingleOrDefault();
                CloseConnection();
                return returnedID;
            }
            catch (Exception ex)
            {
                CloseConnection();
                throw;
            }
        }

        public List<GroupParticipants> GetGroupParticipants(int GroupID)
        {
            try
            {
                OpenConnection();
                List<GroupParticipants> data = _db.Query<GroupParticipants, Users, GroupParticipants>(
                        QueriesRepository.GetGroupParticipants,
                        (participant, user) =>
                        {
                            participant.Users = user;
                            return participant;
                        }, new { GroupID })
                    .ToList();
                CloseConnection();
                return data;
            }
            catch (Exception ex)
            {
                CloseConnection();
                throw;
            }
        }

        public void InserOrUpdateGroupParticipant(GroupParticipants participant)
        {
            try
            {
                OpenConnection();
                _db.Query(QueriesRepository.InsertOrUpdateGroupParticipant, new
                {
                    participant.GroupID,
                    participant.ParticipantID,
                    participant.FirstNameHe,
                    participant.LastNameHe,
                    participant.FirstNameEn,
                    participant.LastNameEn,
                    participant.PassportNumber,
                    participant.PassportExperationDate,
                    participant.Birthday,
                    participant.SEX,
                    participant.Status,
                    participant.CreationDate,
                    participant.UpdateDate,
                    participant.UserID,
                    participant.ParticipantType,
                    participant.SchoolInstituteID
                },
                    commandType: CommandType.StoredProcedure);
                CloseConnection();
            }
            catch (Exception ex)
            {
                CloseConnection();
                throw;
            }
        }

        public bool InsertOrUpdateFlight(Flights flight)
        {
            try
            {
                OpenConnection();
                int results = _db.Execute(QueriesRepository.InsertOrUpdateFlightImport, new
                {
                    flight.ID,
                    flight.StartDate,
                    flight.DepartureFieldID,
                    flight.DestinationID,
                    flight.AirlineName,
                    flight.FlightNumber,
                    flight.NumberOfPassengers,
                    flight.Comments,
                    flight.IsActive,
                    flight.UpdateDate,
                    flight.CreationDate,
                    flight.TakeOffFromIsrael,
                    flight.ArrivalTime
                },
                    commandType: CommandType.Text);
                CloseConnection();

                return results > 0;
            }
            catch (Exception ex)
            {
                CloseConnection();
                throw;
            }
        }

        public bool InsertOrUpdateTrip(Trips trip)
        {
            try
            {
                OpenConnection();
                int results = _db.Execute(QueriesRepository.InsertOrUpdadteTripImport, new
                {
                    trip.ID,
                    trip.DepartureDate,
                    trip.DestinationID,
                    trip.ReturningDate,
                    trip.ReturingField,
                    trip.Comments,
                    trip.CreationDate,
                    trip.UpdateDate
                },
                    commandType: CommandType.Text);
                CloseConnection();

                return results > 0;
            }
            catch (Exception ex)
            {
                CloseConnection();
                throw;
            }
        }

        public GroupParticipants GetGroupParticipant(int ID)
        {
            try
            {
                OpenConnection();
                GroupParticipants data = _db.Query<GroupParticipants, Users, GroupParticipants>(
                        QueriesRepository.GetGroupParticipant,
                        (participant, user) =>
                        {
                            participant.Users = user;
                            return participant;
                        }, new { ID })
                    .SingleOrDefault();
                CloseConnection();
                return data;
            }
            catch (Exception ex)
            {
                CloseConnection();
                throw;
            }
        }

        public void InsertMultipleParticipants(IEnumerable<GroupParticipants> participants)
        {
            OpenConnection();
            using (var transaction = _db.BeginTransaction())
            {
                try
                {
                    foreach (var participant in participants)
                    {
                        _db.Execute(QueriesRepository.InsertOrUpdateGroupParticipant, new
                        {
                            participant.GroupID,
                            participant.ParticipantID,
                            participant.FirstNameHe,
                            participant.LastNameHe,
                            participant.FirstNameEn,
                            participant.LastNameEn,
                            participant.PassportNumber,
                            participant.PassportExperationDate,
                            participant.Birthday,
                            participant.SEX,
                            participant.Status,
                            participant.CreationDate,
                            participant.UpdateDate,
                            participant.UserID,
                            participant.ParticipantType,
                            participant.SchoolInstituteID,
                            participant.Comments,
                            participant.ClassType
                        },
                            commandType: CommandType.StoredProcedure, transaction: transaction);
                    }
                    transaction.Commit();
                    CloseConnection();

                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    CloseConnection();
                    throw;
                }
            }
        }

        public int GetSchoolIDByUserID(int UserID)
        {
            try
            {
                OpenConnection();
                dynamic data = _db.Query<dynamic>(QueriesRepository.GetSchoolIDByUserID, new { UserID }).FirstOrDefault();
                CloseConnection();

                if (data.ID == null)
                {
                    return 0;
                }
                return (int)data.ID;
            }
            catch (Exception ex)
            {
                CloseConnection();
                throw;
            }
        }

        public int GetAgencyIDByUserID(int UserID)
        {
            try
            {
                OpenConnection();
                dynamic data = _db.Query<dynamic>(QueriesRepository.GetAgencyIDByUserID, new { UserID }).FirstOrDefault();
                CloseConnection();

                if (data.ID == null)
                {
                    return 0;
                }
                return (int)data.ID;
            }
            catch (Exception ex)
            {
                CloseConnection();
                throw;
            }
        }

        public int GetNumberOfAssignedParticipantsInGroup(int GroupID)
        {
            try
            {
                OpenConnection();
                dynamic data = _db.Query<dynamic>(QueriesRepository.GetNumberOfAssingedParticipantsToGroup, new { GroupID }).FirstOrDefault();
                CloseConnection();

                if (data.AssignedParticipants == null)
                {
                    return 0;
                }
                return (int)data.AssignedParticipants;
            }
            catch (Exception ex)
            {
                CloseConnection();
                throw;
            }
        }

        public GroupFiles GetGroupFileByID(int FileID)
        {
            try
            {
                OpenConnection();
                GroupFiles data =
                    _db.Query<GroupFiles>(QueriesRepository.GetGroupFileByID, new { FileID }).SingleOrDefault();
                CloseConnection();
                return data;
            }
            catch (Exception ex)
            {
                CloseConnection();
                throw;
            }
        }

        public bool DeleteGroupFile(int FileID)
        {
            try
            {
                OpenConnection();
                int results = _db.Execute(QueriesRepository.DeleteFileByID, new { FileID });

                return results > 0;
            }
            catch (Exception ex)
            {
                CloseConnection();
                throw;
            }
        }

        public bool DeleteNote(int NoteID)
        {
            try
            {
                OpenConnection();
                int results = _db.Execute(QueriesRepository.DeleteNotebyID, new { NoteID });

                return results > 0;
            }
            catch (Exception ex)
            {
                CloseConnection();
                throw;
            }
        }

        public GroupRemarks GetNoteByID(int NoteID)
        {
            try
            {
                OpenConnection();
                GroupRemarks data = _db.Query<GroupRemarks, Users, Schools, GroupRemarks>(QueriesRepository.GetNoteByID,
                    (note, user, school) =>
                    {
                        note.Users = user;
                        note.Schools = school;
                        return note;
                    }, new { NoteID }, splitOn: "UserID, SchoolID").SingleOrDefault();
                CloseConnection();
                return data;
            }
            catch (Exception ex)
            {
                CloseConnection();
                throw;
            }
        }

        public bool DeleteParticipant(int ParticipantID)
        {
            try
            {
                OpenConnection();
                int results = _db.Execute(QueriesRepository.DeleteParticipant, new { ParticipantID });

                return results > 0;
            }
            catch (Exception ex)
            {
                CloseConnection();
                throw;
            }
        }

        public bool UpdateGroupNote(GroupRemarks note)
        {
            try
            {
                OpenConnection();
                int results = _db.Execute(QueriesRepository.UpdateGroupNote, new
                {
                    note.ID,
                    note.UserID,
                    note.Notes,
                    note.UpdateDate

                });

                return results > 0;
            }
            catch (Exception ex)
            {
                CloseConnection();
                throw;
            }
        }

        public List<Instructors> GetAllInstructors(int? RoleID, string InstructorID, string LastName)
        {
            try
            {
                OpenConnection();
                List<Instructors> data = _db.Query<Instructors, InstructorRoles, Instructors>(QueriesRepository.GetAllInstructors,
                    (inst, role) =>
                    {
                        inst.InstructorRoles = role;
                        return inst;
                    }, new
                    {
                        RoleID,
                        InstructorID,
                        LastName
                    }).ToList();
                CloseConnection();

                return data;
            }
            catch (Exception ex)
            {
                CloseConnection();
                throw;
            }
        }

        public int InsertInstructor(Instructors Instructor)
        {
            try
            {
                OpenConnection();
                int results = _db.Query<int>(QueriesRepository.InsertInstructor, Instructor).SingleOrDefault();

                return results;
            }
            catch (Exception ex)
            {
                CloseConnection();
                throw;
            }
        }

        public bool UpdateInstructor(Instructors Instructor)
        {
            try
            {
                OpenConnection();
                int results = _db.Execute(QueriesRepository.UpdateInstructor, Instructor);

                return results > 0;
            }
            catch (Exception ex)
            {
                CloseConnection();
                throw;
            }
        }

        public Instructors GetInstructor(int ID)
        {
            try
            {
                OpenConnection();
                Instructors data = _db.Query<Instructors, InstructorRoles, Instructors>(QueriesRepository.GetInstructor,
                    (instructor, role) =>
                    {
                        instructor.InstructorRoles = role;
                        return instructor;
                    }, new { ID }).SingleOrDefault();
                CloseConnection();
                return data;
            }
            catch (Exception ex)
            {
                CloseConnection();
                throw;
            }
        }

        public bool DeleteInstructor(int ID)
        {
            try
            {
                OpenConnection();
                int results = _db.Execute(QueriesRepository.DeleteInstructor, new { ID });
                CloseConnection();
                return results > 0;
            }
            catch (Exception ex)
            {
                CloseConnection();
                throw;
            }
        }

        public bool DeleteManagerFromGroupByGroupID(int GroupID)
        {
            try
            {
                OpenConnection();
                int results = _db.Execute(QueriesRepository.DeleteManagerFromGroupByGroupID, new { GroupID });
                CloseConnection();
                return results > 0;
            }
            catch (Exception ex)
            {
                CloseConnection();
                throw;
            }
        }

        public bool SetInstructorInActive(int ID, bool IsActive)
        {
            try
            {
                OpenConnection();
                int results = _db.Execute(QueriesRepository.SetInstrucorInActive, new { IsActive, ID });

                return results > 0;
            }
            catch (Exception ex)
            {
                CloseConnection();
                throw;
            }
        }

        public bool UnregisterInstructorTraining(int ID)
        {
            try
            {
                OpenConnection();
                int results = _db.Execute(QueriesRepository.UnregisterInstructorTraining, new { ID });

                return results > 0;
            }
            catch (Exception ex)
            {
                CloseConnection();
                throw;
            }
        }

        public bool InsertInstructorRole(InstructorRoles InstructorRole)
        {
            try
            {
                OpenConnection();
                int results = _db.Execute(QueriesRepository.InsertInstructorRole, InstructorRole);

                return results > 0;
            }
            catch (Exception ex)
            {
                CloseConnection();
                throw;
            }
        }

        public bool UpdateInstructorRole(InstructorRoles InstructorRole)
        {
            try
            {
                OpenConnection();
                int results = _db.Execute(QueriesRepository.UpdateInstructorRole, InstructorRole);

                return results > 0;
            }
            catch (Exception ex)
            {
                CloseConnection();
                throw;
            }
        }

        public List<InstructorRoles> GetAllInstructorRoles(string Lang)
        {
            try
            {
                OpenConnection();
                List<InstructorRoles> data = _db.Query<InstructorRoles>(QueriesRepository.GetInstructorRoles, new { Language = Lang }).ToList();
                CloseConnection();

                return data;
            }
            catch (Exception ex)
            {
                CloseConnection();
                throw;
            }
        }

        public InstructorRoles GetAllInstructorRole(int ID)
        {
            try
            {
                OpenConnection();
                InstructorRoles data = _db.Query<InstructorRoles>(QueriesRepository.GetInstructorRole, new { ID }).SingleOrDefault();
                CloseConnection();

                return data;
            }
            catch (Exception ex)
            {
                CloseConnection();
                throw;
            }
        }

        public bool InsertTraining(Trainings training)
        {
            try
            {
                OpenConnection();
                int results = _db.Execute(QueriesRepository.InsertTraining, training);

                return results > 0;
            }
            catch (Exception ex)
            {
                CloseConnection();
                throw;
            }
        }

        public bool UpdateTraining(Trainings training)
        {
            try
            {
                OpenConnection();
                int results = _db.Execute(QueriesRepository.UpdateTraining, training);

                return results > 0;
            }
            catch (Exception ex)
            {
                CloseConnection();
                throw;
            }
        }

        public bool CloseTraining(Trainings training)
        {
            try
            {
                OpenConnection();
                int results = _db.Execute(QueriesRepository.CloseTraining, new
                {
                    training.IsCompleted,
                    training.ID
                });

                return results > 0;
            }
            catch (Exception ex)
            {
                CloseConnection();
                throw;
            }
        }

        public List<Training> GetAllTrainings(int? RoleID, DateTime StartDate, DateTime EndDate, bool? IsTrainingCompleted, eTrainingLocation? TrainingLocation, eSchoolRegion? TrainingRegion, int? TrainingTypeID)
        {
            try
            {
                OpenConnection();
                List<Training> data = _db.Query<Training, InstructorRoleEntity, Training>(QueriesRepository.GetTrainings,
                    (training, instructorRole) =>
                    {
                        training.Role = instructorRole;
                        return training;
                    }, new
                    {
                        RoleID,
                        StartDate,
                        EndDate,
                        IsTrainingCompleted,
                        TrainingLocation,
                        TrainingRegion,
                        TrainingTypeID
                    }).ToList();
                CloseConnection();

                return data;
            }
            catch (Exception ex)
            {
                CloseConnection();
                throw;
            }
        }

        // public List<Trainings> GetAllTrainings(int? RoleID, int? CreatorID, DateTime StartDate, DateTime EndDate, bool? IsTrainingCompleted, eTrainingLocation? TrainingLocation, eSchoolRegion? TrainingRegion)
        //{
        //    try
        //    {
        //        OpenConnection();
        //        List<Trainings> data = _db.Query<Trainings, InstructorRoles, Trainings>(QueriesRepository.GetTrainings,
        //            (training, instructorRole) =>
        //            {
        //                training.InstructorRoles = instructorRole;
        //                return training;
        //            }, new
        //            {
        //                RoleID,
        //                CreatorID,
        //                StartDate,
        //                EndDate,
        //                IsTrainingCompleted,
        //                TrainingLocation,
        //                TrainingRegion
        //            }).ToList();
        //        CloseConnection();

        //        return data;
        //    }
        //    catch (Exception ex)
        //    {
        //        CloseConnection();
        //        throw;
        //    }
        //}

        public Trainings GetTraining(int ID)
        {
            try
            {
                OpenConnection();
                Trainings data = _db.Query<Trainings, InstructorRoles, Trainings>(QueriesRepository.GetTraining,
                    (training, instructorRole) =>
                    {
                        training.InstructorRoles = instructorRole;
                        return training;
                    }, new { ID }).SingleOrDefault();
                CloseConnection();

                return data;
            }
            catch (Exception ex)
            {
                CloseConnection();
                throw;
            }
        }

        public int GetCountOfInstructorsInTraining(int TrainingID)
        {
            try
            {
                OpenConnection();
                dynamic data = _db.Query<dynamic>(QueriesRepository.GetCountOfInstructorsInTraining, new { TrainingID }).FirstOrDefault();
                CloseConnection();

                if (data.InstructorsCount == null)
                {
                    return 0;
                }
                return (int)data.InstructorsCount;
            }
            catch (Exception ex)
            {
                CloseConnection();
                throw;
            }
        }

        public bool DeleteTraining(int ID)
        {
            try
            {
                OpenConnection();
                int results = _db.Execute(QueriesRepository.DeleteTraining, new { ID });

                return results > 0;
            }
            catch (Exception ex)
            {
                CloseConnection();
                throw;
            }
        }

        public List<InstructorTrainings> GetInstructorTrainingBtTrainingID(int TrainingID)
        {
            try
            {
                OpenConnection();
                List<InstructorTrainings> data = _db
                    .Query<InstructorTrainings, Trainings, Instructors, InstructorRoles, InstructorTrainings>(
                        QueriesRepository.GetInstructorsTrainingByTrainingID,
                        (instTraining, Training, instructor, instructorRole) =>
                        {
                            instTraining.Trainings = Training;
                            instructor.InstructorRoles = instructorRole;
                            instTraining.Instructors = instructor;
                            return instTraining;
                        }, new { TrainingID })
                    .ToList();
                CloseConnection();
                return data;
            }
            catch (Exception ex)
            {
                CloseConnection();
                throw;
            }
        }

        public int InsertNewInstructorAndUpdateQualification(Instructors instructor, InstructorTrainings instTraining, InstructorRecordsHistory recordsHistory)
        {
            OpenConnection();
            using (var transaction = _db.BeginTransaction())
            {
                try
                {
                    int instrcutorID = _db.Query<int>(QueriesRepository.InsertInstructor, instructor, transaction: transaction).SingleOrDefault();
                    instTraining.InstructorID = instrcutorID;
                    int results = _db.Execute(QueriesRepository.RegisterInstructorToTraining, instTraining, transaction: transaction);
                    recordsHistory.InstructorID = instrcutorID;
                    int historyRecord = _db.Execute(QueriesRepository.InsertRecordHistory, recordsHistory, transaction: transaction);

                    transaction.Commit();
                    CloseConnection();
                    return instrcutorID;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    CloseConnection();
                    throw;
                }
            }
        }

        public int InsertNewInstructorAndTraining(Instructors instructor, InstructorTrainings instTraining)
        {
            OpenConnection();
            using (var transaction = _db.BeginTransaction())
            {
                try
                {
                    // Creating instructor created event and adding it to the outbox message
                    int instrcutorId = _db.Query<int>(QueriesRepository.InsertInstructor, instructor, transaction: transaction).SingleOrDefault();
                    
                    // Creating instructor created domain event
                    var instructorCreatedDomainEvent = new InstructorCreatedDomainEvent(instrcutorId);
                    var outbox = DomainEventExtensions.GetOutboxFromDomainEvent(instructorCreatedDomainEvent);

                    int outboxResults = _db.Execute(QueriesRepository.Outbox.InsertOutbox, outbox,
                        transaction: transaction);
                    
                    instTraining.InstructorID = instrcutorId;
                    int results = _db.Execute(QueriesRepository.RegisterInstructorToTraining, instTraining, transaction: transaction);

                    transaction.Commit();
                    CloseConnection();
                    return instrcutorId;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    CloseConnection();
                    throw;
                }
            }
        }

        public int UpdatingInstructorAndTraining(Instructors instructor, InstructorTrainings instTraining, bool updateRole = false)
        {
            OpenConnection();
            using (var transaction = _db.BeginTransaction())
            {
                try
                {
                    if (updateRole)
                    {
                        int result = _db.Execute(QueriesRepository.UpdateInstructorwithRole, instructor, transaction: transaction);
                    }
                    else
                    {
                        int result = _db.Execute(QueriesRepository.UpdateInstructor, instructor, transaction: transaction);
                    }

                    instTraining.InstructorID = instructor.ID;
                    int results = _db.Execute(QueriesRepository.RegisterInstructorToTraining, instTraining, transaction: transaction);

                    transaction.Commit();
                    CloseConnection();
                    return instructor.ID;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    CloseConnection();
                    throw;
                }
            }
        }

        public bool CheckInstructorExistByInstructorID(string InstructorID)
        {
            try
            {
                OpenConnection();
                int data = _db.Query<int>(QueriesRepository.CheckInstuctorExistByInstructorID, new { InstructorID }).SingleOrDefault();
                CloseConnection();

                return data > 0;
            }
            catch (Exception ex)
            {
                CloseConnection();
                throw;
            }
        }

        public List<Instructors> GetInstructorsByRole(int RoleID)
        {
            try
            {
                OpenConnection();
                List<Instructors> data = _db
                    .Query<Instructors>(
                        QueriesRepository.GetInstructorsByRole, new { RoleID }).ToList();
                CloseConnection();
                return data;
            }
            catch (Exception ex)
            {
                CloseConnection();
                throw;
            }
        }

        public Instructors GetInstructorByInstructorID(string InstructorID)
        {
            try
            {
                OpenConnection();
                Instructors data = _db
                    .Query<Instructors, InstructorRoles, Instructors>(
                        QueriesRepository.GetInstructorByInstructorID, (instructor, role) =>
                        {
                            instructor.InstructorRoles = role;
                            return instructor;
                        }, new { InstructorID }).SingleOrDefault();
                CloseConnection();
                return data;
            }
            catch (Exception ex)
            {
                CloseConnection();
                throw;
            }
        }

        public List<Instructors> GetInstructorsByRoleNotInTraining(int RoleID, int TrainingID)
        {
            try
            {
                OpenConnection();
                List<Instructors> data = _db
                    .Query<Instructors>(
                        QueriesRepository.GetInstructorsByRoleNotInTraining, new { RoleID, TrainingID }).ToList();
                CloseConnection();
                return data;
            }
            catch (Exception ex)
            {
                CloseConnection();
                throw;
            }
        }

        public bool UpdateInstructorQualification(Instructors instructor, InstructorRecordsHistory recordsHistory)
        {
            OpenConnection();
            using (var transaction = _db.BeginTransaction())
            {
                try
                {
                    int resultsInst = _db.Execute(QueriesRepository.UpdateInstructorRecords, new
                    {
                        instructor.UpdateDate,
                        instructor.ValidityStartDate,
                        instructor.ValidityEndTime,
                        instructor.CertificateID,
                        instructor.ID
                    },
                        transaction: transaction);
                    int historyRecord = _db.Execute(QueriesRepository.InsertRecordHistory, recordsHistory, transaction: transaction);

                    transaction.Commit();
                    CloseConnection();
                    return resultsInst > 0 && historyRecord > 0;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    CloseConnection();
                    throw;
                }
            }
        }



        public bool UpdateInstructorTraining(InstructorTrainings instructorTraining)
        {
            try
            {
                OpenConnection();
                int resultInstructorTraining = _db.Execute(QueriesRepository.UpdateInstructorTraining, new
                {
                    instructorTraining.CountToValidity,
                    instructorTraining.ID
                });
                CloseConnection();
                return resultInstructorTraining > 0;
            }
            catch (Exception ex)
            {
                CloseConnection();
                throw;
            }
        }

        public bool TrainingCompletion(InstructorTrainings instructorTraining, Instructors instructor, InstructorRecordsHistory recordsHistory, bool UpdateInstructorQualification = false)
        {
            OpenConnection();
            using (var transaction = _db.BeginTransaction())
            {
                try
                {
                    int resultInstructorTraining = _db.Execute(QueriesRepository.UpdateInstructorTraining, new
                    {
                        instructorTraining.CountToValidity,
                        instructorTraining.ID
                    }, transaction: transaction);

                    if (UpdateInstructorQualification)
                    {

                        int resultsInst = _db.Execute(QueriesRepository.UpdateInstructorRecords, new
                        {
                            instructor.UpdateDate,
                            instructor.ValidityStartDate,
                            instructor.ValidityEndTime,
                            instructor.CertificateID,
                            instructor.ID
                        },
                            transaction: transaction);
                        int historyRecord = _db.Execute(QueriesRepository.InsertRecordHistory, recordsHistory,
                            transaction: transaction);
                        transaction.Commit();
                        CloseConnection();
                        return resultsInst > 0 && historyRecord > 0 && resultInstructorTraining > 0;
                    }

                    transaction.Commit();
                    CloseConnection();
                    return resultInstructorTraining > 0;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    CloseConnection();
                    throw;
                }
            }
        }

        public bool RegisterInstructorToTraining(InstructorTrainings instTraining)
        {
            try
            {
                OpenConnection();
                int results = _db.Execute(QueriesRepository.RegisterInstructorToTraining, instTraining);
                CloseConnection();
                return results > 0;
            }
            catch (Exception ex)
            {
                CloseConnection();
                throw;
            }
        }

        public int GetTotalHoursOfTrainingForInstructor(DateTime? Date, int InstructorID)
        {
            try
            {
                OpenConnection();
                dynamic data = _db.Query<dynamic>(QueriesRepository.GetTotalHoursOfTrainingForInstructor, new { Date, InstructorID }).FirstOrDefault();
                CloseConnection();

                if (data.TotalHours == null)
                {
                    return 0;
                }
                return (int)data.TotalHours;
            }
            catch (Exception ex)
            {
                CloseConnection();
                throw;
            }
        }

        public List<InstructorFiles> GetInstructorFiles(int InstructorID)
        {
            try
            {
                OpenConnection();
                List<InstructorFiles> data = _db.Query<InstructorFiles>(QueriesRepository.GetInstructorsFiles, new { InstructorID }).ToList();
                CloseConnection();

                return data;
            }
            catch (Exception ex)
            {
                CloseConnection();
                throw;
            }
        }

        public List<InstructorRemarks> GetInstructorNotes(int InstructorID)
        {
            try
            {
                OpenConnection();
                List<InstructorRemarks> data = _db.Query<InstructorRemarks, Users, InstructorRemarks>(QueriesRepository.GetInstructorNotes, (note, user) =>
                {
                    note.Users = user;
                    return note;
                }, new { InstructorID }).ToList();

                CloseConnection();

                return data;
            }
            catch (Exception ex)
            {
                CloseConnection();
                throw;
            }
        }

        public int InsertInstructorRemark(InstructorRemarks remark)
        {
            try
            {
                OpenConnection();
                int returnedID = _db.Query<int>(QueriesRepository.InsertInstructorNote, remark).SingleOrDefault();
                CloseConnection();
                return returnedID;
            }
            catch (Exception ex)
            {
                CloseConnection();
                throw;
            }
        }

        public InstructorRemarks GetInstructorRemark(int ID)
        {
            try
            {
                OpenConnection();
                InstructorRemarks data = _db.Query<InstructorRemarks, Users, InstructorRemarks>(QueriesRepository.GetInstructorNote,
                    (remark, user) =>
                    {
                        remark.Users = user;
                        return remark;
                    }, new { ID })
                    .SingleOrDefault();
                CloseConnection();

                return data;
            }
            catch (Exception ex)
            {
                CloseConnection();
                throw;
            }
        }

        public bool UpdateInstructorRemark(InstructorRemarks remark)
        {
            try
            {
                OpenConnection();
                int results = _db.Execute(QueriesRepository.UpdateInstructorNote, remark);
                CloseConnection();
                return results > 0;
            }
            catch (Exception ex)
            {
                CloseConnection();
                throw;
            }
        }

        public bool DeleteInstructorRemark(int ID)
        {
            try
            {
                OpenConnection();
                int results = _db.Execute(QueriesRepository.DeleteInstructorRemark, new { ID });
                CloseConnection();
                return results > 0;
            }
            catch (Exception ex)
            {
                CloseConnection();
                throw;
            }
        }

        public bool InsertInstructorFile(InstructorFiles file)
        {
            try
            {
                OpenConnection();
                int results = _db.Execute(QueriesRepository.InsertInstructorFile, file);
                CloseConnection();

                return results > 0;
            }
            catch (Exception ex)
            {
                CloseConnection();
                throw;
            }
        }

        public bool UpdateInstructorFile(InstructorFiles file)
        {
            try
            {
                OpenConnection();
                int results = _db.Execute(QueriesRepository.UpdateInstructorFile, new
                {
                    file.Name,
                    file.Comments,
                    file.ID
                });
                CloseConnection();

                return results > 0;
            }
            catch (Exception ex)
            {
                CloseConnection();
                throw;
            }
        }

        public InstructorFiles GetInstructorFile(int ID)
        {
            try
            {
                OpenConnection();
                InstructorFiles data =
                    _db.Query<InstructorFiles, Users, InstructorFiles>(QueriesRepository.GetInstructorFile,
                        (file, user) =>
                        {
                            file.Users = user;
                            return file;
                        }, new { ID }).SingleOrDefault();
                CloseConnection();
                return data;
            }
            catch (Exception ex)
            {
                CloseConnection();
                throw;
            }
        }

        public List<InstructorTrainings> GetInstructorTrainings(int InstructorID)
        {
            try
            {
                OpenConnection();
                List<InstructorTrainings> data =
                    _db.Query<InstructorTrainings, Trainings, InstructorTrainings>(
                        QueriesRepository.GetInstructorTrainings,
                        (instTraining, training) =>
                        {
                            instTraining.Trainings = training;
                            return instTraining;
                        }, new { InstructorID }).ToList();
                CloseConnection();
                return data;
            }
            catch (Exception ex)
            {
                CloseConnection();
                throw;
            }
        }

        public bool RegisterInstructorToGroup(int GroupID, int InstructorID, int RoleID)
        {
            try
            {
                OpenConnection();
                int results = _db.Execute(QueriesRepository.RegisterInstructorToGroup, new { GroupID, InstructorID, RoleID });
                CloseConnection();
                return results > 0;
            }
            catch (Exception ex)
            {
                CloseConnection();
                throw;
            }
        }

        public List<InstructorGroups> GetGroupInstructors(int GroupID)
        {
            try
            {
                OpenConnection();
                List<InstructorGroups> data =
                    _db.Query<InstructorGroups, Instructors, InstructorRoles, InstructorGroups>(
                        QueriesRepository.GetGroupsInstructors,
                        (instGroup, instructor, role) =>
                        {
                            instGroup.InstructorRoles = role;
                            instGroup.Instructors = instructor;
                            return instGroup;
                        }, new { GroupID }).ToList();
                CloseConnection();
                return data;
            }
            catch (Exception ex)
            {
                CloseConnection();
                throw;
            }
        }

        public bool CheckInstructorRegisteredToGroup(string InstructorID, int GroupID)
        {
            try
            {
                OpenConnection();
                int data = _db.Query<int>(QueriesRepository.CheckIfInstructorIsRegisteredToGroup, new { InstructorID, GroupID }).FirstOrDefault();
                CloseConnection();

                return data > 0;
            }
            catch (Exception ex)
            {
                CloseConnection();
                throw;
            }
        }

        public bool CheckInstructorValidity(string InstructorID)
        {
            try
            {
                OpenConnection();
                int data = _db.Query<int>(QueriesRepository.CheckIfInstructorIsValid, new { InstructorID }).FirstOrDefault();
                CloseConnection();

                return data > 0;
            }
            catch (Exception ex)
            {
                CloseConnection();
                throw;
            }
        }

        public int? CheckIfInstructorIsInTrainingInTimeFrame(string InstructorID, int GroupID, DateTime StartDate, DateTime EndDate)
        {
            try
            {
                OpenConnection();
                int? data = _db.Query<int>(QueriesRepository.CheckIfInstructorIsInTrainingInTimeFrame, new { InstructorID, GroupID, StartDate, EndDate }).FirstOrDefault();
                CloseConnection();

                return data == 0 ? null : data;
            }
            catch (Exception ex)
            {
                CloseConnection();
                throw;
            }
        }

        public int? CheckIfInstructorIsInTrainingInTimeFrame(int InstructorID, int GroupID, DateTime StartDate, DateTime EndDate)
        {
            try
            {
                OpenConnection();
                int? data = _db.Query<int>(QueriesRepository.CheckIfInstructorIsInTrainingInTimeFrameByID, new { InstructorID, GroupID, StartDate, EndDate }).FirstOrDefault();
                CloseConnection();

                return data == 0 ? null : data;
            }
            catch (Exception ex)
            {
                CloseConnection();
                throw;
            }
        }

        public bool CheckIfInstructorIsRegisteredToTrainingID(int InstructorID, int TrainingID)
        {
            try
            {
                OpenConnection();
                int data = _db.Query<int>(QueriesRepository.CheckIfInstructorIsRegisteredToTrainingID, new { InstructorID, TrainingID }).FirstOrDefault();
                CloseConnection();

                return data > 0;
            }
            catch (Exception ex)
            {
                CloseConnection();
                throw;
            }
        }

        public Trips GetTripByGroupID(int ID)
        {
            try
            {
                OpenConnection();
                Trips data = _db.Query<Trips>(QueriesRepository.GetTripByGroupID, new { ID }).SingleOrDefault();
                CloseConnection();
                return data;
            }
            catch (Exception ex)
            {
                CloseConnection();
                throw;
            }
        }

        public bool DeleteInstrucotrFile(int FileID)
        {
            try
            {
                OpenConnection();
                int results = _db.Execute(QueriesRepository.DeleteInstructorFile, new { FileID });

                return results > 0;
            }
            catch (Exception ex)
            {
                CloseConnection();
                throw;
            }
        }

        public bool UpdateInstructorProfilePicture(int ID, string ImageURL)
        {
            try
            {
                OpenConnection();
                int results = _db.Execute(QueriesRepository.UpdateInstructorProfilePicture, new
                {
                    ImageURL,
                    ID
                });
                CloseConnection();

                return results > 0;
            }
            catch (Exception ex)
            {
                CloseConnection();
                throw;
            }
        }

        public bool UpdateGroupContactDetails(int ID, string ContactName, string ContactPhone)
        {
            try
            {
                OpenConnection();
                int results = _db.Execute(QueriesRepository.UpdateGroupContactDetails, new
                {
                    ContactName,
                    ContactPhone,
                    ID
                });
                CloseConnection();

                return results > 0;
            }
            catch (Exception ex)
            {
                CloseConnection();
                throw;
            }
        }

        public bool UpdateGroupFinalStatus(int ID, int FinalStatus)
        {
            try
            {
                OpenConnection();
                int results = _db.Execute(QueriesRepository.UpdateGroupFinalStatus, new
                {
                    FinalStatus,
                    ID
                });
                CloseConnection();

                return results > 0;
            }
            catch (Exception ex)
            {
                CloseConnection();
                throw;
            }
        }

        public Groups GetGroupByID(int ID)
        {
            try
            {
                OpenConnection();
                Groups data =
                    _db.Query<Groups>(
                        QueriesRepository.GetGroupByID, new { ID }).SingleOrDefault();
                CloseConnection();
                return data;
            }
            catch (Exception ex)
            {
                CloseConnection();
                throw;
            }
        }

        public List<T> GetRecords<T>(string query, object param)
        {
            try
            {
                OpenConnection();
                List<T> data =
                    _db.Query<T>(
                        query, param).ToList();
                CloseConnection();
                return data;
            }
            catch (Exception ex)
            {
                CloseConnection();
                throw;
            }
        }


        public bool ExecuteUpdateCommand(string query, object param)
        {

            try
            {
                OpenConnection();
                int results = _db.Execute(query, new { param });
                CloseConnection();
                return results > 0;
            }
            catch (Exception ex)
            {
                CloseConnection();
                throw;
            }
        }


        public bool CheckRecordsExists(string query, object param)
        {
            try
            {
                OpenConnection();
                int data =
                    _db.Query(
                        query, param).FirstOrDefault();
                CloseConnection();

                return data > 0;
            }
            catch (Exception ex)
            {
                CloseConnection();
                throw;
            }
        }

        public bool ExecuteCommand(string query, object param)
        {
            try
            {
                OpenConnection();
                int results = _db.Execute(query, param);
                CloseConnection();
                return results > 0;
            }
            catch (Exception ex)
            {
                CloseConnection();
                throw;
            }
        }

        public bool DeactivateSchool(int ID)
        {
            OpenConnection();
            using (var transaction = _db.BeginTransaction())
            {
                try
                {
                    int resultSchool = _db.Execute(QueriesRepository.DeactivateSchool, new { ID }, transaction: transaction);
                    int resultUser = _db.Execute(QueriesRepository.DeactivateUserbySchool, new { ID }, transaction: transaction);

                    transaction.Commit();
                    CloseConnection();
                    return resultSchool > 0 && resultUser > 0;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    CloseConnection();
                    throw;
                }
            }
        }

        public List<Schools> GetSchools(string InstituteID, string Name, string Email)
        {
            try
            {
                OpenConnection();
                List<Schools> data =
                    _db.Query<Schools>(
                        QueriesRepository.GetAllSchools, new { InstituteID, Name, Email }).ToList();
                CloseConnection();
                return data;
            }
            catch (Exception ex)
            {
                CloseConnection();
                throw;
            }
        }



        public int GetNumberOfRows(string table)
        {
            try
            {
                OpenConnection();
                int NumberOfRows = _db.Query<int>(String.Format(QueriesRepository.GetNumberOfRows, table)).SingleOrDefault();
                CloseConnection();
                return NumberOfRows;
            }
            catch (Exception ex)
            {
                CloseConnection();
                throw;
            }
        }

        public bool CheckGroupExternalIDExist(int GroupID, DateTime StartDate, DateTime EndDate)
        {
            try
            {
                OpenConnection();
                int data = _db.Query<int>(QueriesRepository.CheckGroupIDExist, new { GroupID, StartDate, EndDate }).FirstOrDefault();
                CloseConnection();

                return data > 0;
            }
            catch (Exception ex)
            {
                CloseConnection();
                throw;
            }
        }

        public bool UpdateGroupExternalID(int GroupID, int ID)
        {
            try
            {
                OpenConnection();
                int results = _db.Execute(QueriesRepository.UpdateGroupExternalID, new
                {
                    GroupID,
                    ID
                });
                CloseConnection();

                return results > 0;
            }
            catch (Exception ex)
            {
                CloseConnection();
                throw;
            }
        }

        public bool UpdateGroupAgencyID(int AgencyID, int ID)
        {
            try
            {
                OpenConnection();
                int results = _db.Execute(QueriesRepository.UpdateGroupAgencyID, new
                {
                    AgencyID,
                    ID
                });
                CloseConnection();

                return results > 0;
            }
            catch (Exception ex)
            {
                CloseConnection();
                throw;
            }
        }


        /// <summary>
        /// This methods is used to check if instructor is allocated to inital training. It is being used when registering instructor to initial training.
        /// This method will be ignored if the Role allowing changing between roles.
        /// </summary>
        /// <param name="InstructorID"></param>
        /// <param name="TrainingType"></param>
        /// <returns></returns>
        public bool CheckIfInstructorIsAllocatedToInitialTraining(int InstructorID, int TrainingType)
        {
            try
            {
                OpenConnection();
                int data = _db.Query<int>(QueriesRepository.CheckIfInstructorIsAlreadyAllocatedToInitialTraining, new { InstructorID, TrainingType }).FirstOrDefault();
                CloseConnection();

                return data > 0;
            }
            catch (Exception ex)
            {
                CloseConnection();
                throw;
            }
        }

        /// <summary>
        /// This method returs a full records of instructor specific training
        /// </summary>
        /// <param name="ID">This is the unique identifier of the instructor registered to training</param>
        /// <returns></returns>
        public InstructorTrainings GetInstructorTrainingRecordByID(int ID)
        {
            try
            {
                OpenConnection();
                InstructorTrainings data = _db.Query<InstructorTrainings>(QueriesRepository.GetInstructorTrainingRecordByID, new { ID }).SingleOrDefault();
                CloseConnection();
                return data;
            }
            catch (Exception ex)
            {
                CloseConnection();
                throw;
            }
        }

        public List<T> GetAgencyReport<T>(string query, DateTime StartDate, DateTime EndDate, int? AgencyID, int? Status)
        {
            try
            {
                OpenConnection();
                List<T> data =
                    _db.Query<T>(
                        query, new { StartDate, EndDate, AgencyID, Status }).ToList();
                CloseConnection();
                return data;
            }
            catch (Exception ex)
            {
                CloseConnection();
                throw;
            }
        }

        public bool AddSubSchool(int GroupID, int SchoolID, bool IsPrimary, int NumberOfInstructors)
        {
            try
            {
                OpenConnection();
                int results = _db.Execute(QueriesRepository.AddSubSchool, new
                {
                    GroupID,
                    SchoolID,
                    IsPrimary,
                    NumberOfInstructors
                });
                CloseConnection();

                return results > 0;
            }
            catch (Exception ex)
            {
                CloseConnection();
                throw;
            }
        }

        public bool AssosicateSchoolsForUser(int UserID, IEnumerable<int> Schools)
        {
            OpenConnection();
            using (var transaction = _db.BeginTransaction())
            {
                try
                {
                    int resultsInst = 0;
                    foreach (var SchoolID in Schools)
                    {
                        resultsInst += _db.Execute(QueriesRepository.AssociateSchoolToUserID, new
                        {
                            SchoolID,
                            UserID
                        },
                        transaction: transaction);
                    }


                    transaction.Commit();
                    CloseConnection();
                    return resultsInst > 0;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    CloseConnection();
                    throw;
                }
            }
        }

        public bool DeleteAssosicatedSchool(int ID)
        {
            try
            {
                OpenConnection();
                int results = _db.Execute(QueriesRepository.DeleteAssosicatedSchool, new { ID });
                CloseConnection();
                return results > 0;
            }
            catch (Exception ex)
            {
                CloseConnection();
                throw;
            }
        }

        public int UpdateCountyManagerGroupFileApproval(bool CountyManagerApproval, string CountyManagerComments, int FileID)
        {
            try
            {
                OpenConnection();
                int results = _db.Execute(QueriesRepository.UpdateCountyManagerFileApproval, new { CountyManagerApproval, CountyManagerComments, FileID });
                CloseConnection();

                return results;
            }
            catch (Exception ex)
            {
                CloseConnection();
                throw;
            }
        }

        /// <summary>
        /// Permanent deleting a gourp, this method works with transactionmeaning it must delete any thing prior deletion
        /// </summary>
        /// <param name="GroupID">The group Id to delete</param>
        public bool PermanentDeleteGroup(int GroupID)
        {
            OpenConnection();
            using (var transaction = _db.BeginTransaction())
            {
                try
                {
                    _db.Execute(QueriesRepository.PermenentDeleteGroupByID, new { GroupID },
                        commandType: CommandType.Text, transaction: transaction);
                    transaction.Commit();
                    CloseConnection();

                    return true;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    CloseConnection();
                    throw;
                }
            }
        }


        public bool UpdateAdministrative180FormUpdate(int ID, int FormStatus, DateTime UpdateDate)
        {
            try
            {
                OpenConnection();
                int results = _db.Execute(QueriesRepository.UpdateAdministrative180FormStatus, new
                {
                    FormStatus,
                    UpdateDate,
                    ID
                });
                CloseConnection();

                return results > 0;
            }
            catch (Exception ex)
            {
                CloseConnection();
                throw;
            }
        }

        public bool UpdateAdministrative180FormSchoolData(int ID, string SchoolEmail, string SchoolFax, string SchoolPhone, int Stream, bool IsSpecialEducation, string SchoolManager, string SchoolManagerPhone, string SchoolManagerEmail)
        {
            try
            {
                OpenConnection();
                int results = _db.Execute(QueriesRepository.UpdateAdministrative180FormSchoolData, new
                {
                    SchoolEmail,
                    SchoolFax,
                    SchoolPhone,
                    Stream,
                    IsSpecialEducation,
                    SchoolManager,
                    SchoolManagerEmail,
                    SchoolManagerPhone,
                    ID
                });
                CloseConnection();

                return results > 0;
            }
            catch (Exception ex)
            {
                CloseConnection();
                throw;
            }
        }

        public bool UpdateOperation(string query, object param)
        {
            try
            {
                OpenConnection();
                int results = _db.Execute(query, param, commandType: CommandType.Text);
                CloseConnection();

                return results > 0;
            }
            catch (Exception ex)
            {
                CloseConnection();
                throw;
            }
        }


        public int InsertOperation(string query, object param)
        {
            try
            {
                OpenConnection();
                int results = _db.Execute(query, param, commandType: CommandType.Text);
                CloseConnection();

                return results;
            }
            catch (Exception ex)
            {
                CloseConnection();
                throw;
            }
        }

        public int InsertOperationReturnsID(string query, object param)
        {
            try
            {
                OpenConnection();
                int results = _db.Query<int>(query, param, commandType: CommandType.Text).FirstOrDefault();
                CloseConnection();

                return results;
            }
            catch (Exception ex)
            {
                CloseConnection();
                throw;
            }
        }

        public PermissionRole GetUserPermissions(int UserID)
        {
            try
            {
                OpenConnection();
                var PermissionRoleDictionary = new Dictionary<int, PermissionRole>();
                PermissionRole role =
                    _db.Query<PermissionRole, PermissionBits, PermissionRole>(QueriesRepository.GetUserPermission, (permissionRole, PermissionBits) =>
                    {
                        PermissionRole _permissionRole;
                        if (!PermissionRoleDictionary.TryGetValue(permissionRole.ID, out _permissionRole))
                        {
                            _permissionRole = permissionRole;
                            _permissionRole.Permissions = new List<PermissionBits>();
                            PermissionRoleDictionary.Add(_permissionRole.ID, _permissionRole);
                        }
                        _permissionRole.Permissions.Add(PermissionBits);
                        return permissionRole;
                    }, new { ID = UserID }, splitOn: "PermissionBitID").FirstOrDefault();
                CloseConnection();
                return role;
            }
            catch (Exception ex)
            {
                CloseConnection();
                throw;
            }
        }

        public IEnumerable<PermissionRole> GetPermissionsRoles()
        {
            try
            {
                OpenConnection();
                var PermissionRoleDictionary = new Dictionary<int, PermissionRole>();
                IEnumerable<PermissionRole> roles =
                    _db.Query<PermissionRole, PermissionBits, PermissionRole>(QueriesRepository.GetPermissionRolesAndPermissions, (permissionRole, PermissionBits) =>
                    {
                        PermissionRole _permissionRole;
                        if (!PermissionRoleDictionary.TryGetValue(permissionRole.ID, out _permissionRole))
                        {
                            _permissionRole = permissionRole;
                            _permissionRole.Permissions = new List<PermissionBits>();
                            PermissionRoleDictionary.Add(_permissionRole.ID, _permissionRole);
                        }
                        _permissionRole.Permissions.Add(PermissionBits);
                        return permissionRole;
                    }, null, splitOn: "PermissionBitID").GroupBy(p => p.ID).Select(p => p.FirstOrDefault()).ToList();
                CloseConnection();
                return roles;
            }
            catch (Exception ex)
            {
                CloseConnection();
                throw;
            }
        }

        public PermissionRole GetPermissionsRole(int RoleID)
        {
            try
            {
                OpenConnection();
                var PermissionRoleDictionary = new Dictionary<int, PermissionRole>();
                PermissionRole role =
                    _db.Query<PermissionRole, PermissionBits, PermissionRole>(QueriesRepository.GetPermissionRolesAndPermissionsByRole, (permissionRole, PermissionBits) =>
                    {
                        PermissionRole _permissionRole;
                        if (!PermissionRoleDictionary.TryGetValue(permissionRole.ID, out _permissionRole))
                        {
                            _permissionRole = permissionRole;
                            _permissionRole.Permissions = new List<PermissionBits>();
                            PermissionRoleDictionary.Add(_permissionRole.ID, _permissionRole);
                        }
                        _permissionRole.Permissions.Add(PermissionBits);
                        return permissionRole;
                    }, new { RoleID }, splitOn: "PermissionBitID").GroupBy(p => p.ID).Select(p => p.FirstOrDefault()).FirstOrDefault();
                CloseConnection();
                return role;
            }
            catch (Exception ex)
            {
                CloseConnection();
                throw;
            }
        }

        public bool InsertPermissionRole(PermissionRole permissionRole, IEnumerable<PermissionBits> Permissions)
        {
            OpenConnection();
            using (var transaction = _db.BeginTransaction())
            {
                try
                {

                    //Insert new permission role

                    permissionRole.ID = _db.Query<int>(QueriesRepository.InsertPermissionRole, new
                    {
                        permissionRole.Name,
                        permissionRole.CreationDate,
                        permissionRole.UpdateDate
                    }, transaction).FirstOrDefault();

                    foreach (var permissionBit in Permissions)
                    {
                        permissionBit.PermissionRoleID = permissionRole.ID;
                        permissionBit.ID = _db.Query<int>(QueriesRepository.InsertPermissionBit, new
                        {
                            permissionBit.PermissionRoleID,
                            permissionBit.PermissionBit,
                            permissionBit.CreationDate,
                            permissionBit.UpdateDate
                        }, transaction).FirstOrDefault();

                    }

                    transaction.Commit();
                    CloseConnection();
                    return true;
                }
                catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
                {
                    transaction.Rollback();
                    CloseConnection();
                    return false;
                }
                catch (Exception e)
                {
                    transaction.Rollback();
                    CloseConnection();
                    return false;
                }
            }
        }

        public bool UpdatePermissionRole(PermissionRole permissionRole, IEnumerable<PermissionBits> Permissions)
        {
            OpenConnection();
            using (var transaction = _db.BeginTransaction())
            {
                try
                {

                    //Delete Permissions from permission role
                    int deleteResults = _db.Execute(QueriesRepository.DeletePermissionBitsByRole, new { permissionRole.ID }, transaction: transaction);

                    //Insert new permission role
                    int resutls = _db.Execute(QueriesRepository.UpdatePermissionRole, new
                    {
                        permissionRole.Name,
                        permissionRole.UpdateDate,
                        permissionRole.ID
                    }, transaction);

                    foreach (var permissionBit in Permissions)
                    {
                        permissionBit.PermissionRoleID = permissionRole.ID;
                        permissionBit.ID = _db.Query<int>(QueriesRepository.InsertPermissionBit, new
                        {
                            permissionBit.PermissionRoleID,
                            permissionBit.PermissionBit,
                            permissionBit.CreationDate,
                            permissionBit.UpdateDate
                        }, transaction).FirstOrDefault();

                    }

                    transaction.Commit();
                    CloseConnection();
                    return true;
                }
                catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
                {
                    transaction.Rollback();
                    CloseConnection();
                    return false;
                }
                catch (Exception e)
                {
                    transaction.Rollback();
                    CloseConnection();
                    return false;
                }
            }
        }

        public TransactionResult AddQuickExpedition(Expeditions _expeditionToAdd, Trips _trip, TripFlightAvilability tripFlightAvilability)
        {
            OpenConnection();
            using (var transaction = _db.BeginTransaction())
            {
                try
                {
                    TransactionResult transactionResult = new TransactionResult();
                    //Add expedition
                    int ExpeditionID = _db.Query<int>(QueriesRepository.AddExpedition, new
                    {
                        TripID = _expeditionToAdd.TripID,
                        NumberOFpassengers = _expeditionToAdd.NumberOFpassengers,
                        SchoolID = _expeditionToAdd.SchoolID,
                        BasesPerDay = _expeditionToAdd.BasesPerDay,
                        Manager = _expeditionToAdd.Manager,
                        ManagerPhone = _expeditionToAdd.ManagerPhone,
                        ManagerEmail = _expeditionToAdd.ManagerEmail,
                        ManagerCell = _expeditionToAdd.ManagerCell,
                        Comments = _expeditionToAdd.Comments,
                        Status = _expeditionToAdd.Status,
                        AgentStatus = _expeditionToAdd.AgentStatus,
                        AgentApproval = _expeditionToAdd.AgentApproval,
                        IsActive = _expeditionToAdd.IsActive,
                        CreationDate = _expeditionToAdd.CreationDate,
                        UpdateDate = _expeditionToAdd.UpdateDate,
                        UserID = _expeditionToAdd.UserID,
                        AgencyID = _expeditionToAdd.AgencyID
                    }, transaction: transaction).FirstOrDefault();
                    if (ExpeditionID == 0)
                    {
                        //Add Error
                        transactionResult.Errors.Add(TransactionErrors.ExpeditionCannotBeSaved);
                        transaction.Rollback();
                        CloseConnection();
                        return transactionResult;
                    }
                    _expeditionToAdd.ID = ExpeditionID;

                    //Adding passanges flights
                    ExpeditionsFlights _departureFlight = new ExpeditionsFlights();
                    _departureFlight.ExpeditionID = _expeditionToAdd.ID;

                    if (_trip.Flights.Where(t => t.TakeOffFromIsrael == true).FirstOrDefault() == null)
                    {
                        transactionResult.Errors.Add(TransactionErrors.NoDepartureFlightFound);
                        transaction.Rollback();
                        CloseConnection();
                        return transactionResult;
                    }
                    else if (tripFlightAvilability.AvailableSeats < 1 || tripFlightAvilability.AvailableSeats < _expeditionToAdd.NumberOFpassengers)
                    {
                        transactionResult.Errors.Add(TransactionErrors.NoAvailableSeatsOntheOutGoingFlight);
                        transaction.Rollback();
                        CloseConnection();
                        return transactionResult;
                    }
                    else
                    {
                        _departureFlight.FlightID = _trip.Flights.Where(t => t.TakeOffFromIsrael == true)
                                                                 .FirstOrDefault().ID;
                        _departureFlight.NumberOfPassengers = _expeditionToAdd.NumberOFpassengers;
                    }

                    int _departureFlightID = _db.Query<int>(QueriesRepository.AddExpeditionFlight, _departureFlight, transaction: transaction).FirstOrDefault();

                    if (_departureFlightID == 0)
                    {
                        transactionResult.Errors.Add(TransactionErrors.CannotAddOutGoingFlight);
                        transaction.Rollback();
                        CloseConnection();
                        return transactionResult;
                    }

                    //Adding retured flights
                    ExpeditionsFlights _returedFlight = new ExpeditionsFlights();
                    _returedFlight.ExpeditionID = _expeditionToAdd.ID;

                    if (_trip.Flights.Where(t => t.TakeOffFromIsrael == false).FirstOrDefault() == null)
                    {
                        transactionResult.Errors.Add(TransactionErrors.NoReturnFlightFound);
                        transaction.Rollback();
                        CloseConnection();
                        return transactionResult;
                    }
                    else if (tripFlightAvilability.AvailableSeats < 1 || tripFlightAvilability.AvailableSeats < _expeditionToAdd.NumberOFpassengers)
                    {
                        transactionResult.Errors.Add(TransactionErrors.NoAvailableSeatsOnTheRetunedFlight);
                        transaction.Rollback();
                        CloseConnection();
                        return transactionResult;
                    }
                    else
                    {
                        _returedFlight.NumberOfPassengers = _expeditionToAdd.NumberOFpassengers;
                        _returedFlight.FlightID = _trip.Flights.Where(t => t.TakeOffFromIsrael == false)
                                                                 .FirstOrDefault().ID;
                    }

                    int _returedFlightID = _db.Query<int>(QueriesRepository.AddExpeditionFlight, _returedFlight, transaction: transaction).FirstOrDefault();

                    if (_returedFlightID == 0)
                    {
                        transactionResult.Errors.Add(TransactionErrors.CannotAddReturningFlight);
                        transaction.Rollback();
                        CloseConnection();
                        return transactionResult;
                    }

                    transactionResult.Result = true;

                    transaction.Commit();
                    CloseConnection();
                    return transactionResult;

                }
                catch (Exception ex)
                {
                    TransactionResult transactionResult = new TransactionResult();
                    transactionResult.Errors.Add(TransactionErrors.OtherError);
                    transaction.Rollback();
                    CloseConnection();
                    return transactionResult;
                    throw;
                }
            }

            return null;
        }

        public TransactionResult MoveGroupToAnotherOrder(int OriginalGroupID, int OriginalOrderID, int SelectedOrderID)
        {
            OpenConnection();
            using (var transaction = _db.BeginTransaction())
            {
                try
                {
                    TransactionResult transactionResult = new TransactionResult();

                    // Revoke original Order and Release the Passengesrs and Buses
                    int cancelresult = _db.Execute(QueriesRepository.CancelOrderbyID, new { OrderStatus = (int)eExpeditionStatus.Rejected, OriginalOrderID }, transaction: transaction);

                    if (cancelresult == 0)
                    {
                        transactionResult.Result = false;
                        transactionResult.Errors.Add(TransactionErrors.CannotCancelOrder);
                        transaction.Rollback();
                        CloseConnection();
                        return transactionResult;
                    }

                    // Deleting and release passengers

                    int releasePassengers = _db.Execute(QueriesRepository.RemovePassengersByOrderID, new
                    {
                        OriginalOrderID
                    }, transaction: transaction);

                    if (releasePassengers == 0)
                    {
                        transactionResult.Result = false;
                        transactionResult.Errors.Add(TransactionErrors.CannotCancelOrder);
                        transaction.Rollback();
                        CloseConnection();
                        return transactionResult;
                    }

                    //Move group
                    int moveGroup = _db.Execute(QueriesRepository.MoveGroupToAnotherOrder, new
                    {
                        SelectedOrderID,
                        OriginalGroupID
                    }, transaction: transaction);

                    if (moveGroup == 0)
                    {
                        transactionResult.Result = false;
                        transactionResult.Errors.Add(TransactionErrors.CannotCancelOrder);
                        transaction.Rollback();
                        CloseConnection();
                        return transactionResult;
                    }

                    transactionResult.Result = true;
                    transaction.Commit();
                    CloseConnection();
                    return transactionResult;
                }
                catch (Exception ex)
                {
                    TransactionResult transactionResult = new TransactionResult();
                    transactionResult.Errors.Add(TransactionErrors.OtherError);
                    transaction.Rollback();
                    CloseConnection();
                    return transactionResult;
                }
            }

        }

    }
}
