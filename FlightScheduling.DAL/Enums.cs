﻿using FlightScheduling.Language;
using FlightScheduling.Language.CustomeExtentions;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlightScheduling.DAL
{
    public enum eSchoolStreams
    {
        [Display(ResourceType = typeof(SchoolsStrings), Name = "State")]
        [LocalizedDescription("State", typeof(SchoolsStrings))]
        State = 1,

        [Display(ResourceType = typeof(SchoolsStrings), Name = "ReligiousState")]
        [LocalizedDescription("ReligiousState", typeof(SchoolsStrings))]
        ReligiousState = 2,

        [Display(ResourceType = typeof(SchoolsStrings), Name = "Yeshiva")]
        [LocalizedDescription("Yeshiva", typeof(SchoolsStrings))]
        Yeshiva = 3,

        [Display(ResourceType = typeof(SchoolsStrings), Name = "Ulpana")]
        [LocalizedDescription("Ulpana", typeof(SchoolsStrings))]
        Ulpana = 4,
    }

    public enum eRoleType
    {
        [Display(ResourceType = typeof(AdministrationStrings), Name = "TripPlan")]
        [LocalizedDescription("TripPlan", typeof(AdministrationStrings))]
        TripPlan = 1,

        [Display(ResourceType = typeof(AdministrationStrings), Name = "PreparationPlan")]
        [LocalizedDescription("PreparationPlan", typeof(AdministrationStrings))]
        PreparationPlan = 2,

        [Display(ResourceType = typeof(AdministrationStrings), Name = "Administration")]
        [LocalizedDescription("Administration", typeof(AdministrationStrings))]
        Administration = 3,

        [Display(ResourceType = typeof(AdministrationStrings), Name = "Training")]
        [LocalizedDescription("Training", typeof(AdministrationStrings))]
        Training = 4,

        [Display(ResourceType = typeof(AdministrationStrings), Name = "Management")]
        [LocalizedDescription("Management", typeof(AdministrationStrings))]
        Management = 5
    }

    public enum eFormChapterType
    {
        Administration = 1,
        Instructors = 2,
        Plan = 3,
        Flights = 4,
        TripPlan = 5,
        Contacts = 6
    }

    public enum eSiteType
    {
        [Display(ResourceType = typeof(AdministrationStrings), Name = "SiteTypeHotel")]
        [LocalizedDescription("SiteTypeHotel", typeof(AdministrationStrings))]
        Hotel = 1,

        [Display(ResourceType = typeof(AdministrationStrings), Name = "SiteTypeSite")]
        [LocalizedDescription("SiteTypeSite", typeof(AdministrationStrings))]
        Site = 2,

        [Display(ResourceType = typeof(AdministrationStrings), Name = "SiteTypeResturant")]
        [LocalizedDescription("SiteTypeResturant", typeof(AdministrationStrings))]
        Resturant = 3,

        [Display(ResourceType = typeof(AdministrationStrings), Name = "SiteTypeSynagogue")]
        [LocalizedDescription("SiteTypeSynagogue", typeof(AdministrationStrings))]
        Synagogue = 4,

        [Display(ResourceType = typeof(AdministrationStrings), Name = "SiteTypeAirport")]
        [LocalizedDescription("SiteTypeAirport", typeof(AdministrationStrings))]
        Airport = 5,

        [Display(ResourceType = typeof(AdministrationStrings), Name = "SiteTypeCemetery")]
        [LocalizedDescription("SiteTypeCemetery", typeof(AdministrationStrings))]
        Cemetery = 6,

        [Display(ResourceType = typeof(AdministrationStrings), Name = "SiteTypeCamp")]
        [LocalizedDescription("SiteTypeCamp", typeof(AdministrationStrings))]
        Camp = 7,

        [Display(ResourceType = typeof(AdministrationStrings), Name = "SiteTypeMall")]
        [LocalizedDescription("SiteTypeMall", typeof(AdministrationStrings))]
        Mall = 8,

        [Display(ResourceType = typeof(AdministrationStrings), Name = "SiteTypeHall")]
        [LocalizedDescription("SiteTypeHall", typeof(AdministrationStrings))]
        Hall = 9,


    }

    public enum eActivityType
    {
        [Display(ResourceType = typeof(AdministrationStrings), Name = "SiteTypeHotel")]
        [LocalizedDescription("SiteTypeHotel", typeof(AdministrationStrings))]
        Hotel = 1,

        [Display(ResourceType = typeof(AdministrationStrings), Name = "SiteTypeSite")]
        [LocalizedDescription("SiteTypeSite", typeof(AdministrationStrings))]
        Site = 2,

        [Display(ResourceType = typeof(AdministrationStrings), Name = "SiteTypeResturant")]
        [LocalizedDescription("SiteTypeResturant", typeof(AdministrationStrings))]
        Resturant = 3,

        [Display(ResourceType = typeof(AdministrationStrings), Name = "SiteTypeSynagogue")]
        [LocalizedDescription("SiteTypeSynagogue", typeof(AdministrationStrings))]
        Synagogue = 4,

        [Display(ResourceType = typeof(GroupFormsStrings), Name = "ActivityTypeDrive")]
        [LocalizedDescription("ActivityTypeDrive", typeof(GroupFormsStrings))]
        Drive = 5,

        [Display(ResourceType = typeof(GroupFormsStrings), Name = "ActivityTypeActivity")]
        [LocalizedDescription("ActivityTypeActivity", typeof(GroupFormsStrings))]
        Activity = 6,

        [Display(ResourceType = typeof(GroupFormsStrings), Name = "ActivityTypeLunch")]
        [LocalizedDescription("ActivityTypeLunch", typeof(GroupFormsStrings))]
        Lunch = 7,

        [Display(ResourceType = typeof(GroupFormsStrings), Name = "ActivityTypeRestroomBreak")]
        [LocalizedDescription("ActivityTypeRestroomBreak", typeof(GroupFormsStrings))]
        RestroomBreak = 8,

        [Display(ResourceType = typeof(GroupFormsStrings), Name = "ActivityTypePreparationTime")]
        [LocalizedDescription("ActivityTypePreparationTime", typeof(GroupFormsStrings))]
        PreparationTime = 9,

        [Display(ResourceType = typeof(GroupFormsStrings), Name = "WakeUp")]
        [LocalizedDescription("WakeUp", typeof(GroupFormsStrings))]
        WakeUp = 10,

        [Display(ResourceType = typeof(GroupFormsStrings), Name = "LightsOut")]
        [LocalizedDescription("LightsOut", typeof(GroupFormsStrings))]
        LightsOut = 11,

        [Display(ResourceType = typeof(GroupFormsStrings), Name = "NightDiscussion")]
        [LocalizedDescription("NightDiscussion", typeof(GroupFormsStrings))]
        NightDiscussion = 12,

        [Display(ResourceType = typeof(GroupFormsStrings), Name = "ActivityTypeWalking")]
        [LocalizedDescription("ActivityTypeWalking", typeof(GroupFormsStrings))]
        Walking = 13,

        [Display(ResourceType = typeof(GroupFormsStrings), Name = "ActivityTypeCeremony")]
        [LocalizedDescription("ActivityTypeCeremony", typeof(GroupFormsStrings))]
        Ceremony = 14,
    }

    public enum eFlightDirection
    {
        [Display(ResourceType = typeof(GroupFormsStrings), Name = "Going")]
        [LocalizedDescription("Going", typeof(GroupFormsStrings))]
        Going = 1,

        [Display(ResourceType = typeof(GroupFormsStrings), Name = "Return")]
        [LocalizedDescription("Return", typeof(GroupFormsStrings))]
        Return = 2,
    }

    public enum eEntityType
    {
        [Display(ResourceType = typeof(AdministrationStrings), Name = "SchoolEntity")]
        [LocalizedDescription("SchoolEntity", typeof(AdministrationStrings))]
        School = 1
    }

    public enum eClassType
    {
        [Display(ResourceType = typeof(CommonStrings), Name = "None")]
        [LocalizedDescription("None", typeof(CommonStrings))]
        None = 0,

        [Display(ResourceType = typeof(GroupFormsStrings), Name = "ElevenGrade")]
        [LocalizedDescription("ElevenGrade", typeof(AdministrationStrings))]
        ElevenGrade = 1,

        [Display(ResourceType = typeof(GroupFormsStrings), Name = "TwelveGrade")]
        [LocalizedDescription("TwelveGrade", typeof(AdministrationStrings))]
        TwelveGrade = 2,

        [Display(ResourceType = typeof(GroupFormsStrings), Name = "BothGrade")]
        [LocalizedDescription("BothGrade", typeof(AdministrationStrings))]
        BothGrade = 3,
    }

    public enum eParticipantClassType
    {
        [Display(ResourceType = typeof(CommonStrings), Name = "None")]
        [LocalizedDescription("None", typeof(CommonStrings))]
        None = 0,

        [Display(ResourceType = typeof(GroupsStrings), Name = "ElevenGrade")]
        [LocalizedDescription("ElevenGrade", typeof(GroupsStrings))]
        ElevenGrade = 1,

        [Display(ResourceType = typeof(GroupsStrings), Name = "TwelveGrade")]
        [LocalizedDescription("TwelveGrade", typeof(GroupsStrings))]
        TwelveGrade = 2
    }
}
