﻿using FlightScheduling.DAL.Abstractions;

namespace FlightScheduling.DAL.DomainEvents
{
    public class UserCreatedDomainEvent : IDomainEvent
    {
        public int Id { get; set; }

        public UserCreatedDomainEvent(int id)
        {
            Id = id;
        }
    }
}