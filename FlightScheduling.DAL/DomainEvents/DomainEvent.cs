﻿using System;
using FlightScheduling.DAL.Abstractions;
using FlightScheduling.DAL.CustomEntities.Outbox;
using Newtonsoft.Json;

namespace FlightScheduling.DAL.DomainEvents
{
    public static class DomainEventExtensions
    {
        public static Outbox GetOutboxFromDomainEvent(IDomainEvent domainEvent)
        {
            var outbox = new Outbox(
                domainEvent.GetType().Name,
                DateTime.Now,
                null,
                JsonConvert.SerializeObject(domainEvent, new JsonSerializerSettings()
                {
                    TypeNameHandling = TypeNameHandling.All
                }), String.Empty);

            return outbox;
        }
    }
}