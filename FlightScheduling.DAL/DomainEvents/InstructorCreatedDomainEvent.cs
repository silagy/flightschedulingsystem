﻿using FlightScheduling.DAL.Abstractions;

namespace FlightScheduling.DAL.DomainEvents
{
    public class InstructorCreatedDomainEvent: IDomainEvent
    {
        public int InstructorId { get; set; }

        public InstructorCreatedDomainEvent(int instructorId)
        {
            InstructorId = instructorId;
        }
    }
}