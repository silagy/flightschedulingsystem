﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlightScheduling.DAL
{
    public static class DBHelpers
    {
        public static object ToDbNull(this object value)
        {
            if (value != null)
            {
                return value;
            }

            return DBNull.Value;
        }
    }
}
