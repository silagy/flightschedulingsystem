﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FlightScheduling.Language;
using FlightScheduling.Language.CustomeExtentions;

namespace FlightScheduling.DAL
{
    [MetadataType(typeof(TrainingsMetaData))]
    public partial class Trainings
    {

        public eTrainingLocation TrainingLocationEnum
        {
            get { return (eTrainingLocation)this.TrainingLocation; }

            set { this.TrainingLocation = (int)value; }
        }

        public eTrainingType TrainingTypeEnum
        {
            get { return (eTrainingType)this.TrainingType; }

            set { this.TrainingType = (int)value; }
        }

        public eSchoolRegion? TrainingRegion
        {
            get { if (RegionID != null)
                {
                    return (eSchoolRegion)this.RegionID;
                }

                return null;
            }

            set { this.RegionID = (int)value; }
        }


        //data annotations here
        public class TrainingsMetaData
        {


        }

    }

 
    public enum eTrainingLocation
    {
        [Display(ResourceType = typeof(InstructorsStrings), Name = "Region")]
        [LocalizedDescription("Region", typeof(InstructorsStrings))]
        Region = 1,

        [Display(ResourceType = typeof(InstructorsStrings), Name = "HolocostInstitute")]
        [LocalizedDescription("HolocostInstitute", typeof(InstructorsStrings))]
        HolocostInstitute = 2,

        [Display(ResourceType = typeof(InstructorsStrings), Name = "Headquaters")]
        [LocalizedDescription("Headquaters", typeof(InstructorsStrings))]
        Headquaters = 3,

        [Display(ResourceType = typeof(InstructorsStrings), Name = "Other")]
        [LocalizedDescription("Other", typeof(InstructorsStrings))]
        Other = 4
    }

    public enum eTrainingType
    {
        [Display(ResourceType = typeof(InstructorsStrings), Name = "InitialTraining")]
        [LocalizedDescription("InitialTraining", typeof(InstructorsStrings))]
        InitialTraining = 1,

        [Display(ResourceType = typeof(InstructorsStrings), Name = "RenewalTraining")]
        [LocalizedDescription("RenewalTraining", typeof(InstructorsStrings))]
        RenewalTraining = 2
    }
}
