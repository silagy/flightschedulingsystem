﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FlightScheduling.Language;
using FlightScheduling.Language.CustomeExtentions;

namespace FlightScheduling.DAL
{
    [MetadataType(typeof(GroupsMetaData))]
    public partial class Groups
    {

        public eFinalStatus FinsalStatusEnum
        {
            get { return (eFinalStatus)this.FinalStatus; }

            set { this.FinalStatus = (int)value; }
        }


        //data annotations here
        public class GroupsMetaData
        {


        }

    }

    public enum eFinalStatus
    {
        [Display(ResourceType = typeof(GroupsStrings), Name = "PendingApproval")]
        [LocalizedDescription("PendingApproval", typeof(GroupsStrings))]
        PendingApproval = 1,

        [Display(ResourceType = typeof(GroupsStrings), Name = "Approved")]
        [LocalizedDescription("Approved", typeof(GroupsStrings))]
        Approved = 2,

        [Display(ResourceType = typeof(GroupsStrings), Name = "Cancelled")]
        [LocalizedDescription("Cancelled", typeof(GroupsStrings))]
        Cancelled = 3
    }
}
