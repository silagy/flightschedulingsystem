﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FlightScheduling.Language;
using FlightScheduling.Language.CustomeExtentions;

namespace Flight.DAL.FlightDB.Methods
{
    public enum eRole
    {
        [Display(ResourceType = typeof(AccountStrings), Name = "Administrator")]
        [LocalizedDescription("Administrator", typeof(AccountStrings))]
        Administrator = 1,

        [Display(ResourceType = typeof(AccountStrings), Name = "Agent")]
        [LocalizedDescription("Agent", typeof(AccountStrings))]
        Agent = 2,

        [Display(ResourceType = typeof(AccountStrings), Name = "Staf")]
        [LocalizedDescription("Staf", typeof(AccountStrings))]
        Staf = 3,

        [Display(ResourceType = typeof(AccountStrings), Name = "SchoolManager")]
        [LocalizedDescription("SchoolManager", typeof(AccountStrings))]
        SchoolManager = 4,

        [Display(ResourceType = typeof(AccountStrings), Name = "Viewer")]
        [LocalizedDescription("Viewer", typeof(AccountStrings))]
        Viewer = 5,

        [Display(ResourceType = typeof(AccountStrings), Name = "TrainingManager")]
        [LocalizedDescription("TrainingManager", typeof(AccountStrings))]
        TrainingManager = 6,

        [Display(ResourceType = typeof(AccountStrings), Name = "CountyManager")]
        [LocalizedDescription("CountyManager", typeof(AccountStrings))]
        CountyManager = 7,

        [Display(ResourceType = typeof(AccountStrings), Name = "GroupManager")]
        [LocalizedDescription("GroupManager", typeof(AccountStrings))]
        GroupManager = 8,
        
        [Display(ResourceType = typeof(AccountStrings), Name = "Instructor")]
        [LocalizedDescription("Instructor", typeof(AccountStrings))]
        Instructor = 9
    }

    public enum eGroupFileType
    {
        [Display(ResourceType = typeof(GroupsStrings), Name = "FileType180170")]
        [LocalizedDescription("FileType180170", typeof(GroupsStrings))]
        F180170 = 1,
        [Display(ResourceType = typeof(GroupsStrings), Name = "FileType60")]
        [LocalizedDescription("FileType60", typeof(GroupsStrings))]
        F60 = 2,
        [Display(ResourceType = typeof(GroupsStrings), Name = "FileTypeFinalPDF")]
        [LocalizedDescription("FileTypeFinalPDF", typeof(GroupsStrings))]
        FinalPDF = 3,
        [Display(ResourceType = typeof(GroupsStrings), Name = "FileTypeOther")]
        [LocalizedDescription("FileTypeOther", typeof(GroupsStrings))]
        Other = 4,
        [Display(ResourceType = typeof(GroupsStrings), Name = "ManagerStatement")]
        [LocalizedDescription("ManagerStatement", typeof(GroupsStrings))]
        ManagerStatement = 5,

        [Display(ResourceType = typeof(GroupsStrings), Name = "SperatingSingleParticipant")]
        [LocalizedDescription("SperatingSingleParticipant", typeof(GroupsStrings))]
        SperatingSingleParticipant = 6,

        [Display(ResourceType = typeof(GroupsStrings), Name = "SplitSHabat")]
        [LocalizedDescription("SplitSHabat", typeof(GroupsStrings))]
        SplitSHabat = 7,

        [Display(ResourceType = typeof(GroupsStrings), Name = "SplitEducationActivity")]
        [LocalizedDescription("SplitEducationActivity", typeof(GroupsStrings))]
        SplitEducationActivity = 8,

        [Display(ResourceType = typeof(GroupsStrings), Name = "RoomRoster")]
        [LocalizedDescription("RoomRoster", typeof(GroupsStrings))]
        RoomRoster = 9,

        [Display(ResourceType = typeof(GroupsStrings), Name = "SpecialRequests")]
        [LocalizedDescription("SpecialRequests", typeof(GroupsStrings))]
        SpecialRequests = 10,

        [Display(ResourceType = typeof(GroupsStrings), Name = "BusDistribution")]
        [LocalizedDescription("BusDistribution", typeof(GroupsStrings))]
        BusDistribution = 11,

        [Display(ResourceType = typeof(GroupsStrings), Name = "SpecialFood")]
        [LocalizedDescription("SpecialFood", typeof(GroupsStrings))]
        SpecialFood = 12,
    }

    public enum eGroupFileStatus
    {
        [Display(ResourceType = typeof(GroupsStrings), Name = "New")]
        [LocalizedDescription("New", typeof(GroupsStrings))]
        New = 1,
        [Display(ResourceType = typeof(GroupsStrings), Name = "Approved")]
        [LocalizedDescription("Approved", typeof(GroupsStrings))]
        Approved = 2,
        [Display(ResourceType = typeof(GroupsStrings), Name = "InProgress")]
        [LocalizedDescription("InProgress", typeof(GroupsStrings))]
        InProgress = 3,
        [Display(ResourceType = typeof(GroupsStrings), Name = "Cancelled")]
        [LocalizedDescription("Cancelled", typeof(GroupsStrings))]
        Cancelled = 4
    }
}
