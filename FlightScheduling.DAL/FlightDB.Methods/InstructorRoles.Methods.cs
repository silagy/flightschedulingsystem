﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FlightScheduling.Language;
using FlightScheduling.Language.CustomeExtentions;

namespace FlightScheduling.DAL
{
    [MetadataType(typeof(InstructorRolesMetaData))]
    public partial class InstructorRoles
    {

        public eStartingValidity StartingValidityEnum
        {
            get { return (eStartingValidity)this.StartingValidity; }

            set { this.StartingValidity = (int)value; }
        }


        //data annotations here
        public class InstructorRolesMetaData
        {


        }

    }

 
    public enum eStartingValidity
    {
        [Display(ResourceType = typeof(InstructorsStrings), Name = "BeginningOfYear")]
        [LocalizedDescription("BeginningOfYear", typeof(InstructorsStrings))]
        BeginningOfYear = 1,

        [Display(ResourceType = typeof(InstructorsStrings), Name = "CompletionDate")]
        [LocalizedDescription("CompletionDate", typeof(InstructorsStrings))]
        CompletionDate = 2
    }
}
