﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FlightScheduling.Language;
using FlightScheduling.Language.CustomeExtentions;

namespace FlightScheduling.DAL
{
    [MetadataType(typeof(SchoolsMetaData))]
    public partial class Schools
    {

        public eSchoolRegion? RegionEnum
        {
            get
            {
                if (this.Region != null)
                {
                    return (eSchoolRegion)this.Region;
                }

                return null;
            }

            set { this.Region = (int)value; }
        }


        //data annotations here
        public class SchoolsMetaData
        {


        }

    }

 
    public enum eSchoolRegion
    {
        [Display(ResourceType = typeof(CommonStrings), Name = "None")]
        [LocalizedDescription("None", typeof(CommonStrings))]
        None = 0,

        [Display(ResourceType = typeof(SchoolsStrings), Name = "NorthRegion")]
        [LocalizedDescription("NorthRegion", typeof(SchoolsStrings))]
        NorthRegion = 1,

        [Display(ResourceType = typeof(SchoolsStrings), Name = "CenterRegion")]
        [LocalizedDescription("CenterRegion", typeof(SchoolsStrings))]
        CenterRegion = 2,

        [Display(ResourceType = typeof(SchoolsStrings), Name = "SouthRegion")]
        [LocalizedDescription("SouthRegion", typeof(SchoolsStrings))]
        SouthRegion = 3,

        [Display(ResourceType = typeof(SchoolsStrings), Name = "HifaRegion")]
        [LocalizedDescription("HifaRegion", typeof(SchoolsStrings))]
        HifaRegion = 4,

        [Display(ResourceType = typeof(SchoolsStrings), Name = "TelAvivRegion")]
        [LocalizedDescription("TelAvivRegion", typeof(SchoolsStrings))]
        TelAvivRegion = 5,

        [Display(ResourceType = typeof(SchoolsStrings), Name = "JerusalemRegion")]
        [LocalizedDescription("JerusalemRegion", typeof(SchoolsStrings))]
        JerusalemRegion = 6,

        [Display(ResourceType = typeof(SchoolsStrings), Name = "SettlementsRegion")]
        [LocalizedDescription("SettlementsRegion", typeof(SchoolsStrings))]
        SettlementsRegion = 7,

        [Display(ResourceType = typeof(SchoolsStrings), Name = "OrthodoxRegion")]
        [LocalizedDescription("OrthodoxRegion", typeof(SchoolsStrings))]
        OrthodoxRegion = 8,

        [Display(ResourceType = typeof(SchoolsStrings), Name = "CountryRegion")]
        [LocalizedDescription("CountryRegion", typeof(SchoolsStrings))]
        CountryRegion = 9,

        [Display(ResourceType = typeof(SchoolsStrings), Name = "WorkOffice")]
        [LocalizedDescription("WorkOffice", typeof(SchoolsStrings))]
        WorkOffice = 10
    }
}
