﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Flight.DAL.FlightDB.Methods;
using FlightScheduling.Language;

namespace FlightScheduling.DAL
{
    [MetadataType(typeof(MetadataGroupFiles))]
    public partial class GroupFiles
    {
        public eGroupFileType GroupFileTypeEnum
        {
            get { return (eGroupFileType)this.FileType; }
            set { this.FileType = (int)value; }
        }

        public eGroupFileStatus GroupFileStatusEnum
        {
            get { return (eGroupFileStatus)this.FileStatus; }
            set { this.FileStatus = (int)value; }
        }

        public class MetadataGroupFiles
        {
            [Display(ResourceType = typeof(GroupsStrings), Name = "FileName")]
            public string Name { get; set; }

            [Display(ResourceType = typeof(GroupsStrings), Name = "FileComments")]
            public string Comments { get; set; }
        }
    }
}
