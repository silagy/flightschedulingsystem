﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FlightScheduling.Language;
using FlightScheduling.Language.CustomeExtentions;

namespace FlightScheduling.DAL
{
    [MetadataType(typeof(InstructorsMetaData))]
    public partial class Instructors
    {

        //public eInstructorRoleType InstructorRoleEnum
        //{
        //    get { return (eInstructorRoleType)this.RoleID; }

        //    set { this.RoleID = (int)value; }
        //}

        public eOrganizationalStatus OrganizationalStatusEnum
        {
            get { return (eOrganizationalStatus) this.OrganizationalStatus; }
            set { this.OrganizationalStatus = (int) value; }
        }

        public eInstructorValidity InstructorValidity
        {
            get
            {
                if (this.ValidityEndTime == null)
                {
                    return eInstructorValidity.NoRecords;
                }else if (this.ValidityEndTime >= DateTime.Now)
                {
                    return eInstructorValidity.Valid;
                }else
                {
                    return eInstructorValidity.NotValid;
                }
            }
        }


        //data annotations here
        public class InstructorsMetaData
        {


        }

    }

    public enum eOrganizationalStatus
    {
        [Display(ResourceType = typeof(InstructorsStrings), Name = "NotRelevant")]
        [LocalizedDescription("NotRelevant", typeof(InstructorsStrings))]
        NotRelevant = 1,

        [Display(ResourceType = typeof(InstructorsStrings), Name = "Oz")]
        [LocalizedDescription("Oz", typeof(InstructorsStrings))]
        Oz = 2,

        [Display(ResourceType = typeof(InstructorsStrings), Name = "NewHorizon")]
        [LocalizedDescription("NewHorizon", typeof(InstructorsStrings))]
        NewHorizon = 3
    }

    public enum eInstructorRoleType
    {
        [Display(ResourceType = typeof(InstructorsStrings), Name = "Instructor")]
        [LocalizedDescription("Instructor", typeof(InstructorsStrings))]
        Instructor = 1,

        [Display(ResourceType = typeof(InstructorsStrings), Name = "AccompanyingMentor")]
        [LocalizedDescription("AccompanyingMentor", typeof(InstructorsStrings))]
        AccompanyingMentor = 2,

        [Display(ResourceType = typeof(InstructorsStrings), Name = "ExpeditionManager")]
        [LocalizedDescription("ExpeditionManager", typeof(InstructorsStrings))]
        ExpeditionManager = 3
    }

    public enum eInstructorValidity
    {
        [Display(ResourceType = typeof(InstructorsStrings), Name = "Valid")]
        [LocalizedDescription("Valid", typeof(InstructorsStrings))]
        Valid = 1,

        [Display(ResourceType = typeof(InstructorsStrings), Name = "NotValid")]
        [LocalizedDescription("NotValid", typeof(InstructorsStrings))]
        NotValid = 2,

        [Display(ResourceType = typeof(InstructorsStrings), Name = "NoRecords")]
        [LocalizedDescription("NoRecords", typeof(InstructorsStrings))]
        NoRecords = 3
    }
}
