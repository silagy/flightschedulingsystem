﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FlightScheduling.Language;
using FlightScheduling.Language.CustomeExtentions;

namespace FlightScheduling.DAL
{
    [MetadataType(typeof(MetadataTrip))]
    public partial class Trips
    {

        public eFlightsFields DestenationEnum
        {
            get { return (eFlightsFields) DestinationID; }
            set { DestinationID = (int) value; }
        }

        public eFlightsFields ReturingFieldEnum
        {
            get { return (eFlightsFields)ReturingField; }
            set { ReturingField = (int)value; }
        }

        //data annotations here
        public class MetadataTrip
        {
            
        }
    }


    public enum eFlightsFields
    {
        [Display(ResourceType = typeof(FlightsStrings), Name = "Israel")]
        [LocalizedDescription("Israel", typeof(FlightsStrings))]
        Israel = 1,

        [Display(ResourceType = typeof(FlightsStrings), Name = "Warsaw")]
        [LocalizedDescription("Warsaw", typeof(FlightsStrings))]
        Warsaw = 2,

        [Display(ResourceType = typeof(FlightsStrings), Name = "Krakow")]
        [LocalizedDescription("Krakow", typeof(FlightsStrings))]
        Krakow = 3,

        [Display(ResourceType = typeof(FlightsStrings), Name = "Katowice")]
        [LocalizedDescription("Katowice", typeof(FlightsStrings))]
        Katowice = 4,

        [Display(ResourceType = typeof(FlightsStrings), Name = "Lodge")]
        [LocalizedDescription("Lodge", typeof(FlightsStrings))]
        Lodge = 5,

        [Display(ResourceType = typeof(FlightsStrings), Name = "Wroclaw")]
        [LocalizedDescription("Wroclaw", typeof(FlightsStrings))]
        Wroclaw = 6,

        [Display(ResourceType = typeof(FlightsStrings), Name = "Rzeszow")]
        [LocalizedDescription("Rzeszow", typeof(FlightsStrings))]
        Rzeszow = 7,

        [Display(ResourceType = typeof(FlightsStrings), Name = "Loblin")]
        [LocalizedDescription("Loblin", typeof(FlightsStrings))]
        Loblin = 8,
        
        [Display(ResourceType = typeof(FlightsStrings), Name = "Vilana")]
        [LocalizedDescription("Vilana", typeof(FlightsStrings))]
        Vilana = 9,

        [Display(ResourceType = typeof(FlightsStrings), Name = "Riga")]
        [LocalizedDescription("Riga", typeof(FlightsStrings))]
        Riga = 10,

        [Display(ResourceType = typeof(FlightsStrings), Name = "Saloniki")]
        [LocalizedDescription("Saloniki", typeof(FlightsStrings))]
        Saloniki = 11,

        [Display(ResourceType = typeof(FlightsStrings), Name = "Athens")]
        [LocalizedDescription("Athens", typeof(FlightsStrings))]
        Athens = 12,

        [Display(ResourceType = typeof(FlightsStrings), Name = "Munich")]
        [LocalizedDescription("Munich", typeof(FlightsStrings))]
        Munich = 13,

        [Display(ResourceType = typeof(FlightsStrings), Name = "Berlin")]
        [LocalizedDescription("Berlin", typeof(FlightsStrings))]
        Berlin = 14,
    }
}
