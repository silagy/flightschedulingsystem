﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Flight.DAL.FlightDB.Methods;
using FlightScheduling.Language;
using Otakim.Language;

namespace FlightScheduling.DAL
{
    [MetadataType(typeof(MetadataUser))]
    public partial class Users
    {


        [Display(ResourceType = typeof(AccountStrings), Name = "FullName")]
        public string FullName
        {
            get
            {
                return Firstname + " " + Lastname;
            }
        }

        public eRole RuleEnum
        {
            get { return (eRole)this.UserType; }
            set { this.UserType = (int)value; }
        }

        public eRoleType? RoleTypeEnum
        {
            get
            {
                if (UserRole != null)
                {
                    return (eRoleType)this.UserRole;
                }
                else
                {
                    return null;
                }
            }
            set { this.UserRole = (int)value; }
        }


        //data annotations here
        public class MetadataUser
        {
            [Display(ResourceType = typeof(AccountStrings), Name = "EmailAddress")]
            public string Email { get; set; }

        }

    }


}
