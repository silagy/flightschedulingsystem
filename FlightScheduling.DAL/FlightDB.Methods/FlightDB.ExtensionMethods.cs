﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Migrations;
using System.Data.Entity.Validation;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Flight.DAL;
using Flight.DAL.FlightDB.Methods;
using FlightScheduling.DAL;

namespace FlightScheduling.DAL
{
    public static class FLightDbExtensionMethods
    {
        //Allows: from Orders in model.Order.Include("User", "Manager")
        public static ObjectQuery<T> Include<T>(this ObjectQuery<T> query, params string[] paths)
        {

            foreach (string path in paths)
            {

                query = query.Include(path);

            }

            return query;

        }

        public static DbQuery<T> Include<T>(this DbQuery<T> orders, string[] includes)
        {
            foreach (var include in includes)
            {
                orders = orders.Include(include);
            }

            return orders;

        }





        public static Users GetUserByLoginName(this FlightDB i_Context, string i_LoginName)
        {
            Users loggedInUser;

            loggedInUser = i_Context.Users.SingleOrDefault(u => u.Email == i_LoginName);
            return loggedInUser;
        }

        public static bool CheckIfUsersExists(this FlightDB i_Context, string i_loginname)
        {
            return i_Context.Users.Any(u => u.Email == i_loginname);
        }


        public static Users CreateUser(this FlightDB i_context, Users i_User)
        {
            try
            {
                i_User.IsActive = true;

                i_context.Users.Add(i_User);
                int results = i_context.SaveChanges();
                if (results > 0)
                {

                    return i_User;
                }

                return null;
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }
                throw;
            }

        }

        public static Users UpdateUser(this FlightDB i_context, Users i_User)
        {
            try
            {
                i_context.Entry(i_User).State = EntityState.Modified;
                //i_context.Users.AddOrUpdate(i_User);
                i_context.Users.Attach(i_User);
                int results = i_context.SaveChanges();
                if (results > 0)
                {

                    //User userToReturns =
                    //i_context.Users.Include("Roles").Where(u => u.Username == i_User.Username).FirstOrDefault();

                    return i_User;
                }

                return null;
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }
                throw;
            }

        }

        public static Users GetUserByID(this FlightDB i_context, int UserID)
        {
            return i_context.Users.FirstOrDefault(u => u.ID == UserID);
        }



        public static IEnumerable<Users> RemoveDeletedUsers(this DbQuery<Users> users)
        {
            return users.Where(o => o.IsActive == true);

        }

        public static IEnumerable<Agencies> RemoveInActiveAgencies(this DbQuery<Agencies> agencies)
        {
            return agencies.Where(o => o.IsActive == true);

        }

        public static IEnumerable<Flights> RemoveInActiveFlights(this DbQuery<Flights> agencies)
        {
            return agencies.Where(o => o.IsActive == true);

        }

        public static IEnumerable<Schools> RemoveInActive(this DbQuery<Schools> schools)
        {
            return schools.Where(o => o.IsActive == true);

        }

        public static IEnumerable<Trips> RemoveInActive(this IQueryable<Trips> schools)
        {
            return schools.Where(o => o.IsActive == true);

        }

        public static IEnumerable<T> RemoveInActive<T>(this IQueryable<T> collection)
        {
            Type t = collection.GetType();
            var arg = Expression.Parameter(typeof(T), "p");
            MemberExpression mem = Expression.Property(arg, "IsActive");
            Expression searchExpression = Expression.Equal(mem, Expression.Constant(true));
            var predicate = Expression.Lambda<Func<T, bool>>(searchExpression, arg);

            return collection.Where(predicate);
        }


        private static MemberExpression GetPropertyAccess(ParameterExpression arg, string Property)
        {
            string[] parts = Property.Split('.');

            MemberExpression property = Expression.Property(arg, parts[0]);

            for (int i = 1; i < parts.Length; i++)
            {
                property = Expression.Property(property, parts[i]);
            }

            return property;
        }

       

    }


}
