﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FlightScheduling.Language;
using FlightScheduling.Language.CustomeExtentions;

namespace FlightScheduling.DAL
{
    [MetadataType(typeof(ExpeditionMetaData))]
    public partial class Expeditions
    {

        public eExpeditionStatus ExpeditionStatusEnum
        {
            get { return (eExpeditionStatus) this.Status; }

            set { this.Status = (int) value; }
        }
        


        //data annotations here
        public class ExpeditionMetaData
        {
            

        }

    }

    public enum eExpeditionStatus
    {
        [Display(ResourceType = typeof(ExpeditionStrings), Name = "PendingAprroval")]
        [LocalizedDescription("PendingAprroval", typeof(ExpeditionStrings))]
        PendingApproval = 1,

        [Display(ResourceType = typeof(ExpeditionStrings), Name = "Approved")]
        [LocalizedDescription("Approved", typeof(ExpeditionStrings))]
        Approved = 2,

        [Display(ResourceType = typeof(ExpeditionStrings), Name = "InProcess")]
        [LocalizedDescription("InProcess", typeof(ExpeditionStrings))]
        InProcess = 3,

        [Display(ResourceType = typeof(ExpeditionStrings), Name = "Rejected")]
        [LocalizedDescription("Rejected", typeof(ExpeditionStrings))]
        Rejected = 4,
    }

    public enum eAgentExpeditionStatus
    {
        [Display(ResourceType = typeof(ExpeditionStrings), Name = "NotAssigned")]
        [LocalizedDescription("NotAssigned", typeof(ExpeditionStrings))]
        NotAssigned = 1,

        [Display(ResourceType = typeof(ExpeditionStrings), Name = "Assigned")]
        [LocalizedDescription("Assigned", typeof(ExpeditionStrings))]
        Assigned = 2,

        [Display(ResourceType = typeof(ExpeditionStrings), Name = "Approved")]
        [LocalizedDescription("Approved", typeof(ExpeditionStrings))]
        Approved = 3
    }
}
