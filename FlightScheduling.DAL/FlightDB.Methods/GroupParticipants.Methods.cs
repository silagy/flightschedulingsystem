﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FlightScheduling.Language;
using FlightScheduling.Language.CustomeExtentions;

namespace FlightScheduling.DAL
{
    [MetadataType(typeof(GroupParticipantsMetaData))]
    public partial class GroupParticipants
    {

        public eParticipantStatus ParticipantStatusEnum
        {
            get { return (eParticipantStatus)this.Status; }

            set { this.Status = (int)value; }
        }

        public eParticipantsSex ParticipantsSexEnum
        {
            get { return (eParticipantsSex)this.SEX; }
            set { this.SEX = (int)value; }
        }

        public eParticipantType PatricipantTypeEnum
        {
            get { return (eParticipantType)this.ParticipantType; }
            set { this.ParticipantType = (int)value; }
        }


        //data annotations here
        public class GroupParticipantsMetaData
        {


        }

    }

    public enum eParticipantType
    {
        [Display(ResourceType = typeof(GroupsStrings), Name = "Student")]
        [LocalizedDescription("Student", typeof(GroupsStrings))]
        Student = 1,

        [Display(ResourceType = typeof(GroupsStrings), Name = "Teacher")]
        [LocalizedDescription("Teacher", typeof(GroupsStrings))]
        Teacher = 2,

        [Display(ResourceType = typeof(GroupsStrings), Name = "Instructor")]
        [LocalizedDescription("Instructor", typeof(GroupsStrings))]
        Instructor = 3,

        [Display(ResourceType = typeof(GroupsStrings), Name = "MedicalPersonal")]
        [LocalizedDescription("MedicalPersonal", typeof(GroupsStrings))]
        MedicalPersonal = 4,

        [Display(ResourceType = typeof(GroupsStrings), Name = "Parent")]
        [LocalizedDescription("Parent", typeof(GroupsStrings))]
        Parent = 5,

        [Display(ResourceType = typeof(GroupsStrings), Name = "Other")]
        [LocalizedDescription("Other", typeof(GroupsStrings))]
        Other = 6,

        [Display(ResourceType = typeof(GroupsStrings), Name = "Testimony")]
        [LocalizedDescription("Testimony", typeof(GroupsStrings))]
        Testimony = 7,

        [Display(ResourceType = typeof(GroupsStrings), Name = "Polani")]
        [LocalizedDescription("Polani", typeof(GroupsStrings))]
        Polani = 8,

        [Display(ResourceType = typeof(GroupsStrings), Name = "PolaniPhotographer")]
        [LocalizedDescription("PolaniPhotographer", typeof(GroupsStrings))]
        PolaniPhotographer = 9
    }

    public enum eMedicalStuffType
    {
        [Display(ResourceType = typeof(CommonStrings), Name = "None")]
        [LocalizedDescription("None", typeof(CommonStrings))]
        None = 0,

        [Display(ResourceType = typeof(GroupsStrings), Name = "Doctor")]
        [LocalizedDescription("Doctor", typeof(GroupsStrings))]
        Doctor = 1,

        [Display(ResourceType = typeof(GroupsStrings), Name = "Paramedic")]
        [LocalizedDescription("Paramedic", typeof(GroupsStrings))]
        Paramedic = 2,

        [Display(ResourceType = typeof(GroupsStrings), Name = "Medic")]
        [LocalizedDescription("Medic", typeof(GroupsStrings))]
        Medic = 3,

        [Display(ResourceType = typeof(GroupsStrings), Name = "Nurse")]
        [LocalizedDescription("Nurse", typeof(GroupsStrings))]
        Nurse = 4
    }

    public enum eParticipantStatus
    {
        [Display(ResourceType = typeof(GroupsStrings), Name = "ParticipantPlanned")]
        [LocalizedDescription("ParticipantPlanned", typeof(GroupsStrings))]
        Planned = 1,

        [Display(ResourceType = typeof(GroupsStrings), Name = "ParticipantApproved")]
        [LocalizedDescription("ParticipantApproved", typeof(GroupsStrings))]
        Approved = 2
    }

    public enum eParticipantsSex
    {
        [Display(ResourceType = typeof(CommonStrings), Name = "None")]
        [LocalizedDescription("None", typeof(CommonStrings))]
        None = 0,

        [Display(ResourceType = typeof(GroupsStrings), Name = "Male")]
        [LocalizedDescription("Male", typeof(GroupsStrings))]
        Male = 1,

        [Display(ResourceType = typeof(GroupsStrings), Name = "Female")]
        [LocalizedDescription("Female", typeof(GroupsStrings))]
        Female = 2
    }
}
