﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace FlightScheduling.DAL
{
    static public class QueriesRepository
    {

	    public static class Instructor
	    {
		    public static string GetAllInstructorsToOpenUsers => @"select ID, FirstNameHE as FirstName, LastNameHE as LastName, InstructorID, Email
																		from Instructors as inst
																		where IsActive = 1
																		and RoleID = 5
																		and Email is not null
																		and ID not in (select Users.InstructorID from users where InstructorID is not null)
																		and ID not in (
																		select ID
																		from Instructors
																		where RoleID = 5
																		and Email in (
																		select
																		    Email
																		from Instructors
																		where IsActive = 1
																		group by Email
																		Having  count(Email) > 1)
																		    )";
	    }
	    public static class Outbox
	    {
		    public static string UpdateOutboxMessage =>
			    @"UPDATE Outbox SET ProcessOnUtc = @ProcessOnUtc, Error = @Error WHERE Id = @Id";
		    public static string GetTopOutboxMessages => @"SELECT TOP 50 * FROM Outbox WHERE ProcessOnUtc IS NULL order by CreatedOnUtc";
		    public static string InsertOutbox => @"INSERT INTO Outbox (ProcessOnUtc, CreatedOnUtc, Content, Type, Error)
                                                VALUES (@ProcessOnUtc, @CreatedOnUtc, @Content, @Type, @Error)";
	    }
        public static string GetUserPermission => @"SELECT 
	                                                    PR.ID,
	                                                    PR.Name,
	                                                    PR.CreationDate,
	                                                    PR.UpdateDate,
	                                                    PB.ID as PermissionBitID,
	                                                    PB.PermissionBit
                                                    FROM Users as US
                                                    INNER JOIN PermissionRole as PR ON US.PermissionRoleID = PR.ID
                                                    INNER JOIN PermissionBits as PB ON PR.ID = PB.PermissionRoleID
                                                    WHERE US.ID = @ID";

        public static string GetPermissionRolesAndPermissions => @"SELECT 
	                                                                PR.ID,
	                                                                PR.Name,
	                                                                PR.CreationDate,
	                                                                PR.UpdateDate,
	                                                                PB.ID as PermissionBitID,
	                                                                PB.ID,
	                                                                PB.PermissionBit,
	                                                                PB.CreationDate,
	                                                                PB.UpdateDate
                                                                FROM PermissionRole as PR
                                                                INNER JOIN PermissionBits as PB ON PR.ID = PB.PermissionRoleID";

        public static string GetPermissionRolesAndPermissionsByRole => @"SELECT 
	                                                                PR.ID,
	                                                                PR.Name,
	                                                                PR.CreationDate,
	                                                                PR.UpdateDate,
	                                                                PB.ID as PermissionBitID,
	                                                                PB.ID,
	                                                                PB.PermissionBit,
	                                                                PB.CreationDate,
	                                                                PB.UpdateDate
                                                                FROM PermissionRole as PR
                                                                INNER JOIN PermissionBits as PB ON PR.ID = PB.PermissionRoleID
                                                                WHERE PR.ID = @RoleID";

        public static string GetPermissionRoles => @"SELECT ID, Name FROM PermissionRole";

        public static string UpdatePermissionRole => @"UPDATE PermissionRole SET Name = @Name, UpdateDate = @UpdateDate WHERE ID = @ID";

        public static string DeletePermissionBitsByRole => @"DELETE PermissionBits WHERE PermissionRoleID = @ID";

        public static string InsertPermissionRole => @"INSERT INTO PermissionRole (Name, CreationDate, UpdateDate)
                                                       VALUES (@Name, @CreationDate, @updateDate)
                                                       SELECT CAST(SCOPE_IDENTITY() as int)";

        public static string InsertPermissionBit => @"INSERT INTO PermissionBits (PermissionRoleID,PermissionBit,CreationDate,UpdateDate)
                                                     VALUES (@PermissionRoleID, @PermissionBit, @CreationDate, @UpdateDate)
                                                     SELECT CAST(SCOPE_IDENTITY() as int)";

        public static string UpdateUsersPermissionRole => @"UPDATE Users SET PermissionRoleID = @RoleID WHERE ID IN @Users";

		public static string UpdateUserAccessToSystem = @"UPDATE Users SET IsActivate = @Access WHERE ID IN @Users";

		public static string GetAllUsers => @"SELECT 
	                                            US.ID as UserID,
	                                            US.Firstname,
	                                            US.Lastname,
	                                            US.Email,
	                                            US.Phone,
	                                            US.UserType,
	                                            US.IsActive,
	                                            US.IsActivate,
	                                            US.AgencyID,
	                                            US.SchoolID,
	                                            US.CreationDate,
	                                            US.Region,
	                                            US.UserRole,
	                                            US.PermissionRoleID,
	                                            PR.Name as PermissionRoleName	
                                            FROM Users as US
                                            LEFT JOIN PermissionRole as PR ON US.PermissionRoleID = PR.ID
                                            WHERE
                                            IsActive = @ShowDeletedUsers AND 
                                            (UserType = @RoleID OR @RoleID IS NULL)
                                            AND (Email = @Email OR @Email IS NULL)
											AND (US.IsActivate IN @SelectedActivation OR @SelectedActivation IS NULL)
                                            AND (Firstname like '%' + @Name + '%' OR Lastname like '%' + @Name + '%' OR Firstname + ' ' + Lastname LIKE + '%' + @Name + '%' OR @Name IS NULL)";

        public static string GetUserByInstituteID => @"SELECT * FROM Users WHERE Email = @InstituteID";

        public static string GetUserByInstructorID => @"SELECT * FROM Users WHERE InstructorID = @ID";

        public static string GetUserByEmail => @"SELECT * FROM Users WHERE Email = @Email";

        public static string GetUsersByPage => @"SELECT *
													FROM ( SELECT ROW_NUMBER() OVER (ORDER BY US.CreationDate) AS RowNum, *
													FROM Users as US ) as results
													WHERE RowNum >= @PageNumber 
														AND RowNum < @NumberOfRows
													ORDER BY RowNum";

        public static string GetUserByID => @"SELECT *
												FROM Users
												WHERE ID = @UserID";

        public static string RestoreUser => @"UPDATE Users SET IsActive = 1, IsActivate = 1 WHERE ID = @UserID";

        public static string GetUsersBySchoolID => @"SELECT 
	                                                    US.ID,
	                                                    US.Firstname,
	                                                    US.Lastname,
	                                                    SC.ManagerEmail as Email
                                                    FROM UsersView as US
													INNER JOIN Schools as SC ON US.Email = SC.InstituteID
                                                    WHERE US.SchoolID = @SchoolID";

        public static string AddUserFromSchoolCreation => @"INSERT INTO Users (Firstname, Lastname, Email, Password, Phone, UserType, IsActive, IsActivate, SchoolID, CreationDate)
VALUES (@Firstname, @Lastname, @Email, @Password, @Phone, @UserType, @IsActive, @IsActivate, @SchoolID, @CreationDate)";

        public static string AddUserFromInstructorFolder => @"INSERT INTO Users (Firstname, Lastname, Email, Password, Phone, UserType, IsActive, IsActivate, PermissionRoleID, CreationDate, InstructorID)
VALUES (@Firstname, @Lastname, @Email, @Password, @Phone, @UserType, @IsActive, @IsActivate, @PermissionRoleID, @CreationDate, @InstructorID)
SELECT CAST(SCOPE_IDENTITY() as int)";

        public static string UpdateInstructorIDInUsers => @"UPDATE Users
                                                            SET InstructorID = @InstructorID
                                                            WHERE ID = @ID";

        public static string GetUsersForBySchoolIDAndOrderID => @"SELECT 
	                                                    US.ID,
	                                                    US.Firstname,
	                                                    US.Lastname,
	                                                    SC.ManagerEmail as Email
                                                    FROM Users as US
													INNER JOIN Schools as SC ON US.Email = SC.InstituteID
                                                    WHERE US.SchoolID = @SchoolID
													
													UNION ALL

SELECT 
														E.UserID,
														E.Manager as Firstname,
														'' as Lastname,
														E.ManagerEmail as Email
													FROM Expeditions as E
													WHERE E.ID = @OrderID";

        public static string GetUsersByType => @"SELECT US.ID, US.Firstname, US.Lastname, US.Email FROM Users as US WHERE US.UserType = @UserType";

        public static string GetCountyManagersBySchoolID => @"SELECT US.ID, US.Firstname, US.Lastname, US.Email 
FROM UsersView as US 
WHERE 
US.Region IN (SELECT Region FROM Schools WHERE ID = @SchoolID)
AND US.UserType = @UserType";

        public static string GetUsersByTypeAndRole => @"SELECT US.ID, US.Firstname, US.Lastname, US.Email FROM UsersView as US WHERE US.UserType = @UserType AND US.UserRole = @UserRole";

        public static string GetUsersByAllNotificationParameters => @"SELECT US.ID, US.Firstname, US.Lastname, US.Email 
FROM UsersView as US 
WHERE 
	US.UserType = @UserType 
	AND (US.PermissionRoleID = @UserPermissionRole OR @UserPermissionRole IS NULL)";

        public static string GetEmailsByUserIDs => @"SELECT 
														US.ID,
														CASE
															WHEN US.UserType = 4 THEN SC.ManagerEmail
															ELSE US.Email
														END Email
													FROM UsersView as US
													LEFT JOIN Schools as SC ON  US.SchoolID = SC.ID
													WHERE US.ID IN @Recipients";

        public static string GetGroupManagersforNotifications => @"SELECT 
	                                                            US.ID, 
																US.Firstname,
																US.Lastname,
																US.Email
                                                                FROM InstructorGroups IG 
                                                                INNER JOIN Instructors as INST ON IG.InstructorID = INST.ID
																INNER JOIN UsersView as US ON INST.Email = US.Email
                                                                WHERE IG.GroupID = @GroupID AND IG.RoleID = 2 AND INST.IsActive = 1";

        public static string GetGroupAgencyUsersforNotifications => @"SELECT 
	                                                                    US.ID, 
	                                                                    US.Firstname,
	                                                                    US.Lastname,
	                                                                    US.Email
                                                                    FROM Groups as G
                                                                    INNER JOIN UsersView as US ON G.AgencyID = US.AgencyID
                                                                    WHERE G.ID = @GroupID";

        public static string GetExpeditionToCSV => @";WITH GroupManagers AS (
														SELECT 
															IG.GroupID,
															INST.FirstNameHE + ' ' + INST.LastNameHE as 'Manager',
															INST.CellPhone as ManagerCell,
															INST.Email as ManagerEmail,
															INST.HomePhone as ManagerPhone
														FROM InstructorGroups as IG
														INNER JOIN Instructors as INST ON IG.InstructorID = INST.ID
														WHERE INST.IsActive = 1
														AND IG.IsGroupManager = 1 -- Manager
														)
														SELECT 
																												EX.ID as ExpeditionID,
																												TR.DepartureDate,
																												TR.ReturningDate,
																												TR.DestinationID,
																												TR.ReturingField,
																												SC.Name as SchoolName,
																												SC.InstituteID,
																												GR.NumberOfBuses as BasesPerDay,
																												GR.NumberOfParticipants as NumberOfpassengers,
																												AC.Name as AgencyName,
																												CASE
																												WHEN LEN(GM.Manager) > 0 THEN GM.Manager
																												ELSE EX.Manager
																												END as  'Manager',
																												CASE
																													WHEN LEN(GM.Manager) > 0 THEN GM.ManagerCell
																													ELSE EX.ManagerCell
																												END AS ManagerCell,
																												CASE
																													WHEN LEN(GM.Manager) > 0 THEN GM.ManagerEmail
																													ELSE EX.ManagerEmail
																												END AS ManagerEmail,
																												CASE
																													WHEN LEN(GM.Manager) > 0 THEN GM.ManagerPhone
																													ELSE EX.ManagerPhone
																												END as ManagerPhone,
																												SC.Manager as SchoolManager,
																												SC.ManagerEmail as SchoolManagerEmail,
																												SC.ManagerPhone as SchoolManagerPhone,
																												GR.GroupExternalID as GroupID
																											FROM Expeditions as EX
																											INNER JOIN Trips as TR ON EX.TripID = TR.ID
																											INNER JOIN Schools as SC ON EX.SchoolID = SC.ID
																											INNER JOIN Groups as GR ON GR.OrderID = EX.ID
																											INNER JOIN Agencies as AC ON GR.AgencyID = AC.ID
																											LEFT JOIN GroupManagers as GM ON GR.ID = GM.GroupID
																											WHERE 
																												GR.IsActive = 1 
																												AND TR.DepartureDate >= @StartDate 
																												AND TR.DepartureDate <= @EndDate 
																												AND Ex.[Status] = @Status
																											ORDER BY GroupID ASC";

        public static string GetSystemConfiguration => @"SELECT * FROM SystemConfiguration";

        public static string UpdateSystemConfigurationProperties
            => @"UPDATE SystemConfiguration SET PropertyValue = @PropertyValue WHERE PropertyID = @PropertyID";

        public static string InsertNewMessage
            => @"INSERT INTO Messages (Title,[Message],UserID,CreationDate,UpdateDate,ActiveStartDate,ExpirationDate)
				VALUES (@Title,@Message,@UserID,@CreationDate,@UpdateDate,@ActiveStartDate,@ExpirationDate)
				SELECT CAST(SCOPE_IDENTITY() as int)";

        public static string UpdateMessage => @"UPDATE Messages
                                                SET Title = @Title, Message = @Message, UpdateDate = @UpdateDate, ActiveStartDate = @ActiveStartDate,ExpirationDate = @ExpirationDate
                                                WHERE Id = @Id";

        public static string GetAllMessages => @"SELECT 
													M.*,
													U.Firstname as UserFirstName,
													U.Lastname as UserLastName 
												FROM Messages as M
												INNER JOIN Users as U ON M.UserID = U.ID";
        public static string GetSystemMessage => @"SELECT	
	                                                    MS.Id,
	                                                    MS.Title,
	                                                    MS.Message,
	                                                    MS.UserID,
	                                                    MS.CreationDate,
	                                                    MS.UpdateDate,
	                                                    MS.ActiveStartDate,
	                                                    MS.ExpirationDate
                                                    FROM Messages as MS
                                                    WHERE MS.Id = @ID";

        public static string GetAllMessagesByDates => @"SELECT
													M.*,
													U.Firstname AS UserFirstName,
													U.Lastname AS UserLastName
												FROM Messages AS M
												INNER JOIN Users AS U
													ON M.UserID = U.ID
												WHERE M.ActiveStartDate <= @StartDate AND M.ExpirationDate >= @EndDate";

		public static string DeleteMessageById => @"DELETE Messages WHERE Id = @ID";


		public static string GetGroupMaxID => @"SELECT 
													MAX(G.GroupExternalID) as GroupExternalID
												FROM Groups as G
												INNER JOIN Expeditions as O ON G.OrderID = O.ID
												INNER JOIN Trips as T ON O.TripID = T.ID
												WHERE T.DepartureDate >= @StartDate AND T.DepartureDate <= @EndDate AND G.IsActive = 1";

        public static string GetOrderByID => @"SELECT 
												E.ID,
												E.TripID,
												E.NumberOFpassengers,
												E.SchoolID,
												E.BasesPerDay,
												E.Manager,
												E.ManagerPhone,
												E.ManagerEmail,
												E.ManagerCell,
												E.Comments,
												E.Status,
												E.AgentStatus,
												E.AgentApproval,
												E.IsActive,
												E.CreationDate,
												E.UpdateDate,
												E.UserID,
												E.AgencyID,
												T.ID,
												T.DepartureDate,
												T.ReturningDate
											FROM Expeditions as E
											INNER JOIN Trips as T ON E.TripID = T.ID
											WHERE E.ID = @OrderID";

        public static string GetGroupsColors => @"SELECT * FROM GroupColors";

        public static string InsertNewGroup
            =>
                @"INSERT INTO Groups (OrderID,GroupExternalID,NumberOfBuses,NumberOfParticipants,Status,ColorID,CreationDate,UpdateDate,FinalStatus,AgencyID,IsActive)
					VALUES (@OrderID,@GroupExternalID,@NumberOfBuses,@NumberOfParticipants,@Status,@ColorID,@CreationDate,@UpdateDate,1,@AgencyID,1)			
					INSERT INTO SubGroupSchools (GroupID, SchoolID, IsPrimary)
					SELECT 
						GP.ID,
						EX.SchoolID,
						1
					FROM Groups as GP
					INNER JOIN Expeditions as EX ON GP.OrderID = EX.ID
					WHERE GP.ID = (SELECT CAST(SCOPE_IDENTITY() as int))";

        public static string SetGroupInActive => @"UPDATE Groups SET IsActive = 0 WHERE ID = @GroupID";

        public static string GetMaxGroupIDForInactive => @"SELECT MAX(ID)+1 FROM Groups";

        public static string UpdateGroup => @"UPDATE Groups
                                                SET NumberOfBuses = @NumberOfBuses, NumberOfParticipants = @NumberOfParticipants, Status = @Status, ColorID = @ColorID, UpdateDate = @UpdateDate, AgencyID = @AgencyID
                                                WHERE ID = @ID";


        public static string GetGroupsByOrderID => @"SELECT 
														G.ID,g.OrderID, 
														G.GroupExternalID, 
														G.NumberOfBuses, 
														G.NumberOfParticipants,
														G.Status,
														G.ColorID,
														G.CreationDate,
														G.UpdateDate,
                                                        G.ContactName,
														G.ContactPhone,
                                                        G.FinalStatus
													FROM Groups as G
													WHERE G.OrderID = @OrderID AND G.IsActive = 1";



        public static string GetGroupFilesByGroupID => @"SELECT 
															GF.ID,
															GF.GroupID,
															GF.Name,
															GF.FileType,
															GF.FileStatus,
															GF.Url,
															GF.Comments,
															GF.CreationDate,
															GF.UserID,
                                                            GF.CountyManagerApproval,
                                                            GF.CountyManagerComments,
															US.Firstname as UserFirstName,
															US.Lastname as UserLastName
														FROM GroupFiles as GF
														INNER JOIN Users as US ON GF.UserID = US.ID
														WHERE GF.GroupID = @GroupID AND GF.FileType IN @GroupFileTypes
														ORDER BY GF.FileType ASC, GF.CreationDate DESC";

        public static string InsertNewGroupFile => @"INSERT INTO GroupFiles (GroupID,Name,FileType,FileStatus,Url,Comments,CreationDate,UserID)
													VALUES (@GroupID,@Name,@FileType,@FileStatus,@Url,@Comments,@CreationDate,@UserID)
													SELECT CAST(SCOPE_IDENTITY() as int)";

        public static string GetSingleGroupFolderData => @";WITH GroupFilesCTE AS(
																SELECT 
																	MainFiles.ID,
																	MainFiles.GroupID,
																	MainFiles.FileType,
																	GFD.Name,
																	GFD.FileStatus,
																	GFD.Url,
																	GFD.Comments,
																	GFD.CreationDate
																FROM ( 
																	SELECT 
																		MAX(GF.ID) as ID,
																		GF.GroupID,
																		GF.FileType
																	FROM GroupFiles as GF
																	WHERE GF.GroupID = @GroupID AND GF.FileType != 5
																	GROUP BY
																		GF.GroupID,
																		GF.FileType) as MainFiles
																	INNER JOIN GroupFiles as GFD ON MainFiles.ID = GFD.ID
															)


															SELECT 
																G.ID as GroupID,
																G.GroupExternalID,
																G.NumberOfBuses as GroupNumberOfBuses,
																G.NumberOfParticipants as GroupNumberofParticipants,
																G.Status as GroupStatus,
																G.CreationDate as GroupCreationDate,
																G.UpdateDate as GroupUpdateDate,
																G.ContactName,
																G.ContactPhone,
                                                                G.FinalStatus,
																GF180.ID as GF180FileID,
																GF180.FileStatus as GF180FileStatus,
																GF180.Url as GF180FileUrl,
																GF180.FileType as GF180FileType,
																GF60.ID as GF60FileID,
																GF60.FileStatus as GF60FileStatus,
																GF60.Url as GF60FileUrl,
																GF60.FileType as GF60FileType,
																GFINIPDF.ID as GFFINPDFFileID,
																GFINIPDF.FileStatus as GFFINPDFFileStatus,
																GFINIPDF.Url as GFFINPDFFileUrl,
																GFINIPDF.FileType as GFFINPDFFileType,
																GC.ID,
																GC.Code,
																EX.ID,
																EX.Manager,
																EX.ManagerPhone,
																EX.ManagerEmail,
																EX.ManagerCell,
																TR.ID,
																TR.DepartureDate,
																TR.DestinationID,
																TR.ReturningDate,
																TR.ReturingField,
																SC.ID,
																SC.InstituteID,
																SC.Name,
																AG.ID,
																AG.Name
															FROM Groups as G
															INNER JOIN Expeditions as EX ON G.OrderID = EX.ID
															INNER JOIN Agencies as AG ON G.AgencyID = AG.ID
															INNER JOIN SubGroupSchools as SS ON SS.GroupID = G.ID AND EX.SchoolID = SS.SchoolID
															INNER JOIN Schools as SC ON SS.SchoolID = SC.ID
															INNER JOIN GroupColors as GC ON G.ColorID = GC.ID
															INNER JOIN Trips as TR ON Ex.TripID = TR.ID
															LEFT JOIN GroupFilesCTE as GF180 ON G.ID = GF180.GroupID AND GF180.FileType = 1
															LEFT JOIN GroupFilesCTE as GF60 ON G.ID = GF60.GroupID AND GF60.FileType = 2
															LEFT JOIN GroupFilesCTE as GFINIPDF ON G.ID = GFINIPDF.GroupID AND GFINIPDF.FileType = 3
															WHERE G.ID = @GroupID";

        public static string GetFileByID => @"SELECT * FROM GroupFiles WHERE ID = @ID";

        public static string UpdateGroupFileStatus => @"UPDATE GroupFiles
														SET FileStatus = @FileStatus, Comments = @Comments, UserID = @UserID
														WHERE ID = @ID";

        public static string GetManagerGroupIDsByInstructorID => @"SELECT 
	                                                                IG.GroupID
                                                            FROM InstructorGroups IG 
                                                            INNER JOIN Instructors as INST ON IG.InstructorID = INST.ID
                                                            WHERE INST.ID = @InstructorID AND INST.IsActive = 1";

        public static string GetGroupDetailsNew => @";WITH GroupsCTE AS (
	SELECT DISTINCT
		G.ID as GroupID,
		G.GroupExternalID,
		G.NumberOfBuses as GroupNumberOfBuses,
		G.NumberOfParticipants as GroupNumberofParticipants,
		G.Status as GroupStatus,
        G.FinalStatus,
		G.CreationDate as GroupCreationDate,
		G.UpdateDate as GroupUpdateDate,
		GC.ID as ColorID,
		GC.Code,
		EX.ID as ExpeditionID,
		EX.Manager,
		EX.ManagerPhone,
		EX.ManagerEmail,
		EX.ManagerCell,
		TR.ID as TripID,
		TR.DepartureDate,
		TR.DestinationID,
		TR.ReturningDate,
		TR.ReturingField,
		SC.ID as SchoolID,
		SC.InstituteID,
		SC.Name,
		SC.Region
	FROM Groups as G
	INNER JOIN Expeditions as EX ON G.OrderID = EX.ID
	INNER JOIN SubGroupSchools as SGS ON G.ID = SGS.GroupID
	INNER JOIN Schools as SC ON SGS.SchoolID = SC.ID
	INNER JOIN GroupColors as GC ON G.ColorID = GC.ID
	INNER JOIN Trips as TR ON Ex.TripID = TR.ID
	WHERE 
        G.IsActive = 1
        AND
        (SC.IsAdministrativeInstitude = @ShowAdministrative OR @ShowAdministrative IS NULL)
        AND (
		TR.DepartureDate >= @StartDate AND TR.DepartureDate <= @EndDate
		AND (G.AgencyID = @AgencyID OR @AgencyID IS NULL)
		AND (SC.ID = @SchoolID OR SGS.SchoolID = @SchoolID OR @SchoolID IS NULL)
		AND (SGS.IsPrimary = @IsPrimary OR @IsPrimary IS NULL)
		AND (EX.ID = @OrderID OR @OrderID IS NULL)
		AND (TR.ID = @TripID OR @TripID IS NULL)
		AND (G.FinalStatus IN @GroupStatus)
		AND (SC.InstituteID = @InstituteID OR @InstituteID IS NULL)
		AND (G.GroupExternalID = @GroupID OR @GroupID IS NULL)
		AND (SC.Region = @Region OR @Region IS NULL)
        AND (G.ID IN @GroupIDs OR @GroupIDsCount=0))
	),
	Group180Forms as (
		SELECT 
			COUNT(SG.ID) as NumberOf180Forms,
			SG.GroupID
		FROM GroupsCTE as GR
		INNER JOIN SubGroupSchools as SG ON GR.GroupID = SG.GroupID
		INNER JOIN Schools as SC ON SG.SchoolID = SC.ID
		WHERE SC.IsAdministrativeInstitude = 0
		GROUP BY
			SG.GroupID
	), Group180180Status AS (
		SELECT 
			SUM(CASE 
				WHEN AF.FormStatus = 5 THEN 1
				WHEN AF.FormStatus = 2 THEN 1
				WHEN AF.FormStatus = 3 THEN 1
				ELSE 0
			END) as AdministrationFormCompletion,
			SUM(CASE 
				WHEN AF.FormStatus = 5 THEN 1
				ELSE 0
			END) as AdministrationFormApproval,
			SUM(CASE 
				WHEN GIF.FormStatus = 5 THEN 1
				WHEN GIF.FormStatus = 2 THEN 1
				WHEN GIF.FormStatus = 3 THEN 1
			ELSE 0
			END) as InstructorFormCompletion,
			SUM(CASE 
				WHEN GIF.FormStatus = 5 THEN 1
			ELSE 0
			END) as InstructorFormApproval,
			SUM(CASE 
				WHEN GPF.FormStatus = 5 THEN 1
				WHEN GPF.FormStatus = 2 THEN 1
				WHEN GPF.FormStatus = 3 THEN 1
			ELSE 0
			END) as PlanFormCompletion,
			SUM(CASE 
				WHEN GPF.FormStatus = 5 THEN 1
			ELSE 0
			END) as PlanFormApproval,
			GR.GroupID
		FROM GroupsCTE as GR
		INNER JOIN SubGroupSchools as SG ON GR.GroupID = SG.GroupID
		INNER JOIN Schools as SC ON SG.SchoolID = SC.ID
		LEFT JOIN GroupForms as GGF ON SG.ID = GGF.GroupSchoolID
		LEFT JOIN AdministrationForm180 as AF ON AF.GroupFormID = GGF.ID
		LEFT JOIN GroupInstructorsForm as GIF ON GIF.GroupFormID = GGF.ID
		LEFT JOIn GroupPlanForm as GPF ON GPF.GroupFormID = GGF.ID
		WHERE SC.IsAdministrativeInstitude = 0
		GROUP BY
			GR.GroupID
	), GroupFilesCTE AS(
	    SELECT 
		    MainFiles.ID,
		    MainFiles.GroupID,
		    MainFiles.FileType,
		    GFD.FileStatus,
		    GFD.CreationDate
	    FROM ( 
		    SELECT 
			    MAX(GF.ID) as ID,
			    GF.GroupID,
			    GF.FileType
		    FROM GroupFiles as GF
		    WHERE GF.GroupID IN (SELECT GroupID FROM GroupsCTE) AND GF.FileType = 2
		    GROUP BY
			    GF.GroupID,
			    GF.FileType) as MainFiles
		    INNER JOIN GroupFiles as GFD ON MainFiles.ID = GFD.ID
    )

	SELECT 
	G.GroupID,
	G.GroupExternalID,
	G.GroupNumberOfBuses,
	G.GroupNumberofParticipants,
	G.GroupStatus,
    G.FinalStatus,
	G.GroupCreationDate,
	G.GroupUpdateDate,
	GF180.NumberOf180Forms,
	GFS180.AdministrationFormCompletion,
	GFS180.AdministrationFormApproval,
	GFS180.InstructorFormCompletion,
	GFS180.InstructorFormApproval,
	GFS180.PlanFormCompletion,
	GFS180.PlanFormApproval,
	GF60.FormStatus as GF60FormStatus,
	GF60.UpdateDate as GF60UpdateDate,
    GF60.AgencyApprovalStatus,
	GF60.PlanApprovalStatus,
    GF60OLD.CreationDate as Form60FileCreationDate,
	G.ColorID as ID,
	G.Code,
	G.ExpeditionID as ID,
	G.Manager,
	G.ManagerPhone,
	G.ManagerEmail,
	G.ManagerCell,
	G.TripID as ID,
	G.DepartureDate,
	G.DestinationID,
	G.ReturningDate,
	G.ReturingField,
	G.SchoolID as ID,
	G.InstituteID,
	G.Name,
	G.Region
	FROM GroupsCTE as G
	LEFT JOIN Group180Forms as GF180 ON GF180.GroupID = G.GroupID
	LEFT JOIN Group180180Status as GFS180 ON GFS180.GroupID = G.GroupID
	LEFT JOIN GroupFormsSixty as GF60 ON GF60.GroupID = G.GroupID
	LEFT JOIN GroupFilesCTE GF60OLD ON G.GroupID = GF60OLD.GroupID";

        public static string GetGroupDetailsNewForGroupManager => @";WITH GroupsCTE AS (
	SELECT DISTINCT
		G.ID as GroupID,
		G.GroupExternalID,
		G.NumberOfBuses as GroupNumberOfBuses,
		G.NumberOfParticipants as GroupNumberofParticipants,
		G.Status as GroupStatus,
        G.FinalStatus,
		G.CreationDate as GroupCreationDate,
		G.UpdateDate as GroupUpdateDate,
		GC.ID as ColorID,
		GC.Code,
		EX.ID as ExpeditionID,
		EX.Manager,
		EX.ManagerPhone,
		EX.ManagerEmail,
		EX.ManagerCell,
		TR.ID as TripID,
		TR.DepartureDate,
		TR.DestinationID,
		TR.ReturningDate,
		TR.ReturingField,
		SC.ID as SchoolID,
		SC.InstituteID,
		SC.Name,
		SC.Region
	FROM Groups as G
	INNER JOIN Expeditions as EX ON G.OrderID = EX.ID
	INNER JOIN SubGroupSchools as SGS ON G.ID = SGS.GroupID
	INNER JOIN Schools as SC ON SGS.SchoolID = SC.ID
	INNER JOIN GroupColors as GC ON G.ColorID = GC.ID
	INNER JOIN Trips as TR ON Ex.TripID = TR.ID
	WHERE 
        G.IsActive = 1
        AND
        (SC.IsAdministrativeInstitude = @ShowAdministrative OR @ShowAdministrative IS NULL)
        AND (
		TR.DepartureDate >= @StartDate AND TR.DepartureDate <= @EndDate
        AND (SGS.IsPrimary = 1)
        AND (G.ID IN @GroupIDs))
	),
	Group180Forms as (
		SELECT 
			COUNT(SG.ID) as NumberOf180Forms,
			SG.GroupID
		FROM GroupsCTE as GR
		INNER JOIN SubGroupSchools as SG ON GR.GroupID = SG.GroupID
		INNER JOIN Schools as SC ON SG.SchoolID = SC.ID
		WHERE SC.IsAdministrativeInstitude = 0
		GROUP BY
			SG.GroupID
	), Group180180Status AS (
		SELECT 
			SUM(CASE 
				WHEN AF.FormStatus = 5 THEN 1
				WHEN AF.FormStatus = 2 THEN 1
				WHEN AF.FormStatus = 3 THEN 1
				ELSE 0
			END) as AdministrationFormCompletion,
			SUM(CASE 
				WHEN AF.FormStatus = 5 THEN 1
				ELSE 0
			END) as AdministrationFormApproval,
			SUM(CASE 
				WHEN GIF.FormStatus = 5 THEN 1
				WHEN GIF.FormStatus = 2 THEN 1
				WHEN GIF.FormStatus = 3 THEN 1
			ELSE 0
			END) as InstructorFormCompletion,
			SUM(CASE 
				WHEN GIF.FormStatus = 5 THEN 1
			ELSE 0
			END) as InstructorFormApproval,
			SUM(CASE 
				WHEN GPF.FormStatus = 5 THEN 1
				WHEN GPF.FormStatus = 2 THEN 1
				WHEN GPF.FormStatus = 3 THEN 1
			ELSE 0
			END) as PlanFormCompletion,
			SUM(CASE 
				WHEN GPF.FormStatus = 5 THEN 1
			ELSE 0
			END) as PlanFormApproval,
			GR.GroupID
		FROM GroupsCTE as GR
		INNER JOIN SubGroupSchools as SG ON GR.GroupID = SG.GroupID
		INNER JOIN Schools as SC ON SG.SchoolID = SC.ID
		LEFT JOIN GroupForms as GGF ON SG.ID = GGF.GroupSchoolID
		LEFT JOIN AdministrationForm180 as AF ON AF.GroupFormID = GGF.ID
		LEFT JOIN GroupInstructorsForm as GIF ON GIF.GroupFormID = GGF.ID
		LEFT JOIn GroupPlanForm as GPF ON GPF.GroupFormID = GGF.ID
		WHERE SC.IsAdministrativeInstitude = 0
		GROUP BY
			GR.GroupID
	), GroupFilesCTE AS(
	    SELECT 
		    MainFiles.ID,
		    MainFiles.GroupID,
		    MainFiles.FileType,
		    GFD.FileStatus,
		    GFD.CreationDate
	    FROM ( 
		    SELECT 
			    MAX(GF.ID) as ID,
			    GF.GroupID,
			    GF.FileType
		    FROM GroupFiles as GF
		    WHERE GF.GroupID IN (SELECT GroupID FROM GroupsCTE) AND GF.FileType = 2
		    GROUP BY
			    GF.GroupID,
			    GF.FileType) as MainFiles
		    INNER JOIN GroupFiles as GFD ON MainFiles.ID = GFD.ID
    )

	SELECT 
	G.GroupID,
	G.GroupExternalID,
	G.GroupNumberOfBuses,
	G.GroupNumberofParticipants,
	G.GroupStatus,
    G.FinalStatus,
	G.GroupCreationDate,
	G.GroupUpdateDate,
	GF180.NumberOf180Forms,
	GFS180.AdministrationFormCompletion,
	GFS180.AdministrationFormApproval,
	GFS180.InstructorFormCompletion,
	GFS180.InstructorFormApproval,
	GFS180.PlanFormCompletion,
	GFS180.PlanFormApproval,
	GF60.FormStatus as GF60FormStatus,
	GF60.UpdateDate as GF60UpdateDate,
    GF60.AgencyApprovalStatus,
	GF60.PlanApprovalStatus,
    GF60OLD.CreationDate as Form60FileCreationDate,
	G.ColorID as ID,
	G.Code,
	G.ExpeditionID as ID,
	G.Manager,
	G.ManagerPhone,
	G.ManagerEmail,
	G.ManagerCell,
	G.TripID as ID,
	G.DepartureDate,
	G.DestinationID,
	G.ReturningDate,
	G.ReturingField,
	G.SchoolID as ID,
	G.InstituteID,
	G.Name,
	G.Region
	FROM GroupsCTE as G
	LEFT JOIN Group180Forms as GF180 ON GF180.GroupID = G.GroupID
	LEFT JOIN Group180180Status as GFS180 ON GFS180.GroupID = G.GroupID
	LEFT JOIN GroupFormsSixty as GF60 ON GF60.GroupID = G.GroupID
	LEFT JOIN GroupFilesCTE GF60OLD ON G.GroupID = GF60OLD.GroupID";

        public static string SpGetGroupsDetails => @";WITH GroupsCTE AS (
	                                                        SELECT DISTINCT
		                                                        G.ID as GroupID,
		                                                        G.GroupExternalID,
		                                                        G.NumberOfBuses as GroupNumberOfBuses,
		                                                        G.NumberOfParticipants as GroupNumberofParticipants,
		                                                        G.Status as GroupStatus,
		                                                        G.CreationDate as GroupCreationDate,
		                                                        G.UpdateDate as GroupUpdateDate,
		                                                        GC.ID as ColorID,
		                                                        GC.Code,
		                                                        EX.ID as ExpeditionID,
		                                                        EX.Manager,
		                                                        EX.ManagerPhone,
		                                                        EX.ManagerEmail,
		                                                        EX.ManagerCell,
		                                                        TR.ID as TripID,
		                                                        TR.DepartureDate,
		                                                        TR.DestinationID,
		                                                        TR.ReturningDate,
		                                                        TR.ReturingField,
		                                                        SC.ID as SchoolID,
		                                                        SC.InstituteID,
		                                                        SC.Name
	                                                        FROM Groups as G
	                                                        INNER JOIN Expeditions as EX ON G.OrderID = EX.ID
	                                                        INNER JOIN Schools as SC ON EX.SchoolID = SC.ID
	                                                        INNER JOIN GroupColors as GC ON G.ColorID = GC.ID
	                                                        INNER JOIN Trips as TR ON Ex.TripID = TR.ID
	                                                        INNER JOIN SubGroupSchools as SGS ON G.ID = SGS.GroupID
	                                                        WHERE 
		                                                        TR.DepartureDate >= @StartDate AND TR.DepartureDate <= @EndDate
		                                                        AND (G.AgencyID = @AgencyID OR @AgencyID IS NULL)
		                                                        AND (EX.SchoolID = @SchoolID OR SGS.SchoolID = @SchoolID OR @SchoolID IS NULL)
		                                                        AND (EX.ID = @OrderID OR @OrderID IS NULL)
		                                                        AND (TR.ID = @TripID OR @TripID IS NULL)
		                                                        AND (G.FinalStatus IN @GroupStatus)
                                                                AND (SC.InstituteID = @InstituteID OR @InstituteID IS NULL)
		                                                        AND (G.GroupExternalID = @GroupID OR @GroupID IS NULL)
		                                                        AND (SC.Region = @Region OR @Region IS NULL)
		                                                        --AND (EX.SchoolID IN (SELECT SchoolID FROM CountyManagersSchoolsAssosication UCM WHERE UCM.UserID = @UserID) OR @UserID IS NULL)
                                                        ),
                                                        GroupFilesCTE AS(
	                                                        SELECT 
		                                                        MainFiles.ID,
		                                                        MainFiles.GroupID,
		                                                        MainFiles.FileType,
		                                                        GFD.Name,
		                                                        GFD.FileStatus,
		                                                        GFD.Url,
		                                                        GFD.Comments,
		                                                        GFD.CreationDate,
		                                                        GFD.CountyManagerApproval,
		                                                        GFD.UserID,
		                                                        US.Firstname as SchoolName,
		                                                        US.SchoolID
	                                                        FROM ( 
		                                                        SELECT 
			                                                        MAX(GF.ID) as ID,
			                                                        GF.GroupID,
			                                                        GF.FileType
		                                                        FROM GroupFiles as GF
		                                                        WHERE GF.GroupID IN (SELECT GroupID FROM GroupsCTE) AND GF.FileType != 4
		                                                        GROUP BY
			                                                        GF.GroupID,
			                                                        GF.FileType) as MainFiles
		                                                        INNER JOIN GroupFiles as GFD ON MainFiles.ID = GFD.ID
		                                                        INNER JOIN Users as US ON GFD.UserID = US.ID
                                                        ),
                                                        GroupSubFiles AS (
	                                                        SELECT 
		                                                        G.GroupID, 
		                                                        COUNT(GF.ID) as CountSubOfFiles
	                                                        FROM GroupsCTE as G
	                                                        INNER JOIN GroupFilesCTE as GF ON G.GroupID = GF.GroupID AND G.SchoolID != GF.SchoolID
	                                                        GROUP BY
		                                                        G.GroupID
                                                        )


                                                        SELECT 
	                                                        G.GroupID,
	                                                        G.GroupExternalID,
	                                                        G.GroupNumberOfBuses,
	                                                        G.GroupNumberofParticipants,
	                                                        G.GroupStatus,
	                                                        G.GroupCreationDate,
	                                                        G.GroupUpdateDate,
	                                                        ISNULL(GSF.CountSubOfFiles,0) as CountSubOfFiles,
	                                                        GF180.ID as GF180FileID,
	                                                        GF180.FileStatus as GF180FileStatus,
	                                                        GF180.Url as GF180FileUrl,
	                                                        GF180.FileType as GF180FileType,
	                                                        ISNULL(GF180.CountyManagerApproval,0) as GF180CountyManagerApproval,
	                                                        GF60.ID as GF60FileID,
	                                                        GF60.FileStatus as GF60FileStatus,
	                                                        GF60.Url as GF60FileUrl,
	                                                        GF60.FileType as GF60FileType,
	                                                        GFINIPDF.ID as GFFINPDFFileID,
	                                                        GFINIPDF.FileStatus as GFFINPDFFileStatus,
	                                                        GFINIPDF.Url as GFFINPDFFileUrl,
	                                                        GFINIPDF.FileType as GFFINPDFFileType,
	                                                        G.ColorID as ID,
	                                                        G.Code,
	                                                        G.ExpeditionID as ID,
	                                                        G.Manager,
	                                                        G.ManagerPhone,
	                                                        G.ManagerEmail,
	                                                        G.ManagerCell,
	                                                        G.TripID as ID,
	                                                        G.DepartureDate,
	                                                        G.DestinationID,
	                                                        G.ReturningDate,
	                                                        G.ReturingField,
	                                                        G.SchoolID as ID,
	                                                        G.InstituteID,
	                                                        G.Name
                                                        FROM GroupsCTE as G
                                                        LEFT JOIN GroupFilesCTE as GF180 ON G.GroupID = GF180.GroupID AND GF180.FileType = 1 
                                                        LEFT JOIN GroupFilesCTE as GF60 ON G.GroupID = GF60.GroupID AND GF60.FileType = 2 
                                                        LEFT JOIN GroupFilesCTE as GFINIPDF ON G.GroupID = GFINIPDF.GroupID AND GFINIPDF.FileType = 3 
                                                        LEFt JOIN GroupSubFiles as GSF ON G.GroupID = GSF.GroupID";

        public static string SpGetGroupsDetailsCountyManager => @";WITH GroupsCTE AS (
	                                                                SELECT DISTINCT
		                                                                G.ID as GroupID,
		                                                                G.GroupExternalID,
		                                                                G.NumberOfBuses as GroupNumberOfBuses,
		                                                                G.NumberOfParticipants as GroupNumberofParticipants,
		                                                                G.Status as GroupStatus,
		                                                                G.CreationDate as GroupCreationDate,
		                                                                G.UpdateDate as GroupUpdateDate,
		                                                                GC.ID as ColorID,
		                                                                GC.Code,
		                                                                EX.ID as ExpeditionID,
		                                                                EX.Manager,
		                                                                EX.ManagerPhone,
		                                                                EX.ManagerEmail,
		                                                                EX.ManagerCell,
		                                                                TR.ID as TripID,
		                                                                TR.DepartureDate,
		                                                                TR.DestinationID,
		                                                                TR.ReturningDate,
		                                                                TR.ReturingField,
		                                                                SC.ID as SchoolID,
		                                                                SC.InstituteID,
		                                                                SC.Name
	                                                                FROM Groups as G
	                                                                INNER JOIN Expeditions as EX ON G.OrderID = EX.ID
	                                                                INNER JOIN GroupColors as GC ON G.ColorID = GC.ID
	                                                                INNER JOIN Trips as TR ON Ex.TripID = TR.ID
	                                                                INNER JOIN SubGroupSchools as SGS ON G.ID = SGS.GroupID
	                                                                INNER JOIN Schools as SC ON SGS.SchoolID = SC.ID
	                                                                WHERE 
		                                                                TR.DepartureDate >= @StartDate AND TR.DepartureDate <= @EndDate
		                                                                AND (G.AgencyID = @AgencyID OR @AgencyID IS NULL)
		                                                                AND (EX.SchoolID = @SchoolID OR SGS.SchoolID = @SchoolID OR @SchoolID IS NULL)
		                                                                AND (EX.ID = @OrderID OR @OrderID IS NULL)
		                                                                AND (TR.ID = @TripID OR @TripID IS NULL)
		                                                                AND (G.FinalStatus IN @GroupStatus)
                                                                        AND (SC.InstituteID = @InstituteID OR @InstituteID IS NULL)
		                                                                AND (G.GroupExternalID = @GroupID OR @GroupID IS NULL)
		                                                                AND (SC.Region = @Region OR @Region IS NULL)
		                                                                --AND (EX.SchoolID IN (SELECT SchoolID FROM CountyManagersSchoolsAssosication UCM WHERE UCM.UserID = @UserID) OR @UserID IS NULL)
                                                                ),
                                                                GroupFilesCTE AS(
	                                                                SELECT 
		                                                                MainFiles.ID,
		                                                                MainFiles.GroupID,
		                                                                MainFiles.FileType,
		                                                                GFD.Name,
		                                                                GFD.FileStatus,
		                                                                GFD.Url,
		                                                                GFD.Comments,
		                                                                GFD.CreationDate,
		                                                                GFD.CountyManagerApproval,
		                                                                GFD.UserID,
		                                                                US.Firstname as SchoolName,
		                                                                US.SchoolID
	                                                                FROM ( 
		                                                                SELECT 
			                                                                MAX(GF.ID) as ID,
			                                                                GF.GroupID,
			                                                                GF.FileType
		                                                                FROM GroupFiles as GF
		                                                                WHERE GF.GroupID IN (SELECT GroupID FROM GroupsCTE) AND GF.FileType != 4
		                                                                GROUP BY
			                                                                GF.GroupID,
			                                                                GF.FileType) as MainFiles
		                                                                INNER JOIN GroupFiles as GFD ON MainFiles.ID = GFD.ID
		                                                                INNER JOIN Users as US ON GFD.UserID = US.ID
                                                                ),
                                                                GroupSubFiles AS (
	                                                                SELECT 
		                                                                G.GroupID, 
		                                                                COUNT(GF.ID) as CountSubOfFiles
	                                                                FROM GroupsCTE as G
	                                                                INNER JOIN GroupFilesCTE as GF ON G.GroupID = GF.GroupID AND G.SchoolID != GF.SchoolID
	                                                                GROUP BY
		                                                                G.GroupID
                                                                )


                                                                SELECT 
	                                                                G.GroupID,
	                                                                G.GroupExternalID,
	                                                                G.GroupNumberOfBuses,
	                                                                G.GroupNumberofParticipants,
	                                                                G.GroupStatus,
	                                                                G.GroupCreationDate,
	                                                                G.GroupUpdateDate,
	                                                                ISNULL(GSF.CountSubOfFiles,0) as CountSubOfFiles,
	                                                                GF180.ID as GF180FileID,
	                                                                GF180.FileStatus as GF180FileStatus,
	                                                                GF180.Url as GF180FileUrl,
	                                                                GF180.FileType as GF180FileType,
	                                                                ISNULL(GF180.CountyManagerApproval,0) as GF180CountyManagerApproval,
	                                                                GF60.ID as GF60FileID,
	                                                                GF60.FileStatus as GF60FileStatus,
	                                                                GF60.Url as GF60FileUrl,
	                                                                GF60.FileType as GF60FileType,
	                                                                GFINIPDF.ID as GFFINPDFFileID,
	                                                                GFINIPDF.FileStatus as GFFINPDFFileStatus,
	                                                                GFINIPDF.Url as GFFINPDFFileUrl,
	                                                                GFINIPDF.FileType as GFFINPDFFileType,
	                                                                G.ColorID as ID,
	                                                                G.Code,
	                                                                G.ExpeditionID as ID,
	                                                                G.Manager,
	                                                                G.ManagerPhone,
	                                                                G.ManagerEmail,
	                                                                G.ManagerCell,
	                                                                G.TripID as ID,
	                                                                G.DepartureDate,
	                                                                G.DestinationID,
	                                                                G.ReturningDate,
	                                                                G.ReturingField,
	                                                                G.SchoolID as ID,
	                                                                G.InstituteID,
	                                                                G.Name
                                                                FROM GroupsCTE as G
                                                                LEFT JOIN GroupFilesCTE as GF180 ON G.GroupID = GF180.GroupID AND GF180.FileType = 1 
                                                                LEFT JOIN GroupFilesCTE as GF60 ON G.GroupID = GF60.GroupID AND GF60.FileType = 2 
                                                                LEFT JOIN GroupFilesCTE as GFINIPDF ON G.GroupID = GFINIPDF.GroupID AND GFINIPDF.FileType = 3 
                                                                LEFt JOIN GroupSubFiles as GSF ON G.GroupID = GSF.GroupID";

        public static string GetGroupNotesByGroupID => @"SELECT 
	                                                        GR.ID,
	                                                        GR.UserID,
	                                                        GR.GroupID,
	                                                        GR.Notes,
	                                                        GR.UpdateDate,
	                                                        GR.CreationDate,
	                                                        US.ID,
	                                                        US.Firstname,
	                                                        US.Lastname,
	                                                        GR.SchoolID,
	                                                        SC.Name
                                                        FROM GroupRemarks as GR
                                                        INNER JOIN Users as US ON GR.UserID = US.ID
                                                        INNER JOIN Schools as SC ON GR.SchoolID = SC.ID
                                                        WHERE GroupID = @GroupID";

        public static string InsertGroupNote => @"INSERT INTO GroupRemarks (UserID,GroupID,Notes,UpdateDate,CreationDate,SchoolID)
													VALUES (@UserID,@GroupID,@Notes,@UpdateDate,@CreationDate,@SchoolID)";

        #region GroupParticipants



        public static string GetGroupParticipants => @"SELECT 
														GP.ID,
														GP.GroupID,
														GP.ParticipantID,
														GP.FirstNameHe,
														GP.LastNameHe,
														GP.FirstNameEn,
														GP.LastNameEn,
														GP.PassportNumber,
														GP.PassportExperationDate,
														GP.Birthday,
														GP.SEX,
														GP.Status,
														GP.CreationDate,
														GP.UpdateDate,
                                                        GP.ParticipantType,
                                                        GP.SchoolInstituteID,
                                                        GP.ClassType,
                                                        GP.Comments,
														US.ID,
														US.Firstname,
														US.Lastname
													FROM GroupParticipants as GP
													INNER JOIN Users as US ON GP.UserID = US.ID
													WHERE GP.GroupID = @GroupID";


        public static string InsertOrUpdateGroupParticipant => @"dbo.InsertGroupParticipant";

        public static string InsertNewGroupParticipant => @"INSERT INTO GroupParticipants (GroupID,ParticipantID,FirstNameHe,LastNameHe,FirstNameEn,LastNameEn,PassportNumber,PassportExperationDate
										,Birthday,SEX,Status,CreationDate,UpdateDate,UserID,ParticipantType,SchoolInstituteID,ClassType,Comments)
		VALUES (@GroupID,@ParticipantID,@FirstNameHe,@LastNameHe,@FirstNameEn,@LastNameEn,@PassportNumber,@PassportExperationDate
										,@Birthday,@SEX,@Status,@CreationDate,@CreationDate,@UserID,@ParticipantType,@SchoolInstituteID,@ClassType,@Comments)";

        public static string UpdateGroupParticipant => @"UPDATE GroupParticipants
		SET  FirstNameHe = @FirstNameHe, LastNameHe = @LastNameHe, FirstNameEn = @FirstNameEn, LastNameEn = @LastNameEn, PassportNumber = @PassportNumber,
		PassportExperationDate = @PassportExperationDate, Birthday = @Birthday, SEX = @SEX, Status = @Status, UserID = @UserID, UpdateDate = @UpdateDate,
		ParticipantType = @ParticipantType, SchoolInstituteID = @SchoolInstituteID, ClassType = @ClassType, Comments = @Comments
		WHERE ID = @ID";

        public static string GetGroupParticipant => @"SELECT
	                                                    GP.ID,
	                                                    GP.GroupID,
	                                                    GP.ParticipantID,
	                                                    GP.FirstNameHe,
	                                                    GP.LastNameHe,
	                                                    GP.FirstNameEn,
	                                                    GP.LastNameEn,
	                                                    GP.PassportNumber,
	                                                    GP.PassportExperationDate,
	                                                    GP.Birthday,
	                                                    GP.SEX,
	                                                    GP.Status,
	                                                    GP.CreationDate,
	                                                    GP.UpdateDate,
                                                        GP.ParticipantType,
                                                        GP.SchoolInstituteID,
                                                        GP.ClassType,
	                                                    US.ID,
	                                                    US.Firstname,
	                                                    US.Lastname
                                                    FROM GroupParticipants AS GP
                                                    INNER JOIN Users AS US
	                                                    ON GP.UserID = US.ID
                                                    WHERE GP.ID = @ID";


        public static string GetSchoolIDByUserID => @"SELECT S.ID FROM Schools as S
	                                                    INNER JOIN Users as  US ON S.ID = US.SchoolID
	                                                        WHERE US.ID = @UserID";

        public static string GetInstiuteIDByUserID => @"SELECT S.InstituteID FROM Schools as S
	                                                    INNER JOIN Users as  US ON S.ID = US.SchoolID
	                                                        WHERE US.ID = @UserID";

        public static string GetSchoolByIntituteID => @"SELECT * FROM Schools WHERE InstituteID = @InstituteID";

        public static string GetSchoolByID => @"SELECT * FROM Schools WHERE ID = @ID";

        public static string GetAgencyIDByUserID => @"SELECT AG.ID FROM Agencies as AG
                                                        INNER JOIN Users as US ON AG.ID = US.AgencyID
                                                        WHERE US.ID = @UserID";

        public static string GetNumberOfAssingedParticipantsToGroup
            => @"SELECT (SELECT COUNT(ID) as AssignedParticipants FROM GroupParticipants WHERE GroupID = @GroupID) + (SELECT COUNT(ID) as AssignedInstructors
                    FROM InstructorGroups as IG
                    WHERE IG.GroupID = @GroupID) as AssignedParticipants";

        public static string CheckIfSchoolpConnectedToGroup => @"SELECT 
	                                                                SG.SchoolID 
                                                                FROM SubGroupSchools as SG
                                                                INNER JOIN Schools as SC ON SG.SchoolID = SC.ID
                                                                WHERE SG.GroupID = @GroupID AND SC.InstituteID = @InstituteID";

        #endregion

        public static string GetGroupFileByID => @"SELECT * FROM GroupFiles as GF WHERE GF.ID = @FileID";

        public static string DeleteFileByID => @"DELETE FROM GroupFiles WHERE ID = @FileID";

        public static string DeleteNotebyID => @"DELETE FROM GroupRemarks WHERE ID = @NoteID";

        public static string GetNoteByID => @"SELECT 
															GR.ID,
															GR.GroupID,
															GR.Notes,
															GR.UpdateDate,
															GR.CreationDate,
															GR.UserID,
															US.Firstname,
															US.Lastname,
                                                            GR.SchoolID,
	                                                        SC.Name
														FROM GroupRemarks as GR
														INNER JOIN Users as US ON GR.UserID = US.ID
                                                        INNER JOIN Schools as SC ON GR.SchoolID = SC.ID
														WHERE GR.ID = @NoteID";

        public static string DeleteParticipant => @"DELETE FROM GroupParticipants WHERE ID = @ParticipantID";

        public static string DeleteMultipleParticipants => @"DELETE FROM GroupParticipants WHERE ID IN @Items";

        public static string UpdateGroupNote => @"UPDATE GroupRemarks
                                                    SET UserID = @UserID, Notes = @Notes, UpdateDate = @UpdateDate
                                                    WHERE ID = @ID";

        public static string GetAllInstructors => @"SELECT 
	                                                    IST.*,
	                                                    IR.*
                                                    FROM Instructors as IST
                                                    INNER JOIN InstructorRoles as IR ON IST.RoleID = IR.ID
                                                    WHERE 
	                                                    (IST.RoleID = @RoleID OR @RoleID IS NULL)
	                                                    AND (IST.InstructorID = @InstructorID OR @InstructorID IS NULL)
	                                                    AND (IST.LastNameHE = @LastName OR @LastName IS NULL)
                                                        AND IST.IsActive = 1";

        public static string InsertInstructor
            =>
                @"INSERT INTO Instructors (InstructorID,FirstNameHE,LastNameHE,FirstNameEN,LastNameEN,PassportID,[Address],City,ZipCode,HomePhone,CellPhone,RoleID,CreationDate,UpdateDate,UserID,Email,OrganizationalStatus,SEX)
                    VALUES (@InstructorID,@FirstNameHE,@LastNameHE,@FirstNameEN,@LastNameEN,@PassportID,@Address,@City,@ZipCode,@HomePhone,@CellPhone,@RoleID,@CreationDate,@UpdateDate,@UserID,@Email,@OrganizationalStatus,@SEX)
                    SELECT CAST(SCOPE_IDENTITY() as int)";

        public static string UpdateInstructor => @"UPDATE Instructors
                                                    SET FirstNameHE = @FirstNameHE, LastNameHE = @LastNameHE, FirstNameEN = @FirstNameEN, LastNameEN = @LastNameEN,
                                                    PassportID = @PassportID, [Address] = @Address, City = @City, ZipCode = @ZipCode, HomePhone = @HomePhone,
                                                    CellPhone = @CellPhone, Email = @Email, UpdateDate = @UpdateDate, UserID = @UserID, OrganizationalStatus = @OrganizationalStatus, SEX = @SEX
                                                    WHERE ID = @ID";

        public static string UpdateInstructorwithRole => @"UPDATE Instructors
                                                    SET FirstNameHE = @FirstNameHE, LastNameHE = @LastNameHE, FirstNameEN = @FirstNameEN, LastNameEN = @LastNameEN,
                                                    PassportID = @PassportID, [Address] = @Address, City = @City, ZipCode = @ZipCode, HomePhone = @HomePhone, SEX = @SEX,
                                                    CellPhone = @CellPhone, Email = @Email, UpdateDate = @UpdateDate, UserID = @UserID, OrganizationalStatus = @OrganizationalStatus, RoleID = @RoleID,ValidityStartDate = @ValidityStartDate, ValidityEndTime = @ValidityEndTime, CertificateID = ''
                                                    WHERE ID = @ID";

        public static string GetInstructor => @"SELECT 
	                                                IST.*,
	                                                IR.*
                                                FROM Instructors as IST
                                                INNER JOIN InstructorRoles as IR ON IST.RoleID = IR.ID
                                                WHERE IST.ID = @ID AND IST.IsActive = 1";

        public static string DeleteInstructor => @"DELETE FROM Instructors WHERE ID = @ID";

        public static string SetInstrucorInActive => @"UPDATE Instructors SET IsActive = @IsActive WHERE ID = @ID";

        public static string InsertInstructorRole
            =>
                @"INSERT INTO InstructorRoles (Name,RequiredHours,ExpirationDaysRule,StartingValidity,StartingRenewal,AutomaticRenewal,[Language],IsManager,AllowChangingRole, AllowLogin)
                VALUES (@Name,@RequiredHours,@ExpirationDaysRule,@StartingValidity,@StartingRenewal,@AutomaticRenewal,@Language,@IsManager,@AllowChangingRole, @AllowLogin)
                SELECT CAST(SCOPE_IDENTITY() as int)";

        public static string UpdateInstructorRole => @"UPDATE InstructorRoles
                                                        SET Name = @Name, RequiredHours = @RequiredHours, ExpirationDaysRule = @ExpirationDaysRule, StartingValidity = @StartingValidity,
                                                        StartingRenewal = @StartingRenewal, AutomaticRenewal = @AutomaticRenewal, IsManager = @IsManager, AllowChangingRole = @AllowChangingRole, AllowLogin = @AllowLogin
                                                        WHERE ID = @ID";

        public static string GetInstructorRoles => @"SELECT * FROM InstructorRoles WHERE [Language] = @Language";

        public static string GetInstructorRole => @"SELECT * FROM InstructorRoles WHERE ID = @ID";

        public static string InsertTraining
            => @"INSERT INTO Trainings (Name,TrainingDate,TrainingLocation,TrainingHours,TrainingType,RoleID,CreatorID,RegionID,TrainingTypeID)
VALUES (@Name, @TrainingDate, @TrainingLocation, @TrainingHours, @TrainingType, @RoleID, @CreatorID,@RegionID,@TrainingTypeID)";

        public static string UpdateTraining => @"UPDATE Trainings
                                                SET Name = @Name, TrainingDate = @TrainingDate, TrainingLocation = @TrainingLocation,
                                                TrainingHours = @TrainingHours, TrainingType = @TrainingType, RoleID = @RoleID, CreatorID = @CreatorID, RegionID = @RegionID, TrainingTypeID = @TrainingTypeID
                                                WHERE ID = @ID";

        public static string CloseTraining => @"UPDATE Trainings
                                                SET IsCompleted = @IsCompleted
                                                WHERE ID = @ID";

        public static string GetTrainingTypes => @"SELECT ID, Name FROM TrainingTypes";

        public static string GetTrainings => @"SELECT
	                                                TR.ID,
													TR.Name,
													TR.TrainingLocation as TrainingLocationID,
													TR.TrainingDate,
													TR.TrainingHours,
													TR.TrainingType as TrainingValidationID,
													TR.IsCompleted,
													TR.RoleID,
													TR.RegionID,
													TT.ID TrainingTypeID,
													TT.Name as TrainingTypeName,
	                                                IR.ID,
	                                                IR.Name,
	                                                IR.RequiredHours,
	                                                IR.StartingValidity,
	                                                IR.StartingRenewal,
	                                                IR.AutomaticRenewal,
	                                                IR.Language
                                                FROM Trainings as TR
                                                INNER JOIN InstructorRoles as IR ON TR.RoleID = IR.ID
												LEFT JOIN TrainingTypes as  TT ON TR.TrainingTypeID = TT.ID 
                                                WHERE 
	                                                (TR.RoleID = @RoleID OR @RoleID IS NULL)
	                                                AND (TR.TrainingTypeID = @TrainingTypeID OR @TrainingTypeID IS NULL)
	                                                --AND (TR.CreatorID = @CreatorID OR @CreatorID IS NULL)
													AND (TR.TrainingDate >= @StartDate AND TR.TrainingDate <= @EndDate)
                                                    AND (TR.TrainingLocation = @TrainingLocation OR @TrainingLocation IS NULL)
                                                    AND (TR.RegionID = @TrainingRegion OR @TrainingRegion IS NULL)
													AND (ISNULL(TR.IsCompleted,0) = @IsTrainingCompleted OR @IsTrainingCompleted IS NULL)
                                                ORDER BY TR.TrainingDate DESC ";


        public static string GetTraining => @"SELECT 
	                                            TR.*,
	                                            IR.ID,
	                                            IR.Name,
	                                            IR.RequiredHours,
                                                IR.ExpirationDaysRule,
	                                            IR.StartingValidity,
	                                            IR.StartingRenewal,
	                                            IR.AutomaticRenewal,
	                                            IR.Language
                                            FROM Trainings as TR
                                            INNER JOIN InstructorRoles as IR ON TR.RoleID = IR.ID
                                            WHERE TR.ID = @ID";

        public static string GetTrainingForDropDown
            =>
                @"SELECT TR.ID, TR.Name FROM Trainings as TR 
                    WHERE (TR.IsCompleted = @TrainingCompleted OR @TrainingCompleted IS NULL)";


        public static string GetCountOfInstructorsInTraining =>
            @"SELECT COUNT(ID) as 'InstructorsCount' FROM InstructorTrainings WHERE TrainingID = @TrainingID";

        public static string DeleteTraining => @"DELETE FROM Trainings WHERE ID = @ID";

        public static string GetInstructorsTrainingByTrainingID => @"SELECT 
	                                                                    IT.*,
	                                                                    TR.*,
	                                                                    INS.*,
	                                                                    ISTR.*
                                                                    FROM InstructorTrainings as IT
                                                                    INNER JOIN Trainings as TR ON IT.TrainingID = TR.ID
                                                                    INNER JOIN Instructors as INS ON IT.InstructorID = INS.ID
                                                                    INNER JOIN InstructorRoles as ISTR ON INS.RoleID = ISTR.ID
                                                                    WHERE IT.TrainingID = @TrainingID AND INS.IsActive = 1";

        public static string RegisterInstructorToTraining =>
            @"INSERT INTO InstructorTrainings (TrainingID,InstructorID,CountToValidity) VALUES (@TrainingID,@InstructorID,@CountToValidity)";

        public static string InsertRecordHistory
            =>
                @"INSERT INTO InstructorRecordsHistory (InstructorID,ValidityStartDate,ValidityEndDate,CreationDate,UserID)
VALUES (@InstructorID, @ValidityStartDate, @ValidityEndDate, @CreationDate, @UserID)";

        public static string CheckInstuctorExistByInstructorID =>
            @"SELECT ID FROM Instructors WHERE InstructorID = @InstructorID";

        public static string GetInstructorsByRole => @"SELECT * FROM Instructors WHERE RoleID = @RoleID AND IsActive = 1";

        public static string GetInstructorsByRoleNotInTraining => @"SELECT * FROM Instructors
                                                                    WHERE RoleID = @RoleID AND ID NOT IN (SELECT InstructorID FROM InstructorTrainings WHERE TrainingID = @TrainingID)";

        public static string UpdateInstructorRecords => @"UPDATE Instructors
                                                          SET UpdateDate = @UpdateDate, ValidityStartDate = @ValidityStartDate, ValidityEndTime = @ValidityEndTime, CertificateID = @CertificateID
                                                          WHERE ID = @ID";


        public static string GetTotalHoursOfTrainingForInstructor => @"SELECT SUM(TR.TrainingHours) as TotalHours 
                                                                            FROM InstructorTrainings as IT
                                                                            INNER JOIN Trainings as TR ON IT.TrainingID = TR.ID
                                                                            INNER JOIN Instructors as IST ON IT.InstructorID = IST.ID
                                                                            WHERE (TR.TrainingDate >= @Date OR @Date IS NULL)
                                                                            AND IT.CountToValidity = 1
                                                                            AND IT.InstructorID = @InstructorID";

        public static string GetTotalHoursOfTrainingForInstructorbyTrainingType => @"SELECT SUM(TR.TrainingHours) as TotalHours 
                                                                            FROM InstructorTrainings as IT
                                                                            INNER JOIN Trainings as TR ON IT.TrainingID = TR.ID
                                                                            INNER JOIN Instructors as IST ON IT.InstructorID = IST.ID
                                                                            WHERE TR.TrainingDate >= @Date
                                                                            AND IT.CountToValidity = 1
                                                                            AND IT.InstructorID = @InstructorID
                                                                            AND TR.TrainingTypeID = @TrainingTypeID";

        public static string GetInstructorsFiles
            => @"SELECT * FROM InstructorFiles WHERE InstructorID = @InstructorID ORDER BY CreationDate DESC";

        public static string GetInstructorNotes
            => @"SELECT IR.*, US.*
	                FROM InstructorRemarks as IR
	                INNER JOIN Users US ON IR.UserID = US.ID
	                WHERE IR.InstructorID = @InstructorID ORDER BY IR.UpdateDate DESC";

        public static string InsertInstructorNote => @"INSERT INTO InstructorRemarks (UserID,InstructorID,Notes,UpdateDate,CreationDate)
	                                                    VALUES (@UserID, @InstructorID, @Notes, @UpdateDate, @CreationDate)
                                                        SELECT CAST(SCOPE_IDENTITY() as int)";

        public static string GetInstructorNote => @"SELECT IR.*, US.*
	                                                FROM InstructorRemarks as IR
	                                                INNER JOIN Users US ON IR.UserID = US.ID
	                                                WHERE IR.ID = @ID";

        public static string UpdateInstructorNote => @"UPDATE InstructorRemarks
	                                                    SET Notes = @Notes, UserID = @UserID, UpdateDate = @UpdateDate
	                                                    WHERE ID = @ID";

        public static string DeleteInstructorRemark => @"DELETE InstructorRemarks WHERE ID = @ID";

        public static string InsertInstructorFile
            => @"INSERT INTO InstructorFiles (InstructorID,Name,Url,Comments,CreationDate,UserID)
	VALUES (@InstructorID,@Name,@Url,@Comments,@CreationDate,@UserID)";

        public static string UpdateInstructorFile => @"UPDATE InstructorFiles
	                                                    SET Name = @Name, Comments = @Comments
	                                                    WHERE ID = @ID";

        public static string GetInstructorFile => @"SELECT INF.*, US.* FROM InstructorFiles as INF
	                                                INNER JOIN Users as US ON INF.UserID = US.ID
	                                                WHERE INF.ID = @ID";

        public static string GetInstructorTrainings => @"SELECT IST.*, TR.*
	                                                    FROM InstructorTrainings as IST
	                                                    INNER JOIN Trainings as TR ON IST.TrainingID = TR.ID
	                                                    WHERE IST.InstructorID = @InstructorID
                                                        ORDER BY TR.TrainingDate DESC";

        public static string RegisterInstructorToGroup
            => @"INSERT INTO InstructorGroups (GroupID,InstructorID,RoleID) VALUES (@GroupID, @InstructorID,@RoleID)";

        public static string GetGroupsInstructors => @"SELECT IG.*, INST.*, ISR.*, SC.Name as SchoolName
                                                        FROM InstructorGroups as IG
                                                        INNER JOIN Instructors as INST ON IG.InstructorID = INST.ID
                                                        INNER JOIN InstructorRoles as ISR ON IG.RoleID = ISR.ID
														LEFT JOIN Schools as SC ON IG.SchoolID = SC.ID
                                                        WHERE IG.GroupID = @GroupID AND INST.IsActive = 1";

        public static string GetGroupInstructorShortData => @"SELECT 
	                                                        IG.ID, 
	                                                        IG.InstructorID as InstructorInternalID, 
	                                                        INST.FirstNameHE, INST.LastNameHE, 
	                                                        INST.FirstNameEN, 
	                                                        INST.LastNameEN, 
	                                                        INST.InstructorID, 
	                                                        INST.PassportID, 
	                                                        ISR.Name as RoleName, 
	                                                        SC.Name as SchoolName, 
	                                                        INST.ValidityEndTime,
	                                                        IG.IsGroupManager, 
                                                            IG.IsDeputyDirector,
	                                                        IG.RoleID,
	                                                        INST.ImageURL
                                                        FROM InstructorGroups as IG
                                                        INNER JOIN Instructors as INST ON IG.InstructorID = INST.ID
                                                        INNER JOIN InstructorRoles as ISR ON INST.RoleID = ISR.ID
														LEFT JOIN Schools as SC ON IG.SchoolID = SC.ID
                                                        WHERE IG.GroupID = @GroupID AND INST.IsActive = 1";

        public static string CheckIfInstructorIsRegisteredToGroup => @"SELECT IG.ID 
	                                                                    FROM InstructorGroups as IG
	                                                                    INNER JOIN Instructors as INS ON IG.InstructorID = INS.ID
	                                                                    WHERE INS.InstructorID = @InstructorID AND IG.GroupID = @GroupID";

        public static string CheckIfInstructorIsValid
            => @"SELECT ID FROM Instructors WHERE InstructorID = @InstructorID AND ValidityEndTime >= GETDATE()";

        public static string GetInstructorByInstructorID =>
                                                            @"SELECT INS.*, IR.*
                                                FROM Instructors as INS
                                                INNER JOIN InstructorRoles as IR ON INS.RoleID = IR.ID
                                                WHERE InstructorID = @InstructorID";

        public static string CheckIfInstructorIsInTrainingInTimeFrame => @"	SELECT ISG.GroupID
	                                                                            FROM InstructorGroups as ISG
	                                                                            INNER JOIN Instructors as INST ON ISG.InstructorID = ISG.InstructorID
	                                                                            INNER JOIN Groups as GP ON ISG.GroupID = GP.ID
	                                                                            INNER JOIN Expeditions as EX ON GP.OrderID = Ex.ID
	                                                                            INNER JOIN Trips as TR ON EX.TripID = TR.ID
	                                                                            WHERE 
		                                                                            INST.InstructorID = @InstructorID
		                                                                            AND ISG.GroupID != @GroupID
		                                                                            AND 
		                                                                            (TR.DepartureDate >= @StartDate AND TR.DepartureDate <= @EndDate
		                                                                            OR TR.ReturningDate >= @StartDate AND TR.ReturningDate <= @EndDate)";

        public static string CheckIfInstructorIsInTrainingInTimeFrameByID => @"	SELECT ISG.GroupID
	                    FROM InstructorGroups as ISG
	                    INNER JOIN Groups as GP ON ISG.GroupID = GP.ID
	                    INNER JOIN Expeditions as EX ON GP.OrderID = Ex.ID
	                    INNER JOIN Trips as TR ON EX.TripID = TR.ID
	                    WHERE 
		                    ISG.InstructorID = @InstructorID
		                    AND ISG.GroupID != @GroupID
		                    AND (TR.DepartureDate >= @StartDate AND TR.DepartureDate <= @EndDate
		                    OR TR.ReturningDate >= @StartDate AND TR.ReturningDate <= @EndDate)";

        public static string GetTripByGroupID = @"SELECT TR.*
                                                FROM Groups as GP
                                                INNER JOIN Expeditions as EX ON GP.OrderID = Ex.ID
                                                INNER JOIN Trips as TR ON EX.TripID = TR.ID
                                                WHERE GP.ID = @ID";


        public static string DeleteInstructorFile => @"DELETE FROM InstructorFiles WHERE ID = @ID";

        public static string UpdateInstructorTraining => @"UPDATE InstructorTrainings
                                                            SET CountToValidity = @CountToValidity
                                                            WHERE ID = @ID";

        public static string UpdateInstructorProfilePicture => @"UPDATE Instructors
                                                                    SET ImageURL = @ImageURL
                                                                    WHERE ID = @ID";

        public static string UpdateGroupContactDetails => @"UPDATE Groups
                                                            SET ContactName = @ContactName, ContactPhone = @ContactPhone
                                                            WHERE ID = @ID";

        public static string UpdateGroupFinalStatus => @"UPDATE Groups SET FinalStatus = @FinalStatus WHERE ID = @ID";

        public static string GetGroupByID => @"SELECT * FROM Groups WHERE ID = @ID";

        public static string SearchForInstructor => @"SELECT ID as InternalID, InstructorID as ID, FirstNameHE as FirstName, LastNameHE as LastName, ImageURL, ValidityEndTime FROM Instructors
                                                        WHERE 
                                                        RoleID IN @RoleID AND
                                                        (InstructorID like '%' + @SearchTerm + '%'
                                                        OR FirstNameHE like '%' + @SearchTerm + '%'
                                                        OR LastNameHE like '%' + @SearchTerm + '%'
                                                        OR FirstNameHE + ' ' + LastNameHE like '%' + @SearchTerm + '%')";

        public static string SearchForManager => @"SELECT 
	INST.ID as InternalID, 
	INST.InstructorID as ID, 
	INST.FirstNameHE as FirstName, 
	INST.LastNameHE as LastName, 
	INST.ImageURL, 
	INST.ValidityEndTime, 
	INST.Email,
	IR.Name as RoleName
FROM Instructors as INST
INNER JOIN InstructorRoles as IR ON INST.RoleID = IR.ID
                                                        WHERE 
														RoleID IN @RoleID AND
                                                        (InstructorID like '' + @SearchTerm + '%'
                                                        OR FirstNameHE like '%' + @SearchTerm + '%'
                                                        OR LastNameHE like '%' + @SearchTerm + '%'
                                                        OR FirstNameHE + ' ' + LastNameHE like '%' + @SearchTerm + '%')";

        public static string UnregisterInstructorTraining => @"DELETE InstructorTrainings WHERE ID = @ID";

        public static string DeactivateSchool => @"UPDATE Schools SET IsActive = 0 WHERE ID = @ID";

        public static string DeactivateUserbySchool => @"UPDATE Users SET IsActive = 0 WHERE SchoolID = @ID";

        public static string GetAllSchools => @"SELECT 
	                                                SC.ID,
	                                                SC.InstituteID,
	                                                SC.Name,
	                                                SC.Email,
	                                                SC.Fax,
	                                                SC.Phone,
	                                                SC.Manager,
	                                                SC.ManagerPhone,
	                                                SC.ManagerEmail,
	                                                SC.IsActive,
	                                                SC.City,
	                                                SC.Region,
	                                                US.IsActivate as 'IsActivated'
                                                FROM 
                                                Schools as SC
                                                INNER JOIN Users as US ON SC.InstituteID = US.Email AND US.UserType = 4 --School
                                                WHERE
	                                                SC.IsActive = 1
	                                                AND (
	                                                (SC.InstituteID like '%' + @InstituteID + '%' OR @InstituteID IS NULL)
	                                                AND (SC.Name like '%' + @Name + '%' OR @Name IS NULL)
	                                                AND (SC.Email like '%' + @Email + '%' OR @Email IS NULL))
	                                                ORDER BY SC.InstituteID DESC";


        public static string GetNumberOfRows => @"SELECT COUNT(ID) as NumberOfRows FROM {0}";

        public static string GetGroupsReport => @"SELECT 
	                                                GP.ID as GroupID,
	                                                GP.GroupExternalID as GroupExternalID,
	                                                GP.NumberOfBuses,
	                                                GP.NumberOfParticipants,
	                                                SC.InstituteID as SchoolInsititueID,
	                                                SC.Name as SchoolName,
	                                                AG.Name as AgencyName,
	                                                TR.DepartureDate,
	                                                TR.DestinationID,
	                                                TR.ReturningDate,
	                                                TR.ReturingField
                                                FROM Groups GP
                                                INNER JOIN Expeditions as EX ON GP.OrderID = EX.ID
                                                INNER JOIN Trips as TR ON EX.TripID = TR.ID
                                                INNER JOIN Schools SC ON EX.SchoolID = SC.ID
                                                INNER JOIN Agencies as AG ON GP.AgencyID = AG.ID
                                                WHERE 
                                                GP.IsActive = 1 AND
                                                (TR.DepartureDate >= @StartDate AND TR.DepartureDate <= @EndDate)
                                                AND (AG.ID = @AgencyID OR @AgencyID IS NULL)
                                                AND (TR.DestinationID = @DepartureID OR @DepartureID IS NULL)
                                                AND (TR.ReturingField = @ReturningField OR @ReturningField IS NULL)";

        public static string GetAllAgencies => @"SELECT * FROM Agencies WHERE IsActive = 1";

        public static string GetInstructorTrainingReport => @"SELECT
	                                                            INS.ID,
	                                                            INS.InstructorID,
	                                                            INS.FirstNameHE + ' ' + INS.LastNameHE as 'FullNameHE',
	                                                            INS.City,
	                                                            INS.CellPhone,
	                                                            INS.Email,
	                                                            IR.Name as 'InstructorRole',
	                                                            INS.OrganizationalStatus,
	                                                            TR.Name as 'TrainingName',
	                                                            TR.TrainingDate,
	                                                            TR.TrainingType,
	                                                            TR.TrainingHours,
	                                                            TR.IsCompleted,
	                                                            TR.TrainingLocation
                                                            FROM InstructorTrainings as IT
                                                            INNER JOIN Instructors as INS ON IT.InstructorID = INS.ID
                                                            INNER JOIN InstructorRoles as IR ON INS.RoleID = IR.ID
                                                            INNER JOIN Trainings as TR ON IT.TrainingID = TR.ID
                                                            WHERE (TR.TrainingType = @TrainingType OR @TrainingType IS NULL)
                                                            AND (TR.TrainingLocation = @TrainingLocation OR @TrainingLocation IS NULL)
                                                            AND (INS.OrganizationalStatus = @OrgStatus OR @OrgStatus IS NULL)
                                                            AND (TR.IsCompleted = @TrainingCompleted OR @TrainingCompleted IS NULL)
                                                            AND (TR.ID = @TrainingID OR @TrainingID IS NULL)
                                                            AND (TR.RoleID = @InstructorType OR @InstructorType IS NULL)";

        public static string CheckGroupIDExist => @"SELECT G.ID FROM Groups as G
                                                    INNER JOIN Expeditions O ON G.OrderID = O.ID
                                                    INNER JOIN Trips as T ON O.TripID = T.ID
                                                    WHERE G.GroupExternalID = @GroupID AND T.DepartureDate >= @StartDate AND T.DepartureDate <= @EndDate";

        public static string UpdateGroupExternalID => @"UPDATE Groups SET GroupExternalID = @GroupID WHERE ID = @ID";

        public static string CheckIfInstructorIsAlreadyAllocatedToInitialTraining => @"SELECT IT.ID 
                                                                                        FROM InstructorTrainings as IT
                                                                                        INNER JOIN Trainings as TR On IT.TrainingID = TR.ID
                                                                                        INNER JOIN Instructors as INS On IT.InstructorID = INS.ID
                                                                                        INNER JOIN InstructorRoles as IR ON INS.RoleID = IR.ID
                                                                                        WHERE IT.InstructorID = @InstructorID AND TR.TrainingType = @TrainingType AND IR.AllowChangingRole = 0";

        public static string CheckIfInstructorIsRegisteredToTrainingID
            => @"SELECT ID FROM InstructorTrainings WHERE InstructorID = @InstructorID AND TrainingID = @TrainingID";

        public static string GetInstructorTrainingRecordByID => @"SELECT * FROM InstructorTrainings WHERE ID = @ID";

        public static string GetInstructorGroups => @"SELECT 
	                                                    IG.InstructorID as InstructorId,
	                                                    IG.GroupID as GroupId,
	                                                    GP.GroupExternalID as GroupExternalId,
	                                                    TR.DepartureDate as GroupDepartureDate,
	                                                    TR.DestinationID as DestinationId,
	                                                    SC.InstituteID as InstituteId,
	                                                    SC.Name as SchoolName
                                                    FROM InstructorGroups as IG
                                                    INNER JOIN Groups as GP ON IG.GroupID = GP.ID
                                                    INNER JOIN Expeditions as ORD ON GP.OrderID = ORD.ID
                                                    INNER JOIN Trips as TR ON ORD.TripID = TR.ID
                                                    INNER JOIN Schools as SC ON ORD.SchoolID = SC.ID
                                                    WHERE IG.InstructorID = @InstructorId";


        public static string GetGroupsBySchoolID => @"SELECT 
	                                                    GP.ID,
	                                                    GP.OrderID,
	                                                    GP.NumberOfParticipants,
	                                                    GP.NumberOfBuses,
	                                                    Gp.GroupExternalID,
	                                                    GP.Status,
	                                                    GP.CreationDate,
	                                                    GP.UpdateDate,
	                                                    TR.DepartureDate
                                                    FROM SubGroupSchools as SGP
                                                    INNER JOIN Groups as GP ON SGP.GroupID = GP.ID
                                                    INNER JOIN Expeditions as EX ON GP.OrderID = EX.ID
                                                    INNER JOIN Trips as TR ON EX.TripID = TR.ID
                                                    WHERE SGP.SchoolID = @ID";

        public static string GetAgencyReport => @";WITH ExpedtionsWithTrips AS (
	SELECT 
		EX.ID,
		EX.Status,
		TR.ID as TripID,
		EX.BasesPerDay,
		TR.DepartureDate,
		TR.DestinationID,
		TR.ReturingField,
		TR.ReturningDate,
		G.ID AS GroupID,
		G.GroupExternalID,
		SGS.SchoolID,
		EX.AgencyID
	FROM Expeditions as EX
	INNER JOIN Trips as TR ON EX.TripID = TR.ID
	INNER JOIN Groups AS g ON g.OrderID = EX.ID
	INNER JOIN SubGroupSchools sgs ON sgs.GroupID = G.ID
	WHERE
	TR.DepartureDate >= @StartDate AND TR.DepartureDate <= @EndDate
),
EXFlights as (
SELECT 
	EF.ExpeditionID,
	SUM(EF.NumberOfPassengers) as NumberOfPassengers
FROM ExpeditionsFlights as EF
INNER JOIN Flights as FL ON EF.FlightID = FL.ID
WHERE FL.DestinationID = 1
GROUP BY
	EF.ExpeditionID
),
CostPerStudent AS (
SELECT 
	sgs.SchoolID,
	sgs.GroupID,
	gf.GroupSchoolID,
	af.CostPerStudent
FROM SubGroupSchools sgs
INNER JOIN GroupForms gf ON sgs.ID = gf.GroupSchoolID
INNER JOIN AdministrationForm180 af ON gf.ID = af.GroupFormID
)

SELECT 
EX.ID as ExpeditionID,
EX.Status as ExpeditionStatus,
EX.TripID as TripID,
SC.Name as SchoolName,
SC.ID as SchoolID,
SC.Region AS RegionID,
EX.BasesPerDay as BusesRequiredPerDay,
SC.InstituteID,
EX.DepartureDate,
EX.DestinationID,
EX.ReturingField,
EX.ReturningDate as ReturnDate,
AG.Name as AgencyName,
ISNULL(EF.NumberOfPassengers, 0) as RegisteredPassengers,
EX.GroupExternalID,
CPS.CostPerStudent
FROM ExpedtionsWithTrips as EX
INNER JOIN Schools as SC ON EX.SchoolID = SC.ID
INNER JOIN Agencies as AG ON EX.AgencyID = AG.ID
LEFT JOIN EXFlights as EF ON EF.ExpeditionID = EX.ID
LEFT JOIN CostPerStudent AS CPS ON Ex.SchoolID = CPS.SchoolID AND EX.GroupID = CPS.GroupID
WHERE
(EX.Status NOT IN (@Status) OR @Status IS NULL)
AND (EX.AgencyID = @AgencyID OR @AgencyID IS NULL)";

        public static string GetAllSchoolsReport => @"	SELECT DISTINCT
		G.ID as GroupID,
		G.GroupExternalID as GroupExternalID,
		G.NumberOfBuses,
		G.NumberOfParticipants as NumberOfPassengers,
		G.Status as GroupStatus,
		TR.ID as TripID,
		TR.DepartureDate,
		SC.ID as ID,
		SC.InstituteID,
		SC.Name,
		SC.Region as RegionID,
		SC.Email,
		SC.Fax,
		SC.Phone,
		SC.Manager,
		SC.ManagerPhone,
		SC.ManagerEmail,
		US.IsActivate,
		SC.CreationDate,
		SC.UpdateDate
	FROM Groups as G
	INNER JOIN Expeditions as EX ON G.OrderID = EX.ID
	INNER JOIN SubGroupSchools as SGS ON G.ID = SGS.GroupID
	INNER JOIN Schools as SC ON SGS.SchoolID = SC.ID
	INNER JOIN Users as US ON SC.InstituteID = US.Email AND US.UserType = 4
	INNER JOIN Trips as TR ON Ex.TripID = TR.ID
	WHERE 
        G.IsActive = 1
        AND (
		TR.DepartureDate >= @StartDate AND TR.DepartureDate <= @EndDate	
		AND (SC.InstituteID = @InstituteID OR @InstituteID IS NULL)
		AND (G.GroupExternalID = @GroupID OR @GroupID IS NULL))";


        public static string GetGroupSubSchools => @"SELECT 
	                                                    SG.ID,
	                                                    SG.GroupID,
	                                                    SG.SchoolID,
	                                                    SG.IsPrimary,
	                                                    SC.InstituteID,
	                                                    SC.Name as SchoolName,
														SC.IsAdministrativeInstitude,
														SC.Region,
														SC.Manager,
														SC.ManagerPhone,
														SC.ManagerEmail
                                                    FROM SubGroupSchools as SG
                                                    INNER JOIN Schools as SC ON SG.SchoolID = SC.ID
                                                    WHERE SG.GroupID = @GroupID";

        public static string AddSchool => @"INSERT INTO Schools (InstituteID, Name, Email, Fax, Phone, Manager, ManagerPhone, ManagerEmail, IsActive, CreationDate, UpdateDate, City, Region, IsAdministrativeInstitude)
VALUES (@InstituteID, @Name, @Email, @Fax, @Phone, @Manager, @ManagerPhone, @ManagerEmail, @IsActive, @CreationDate, @UpdateDate, @City, @Region, @IsAdministrativeInstitude);
        SELECT CAST(SCOPE_IDENTITY() as int)";


        public static string AddSubSchool => @"INSERT INTO SubGroupSchools (GroupID, SchoolID, IsPrimary, NumberOfInstructors)
                                                VALUES (@GroupID, @SchoolID, @IsPrimary,@NumberOfInstructors)";

        public static string UpdateSubSchool => @"UPDATE SubGroupSchools SET NumberOfInstructors = @NumberOfInstructors WHERE ID = @ID";

        public static string GetSubSchoolByID => @"SELECT * FROM SubGroupSchools WHERE ID = @ID";

        public static string GetSubSchoolsByGroupID => @"SELECT 
	                                                        SGS.SchoolID as ID,
	                                                        SC.Name
                                                        FROM SubGroupSchools as SGS
                                                        INNER JOIN Schools as SC ON SGS.SchoolID = SC.ID
                                                        WHERE SGS.GroupID = @GroupID";

        public static string GetCountyManagerSchoolsAssosicationByUserID => @"SELECT 
	                                                                             USA.SchoolID as ID,
	                                                                             SC.InstituteID,
	                                                                             SC.Name,
	                                                                             SC.Email,
	                                                                             SC.Fax,
	                                                                             SC.Phone,
	                                                                             SC.Manager
                                                                            FROM CountyManagersSchoolsAssosication as USA
                                                                            INNER JOIN Schools as SC ON USA.SchoolID = SC.ID
                                                                            WHERE USA.IsActive = 1 AND USA.UserID = @UserID";

        public static string GetSchoolsToAssociateToCountyManager => @"SELECT 
	                                                                    SC.ID,
	                                                                    CAST(SC.InstituteID as Nvarchar(500)) + ' - ' + SC.Name as Name
                                                                    FROM Schools as SC
                                                                    WHERE SC.IsActive = 1 AND SC.ID NOT IN (SELECT SchoolID FROM CountyManagersSchoolsAssosication WHERE UserID = @UserID)";

        public static string AssociateSchoolToUserID => @"INSERT INTO CountyManagersSchoolsAssosication (SchoolID,UserID,IsActive)
                                                            VALUES (@SchoolID, @UserID, 1)";

        public static string GetAssosicateSchoolIDToDelete => @"SELECT ID FROM CountyManagersSchoolsAssosication WHERE UserID = @UserID AND SchoolID = @SchoolID";

        public static string DeleteAssosicatedSchool => @"DELETE FROM CountyManagersSchoolsAssosication WHERE ID = @ID";

        public static string UpdateCountyManagerFileApproval => @"UPDATE GroupFiles SET CountyManagerApproval = @CountyManagerApproval, CountyManagerComments = @CountyManagerComments WHERE ID = @FileID";

        public static string GetAllNotificationTemplage => @"SELECT 
	                                                            NT.ID,
	                                                            NT.Name,
	                                                            NT.TriggerType,
	                                                            NT.Subject,
	                                                            NT.Message,
	                                                            NT.SchedulingType,
	                                                            NT.IsActive,
	                                                            NT.UserType
                                                            FROM NotificationTempates as NT";

        public static string UnassignInstructorFromGroup => @"DELETE FROM InstructorGroups WHERE ID = @ID";

        public static string CheckSubSchoolDeletionIsAllowed => @"SELECT GR.ID FROM GroupRemarks as GR WHERE GR.SchoolID = @SchoolID AND GR.GroupID = @GroupID";

        public static string DeleteSubSchool => @"DELETE SubGroupSchools WHERE ID = @ID";


        #region Notifications

        public static string AddNotificationTemplate => @"INSERT INTO NotificationTempates (Name,TriggerType,Subject,Message,SchedulingType,IsActive,UserType, UserPermissionRole)
                                                        VALUES (@Name, @TriggerType, @Subject, @Message, @SchedulingType, @IsActive, @UserType, @UserPermissionRole)";

        public static string GetNotificationTempalteByID => @"SELECT 
	                                                            NT.ID,
	                                                            NT.Name,
	                                                            NT.TriggerType,
	                                                            NT.Subject,
	                                                            NT.Message,
	                                                            NT.SchedulingType,
	                                                            NT.IsActive,
	                                                            NT.UserType,
                                                                NT.UserPermissionRole
                                                            FROM NotificationTempates as NT
                                                            WHERE NT.ID = @ID";

        public static string UpdateNotificationTemplate => @"UPDATE NotificationTempates
                                                            SET Name = @Name, TriggerType = @TriggerType, Subject = @Subject, Message = @Message, SchedulingType = @SchedulingType, IsActive = @IsActive, UserType = @UserType, UserPermissionRole = @UserPermissionRole
                                                            WHERE ID = @ID";

        public static string GetNotificationTemplatesByTrrigerType => @"SELECT * FROM NotificationTempates WHERE TriggerType = @TriggerType";

        public static string InsertNotification => @"INSERT INTO Notifications (NotificationTempateID,UserID,EmailAddress,NotificationStatus,MessageSubject,MessageBody,CallToAction, CreationDate, IsRead)
                                                    VALUES (@NotificationTempateID, @UserID, @EmailAddress, @NotificationStatus, @MessageSubject, @MessageBody, @CallToAction, @CreationDate, @IsRead)";

        public static string InsertGlobalNotification => @"INSERT INTO Notifications (UserID,EmailAddress,NotificationStatus,MessageSubject,MessageBody, CreationDate, IsRead)
                                                    VALUES (@UserID, @EmailAddress, @NotificationStatus, @MessageSubject, @MessageBody, @CreationDate, @IsRead)";

        public static string GetNotificationsToBeSnetByStatus => @"SELECT * FROM Notifications WHERE NotificationStatus = @Status";

        public static string UpdateNotificationToBeSnet => @"UPDATE Notifications SET NotificationStatus = @NotificationStatus, SendDate = @SendDate, Remark = @Remark WHERE ID = @ID";

        public static string GetUserNotificationsByDate => @"SELECT 
	                                                            N.ID,
	                                                            N.NotificationTempateID,
	                                                            N.MessageSubject,
	                                                            N.MessageBody,
	                                                            N.CreationDate,
	                                                            N.NotificationStatus,
                                                                N.CallToAction
                                                            FROM Notifications as N
                                                            WHERE 
                                                            N.CreationDate > @Date 
                                                            AND N.UserID = @UserID
                                                            AND (N.IsRead = @IsRead OR @IsRead IS NULL)
                                                            ORDER BY N.CreationDate DESC";

        public static string GetUserNotificationCountbyDate => @"SELECT 
	                                                            COUNT(N.ID)
                                                            FROM Notifications as N
                                                            WHERE 
                                                            N.CreationDate > @Date 
                                                            AND N.UserID = @UserID
                                                            AND N.IsRead = 0";

        public static string MarkNotificationAsRead => @"UPDATE Notifications SET IsRead = 1 WHERE ID = @ID";



        public static string GetGroup180FormNotificationDataCreation => @"SELECT 
	                                                        G.ID as GroupID,
	                                                        g.GroupExternalID as GroupExternalID,
	                                                        SC.ID as SchoolID,
	                                                        SC.Name as SchoolName
                                                        FROM Groups as G
                                                        INNER JOIN SubGroupSchools as SG ON G.ID = SG.GroupID
                                                        INNER JOIN Schools as SC ON SG.SchoolID = SC.ID
                                                        WHERE g.GroupExternalID = @GroupExternalID 
														AND G.OrderID = 5686 
														AND  SC.IsAdministrativeInstitude = 0";

        public static string GetGroup180AdministrativeFormNotificationStatusUpdate => @"SELECT 
	                                                                                        g.ID as GroupID,
	                                                                                        g.GroupExternalID as GroupExternalID,
	                                                                                        SC.ID as SchoolID,
	                                                                                        SC.Name as SchoolName,
	                                                                                        AF.UpdateDate
                                                                                        FROM AdministrationForm180 as AF
                                                                                        INNER JOIN GroupForms as GF ON AF.GroupFormID = GF.ID
                                                                                        INNER JOIN SubGroupSchools as SG on GF.GroupSchoolID = SG.ID
                                                                                        INNER JOIN Groups as G ON SG.GroupID = G.ID
                                                                                        INNER JOIN Schools as SC ON SG.SchoolID = SC.ID
                                                                                        WHERE AF.ID = @FormID";

        public static string GetGroup180InstructorFormNotificationStatusUpdate => @"SELECT 
	                                                                                    g.ID as GroupID,
	                                                                                    g.GroupExternalID as GroupExternalID,
	                                                                                    SC.ID as SchoolID,
	                                                                                    SC.Name as SchoolName,
	                                                                                    AF.UpdateDate
                                                                                    FROM GroupInstructorsForm as AF
                                                                                    INNER JOIN GroupForms as GF ON AF.GroupFormID = GF.ID
                                                                                    INNER JOIN SubGroupSchools as SG on GF.GroupSchoolID = SG.ID
                                                                                    INNER JOIN Groups as G ON SG.GroupID = G.ID
                                                                                    INNER JOIN Schools as SC ON SG.SchoolID = SC.ID
                                                                                    WHERE AF.ID = @FormID";

        public static string GetGroup180PlanFormNotificationStatusUpdate => @"SELECT 
	                                                                            g.ID as GroupID,
	                                                                            g.GroupExternalID as GroupExternalID,
	                                                                            SC.ID as SchoolID,
	                                                                            SC.Name as SchoolName,
	                                                                            AF.UpdateDate
                                                                            FROM GroupPlanForm as AF
                                                                            INNER JOIN GroupForms as GF ON AF.GroupFormID = GF.ID
                                                                            INNER JOIN SubGroupSchools as SG on GF.GroupSchoolID = SG.ID
                                                                            INNER JOIN Groups as G ON SG.GroupID = G.ID
                                                                            INNER JOIN Schools as SC ON SG.SchoolID = SC.ID
                                                                            WHERE AF.ID = @FormID";


        public static string DeleteOldNotifications => @"DELETE Notifications WHERE IsRead = 1 OR CreationDate > @Date";

        #endregion



        public static string GetEmergencyContactReportByDate => @"SELECT 
	                                                                SC.Name as SchoolName,
	                                                                SC.Region,
	                                                                GP.ContactName as EmergencyContactName,
																	GP.GroupExternalID as GroupID,
	                                                                Gp.ContactPhone as EmergencyContactPhone,
	                                                                SC.Manager as SchoolManagerName,
	                                                                SC.ManagerPhone as SchoolManagerPhone
                                                                FROM Groups as GP
                                                                INNER JOIN Expeditions as EX On GP.OrderID = EX.ID
                                                                INNER JOIN Trips as TR ON EX.TripID = TR.ID
                                                                INNER JOIn Schools as SC ON EX.SchoolID = SC.ID
                                                                WHERE TR.DepartureDate <= @TargetDate AND TR.ReturningDate >= @TargetDate";

        public static string GetStudentsInExpedition => @"SELECT 
	                                                        GPP.FirstNameHe as StudentFirstName,
	                                                        GPP.LastNameHe as StudentLastName,
	                                                        GPP.PassportNumber,
	                                                        GPP.ParticipantType,
                                                            GPP.ParticipantID,
	                                                        GP.GroupExternalID,
	                                                        SC.Name as SchoolName,
	                                                        SC.Region
                                                        FROM Groups as GP
                                                        INNER JOIN Expeditions as EX On GP.OrderID = EX.ID
                                                        INNER JOIN Trips as TR ON EX.TripID = TR.ID
                                                        INNER JOIN Schools as SC ON EX.SchoolID = SC.ID
                                                        INNER JOIN GroupParticipants as GPP ON GP.ID = GPP.GroupID
                                                        WHERE (TR.DepartureDate <= @EndDate AND TR.ReturningDate >= @StartDate)
                                                        AND (GPP.ParticipantID = @ParticipantID OR @ParticipantID IS NULL)";

        public static string GetInstructorsInExpedition => @"SELECT 
	INST.FirstNameHe as StudentFirstName,
	INST.LastNameHe as StudentLastName,
	INST.PassportID as PassportNumber,
    INST.InstructorID as ParticipantID,
	ROl.Name as RoleName,
	GP.GroupExternalID,
	SC.Name as SchoolName,
	SC.Region
FROM Groups as GP
INNER JOIN Expeditions as EX On GP.OrderID = EX.ID
INNER JOIN Trips as TR ON EX.TripID = TR.ID
INNER JOIN Schools as SC ON EX.SchoolID = SC.ID
INNER JOIN InstructorGroups as GPP ON GP.ID = GPP.GroupID
INNER JOIN Instructors as INST ON GPP.InstructorID = INST.ID AND INST.IsActive = 1
INNER JOIN InstructorRoles as ROL ON INST.RoleID = ROL.ID
WHERE (TR.DepartureDate <= @EndDate AND TR.ReturningDate >= @StartDate)
        AND (INST.InstructorID = @ParticipantID OR @ParticipantID IS NULL)";

        public static string GetSchoolAllGroupRemarks => @"SELECT 
	                                                        GP.ID as GroupID,
	                                                        GP.GroupExternalID,
	                                                        GR.UpdateDate,
	                                                        GR.Notes,
	                                                        GR.UserID,
	                                                        US.Firstname,
	                                                        US.Lastname
                                                        FROM GroupRemarks as GR
                                                        INNER JOIN Groups as GP ON GR.GroupID = GP.ID
                                                        INNER JOIN Users as US ON GR.UserID = US.ID
                                                        WHERE GR.SchoolID = @SchoolID";

        public static string GetGEnericRemarksByEntityID => @"SELECT 
	                                                            ER.ID,
	                                                            ER.UserID,
	                                                            ER.EntityType,
	                                                            ER.EntityID,
	                                                            ER.Notes,
	                                                            ER.UpdateDate,
	                                                            ER.CreationDate,
	                                                            US.Firstname,
	                                                            US.Lastname
                                                            FROM EntityRemarks as ER
                                                            INNER JOIN Users as US ON ER.UserID = US.ID
                                                            WHERE ER.EntityType = @EntityType
                                                            AND ER.EntityID = @ID";

        public static string AddGenericRemarkByEntityID => @"INSERT INTO EntityRemarks (UserID, EntityType, EntityID, Notes, UpdateDate, CreationDate)
VALUES (@UserID, @EntityType, @EntityID, @Notes, @UpdateDate, @CreationDate)";

        public static string DeleteGenericRemark => "DELETE FROM EntityRemarks WHERE ID = @ID";

        public static string DeleteGroupFormsBySchoolID => @"DELETE AdministrationForm180 WHERE GroupFormID = @GroupFormID
                                                                DELETE GroupInstructorsForm WHERE GroupFormID = @GroupFormID
                                                                DELETE GroupPlanDetails WHERE ID IN (
                                                                SELECT GPD.ID
                                                                FROM GroupPlanForm as GP
                                                                INNER JOIN GroupPlanDetails as GPD ON GP.ID = GPD.GroupPlanID
                                                                WHERE GP.GroupFormID = @GroupFormID)
                                                                DELETE GroupPlanForm WHERE GroupFormID = @GroupFormID
                                                                DELETE GroupForms WHERE ID = @GroupFormID";

        public static string PermenentDeleteGroupByID => @" DELETE GroupParticipants WHERE GroupID = @GroupID
                                                            DELETE InstructorGroups WHERE GroupID = @GroupID
                                                            DELETE GroupRemarks WHERE GroupID = @GroupID
                                                            DELETE GroupFiles WHERE GroupID = @GroupID
                                                            DELETE SubGroupSchools WHERE GroupID = @GroupID
                                                            DELETE Groups WHERE ID = @GroupID";

        public static string PermenentDeleteGroupFormsSteps = @"DELETE FROM GroupInstructorsForm WHERE GroupFormID IN @GroupForms";

        public static string PermenetDeleteGroupForms = @"DELETE FROM GroupForms WHERE GroupSchoolID IN @GroupSchoolIDs";

        public static string PermenetDeleteSubSchools = @"DELETE FROM SubGroupSchools WHERE ID IN @SubSchoolIDs";

        public static string GetGroupFormsIDs = @"SELECT ID FROM GroupForms WHERE GroupSchoolID IN @GroupSchoolIDs";

        public static string GetGroupFormID => @"SELECT ID FROM GroupForms WHERE GroupSchoolID = @SubSchoolID";

        public static string UpdateGroupAgencyID => @"UPDATE Groups SET AgencyID = @AgencyID WHERE ID = @ID";

        public static string GetGroupAgencyID => @"SELECT AgencyID FROM Groups WHERE ID = @ID";

        #region DailyDashboard

        public static string GetDailyDashboardSummary => @";WITH GroupIstructors AS (
	                                                            SELECT 
		                                                            IG.GroupID,
		                                                            ISNULL(COUNT(IG.ID),0) as NumberOfINstructors
	                                                            FROM InstructorGroups as IG
	                                                            GROUP BY
		                                                            IG.GroupID
                                                            ),
                                                            NumberOfParticipants AS (
	                                                            SELECT 
		                                                            GP.GroupID,
		                                                            ISNULL(COUNT(GP.ID), 0) as NumberOfParticipants 
	                                                            FROM GroupParticipants as GP
	                                                            GROUP BY GP.GroupID
                                                            ),
                                                            DepartureFlights AS (
	                                                            SELECT 
		                                                            EF.ExpeditionID,
		                                                            FL.TakeOffFromIsrael,
		                                                            MIN(FL.ArrivalTime) as ArrivalTime,
		                                                            TF.TripID
	                                                            FROM ExpeditionsFlights as EF
	                                                            INNER JOIN dbo.Flights as FL ON EF.FlightID = FL.ID
	                                                            INNER JOIN TripsFlightAssociations as TF ON FL.ID = TF.FlightID
	                                                            WHERE 
	                                                            FL.IsActive = 1 
	                                                            AND FL.TakeOffFromIsrael = 1
	                                                            AND FL.ArrivalTime <= @StartTime
	                                                            GROUP BY
		                                                            EF.ExpeditionID,
		                                                            FL.TakeOffFromIsrael,
		                                                            TF.TripID
                                                            ),
                                                            ReturningFlights AS (
	                                                            SELECT 
		                                                            EF.ExpeditionID,
		                                                            MAX(FL.ArrivalTime) as ArrivalTime,
		                                                            TF.TripID
	                                                            FROM ExpeditionsFlights as EF
	                                                            INNER JOIN dbo.Flights as FL ON EF.FlightID = FL.ID
	                                                            INNER JOIN TripsFlightAssociations as TF ON FL.ID = TF.FlightID
	                                                            WHERE FL.IsActive = 1 
	                                                            AND FL.TakeOffFromIsrael = 0
	                                                            AND FL.ArrivalTime > @StartTime
	                                                            GROUP BY
		                                                            EF.ExpeditionID,
		                                                            TF.TripID
                                                            )

                                                            SELECT 
	                                                            DISTINCT GP.ID as GroupID,
	                                                            GP.NumberOfBuses,
	                                                            ISNULL(GNP.NumberOfParticipants,0) + ISNULL(GI.NumberOfINstructors,0) as NumberOfParticipants,
	                                                            GP.GroupExternalID,
	                                                            Departures.ArrivalTime,
	                                                            Arrivals.ArrivalTime as ReturningTime
                                                            FROM Groups as GP
                                                            INNER JOIN Expeditions as EX ON GP.OrderID = EX.ID
                                                            INNER JOIN DepartureFlights as Departures ON Ex.ID = Departures.ExpeditionID AND EX.TripID = Departures.TripID
                                                            INNER JOIN ReturningFlights as Arrivals ON Ex.ID = Arrivals.ExpeditionID AND EX.TripID = Arrivals.TripID
                                                            INNER JOIN Trips as TR ON EX.TripID = TR.ID
                                                            LEFT JOIN NumberOfParticipants as GNP ON GNP.GroupID = GP.ID
                                                            LEFT JOIN GroupIstructors as GI ON GI.GroupID = GP.ID
                                                            WHERE GP.FinalStatus = 2";

        public static string GetdailyDashboardGroupsData => @";WITH DepartureFlights AS (
	                                                            SELECT 
		                                                            EF.ExpeditionID,
		                                                            MIN(FL.ArrivalTime) as ArrivalTime,
		                                                            TF.TripID
	                                                            FROM ExpeditionsFlights as EF
	                                                            INNER JOIN dbo.Flights as FL ON EF.FlightID = FL.ID
	                                                            INNER JOIN TripsFlightAssociations as TF ON FL.ID = TF.FlightID
	                                                            WHERE	FL.IsActive = 1 
																		AND FL.TakeOffFromIsrael = 1
																		AND FL.ArrivalTime <= @StartTime
	                                                            GROUP BY
		                                                            EF.ExpeditionID,
		                                                            TF.TripID
                                                            ),
                                                            ReturningFlights AS (
	                                                            SELECT 
		                                                            EF.ExpeditionID,
		                                                            MAX(FL.ArrivalTime) as ArrivalTime,
		                                                            TF.TripID
	                                                            FROM ExpeditionsFlights as EF
	                                                            INNER JOIN dbo.Flights as FL ON EF.FlightID = FL.ID
	                                                            INNER JOIN TripsFlightAssociations as TF ON FL.ID = TF.FlightID
	                                                            WHERE	FL.IsActive = 1 
																		AND FL.TakeOffFromIsrael = 0
																		AND FL.ArrivalTime > @StartTime
	                                                            GROUP BY
		                                                            EF.ExpeditionID,
		                                                            FL.TakeOffFromIsrael,
		                                                            TF.TripID
                                                            )

                                                            SELECT 
	                                                            DISTINCT GP.ID as GroupID,
	                                                            GP.GroupExternalID,
	                                                            SC.ID as SchoolID,
	                                                            SC.InstituteID,
	                                                            SC.Name as SchoolName,
	                                                            SC.Region,
	                                                            AG.Name as AgencyName
                                                            FROM Groups as GP
                                                            INNER JOIN Expeditions as EX ON GP.OrderID = EX.ID
                                                            INNER JOIN DepartureFlights as Departures ON Ex.ID = Departures.ExpeditionID AND EX.TripID = Departures.TripID
                                                            INNER JOIN ReturningFlights as Arrivals ON Ex.ID = Arrivals.ExpeditionID AND EX.TripID = Arrivals.TripID
                                                            INNER JOIN Trips as TR ON EX.TripID = TR.ID
                                                            INNER JOIN SubGroupSchools as SS ON GP.ID = SS.GroupID
                                                            INNER JOIN Schools as SC On SS.SchoolID = SC.ID
                                                            INNER JOIn Agencies as AG ON GP.AgencyID = AG.ID
                                                            WHERE GP.FinalStatus = 2";
        #endregion

        #region Group Files Comments

        public static string InsertNewFileComment
            => @"INSERT INTO GroupFileComments (FileID,Comment,UserID,IsActive,CreationDate,UpdateDate)
                VALUES (@FileID,@Comment,@UserID,@IsActive,@CreationDate,@UpdateDate)";

        public static string GetAllCommentsbyFileID => @"SELECT 
	                                                        GFC.ID,
	                                                        GFC.Comment,
	                                                        GFC.CreationDate,
	                                                        GFC.UpdateDate,
	                                                        GFC.FileID,
	                                                        GF.Name,
	                                                        GF.FileType,
	                                                        GF.FileStatus,
	                                                        GF.CountyManagerApproval,
	                                                        US.Firstname,
	                                                        US.Lastname,
	                                                        US.Email
                                                        FROM GroupFileComments as GFC
                                                        INNER JOIN GroupFiles as GF ON GFC.FileID = GF.ID
                                                        INNER JOIN Users as US ON GFC.UserID = US.ID
                                                        WHERE GFC.FileID = @FileID
                                                        ORDER BY GFC.UpdateDate DESC";

        #endregion

        #region Schools

        public static string GetSchoolPreviousExpeditions => @"SELECT 
	                                                                ORD.ID as OrderID,
	                                                                ORD.TripID,
	                                                                TR.DepartureDate,
	                                                                ORD.SchoolID
                                                                FROM Expeditions as ORD
                                                                INNER JOIN Trips as TR ON ORD.TripID = TR.ID
                                                                WHERE 
	                                                                ORD.SchoolID = @SchoolID 
	                                                                AND TR.ID <> @TripID 
	                                                                AND ORD.IsActive = 1 
	                                                                AND ORD.Status = 4 
	                                                                AND TR.DepartureDate > @MinDate";

        #endregion

        #region Flights

        public static string InsertOrUpdateFlightImport => @"IF EXISTS(SELECT ID FROM Flights WHERE ID = @ID)
	                                                            BEGIN
		                                                            UPDATE Flights
		                                                            SET  StartDate = @StartDate, DepartureFieldID = @DepartureFieldID, DestinationID = @DestinationID, NumberOfPassengers = @NumberOfPassengers, AirlineName = @AirlineName,
		                                                            FlightNumber = @FlightNumber, TakeOffFromIsrael = @TakeOffFromIsrael, Comments = @Comments, UpdateDate = @UpdateDate, ArrivalTime = @ArrivalTime
		                                                            WHERE ID = @ID 
	                                                            END
                                                            ELSE
	                                                            BEGIN
		                                                            INSERT INTO Flights (StartDate,DepartureFieldID,DestinationID,NumberOfPassengers,AirlineName,FlightNumber,TakeOffFromIsrael,Comments,CreationDate,UpdateDate,IsActive,ArrivalTime)
		                                                            VALUES (@StartDate,@DepartureFieldID,@DestinationID,@NumberOfPassengers,@AirlineName,@FlightNumber,@TakeOffFromIsrael,@Comments,@CreationDate,@UpdateDate,@IsActive,@ArrivalTime)
	                                                            END";

        #endregion

        #region Trips

        public static string GetFutureTrips => @"SELECT 
	                                                TR.ID,
	                                                TR.DepartureDate,
	                                                TR.DestinationID
                                                FROM Trips as TR
                                                WHERE TR.DepartureDate >= @Date";

		public static string GetTripsWithSearch => @"SELECT
														t.ID,
														t.DepartureDate,
														t.ReturningDate,
														t.DestinationID,
														t.ReturingField,
														t.Comments
													FROM Trips as t
													WHERE t.IsActive = 1 AND
													(t.ID = @ID OR @ID IS NULL) AND
													(t.DepartureDate >= @StartDate) AND
													(t.DepartureDate <= @EndDate)";

		public static string GetTripAndOrdersBySchoolID => @"SELECT
    t.ID,
    t.DepartureDate,
    t.ReturningDate,
    t.DestinationID,
    t.ReturingField,
    t.Comments,
    e.BasesPerDay,
    e.NumberOFpassengers
FROM Expeditions as e
INNER JOIN Trips as t ON e.TripID = t.ID
WHERE t.IsActive = 1
AND t.DepartureDate > @StartDate
AND e.ID <> @OrderID
AND e.SchoolID = @SchoolID";


		public static string MoveOrderToTripID => @"UPDATE Expeditions SET TripID = @TripID WHERE ID = @OrderID";

        public static string GetAvaialbeTripsPerDay => @";WITH TotalPassengers as (
	SELECT 
		TR.ID,
		SUM(FL.NumberOfPassengers) as TotalPassengers
	FROM Trips as TR
	INNER JOIN TripsFlightAssociations as FTA ON TR.ID = FTA.TripID
	INNER JOIN Flights as FL on FTA.FlightID = fl.ID
	WHERE TR.IsActive = 1 AND TR.DepartureDate = @Date AND FL.TakeOffFromIsrael = 1
	GROUP BY TR.ID
), AvaliablePassengers AS (
	SELECT 
		TR.ID,
		SUM(EXF.NumberOfPassengers) BookedPassengers
	FROM Trips AS TR
	LEFT JOIN Expeditions AS EX ON TR.ID = EX.TripID
	LEFT JOIN ExpeditionsFlights as EXF ON EX.ID = EXF.ExpeditionID
	LEFT JOIN Flights AS FL ON EXF.FlightID = FL.ID
	WHERE TR.IsActive = 1 AND TR.DepartureDate = @Date AND EX.Status IN (2,3) AND FL.TakeOffFromIsrael = 1
	GROUP BY TR.ID
), AvalableBuses AS (
	SELECT 
		SUM(EX.BasesPerDay) as TotalBuses,
		MIN(@Date) as DapartureDate
	FROM Trips as TR
	INNER JOIN Expeditions as EX ON TR.ID = EX.TripID
	WHERE TR.IsActive = 1 AND @Date BETWEEN TR.DepartureDate AND TR.ReturningDate AND EX.Status IN (2,3)
)


SELECT 
	TR.ID,
	TR.Comments,
	TR.CreationDate,
	TR.DepartureDate,
	TR.DestinationID,
	TR.ReturningDate,
	TR.ReturingField,
	TP.TotalPassengers,
	ISNULL(AP.BookedPassengers, 0) as BookedPassengers,
	AB.TotalBuses
FROM Trips as TR
INNER JOIN TotalPassengers as TP ON TR.ID = TP.ID
LEFT JOIN AvaliablePassengers as AP ON TR.ID = AP.ID
LEFT JOIN AvalableBuses as AB ON AB.DapartureDate = TR.DepartureDate
WHERE TR.DepartureDate = @Date AND TR.IsActive = 1";


        public static string GetTripsWithAvailablilityInDateRange => @"If(OBJECT_ID('tempdb..#TripsTemp') Is Not Null)
    Begin
        Drop Table #TripsTemp
    End

    CREATE TABLE #TripsTemp (
	    ID INT,
	    DepartureDate DATETIME,
	    CreationDate DATETIME,
	    DestinationID INT,
	    ReturningDate DATETIME,
	    ReturingField INT,
		MaxBuses INT
    )

    INSERT INTO #TripsTemp (ID, DepartureDate, CreationDate, DestinationID, ReturningDate, ReturingField, MaxBuses)
    SELECT 
	    TR.ID,
	    TR.DepartureDate,
	    TR.CreationDate,
	    TR.DestinationID,
	    TR.ReturningDate,
	    TR.ReturingField,
		dbo.GetNumberOfBusesWithInRange(TR.DepartureDate, TR.ReturningDate)
    FROM Trips as TR
    WHERE TR.DepartureDate >= @StartDate AND TR.DepartureDate <= @EndDate AND TR.IsActive = 1
		

    If(OBJECT_ID('tempdb..#TotalPassenagers') Is Not Null)
    Begin
        Drop Table #TotalPassenagers
    End

    CREATE TABLE #TotalPassenagers (
	    TripID INT,
	    TotalPassengers INT
    )

    INSERT INTO #TotalPassenagers (TripID, TotalPassengers)
    SELECT 
	    TR.ID,
	    ISNULL(SUM(FL.NumberOfPassengers), 0)
    FROM #TripsTemp as TR
    INNER JOIN TripsFlightAssociations as FTA ON TR.ID = FTA.TripID
    INNER JOIN Flights as FL on FTA.FlightID = fl.ID
    WHERE FL.TakeOffFromIsrael = 1
    GROUP BY TR.ID

    If(OBJECT_ID('tempdb..#AvaliablePassengers') Is Not Null)
    Begin
        Drop Table #AvaliablePassengers
    End

    CREATE TABLE #AvaliablePassengers (
	    TripID INT,
	    BookedPassengers INT
    )

    INSERT INTO #AvaliablePassengers (TripID, BookedPassengers)
    SELECT 
	    TR.ID,
	    ISNULL(SUM(EXF.NumberOfPassengers),0)
    FROM #TripsTemp as TR
    LEFT JOIN Expeditions AS EX ON TR.ID = EX.TripID
    LEFT JOIN ExpeditionsFlights as EXF ON EX.ID = EXF.ExpeditionID
    LEFT JOIN Flights AS FL ON EXF.FlightID = FL.ID
    WHERE EX.Status IN (2,3) AND FL.TakeOffFromIsrael = 1
    GROUP BY TR.ID


    If(OBJECT_ID('tempdb..#AvailableBuesesPerDay') Is Not Null)
    Begin
        Drop Table #AvailableBuesesPerDay
    End

    CREATE TABLE #AvailableBuesesPerDay (
	    DepartureDate DATETIME,
	    NumberOfBuses INT
    )

    INSERT INTO #AvailableBuesesPerDay (DepartureDate,NumberOfBuses)
    SELECT 
	    Dates.DepartureDate,
	    ISNULL(MIN(EX.BasesPerDay), 0)
    FROM (SELECT DISTINCT DepartureDate FROM #TripsTemp) AS Dates
    INNER JOIN #TripsTemp as TR ON TR.DepartureDate = Dates.DepartureDate
    INNER JOIN Expeditions as EX ON TR.ID = EX.TripID
    WHERE Dates.DepartureDate BETWEEN TR.DepartureDate AND TR.ReturningDate AND EX.Status IN (2,3)
    GROUP BY Dates.DepartureDate

    SELECT 
	    TR.ID as TripID,
	    TR.DepartureDate,
	    TR.CreationDate,
	    TR.DestinationID,
	    TR.ReturningDate,
	    TR.ReturingField,
	    TP.TotalPassengers,
	    ISNULL(AP.BookedPassengers, 0) as BookedPassengers,
	    TR.MaxBuses as NumberOfBuses
    FROM #TripsTemp as TR
    INNER JOIN #TotalPassenagers as TP ON TR.ID = TP.TripID
    LEFT JOIN #AvaliablePassengers as AP ON TR.ID = AP.TripID";

        public static string GetTripsInDateRange => @"SELECT 
	                                                            TR.ID as TripID,
	                                                            TR.DepartureDate,
	                                                            TR.CreationDate,
	                                                            TR.DestinationID,
	                                                            TR.ReturningDate,
	                                                            TR.ReturingField,
																F.ArrivalTime
                                                            FROM Trips as TR
															LEFT OUTER JOIN (
																SELECT 
																	TF.TripID,
																	MIN(F.ArrivalTime) as  ArrivalTime
																FROM TripsFlightAssociations as TF
																INNER JOIN Flights as F ON TF.FlightID = F.ID
																GROUP BY TF.TripID
															) as F ON F.TripID = TR.ID
                                                            WHERE TR.DepartureDate >= @StartDate AND TR.DepartureDate <= @EndDate AND TR.IsActive = 1";

        public static string GetTripBusesPerDay => @"SELECT 
	                                                    TR.ID as TripID,
	                                                    TR.DepartureDate,
	                                                    TR.ReturningDate,
	                                                    SUM(ISNULL(EX.BasesPerDay,0)) as BasesPerDay
                                                    FROM Trips as TR
                                                    INNER JOIN Expeditions as EX ON TR.ID = EX.TripID
                                                    WHERE TR.DepartureDate <= @EndDate
                                                    AND TR.ReturningDate >= @StartDate
                                                    AND EX.Status IN (2,3)
                                                    GROUP BY
	                                                    TR.ID,
	                                                    TR.DepartureDate,
	                                                    TR.ReturningDate";

        public static string GetTripBookedPassengers => @"SELECT 
	                                                        TR.ID as TripID,
	                                                        ISNULL(SUM(EXF.NumberOfPassengers),0) as BookedPassengers
                                                        FROM Trips as TR
                                                        LEFT JOIN Expeditions AS EX ON TR.ID = EX.TripID
                                                        LEFT JOIN ExpeditionsFlights as EXF ON EX.ID = EXF.ExpeditionID
                                                        LEFT JOIN Flights AS FL ON EXF.FlightID = FL.ID
                                                        WHERE TR.DepartureDate >= @StartDate AND TR.DepartureDate <= @EndDate AND TR.IsActive = 1
                                                        AND EX.Status IN (2,3) AND FL.TakeOffFromIsrael = 1
                                                        GROUP BY TR.ID";

        public static string GetTotalPaggengersForTrips => @"SELECT 
	TR.ID,
	ISNULL(SUM(FL.NumberOfPassengers), 0) as TotalPassensgers
FROM Trips as TR
INNER JOIN TripsFlightAssociations as FTA ON TR.ID = FTA.TripID
INNER JOIN Flights as FL on FTA.FlightID = fl.ID
WHERE TR.DepartureDate <= @EndDate
AND TR.ReturningDate >= @StartDate
AND FL.TakeOffFromIsrael = 1
GROUP BY
	TR.ID";

        public static string InsertOrUpdadteTripImport => @"IF EXISTS(SELECT ID FROM Trips WHERE ID = @ID)
                                                                BEGIN
	                                                                UPDATE Trips
	                                                                SET  DepartureDate = @DepartureDate, DestinationID = @DestinationID, ReturningDate = @ReturningDate, ReturingField = @ReturingField, Comments = @Comments, CreationDate = @CreationDate, UpdateDate = @UpdateDate
	                                                                WHERE ID = @ID 
                                                                END
                                                                ELSE
                                                                BEGIN
	                                                                INSERT INTO Trips (DepartureDate,DestinationID,ReturningDate,ReturingField,Comments,CreationDate,UpdateDate,IsActive)
	                                                                VALUES (@DepartureDate, @DestinationID, @ReturningDate, @ReturingField, @Comments, @CreationDate, @UpdateDate, 1)
                                                                END";
        #endregion


        #region Administrative180Form

        public static string GetAdministrative180GeneralDetails => @"SELECT 
	                                                                    G.ID as GroupID,
	                                                                    G.AgencyID,
	                                                                    G.NumberOfBuses,
	                                                                    SC.ID as SchoolID,
	                                                                    SC.InstituteID,
	                                                                    SC.Name as SchoolName,
	                                                                    SC.Email as SchoolEmail,
	                                                                    SC.Fax as SchoolFax,
	                                                                    SC.Phone as SchoolPhone,
	                                                                    SC.Stream as SchoolStream,
	                                                                    SC.IsSpecialEducation,
	                                                                    SC.Manager,
	                                                                    SC.ManagerPhone,
	                                                                    SC.ManagerEmail
                                                                    FROM SubGroupSchools as SGS
                                                                    INNER JOIN Groups as G ON SGS.GroupID = G.ID
                                                                    INNER JOIN Schools as SC ON SGS.SchoolID = SC.ID
                                                                    WHERE SGS.GroupID = @ID AND SGS.SchoolID = @SchoolID";

        public static string GetAdministrative180SchoolDetails => @"SELECT 
	                                                                    TR.ID,
	                                                                    TR.DepartureDate,
	                                                                    TR.ReturningDate as ReturnDate,
	                                                                    TR.DestinationID as Destenation,
	                                                                    TR.ReturingField as ReturnedFrom
                                                                    FROM Groups as G
                                                                    INNER JOIN Expeditions as EX ON G.OrderID = EX.ID
                                                                    INNER JOIN Trips as TR ON Ex.TripID = TR.ID
                                                                    WHERE G.ID = @ID";


        public static string DeleteManagerFromGroupByGroupID => @"DELETE FROM InstructorGroups WHERE IsGroupManager = 1 AND GroupID = @GroupID";

        public static string GetGroup180FormDetails => @"SELECT GF.ID, SG.ID as GroupSchoolID, GF.FormType, GF.CreationDate, GF.UpdateDate
                                                            FROM SubGroupSchools as SG
                                                            LEFT JOIN GroupForms as GF ON SG.ID = GF.GroupSchoolID
                                                            WHERE SG.SchoolID = @SchoolID AND SG.GroupID = @GroupID ";

        public static string Create180GeneralForm => @"INSERT INTO GroupForms (GroupSchoolID, FormType, CreationDate, UpdateDate)
                                                        VALUES (@SchoolGroupAssosicationID, @FormType, @CreationDate, @UpdateDate)
                                                        SELECT CAST(SCOPE_IDENTITY() as int)";

        public static string Create180AdministrativeFormBasicData => @"INSERT INTO AdministrationForm180 (GroupFormID,FormStatus,CreationDate,UpdateDate, UserID)
                                                                VALUES (@GroupFormID, @FormStatus, @CreationDate, @UpdateDate, @UserID)
                                                                            SELECT CAST(SCOPE_IDENTITY() as int)";

        public static string GetAdministrativeFormSchoolDetails => @"SELECT 
	                                                                    SGS.ID as SchoolGroupAssosicationID,
																		SC.ID as SchoolID, 
	                                                                    SC.Name as SchoolName, 
	                                                                    SC.Email as SchoolEmail, 
	                                                                    SC.Stream as SchoolStream,
	                                                                    SC.InstituteID,
	                                                                    SC.Fax as SchoolFax,
	                                                                    SC.Phone as SchoolPhone,
	                                                                    SC.IsSpecialEducation as IsSpecialEducation,
	                                                                    SC.Manager,
	                                                                    SC.ManagerPhone,
	                                                                    SC.ManagerEmail,
                                                                        SC.IsAdministrativeInstitude,
                                                                        SC.Region as RegionID,
                                                                        SGS.NumberOfInstructors
                                                                    FROM SubGroupSchools as SGS
																	INNER JOIN Schools as SC ON SGS.SchoolID = SC.ID
                                                                    WHERE SC.ID = @SchoolID AND SGS.GroupID = @GroupID";
        public static string GetAdministrativeFormGroupDetails => @"SELECT 
	                                                                    G.ID as GroupID,
	                                                                    G.AgencyID,
	                                                                    G.NumberOfBuses
                                                                    FROM Groups as G
                                                                    WHERE g.ID = @GroupID";

        public static string GetAdiministrativeFormDetails => @"SELECT 
	                                                                AF.ID,	
	                                                                AF.GroupFormID,
	                                                                AF.ContractUrl,
	                                                                AF.SignatureUrl,
	                                                                AF.FormStatus,
	                                                                AF.CreationDate,
	                                                                AF.UpdateDate,
	                                                                AF.UserID,
                                                                    AF.ClassType,
																	AF.CostPerStudent
                                                                FROM AdministrationForm180 as AF
                                                                WHERE AF.GroupFormID = @GroupFormID";

        public static string UpdateAdministrative180FormStatus => "UPDATE AdministrationForm180 SET FormStatus = @FormStatus, UpdateDate = @UpdateDate WHERE ID = @ID";

        public static string UpdateAdministrative180FormGenericData => @"UPDATE AdministrationForm180
                                                                            SET ClassType = @ClassType, CostPerStudent = @CostPerStudent
                                                                            WHERE ID = @ID";

        public static string UpdateAdministrative180FormSchoolData => @"UPDATE Schools
                                                                        SET Email = @SchoolEmail, Fax = @SchoolFax, Phone = @SchoolPhone, Stream = @Stream, IsSpecialEducation = @IsSpecialEducation,
                                                                        Manager = @SchoolManager, ManagerPhone = @SchoolManagerPhone, ManagerEmail = @SchoolManagerEmail
                                                                        WHERE ID = @ID";

        public static string UpdateAdministrative180FormGroupData => @"UPDATE Groups SET NumberOfBuses = @NumberofBuses, AgencyID = @AgencyID
WHERE ID = @GroupID";

        public static string GetGroupManagerDataByGroupID => @"SELECT 
	                                                                IG.InstructorID,
	                                                                INST.FirstNameHE,
	                                                                INST.LastNameHE,
	                                                                INST.ImageURL,
	                                                                INST.Email,
	                                                                INST.HomePhone,
	                                                                INST.CellPhone,
	                                                                INST.ValidityEndTime
                                                                FROM InstructorGroups IG 
                                                                INNER JOIN Instructors as INST ON IG.InstructorID = INST.ID
                                                                WHERE 
																	IG.GroupID = @GroupID AND 
																	IG.RoleID = @RoleID AND 
																	IG.IsGroupManager = 1 AND 
																	INST.IsActive = 1";

        public static string UpdateGroupManagerBasicData => @"UPDATE Instructors
                                                                SET Email = @Email, HomePhone = @HomePhone, CellPhone = @CellPhone
                                                                WHERE ID = @ID";

        public static string UpdateAdministrativeForm180SignatureUrl => @"UPDATE AdministrationForm180 SET SignatureUrl = @SignatureUrl WHERE ID = @ID";
        public static string UpdateAdministrativeForm180ContactUrl => @"UPDATE AdministrationForm180 SET ContractUrl = @ContractUrl WHERE ID = @ID";

        public static string AdministrativeForm180_GetFiles => @"SELECT SignatureUrl, ContractUrl FROM AdministrationForm180 WHERE ID = @ID";

        public static string UpdateAdministrationFormStatus => @"UPDATE AdministrationForm180 SET FormStatus = @FormStatus, UpdateDate = @UpdateDate WHERE ID = @ID";
        #endregion

        #region FormComments
        public static string GetFormComments => @"SELECT 
	                                                FC.*,
	                                                US.Firstname,
	                                                US.Lastname
                                                FROM FormsComments as FC
                                                INNER JOIN Users as US ON FC.UserID = US.ID
                                                WHERE FormID = @FormID AND FormChapterType = @FormChapterType
                                                ORDER BY FC.UpdateDate DESC";

        public static string InsertFormComment => @"INSERT INTO FormsComments (FormChapterType, FormID, Comment, UserID, CreationDate, UpdateDate)
                                                    VALUES (@FormChapterType, @FormID, @Comment, @UserID, @CreationDate, @UpdateDate)
                                                    SELECT CAST(SCOPE_IDENTITY() as int)";
        #endregion

        #region Instructor Form Queries

        public static string GetGroupInstructorFormDetails => @"SELECT *
                                                            FROM GroupInstructorsForm
                                                            WHERE GroupFormID = @FormID";

        public static string InsertGroupInstructorFormDetails => @"INSERT INTO GroupInstructorsForm (GroupFormID, FormStatus ,CreationDate, UpdateDate)
                                                                    VALUES (@FormID, @FormStatus, @CreationDate, @UpdateDate)
                                                                    SELECT CAST(SCOPE_IDENTITY() as int)";

        public static string GetListofInstructorsInGroupsBySchoolID => @"SELECT 
	                                                                        INST.FirstNameHE as FirstName,
	                                                                        INST.LastNameHE as LastName,
                                                                            IG.ID as AssociationID,
	                                                                        IG.InstructorID as ID,
	                                                                        INST.InstructorID,
	                                                                        INST.CellPhone as PhoneNumber,
	                                                                        IG.IsGroupManager,
                                                                            IG.IsDeputyDirector,
	                                                                        IG.Comments,
	                                                                        INST.ValidityEndTime as ValidityEndDate,
	                                                                        INST.ImageURL,
	                                                                        IR.ID as RoleID,
	                                                                        IR.Name as RoleName
                                                                        FROM InstructorGroups as IG
                                                                        INNER JOIN Instructors as INST ON IG.InstructorID = INST.ID
                                                                        INNER JOIN InstructorRoles as IR ON INST.RoleID = IR.ID
                                                                        WHERE GroupID = @GroupID AND IG.SchoolID = @SchoolID AND IG.RoleID IN (3,2)";

        public static string AddInstructorToGroupWithSchoolID => @"INSERT INTO InstructorGroups (GroupID, InstructorID, RoleID, Comments, IsGroupManager, SchoolID, IsDeputyDirector)
                                                                    VALUES (@GroupID, @InstructorID, @RoleID, @Comments, @IsGroupManager, @SchoolID, @IsDeputyDirector)
                                                                    SELECT CAST(SCOPE_IDENTITY() as int)";

        public static string AddInstructorToGroupWithoutSchoolID => @"INSERT INTO InstructorGroups (GroupID, InstructorID, RoleID, Comments, IsGroupManager, IsDeputyDirector)
                                                                    VALUES (@GroupID, @InstructorID, @RoleID, @Comments, @IsGroupManager, @IsDeputyDirector)
                                                                    SELECT CAST(SCOPE_IDENTITY() as int)";

        public static string RemoveInstructorFromGroupByID => @"DELETE InstructorGroups WHERE ID = @ID";

        public static string UpdateGroupInstructorFormStepStatus => @"UPDATE GroupInstructorsForm SET FormStatus = @FormStatus, UpdateDate = @UpdateDate WHERE ID = @ID";
        #endregion

        #region Group Plan Form Queries

        public static string GetGroupPlanSubjects => @"SELECT * FROM GroupPlanSubjects WHERE IsActive = 1";

        public static string AddGroupPlanSubject => @"INSERT INTO GroupPlanSubjects (Name, HoursRequired, CreationDate, UpdateDate, IsActive)
VALUES (@Name, @HoursRequired, @CreationDate, @UpdateDate, 1)";

        public static string UpdateGroupPlanSubject => @"UPDATE GroupPlanSubjects SET Name = @Name, HoursRequired = @HoursRequired, UpdateDate = @UpdateDate WHERE ID = @ID";

        public static string GetGroupPlanSubjectByID => @"SELECT * FROM GroupPlanSubjects WHERE ID = @ID";

        public static string DeleteGroupPlanSubject => @"UPDATE GroupPlanSubjects SET IsActive = 0 WHERE ID = @ID";

        public static string GetGroupPlan180FormDetails => @"SELECT * FROM GroupPlanForm WHERE GroupFormID = @FormID";

        public static string CreateGroupPlan180FormDetails => @"INSERT INTO GroupPlanForm (GroupFormID,FormStatus,CreationDate,UpdateDate)
                                                                VALUES (@GroupFormID, @FormStatus, @CreationDate, @UpdateDate)
                                                                SELECT CAST(SCOPE_IDENTITY() as int)";

        public static string GetGroupPlanDetailsByFormID => @"SELECT 
	                                                            GPD.ID,
	                                                            GPD.PlanName,
	                                                            GPD.GroupPlanID,
	                                                            GPD.Instructor,
	                                                            GPD.PlanHours,
	                                                            GPD.PlanDate,
	                                                            GPD.PlanLocation,
	                                                            GPD.CreationDate,
	                                                            GPD.UpdateDate,
	                                                            GPS.ID as GroupPlanSubjectID,
	                                                            GPS.Name as GroupPlanSubjectName
                                                            FROM GroupPlanDetails as GPD
                                                            INNER JOIN GroupPlanSubjects as GPS ON GPD.GroupPlanSubjectID = GPS.ID
                                                            WHERE GPD.GroupPlanID = @FormID";

        public static string CreateGroupPlanDetails => @"INSERT INTO GroupPlanDetails (GroupPlanID, Instructor, PlanHours, PlanDate, PlanLocation, CreationDate, UpdateDate, GroupPlanSubjectID, PlanName)
                                                        VALUES (@GroupPlanID, @Instructor, @PlanHours, @PlanDate, @PlanLocation, @CreationDate, @UpdateDate, @GroupPlanSubjectID, @PlanName)";

        public static string GetGroupPlanDetailsByID => @"SELECT 
	                                                        GPD.ID,
	                                                        GPD.PlanName,
	                                                        GPD.GroupPlanID,
	                                                        GPD.Instructor,
	                                                        GPD.PlanHours,
	                                                        GPD.PlanDate,
	                                                        GPD.PlanLocation,
	                                                        GPD.CreationDate,
	                                                        GPD.UpdateDate,
	                                                        GPS.ID as GroupPlanSubjectID,
	                                                        GPS.Name as GroupPlanSubjectName
                                                        FROM GroupPlanDetails as GPD
                                                        INNER JOIN GroupPlanSubjects as GPS ON GPD.GroupPlanSubjectID = GPS.ID
                                                        WHERE GPD.ID = @ID";

        public static string UpdateGroupPlanDetailsByID => @"UPDATE GroupPlanDetails 
                                                                SET Instructor = @Instructor,
	                                                                PlanHours = @PlanHours,
	                                                                PlanDate = @PlanDate,
	                                                                PlanLocation = @PlanLocation,
	                                                                UpdateDate = @UpdateDate,
	                                                                PlanName = @PlanName,
	                                                                GroupPlanSubjectID = @GroupPlanSubjectID
                                                                WHERE ID = @ID";

        public static string DeleteGroupPlanDetailsByID => @"DELETE GroupPlanDetails WHERE ID = @ID";

        public static string UpdateGroupPlanForm => @"UPDATE GroupPlanForm SET FormStatus = @FormStatus, UpdateDate = @UpdateDate, EdicationPlanDetails = @EdicationPlanDetails WHERE ID = @ID";

        public static string UpdateGroupPlanFormStatus => @"UPDATE GroupPlanForm SET FormStatus = @FormStatus, UpdateDate = @UpdateDate WHERE ID = @ID";
        #endregion

        #region GroupForms

        public static string GetGroup180FormFullStatus => @"SELECT 
	                                                            SG.GroupID,
	                                                            SG.SchoolID,
	                                                            SC.Name as SchoolName,
	                                                            SC.InstituteID,
	                                                            SG.ID as GroupSchoolID,
	                                                            GF.ID as GroupFormID,
	                                                            GF.FormType as GroupFormType,
	                                                            GF.UpdateDate as GroupFormUpdateDate,
	                                                            GAF.ID as AdministrationFormID,
	                                                            GAF.FormStatus as AdministrationFormStatus,
	                                                            GIF.ID as InstructorFormID,
	                                                            GIF.FormStatus as InstructorFormStatus,
	                                                            GPF.ID as GroupPlanFormID,
	                                                            GPF.FormStatus as GroupPlanFormStatus
                                                            FROM SubGroupSchools as SG
                                                            INNER JOIN Schools as SC ON SG.SchoolID = SC.ID
                                                            INNER JOIN GroupForms as GF ON SG.ID = GF.GroupSchoolID
                                                            LEFT JOIN AdministrationForm180 as GAF ON GAF.GroupFormID = GF.ID
                                                            LEFT JOIN GroupInstructorsForm as GIF ON GIF.GroupFormID = GF.ID
                                                            LEFT JOIN GroupPlanForm as GPF ON GPF.GroupFormID = GF.ID
                                                            WHERE SG.GroupID = @GroupID AND SG.SchoolID = @SchoolID";

        public static string GetSchoolGroupForMove180 => @"SELECT 
	                                                            S.ID as GroupFormID,
	                                                            S.GroupID,
	                                                            S.SchoolID 
                                                            FROM SubGroupSchools as S
                                                            WHERE S.GroupID = @GroupID 
	                                                            AND S.SchoolID = @SchoolID";

        public static string CheckIfTargetGroupHas180Form => @"SELECT 
	                                                                S.GroupID,
	                                                                S.SchoolID,
	                                                                GF.GroupSchoolID
                                                                FROM SubGroupSchools as S
                                                                LEFT JOIN GroupForms as GF ON S.ID = GF.GroupSchoolID
                                                                WHERE 
	                                                                S.GroupID = @GroupID
	                                                                AND S.SchoolID = @SchoolID";

        public static string Move180ReplaceForm => @"UPDATE GroupForms
                                                    SET GroupSchoolID = @GroupFormID
                                                    WHERE GroupSchoolID = @TargetGroupSchoolID";

        public static string GetSchoolGroupIDByGroupIDAndSchoolID => @"SELECT ID FROM SubGroupSchools WHERE GroupID = @GroupID AND SchoolID = @SchoolID";

        public static string GetGroupFormIntenalIDByGroupFormID => @"SELECT ID FROM GroupForms WHERE GroupSchoolID = @GroupFormID";

        public static string Move180FromByReplacingGroupFormID => @"UPDATE GroupForms SET GroupSchoolID = @GroupSchoolID WHERE ID = @ID";

        public static string Override180Form => @"SELECT * FROM SubGroupSchools WHERE GroupID = 2111 AND SchoolID = 29
                                                    UPDATE GroupForms SET GroupSchoolID = @PreviousGroupFormID
                                                    WHERE ID = (SELECT ID FROM GroupForms WHERE GroupSchoolID = @TargetGroupFormID)";

        public static string GetGroup180Forms => @";WITH FormCommentsView as (
	                                                    SELECT 
		                                                    GF.ID as FormID,
		                                                    COUNT(AFC.ID) as AdministrationNumberOfComments,
		                                                    COUNT(IFC.ID) as InstructorsNumberOfComments,
		                                                    COUNT(PFC.ID) as PlanNumberOfComments
	                                                    FROM SubGroupSchools as SG
                                                        INNER JOIN Schools as SC ON SG.SchoolID = SC.ID
	                                                    INNER JOIN GroupForms as GF ON SG.ID = GF.GroupSchoolID
	                                                    LEFT JOIN FormsComments as AFC ON AFC.FormID = GF.ID AND AFC.FormChapterType = 1
	                                                    LEFT JOIN FormsComments as IFC ON IFC.FormID = GF.ID AND IFC.FormChapterType = 2
	                                                    LEFT JOIN FormsComments as PFC ON PFC.FormID = GF.ID AND PFC.FormChapterType = 3
	                                                    WHERE SG.GroupID = @GroupID 
	                                                    AND SC.IsAdministrativeInstitude != 1
	                                                    AND (SC.ID = @SchoolID OR @SchoolID IS NULL)
	                                                    AND (SC.Region = @Region OR @Region IS NULL)
	                                                    GROUP BY 
		                                                    GF.ID
                                                    )
                                                    SELECT
                                                        SG.IsPrimary as SchoolIsPrimary,
	                                                    SG.GroupID,
	                                                    SG.SchoolID,
	                                                    SC.Name as SchoolName,
	                                                    SC.InstituteID,
	                                                    SG.ID as GroupSchoolID,
	                                                    GF.ID as GroupFormID,
	                                                    GF.FormType as GroupFormType,
	                                                    GF.UpdateDate as GroupFormUpdateDate,
	                                                    GAF.ID as AdministrationFormID,
	                                                    GAF.FormStatus as AdministrationFormStatus,
	                                                    GAF.UpdateDate as AdministrationFormUpdateDate,
	                                                    FC.AdministrationNumberOfComments,
	                                                    GIF.ID as InstructorFormID,
	                                                    GIF.FormStatus as InstructorFormStatus,
	                                                    GIF.UpdateDate as InstructorFormUpdateDate,
	                                                    FC.InstructorsNumberOfComments,
	                                                    GPF.ID as GroupPlanFormID,
	                                                    GPF.FormStatus as GroupPlanFormStatus,
	                                                    GPF.UpdateDate as GroupPlanFormUpdateDate,
	                                                    FC.PlanNumberOfComments
                                                    FROM SubGroupSchools as SG
                                                    INNER JOIN Schools as SC ON SG.SchoolID = SC.ID
                                                    LEFT JOIN GroupForms as GF ON SG.ID = GF.GroupSchoolID
                                                    LEFT JOIN AdministrationForm180 as GAF ON GAF.GroupFormID = GF.ID
                                                    LEFT JOIN GroupInstructorsForm as GIF ON GIF.GroupFormID = GF.ID
                                                    LEFT JOIN GroupPlanForm as GPF ON GPF.GroupFormID = GF.ID
                                                    LEFT JOIN FormCommentsView as FC ON GF.ID = FC.FormID
                                                    WHERE 
                                                    SG.GroupID = @GroupID 
                                                    AND SC.IsAdministrativeInstitude != 1
                                                    AND (SC.ID = @SchoolID OR @SchoolID IS NULL)
                                                    AND (SC.Region = @Region OR @Region IS NULL)";

        public static string GetSchoolsGroupsBySchoolandGroup => @"SELECT 
	G.ID as GroupID,
	G.GroupExternalID,
	T.DepartureDate,
	T.ReturningDate,
	T.DestinationID,
	T.ReturingField,
	SGS.SchoolID
FROM SubGroupSchools as SGS
INNER JOIN Groups as G ON SGS.GroupID = G.ID
INNER JOIN Expeditions as E ON G.OrderID = E.ID
INNER JOIN Trips as T ON E.TripID = T.ID
WHERE 
	SGS.SchoolID = @SchoolID
	AND G.ID <> @GroupID
	AND T.DepartureDate >= GETDATE()";
        #endregion

        #region Reports

        public static string GetGroupManagersReport => @";WITH TotalParticipants AS (
															SELECT 
																GP.GroupID,
																COUNT(GP.ParticipantID) as TotalParticipants
															FROM GroupParticipants as GP
															GROUP BY 
																GP.GroupID
														),
														TotalInstructors AS (
															SELECT 
																IG.GroupID,
																COUNT(IG.InstructorID) as TotalInstructors
															FROM InstructorGroups as IG
															INNER JOIN Instructors as IST ON IG.InstructorID = IST.ID
															WHERE IST.IsActive = 1
															GROUP BY
																IG.GroupID
														),
														GroupManagers AS (
															SELECT
																IG.GroupID,
																IG.SchoolID,
																IG.RoleID,
																INST.FirstNameHE,
																INST.LastNameHE,
																INST.Email as GroupManagerEmail,
																INST.CellPhone as GroupManagerCell,
																IG.IsGroupManager,
																IG.IsDeputyDirector
															FROM InstructorGroups as IG
															INNER JOIN Instructors as INST ON IG.InstructorID = INST.ID
															WHERE (IG.IsDeputyDirector = 1 OR IG.IsGroupManager = 1) AND INST.IsActive = 1
	
														)

														SELECT 
															G.ID as GroupID,
															G.GroupExternalID,
															TR.DepartureDate,
															TR.DestinationID,
															G.NumberOfBuses,
															ISNULL(TP.TotalParticipants,0) + ISNULL(TI.TotalInstructors,0) as TotalParticipants,
															SC.InstituteID,
															SC.Name as SchoolName,
															GM.FirstNameHE as GroupManagerFirstName,
															GM.LastNameHE as GroupManagerLastName,
															GM.GroupManagerCell,
															GM.GroupManagerEmail,
															GDM.FirstNameHE as DeputyManagerFirstName,
															GDM.LastNameHE as DeputyManagerLastName,
															GDM.GroupManagerCell as DeputyManagerCell,
															GDM.GroupManagerEmail as DeputyManagerEmail
														FROM SubGroupSchools as SSG
														INNER JOIN Schools as SC ON SSG.SchoolID = SC.ID
														INNER JOIN Groups as G ON SSG.GroupID = G.ID
														INNER JOIN Expeditions as EX ON G.OrderID = EX.ID
														INNER JOIN Trips as TR ON EX.TripID = TR.ID
														LEFT JOIN TotalParticipants as TP ON TP.GroupID = SSG.GroupID
														LEFT JOIN TotalInstructors as TI ON TI.GroupID = SSG.GroupID
														LEFT JOIN GroupManagers as GM ON SSG.GroupID = GM.GroupID AND GM.IsGroupManager = 1
														LEFT JOIN GroupManagers as GDM ON SSG.GroupID = GDM.GroupID AND GDM.IsDeputyDirector = 1
														WHERE 
															SSG.IsPrimary = 1
															AND G.IsActive = 1
															AND TR.DepartureDate >= @StartDate
															AND TR.DepartureDate <= @EndDate
															AND (G.GroupExternalID = @GroupExternalID OR @GroupExternalID IS NULL)
															AND (TR.DestinationID = @DepartureFieldID OR @DepartureFieldID IS NULL)";

        public static string GetTripAvilabilitySeats => @";WITH #Trips AS (
	                                                            SELECT
		                                                            TR.ID as TripID,
		                                                            TR.DepartureDate,
		                                                            TR.DestinationID,
		                                                            TR.ReturningDate,
		                                                            TR.ReturingField
	                                                            FROM Trips as TR
	                                                            WHERE TR.DepartureDate >= @StartDate AND TR.DepartureDate <= @EndDate AND TR.IsActive = 1
                                                            ),
                                                            #BusesPerDepartureDate AS (
	                                                            SELECT 
		                                                            TR.DepartureDate,
		                                                            SUM(ISNULL(EX.BasesPerDay,0)) as BasesPerDay
	                                                            FROM #Trips as TR
	                                                            INNER JOIN Expeditions as EX ON TR.TripID = EX.TripID
	                                                            WHERE TR.DepartureDate <= @EndDate
	                                                            AND TR.ReturningDate >= @StartDate
	                                                            AND EX.Status IN (2,3)
	                                                            GROUP BY
		                                                            TR.DepartureDate
                                                            ),
                                                            #BookedPassengers AS (
	                                                            SELECT 
		                                                            TR.TripID,
		                                                            ISNULL(SUM(EXF.NumberOfPassengers),0) as BookedPassengers
	                                                            FROM #Trips as TR
	                                                            INNER JOIN Expeditions AS EX ON TR.TripID = EX.TripID
	                                                            INNER JOIN ExpeditionsFlights as EXF ON EX.ID = EXF.ExpeditionID
	                                                            INNER JOIN Flights AS FL ON EXF.FlightID = FL.ID
	                                                            AND EX.Status = 2 AND FL.TakeOffFromIsrael = 1 AND EX.IsActive = 1
	                                                            GROUP BY TR.TripID
                                                            ),
                                                            #TotalPasssengers AS (
	                                                            SELECT 
		                                                            TR.TripID,
		                                                            SUM(ISNULL(FL.NumberOfPassengers, 0)) as TotalPassengers
	                                                            FROM #Trips as TR
	                                                            INNER JOIN TripsFlightAssociations as TF ON TR.TripID = TF.TripID
	                                                            INNER JOIN Flights as FL ON TF.FlightID = FL.ID
	                                                            WHERE FL.IsActive = 1 AND FL.TakeOffFromIsrael = 1
	                                                            GROUP BY
		                                                            TR.TripID
                                                            )

                                                            SELECT
	                                                            TR.TripID,
	                                                            TR.DepartureDate,
	                                                            TR.DestinationID,
	                                                            ISNULL(BPD.BasesPerDay, 0) as BasesPerDay,
	                                                            TP.TotalPassengers,
	                                                            ISNULL(BP.BookedPassengers, 0) as BookedPassengers
                                                            FROM #Trips as TR
                                                            LEFT JOIN #BusesPerDepartureDate as BPD ON TR.DepartureDate = BPD.DepartureDate
                                                            LEFT JOIN #TotalPasssengers as TP ON TR.TripID = TP.TripID
                                                            LEFT JOIN #BookedPassengers as BP ON TR.TripID = BP.TripID";
        #endregion


        #region Hotels

        public static string GetAllCities => @"SELECT * FROM CitiesView";

        public static string GetAllSites => @"SELECT 
	                                            S.ID,
	                                            S.ExternalID,
	                                            S.SiteType,
	                                            S.Name,
	                                            S.LocalName,
	                                            S.Address,
	                                            S.Limitations,
	                                            S.CreationDate,
	                                            S.UpdateDate,
	                                            S.IsActive,
	                                            C.CityID,
	                                            C.CityName,
	                                            C.CityExternalID,
	                                            C.RegionID,
	                                            C.RegionName,
	                                            C.DistrictID,
	                                            C.DistrictName
                                            FROM Sites as S
                                            INNER JOIN CitiesView as C ON S.CityID = C.CityID
                                            WHERE S.IsActive = 1
                                            AND (S.Name like CONCAT('%', @SiteName, '%') OR @SiteName IS NULL)
                                            AND (S.SiteType IN @SelectedSiteTypes OR @SelectedSiteTypes IS NULL)
                                            AND (S.CityID IN @SelectedCities OR @SelectedCities IS NULL)";

        public static string AddSite => @"INSERT INTO Sites (ExternalID, SiteType, Name, LocalName, CityID, Address, Limitations, CreationDate, UpdateDate, IsActive)
                                           VALUES (@ExternalID, @SiteType, @Name, @LocalName, @CityID, @Address, @Limitations, @CreationDate, @UpdateDate, @IsActive)";

        public static string UpdateSite => @"UPDATE Sites SET Name = @Name, LocalName = @LocalName, CityID = @CityID, Address = @Address, Limitations = @Limitations, UpdateDate = @UpdateDate, ExternalID = @ExternalID, SiteType = @SiteType
	                                            WHERE ID = @ID";


        public static string GetSiteById => @"SELECT * FROM Sites WHERE ID = @ID";

        public static string GetSiteIDByTypeAndName => @"SELECT ID, CityID FROM Sites WHERE SiteType = @SiteType AND Name = @Name";

        public static string DeleteSite => @"UPDATE Sites SET IsActive = 0 WHERE ID = @ID";
        #endregion

        #region GroupForm60

        public static string GetGroupForm60 => @";WITH UsersTemp AS (
	SELECT ID, FirstName + ' ' + LastName as UserName
	FROM Users
	WHERE UserType != 4
)
SELECT  
	GF.*,
	G.GroupExternalID,
	GF.FormApprovalUserID,
	FAU.UserName as 'FormApprovalUserName',
	GF.AgencyApprovalUserID,
	AAU.UserName as 'AgencyApprovalUserName',
	GF.PlanApprovalUserID,
	PAU.UserName as 'PlanApprovalUserName',
    GF.PlanApprovalStatus,
    GF.AgencyApprovalStatus
FROM GroupFormsSixty GF
INNER JOIN Groups as G ON GF.GroupID = G.ID
LEFT JOIN UsersTemp as FAU ON GF.FormApprovalUserID = FAU.ID
LEFT JOIN UsersTemp as AAU ON GF.AgencyApprovalUserID = AAU.ID
LEFT JOIN UsersTemp as PAU ON GF.PlanApprovalUserID = PAU.ID
WHERE GF.GroupID = @GroupID";

        public static string CreateGroupForm60 => @"INSERT INTO GroupFormsSixty (CreationDate, UpdateDate, GroupID, FormStatus, AgencyApprovalStatus, PlanApprovalStatus)
VALUES (@CreationDate, @UpdateDate, @GroupID, @FormStatus, @AgencyApprovalStatus, @PlanApprovalStatus)
SELECT CAST(SCOPE_IDENTITY() as int)";

        public static string GetForm60Schools => @"SELECT 
	                                                SC.Name,
	                                                SC.ID as SchoolID,
	                                                SC.InstituteID,
	                                                SG.ID as SchoolGroupID,
	                                                SG.GroupID,
	                                                SG.IsPrimary,
	                                                SC.IsAdministrativeInstitude,
	                                                SC.Stream,
	                                                SC.IsSpecialEducation
                                                FROM SubGroupSchools as SG
                                                INNER JOIN Schools as SC ON SG.SchoolID = SC.ID
                                                WHERE GroupID = @GroupID";

        public static string GetGroupManagerBasicInfoByID => @"SELECT 
	                                                            ID,
	                                                            CellPhone,
	                                                            HomePhone,
	                                                            Email
                                                            FROM Instructors
                                                            WHERE ID = @InstructorID";

        public static string GetGroupActualFlights => @"SELECT *
FROM GroupActualFlights
WHERE GroupID = @GroupID";

        public static string GetFlightByGroupAndDirection => @"SELECT *
                                                                FROM GroupActualFlights
                                                                WHERE GroupID = @GroupID
                                                                AND Direction = @Direction
                                                                ORDER BY Arrival DESC";

        public static string GetGroupTripDetails => @"SELECT 
	TR.DepartureDate,
	TR.ReturningDate,
    TR.DestinationID,
	TR.ReturingField
FROM Groups as G
INNER JOIN Expeditions as O ON G.OrderID = O.ID
INNER JOIN Trips as TR ON O.TripID = TR.ID
WHERE G.ID = @GroupID";

        public static string AddNewActualFlight => @"INSERT INTO GroupActualFlights (Direction, FlightNumber, Airline, Departure, Arrival, NumberOfPassengers, GroupID, CreationDate, UpdateDate, DestinationLocationID, ArrivalLocationID)
                                                    VALUES (@Direction, @FlightNumber, @Airline, @Departure, @Arrival, @NumberOfPassengers, @GroupID, @CreationDate, @UpdateDate, @DestinationLocationID, @ArrivalLocationID)
SELECT CAST(SCOPE_IDENTITY() as int)";

        public static string GetActualFlightByID => @"SELECT * FROM GroupActualFlights WHERE ID = @FlightID";

        public static string UpdateActualFlight => @"UPDATE GroupActualFlights SET
	                                                    Direction = @Direction,
	                                                    FlightNumber = @FlightNumber,
	                                                    Airline = @Airline,
	                                                    Departure = @Departure,
	                                                    Arrival = @Arrival,
	                                                    NumberOfPassengers = @NumberOfPassengers,
	                                                    UpdateDate = @UpdateDate,
                                                        DestinationLocationID = @DestinationLocationID,
                                                        ArrivalLocationID = @ArrivalLocationID
                                                    WHERE ID = @FlightID";

        public static string GetForm60GroupGeneralDetails => @"SELECT 
	                                                            G.ID as GroupID,
	                                                            G.NumberOfParticipants,
	                                                            G.GroupExternalID,
	                                                            G.NumberOfBuses,
	                                                            G.ColorID,
                                                                G.ContactName,
                                                                G.ContactPhone
                                                            FROM Groups as G
                                                            WHERE G.ID = @GroupID";

        public static string GetForm60GroupTripDetails => @"SELECT 
	                                                            TR.ID,
	                                                            TR.DepartureDate,
	                                                            TR.DestinationID,
	                                                            TR.ReturningDate,
	                                                            TR.ReturingField
                                                            FROM Groups as G
                                                            INNER JOIN Expeditions as ORD ON ORD.ID = G.OrderID
                                                            INNER JOIN Trips as TR ON TR.ID = ORD.TripID
                                                            WHERE G.ID = @GroupID";

        public static string DeleteActualFlight => @"DELETE FROM GroupActualFlights WHERE ID = @ID";

        public static string GetNumberOfPassengersByDirection => @"SELECT 
	                                                                    Direction,
	                                                                    SUM(NumberOfPassengers) as NumberOfPassengers
                                                                    FROM GroupActualFlights
                                                                    WHERE GroupID = @GroupID
                                                                    GROUP BY Direction";

        public static string GetActivityByDate => @"SELECT 
	                                                    GP.ID,
	                                                    GP.ActivityType,
	                                                    GP.Name,
	                                                    GP.StartTime,
	                                                    GP.EndTime,
	                                                    GP.Comments,
	                                                    GP.GroupID,
	                                                    GP.CreationDate,
	                                                    GP.UpdateDate,
														GP.SchoolName,
														GP.SchoolAddress,
														GP.ContactName,
														GP.ContactEmail,
														GP.ContactCell,
														GP.ContactPhone,
	                                                    S.ID as SiteID,
	                                                    S.ExternalID,
	                                                    S.SiteType,
	                                                    S.Name as SiteName,
	                                                    s.LocalName,
	                                                    S.LocalRegion,
	                                                    S.Limitations,
	                                                    S.CreationDate,
	                                                    S.UpdateDate,
	                                                    C.CityID,
	                                                    C.CityName,
	                                                    C.CityExternalID,
	                                                    C.RegionID,
	                                                    C.RegionName,
	                                                    C.DistrictID,
	                                                    C.DistrictName
                                                    FROM GroupTripPlan as GP
                                                    INNER JOIN CitiesView as C ON GP.CityID = C.CityID 
                                                    LEFT JOIN Sites as S ON GP.SiteID = S.ID
                                                    WHERE 
	                                                    (GP.StartTime BETWEEN @StartDate 
														AND @EndDate) 
	                                                    AND (GP.ActivityType = @ActivityType OR @ActivityType IS NULL)
														AND Gp.GroupID = @GroupID";

        public static string GetActivityByID => @"	SELECT 
		                                                GP.ID,
		                                                GP.ActivityType,
		                                                GP.Name,
		                                                GP.StartTime,
		                                                GP.EndTime,
		                                                GP.Comments,
		                                                GP.GroupID,
		                                                GP.CreationDate,
		                                                GP.UpdateDate,
		                                                C.CityID,
		                                                C.CityName,
		                                                C.CityExternalID,
		                                                C.RegionID,
		                                                C.RegionName,
		                                                C.DistrictID,
		                                                C.DistrictName,
		                                                S.ID as SiteID,
		                                                S.ExternalID,
		                                                S.SiteType,
		                                                S.Name as SiteName,
		                                                s.LocalName,
		                                                S.LocalRegion,
		                                                S.Limitations,
		                                                S.CreationDate,
		                                                S.UpdateDate
	                                                FROM GroupTripPlan as GP
	                                                INNER JOIN CitiesView as C ON GP.CityID = C.CityID 
	                                                LEFT JOIN Sites as S ON GP.SiteID = S.ID
	                                                WHERE 
		                                                GP.ID = @ID";

        public static string GetLastArrivalTimeOnDayOfArrivalByTripID => @"SELECT 
	MAX(FL.ArrivalTime) as ArrivalTime 
FROM TripsFlightAssociations as TF
INNER JOIN Flights as FL ON TF.FlightID = FL.ID
WHERE 
	FL.TakeOffFromIsrael = 1 AND
	FL.IsActive = 1 AND
	TF.TripID = @TripID";

        public static string GetLastArrivalTimeOnDayOfArrivalByGroupIDActual => @"SELECT 
	MAX(Arrival) as ArrivalTime
FROM GroupActualFlights
WHERE GroupID = @GroupID
AND Direction = 1";

        public static string GetLastArrivalTimeOnDayOfDepartureByGroupIDActual => @"SELECT 
	MIN(Departure) as DepartureTime
FROM GroupActualFlights
WHERE GroupID = @GroupID
AND Direction = 2";

        public static string GetLastArrivalTimeOnDayOfDepartureByTripID => @"SELECT 
	MIN(FL.StartDate) as DepartureTime 
FROM TripsFlightAssociations as TF
INNER JOIN Flights as FL ON TF.FlightID = FL.ID
WHERE 
	FL.TakeOffFromIsrael = 0 AND
	FL.IsActive = 1 AND
	TF.TripID = @TripID";

        public static string InsertBasicTripActivity => @"INSERT INTO GroupTripPlan (ActivityType, Name, SiteID, CityID, StartTime, EndTime, Comments, GroupID, CreationDate, UpdateDate)
VALUES (@ActivityType, @Name, @SiteID, @CityID, @StartTime, @EndTime, @Comments, @GroupID, @CreationDate, @UpdateDate)";

        public static string UpdateBasicTripActivity => @"UPDATE GroupTripPlan
                                                            SET StartTime = @StartTime,
	                                                            EndTime = @EndTime,
	                                                            Comments = @Comments
                                                            WHERE ID = @ID";

        public static string InsertExtendedActivity => @"INSERT INTO GroupTripPlan (ActivityType, Name, SiteID, CityID, StartTime, EndTime, Comments, GroupID, CreationDate, UpdateDate, ContactName, ContactEmail, ContactPhone, ContactCell, SchoolName, SchoolAddress)
VALUES (@ActivityType, @Name, @SiteID, @CityID, @StartTime, @EndTime, @Comments, @GroupID, @CreationDate, @UpdateDate, @ContactName, @ContactEmail, @ContactPhone, @ContactCell, @SchoolName, @SchoolAddress)";

        public static string GetSitesByCityandSiteType => @"SELECT 
	                                                        S.ID,
	                                                        S.ExternalID,
	                                                        S.SiteType,
	                                                        S.Name,
	                                                        s.LocalName,
	                                                        S.LocalRegion,
	                                                        S.Limitations,
	                                                        S.CreationDate,
	                                                        S.UpdateDate
                                                        FROM Sites as S
                                                        WHERE 
	                                                        S.IsActive = 1
	                                                        AND s.CityID = @CityID
															AND s.SiteType = @SiteType";

        public static string GetSitesByRegionOrCity => @"SELECT 
	                                                        S.ID,
	                                                        S.ExternalID,
	                                                        S.SiteType,
	                                                        S.Name,
	                                                        s.LocalName,
	                                                        S.LocalRegion,
	                                                        S.Limitations,
	                                                        S.CreationDate,
	                                                        S.UpdateDate,
	                                                        C.CityID,
	                                                        C.CityName,
	                                                        C.CityExternalID,
	                                                        C.RegionID,
	                                                        C.RegionName,
	                                                        C.DistrictID,
	                                                        C.DistrictName 
                                                        FROM Sites as S
                                                        INNER JOIN CitiesView as C ON S.CityID = C.CityID
                                                        WHERE 
	                                                        S.IsActive = 1
	                                                        AND C.RegionID = @RegionID OR @RegionID IS NULL
	                                                        AND C.CityID = @CityID OR @CityID IS NULL";

        public static string GetCitiesByRegionOrCity => @"SELECT 
	C.CityID,
	C.CityName,
	C.RegionName,
	C.DistrictName
FROM CitiesView as C
WHERE C.RegionID = @RegionID OR @RegionID IS NULL
AND C.CityID = @CityID OR @CityID IS NULL";

        public static string GetSitesByTypeAndRegion => @"SELECT 
	S.ID,
	S.ExternalID,
	S.SiteType,
	S.Name,
	s.LocalName,
	S.LocalRegion,
	S.Limitations,
	S.CreationDate,
	S.UpdateDate,
	C.CityID,
	C.CityName,
	C.CityExternalID,
	C.RegionID,
	C.RegionName,
	C.DistrictID,
	C.DistrictName 
FROM Sites as S
INNER JOIN CitiesView as C ON S.CityID = C.CityID
WHERE 
	S.IsActive = 1
	AND C.RegionID = @RegionID
	AND S.SiteType = @SiteType";

        public static string DeleteTripDay => @"DELETE GroupTripPlan WHERE GroupID = @GroupID AND StartTime > @StartTime AND StartTime < @EndTime";

        public static string DeleteTripActivity => @"DELETE GroupTripPlan WHERE ID = @ID";

        public static string GetPreviusSiteID => @"SELECT TOP 1 SiteID FROM GroupTripPlan 
                                                    WHERE StartTime < @StartTime AND GroupID = @GroupID
                                                    ORDER BY StartTime DESC";

        public static string GetPreviusActivityIDByActivityType => @"SELECT TOP 1 GP.ID FROM GroupTripPlan as GP
                                                    INNER JOIN Sites as ST ON  GP.SiteID = ST.ID
                                                    WHERE GP.StartTime < @StartTime AND GroupID = @GroupID
													AND GP.ActivityType = @ActivityType
													AND ST.SiteType = @SiteType
                                                    ORDER BY StartTime DESC";

        public static string GetCityIDBySiteID => @"SELECT CityID FROM Sites WHERE ID = @ID";

        public static string GetCityByID => @"SELECT * FROM CitiesView WHERE CityID = @CityID";

        public static string GetGroupParticipantsForm60 => @"SELECT 	
	                                                            ID,
	                                                            GroupID,
	                                                            ParticipantID,
	                                                            FirstNameHe,
	                                                            LastNameHe,
	                                                            FirstNameEn,
	                                                            LastNameEn,
	                                                            PassportNumber,
	                                                            PassportExperationDate,
	                                                            Birthday,
	                                                            SEX,
	                                                            ParticipantType,
	                                                            ContactNumber,
	                                                            Speciality,
	                                                            CreationDate,
	                                                            UpdateDate,
                                                                MedicalType
                                                            FROM GroupParticipants
                                                            WHERE 
	                                                            GroupID = @GroupID
	                                                            AND ParticipantType IN @ParticipantType";

        public static string AddParticipantBasic => @"INSERT INTO GroupParticipants (GroupID, ParticipantID, FirstNameHe, LastNameHe, FirstNameEn, LastNameEn, PassportNumber, PassportExperationDate, Birthday, SEX, Status, CreationDate, UpdateDate, UserID, ParticipantType, ContactNumber, Speciality, MedicalType)
	                                                    VALUES (@GroupID, @ParticipantID, @FirstNameHe, @LastNameHe, @FirstNameEn, @LastNameEn, @PassportNumber, @PassportExperationDate, @Birthday, @SEX, @Status, @CreationDate, @UpdateDate, @UserID, @ParticipantType, @ContactNumber, @Speciality, @MedicalType)";

        public static string UpdatePacrticipantBasic => @"UPDATE GroupParticipants
	                                                            SET ParticipantID = @ParticipantID,
		                                                            FirstNameHe = @FirstNameHe,
		                                                            LastNameHe = @LastNameHe,
		                                                            FirstNameEn = @FirstNameEn,
		                                                            LastNameEn = @LastNameEn,
		                                                            PassportNumber = @PassportNumber,
		                                                            PassportExperationDate = @PassportExperationDate,
		                                                            Birthday = @Birthday,
		                                                            SEX = @SEX,
		                                                            UpdateDate = @UpdateDate,
		                                                            UserID = @UserID,
		                                                            ContactNumber = @ContactNumber,
		                                                            Speciality = @Speciality,
                                                                    ParticipantType = @ParticipantType,
                                                                    MedicalType = @MedicalType
	                                                            WHERE ID = @ID";

        public static string GetSingleParticipantBasic => @"SELECT 
		                                                        ID,
		                                                        GroupID,
		                                                        ParticipantID,
		                                                        FirstNameHe,
		                                                        LastNameHe,
		                                                        FirstNameEn,
		                                                        LastNameEn,
		                                                        PassportNumber,
		                                                        PassportExperationDate,
		                                                        Birthday,
		                                                        SEX,
		                                                        Status,
		                                                        ParticipantType,
		                                                        ContactNumber,
		                                                        Speciality,
		                                                        CreationDate,
		                                                        UpdateDate,
		                                                        UserID,
                                                                MedicalType
	                                                        FROM GroupParticipants
	                                                        WHERE ID = @ID";

        public static string GetInstructorForm60 => @"SELECT 
    IG.ID,
	IG.GroupID,
	IG.ContactNumber,
	IG.IsGroupManager,
	IG.InstructorID,
	INS.FirstNameHE,
	INS.LastNameHE,
	INS.FirstNameEN,
	INS.LastNameEN,
	INS.PassportID,
	INS.ValidityEndTime,
    INS.Email
FROM InstructorGroups as IG
INNER JOIN Instructors as INS ON IG.InstructorID = INS.ID
WHERE 
	IG.GroupID = @GroupID
	AND INS.RoleID = @RoleID";

        public static string AddInstructorForm60 => @"INSERT INTO InstructorGroups (GroupID, InstructorID, RoleID, Comments, IsGroupManager, ContactNumber)
	VALUES (@GroupID, @InstructorID, @RoleID, @Comments, @IsGroupManager, @ContactNumber)";

        public static string AddStafToGroup => @"INSERT INTO InstructorGroups (GroupID, InstructorID, RoleID, Comments, IsGroupManager, IsDeputyDirector,SchoolID)
	VALUES (@GroupID, @InstructorID, @RoleID, @Comments, @IsGroupManager, @IsDeputyDirector, @SchoolID)";

        public static string UpdateStafToGroup => @"UPDATE InstructorGroups 
	SET RoleID = @RoleID,
		Comments = @Comments,
		IsGroupManager = @IsGroupManager,
		IsDeputyDirector = @IsDeputyDirector,
		SchoolID = @SchoolID
	WHERE ID = @ID";

        public static string GetInstructorGroup => @"SELECT *
                                                    FROM InstructorGroups
                                                    WHERE ID = @ID";

        public static string UpdateInstructorEmail => @"UPDATE Instructors SET Email = @Email WHERE ID = @InstructorID";

        public static string GetContactInCasOfEmergency => @"SELECT ID as GroupID, ContactName, ContactPhone FROM Groups WHERE ID = @GroupID";

        public static string GetManagerStetementFile = @"SELECT * FROM GroupFiles WHERE FileType = @FileType AND GroupID = @GroupID";

        public static string GetGroupParticipantReportBasicParticipant => @"SELECT 
		COUNT(ID) NumberOfParticipants,
		GroupID,
		ParticipantType
	FROM GroupParticipants
	WHERE GroupID = @GroupID
	GROUP BY
		GroupID,
		ParticipantType";

        public static string GetGroupParticipantReportInstructors => @"SELECT 
		IG.GroupID,
		COUNT(IG.InstructorID) as NumberOfParticipants,
		IR.Name as ParticipantType,
		IG.RoleID
	FROM InstructorGroups as IG
	INNER JOIN InstructorRoles as IR ON IG.RoleID = IR.ID
	INNER JOIN Instructors as INST ON IG.InstructorID = INST.ID
	WHERE IG.GroupID = @GroupID AND INST.IsActive = 1
	GROUP BY
		IG.GroupID,
		IR.Name,
		IG.RoleID";

        public static string UpdateForm60FinalStatus => @"UPDATE GroupFormsSixty
                                                            SET FormStatus = @Status, FormApprovalUserID = @UserID
                                                            WHERE GroupID = @GroupID";

		public static string UpdateForm60UpdateDate => @"UPDATE GroupFormsSixty
                                                            SET UpdateDate = @Date
                                                            WHERE GroupID = @GroupID";

		public static string UpdateForm60FinalStatusOnly => @"UPDATE GroupFormsSixty
                                                            SET FormStatus = @Status
                                                            WHERE GroupID = @GroupID";

        public static string UpdateForm60AgencyApprovalStatus => @"UPDATE GroupFormsSixty
                                                                    SET AgencyApprovalStatus = @Status, AgencyApprovalUserID = @UserID
                                                                    WHERE GroupID = @GroupID";

        public static string UpdateForm60PlanApprovalStatus => @"UPDATE GroupFormsSixty
                                                                    SET PlanApprovalStatus = @Status, PlanApprovalUserID = @UserID
                                                                    WHERE GroupID = @GroupID";

        public static string SearchForGroupParticipant => @"SELECT ID 
                                                                FROM GroupParticipants
                                                                WHERE GroupID = @GroupID
                                                                AND ( 
                                                                (ParticipantID = @ParticipantID)
                                                                OR (PassportNumber = @PassportNumber))";

        public static string GetForm60Statuses => @"SELECT 
	                                                    AgencyApprovalStatus,
	                                                    PlanApprovalStatus
                                                    FROM GroupFormsSixty
                                                    WHERE GroupID = @GroupID";

        #endregion

        #region Cities

        public static string GetCitiesWithFilters => @"SELECT * 
                                                        FROM CitiesView as S
                                                        WHERE 
                                                        (S.CityName like CONCAT('%', @CityName, '%') OR @CityName IS NULL)
                                                        AND (S.RegionID IN @SelectedRegions OR @SelectedRegions IS NULL)
                                                        AND (S.DistrictID IN @SelectedDistricts OR @SelectedDistricts IS NULL)";

        public static string GetRegions => @"SELECT * FROM Regions WHERE IsActive = 1";

        public static string GetDistricts => @"SELECT * FROM Districts WHERE IsActive = 1";

        public static string AddCity => @"INSERT INTO Cities (Name, ExternalID , RegionID, DistrictID, CreationDate, UpdateDate, IsActive)
VALUES (@Name,@ExternalID, @RegionID, @DistrictID, @CreationDate, @UpdateDate, 1)";

        public static string UpdateCity => @"UPDATE Cities SET
	                                            Name = @Name,
	                                            ExternalID = @ExternalID,
	                                            RegionID = @RegionID,
	                                            DistrictID = @DistrictID,
	                                            UpdateDate = @UpdateDate
                                            WHERE ID = @CityID";

        #endregion

        public static string GetSchoolGlobalRemarkReport => @"SELECT 
	                                                            R.ID,
	                                                            R.EntityID as SchoolID,
	                                                            R.Notes,
	                                                            R.UpdateDate,
	                                                            SC.InstituteID,
	                                                            SC.Name as SchoolName,
	                                                            US.Firstname,
	                                                            US.Lastname
                                                            FROM EntityRemarks as R
                                                            INNER JOIN Schools as SC ON R.EntityID = SC.ID
                                                            INNER JOIN Users as US ON R.UserID = US.ID
                                                            WHERE R.EntityType = 1 AND SC.IsActive = 1";

        public static string GroupsByRegionReport => @";WITH CountParticipants AS (
	SELECT 
		GP.GroupID,
		SC.ID as SchoolID,
		COUNT(GP.ID) as NumberOfParticipants
	FROM GroupParticipants as GP
	INNER JOIN Schools as SC ON GP.SchoolInstituteID = SC.InstituteID
	WHERE ParticipantType = 1
GROUP BY
	GroupID,
	SC.ID
), CountNoneParticipants AS (
	SELECT 
		GP.GroupID,
		SC.ID as SchoolID,
		COUNT(GP.ID) as NumberOfNoneParticipants
	FROM GroupParticipants as GP
	INNER JOIN Schools as SC ON GP.SchoolInstituteID = SC.InstituteID
	WHERE ParticipantType != 1
GROUP BY
	GroupID,
	SC.ID
), CountInstructors AS (
	SELECT 
	GI.GroupID,
	SC.ID as SchoolID,
	COUNT(GI.ID) as NumberOfInstructors
FROM InstructorGroups as GI
INNER JOIN Schools as SC ON GI.SchoolID = SC.ID
GROUP BY
	GI.GroupID,
	SC.ID
)

SELECT 
	SC.Region,
	SC.Name as SchoolName,
	SC.InstituteID,
	TR.DepartureDate,
	G.GroupExternalID,
	CP.NumberOfParticipants,
	ISNULL(CNP.NumberOfNoneParticipants, 0) + ISNULL(CI.NumberOfInstructors, 0) as CountInstructors
FROM SubGroupSchools as SG 
INNER JOIN Groups as G ON G.ID = SG.GroupID
INNER JOIN Expeditions as EX ON G.OrderID = EX.ID
INNER JOIN Trips as TR ON EX.TripID = TR.ID
INNER JOIN Schools as SC ON SG.SchoolID = SC.ID
LEFT JOIN CountParticipants as CP ON SG.GroupID = CP.GroupID AND SG.SchoolID = CP.SchoolID
LEFT JOIN CountNoneParticipants as CNP ON SG.GroupID = CNP.GroupID AND SG.SchoolID = CNP.SchoolID
LEFT JOIN CountInstructors as CI ON SG.GroupID = CI.GroupID AND SG.SchoolID = CI.SchoolID
WHERE 
	G.IsActive = 1 AND
	SC.IsActive = 1 AND
	TR.DepartureDate >= @StartDate AND
	TR.DepartureDate <= @EndDate AND
	(SC.Region = @Region OR @Region IS NULL) AND
	(SC.InstituteID = @InstituteID OR @InstituteID IS NULL)";


        public static string DeleteGroupTripDay => @"DELETE GroupTripPlan
                                                        WHERE 
	                                                        GroupID = @GroupID 
	                                                        AND StartTime >= @StartDate
	                                                        AND StartTime < @EndDate";

        public static string GetInstructorsAssignmentsForGroups => @"SELECT 
	INST.FirstNameHE as FirstName,
	INST.LastNameHE as LastName,
	IG.InstructorID as ID,
	IG.RoleID as InstructorGroupRoleID,
	INST.ValidityEndTime,
	INST.InstructorID,
	IR.Name as RoleName,
	SC.ID as SchoolID,
	SC.Name as SchoolName,
	SC.InstituteID,
	SC.Region,
	GP.ID as GroupID,
	GP.GroupExternalID,
	TR.DepartureDate
FROM InstructorGroups as IG
INNER JOIN Instructors as INST ON IG.InstructorID = INST.ID
INNER JOIN InstructorRoles as IR ON INST.RoleID = IR.ID
INNER JOIN Schools as SC ON IG.SchoolID = SC.ID
INNER JOIN Groups as GP ON IG.GroupID = GP.ID
INNER JOIN Expeditions as EX ON GP.OrderID = EX.ID
INNER JOIN Trips as TR ON EX.TripID = TR.ID
WHERE 
    GP.IsActive = 1 AND
	TR.DepartureDate >= @StartDate AND
	TR.DepartureDate <= @EndDate AND
	(IG.RoleID IN @Roles OR @CountRoles = 0) AND
	(SC.Region = @Region OR @Region IS NULL) AND
	(INST.InstructorID = @InstructorID OR @InstructorID IS NULL) AND
	(GP.GroupExternalID = @GroupExternalID OR @GroupExternalID IS NULL)";


        public static string UpdateInstructorRoleByInstructorID => @"UPDATE Instructors SET RoleID = @RoleID WHERE ID = @ID";

        public static string GroupJoinedParticipantionList => @"SELECT 
	                                                                GP.ID,
	                                                                GP.FirstNameHe AS FirstNameHe,
	                                                                GP.LastNameHe AS LastNameHe,
	                                                                GP.FirstNameEn AS FirstNameEn,
	                                                                GP.LastNameEn AS LastNameEn,
	                                                                GP.ParticipantID AS PersonalID,
	                                                                GP.ID AS GroupID,
	                                                                GP.GroupID AS GroupExternalID,
	                                                                GP.PassportNumber,
	                                                                GP.ParticipantType AS RoleID,
	                                                                ParticipantInternalType = 'Participant'
                                                                FROM GroupParticipants as GP
                                                                INNER JOIN Users as US ON GP.UserID = US.ID
                                                                WHERE GP.GroupID = @ID AND GP.ParticipantType NOT IN (8,9)

                                                                UNION ALL

                                                                SELECT 
	                                                                IG.InstructorID as ID,
	                                                                INST.FirstNameHE as FirstNameHe,
	                                                                INST.LastNameHE as LastNameHe,
	                                                                INST.FirstNameEN AS FirstNameEn,
	                                                                INST.LastNameEN AS LastNameEn,
	                                                                INST.InstructorID AS PersonalID,
	                                                                GP.ID as GroupID,
	                                                                GP.GroupExternalID AS GroupExternalID,
	                                                                INST.PassportID AS PassportNumber,
	                                                                IG.RoleID as RoleID,
	                                                                ParticipantInternalType = 'Stuff'
                                                                FROM InstructorGroups as IG
                                                                INNER JOIN Instructors as INST ON IG.InstructorID = INST.ID
                                                                INNER JOIN InstructorRoles as IR ON INST.RoleID = IR.ID
                                                                INNER JOIN Groups as GP ON IG.GroupID = GP.ID
                                                                INNER JOIN Expeditions as EX ON GP.OrderID = EX.ID
                                                                INNER JOIN Trips as TR ON EX.TripID = TR.ID
                                                                WHERE 
                                                                    GP.IsActive = 1 AND
	                                                                IG.GroupID = @ID";

        public static string DeleteMultipleFlights => @"UPDATE Flights SET IsActive = 0 WHERE ID IN @IDs
                                                        DELETE TripsFlightAssociations WHERE FlightID IN @IDs";

        public static string CheckIfTripHasFlights = @"SELECT COUNT(*) as NumberOfFLights 
                                                        FROM TripsFlightAssociations as FA
                                                        INNER JOIN Flights as FL ON FL.ID = FA.FlightID
                                                        WHERE TripID = @ID AND FL.IsActive = 1";

        public static string GetUserIDByEmail => @"SELECT ID From Users as U WHERE U.Email = @Username";

        public static string InsertNewPasswordChangeRequest => @"INSERT INTO PasswordChangeRequests (ID, RequestDate, UserID) VALUES (@ID, @RequestDate, @UserID)";

        public static string GetPasswordChangeRequest => @"SELECT * FROM PasswordChangeRequests WHERE ID = @UserID";

        public static string UpdateUserPassword => @"UPDATE Users SET Password = @Password WHERE ID = @UserID";

        public static string GetSchoolManagerDetailsBySchoolID => @"SELECT S.ManagerEmail, S.Manager FROM Schools as S WHERE S.ID = @SchoolID";

        public static string AddExpedition => @"INSERT INTO Expeditions 
(TripID, NumberOFpassengers, SchoolID, BasesPerDay, Manager, ManagerPhone, ManagerEmail, ManagerCell, Comments, Status, AgentStatus, AgentApproval, IsActive, CreationDate, UpdateDate, UserID, AgencyID)
VALUES (@TripID, @NumberOFpassengers, @SchoolID, @BasesPerDay, @Manager, @ManagerPhone, @ManagerEmail, @ManagerCell, @Comments, @Status, @AgentStatus, @AgentApproval, @IsActive, @CreationDate, @UpdateDate, @UserID, @AgencyID)
SELECT CAST(SCOPE_IDENTITY() as int)";

        public static string GetTripById => @"SELECT * FROM Trips WHERE ID = @ID";

        public static string GetTripFlightsByTripID => @"SELECT 
	F.*
FROM Flights as F
INNER JOIN TripsFlightAssociations as TF ON F.ID = TF.FlightID
WHERE TF.TripID = @TripID";

        public static string AddExpeditionFlight => @"INSERT INTO ExpeditionsFlights (ExpeditionID, FlightID, NumberOfPassengers)
VALUES (@ExpeditionID, @FlightID, @NumberOfPassengers)
SELECT CAST(SCOPE_IDENTITY() as int)";

		public static string GetTripFlightsAvailability => @";WITH #BookedSeats AS (
	 SELECT 
	    TR.ID,
	    ISNULL(SUM(EXF.NumberOfPassengers),0) BookedPassengers
    FROM Trips as TR
    LEFT JOIN Expeditions AS EX ON TR.ID = EX.TripID
    LEFT JOIN ExpeditionsFlights as EXF ON EX.ID = EXF.ExpeditionID
    LEFT JOIN Flights AS FL ON EXF.FlightID = FL.ID
    WHERE EX.Status IN (2,3) AND FL.TakeOffFromIsrael = 1 AND TR.ID = @TripID
    GROUP BY TR.ID
)

SELECT 
	TR.ID,
	FTA.FlightID,
	ISNULL(SUM(F.NumberOfPassengers),0) TotalSeats,
	ISNULL(SUM(B.BookedPassengers),0) BookedPassengers
FROM Trips as TR
INNER JOIN TripsFlightAssociations as FTA ON TR.ID = FTA.TripID
INNER JOIN Flights as F ON FTA.FlightID = F.ID
LEFT JOIN #BookedSeats as B ON TR.ID = B.ID
WHERE TR.ID = @TripID AND F.TakeOffFromIsrael = 1
GROUP BY
	TR.ID,
	FTA.FlightID";

		public static string GetExpeditionStatusReport => @"SELECT 
							e.ID AS ExpeditionId,
							e.TripID,
							e.NumberOFpassengers AS NumOfPassenges,
							e.BasesPerDay AS NumOfBuses,
							e.Manager,
							e.ManagerPhone,
							e.ManagerCell,
							e.ManagerEmail,
							e.Status AS StatusId,
							T.DepartureDate,
							s.InstituteID,
							s.Name AS SchoolName,
							s.Region AS RegionID
						FROM Expeditions e
						INNER JOIN Schools AS s ON e.SchoolID = s.ID
						INNER JOIN Trips t ON e.TripID = T.ID
						WHERE
						t.DepartureDate >= @StartDate AND t.DepartureDate <= @EndDate
						AND (e.Status IN (@StatusId) OR @StatusId IS NULL)";

		public static string GetExpeditionByTripID => @"SELECT
															e.ID,
															e.TripID,
															e.NumberOFpassengers,
															e.BasesPerDay,
															e.Manager,
															e.ManagerPhone,
															e.ManagerEmail,
															e.ManagerCell,
															e.Comments,
															e.Status,
															e.AgentStatus,
															e.AgentApproval,
															e.IsActive,
															e.CreationDate,
															e.UpdateDate,
															e.SchoolID,
															s.Name as SchoolName
														FROM Expeditions as e
														INNER JOIN Schools S on e.SchoolID = S.ID
														WHERE e.TripID = @TripID";


		public static string GetInstructorIDByEmail => @"SELECT ID FROM Instructors WHERE Email = @Email AND IsActive = 1";


		#region Move Group Queries

		public static string GetOrderandSchoolIDFromGroupID => @"SELECT
																	e.SchoolID,
																	g.OrderID
																FROM Groups as g
																INNER JOIN Expeditions as e ON g.OrderID = e.ID
																WHERE g.ID = @GroupID";

		public static string GetAvailableTripsForMovingOrder => @"SELECT
    t.ID as TripID,
    t.DepartureDate,
    t.ReturningDate as ReturnDate,
    t.DestinationID,
    t.ReturingField,
    e.ID as OrderID,
    e.NumberOFpassengers as NumOfPassangersRequired,
    e.BasesPerDay as BusesRequiredPerDay,
    e.Manager as ExpeditionManager,
    e.Status,
    e.AgentStatus
FROM Expeditions as e
INNER JOIN trips as t ON e.TripID = t.ID
WHERE
    e.ID <> @OrderID
    AND e.SchoolID = @SchoolID
    AND e.Status IN (1,2) -- Pending Approval
    AND t.DepartureDate > GETDATE()";

		public static string CancelOrderbyID => @"UPDATE Expeditions SET Status = @OrderStatus WHERE ID = @OriginalOrderID";

		public static string RemovePassengersByOrderID => @"DELETE ExpeditionsFlights WHERE ExpeditionID = @OriginalOrderID";

		public static string MoveGroupToAnotherOrder => @"UPDATE Groups SET OrderID = @SelectedOrderID WHERE ID = @OriginalGroupID";

		public static string GetOriginalOrderDetails => @"SELECT
    e.ID,
    e.TripID,
    e.Status,
    e.AgentStatus,
    e.Manager as ExpeditionManager,
    e.ManagerPhone as ExpeditionManagerPhone,
    e.ManagerCell as ExpeditionManagerCell,
    e.ManagerEmail as ExpeditionManagerEmail,
    e.BasesPerDay as BusesRequiredPerDay,
    e.NumberOFpassengers as NumOfPassangersRequired,
    e.SchoolID,
    s.Name,
    s.InstituteID
FROM Expeditions as e
INNER JOIN Schools S on e.SchoolID = S.ID
WHERE e.ID = @OrderID";

		public static string GetOriginalTripDetails => @"SELECT
    t.ID as TripID,
    t.ReturingField,
    t.DestinationID,
    t.ReturningDate,
    t.DepartureDate
FROM trips as t
WHERE t.ID = @TripID";


		public static string GetOrderForClonning => @"SELECT
														e.ID,
														e.TripID as TripId,
														e.Status,
														e.AgentStatus,
														e.Manager as ExpeditionManager,
														e.ManagerPhone as ExpeditionManagerPhone,
														e.ManagerCell as ExpeditionManagerCell,
														e.ManagerEmail as ExpeditionManagerEmail,
														e.BasesPerDay as BusesRequiredPerDay,
														e.NumberOFpassengers as NumOfPassangersRequired,
														e.SchoolID
													FROM Expeditions as e
													INNER JOIN Schools S on e.SchoolID = S.ID
													WHERE e.ID IN (SELECT
														g.OrderID
													FROM Groups as g
													INNER JOIN Expeditions as e ON g.OrderID = e.ID
													WHERE g.ID = @GroupID)";


		#endregion
	}


}
