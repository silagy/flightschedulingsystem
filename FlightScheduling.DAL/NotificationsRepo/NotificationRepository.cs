﻿using System;
using FlightScheduling.DAL.CustomEntities.Notifications;

namespace FlightScheduling.DAL.NotificationsRepo
{
    public static class NotificationRepository
    {
        public static Notifications CreateWelcomeMessage(Users user)
        {
            Notifications notification = new Notifications();
                notification.NotificationTempateID = 1;
                notification.MessageSubject = @"ברוך הבא למערכת הומלנד";
                
                notification.MessageBody = string.Format(@"<h4>שלום רב, {0}
            </h4><p>חשבונך במערכת הטיסות לפולין אושר, על ידי מנהל המערכת. סיסמתך למערכת היא <strong>{1}</strong></p>
            <p>הכניסה למערכת מאובטחת. יש לשמור את פרטי הכניסה והסיסמה במקום בטוח.</p>
            <p>בברכה, מנהלת פולין</p>", user.Firstname, user.Lastname);

                notification.MessageBody += "<p>אין להשיב למיל זה <br />בכל נושא יש לפנות למינהלת פולין</p>";
                
                notification.CreationDate = DateTime.Now;
                notification.CallToAction = "";
                notification.IsRead = false;
                notification.EmailAddress = user.Email;
                notification.UserID = user.ID;
                notification.NotificationStatus = eNotificationStatus.Pendding;

                return notification;
        }
        
        public static bool IsValidEmail(string email)
        {
            try
            {
                var addr = new System.Net.Mail.MailAddress(email);
                return addr.Address == email;
            }
            catch
            {
                return false;
            }
        }
    }
}