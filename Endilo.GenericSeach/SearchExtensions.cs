﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Web.Mvc;
using GenericSearch;

namespace Endilo.GenericSearch
{
    public static class SearchExtensions
    {
        public static IQueryable<T> ApplySearchCriterias<T>(this IQueryable<T> query, IEnumerable<AbstractSearch> searchCriterias)
        {
            foreach (var criteria in searchCriterias)
            {
                query = criteria.ApplyToQuery(query);
            }

            var result = query.ToArray();

            return query;
        }

        public static ICollection<AbstractSearch> GetDefaultSearchCriterias(this Type type)
        {
            var properties = type.GetProperties()
                .Where(p => p.CanRead && p.CanWrite)
                .OrderBy(p => p.Name);

            var searchCriterias = properties
                .Select(p => CreateSearchCriteria(type, p.PropertyType, p.Name))
                .Where(s => s != null)
                .ToList();

            return searchCriterias;
        }

        public static ICollection<AbstractSearch> AddDefaultSearchCriteria<T>(this ICollection<AbstractSearch> searchCriterias, Expression<Func<T, object>> property)
        {
            Type propertyType = null;
            string fullPropertyPath = GetPropertyPath(property, out propertyType);

            AbstractSearch searchCriteria = CreateSearchCriteria(typeof(T), propertyType, fullPropertyPath);

            if (searchCriteria != null)
            {
                searchCriterias.Add(searchCriteria);
            }

            return searchCriterias;
        }

        public static ICollection<AbstractSearch> AddDefaultSearchCriteria<T>(this ICollection<AbstractSearch> searchCriterias, Expression<Func<T, object>> property, string customLabel)
        {
            Type propertyType = null;
            string fullPropertyPath = GetPropertyPath(property, out propertyType);

            AbstractSearch searchCriteria = CreateSearchCriteria(typeof(T), propertyType, fullPropertyPath);

            if (!String.IsNullOrEmpty(customLabel))
            {
                searchCriteria.CustomLabel = customLabel;

            }

            if (searchCriteria != null)
            {
                searchCriterias.Add(searchCriteria);
            }

            return searchCriterias;
        }

        public static ICollection<AbstractSearch> AddTextSearchCriteria<T>(this ICollection<AbstractSearch> searchCriterias, Expression<Func<T, object>> property, eTextComparators i_comperer)
        {
            Type propertyType = null;
            string fullPropertyPath = GetPropertyPath(property, out propertyType);

            TextSearch searchCriteria = new TextSearch();
            searchCriteria.Property = fullPropertyPath;
            searchCriteria.Comparator = i_comperer;
            searchCriteria.TargetTypeName = typeof(T).AssemblyQualifiedName;



            if (searchCriteria != null)
            {
                searchCriterias.Add(searchCriteria);
            }

            return searchCriterias;
        }

        public static ICollection<AbstractSearch> AddBolleanSearchCriteria<T>(this ICollection<AbstractSearch> searchCriterias, Expression<Func<T, object>> property, eBolleanComparators eComperer, string customLabel)
        {
            Type propertyType = null;
            string fullPropertyPath = GetPropertyPath(property, out propertyType);

            BooleanSearch searchCriteria = new BooleanSearch();
            searchCriteria.Property = fullPropertyPath;
            searchCriteria.comperer = eComperer;
            if (!String.IsNullOrEmpty(customLabel))
            {
                searchCriteria.CustomLabel = customLabel;

            }
            searchCriteria.TargetTypeName = typeof(T).AssemblyQualifiedName;



            if (searchCriteria != null)
            {
                searchCriterias.Add(searchCriteria);
            }

            return searchCriterias;
        }

        public static ICollection<AbstractSearch> AddNumaricSearchCriteria<T>(this ICollection<AbstractSearch> searchCriterias, Expression<Func<T, object>> property, eNumericComparators i_comperer, string customLabel)
        {
            Type propertyType = null;
            string fullPropertyPath = GetPropertyPath(property, out propertyType);

            NumericSearch searchCriteria = new NumericSearch();
            searchCriteria.Property = fullPropertyPath;
            if (!String.IsNullOrEmpty(customLabel))
            {
                searchCriteria.CustomLabel = customLabel;

            }
            searchCriteria.Comparator = i_comperer;
            searchCriteria.TargetTypeName = typeof(T).AssemblyQualifiedName;

            if (searchCriteria != null)
            {
                searchCriterias.Add(searchCriteria);
            }

            return searchCriterias;
        }

        public static ICollection<AbstractSearch> AddEnumSearchCriteria<T>(this ICollection<AbstractSearch> searchCriterias, Expression<Func<T, object>> property, bool isMultiselect, eMultiselectComperer iComperer, string customLabel)
        {
            Type propertyType = null;
            string fullPropertyPath = GetPropertyPath(property, out propertyType);

            EnumSearch searchCriteria = new EnumSearch(propertyType);
            searchCriteria.Multiselect = isMultiselect;
            searchCriteria.Property = fullPropertyPath;
            if (!String.IsNullOrEmpty(customLabel))
            {
                searchCriteria.CustomLabel = customLabel;

            }
            searchCriteria.TargetTypeName = typeof(T).AssemblyQualifiedName;
            searchCriterias.Add(searchCriteria);
            return searchCriterias;
        }

        public static ICollection<AbstractSearch> AddMultipleSelectListSearchCriteria<T>(this ICollection<AbstractSearch> searchCriterias, Expression<Func<T, object>> property, MultiSelectList possibleValues, eMultiselectComperer multiSelectComparer, string customLabel)
        {
            Type propertyType = null;
            string fullPropertyPath = GetPropertyPath(property, out propertyType);

            AbstractSearch searchCriteria = new CustomListSearch()
            {
                Multiselect = true,
                Property = fullPropertyPath,
                TargetTypeName = typeof(T).AssemblyQualifiedName,
                AvailableValues = possibleValues,
                MultiselectComperer = multiSelectComparer,
            };

            if (!String.IsNullOrEmpty(customLabel))
            {
                searchCriteria.CustomLabel = customLabel;

            }
            searchCriterias.Add(searchCriteria);
            return searchCriterias;
        }

        public static ICollection<AbstractSearch> AddCustomMultipleSelectListSearchCriteria<T>(this ICollection<AbstractSearch> searchCriterias, Expression<Func<T, object>> property, MultiSelectList possibleValues, eMultiselectComperer multiSelectComparer, string customLabel)
        {
            Type propertyType = null;
            string fullPropertyPath = GetPropertyPath(property, out propertyType);

            AbstractSearch searchCriteria = new CustomListSearch()
            {
                Multiselect = true,
                Property = fullPropertyPath,
                TargetTypeName = typeof(T).AssemblyQualifiedName,
                AvailableValues = possibleValues,
                MultiselectComperer = multiSelectComparer,
            };

            if (!String.IsNullOrEmpty(customLabel))
            {
                searchCriteria.CustomLabel = customLabel;

            }
            searchCriterias.Add(searchCriteria);
            return searchCriterias;
        }



        public static ICollection<AbstractSearch> AddSingleSelectListSearchCriteria<T>(this ICollection<AbstractSearch> searchCriterias, Expression<Func<T, object>> property, SelectList possibleValues, eMultiselectComperer multiSelectComparer, string customLabel)
        {
            Type propertyType = null;
            string fullPropertyPath = GetPropertyPath(property, out propertyType);

            AbstractSearch searchCriteria = new CustomListSearch()
            {
                Multiselect = false,
                Property = fullPropertyPath,
                TargetTypeName = typeof(T).AssemblyQualifiedName,
                AvailableValues = possibleValues,
                MultiselectComperer = multiSelectComparer,
            };

            if (!String.IsNullOrEmpty(customLabel))
            {
                searchCriteria.CustomLabel = customLabel;

            }

            searchCriterias.Add(searchCriteria);
            return searchCriterias;
        }

        private static AbstractSearch CreateSearchCriteria(Type targetType, Type propertyType, string property)
        {
            AbstractSearch result = null;

            if (propertyType.Equals(typeof(string)))
            {
                result = new TextSearch();
            }
            else if (propertyType.Equals(typeof(int)) || propertyType.Equals(typeof(int?)))
            {
                result = new NumericSearch();
            }
            else if (propertyType.Equals(typeof(DateTime)) || propertyType.Equals(typeof(DateTime?)))
            {
                result = new DateSearch();
            }
            else if (propertyType.IsEnum)
            {
                result = new EnumSearch(propertyType);
            }
            else if (propertyType.Equals(typeof(Boolean)) || propertyType.Equals(typeof(Boolean?)))
            {
                result = new BooleanSearch();
            }

            if (result != null)
            {
                result.Property = property;
                result.TargetTypeName = targetType.AssemblyQualifiedName;
            }

            return result;
        }

        private static AbstractSearch CreateSearchCriteria(Type targetType, Type propertyType, string property, string customLabel)
        {
            AbstractSearch result = null;

            if (propertyType.Equals(typeof(string)))
            {
                result = new TextSearch();
            }
            else if (propertyType.Equals(typeof(int)) || propertyType.Equals(typeof(int?)))
            {
                result = new NumericSearch();
            }
            else if (propertyType.Equals(typeof(DateTime)) || propertyType.Equals(typeof(DateTime?)))
            {
                result = new DateSearch();
            }
            else if (propertyType.IsEnum)
            {
                result = new EnumSearch(propertyType);
            }

            if (result != null)
            {
                result.Property = property;
                result.TargetTypeName = targetType.AssemblyQualifiedName;

                if (!String.IsNullOrEmpty(customLabel))
                {
                    result.CustomLabel = customLabel;

                }
            }

            return result;
        }



        public static ICollection<AbstractSearch> AddCustomSearchCriteria<T>(this ICollection<AbstractSearch> searchCriterias, Expression<Func<T, object>> property, string customLabel)
        {
            Type propertyType = null;
            string fullPropertyPath = GetPropertyPath(property, out propertyType);

            AbstractSearch searchCriteria = CreateSearchCriteria(typeof(T), propertyType, fullPropertyPath, customLabel);

            if (searchCriteria != null)
            {
                searchCriterias.Add(searchCriteria);
            }

            return searchCriterias;
        }

        private static string GetPropertyPath<T>(Expression<Func<T, object>> expression, out Type targetType)
        {
            var lambda = expression as LambdaExpression;
            MemberExpression memberExpression;
            if (lambda.Body is UnaryExpression)
            {
                var unaryExpression = lambda.Body as UnaryExpression;
                memberExpression = unaryExpression.Operand as MemberExpression;
            }
            else
            {
                memberExpression = lambda.Body as MemberExpression;
            }

            if (memberExpression == null)
            {
                throw new ArgumentException("Please provide a lambda expression like 'n => n.PropertyName'", "expression");
            }

            var propertyInfo = memberExpression.Member as PropertyInfo;
            targetType = propertyInfo.PropertyType;

            string property = memberExpression.ToString();

            return property.Substring(property.IndexOf('.') + 1);
        }
    }
}
