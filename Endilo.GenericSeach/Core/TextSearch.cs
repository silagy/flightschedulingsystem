﻿using System.ComponentModel.DataAnnotations;
using System.Linq.Expressions;

namespace Endilo.GenericSearch
{
    public class TextSearch : AbstractSearch
    {
        public string SearchTerm { get; set; }

        public eTextComparators Comparator { get; set; }

        protected override Expression BuildExpression(MemberExpression property)
        {
            if (this.SearchTerm == null)
            {
                return null;
            }

            var searchExpression = Expression.Call(
                property,
                typeof(string).GetMethod(this.Comparator.ToString(), new[] { typeof(string) }), Expression.Constant(this.SearchTerm));

            return searchExpression;
        }

    }

  
}
