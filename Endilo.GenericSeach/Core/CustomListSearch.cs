﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Web.Mvc;
using System.Linq.Expressions;

namespace Endilo.GenericSearch
{
    public class CustomListSearch : AbstractSearch
    {
		public IEnumerable<String> SearchTerms { get; set; }

		public IEnumerable<SelectListItem> AvailableValues { get; set; }
		
		public bool Multiselect { get; set; }

		public eMultiselectComperer MultiselectComperer { get; set; }

        protected override Expression BuildExpression(MemberExpression property)
        {
			if (SearchTerms == null) return null;
			var propertyType = property.Type;
			IEnumerable<Expression> equalityExpressions;

            if (!Multiselect && String.IsNullOrEmpty(SearchTerms.FirstOrDefault()))
            {
                return null;
            }

			if (propertyType == typeof(int))
			{
				equalityExpressions = SearchTerms.Select(v => Expression.Equal(property, Expression.Constant((int.Parse(v)))));
			}
			else
			{
				equalityExpressions = SearchTerms.Select(v => Expression.Equal(property, Expression.Constant((v.ToString()))));
			}

			Expression searchExpression = equalityExpressions.FirstOrDefault();
			foreach (var exp in equalityExpressions.Skip(1))
			{
				switch (MultiselectComperer)
				{
					case eMultiselectComperer.Or:
						searchExpression = Expression.Or(exp, searchExpression);
						break;
					case eMultiselectComperer.And:
						searchExpression = Expression.And(exp, searchExpression);
						break;
					default:
						searchExpression = null;
						break;
				}
			}

			return searchExpression;
        }

    }
}
