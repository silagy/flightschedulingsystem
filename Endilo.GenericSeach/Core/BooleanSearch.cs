﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel.DataAnnotations;
using System.Linq.Expressions;

namespace Endilo.GenericSearch
{
    public class BooleanSearch : AbstractSearch
    {
        public bool SearchTerm { get; set; }

        public eBolleanComparators comperer { get; set; }

        public eBolleanOptions EBolleanOptions { get; set; }


        protected override Expression BuildExpression(MemberExpression property)
        {


            if (this.SearchTerm == false && comperer == eBolleanComparators.CheckBox)
            {
                return null;
            }
            else
            {
                switch (EBolleanOptions)
                {
                        case eBolleanOptions.None:
                    {
                        return null;
                        break;
                    }
                        case eBolleanOptions.No:
                    {
                        SearchTerm = false;
                        break;
                    }
                        case eBolleanOptions.Yes:
                    {
                        SearchTerm = true;
                        break;
                    }
                        
                }
            }


            var searchExpression = Expression.Equal(property, Expression.Constant(SearchTerm));

            return searchExpression;
        }
    }


}
