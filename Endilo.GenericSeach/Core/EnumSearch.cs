﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace Endilo.GenericSearch
{
    public class EnumSearch : AbstractSearch
    {
        public ICollection<string> SearchTerms { get; set; }

		public bool Multiselect { get; set; }

		public eMultiselectComperer MultiselectComperer { get; set; }


		public Type EnumType
        {
            get
            {
                return Type.GetType(this.EnumTypeName);
            }
        }

        public string EnumTypeName { get; set; }

        public EnumSearch()
        {
        }

        public EnumSearch(Type enumType)
        {
            this.EnumTypeName = enumType.AssemblyQualifiedName;
        }

		public EnumSearch(Type enumType, bool isMultiselect)
		{
			this.EnumTypeName = enumType.AssemblyQualifiedName;
			this.Multiselect = isMultiselect;
		}

        protected override System.Linq.Expressions.Expression BuildExpression(System.Linq.Expressions.MemberExpression property)
        {
            if (this.SearchTerms == null)
            {
                return null;
            }
			
			if (this.SearchTerms.First() == string.Empty)
			{
				return null;
			}

			var enumValues = SearchTerms.Select(s => Enum.Parse(this.EnumType, s));

			IEnumerable<Expression> equalityExpressions = enumValues.Select(v=> Expression.Equal(property,Expression.Constant(v)));

			Expression searchExpression = equalityExpressions.FirstOrDefault();
			foreach (var exp in equalityExpressions.Skip(1))
			{
				if (MultiselectComperer == eMultiselectComperer.Or)
				{
					searchExpression = Expression.Or(exp, searchExpression);
				}
				else
				{
					searchExpression = Expression.And(exp, searchExpression);
				}
			}

            return searchExpression;
        }
    }
}
