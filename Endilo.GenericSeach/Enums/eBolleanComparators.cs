﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GenericSearch.Language;

namespace Endilo.GenericSearch
{
	public enum eBolleanComparators
	{
        [Display(Name = "CheckBox")]
		CheckBox,

        [Display(Name = "DropdownList")]
		DropdownList
	}

    public enum eBolleanOptions
    {
        [Display(ResourceType = typeof(EnumStrings), Name = "None")]
        None = 0,

        [Display(ResourceType = typeof(EnumStrings), Name = "Yes")]
        Yes = 1,
        [Display(ResourceType = typeof(EnumStrings), Name = "No")]
        No = 2
    }
}
