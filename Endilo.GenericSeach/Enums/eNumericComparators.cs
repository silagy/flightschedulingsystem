﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Endilo.GenericSearch
{
	public enum eNumericComparators
	{
		[Display(Name = "<")]
		Less,

		[Display(Name = "<=")]
		LessOrEqual,

		[Display(Name = "==")]
		Equal,

		[Display(Name = ">=")]
		GreaterOrEqual,

		[Display(Name = ">")]
		Greater,
        [Display(Name = "Contains")]
        Contains
	}
}
