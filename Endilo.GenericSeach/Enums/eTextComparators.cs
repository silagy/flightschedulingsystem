﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Endilo.GenericSearch
{
	public enum eTextComparators
	{
		[Display(Name = "Contains")]
		Contains,

		[Display(Name = "==")]
		Equals
	}
}
