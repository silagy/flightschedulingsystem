﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GenericSearch.Enums;

namespace Endilo.GenericSearch
{
	public enum eDateComparators 
	{
		[Display(Name = "Range")]
		InRange,

		[Display(Name = "<")]
		Less,

		[Display(Name = "<=")]
		LessOrEqual,

		[Display(Name = "==")]
		Equal,

		[Display(Name = ">=")]
		GreaterOrEqual,

		[Display(Name = ">")]
		Greater,

		
	}
}
