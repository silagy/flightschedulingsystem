﻿using System;
using System.Web;
using System.Linq;
using System.Collections.Generic;
using System.Data;
using System.Security.Cryptography;
using System.Text;



namespace Flight.Membership
{
	public static class CryptoHelpers
	{
		public static readonly HashAlgorithm k_SHA256CryptoAlgorithm = new SHA256CryptoServiceProvider();

		public static string EncryptPassword(string i_Password, HashAlgorithm i_CryptoAlgorithm)
		{
			Byte[] inputBytes = Encoding.UTF8.GetBytes(i_Password);
			Byte[] hashedBytes = i_CryptoAlgorithm.ComputeHash(inputBytes);

			StringBuilder stringBuilder = new StringBuilder();
			foreach (byte b in hashedBytes)
			{
				stringBuilder.AppendFormat("{0:X2}", b);
			}

			return stringBuilder.ToString();
		}
	}

}