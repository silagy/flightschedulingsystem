﻿using System;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Security;
using System.Collections.Generic;
using System.Linq;
using FlightScheduling.DAL;
using Otakim.Language;
using System.Data.Entity;
using System.Data.Entity.Migrations;

namespace Flight.Membership
{
    public class FlightMembershipProvider : MembershipProvider
    {
        public override string ApplicationName
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public override bool ChangePassword(string username, string oldPassword, string newPassword)
        {
            try
            {
                using (var _db = new FlightDB())
                {
                    Users _userToUpdatePass = _db.Users.FirstOrDefault(u => u.Email == username);
                    var hashCodeForOldPass = CryptoHelpers.EncryptPassword(oldPassword, CryptoHelpers.k_SHA256CryptoAlgorithm);

                    if (string.Equals(hashCodeForOldPass, _userToUpdatePass.Password, StringComparison.InvariantCultureIgnoreCase))
                    {
                        var _attachedUser = _db.Users.Attach(_userToUpdatePass);
                        if (_attachedUser != null)
                        {
                            var _newPassword = CryptoHelpers.EncryptPassword(newPassword, CryptoHelpers.k_SHA256CryptoAlgorithm);
                            _attachedUser.Password = _newPassword;
                            _db.SaveChanges();
                            return true;
                        }
                    }
                }
            }
            catch (Exception)
            {
                return false;
            }
            return false;
        }

        public override bool ChangePasswordQuestionAndAnswer(string username, string password, string newPasswordQuestion, string newPasswordAnswer)
        {
            throw new NotImplementedException();
        }

        public override MembershipUser CreateUser(string username, string password, string email, string passwordQuestion, string passwordAnswer, bool isApproved, object providerUserKey, out MembershipCreateStatus status)
        {
            throw new NotImplementedException();
        }

        public Users CreateUser(Users i_userToCreate)
        {

            using (var _db = new FlightDB())
            {
                _db.Database.CommandTimeout = 180;
                //i_userToCreate.Roles.Add(_db.GetRoleByName("User"));
                i_userToCreate.CreationDate = DateTime.Now;
                var _newPassword = CryptoHelpers.EncryptPassword(i_userToCreate.Password, CryptoHelpers.k_SHA256CryptoAlgorithm);
                i_userToCreate.Password = _newPassword;

                //Role role = _db.Roles.Find((int) Role.eRole.User);
                //i_userToCreate.Roles.Add(role);

               return i_userToCreate = _db.CreateUser(i_userToCreate);
                //FlightRoleProvider roleProvider = new FlightRoleProvider();
                //return roleProvider.AddUserToRoles(i_userToCreate, new Role.eRole[] { Role.eRole.User });

            }
        }

        public override bool DeleteUser(string username, bool deleteAllRelatedData)
        {
            throw new NotImplementedException();
        }

        public override bool EnablePasswordReset
        {
            get { throw new NotImplementedException(); }
        }

        public override bool EnablePasswordRetrieval
        {
            get { throw new NotImplementedException(); }
        }

        public override MembershipUserCollection FindUsersByEmail(string emailToMatch, int pageIndex, int pageSize, out int totalRecords)
        {
            throw new NotImplementedException();
        }

        public override MembershipUserCollection FindUsersByName(string usernameToMatch, int pageIndex, int pageSize, out int totalRecords)
        {
            throw new NotImplementedException();
        }

        public override MembershipUserCollection GetAllUsers(int pageIndex, int pageSize, out int totalRecords)
        {
            throw new NotImplementedException();
        }

        public override int GetNumberOfUsersOnline()
        {
            throw new NotImplementedException();
        }

        public override string GetPassword(string username, string answer)
        {
            throw new NotImplementedException();
        }

        public override MembershipUser GetUser(string username, bool userIsOnline)
        {
            using (var _db = new FlightDB())
            {
                Users _user = _db.Users.FirstOrDefault(u => u.Email.Equals(username));

                if (_user != null)
                {
                    return new MembershipUser("FlightMembershipProvider", username,
                       _user, _user.Email, null,
                       null, true, false, DateTime.MinValue, DateTime.MinValue,
                       DateTime.MinValue, DateTime.MinValue, DateTime.MinValue);
                }
                else
                {
                    return null;
                }
            } 
        }

        public override MembershipUser GetUser(object providerUserKey, bool userIsOnline)
        {
            throw new NotImplementedException();
        }

        public override string GetUserNameByEmail(string email)
        {
            throw new NotImplementedException();
        }

        public override int MaxInvalidPasswordAttempts
        {
            get { throw new NotImplementedException(); }
        }

        public override int MinRequiredNonAlphanumericCharacters
        {
            get { throw new NotImplementedException(); }
        }

        public override int MinRequiredPasswordLength
        {
            get { throw new NotImplementedException(); }
        }

        public override int PasswordAttemptWindow
        {
            get { throw new NotImplementedException(); }
        }

        public override MembershipPasswordFormat PasswordFormat
        {
            get { throw new NotImplementedException(); }
        }

        public override string PasswordStrengthRegularExpression
        {
            get { throw new NotImplementedException(); }
        }

        public override bool RequiresQuestionAndAnswer
        {
            get { throw new NotImplementedException(); }
        }

        public override bool RequiresUniqueEmail
        {
            get { throw new NotImplementedException(); }
        }

        public override string ResetPassword(string username, string answer)
        {
            throw new NotImplementedException();
        }

        public override bool UnlockUser(string userName)
        {
            throw new NotImplementedException();
        }

        public override void UpdateUser(MembershipUser user)
        {
            throw new NotImplementedException();
        }

        public override bool ValidateUser(string i_Username, string i_Password)
        {
            bool validationResult = false;

            using (var _context = new FlightDB())
            {
                Users userToValidate = _context.Users.SingleOrDefault(u => u.Email == i_Username);
                if (userToValidate != null)
                {
                    string hash = CryptoHelpers.EncryptPassword(i_Password, CryptoHelpers.k_SHA256CryptoAlgorithm);
                    validationResult = (string.Equals(hash, userToValidate.Password, StringComparison.InvariantCultureIgnoreCase)) ? true : false;


                }
            }

            return validationResult;
        }

        public Users UpdateUser(Users userToUpdate)
        {
            using (var _db = new FlightDB())
            {
                var _newPassword = CryptoHelpers.EncryptPassword(userToUpdate.Password,
                                                                 CryptoHelpers.k_SHA256CryptoAlgorithm);
                userToUpdate.Password = _newPassword;

                _db.Users.AddOrUpdate(userToUpdate);

                int results = _db.SaveChanges();

                return userToUpdate;
            }

        }

       

        /// <summary>
        /// Check if username exist in the system (DB) 
        /// </summary>
        /// <param name="i_username">Getting the username </param>
        /// <returns>True / False</returns>
        public bool CheckUsername(string i_username)
        {
            bool exist = false;

            using (var _db = new FlightDB())
            {
                return _db.Users.Any(u => u.Email.Equals(i_username));
            }
        }

        /// <summary>
        /// Check if username exist in the system (DB) 
        /// </summary>
        /// <param name="i_username">Getting the username </param>
        /// <returns>True / False</returns>
        public bool CheckEmail(string i_email)
        {
            bool exist = false;

            using (var _db = new FlightDB())
            {
                return _db.Users.Any(u => u.Email.Equals(i_email));
            }
        }


        public bool CheckPassword(string password,string userName)
        {
            using (var _db = new FlightDB())
            {
                return _db.Users.FirstOrDefault(u => u.Email == userName).Password.ToUpper() == CryptoHelpers.EncryptPassword(password, CryptoHelpers.k_SHA256CryptoAlgorithm) ? true : false;
            }
        }

        public Users GetUserByEmailOrUserName(string i_UserOrEmail)
        {
            using (var _context = new FlightDB())
            {
                Users userTotoreturn = _context.Users.Include("Agencies").Where(u => u.Email.Equals(i_UserOrEmail)).FirstOrDefault();

                // Fixed bug for admin login --> if logged in with user == "Admin" the system can find him an then NULL is saved inthe session.
                if (userTotoreturn == null)
                {
                    userTotoreturn = _context.Users.Where(u => u.Email == i_UserOrEmail).FirstOrDefault();
                }

                return userTotoreturn;
            }
        }

    }
}