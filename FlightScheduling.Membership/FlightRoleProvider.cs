﻿using System;
using System.Data.Entity.Validation;
using System.Security.Cryptography;
using System.Text;
using System.Web.Security;
using Flight.DAL.FlightDB.Methods;
using FlightScheduling.DAL;
using System.Linq;
using System.Data.Entity;
using System.Collections.Generic;

namespace Flight.Membership
{
    public class FlightRoleProvider : RoleProvider
    {
        public override void AddUsersToRoles(string[] usernames, string[] roleNames)
        {
            throw new NotImplementedException();
        }


        public Users AddUserToRoles(Users i_User, eRole[] roleNames)
        {

            //using (var _db = new FlightDB())
            //{
            //    try
            //    {

            //        _db.Users.Attach(i_User);
            //        _db.Entry(i_User).State = EntityState.Modified;
            //        foreach (var roleName in roleNames)
            //        {
            //            Role role = _db.Roles.FirstOrDefault(r => r.RoleID == (int)roleName);
            //            _db.Roles.Attach(role);
            //            i_User.Roles.Add(role);


            //        }

            //        return _db.UpdateUser(i_User); 

            //        _db.Users.Add(i_User);

            //        int results = 0;
            //        results = _db.SaveChanges();

            //        if (results > 0)
            //        {
            //            return i_User;
            //        }

            //        return null;
            //    }
            //    catch (DbEntityValidationException e)
            //    {
            //        foreach (var eve in e.EntityValidationErrors)
            //        {
            //            Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
            //                eve.Entry.Entity.GetType().Name, eve.Entry.State);
            //            foreach (var ve in eve.ValidationErrors)
            //            {
            //                Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
            //                    ve.PropertyName, ve.ErrorMessage);
            //            }
            //        }
            //        throw;
            //    }

           
           // }
         throw new NotImplementedException();

        }

        public override void CreateRole(string roleName)
        {
            throw new NotImplementedException();
        }

        public override bool DeleteRole(string roleName, bool throwOnPopulatedRole)
        {
            throw new NotImplementedException();
        }

        public override string[] FindUsersInRole(string roleName, string usernameToMatch)
        {
            throw new NotImplementedException();
        }

        public override string[] GetAllRoles()
        {
            throw new NotImplementedException();
        }

        public override string[] GetRolesForUser(string username)
        {
            string[] rolesOfUser;
            using (var _context = new FlightDB())
            {
                Users user = _context.Users.FirstOrDefault(u => u.Email == username);
                if (user == null)
                {
                    return null;
                }
                rolesOfUser = new[]
                {
                    user.RuleEnum.ToString()
                };
            }

            return rolesOfUser;

            throw new NotImplementedException();

        }

        public override string[] GetUsersInRole(string roleName)
        {
            throw new NotImplementedException();
        }

        public override bool IsUserInRole(string username, string roleName)
        {
            var rolesOfUser = GetRolesForUser(username);
            return rolesOfUser.Contains(roleName);
        }

        public override void RemoveUsersFromRoles(string[] usernames, string[] roleNames)
        {
            throw new NotImplementedException();
        }

        public override bool RoleExists(string roleName)
        {
            throw new NotImplementedException();
        }

        public override string ApplicationName
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }
    }
}